package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.ArrayModuleList;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.ExpandableListAdapter;

import java.util.ArrayList;


import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;



import android.R.anim;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.EditText;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import android.widget.TextView;
import android.widget.Toast;


public class PoolShowDialog extends Activity{
    String arrstrname="",gettempname="",getmodulename="",str="",getcurretview="",gettempoption="",apprdval="";
	DatabaseFunction db;
	CommonFunction cf;
	ArrayModuleList ar;
	View v;
	int getid;
	RadioGroup rg;
	RadioButton[] rd;
	CheckBox[] ch,childch;
	EditText[] wateret;
	LinearLayout poollin,lin,rdlin,firstlin,secondlin,seclin,thirdlin,thrlin,fourthlin,fourlin,fifthlin,fivelin;
	EditText ettxt,childettxt,secchildettxt,thrchildettxt,fourchildettxt,fivechildettxt;
	TextView firsttxt,secondtxt,thirdtxt,fourthtxt,fifthtxt;
	String[] rdname,poolfeatures={"Ladders","Diving Board","Slide","Railing","Other"},laddertype={"Stainless Steel","Plastic","Other"},
			 divingboardtype={"Fiberglass","Stainless Frame","Other"},
			 poollighting={"High Voltage","Low Voltage","Fibreoptics",
			"Switch Location","Operable","Other"},switchlocation={"Inside","Outside","Timer Controlled","Remote","Other"},
			poolequipment={"Equipment Supported On Concrete Pad","Location","Protect From Roof Run Off"},location={"Right Side","Rear Side",
			"Left Side","Other"},plumbingsystem={"Material","Skimmer","Main Drain","Valves","Automated Fill System"},material={"PVC","Copper",
			"Brass","Galvanized","PEX"},skimmer={"1 Skimmer","2 Skimmer","Water Line Above Skimmer","Cover - Lid Present","Basket Present",
			"Floating Barrel Present","Other"},maindrain={"Located Deepest End","Safety Drain � Anti-Vortex Cover Present",
			"Safety Drain Cover Not Present","Hydrostatic Valve Not Confirmed","Other"},valves={"Three Port Valve","Motorized Three Port Valve",
			"Reverse Flow Water Heater Valve(s)","Multiport Valve","Check Valve(s)","Gate Valve(s)","Ball Valve","Other"},automated={"Water Level Control Device Present",
			"Programmable","Manual"},poolpumping={"No Of Pumps Present","Pump Types"},pumppresent={"1","2","3","4","Other"},pumptypes={"Booster Pump","Spa Pump",
			"Jazuzzi Pump","Strainer Pot/Basket","Fountain Pump","Other"},filteringsystem={"Sand","Cartridge","Diatomaceous Earth � DE","Piston Type Backwash Valve(s)",
         	"Rotary Type Backwash Valve(s)","Multiport Valve","Backwash Hose Present","Backwash Hose Not Present","Pressure Gauge","Sight Glass","Other"},
         	poolcleaning={"Booster Pump System","Suction Side System","Boosterless Water Pressure System","Electric Robot","Self Cleaning Jets","Other"},
         	boosterpumpsystem={"Vacuum Head","Sweep Head"},watertreatment={"InLine Chlorinator","Pump Chlorinator","Ozone Unit","Floating Chlorine Device",
			"Manual Delivery","Other"},pumpchlor={"Peristaltic","Standard"},waterheatingsystem={"Gas Heater","Solar Heater","Heat Pump","Electric Heater","Oil Heater","Thermostat",
			"On � Off Switch","Other"},gasheater={"Natural Gas","Propane","Electronic Ignition","Pilot Light On","Pilot Light Off","Gas Valve Present",
			"Fusible Link","Pressure Switch","High Limit Switch","Not Tested"},solarheater={"Open Loop","Closed Loop"},thermostat={"Located Inside","Located At Equipment"},
			timers={"Electromechanical Timer","Electronic Timers","Twist Timer","Location","Remote Control(s)","Programmable Controls"},timerlocation={"Inside","At Equipment",
			"Remote","Other"},remotecontrols={"Pneumatic Air Switch","Electronic Switch"},progcontrols={"Inside","Outside"},
			electricalservice={"External Sub Panel","External Receptacle","Wiring Type","Conduit � Wire Protection","Equipment Bonding","Cover Present"},
			extsubpanel={"30 AMP","40 AMP","50 AMP","100 AMP","Circuit Breaker(s)","Shut Off","Other"},wiringtype={"Romex","Other"},conduit={"Plastic","Water Resistant",
			"Other"},poolcovers={"Spa Only","Pool and Spa","Specialty Spa Cover","Bubble Cover","Rollers","Vinyl","Electronic","Fiberglass",
            "Mesh","Wood","Safety Clips/Features Present","Other"},safetyequipment={"Signage","Floatation Device/Toss Ring","Life Hook","Safety Barrier","Other x 3"},
            waterchemistry={"Chlorine residual","Total Alkalinity","pH","Hardness","Total Dissolved Solids","Cyanuric Acid","Temperature"},generalcomments={"Filter",
			"Pumps","Filter Pressure","Heater","Other"};

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		
		    Bundle b = this.getIntent().getExtras();
			arrstrname = b.getString("arrstr");
			Bundle b1 = this.getIntent().getExtras();
			gettempname = b1.getString("tempname");
			Bundle b2 = this.getIntent().getExtras();
			getmodulename = b2.getString("modulename");
			Bundle b3 = this.getIntent().getExtras();
			getcurretview = b3.getString("currentview");
			Bundle b4 = this.getIntent().getExtras();
			gettempoption = b4.getString("tempoption");
			Bundle b5 = this.getIntent().getExtras();
			getid = b5.getInt("id");System.out.println("inside app show dialos");
			
            setContentView(R.layout.poolcustomizedialog);
		    
		    ar = new ArrayModuleList(this,getmodulename);
			cf = new CommonFunction(this);
			db = new DatabaseFunction(this);
			
			TextView hdr = (TextView)findViewById(R.id.header);
			hdr.setText(arrstrname);
			
			firstlin = (LinearLayout)findViewById(R.id.first);
			firsttxt = (TextView)findViewById(R.id.firsttext);
			secondlin = (LinearLayout)findViewById(R.id.second);
			secondtxt = (TextView)findViewById(R.id.secondtext);
			thirdlin = (LinearLayout)findViewById(R.id.third);
			thirdtxt = (TextView)findViewById(R.id.thirdtext);
			fourthlin = (LinearLayout)findViewById(R.id.fourth);
			fourthtxt = (TextView)findViewById(R.id.fourthtext);
			fifthlin = (LinearLayout)findViewById(R.id.fifth);
			fifthtxt = (TextView)findViewById(R.id.fifthtext);
			
			rdlin = (LinearLayout)findViewById(R.id.rdg);
			lin = (LinearLayout)findViewById(R.id.chk);
			seclin = (LinearLayout)findViewById(R.id.secondchk);
			thrlin = (LinearLayout)findViewById(R.id.thirdchk);
			fourlin = (LinearLayout)findViewById(R.id.fourthchk);
			fivelin = (LinearLayout)findViewById(R.id.fifthchk);
			
			 if(arrstrname.equals("Pool Features"))
			 {
			    rdname = poolfeatures;
			 }
			 else if(arrstrname.equals("Pool Lighting"))
			 {
				 rdname = poollighting;				
			 }
			 else if(arrstrname.equals("Pool Equipment"))
			 {
				 rdname = poolequipment;
			 }
			 else if(arrstrname.equals("Plumbing Systems"))
			 {
				 rdname = plumbingsystem;
			 }
			 else if(arrstrname.equals("Pool Pumping System"))
			 {
				 rdname = poolpumping;
			 }
			 /*else if(arrstrname.equals("Filtering System"))
			 {
				 rdname = filteringsystem;
			 }
			*//* else if(arrstrname.equals("Pool Cleaning Equipment"))
			 { 
				 rdname = poolcleaning;
			 }*/
			 else if(arrstrname.equals("Water Treatment"))
			 {
				 rdname = watertreatment;
			 }
			 else if(arrstrname.equals("Water Heating Systems"))
			 {
				 rdname = waterheatingsystem;
			 }
			 else if(arrstrname.equals("Timers/Clocks/Controls"))
			 {
				 rdname = timers;
			 }
			 else if(arrstrname.equals("Electrical Service"))
			 {
				 rdname = electricalservice;
			 }
			 else if(arrstrname.equals("Pool Cover(s)"))
			 {
				 rdname = poolcovers;
			 }
			 else if(arrstrname.equals("Safety Equipment"))
			 {
				 rdname = safetyequipment;
			 }
			 else if(arrstrname.equals("Water Chemistry Test"))
			 {
				 rdname = waterchemistry;
			 }
			 else if(arrstrname.equals("General Comments/Observations"))
			 {
				 rdname = generalcomments;
			 }
			 
			 ch = new CheckBox[rdname.length];
			 rdlin.removeAllViews();
			 
			 rg = new RadioGroup(this);
			 rg.setOrientation(rg.VERTICAL);
			 poollin = new LinearLayout(this);
			 poollin.setOrientation(LinearLayout.VERTICAL);
			 poollin.removeAllViews();
			 
			 LinearLayout spoollin = new LinearLayout(this);
			 spoollin.setOrientation(LinearLayout.HORIZONTAL);
			 System.out.println("filet="+arrstrname);
			 if(arrstrname.equals("Water Treatment")
					 || arrstrname.equals("General Comments/Observations"))
			 { System.out.println("sdsds="+arrstrname);
				 rd = new RadioButton[rdname.length];
				 for(int i=0;i<rdname.length;i++)
				 {
					 rd[i] = new RadioButton(this);
					 rd[i].setText(rdname[i]);
					 rd[i].setTag(rdname[i]);
					 rd[i].setTextColor(Color.parseColor("#000000"));
					 rg.addView(rd[i]);
					 System.out.println("dfds=");
					 rg.setOnCheckedChangeListener(new onchecklistner(i));
					
				 }rdlin.addView(rg);
			 }
			 else if(arrstrname.equals("Water Chemistry Test"))
			 {
				 wateret = new EditText[rdname.length];
				 rdlin.removeAllViews();
				 poollin.removeAllViews();
				
				 for(int i=0;i<rdname.length;i++)
				 {		
					 ch[i] = new CheckBox(this);
					 ch[i].setText(rdname[i]);
					 ch[i].setMaxWidth(200);
					 ch[i].setTextColor(Color.parseColor("#000000"));
					 poollin.addView(ch[i]);
					 
					 wateret[i] = new EditText(this);
					 wateret[i].setMaxWidth(200);
					 wateret[i].setTextColor(Color.parseColor("#000000"));
					 wateret[i].setVisibility(v.GONE);
					 poollin.addView(wateret[i]);
					 				
					 
					 ch[i].setOnCheckedChangeListener(new onpoolchecklistner(i));
				 }
				 rdlin.addView(poollin);
			 }
			 else
			 {
				 for(int i=0;i<rdname.length;i++)
				 {
					 ch[i] = new CheckBox(this);
					 ch[i].setText(rdname[i]);
					 ch[i].setMaxWidth(200);
					 ch[i].setTextColor(Color.parseColor("#000000"));
					 rdlin.addView(ch[i]);
					 
					 ch[i].setOnCheckedChangeListener(new onchecklistner(i));
					
				 }
			 }
			
			
			ettxt = (EditText)findViewById(R.id.othertxt);
			childettxt = (EditText)findViewById(R.id.childothertxt);
			secchildettxt = (EditText)findViewById(R.id.secondchildothertxt);
			thrchildettxt = (EditText)findViewById(R.id.thirdchildothertxt);
			fourchildettxt = (EditText)findViewById(R.id.fourthchildothertxt);
			fivechildettxt = (EditText)findViewById(R.id.fifthchildothertxt);
			
			Button btncancel = (Button) findViewById(R.id.cancel);
			btncancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					back();
					
				}
			});
			
	}
	class onpoolchecklistner implements OnCheckedChangeListener{

		public int id;
		public onpoolchecklistner(int i) {
			// TODO Auto-generated constructor stub
			id=i;
		}

		@Override
		public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
			// TODO Auto-generated method stub
			if(arg1)
			{
				wateret[id].setVisibility(v.VISIBLE);
			}
			else
			{
				wateret[id].setVisibility(v.GONE);
			}
		}
		
	}
	

	class onchecklistner implements OnCheckedChangeListener, android.widget.RadioGroup.OnCheckedChangeListener{

		public int id;
		public onchecklistner(int i) {
			// TODO Auto-generated constructor stub
			id=i;
		}
		int len=0;String[] name = null;
		
		 public void onCheckedChanged(RadioGroup group, int checkedId) {

		 // Here you can get your Radio button checked id
			 String getselected = (String) findViewById(checkedId).getTag();
			if(getselected.equals("Other"))
			{System.out.println("arrstrname="+arrstrname);
				if(apprdval.equals("Skimmer") || apprdval.equals("Pump Types"))
				{
					secchildettxt.setVisibility(v.VISIBLE);
				}
				else if(apprdval.equals("Main Drain"))
				{
					thrchildettxt.setVisibility(v.VISIBLE);
				}
				else if(apprdval.equals("Valves"))
				{
					fourchildettxt.setVisibility(v.VISIBLE);
				}
				else if(apprdval.equals("Automated Fill System"))
				{
					fivechildettxt.setVisibility(v.VISIBLE);
				}
				else if(arrstrname.equals("Water Treatment")
						 || arrstrname.equals("General Comments/Observations"))
					
					
				{
					ettxt.setVisibility(v.VISIBLE);
					firstlin.setVisibility(v.GONE);childettxt.setVisibility(v.GONE);
				}
				else if(apprdval.equals("Wiring Type"))
				{
					secchildettxt.setVisibility(v.VISIBLE);
					
				}
				else if(apprdval.equals("Conduit - Wire Protection"))
				{
					thrchildettxt.setVisibility(v.VISIBLE);
					
				}
				else
				{
				    childettxt.setVisibility(v.VISIBLE);
				}
			}
			else
			{
				if(apprdval.equals("Skimmer") || apprdval.equals("Pump Types"))
				{
					secchildettxt.setText("");
					secchildettxt.setVisibility(v.GONE);
				}
				else if(apprdval.equals("Main Drain"))
				{
					thrchildettxt.setText("");
					thrchildettxt.setVisibility(v.GONE);
				}
				else if(apprdval.equals("Valves"))
				{
					fourchildettxt.setText("");
					fourchildettxt.setVisibility(v.GONE);
				}
				else if(apprdval.equals("Automated Fill System"))
				{
					fivechildettxt.setText("");
					fivechildettxt.setVisibility(v.GONE);
				}
				else if(arrstrname.equals("Pool Cleaning Equipment") || arrstrname.equals("Water Treatment")
						 || arrstrname.equals("General Comments/Observations"))
				{
					if(getselected.equals("Booster Pump System"))
					{
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(getselected);
						 name = boosterpumpsystem;
						 view(name,lin,childettxt);
					}
					else if(getselected.equals("Pump Chlorinator"))
					{
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(getselected);
						 name = pumpchlor;
						 view(name,lin,childettxt);
					}
					else
					{
						 firstlin.setVisibility(v.GONE);childettxt.setVisibility(v.GONE);
					}
					ettxt.setText("");
					ettxt.setVisibility(v.GONE);
				}
				else if(arrstrname.equals("Timers/Clocks/Controls"))
				{
					if(apprdval.equals("Location"))
					{
						childettxt.setText("");
						childettxt.setVisibility(v.GONE);
					}
				}
				else if(arrstrname.equals("Electrical Service"))
				{
					if(apprdval.equals("External Sub Panel"))
					{
						childettxt.setText("");
						childettxt.setVisibility(v.GONE);
					}
					else if(apprdval.equals("Wiring Type"))
					{
						secchildettxt.setText("");
						secchildettxt.setVisibility(v.GONE);
					}
				}
				else
				{
					childettxt.setText("");
				    childettxt.setVisibility(v.GONE);
				}
				
				
			}
		
		 }
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			 apprdval = buttonView.getText().toString().trim();
			if(isChecked)
			{
				 
				 
				 if(apprdval.equals("Other"))
				 {
					 ettxt.setText("");
					 ettxt.setVisibility(v.VISIBLE);
				 }
				 else
				 {
					 if(apprdval.equals("Ladders"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 name = laddertype;
						 view(name,lin,childettxt);
					 }
					 else if(apprdval.equals("Diving Board"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 name = divingboardtype;
						 view(name,seclin,secchildettxt);
					 }
					 else if(apprdval.equals("Slide"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 name = divingboardtype;
						 view(name,thrlin,thrchildettxt);
					 }
					 else if(apprdval.equals("Switch Location"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);
						 name = switchlocation;						 
						 rdview(name,lin,childettxt);
					 }
					 else if(apprdval.equals("Location"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);
						 name = location;						 
						 rdview(name,lin,childettxt);
					 }
					 else if(apprdval.equals("Material"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);
						 name = material;						 
						 rdview(name,lin,childettxt);
					 }
					 else if(apprdval.equals("Skimmer"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 seclin.setVisibility(v.VISIBLE);
						 name = skimmer;						 
						 rdview(name,seclin,secchildettxt);
					 }
					 else if(apprdval.equals("Main Drain"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 thrlin.setVisibility(v.VISIBLE);
						 name = maindrain;						 
						 rdview(name,thrlin,thrchildettxt);
					 }
					 else if(apprdval.equals("Valves"))
					 {
						 fourthlin.setVisibility(v.VISIBLE);fourthtxt.setText(apprdval);
						 fourlin.setVisibility(v.VISIBLE);
						 name = valves;						 
						 rdview(name,fourlin,fourchildettxt);
					 }
					 else if(apprdval.equals("Automated Fill System"))
					 {
						 fifthlin.setVisibility(v.VISIBLE);fifthtxt.setText(apprdval);
						 fivelin.setVisibility(v.VISIBLE);
						 name = automated;						 
						 rdview(name,fivelin,fivechildettxt);
					 }
					 else if(apprdval.equals("No Of Pumps Present"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);
						 name = pumppresent;						 
						 rdview(name,lin,childettxt);
					 }
					 else if(apprdval.equals("Pump Types"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 seclin.setVisibility(v.VISIBLE);
						 name = pumptypes;						 
						 rdview(name,seclin,secchildettxt);
					 }
					 else if(apprdval.equals("Gas Heater"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);
						 name = gasheater;						 
						 rdview(name,lin,childettxt);
					 }
					 else if(apprdval.equals("Solar Heater"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 seclin.setVisibility(v.VISIBLE);
						 name = solarheater;						 
						 rdview(name,seclin,secchildettxt);
					 }
					 else if(apprdval.equals("Thermostat"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 thrlin.setVisibility(v.VISIBLE);
						 name = thermostat;						 
						 rdview(name,thrlin,thrchildettxt);
					 }
					 else if(apprdval.equals("Location"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);
						 name = timerlocation;						 
						 rdview(name,lin,childettxt);
					 }
					 else if(apprdval.equals("Remote Control(s)"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 seclin.setVisibility(v.VISIBLE);
						 name = remotecontrols;						 
						 rdview(name,seclin,secchildettxt);
					 }
					 else if(apprdval.equals("Programmable Controls"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 thrlin.setVisibility(v.VISIBLE);
						 name = progcontrols;						 
						 rdview(name,thrlin,thrchildettxt);
					 }
					 else if(apprdval.equals("External Sub Panel"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);
						 name = extsubpanel;						 
						 rdview(name,lin,childettxt);
					 }
					 else if(apprdval.equals("Wiring Type"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 seclin.setVisibility(v.VISIBLE);
						 name = wiringtype;						 
						 rdview(name,seclin,secchildettxt);
					 }
					 else if(apprdval.equals("Conduit � Wire Protection"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 thrlin.setVisibility(v.VISIBLE);
						 name = conduit;						 
						 rdview(name,thrlin,thrchildettxt);
					 }
				 }
				
			}
			 else  
			 {
				 if(apprdval.equals("Other"))
				 {
				     ettxt.setVisibility(v.GONE);
				 }
				 else
				 {
				     if(apprdval.equals("Ladders")|| apprdval.equals("Switch Location") || apprdval.equals("Location")|| apprdval.equals("Material")
				    		|| apprdval.equals("No Of Pumps Present")|| apprdval.equals("Gas Heater") || (apprdval.equals("Location") && arrstrname.equals("Timers/Clocks/Controls"))
				    		|| apprdval.equals("External Sub Panel"))
				     {
					    firstlin.setVisibility(v.GONE);childettxt.setVisibility(v.GONE);
				     }
				     else if(apprdval.equals("Diving Board")|| apprdval.equals("Skimmer") || apprdval.equals("Pump Types")|| apprdval.equals("Solar Heater")
				    		 || apprdval.equals("Remote Control(s)")||apprdval.equals("Wiring Type"))
				     {
					    secondlin.setVisibility(v.GONE);secchildettxt.setVisibility(v.GONE);
				     }
				     else if(apprdval.equals("Slide") || apprdval.equals("Main Drain")||apprdval.equals("Thermostat") || apprdval.equals("Programmable Controls")
				    		 || apprdval.equals("Conduit � Wire Protection"))
				     {
					    thirdlin.setVisibility(v.GONE);thrchildettxt.setVisibility(v.GONE);
				     }
				     else if(apprdval.equals("Valves"))
				     {
				    	 fourlin.setVisibility(v.GONE);fourchildettxt.setVisibility(v.GONE);
				     }
				     else if(apprdval.equals("Automated Fill System"))
				     {
				    	 fivelin.setVisibility(v.GONE);fifthtxt.setVisibility(v.GONE);
				     }
				 }
			 }
			
		}
		
	}
	
	protected void rdview(String[] name, LinearLayout lin2, EditText childettxt2) {
		// TODO Auto-generated method stub
		RadioGroup rdgswtch = new RadioGroup(PoolShowDialog.this);
		
		 RadioButton[] rds = new RadioButton[name.length];
		 lin2.removeAllViews();
		 for(int i=0;i<name.length;i++)
		 {
			 rds[i] = new RadioButton(PoolShowDialog.this);
			 rds[i].setText(name[i]);
			 rds[i].setTag(name[i]);
			 rds[i].setTextColor(Color.parseColor("#000000"));
			 
			 rdgswtch.addView(rds[i]);
			 rdgswtch.setOnCheckedChangeListener(new onchecklistner(i));
			
		 }
		 lin2.addView(rdgswtch);
	}
    
   
	protected void view(String[] name, LinearLayout lin2, final EditText secchildettxt2) {
		// TODO Auto-generated method stub
		String[] id = name;
		
		 childch = new CheckBox[id.length];
		 lin2.removeAllViews();
		 for(int i=0;i<id.length;i++)
		 {
			 childch[i] = new CheckBox(PoolShowDialog.this);
			 childch[i].setText(id[i]);
			 childch[i].setTextColor(Color.parseColor("#000000"));
			 
			 lin2.addView(childch[i]);
			 
			 childch[i].setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					
					String strchild = buttonView.getText().toString().trim();
					System.out.println("strchild="+strchild);
					if(isChecked)
					{
						if(strchild.equals("Other"))
						{
							secchildettxt2.setVisibility(v.VISIBLE);
						}
					}
					else
					{
						if(strchild.equals("Other"))
						{
							secchildettxt2.setText("");
							secchildettxt2.setVisibility(v.GONE);
						}
					}
				}
			});
		 }
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}

	private void back() {
		// TODO Auto-generated method stub
		Intent i = new Intent(PoolShowDialog.this,CreateSubModule.class);						
		i.putExtra("templatename", gettempname);
		i.putExtra("currentview", getcurretview);
		i.putExtra("modulename", getmodulename);
		i.putExtra("tempoption", gettempoption);
		i.putExtra("id",getid);
		//startActivity(i);
		setResult(101,i);
		finish();
	}
}
