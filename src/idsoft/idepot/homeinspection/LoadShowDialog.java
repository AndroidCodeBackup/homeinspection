package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.ArrayModuleList;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.util.ArrayList;




import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class LoadShowDialog extends Activity{
	String arrstrname="",gettempname="",getph="",getmodulename="",str="",seltdval="",strchk="",strother="",getcurretview="";
	DatabaseFunction db;
	CommonFunction cf;
	ArrayModuleList ar;
	String[] s; 
	CheckBox[] ch;
	RadioButton[] rd;
	int dialog_other=0,getid=0;
	LinearLayout lin;
	EditText etother;
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		
		    Bundle b = this.getIntent().getExtras();
			arrstrname = b.getString("arrstr");
			Bundle b1 = this.getIntent().getExtras();
			gettempname = b1.getString("templatename");
			Bundle b2 = this.getIntent().getExtras();
			getmodulename = b2.getString("modulename");
			Bundle b3 = this.getIntent().getExtras();
			getph = b3.getString("selectedid");
			Bundle b4 = this.getIntent().getExtras();
			getid = b4.getInt("id");
			Bundle b5 = this.getIntent().getExtras();
			
			getcurretview = b5.getString("currentview");
			
			
			System.out.println("getphinshow="+getph+gettempname+"getcurretview"+getcurretview+"getid="+getid);
			 setContentView(R.layout.customizedialog);
			ar = new ArrayModuleList(this,getmodulename);
			cf = new CommonFunction(this);
			db = new DatabaseFunction(this);
			
			db.CreateTable(1);
			db.CreateTable(3);
			db.CreateTable(8);
			db.CreateTable(7);
			db.userid();
			
			Declaration();
			
	}
	private void Declaration() {
		// TODO Auto-generated method stub
        dbvalue();
		
		show_alert();
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	//	back();
	}

	private void dbvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		cf.GetCurrentModuleName(getmodulename);		
		
		for(int j=0;j<cf.arrsub.length;j++)
		{
			System.out.println("arrst"+arrstrname+"sub="+cf.arrsub[j]);
			if(arrstrname.equals(cf.arrsub[j]))
			{
				for(int i=j;i<=j;i++)
				{
					s = ar.arrload[i];		
					
					for(int k=0;k<s.length;k++)
					 {
						 str_arrlst.add(s[k]);
					 }
				}
			}
		}		
		
		try
		{
			 Cursor c1=db.hi_db.rawQuery("select * from " + db.AddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(c1.getCount()>0)
			 {
				 if (c1 != null) {
					 c1.moveToFirst();System.out.println("cn="+c1.getCount());
						do {
						    String defvalue =db.decode(c1.getString(c1.getColumnIndex("fld_other")));
						    str_arrlst.add(defvalue);
							Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_templatename='"+gettempname+"'", null);
	    		            int ccnt = cur.getCount();System.out.println("ccnt="+ccnt);
	    		            if(ccnt==0)
	    		            {System.out.println("insert");
	    		            	db.hi_db.execSQL(" INSERT INTO "
										+ db.SaveAddOther
										+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
										+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+arrstrname+"','"
										+ db.encode(defvalue) + "','"+gettempname+"')");
	    		            	
	    		            }
	    		         //   cur.close();
						
						}while (c1.moveToNext());
					}
			 }
			 else
			 {System.out.println("else");
				 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'",null);
				System.out.println("else"+"select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'");
				 if(c.getCount()>0)
				 {
					 c.moveToFirst();
					 for(int i =0;i<c.getCount();i++,c.moveToNext())
					 {
						 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
					 }//c.close();
				 }
			 }
			 
			/* Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'",null);
			System.out.println("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'");
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =0;i<c.getCount();i++,c.moveToNext())
				 {
					 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
				 }
			 }*/
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
     public void show_alert() {		
	
		lin = (LinearLayout)findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)findViewById(R.id.header);
		hdr.setText(arrstrname);
		
		show();
		
		Button btncancel = (Button) findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		Button btnsave = (Button)findViewById(R.id.ok);
		btnsave.setOnClickListener(new OnClickListener() {
			String seltdval = "";
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(arrstrname.equals("Main Panel Meter Located Inside")||arrstrname.equals("Main Panel Meter Located Outside") 
						|| arrstrname.equals("Fire Separation Present")|| arrstrname.equals("Moisture Stains Present")
						|| arrstrname.equals("Typical Cracks") || arrstrname.equals("Fire Door")
						|| arrstrname.equals("Auto Closure") || arrstrname.equals("Water Meter Flow Detector Stable")
						|| arrstrname.equals("5 Year Wiring Replacement Probability")
						||  arrstrname.equals("Supply Registers Located")
						|| arrstrname.equals("Return Registers Located")|| arrstrname.equals("Safety Glass Satmp at Shower Enclosure")
						|| arrstrname.equals("Spa - Whirlpool Style/Type")|| arrstrname.equals("Water Clarity")|| arrstrname.equals("Pool Shape")
						)
				{
					for (int i = 0; i < str_arrlst.size(); i++) {
						if (rd[i].isChecked()) {
							seltdval = rd[i].getText().toString();
						}
					}
				}
				else
				{
					for (int i = 0; i < str_arrlst.size(); i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
				}
				
				String finalselstrval = "";
				finalselstrval = seltdval;
				
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{
					try
					{
						//Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
						Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"'", null);
						if (cur.getCount() > 0) {
							db.hi_db.execSQL("UPDATE "
									+ db.SaveSubModule
									+ " SET fld_description='"
									+ db.encode(finalselstrval)
									+ "'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'");
						}
						else
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.SaveSubModule
									+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES"
									+ "('"+db.UserId+"','"+getph+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(finalselstrval)+"','"+db.encode(gettempname)+"','0')");
						}
						cf.DisplayToast("Option added successfully");
						back();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		});
		
		Button btnother = (Button)findViewById(R.id.addnew);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=1;
				dialogshow();
				
				
			}
		});
		
		Button btnvc = (Button) findViewById(R.id.setvc);
		btnvc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("loadshowdialog="+getph+gettempname);
				Intent intent = new Intent(LoadShowDialog.this, LoadNewSetVC.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurretview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("submodulename", arrstrname);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("selectedid", getph);			
				intent.putExtras(b6);
				startActivity(intent);
				finish();
				
			}
		});
	}

	protected void dialogshow() {
		// TODO Auto-generated method stub
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.addnewcustom);
		
		etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
		cf.hidekeyboard(etother);
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=0;
				add_dialog.dismiss();
			}
		});
		
		Button btnsave = (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etother.getText().toString().trim().equals("")) {
					cf.DisplayToast("Please enter the other text");
					
				} else {
					if(str_arrlst.contains(etother.getText().toString().trim().toLowerCase()))
					{
						cf.DisplayToast("Already exists!! Please enter the new text");
						etother.setText("");
						etother.requestFocus();
					}
					else
					{
						try
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.SaveAddOther
									+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
									+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+arrstrname+"','"
									+ db.encode(etother.getText().toString()) + "','"+gettempname+"')");
							
							String addedval = db.encode(etother.getText().toString());
							System.out.println("addedval="+addedval);
							str_arrlst.add(addedval);
							dialog_other=0;
							cf.DisplayToast("Added successfully");
							add_dialog.cancel();
							
							lin.removeAllViews();
							dbvalue();
							show();
						
							
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
		
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	private void show() {
		// TODO Auto-generated method stub
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(arrstrname)+"'", null);
			System.out.println("show"+"select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(arrstrname)+"'");
			if (cur.getCount() > 0) {
				cur.moveToFirst();
				for (int i = 0; i < cur.getCount(); i++, cur.moveToNext()) {
					str = db.decode(cur.getString(cur
							.getColumnIndex("fld_description")));
	                
				}
				System.out.println("strafter"+str);
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
			
		if(arrstrname.equals("Main Panel Meter Located Inside")||arrstrname.equals("Main Panel Meter Located Outside") 
				|| arrstrname.equals("Fire Separation Present")|| arrstrname.equals("Moisture Stains Present")
				|| arrstrname.equals("Typical Cracks") || arrstrname.equals("Fire Door")
				|| arrstrname.equals("Auto Closure") || arrstrname.equals("Water Meter Flow Detector Stable")
				|| arrstrname.equals("5 Year Wiring Replacement Probability")
				|| arrstrname.equals("Supply Registers Located")
				|| arrstrname.equals("Return Registers Located")|| arrstrname.equals("Safety Glass Satmp at Shower Enclosure")
				|| arrstrname.equals("Spa - Whirlpool Style/Type")|| arrstrname.equals("Water Clarity")|| arrstrname.equals("Pool Shape")
				)
			{
				rd = new RadioButton[str_arrlst.size()];
				RadioGroup rg = new RadioGroup(this);
				rg.setOrientation(RadioGroup.VERTICAL);
				for (int i = 0; i < str_arrlst.size(); i++) {

					rd[i] = new RadioButton(this);
					rg.addView(rd[i]);
					rd[i].setText(str_arrlst.get(i));
					rd[i].setTextColor(Color.BLACK);
					lin.removeAllViews();
					lin.addView(rg);
					
				}

				if (str != "") {
					cf.setValuetoRadio(rd, str);
				}
			}
			else
			{		
				System.out.println("str_arrlst.size()="+str_arrlst.size());
				ch = new CheckBox[str_arrlst.size()];
				
				for (int i = 0; i < str_arrlst.size(); i++) {
		        		ch[i] = new CheckBox(this);
		        		System.out.println("text="+str_arrlst.get(i));
						ch[i].setText(str_arrlst.get(i));
						ch[i].setChecked(true);//Need to remove this(9/2/2015)
						ch[i].setTextColor(Color.BLACK);
						lin.addView(ch[i]);
						
						ch[i].setOnCheckedChangeListener(new CHListener(i));
				}
		
				if (str != "") {
					System.out.println("strinside="+str);
						cf.setValuetoCheckbox(ch, str);
				}
			}
	}
	class CHListener implements OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
				
				
			}
		}
		
	}
	public void onSaveInstanceState(Bundle outState) {
		 
        outState.putInt("other_index", dialog_other);
        if(dialog_other==1)
        {
          outState.putString("othertext_index", etother.getText().toString());
        }
        outState.putString("checkbox_index", seltdval);	      
       
        super.onSaveInstanceState(outState);
    }
   protected void onRestoreInstanceState(Bundle savedInstanceState) {
	   dialog_other = savedInstanceState.getInt("other_index");
	   if(dialog_other==1)
	   {
		   dialogshow();
		   strother = savedInstanceState.getString("othertext_index");
		   etother.setText(strother);
		 
	   }
	   
	   seltdval = savedInstanceState.getString("checkbox_index");
	   if(!seltdval.equals(""))
	   {
		   cf.setValuetoCheckbox(ch, seltdval);
		 
	   }
	 
	   super.onRestoreInstanceState(savedInstanceState);
    }
	private void back() {
		// TODO Auto-generated method stub
		Intent i = new Intent(LoadShowDialog.this,LoadSubModules.class);
		System.out.println("gettempname="+gettempname+"getmodulename="+getmodulename+"getph="+getph+"getid="+getid);
		i.putExtra("templatename", gettempname);
		i.putExtra("modulename", getmodulename);
		i.putExtra("selectedid", getph);
		i.putExtra("currentview", getcurretview);		
		i.putExtra("id",getid);System.out.println("getid in load="+getid);
		//startActivity(i);
		setResult(101,i);
		finish();	
	}

}
