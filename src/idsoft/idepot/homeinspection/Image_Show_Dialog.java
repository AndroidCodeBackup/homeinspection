package idsoft.idepot.homeinspection;

import java.io.BufferedInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Loads images from SD card. 
 * 
 * @author Mihai Fonoage
 *
 */
public class Image_Show_Dialog extends Activity{

	String cb="";
	String[] cbarr,arr;
	 Uri uri = null;         
	ArrayAdapter getarr;
    /**
     * Grid view holding the images.
     */
    private ListView sdcardImages;
    /**
     * Image adapter for the grid view.
     */
    private ImageAdapter imageAdapter;
    String str="",asd="";
    String[] strarr,newarr;
    /**
     * Display used for getting the width of the screen. 
     */
    private Display display;

    /**
     * Creates the content view, sets up the grid, the adapter, and the click listener.
     * 
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        // Request progress bar
       // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);
       
        //display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

       
        
  
		
		
		 setupViews();
	     setProgressBarIndeterminateVisibility(true); 
	        loadImages();//display();
       /* Button btnsave = (Button)findViewById(R.id.save);
        btnsave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				     hi_db.execSQL("DELETE FROM " + SaveIamge);
				
					//System.out.println("newarrstr="+newarr[i]);
					try
					{	System.out.println("strarr="+strarr.length);
						for(int i=0;i<strarr.length;i++)
						{
						   //System.out.println("deleted");
							if(cbarr[i].equals("true"))
							{
								
								System.out.println("strarr[i]="+strarr[i]);
							 	hi_db.execSQL(" INSERT INTO "+SaveIamge+" (fld_iameg,fld_selected) VALUES" +
										"('"+strarr[i]+"','1')");
							}
						}
			          
			           	System.out.println("inserted");
			      
					}catch (Exception e) {
						// TODO: handle exception
					}
			
			}
		});*/
    }

   /* private void display() {
		// TODO Auto-generated method stub
    	 try
         {String incval="",cd="";String[] arrc;
                Cursor cur = hi_db.rawQuery("select * from " + SaveIamge , null);
	            int cnt = cur.getCount();
	            if(cnt>0)
	            {
	            	cur.moveToFirst();
	            	
	            	if (cur != null) {
						do {
							 incval = cur.getString(cur.getColumnIndex("fld_iameg"));
							 String suboption =cur.getString(cur.getColumnIndex("fld_selected"));
								
								cd += suboption+"~";
								arrc = cd.split("~");
								
							    if(suboption.equals("1"))
					            {
							    	System.out.println("equals");
									cb += "true"+"~";
									
						        }
					            else
					            {
					            	System.out.println("notequals");
					            	cb += "false"+"~";
									
							    }
								cbarr = cb.split("~");
							// getarr.add(arr);
						}
	            	 while (cur.moveToNext());
	            	}
	            }
         }catch (Exception e) {
			// TODO: handle exception
		}
	}*/

	/**
     * Free up bitmap related resources.
     */
    protected void onDestroy() {
        super.onDestroy();
        final ListView grid = sdcardImages;
        final int count = grid.getChildCount();
        ImageView v = null;
        for (int i = 0; i < count; i++) {
            v = (ImageView) grid.getChildAt(i);
            ((BitmapDrawable) v.getDrawable()).setCallback(null);
        }
    }
    /**
     * Setup the grid view.
     */
    private void setupViews() {
        sdcardImages = (ListView) findViewById(R.id.sdcard);
        //sdcardImages.setNumColumns(display.getWidth()/95);
        sdcardImages.setClipToPadding(false);
       // sdcardImages.setOnItemClickListener(Image_Show_Dialog.this);
        imageAdapter = new ImageAdapter(getApplicationContext()); 
        sdcardImages.setAdapter(imageAdapter);
    }
    /**
     * Load images.
     */
    private void loadImages() {
        //Object data = getLastNonConfigurationInstance();
        /*if (data == null) {*/
    	  new LoadImagesFromSDCard().execute();
        /*} else {
            
            if (photos.length == 0) {
            	System.out.println("photo");
                new LoadImagesFromSDCard().execute();
            }
           
        }*/
          
       // System.out.println("lenfght="+arr.length);
       
    }
    /**
     * Add image(s) to the grid view adapter.
     * 
     * @param value Array of LoadedImages references
     */
    private void addImage(LoadedImage... value) {
    	System.out.println("loaded+"+value);
        for (LoadedImage image : value) {System.out.println("fpr");
            imageAdapter.addPhoto(image);
            imageAdapter.notifyDataSetChanged();
        }
    }
    
    /**
     * Save bitmap images into a list and return that list. 
     * 
     * @see android.app.Activity#onRetainNonConfigurationInstance()
     */
    @Override
    public Object onRetainNonConfigurationInstance() {
        final ListView grid = sdcardImages;
        final int count = grid.getChildCount();
        final LoadedImage[] list = new LoadedImage[count];

        for (int i = 0; i < count; i++) {
            final ImageView v = (ImageView) grid.getChildAt(i);
            list[i] = new LoadedImage(((BitmapDrawable) v.getDrawable()).getBitmap());
        }

        return list;
    }
    /**
     * Async task for loading the images from the SD card. 
     * 
     * @author Mihai Fonoage
     *
     */
    class LoadImagesFromSDCard extends AsyncTask<Object, LoadedImage, Object> {
        
        /**
         * Load images from SD Card in the background, and display each image on the screen. 
         *  
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @SuppressWarnings("deprecation")
		@Override
        protected Object doInBackground(Object... params) {
            //setProgressBarIndeterminateVisibility(true); 
            Bitmap bitmap = null;
            Bitmap newBitmap = null;
            String[] projection = {MediaStore.Images.Thumbnails._ID};  
            final Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                projection,
                                null,
                                null,
                                null);
                        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
                        int size = cursor.getCount();
                        System.out.println("sixze"+size);
                      
                        if (size == 0) {
                            //No Images available, post some message to the user
                        }
                        int imageID = 0;
                        for (int i = 0; i < size; i++) {
                            cursor.moveToPosition(i);
                            imageID = cursor.getInt(columnIndex);
                            uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + imageID) ;
                            System.out.println("uri="+uri);
                            str += uri.toString()+"~";
                            strarr = str.split("~");
                            cb += "false"+"~";
                            cbarr = cb.split("~");
                            
                                                       
                            try {
                            System.out.println("try");
                                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));
                            	 // bitmap = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver() , uri);
                                System.out.println("bitmap"+bitmap);
                                if (bitmap != null) {
                                    newBitmap = Bitmap.createScaledBitmap(bitmap, 70, 70, true);
                                    bitmap.recycle();
                                    if (newBitmap != null) {
                                        publishProgress(new LoadedImage(newBitmap));
                                    }
                                }
                            } catch (IOException e) {
                                //Error fetching image, try to recover
                            	System.out.println("erroe="+e.getMessage());
                            }
                        }
                    
                     
                        cursor.close();
                        return null;
         
            // Set up an array of the Thumbnail Image ID column we want
           // String[] projection = {MediaStore.Images.Thumbnails._ID};
         
           /* final String[] columns = {MediaStore.Images.Media.DATA,MediaStore.Images.Media._ID };
            final String orderBy = MediaStore.Images.Media._ID;
            @SuppressWarnings("deprecation")
            Cursor cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
            int columnIndex = cursor.getColumnIndex(MediaStore.Images.Media._ID);*/
          
           // int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA);
          /*  int imageID = 0;
            String[] projection = {MediaStore.Images.Thumbnails._ID};


            final Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    projection,
                    null,
                    null,
                    null);
            System.out.println("cursor.getCount()="+cursor.getCount());
            ArrayList<String> result = new ArrayList<String>(cursor.getCount());
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
            System.out.println("dataColumn="+dataColumn);
            //Log.i("cursor.getCount()) :", cursor.getCount() + "");

            if (cursor.moveToFirst()) {
               
                do {
                    final String data = cursor.getString(dataColumn);
                  //  imageID = cursor.getInt(dataColumn);
                    uri = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "" + cursor.getInt(dataColumn)) ;
                    System.out.println("uri="+uri);
                   // Log.i ("data :",data );
                    str += uri.toString()+"~";
                    strarr = str.split("~");
                    cb += "false"+"~";
                    cbarr = cb.split("~");
                    try {
                    	System.out.println("trey");
                    
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));
                        System.out.println("bitmap"+bitmap);
                        if (bitmap != null) {
                            newBitmap = Bitmap.createScaledBitmap(bitmap, 70, 70, true);
                            bitmap.recycle();
                            if (newBitmap != null) {
                            	System.out.println("newbutma");
                                publishProgress(new LoadedImage(newBitmap));
                            }
                        }
                    } catch (IOException e) {
                    	System.out.println("error="+e.getMessage());
                        //Error fetching image, try to recover
                    }
                    result.add(data);
                } while (cursor.moveToNext());
            }
            cursor.close();

            int size = cursor.getCount();
            System.out.println("sixze"+size);
            // If size is 0, there are no images on the SD Card.
            if (size == 0) {
                //No Images available, post some message to the user
            }
       
          
            cursor.close();
            return result;*/
        }
        /**
         * Add a new LoadedImage in the images grid.
         *
         * @param value The image.
         */
        @Override
        public void onProgressUpdate(LoadedImage... value) {
        	System.out.println("value+"+value);
            addImage(value);
        }
        /**
         * Set the visibility of the progress bar to false.
         * 
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(Object result) {
            setProgressBarIndeterminateVisibility(false);
        }
    }

    /**
     * Adapter for our image files. 
     * 
     * @author Mihai Fonoage
     *
     */
    class ImageAdapter extends BaseAdapter {

        private Context mContext; 
    	private LayoutInflater mInflater;
		ViewHolder holder;
        private ArrayList<LoadedImage> photos = new ArrayList<LoadedImage>();

        public ImageAdapter(Context context) { 
        	System.out.println("comesinside");
        	mInflater = LayoutInflater.from(context);
            mContext = context; 
        } 

        public void addPhoto(LoadedImage photo) { 
        	System.out.println("photo"+photo);
            photos.add(photo); 
        } 

        public int getCount() { 
        	System.out.println("sixepj"+photos.size());
            return photos.size(); 
        } 

        public Object getItem(int position) { 
            return photos.get(position); 
        } 

        public long getItemId(int position) { 
            return position; 
        } 

        public View getView(int position, View convertView, ViewGroup parent) { 
            final ImageView imageView; 
            
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.activity_photos, null);						
			holder.moduletext = (ImageView) convertView.findViewById(R.id.vc_submodule);
			holder.cbsubmodule = (CheckBox)convertView.findViewById(R.id.cb_submodule);
			
			System.out.println("cbarr[position]="+cbarr[position]);
			
			 convertView.setTag(holder);	
            /*if (convertView == null) { 
                imageView = new ImageView(mContext); 
            } else { 
                imageView = (ImageView) convertView; 
            } */
            holder.moduletext.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.moduletext.setPadding(8, 8, 8, 8);
            holder.moduletext.setImageBitmap(photos.get(position).getBitmap());
           // System.out.println("pd="+photos.get(position).getBitmap());
            
            if (strarr[position].contains("null")) {
            	strarr[position] = strarr[position].replace("null", "");
				
			}
			else
			{
				
			}
            holder.cbsubmodule.setOnCheckedChangeListener(new oncheckedlistener(position,holder.moduletext,holder.cbsubmodule));
            if(cbarr[position].equals("true"))
			{
				holder.cbsubmodule.setChecked(true);
			}
			else
			{
				holder.cbsubmodule.setChecked(false);
			}
            return convertView; 
        } 
        class ViewHolder {
			TextView spinsubmodule,vcsubmodule;
			ImageView moduleimg,moduletext;
			CheckBox cbsubmodule;
		   
		}
    }
    class oncheckedlistener implements OnCheckedChangeListener {
		public int id =0;
		public TextView spmod,vcmod;
		public CheckBox cbmod;
		public ImageView moduleimg;
	

		public oncheckedlistener(int position, ImageView moduletextimg,
				CheckBox cbsubmodule) {
			// TODO Auto-generated constructor stub
			id=position;
			moduleimg=moduletextimg;
			cbmod=cbsubmodule;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
							
			if(isChecked)
			{
			//	System.out.println("checkd= "+strarr[id]);
				
				
				cbarr[id]="true"; 
			
		
			
			}
			else
			{
				cbarr[id]="false";
			
			}
			
		}

	
 }
    /**
     * A LoadedImage contains the Bitmap loaded for the image.
     */
    private static class LoadedImage {
        Bitmap mBitmap;

        LoadedImage(Bitmap bitmap) {
            mBitmap = bitmap;
        }

        public Bitmap getBitmap() {
            return mBitmap;
        }
    }
	
 

	

}