package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.io.ByteArrayOutputStream;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;



import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class CoverPhoto extends Activity{
	String getinspectorid="",getsrid="",gettempname="",getcurrentview="",getmodulename="",getinspectionpoint="",getcaption="Front Elevation",getsubmodulename="",gettempoption="",imagepath="",imagecaption="",captionval="",
			selectedImagePath="",imgPath="";
	ImageView closeimg,galleryimg,cameraimg;
	DatabaseFunction db;
	CommonFunction cf;
	Bitmap bitmap =null;
	String[] photoarr,imgpath,captionarr,imgcaption,imgcaptionval,value,captionvalarr,imgcaptionvar;
	int[] idarr;
	TextView txtnoimage;
	ListView photolist;
	EditText edtcaption;
	public Uri CapturedImageURI;
	View v;
	int maximg=1,t=0,cnt=0,idval=0;
	final private int CAPTURE_IMAGE = 2;
	
	  @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);        
	     
	        setContentView(R.layout.image_selection_imagepicker);
	        
	        Bundle b = this.getIntent().getExtras();
			getinspectorid = b.getString("inspectorid");
			Bundle b1 = this.getIntent().getExtras();
			getsrid = b1.getString("srid");
			
			db = new DatabaseFunction(this);
			cf = new CommonFunction(this);
			db.CreateTable(34);
			
			((TextView)findViewById(R.id.text)).setVisibility(v.VISIBLE);
			        
	        closeimg = (ImageView)findViewById(R.id.close);
	        closeimg.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
	        
	        txtnoimage = (TextView)findViewById(R.id.option_notext);
	        photolist = (ListView)findViewById(R.id.option_list);
	        
	        galleryimg = (ImageView)findViewById(R.id.gallery);
	        galleryimg.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					 // TODO Auto-generated method stub
					    Cursor cur =db.hi_db.rawQuery("select * from " + db.CoverPhoto + " WHERE fld_srid='"+getsrid+"' and fld_inspectorid='"+getinspectorid+"'", null);
			            int cnt = cur.getCount();
			            if(cnt<maximg)
			            {
			            	   	cur.moveToFirst();
			            	   	String[] selected_paht = new String[cur.getCount()];
			     				
			     				for(int i=0;i<cur.getCount();i++)
			     				{
			     					selected_paht[i]=db.decode(cur.getString(cur.getColumnIndex("fld_image"))); 
			     					
			     					cur.moveToNext();
			     				}
					    		
							    Intent i = new Intent(CoverPhoto.this,Select_phots.class);						
								i.putExtra("Selectedvalue", selected_paht);
								i.putExtra("Total_Maximumcount", 1);
								i.putExtra("Maximumcount", cnt);
								startActivityForResult(i, 121);
			            }
			            else
			            {
			            	cf.DisplayToast("You have reached the maximum count to upload");
			            }
				}
			});
	        
	        cameraimg = (ImageView)findViewById(R.id.camera);
	        cameraimg.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Cursor cur =db.hi_db.rawQuery("select * from " + db.CoverPhoto + " WHERE fld_srid='"+getsrid+"' and fld_inspectorid='"+getinspectorid+"'", null);
		            int cnt = cur.getCount();
		            if(cnt<maximg)
		            {
		            	 Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		            	 intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
		                 startActivityForResult(intent, CAPTURE_IMAGE);
		            }
		            else
		            {
		            	cf.DisplayToast("You have reached the maximum count to upload");
		            }
					
				}
			});
	        
	        display();
	  }
	  protected Uri  setImageUri() {
			// TODO Auto-generated method stub
			  File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
	          Uri imgUri = Uri.fromFile(file);
	          this.imgPath = file.getAbsolutePath();
	          
			  System.out.println("imgPath="+imgPath);
			return imgUri;
		}
		public String getImagePath() {
	        return imgPath;
	    }
		 @Override
		    protected void onSaveInstanceState(Bundle outState) {
			   
		        outState.putString("photo_index", imgPath);
		        super.onSaveInstanceState(outState);
		    }
		 @Override
		    protected void onRestoreInstanceState(Bundle savedInstanceState) {
			      imgPath = savedInstanceState.getString("photo_index");
			    
			   
			      System.out.println("imgPath="+imgPath);
		        super.onRestoreInstanceState(savedInstanceState);
		    }
	  public void clicker(View v)
	  {
		  switch(v.getId())
		  {
			  case R.id.option_cancel:
				  back();
				  break;
			  case R.id.option_ok:
                  try
                  {
                	    Cursor cur =db.hi_db.rawQuery("select * from " + db.CoverPhoto + " WHERE fld_srid='"+getsrid+"' and fld_inspectorid='"+getinspectorid+"'", null);
	  		            int cnt = cur.getCount();
	  		            if(cnt>0)
	  		            {
	  		            	for(int i=0;i<captionarr.length;i++)
	  					   {
	  						  /*if(!imgcaptionval[i].equals("textfill"))
	  						  {
	  							  t=1;
	  							  cf.DisplayToast("Please enter the caption");
	  							  return;
	  						  }
	  						  else
	  						  {*/
	  							  t=0;
	  						  //}
	  					  }
	  		            	 if(t==0)
	  						  {
	  		            		  for(int i=0;i<captionarr.length;i++)
	  							  {
	  								  db.hi_db.execSQL("UPDATE "+db.CoverPhoto+ " SET fld_caption='"+db.encode(captionarr[i])+"' WHERE fld_inspectorid='"+getinspectorid+"' and fld_image='"+db.encode(photoarr[i])+"' and fld_srid='"+getsrid+"'");
	  							  }
	  							  cf.DisplayToast("Saved successfully");
	  							  back();
	  						  }
	  		            }
	  		            else
	  		            {
	  		            	cf.DisplayToast("Please upload at least 1 image");
	  		            }
                  }
  		            catch (Exception e) {
						// TODO: handle exception
					}
				  
				 
				  display();
				
				  break;
		  }
	  }
	 
	  @Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			// TODO Auto-generated method stub
			super.onActivityResult(requestCode, resultCode, data);
			try
			{
			if(resultCode == RESULT_OK)
			{
				 
				if(requestCode == 121)
				{
				
					 int getlast=0;
					 value=	data.getExtras().getStringArray("Selected_array");
				 System.out.println("Lengt="+value.length);
					  for(int i=0;i<value.length;i++)
					  {
						  Cursor cur =db.hi_db.rawQuery("select COUNT(p_Id) as _id from " + db.CoverPhoto + " WHERE fld_srid='"+getsrid+"' and fld_inspectorid='"+getinspectorid+"'", null);
						  System.out.println("select COUNT(p_Id) as _id from " + db.CoverPhoto + " WHERE fld_srid='"+getsrid+"' and fld_inspectorid='"+getinspectorid+"'"+cur.getCount());
						  if(cur.getCount()>0)
						  {
							  cur.moveToFirst();
					          getlast = cur.getInt(cur.getColumnIndex("_id"));System.out.println("DD"+getlast);
					          getlast = getlast+1;
						  }
						 
						  
						  System.out.println("INSERT INTO "+db.CoverPhoto+" (fld_image,fld_caption,fld_inspectorid,fld_srid) VALUES" +
									"('"+db.encode(value[i])+"','"+db.encode(getcaption)+"','"+getinspectorid+"','"+getsrid+"')");
						 db.hi_db.execSQL("INSERT INTO "+db.CoverPhoto+" (fld_image,fld_caption,fld_inspectorid,fld_srid) VALUES" +
									"('"+db.encode(value[i])+"','"+db.encode(getcaption)+"','"+getinspectorid+"','"+getsrid+"')");
					  }
					  display();
				
				}
				else if(requestCode == CAPTURE_IMAGE)
				{
					
					 selectedImagePath = getImagePath();
					 System.out.println("imgPath="+imgPath);
					 System.out.println("selectedImagePath= c"+selectedImagePath);
					 File f = new File(selectedImagePath);
						double length = f.length();
						double kb = length / 1024;
						double mb = kb / 1024;
						
						if (mb >= 2) {
							cf.DisplayToast("File size exceeds!! Too large to attach");
						} else {
						
							try {
								Bitmap bitmapdb = decodeFile(selectedImagePath);
								if(bitmapdb==null)
								{
									cf.DisplayToast("File corrupted!! Cant able to attach");
								}
								else
								{
									selectedImagePath = selectedImagePath;
									 int getlast=0;
									 Cursor cur =db.hi_db.rawQuery("select COUNT(p_Id) as _id from " + db.CoverPhoto + " WHERE fld_srid='"+getsrid+"' and fld_inspectorid='"+getinspectorid+"'", null);
									  if(cur.getCount()>0)
									  {
										  cur.moveToFirst();
								          getlast = cur.getInt(cur.getColumnIndex("_id"));
								          getlast = getlast+1;
									  }
									  db.hi_db.execSQL("INSERT INTO "+db.CoverPhoto+" (fld_image,fld_caption,fld_inspectorid,fld_srid) VALUES" +
												"('"+db.encode(selectedImagePath)+"','"+db.encode(getcaption)+"','"+getinspectorid+"','"+getsrid+"')");
									  
									  
								  
								   display();
								
								}
							} catch (Exception e) {
								System.out.println("my exception " + e);
							}
						}
	

				
				}
				else if(requestCode == 101)
				{
					  String value=	data.getExtras().getString("Path"); 
					  String  id=	data.getExtras().getString("id"); 
					  boolean dele=data.getExtras().getBoolean("Delete_data");
					if(dele)
					{
						db.hi_db.execSQL("DELETE FROM "+db.CoverPhoto+" WHERE fld_inspectorid='"+getinspectorid+"' and fld_srid='"+getsrid+"'");
					}
					display();

				}
				else if(requestCode == 122)
				{
					 String value=	data.getExtras().getString("Path"); 
					 int  id=	data.getExtras().getInt("id"); 
					 boolean dele=data.getExtras().getBoolean("Delete_data");
					if(!dele)
					  {
						db.hi_db.execSQL(" UPDATE  "
	 							+ db.CoverPhoto
	 							+ " SET fld_image='"+db.encode(value)+"' "
	 							+ "  WHERE p_Id='"+id+"'");
							cf.DisplayToast("Image saved successfully");					
							
					  }
					  else
					  {
						 
							
							if(dele)
							{
								db.hi_db.execSQL("DELETE FROM "+db.CoverPhoto+" WHERE p_Id='"+id+"'");
							//	cf.DisplayToast("Image deleted successfully");	
							}
					  }
					display();

				}
			}}catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	  private static Bitmap decodeFile(String file) {
		    try {
		    	
		    	File f=new File(file);	    	
		        // Decode image size
		        BitmapFactory.Options o = new BitmapFactory.Options();
		        o.inJustDecodeBounds = true;
		        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

		        // The new size we want to scale to
		        final int REQUIRED_SIZE = 150;

		        // Find the correct scale value. It should be the power of 2.
		        int scale = 1;
		        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
		                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
		            scale *= 2;

		        // Decode with inSampleSize
		        BitmapFactory.Options o2 = new BitmapFactory.Options();
		        o2.inSampleSize = scale;
		        o.inJustDecodeBounds = false;
		        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		    } catch (FileNotFoundException e) {
		    }

		    return null;
		}
		private void display() {
		// TODO Auto-generated method stub
			 try
	         {
				    imagepath="";imagecaption="";captionval="";
	                Cursor cur =db.hi_db.rawQuery("select * from " + db.CoverPhoto + " WHERE fld_srid='"+getsrid+"' and fld_inspectorid='"+getinspectorid+"'", null);
		            cnt = cur.getCount();
		            if(cnt==0)
		            {
		            	txtnoimage.setVisibility(v.VISIBLE);
		            	photolist.setVisibility(v.GONE);
		            }
		            else
		            {
		            	
		            	photolist.setVisibility(v.VISIBLE);
		            	txtnoimage.setVisibility(v.GONE);
		            	cur.moveToFirst();
		            	photoarr=new String[cnt];
		            	captionarr=new String[cnt];
		            	captionvalarr=new String[cnt];
		            	idarr=new int[cnt];
		            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
		            	{
		            		photoarr[i] = db.decode(cur.getString(cur.getColumnIndex("fld_image")));
		            		captionarr[i] = db.decode(cur.getString(cur.getColumnIndex("fld_caption")));
		            		idarr[i] = cur.getInt(cur.getColumnIndex("p_Id"));
		            		captionvalarr[i] = "textfill";
		            	}
		            	photolist.setAdapter(null);
						 photolist.setAdapter(new EfficientAdapter(photoarr,captionarr,captionvalarr,idarr,this));
		            }
	         }catch (Exception e) {
				// TODO: handle exception
			}
	}
		class EfficientAdapter extends BaseAdapter {
			private static final int IMAGE_MAX_SIZE = 0;
			private LayoutInflater mInflater;
			ViewHolder holder;
			View v2;
			
			public EfficientAdapter(String[] cbval, String[] captionarr, String[] captionvalarr,int[] idarr1, Context context) {
				mInflater = LayoutInflater.from(context);
				imgpath=cbval;			
				imgcaption=captionarr;
				imgcaptionval=captionvalarr;
				idarr=idarr1;

			}

			public int getCount() {
				int arrlen = 0;				
				 arrlen = photoarr.length;				
				return arrlen;
			}

			public Object getItem(int position) {
				return position;
			}

			public long getItemId(int position) {
				return position;
			}

			public View getView(int position, View convertView, ViewGroup parent) {
				
						if(cnt!=0)
						{
						    holder = new ViewHolder();
							convertView = mInflater.inflate(R.layout.activity_photos, null);						
							holder.vcphoto = (ImageView) convertView.findViewById(R.id.image);
							holder.vccaption = (EditText)convertView.findViewById(R.id.caption);							
							
							 convertView.setTag(holder);				 
							
							if (photoarr[position].contains("null") || captionarr[position].contains("null")) {
								photoarr[position] = photoarr[position].replace("null", "");
								captionarr[position] = captionarr[position].replace("null", "");
							}
							
								bitmap = ShrinkBitmap(photoarr[position],150,150);
								if(bitmap!=null)
								{
									holder.vcphoto.setImageBitmap(bitmap); 
								}
								else
								{
									holder.vcphoto.setBackgroundDrawable(getResources().getDrawable(R.drawable.photonotavail));

								}
								
								holder.vccaption.setText(captionarr[position]);
							
							    holder.vccaption.addTextChangedListener(new ontextchangelistner(position,holder.vccaption));
							    holder.vcphoto.setOnClickListener(new imagelistner(position,holder.vcphoto,idarr[position]));
						}
				return convertView;
			}

			 class ViewHolder {
			    ImageView vcphoto;
				EditText vccaption;
			   
			}

		}
		class imagelistner implements OnClickListener
		{

			int id=0,autoid=0;
			ImageView myphoto;
			public imagelistner(int position, ImageView vcphoto,int autoincid) {
				// TODO Auto-generated constructor stub
				id=position;
				myphoto=vcphoto;
				autoid=autoincid;
			}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new  Intent(CoverPhoto.this,Editphotos.class);
				in.putExtra("Path",photoarr[id]);
				in.putExtra("id", autoid);
				in.putExtra("delete","true");
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				in.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				in.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				in.putExtras(b4);
				Bundle b7 = new Bundle();
				b7.putString("selectedid", getsrid);			
				in.putExtras(b7);
				Bundle b6 = new Bundle();
				b6.putString("submodulename", getsubmodulename);			
				in.putExtras(b6);
				Bundle b8 = new Bundle();
				b8.putString("tempoption", gettempoption);			
				in.putExtras(b8);
				Bundle b = new Bundle();
				b.putString("selectedvc", getcaption);			
				in.putExtras(b);
				startActivityForResult(in, 122);
				//startActivityForResult(in, 101);

			}
			
		}
		class ontextchangelistner implements TextWatcher
		{
			
			int id;

			public ontextchangelistner(int position, EditText vccaption) {
				// TODO Auto-generated constructor stub
				id=position;
				edtcaption = vccaption;
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				captionarr[id]=s.toString();
				if(s.toString().equals(""))
				{
					imgcaptionval[id]="textnotfill";
				}
				else
				{
					 imgcaptionval[id]="textfill";
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				    if (s.toString().startsWith(" "))
			        {
				    	captionarr[id]="";
					    //edtcaption.setText("");
					    imgcaptionval[id]="textnotfill";
			        }
				    else
				    {
				        imgcaptionval[id]="textfill";
				    }
			}
			
		}
		private void back() {
			// TODO Auto-generated method stub
			Intent i = new Intent(CoverPhoto.this,StartInspection.class);
			i.putExtra("inspectorid", getinspectorid);
			startActivity(i);
			finish();
			
			
		}
		public Bitmap ShrinkBitmap(String string, int i, int j) {
			// TODO Auto-generated method stub
			  
				try {
					BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
					bmpFactoryOptions.inJustDecodeBounds = true;
				    bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);

					int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
							/ (float) j);
					int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
							/ (float) i);
	               
					if (heightRatio > 1 || widthRatio > 1) {
						if (heightRatio > widthRatio) {
							bmpFactoryOptions.inSampleSize = heightRatio;
						} else {
							bmpFactoryOptions.inSampleSize = widthRatio;
						}
					}

					bmpFactoryOptions.inJustDecodeBounds = false;
					bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);
					//System.out.println("bitmap="+bitmap);
					return bitmap;
				} catch (Exception e) {
				System.out.println("catch="+e.getMessage());
					return bitmap;
				}

		}
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			// super.onBackPressed();
			back();
		}
}
