package idsoft.idepot.homeinspection;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.idepot.homeinspection.ForgotPassword.Start_xporting;
import idsoft.idepot.homeinspection.LoadSubModules.EfficientAdapter;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;




import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CreateSubModule extends Activity{
	String gettempname="",getcurrentview="",getmodulename="",gettempoption="",str_module="",str_cbval="",error_msg="",title="Submit Template";
	TextView tv_seltemplate,tv_modulename;
	Button btnback,btnhome, btnsubmit;
	int pos,newid=0,dialogid=0;
	Boolean chkbool=false;
	ListView modulelist;
	String[] arrstrmodule,cbval,value,arrdescription;
	View v;
	CommonFunction cf;
	DatabaseFunction db;
	Webservice_config wb;
	EfficientAdapter effadap;
	Progress_staticvalues p_sv;
	String[] crawlspace={"Crawlspace Area - Characteristics","Crawlspace - Access","Crawlspace - Main Beam","Crawlspace - Floors","Crawlspace - Exposed Foundation Wall"},
			basement={"Basement - Size","Basement - Access","Basement - Stairs","Basement - Floors","Basement - Exposed Columns and Beams","Basement - Walls/Ceilings",
			"Basement - Exposed Floor Systems","Basement - Drain Tile To","Basement - Heat/Cool(HVAC)","Basement - Finishes"},electricmeter={"Main Panel Meter Located Inside",
			 "Main Panel Meter Located Outside","Main Panel Meter"},extsiding={"Masonry/Plaster","Siding/Paneling","Wall Shingles","Accessibility"},
			skylights={"# of Skylights","Skylight Type"};
	private int did;
	private int handler_msg=2;
	SoapObject responsepools = null;
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(4);
		db.CreateTable(6);
		db.CreateTable(27);
		db.userid();
				
		setContentView(R.layout.activity_createsubmodule);
		 ((TextView)findViewById(R.id.note)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		tv_seltemplate = (TextView)findViewById(R.id.seltemplate);
		tv_seltemplate.setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
		tv_modulename = (TextView)findViewById(R.id.modulename);
		tv_modulename.setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		
		((Spinner)findViewById(R.id.spinloadtemplate)).setVisibility(View.GONE);
		((Button)findViewById(R.id.btn_takephoto)).setVisibility(View.GONE);
		
		cf.GetCurrentModuleName(getmodulename);
	
		DefaultValue_Insert();
		
		btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(CreateSubModule.this,HomeScreen.class));
				finish();
				 
			}
		});
		
		btnsubmit = (Button)findViewById(R.id.btn_submit);
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				  if(cf.isInternetOn())
				  {
					   new Start_xporting().execute("");
				  }
				  else
				  {
					  cf.DisplayToast("Please enable your internet connection.");
					  
				  } 
			}
		});
		modulelist = (ListView)findViewById(R.id.modulelist);
		tv_modulename.setVisibility(v.VISIBLE);
		Display();
		
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="Template Submission";
			    p_sv.progress_Msg="Please wait..Your template has been submitting.";
		       
				startActivityForResult(new Intent(CreateSubModule.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		      Submit_Template();
			 		      if(getmodulename.equals("Swimming Pools"))
			 		      {
			 		    	  SwimmingPools_Sub();
			 		      }
			 		      Submit_VC();
			 			/* try
                     	 {
                     		Cursor cur1 = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	               	        if(cur1.getCount()>0)
	               	        {
	               	        	submit_visiblecondition();
	               	        }
	               	        else
	               	        {
	               	        	handler_msg = 0;
	               	        }
               	        
                     	 }catch (Exception e) {
       					// TODO: handle exception
       				     }*/
			 		
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   
		 @Override
			protected void onProgressUpdate(String... progress) {
		
				 
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
               	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	
    private void Submit_Template() throws IOException, XmlPullParserException {
		
		db.userid();
		 
	   
 		
 		for(int i=0;i<cf.arrsub.length;i++)
		{
 			
		  try
 		  {
 			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'", null);
	        if(cur.getCount()>0)
	        {
	        	cur.moveToFirst();	        	
	        	
	        	/*for(int i=0;i<cur.getCount();i++,cur.moveToNext())
	        	 * 
	        	{*/
	        	    SoapObject request = new SoapObject(wb.NAMESPACE,"CreateTemplateDataPoint");
	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope.dotNet = true;
	      		    
	        		request.addProperty("cd_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
	         		request.addProperty("fld_inspectorid",db.UserId);
	         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
	         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
	         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
	         		request.addProperty("fld_NA",cur.getInt(cur.getColumnIndex("fld_NA")));
	         		String str = Html.fromHtml(db.decode(cur.getString(cur.getColumnIndex("fld_description")))).toString();
	         		if(str.endsWith(","))
	         		{
	         			str = str.substring(0,str.length()-1);
	         		}
	         		request.addProperty("fld_description",str);
	         		
	         		envelope.setOutputSoapObject(request);
	         		System.out.println("request="+request);
	         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
	         		SoapObject response = null;
	                try
	                {
	                      androidHttpTransport.call(wb.NAMESPACE+"CreateTemplateDataPoint", envelope);
	                      response = (SoapObject)envelope.getResponse();
	                      System.out.println("CreateTemplateDataPointresult "+response);
	                      
	                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	 
	                    		  try
	 	          				 {
	 	          					db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_sendtoserver='"+response.getProperty("cd_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	                    	  
	                    	 
	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                catch(Exception e)
	                {
	                     e.printStackTrace();
	                }
	        	}
	  	}
 		catch (Exception e) {
			// TODO: handle exception
		}
 		
		}      
       
   }

    private void Submit_VC() throws IOException, XmlPullParserException {
    			
    			db.userid();
    	 		
    	 		 try
    	 		  {
    	 			 Cursor cur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
    		         if(cur.getCount()>0)
    		         {
    		        	cur.moveToFirst();	       	
    		        	
    		        	
    		        	    SoapObject request = new SoapObject(wb.NAMESPACE,"CreateTemplateVC");
    		                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
    		      		    envelope.dotNet = true;
    		      		    
    		        		request.addProperty("sv_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
    		         		request.addProperty("fld_inspectorid",db.UserId);
    		         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
    		         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
    		         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
    		         		request.addProperty("fld_primary",db.decode(cur.getString(cur.getColumnIndex("fld_primary"))));
    		         		String str = Html.fromHtml(db.decode(cur.getString(cur.getColumnIndex("fld_secondary")))).toString();
    		         		if(str.endsWith(","))
    		         		{
    		         			str = str.substring(0,str.length()-1);
    		         		}
    		         		request.addProperty("fld_secondary",str);
    		         		request.addProperty("fld_comments",db.decode(cur.getString(cur.getColumnIndex("fld_comments"))));
    		         		String inspstr = Html.fromHtml(db.decode(cur.getString(cur.getColumnIndex("fld_inspectionpoint")))).toString();
    		         		if(inspstr.endsWith(","))
    		         		{
    		         			inspstr = inspstr.substring(0,inspstr.length()-1);
    		         		}
    		         		request.addProperty("fld_inspectionpoint",inspstr);
    		         	
    		         		envelope.setOutputSoapObject(request);
    		         		System.out.println("request="+request);
    		         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
    		         		SoapObject response = null;
    		                try
    		                {
    		                      androidHttpTransport.call(wb.NAMESPACE+"CreateTemplateVC", envelope);
    		                      response = (SoapObject)envelope.getResponse();
    		                      System.out.println("CreateTemplateVCresult "+response);
    		                      
    		                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
    		                      if(response.getProperty("StatusCode").toString().equals("0"))
    		              		  {
    		                    	 
    		                    		  try
    		 	          				 {
    		 	          					db.hi_db.execSQL("UPDATE "+db.SetVC+ " SET fld_sendtoserver='"+response.getProperty("sv_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cur.getString(cur.getColumnIndex("fld_submodule")))+"' and fld_primary='"+db.encode(cur.getString(cur.getColumnIndex("fld_primary")))+"'");
    		 	          				 }
    		 	          				 catch (Exception e) {
    		 	          					// TODO: handle exception
    		 	          				 }
    		 	                    	handler_msg = 0;	  
    		                    	  
    		                    	 
    		                      	
    		              		  }
    		              		  else
    		              		  {
    		              			 error_msg = response.getProperty("StatusMessage").toString();
    		              			 handler_msg=1;
    		              			
    		              		  }
    		                }
    		                catch(Exception e)
    		                {
    		                     e.printStackTrace();
    		                }
    		        	//}
    		         }
    		  	}
    	 		catch (Exception e) {
    				// TODO: handle exception
    			}    	 	
	}
	private void SwimmingPools_Sub() throws IOException, XmlPullParserException {
	
		// TODO Auto-generated method stub
			  Cursor c1 = db.hi_db.rawQuery("select * from " + db.CreateSubOptionPools + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"'", null);
     		  System.out.println("df"+c1.getCount());
   	          if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();
   	        
				 do
				 {
				
   	        		System.out.println("inside for loop");
   	        	 SoapObject requestpools = new SoapObject(wb.NAMESPACE,"Create_SubPools");
	                SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    requestpools.addProperty("s_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    requestpools.addProperty("fld_inspectorid",db.UserId);
	         		
	      		 
	      		 requestpools.addProperty("fld_module",db.decode(c1.getString(c1.getColumnIndex("fld_module"))));
	      		requestpools.addProperty("fld_submodule",db.decode(c1.getString(c1.getColumnIndex("fld_submodule"))));
	      		requestpools.addProperty("fld_pooloption",db.decode(c1.getString(c1.getColumnIndex("fld_pooloption"))));
	      		String str1 = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_subpooloption")))).toString();
      		if(str1.endsWith(","))
      		{
      			str1 = str1.substring(0,str1.length()-1);
      		}
      		requestpools.addProperty("fld_subpooloption",str1);
      		
      		
	      		requestpools.addProperty("fld_subpoolother",db.decode(c1.getString(c1.getColumnIndex("fld_subpoolother"))));
	      		requestpools.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		
       		
	      		    
	      		    envelope1.setOutputSoapObject(requestpools);
	         		System.out.println("requestpools="+requestpools);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);System.out.println("dsfds");
	         		
	         		try
	                {System.out.println("saas");
	                androidHttpTransport1.call(wb.NAMESPACE+"Create_SubPools", envelope1);
	                      responsepools = (SoapObject)envelope1.getResponse();
	                      System.out.println("Create_SubPools "+responsepools);
	                     // handler_msg = 0;	  
	                      if(responsepools.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	 
	                    		  try
	 	          				 {
	                    			  System.out.println("SubmoduleEnc="+db.encode(c1.getString(c1.getColumnIndex("fld_submodule"))));
	                    			  System.out.println("SubmoduleDec="+db.decode(c1.getString(c1.getColumnIndex("fld_submodule"))));
	                    			  System.out.println("SubmoduleNot="+c1.getString(c1.getColumnIndex("fld_submodule")));
	 	          					db.hi_db.execSQL("UPDATE "+db.CreateSubOptionPools+ " SET fld_sendtoserver='"+responsepools.getProperty("s_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and fld_pooloption='"+c1.getString(c1.getColumnIndex("fld_pooloption"))+"'");
	 	          					System.out.println("UPDATE "+db.CreateSubOptionPools+ " SET fld_sendtoserver='"+responsepools.getProperty("s_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and fld_pooloption='"+c1.getString(c1.getColumnIndex("fld_pooloption"))+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	                    	  
	                    	 
	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = responsepools.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     	  
	}
   
	private void submit_visiblecondition() {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(wb.NAMESPACE,"CreateTemplateVC");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
 		envelope.dotNet = true;
 		
 		try
 		{
 			Cursor cur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	        if(cur.getCount()>0)
	        {
	        	cur.moveToFirst();	        	
	        	
	        	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
	        	{
	        		request.addProperty("sv_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
	         		request.addProperty("fld_inspectorid",db.UserId);
	         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
	         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
	         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
	         		request.addProperty("fld_primary",db.decode(cur.getString(cur.getColumnIndex("fld_primary"))));
	         		request.addProperty("fld_secondary",db.decode(cur.getString(cur.getColumnIndex("fld_secondary"))));
	         		request.addProperty("fld_comments",db.decode(cur.getString(cur.getColumnIndex("fld_comments"))));
	         		request.addProperty("fld_inspectionpoint",db.decode(cur.getString(cur.getColumnIndex("fld_inspectionpoint"))));
	         		
	         		envelope.setOutputSoapObject(request);
	         		System.out.println("CreateTemplateVCrequest="+request);
	         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
	         		SoapObject response = null;
	                try
	                {
	                      androidHttpTransport.call(wb.NAMESPACE+"CreateTemplateVC", envelope);
	                      response = (SoapObject)envelope.getResponse();
	                      System.out.println("CreateTemplateVCresult "+response);
	                      
	                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                      	 handler_msg = 0;
	                	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                catch(Exception e)
	                {
	                     e.printStackTrace();
	                }
	         		
	        	}
	        }
 		}
 		catch (Exception e) {
			// TODO: handle exception
		}
 		
 		
 		
	}
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(CreateSubModule.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public void success_alert() {
		// TODO Auto-generated method stub
       
		AlertDialog al = new AlertDialog.Builder(CreateSubModule.this).setMessage("You've successfully submitted your template.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				startActivity(new Intent(CreateSubModule.this,HomeScreen.class));
				finish();
				
				
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	
	
	private void Display() {
		// TODO Auto-generated method stub
		/*for(int i=0;i<cf.arrsub.length;i++)
		{*/
			
			try
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	            System.out.println("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'  and fld_module='"+db.encode(getmodulename)+"'");
				int cnt = cur.getCount();System.out.println("cnt="+cnt);
	            if(cnt>0)
	            {
	            	cur.moveToFirst();
					if (cur != null) {
						cbval=  new String[cur.getCount()];
						arrstrmodule = new String[cur.getCount()];
						arrdescription= new String[cur.getCount()];
						int j=0;
						do {
							String incval =cur.getString(cur.getColumnIndex("fld_NA"));
							String suboption =db.decode(cur.getString(cur.getColumnIndex("fld_submodule")));
							
							/*str_module += suboption+"~";
							arrstrmodule = str_module.split("~");*/
							arrstrmodule[j] = suboption;
							arrdescription[j] =db.decode(cur.getString(cur.getColumnIndex("fld_description")));
							
							if(incval.equals("1"))
				            {
								//str_cbval += "checked"+"~";
								cbval[j] = "checked";
								
					        }
				            else
				            {
				            	//str_cbval += "unchecked"+"~";
				            	cbval[j] = "unchecked";
								
						    }
							j++;
							//cbval = str_cbval.split("~");
							
							
					
						} while (cur.moveToNext());
						 effadap = new EfficientAdapter(cbval, this);
							modulelist.setAdapter(effadap);
							if(dialogid!=0)
							{
								modulelist.setSelection(dialogid);
							}
					}
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		
	}
	private void DefaultValue_Insert() {
		// TODO Auto-generated method stub
		cf.GetCurrentModuleName(getmodulename);
		for(int i=0;i<cf.arrsub.length;i++)
		{System.out.println("for");
			try
			{System.out.println("try");
				Cursor selcur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	            System.out.println("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+cf.arrsub[i]+"' and fld_module='"+db.encode(getmodulename)+"'");
				int cnt = selcur.getCount();
	            if(cnt==0)
	            {
	            	try
	            	{
	            		db.hi_db.execSQL(" INSERT INTO "+db.CreateDefault+" (fld_inspectorid,fld_module,fld_submodule,fld_templatename,fld_NA,fld_description) VALUES" +
								"('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(cf.arrsub[i])+"','"+db.encode(gettempname)+"','1','')");
                    	
	            	}
	            	catch (Exception e) {
						// TODO: handle exception
					}
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("catch="+e.getMessage());
			}
		}
		
	}
	protected void back() {
		// TODO Auto-generated method stub
		System.out.println("back"+gettempoption);
		Intent intent;
        if(gettempoption.equals("Report Section/Module"))
        {
       	    intent = new Intent(CreateSubModule.this, CreateTemplate.class);
        }
        else
        {
       	   intent = new Intent(CreateSubModule.this, CreateModule.class);
        }
		Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", "create");			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		startActivity(intent);
		finish();
	}
	class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
		int bi,ci,si,ei,ski;
		
		public EfficientAdapter(String[] cbval, Context context) {
			mInflater = LayoutInflater.from(context);
			value=cbval;
	
		}

		public int getCount() {
			int arrlen = 0;
			
			arrlen = arrstrmodule.length;		
		
			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
					    holder = new ViewHolder();
						convertView = mInflater.inflate(R.layout.custommodulelist, null);	
						holder.rel = (RelativeLayout)convertView.findViewById(R.id.rel);
						holder.moduletext = (TextView) convertView.findViewById(R.id.submodule);
						holder.cbsubmodule = (CheckBox)convertView.findViewById(R.id.cb_submodule);
						holder.spinsubmodule = (TextView)convertView.findViewById(R.id.spin_submodule);
						holder.spinsubmodule.setText("--Select--");
						holder.vcsubmodule = (TextView)convertView.findViewById(R.id.vc_submodule);
						holder.selectedatapoints = (TextView)convertView.findViewById(R.id.selected_data);
						holder.selectedatapoints.setVisibility(v.GONE);
						
						if (!arrdescription[position].equals("null") && !arrdescription[position].equals("")) 
						{
							arrdescription[position] = arrdescription[position].replace("null", "");
							holder.selectedatapoints.setVisibility(v.VISIBLE);
							String selectedtxt = cf.replacelastendswithcomma(arrdescription[position]);
							holder.selectedatapoints.setText(Html.fromHtml("<b>Selected : </b>"+selectedtxt));						
						}
						else
						{
							holder.selectedatapoints.setVisibility(v.GONE);
						}
						
						 convertView.setTag(holder);				 
						 holder.cbsubmodule.setOnCheckedChangeListener(new oncheckedlistener(position,holder.cbsubmodule,holder.spinsubmodule,holder.vcsubmodule));
						 holder.cbsubmodule.setOnTouchListener(new OntouchListener(position,holder.cbsubmodule,holder.spinsubmodule,holder.vcsubmodule));
						 holder.spinsubmodule.setOnClickListener(new onclicklistener(position,holder.cbsubmodule));
						 holder.vcsubmodule.setOnClickListener(new vcclicklistener(position,holder.cbsubmodule,holder.spinsubmodule,holder.vcsubmodule));
							
						 if(value[position].equals("checked"))
						 {
							 holder.cbsubmodule.setChecked(true);holder.cbsubmodule.setTextColor(Color.parseColor("#7CB34F"));
							
						 }
						 else
						 {
							 holder.cbsubmodule.setChecked(false);
						 }
					
						if (arrstrmodule[position].contains("null")) {
							arrstrmodule[position] = arrstrmodule[position].replace("null", "");
							holder.moduletext.setText(Html.fromHtml(arrstrmodule[position]));
						
						}
						else
						{
							holder.moduletext.setText(Html.fromHtml(arrstrmodule[position]));
							
						}					
						if (arrstrmodule[position].equals("Basement"))
						{
							bi = position;
						}
						if (arrstrmodule[position].equals("Crawlspace"))
						{
							ci = position;
						}
						if (arrstrmodule[position].equals("Siding/Surface Materials") && getmodulename.equals("Exterior"))
						{
							si = position;
						}
						if (arrstrmodule[position].equals("Electric Meter Location"))
						{
							ei = position;
						}
						if (arrstrmodule[position].equals("Skylights"))
						{
							ski = position;
						}
						
						if (arrstrmodule[position].equals("Basement")||arrstrmodule[position].equals("Crawlspace")||(arrstrmodule[position].equals("Siding/Surface Materials")&& getmodulename.equals("Exterior"))
							||arrstrmodule[position].equals("Electric Meter Location")||arrstrmodule[position].equals("Skylights"))
						{
							 holder.spinsubmodule.setVisibility(convertView.INVISIBLE);
							 holder.vcsubmodule.setVisibility(convertView.INVISIBLE);
							
						}
						if (arrstrmodule[position].equals("Basement - Size")||arrstrmodule[position].equals("Basement - Access")
								||arrstrmodule[position].equals("Basement - Stairs") ||arrstrmodule[position].equals("Basement - Floors")
								||arrstrmodule[position].equals("Basement - Exposed Columns and Beams") ||arrstrmodule[position].equals("Basement - Walls/Ceilings")
								||arrstrmodule[position].equals("Basement - Exposed Floor Systems")||arrstrmodule[position].equals("Basement - Drain Tile To")
								||arrstrmodule[position].equals("Basement - Heat/Cool(HVAC)")||arrstrmodule[position].equals("Basement - Finishes")
								||arrstrmodule[position].equals("Crawlspace Area - Characteristics")||arrstrmodule[position].equals("Crawlspace - Access")
								||arrstrmodule[position].equals("Crawlspace - Main Beam")||arrstrmodule[position].equals("Crawlspace - Floors")
								||arrstrmodule[position].equals("Crawlspace - Exposed Foundation Wall")||arrstrmodule[position].equals("Masonry/Plaster")
								||arrstrmodule[position].equals("Siding/Paneling")||arrstrmodule[position].equals("Wall Shingles")
								||arrstrmodule[position].equals("Accessibility")||arrstrmodule[position].equals("Main Panel Meter Located Inside")
								||arrstrmodule[position].equals("Main Panel Meter Located Outside")||arrstrmodule[position].equals("Main Panel Meter")
								||arrstrmodule[position].equals("# of Skylights")||arrstrmodule[position].equals("Skylight Type"))
						{
							 if(value[bi].equals("checked"))
							 {
								 if (arrstrmodule[position].equals("Basement - Size")||arrstrmodule[position].equals("Basement - Access")
								    ||arrstrmodule[position].equals("Basement - Stairs") ||arrstrmodule[position].equals("Basement - Floors")
									||arrstrmodule[position].equals("Basement - Exposed Columns and Beams") ||arrstrmodule[position].equals("Basement - Walls/Ceilings")
									||arrstrmodule[position].equals("Basement - Exposed Floor Systems")||arrstrmodule[position].equals("Basement - Drain Tile To")
									||arrstrmodule[position].equals("Basement - Heat/Cool(HVAC)")||arrstrmodule[position].equals("Basement - Finishes")
									)
									{
								        holder.rel.setVisibility(v.GONE);
									}
							
							 }
							 else
							 {
								   if (arrstrmodule[position].equals("Basement - Size")||arrstrmodule[position].equals("Basement - Access")
								    ||arrstrmodule[position].equals("Basement - Stairs") ||arrstrmodule[position].equals("Basement - Floors")
									||arrstrmodule[position].equals("Basement - Exposed Columns and Beams") ||arrstrmodule[position].equals("Basement - Walls/Ceilings")
									||arrstrmodule[position].equals("Basement - Exposed Floor Systems")||arrstrmodule[position].equals("Basement - Drain Tile To")
									||arrstrmodule[position].equals("Basement - Heat/Cool(HVAC)")||arrstrmodule[position].equals("Basement - Finishes")
									)
									{
								        holder.rel.setVisibility(v.VISIBLE);
									}
								 
							 }
							 if(value[ci].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("Crawlspace Area - Characteristics")||arrstrmodule[position].equals("Crawlspace - Access")
									||arrstrmodule[position].equals("Crawlspace - Main Beam")||arrstrmodule[position].equals("Crawlspace - Floors")
									||arrstrmodule[position].equals("Crawlspace - Exposed Foundation Wall"))
								 {
									 holder.rel.setVisibility(v.GONE);
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("Crawlspace Area - Characteristics")||arrstrmodule[position].equals("Crawlspace - Access")
									||arrstrmodule[position].equals("Crawlspace - Main Beam")||arrstrmodule[position].equals("Crawlspace - Floors")
									||arrstrmodule[position].equals("Crawlspace - Exposed Foundation Wall"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
							 
							 if(value[si].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("Masonry/Plaster")
									||arrstrmodule[position].equals("Siding/Paneling")||arrstrmodule[position].equals("Wall Shingles")
									||arrstrmodule[position].equals("Accessibility"))
								 {
									 holder.rel.setVisibility(v.GONE);
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("Masonry/Plaster")
											||arrstrmodule[position].equals("Siding/Paneling")||arrstrmodule[position].equals("Wall Shingles")
											||arrstrmodule[position].equals("Accessibility"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
							 
							 if(value[ei].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("Main Panel Meter Located Inside")
								    ||arrstrmodule[position].equals("Main Panel Meter Located Outside")||arrstrmodule[position].equals("Main Panel Meter"))
								 {
									 holder.rel.setVisibility(v.GONE);
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("Main Panel Meter Located Inside")
										    ||arrstrmodule[position].equals("Main Panel Meter Located Outside")||arrstrmodule[position].equals("Main Panel Meter"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
							 
							 if(value[ski].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("# of Skylights")||arrstrmodule[position].equals("Skylight Type"))
								 {
									 holder.rel.setVisibility(v.GONE);
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("# of Skylights")||arrstrmodule[position].equals("Skylight Type"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
						}
						
			return convertView;
		}

		 class ViewHolder {
			TextView moduletext,spinsubmodule,vcsubmodule,selectedatapoints;
			ImageView moduleimg;
			CheckBox cbsubmodule;
			RelativeLayout rel;
		   
		}

	}
	
	class vcclicklistener implements OnClickListener
	{
		public int id =0;
		public TextView spmod,vcmod;
		public CheckBox cbmod;
		
		public vcclicklistener(int position, CheckBox cbsubmodule, TextView spinsubmodule, TextView vcsubmodule) {
			// TODO Auto-generated constructor stub
			id=position;
			cbmod=cbsubmodule;
			spmod=spinsubmodule;
			vcmod=vcsubmodule;
			
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(!cbmod.isChecked())
			{
				Intent intent = new Intent(CreateSubModule.this, NewSetVC.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", "create");			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("submodulename", arrstrmodule[id]);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", gettempoption);			
				intent.putExtras(b6);
				System.out.println("gettempoptionsubmod="+gettempoption);
				startActivity(intent);
				finish();
				
			}
		}
		
	}
	class onclicklistener implements OnClickListener{

		public int id=0;
		public CheckBox cbmod;
		
		public onclicklistener(int position, CheckBox cbsubmodule) {
			// TODO Auto-generated constructor stub
			id = position;System.out.println("id=="+id);
			cbmod=cbsubmodule;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(!cbmod.isChecked())
			{
				//sp=1;
				newid=id;
				Intent i;
				if(arrstrmodule[id].equals("Washing Machine Present") || arrstrmodule[id].equals("Dryer Present")|| arrstrmodule[id].equals("Dishwasher Present")
						|| arrstrmodule[id].equals("Disposal Present")|| arrstrmodule[id].equals("Oven Present")|| arrstrmodule[id].equals("Exhaust Fan System Present")
						|| arrstrmodule[id].equals("Refrigerator Present")|| arrstrmodule[id].equals("Microwave Present")|| arrstrmodule[id].equals("Trash Compactor Present")
						|| arrstrmodule[id].equals("Bread Warmer Present") ||  arrstrmodule[id].equals("Range top Present") ||  arrstrmodule[id].equals("Lawn Sprinkler system Present")
						|| arrstrmodule[id].equals("Water Softener Present") || arrstrmodule[id].equals("Water heater Present") || arrstrmodule[id].equals("Attic/House fan Present")
						|| arrstrmodule[id].equals("Door bell Present"))
				{
					i = new Intent(CreateSubModule.this,AppShowDialog.class);
				}
				else if(arrstrmodule[id].equals("Pool Features") || arrstrmodule[id].equals("Pool Lighting")
						|| arrstrmodule[id].equals("Pool Equipment") || arrstrmodule[id].equals("Plumbing Systems")
						|| arrstrmodule[id].equals("Pool Pumping System")
						|| arrstrmodule[id].equals("Pool Cleaning Equipment") || arrstrmodule[id].equals("Water Treatment")
						|| arrstrmodule[id].equals("Water Heating Systems") || arrstrmodule[id].equals("Timers/Clocks/Controls")
						|| arrstrmodule[id].equals("Electrical Service") || arrstrmodule[id].equals("Pool Cover(s)")
						|| arrstrmodule[id].equals("Safety Equipment")|| arrstrmodule[id].equals("Water Chemistry Test")
						|| arrstrmodule[id].equals("General Comments/Observations"))
				{
					i = new Intent(CreateSubModule.this,LoadPoolShowDialog.class);
				}
				else
				{
					System.out.println("TTTTTTTTCreatesub");
					i = new Intent(CreateSubModule.this,ShowDialog.class);
				}
				    i.putExtra("arrstr", arrstrmodule[id]);
				    i.putExtra("id",id);
					i.putExtra("templatename", gettempname);
					i.putExtra("modulename", getmodulename);
					i.putExtra("currentview", getcurrentview);
					i.putExtra("tempoption", gettempoption);
					i.putExtra("selectedid", "");
					startActivityForResult(i, 101);
					//startActivity(i);
				
			//	new Show_Dialog(arrstrmodule[id],gettempname,getmodulename,CreateSubModule.this);
			   
			}
		}
		
	}
	 @Override  
     protected void onActivityResult(int requestCode, int resultCode, Intent data)  
     {  
               super.onActivityResult(requestCode, resultCode, data);  
                   
                // check if the request code is same as what is passed  here it is 2  
                      if(requestCode==101)  
                       {  
                    	  
                    	  gettempname=data.getStringExtra("templatename");  
                    	  getmodulename=data.getStringExtra("modulename");
                    	  getcurrentview=data.getStringExtra("currentview");
                    	  gettempoption=data.getStringExtra("tempoption");
                    	  dialogid=data.getExtras().getInt("id");
                    	  Display();
                    	  effadap.notifyDataSetChanged();	
                    	 // modulelist.setSelection(dialogid);
               
                       }  
   
   }  
	 @Override
	    protected void onSaveInstanceState(Bundle outState) {
		 if (p_sv != null) {
			  p_sv.progress_live=false;
		 }
	       // outState.putInt("spin_index", sp);
	        outState.putInt("id_index", newid);
	        outState.putStringArray("module_index", arrstrmodule);
	        outState.putString("temp_index", gettempname);
	        outState.putString("modulename_index", getmodulename);
	        outState.putInt("id", dialogid);
	        outState.putInt("handler", handler_msg);System.out.println("handleronsave="+handler_msg);
	        outState.putString("error", error_msg);
	        super.onSaveInstanceState(outState);
	    }
	 @Override
	    protected void onRestoreInstanceState(Bundle savedInstanceState) {
		 //  sp = savedInstanceState.getInt("spin_index");
		   newid = savedInstanceState.getInt("id_index");
		   arrstrmodule = savedInstanceState.getStringArray("module_index");
		   gettempname = savedInstanceState.getString("temp_index");
		   getmodulename = savedInstanceState.getString("modulename_index");
		   dialogid = savedInstanceState.getInt("id");
		   handler_msg = savedInstanceState.getInt("handler");System.out.println("handleronrestore="+handler_msg);
	       error_msg = savedInstanceState.getString("error");
	       
	        if(handler_msg==0)
	    	{
	    		success_alert();
	    	}
	    	else if(handler_msg==1)
	    	{
	    		failure_alert();
	    	}
	    	
		 //  System.out.println("sp=="+sp);
		   /*if(sp==1)
		   {
			   //new Show_Dialog(arrstrmodule[newid],gettempname,getmodulename,CreateSubModule.this);
			    Intent i = new Intent(CreateSubModule.this,ShowDialog.class);						
				i.putExtra("arrstr", arrstrmodule[newid]);
				i.putExtra("tempname", gettempname);
				i.putExtra("modulename", getmodulename);
				i.putExtra("currentview", getcurrentview);
				i.putExtra("tempoption", gettempoption);
				startActivity(i);
		   }*/
		 
		   super.onRestoreInstanceState(savedInstanceState);
	    }
	 class OntouchListener implements OnTouchListener{
		    public int id =0;
			public TextView spmod,vcmod;
			public CheckBox cbmod;

		public OntouchListener(int position, CheckBox cbsubmodule,
				TextView spinsubmodule, TextView vcsubmodule) {
			// TODO Auto-generated constructor stub
			
			id=position;
			cbmod=cbsubmodule;
			spmod=spinsubmodule;
			vcmod=vcsubmodule;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if(cbmod.isChecked())
			{
			    chkbool=true;
			}
			else
			{
				chkbool=false;
			}
			return false;
		}
		 
	 }
	 class oncheckedlistener implements OnCheckedChangeListener {
			public int id =0;
			public TextView spmod,vcmod;
			public CheckBox cbmod;
			
			public oncheckedlistener(int position, CheckBox cbsubmodule, TextView spinsubmodule, TextView vcsubmodule) {
				// TODO Auto-generated constructor stub
				id=position;
				cbmod=cbsubmodule;
				spmod=spinsubmodule;
				vcmod=vcsubmodule;
				
			}

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				
				effadap.notifyDataSetChanged();
				System.out.println("ischecked="+isChecked+getmodulename);
				if(isChecked)
				{
					
					if(cf.arrsub[id].equals("Crawlspace")||cf.arrsub[id].equals("Basement")|| cf.arrsub[id].equals("Electric Meter Location")
							|| cf.arrsub[id].equals("Skylights") || (cf.arrsub[id].equals("Siding/Surface Materials")&& getmodulename.equals("Exterior")))
					{
						if(getmodulename.equals("Exterior"))
						{
							
						}
						String[] strr = null;
						if(cf.arrsub[id].equals("Crawlspace"))
						{
							strr = crawlspace;
						}
						else if(cf.arrsub[id].equals("Basement"))
						{
							strr = basement;
						}
						else if(cf.arrsub[id].equals("Electric Meter Location"))
						{
							strr = electricmeter;
						}
						else if(cf.arrsub[id].equals("Siding/Surface Materials") )
						{
							strr = extsiding;
						}
						else if(cf.arrsub[id].equals("Skylights"))
						{
							strr = skylights;
						}
						showcrawl(cbmod,id,strr);
					}
					else
					{System.out.println("else");
					try
					{
						Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_module='"+db.encode(getmodulename)+"'", null);
			            int cnt = cur.getCount();
			            if(cnt>0)
			            {
			            	cur.moveToFirst();
							String defaultval =db.decode(cur.getString(cur.getColumnIndex("fld_description")));
							System.out.println("defaultval="+defaultval);
							if(defaultval.equals(""))
							{
								spmod.setBackgroundResource(R.drawable.btn_dropdown_disabled);
								vcmod.setTextColor(Color.parseColor("#C0C0C0"));
								vcmod.setText(Html.fromHtml("Add/Edit Condition"));
								value[id]="checked";
								try
								{
									db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_NA='1' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_module='"+db.encode(getmodulename)+"'");
								}
								catch (Exception e) {
									// TODO: handle exception
								}
							}
			               else
			               {
			            	
			            	AlertDialog.Builder popupDialog = new AlertDialog.Builder(CreateSubModule.this);
			            	popupDialog.setTitle("Override...");
			            	popupDialog.setMessage("If you click yes, you will override the data saved to NA.\nDo you want to proceed?");
			            	popupDialog.setIcon(R.drawable.ic_launcher);
			            	popupDialog.setPositiveButton("Yes",
							        new DialogInterface.OnClickListener() {
							            public void onClick(DialogInterface dialog, int which) {
							               
							            	spmod.setBackgroundResource(R.drawable.btn_dropdown_disabled);
							            	vcmod.setTextColor(Color.parseColor("#C0C0C0"));
											vcmod.setText(Html.fromHtml("Add/Edit Condition"));
											value[id]="checked";
											try
											{
												
												db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_NA='1',fld_description='' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_module='"+db.encode(getmodulename)+"'");
												System.out
														.println("UPDATE "+db.CreateDefault+ " SET fld_NA='1',fld_description='' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_module='"+db.encode(getmodulename)+"'");
												Display();
												effadap.notifyDataSetChanged();
												 modulelist.setSelection(id);
											}
											catch (Exception e) {
												// TODO: handle exception
											}
							            	 dialog.cancel();
							            }
							        });
							
			            	popupDialog.setNegativeButton("No",
							        new DialogInterface.OnClickListener() {
							            public void onClick(DialogInterface dialog, int which) {
							                // Write your code here to execute after dialog
							            	cbmod.setChecked(false);
							                dialog.cancel();
							            }
							        });
							 
			            	popupDialog.show();
			            }
					 }
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					
					}
				
				}
				else
				{System.out.println("uncheckelse");
					
					try
					{
						db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_NA='0' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_module='"+db.encode(getmodulename)+"'");
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					
					spmod.setBackgroundResource(R.drawable.btn_dropdown_normal);
					vcmod.setTextColor(Color.parseColor("#3F00FF"));
					vcmod.setText(Html.fromHtml("<u>Add/Edit Condition</u>"));
					value[id]="unchecked";
					System.out.println("chkbool="+chkbool);
					if(chkbool)
					{
						chkbool=false;System.out.println("firstentry");
						Boolean flag=true;
						if(arrstrmodule[id].equals("Siding/Surface Materials") && getmodulename.equals("Exterior"))
						{
							flag=false;
						}
						if(!arrstrmodule[id].equals("Basement")&&!arrstrmodule[id].equals("Crawlspace") && !arrstrmodule[id].equals("Skylights")
								&& !arrstrmodule[id].equals("Electric Meter Location") && flag.equals(true))
						{System.out.println("enters");
							Intent i;
							if(arrstrmodule[id].equals("Washing Machine Present") || arrstrmodule[id].equals("Dryer Present")|| arrstrmodule[id].equals("Dishwasher Present")
									|| arrstrmodule[id].equals("Disposal Present")|| arrstrmodule[id].equals("Oven Present")|| arrstrmodule[id].equals("Exhaust Fan System Present")
									|| arrstrmodule[id].equals("Refrigerator Present")|| arrstrmodule[id].equals("Microwave Present")|| arrstrmodule[id].equals("Trash Compactor Present")
									|| arrstrmodule[id].equals("Bread Warmer Present") ||  arrstrmodule[id].equals("Range top Present") ||  arrstrmodule[id].equals("Lawn Sprinkler system Present")
									|| arrstrmodule[id].equals("Water Softener Present") || arrstrmodule[id].equals("Water heater Present") || arrstrmodule[id].equals("Attic/House fan Present")
									|| arrstrmodule[id].equals("Door bell Present"))
							{
								i = new Intent(CreateSubModule.this,AppShowDialog.class);
							}
							else if(arrstrmodule[id].equals("Pool Features") || arrstrmodule[id].equals("Pool Lighting")
									|| arrstrmodule[id].equals("Pool Equipment") || arrstrmodule[id].equals("Plumbing Systems")
									|| arrstrmodule[id].equals("Pool Pumping System")
									|| arrstrmodule[id].equals("Water Treatment")
									|| arrstrmodule[id].equals("Water Heating Systems") || arrstrmodule[id].equals("Timers/Clocks/Controls")
									|| arrstrmodule[id].equals("Electrical Service") || arrstrmodule[id].equals("Pool Cover(s)")
									|| arrstrmodule[id].equals("Safety Equipment")|| arrstrmodule[id].equals("Water Chemistry Test")
									|| arrstrmodule[id].equals("General Comments/Observations"))
							{
								i = new Intent(CreateSubModule.this,LoadPoolShowDialog.class);
								
							}
							else
							{
							     i = new Intent(CreateSubModule.this,ShowDialog.class);
							     
							}
							System.out.println("inside create sub modules page");
							    i.putExtra("arrstr", arrstrmodule[id]);
							    i.putExtra("id",id);
								i.putExtra("templatename", gettempname);
								i.putExtra("modulename", getmodulename);
							 	i.putExtra("currentview", getcurrentview);
								i.putExtra("tempoption", gettempoption);
								i.putExtra("selectedid", "");
								startActivityForResult(i, 101);
						}
					}
				}
				
			}

		
	 }
	 public void showcrawl(final CheckBox cbmod, final int id, final String[] strr) {
			// TODO Auto-generated method stub
			AlertDialog.Builder popupDialog = new AlertDialog.Builder(CreateSubModule.this);
	    	popupDialog.setTitle("Override...");
	    	popupDialog.setMessage("Are you sure you want to overwrite the saved data?");
	    	popupDialog.setIcon(R.drawable.ic_launcher);
	    	popupDialog.setPositiveButton("Yes",
			        new DialogInterface.OnClickListener() {
			            public void onClick(DialogInterface dialog, int which) {
			               
			            	
							cbmod.setChecked(true);
							try
							{
								db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_NA='1',fld_description='' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+cf.arrsub[id]+"'");
								value[id]="checked";
								for(int i=0;i<strr.length;i++)
								{
									db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_NA='1',fld_description='' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+strr[i]+"'");
								
								}
								Display();
								effadap.notifyDataSetChanged();
								 modulelist.setSelection(id);
							}
							catch (Exception e) {
								// TODO: handle exception
							}
			            	 dialog.cancel();
			            }
			        });
			
	    	popupDialog.setNegativeButton("No",
			        new DialogInterface.OnClickListener() {
			            public void onClick(DialogInterface dialog, int which) {
			                // Write your code here to execute after dialog
			            	cbmod.setChecked(false);
			                dialog.cancel();
			            }
			        });
			 
	    	popupDialog.show();
		}
			
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
}
