package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.io.ByteArrayOutputStream;




import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.text.Layout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Spinner;

public class CreateTemplate extends Activity{
	RadioGroup rdg_createtempplate;
	String str_rdgtemplate="",getmodulename="--Select--",gettempname="",tempname="",tempoption="",tempmodulename="",
			tempdate="",temptime="",sel_tempname="",sel_tempoption="",sel_modulename="",val="";
	LinearLayout modulelinear;
	Spinner spin_module;
	String[] module ;
	static String[] arrtempname,arrtempoption,arrtempmodulename,arrtempdate,arrtemptime;
	View v;
	ArrayAdapter modulearrayadap;
	Button btn_back,btn_save,btn_clear;
	CommonFunction cf;
	DatabaseFunction db;
	EditText et_tempname;
	boolean rgcond=false;
	ListView list_template;
	TextView tv_notemp;
	 Bundle newBundy = new Bundle();
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		setContentView(R.layout.activity_createtemplate);
		db.CreateTable(1);
		db.CreateTable(2);
		db.userid();
		cf.GetCalender();
		
		rdg_createtempplate = (RadioGroup)findViewById(R.id.rdg_template);
		modulelinear = (LinearLayout)findViewById(R.id.modulelinear);
		spin_module = (Spinner)findViewById(R.id.spin_module);
		
		
		module = getResources().getStringArray(R.array.module);
		modulearrayadap = new ArrayAdapter<String>(CreateTemplate.this,android.R.layout.simple_spinner_item,module);
		modulearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_module.setAdapter(modulearrayadap);
		spin_module.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		et_tempname = (EditText)findViewById(R.id.et_templatename);
		et_tempname.addTextChangedListener(new CustomTextWatcher(et_tempname));
		list_template = (ListView)findViewById(R.id.templatelist);
		tv_notemp = (TextView)findViewById(R.id.noinfotemp);
		
		rdg_createtempplate.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if(isChecked)
		        {
		        	str_rdgtemplate= checkedRadioButton.getText().toString().trim();
		        	if(str_rdgtemplate.equals("Report Section/Module"))
		        	{
		        		modulelinear.setVisibility(v.VISIBLE);
		        		et_tempname.setText("");
		        	}
		        	else
		        	{
		        		spin_module.setSelection(0);
		        		modulelinear.setVisibility(v.GONE);
		        		et_tempname.setText("");
		        	}
		        }
				
			}
		});
		
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(CreateTemplate.this,HomeScreen.class));
				finish();
			}
		});
		
		btn_clear = (Button)findViewById(R.id.btn_clear) ;
		btn_clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 clear();
			}
		});
		
		et_tempname.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        boolean handled = false;
		        if (actionId == EditorInfo.IME_ACTION_DONE) {
		        	cf.hidekeyboard(et_tempname);
		        	save_validation();
		            handled = true;
		        }
		        return handled;
		    }
		});
		
		btn_save = (Button)findViewById(R.id.btn_save) ;
		btn_save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				save_validation();
			}
		});
		
		LoadTemplates();
	}
	 protected void save_validation() {
		// TODO Auto-generated method stub
		 if(str_rdgtemplate.equals(""))
     	{
				cf.DisplayToast("Please select the Create Template option");
     	}
			else
			{
				if(str_rdgtemplate.equals("Report Section/Module"))
	        	{
					if(getmodulename.equals("--Select--"))
					{
						cf.DisplayToast("Please select the Module");
					}
					else
					{
						TemplateName_Validation();
					}
	        	}
				else
				{ 
					TemplateName_Validation();
				}
			}
	}
	@Override
	    protected void onSaveInstanceState(Bundle outState) {
		 
	        outState.putString("template_index", str_rdgtemplate);
	        outState.putString("module_index", getmodulename);
	        outState.putString("templatename_index", gettempname);
	        outState.putString("dialog_index", val);
	        outState.putString("dialogtempname_index", sel_tempname);
	        outState.putString("dialogtempoption_index", sel_tempoption);
	        outState.putString("dialogtempmodule_index", sel_modulename);
	        super.onSaveInstanceState(outState);
	    }
	 @Override
	    protected void onRestoreInstanceState(Bundle savedInstanceState) {
		   str_rdgtemplate = savedInstanceState.getString("template_index");
		   getmodulename = savedInstanceState.getString("module_index");
		   gettempname = savedInstanceState.getString("templatename_index");
		   val = savedInstanceState.getString("dialog_index");
		   sel_tempname = savedInstanceState.getString("dialogtempname_index");
		   sel_tempoption = savedInstanceState.getString("dialogtempoption_index");
		   sel_modulename = savedInstanceState.getString("dialogtempmodule_index");
		   if(!str_rdgtemplate.equals(""))
		   {
			   ((RadioButton)findViewById(R.id.rdg_template).findViewWithTag(str_rdgtemplate)).setChecked(true);   
		   }
		   if(str_rdgtemplate.equals("Report Section/Module"))
		   {
			   modulelinear.setVisibility(v.VISIBLE);
			   spin_module.setSelection(modulearrayadap.getPosition(getmodulename));
		   }
		   else
		   {
			   spin_module.setSelection(0);
			   modulelinear.setVisibility(v.GONE);
		   }
		   et_tempname.setText(gettempname);
		   if(val.equals("2"))
		   {
			   deletedialogshow();
		   }
		  
		   super.onRestoreInstanceState(savedInstanceState);
	    }

	protected void clear() {
		// TODO Auto-generated method stub
		rgcond=true;str_rdgtemplate="";
		 try
		 {
			 if(rgcond)
			 {
				 rdg_createtempplate.clearCheck();
			 }
		 }
		 catch (Exception e) {
			// TODO: handle exception
		}
		 spin_module.setSelection(0);
		 modulelinear.setVisibility(v.GONE);
		 et_tempname.setText("");
	}
	public void TemplateName_Validation() {
		// TODO Auto-generated method stub
		gettempname = et_tempname.getText().toString();
		if(gettempname.trim().equals(""))
		{
			cf.DisplayToast("Please enter the Template Name");
		}
		else
		{
			try
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_tempoption='"+str_rdgtemplate+"'", null);
				if(cur.getCount()==0)
				{
					db.hi_db.execSQL(" INSERT INTO "+db.CreateTemplate+" (fld_inspectorid,fld_tempoption,fld_modulename,fld_templatename,fld_date,fld_time) VALUES" +
							"('"+db.UserId+"','"+str_rdgtemplate+"','"+db.encode(getmodulename)+"','"+db.encode(gettempname)+"','"+cf.cd+"','"+cf.ct+"')");
					cf.DisplayToast("Template name added successfully");
                	cf.HideKeyboard(et_tempname);
                	clear();
                	LoadTemplates();
				}
				else
				{
					cf.DisplayToast("Template already exists");
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	private void LoadTemplates() {
		// TODO Auto-generated method stub
		tempname="";tempoption="";tempmodulename="";tempdate="";temptime="";
		try
		{
			
    		Cursor cr_sel= db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' order by fld_date,fld_time DESC", null);
	     	if(cr_sel.getCount()>0)
			{
				tv_notemp.setVisibility(v.GONE);
				list_template.setVisibility(v.VISIBLE);
				cr_sel.moveToFirst();
				if (cr_sel != null) {
					do {
						tempname += db.decode(cr_sel.getString(cr_sel.getColumnIndex("fld_templatename"))) + "~";
						System.out.println("tempname"+tempname);
						System.out.println(db.decode(cr_sel.getString(cr_sel.getColumnIndex("fld_templatename"))) );
						tempoption += db.decode(cr_sel.getString(cr_sel.getColumnIndex("fld_tempoption"))) + "~";
						tempmodulename += db.decode(cr_sel.getString(cr_sel.getColumnIndex("fld_modulename"))) + "~";
						tempdate += cr_sel.getString(cr_sel.getColumnIndex("fld_date"))+"~";
						temptime += cr_sel.getString(cr_sel.getColumnIndex("fld_time"))+"~";
						
						
						

					} while (cr_sel.moveToNext());
					arrtempname = tempname.split("~");
					arrtempoption = tempoption.split("~");
					arrtempmodulename = tempmodulename.split("~");
					arrtempdate = tempdate.split("~");
					arrtemptime = temptime.split("~");
					list_template.setAdapter(new EfficientAdapter(this));
				}
           }
			else
			{
				tv_notemp.setVisibility(v.VISIBLE);
				list_template.setVisibility(v.GONE);
			}
		}
	    catch (Exception e) {
			// TODO: handle exception
		}
	}
	class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
	
		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
			
		}

		public int getCount() {
			int arrlen = 0;
			
				arrlen = arrtempname.length;
				return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.listview, null);
				holder.text = (TextView) convertView.findViewById(R.id.TextView01);
				holder.text2 = (TextView) convertView.findViewById(R.id.TextView02);
				holder.text3 = (TextView) convertView.findViewById(R.id.TextView04);
				holder.createtxt = (Button) convertView.findViewById(R.id.createbtn);
				holder.deletetxt = (Button) convertView.findViewById(R.id.deletebtn);
				
				convertView.setTag(holder);
						
				if (arrtempname[position].contains("null")||arrtempoption[position].contains("null")
						||arrtempdate[position].contains("null")
						||arrtemptime[position].contains("null")) {
					arrtempname[position] = arrtempname[position].replace("null", "");
					arrtempoption[position] = arrtempoption[position].replace("null", "");
					if(arrtempoption[position].equals("Report Section/Module"))
					{
						if(arrtempmodulename[position].contains("null"))
						{
					      arrtempmodulename[position] = arrtempmodulename[position].replace("null", "");
						}
					}
					arrtempdate[position] = arrtempdate[position].replace("null", "");
					arrtemptime[position] = arrtemptime[position].replace("null", "");
					
					holder.text.setText(Html.fromHtml("<b>Template name :</b> "+arrtempname[position]));
					if(arrtempoption[position].equals("Report Section/Module"))
					{
						holder.text2.setText(Html.fromHtml("<b>Template created for :</b> "+arrtempmodulename[position]+" "+arrtempoption[position]));
					}
					else
					{
					    holder.text2.setText(Html.fromHtml("<b>Template created for :</b> "+arrtempoption[position]));
					}
					holder.text3.setText(Html.fromHtml("<b>Created on :</b> "+arrtempdate[position]+" @ "+arrtemptime[position]));
				}
				else
				{
					holder.text.setText(Html.fromHtml("<b>Template name :</b> "+arrtempname[position]));
					if(arrtempoption[position].equals("Report Section/Module"))
					{
						holder.text2.setText(Html.fromHtml("<b>Template created for :</b> "+arrtempmodulename[position]+" "+arrtempoption[position]));
					}
					else
					{
					    holder.text2.setText(Html.fromHtml("<b>Template created for :</b> "+arrtempoption[position]));
					}
					holder.text3.setText(Html.fromHtml("<b>Created on :</b> "+arrtempdate[position]+" @ "+arrtemptime[position]));
					
				}
				holder.createtxt.setOnClickListener(new CreateClickListner(position));
				holder.deletetxt.setOnClickListener(new DeleteClickListner(position));
		
			return convertView;
		}

		class ViewHolder {
			TextView text,text2,text3;
			Button createtxt,deletetxt; 
		
		}

	}
	class DeleteClickListner implements OnClickListener {
		public int id =0;
		public DeleteClickListner(int position) {
			// TODO Auto-generated constructor stub
			id=position;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			   sel_tempname = arrtempname[id];
	           if(arrtempoption[id].equals("Report Section/Module"))
			   {
	                sel_modulename = arrtempmodulename[id];
			   }
	           else
	           {
	        	   sel_modulename ="";
	           }
	          
	          sel_tempoption = arrtempoption[id];val="2";
	        
	          deletedialogshow();
	          
		}
		
	}
	 class CreateClickListner implements OnClickListener {
		public int id =0;
		
		public CreateClickListner(int position) {
			// TODO Auto-generated constructor stub
			System.out.println("string="+position);
			id=position;
			//val=string;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
           sel_tempname = arrtempname[id];
           if(arrtempoption[id].equals("Report Section/Module"))
		   {
                sel_modulename = arrtempmodulename[id];
		   }
           else
           {
        	   sel_modulename ="";
           }
          
          sel_tempoption = arrtempoption[id];val="1";
       System.out.println("val="+val);
          /* if(val.equals("1"))
           {
				*/
       
       if(sel_modulename.equals("HVAC"))
       {
    	   
    	   cf.boolhvac=true;
    	    Intent  intent = new Intent(CreateTemplate.this, NewHVAC1.class);
    	    Bundle b2 = new Bundle();
			b2.putString("templatename", sel_tempname);			
			intent.putExtras(b2);
			Bundle b3 = new Bundle();
			b3.putString("currentview", "create");			
			intent.putExtras(b3);
			Bundle b4 = new Bundle();
			b4.putString("modulename", sel_modulename);			
			intent.putExtras(b4);
			Bundle b5 = new Bundle();
			b5.putString("tempoption", sel_tempoption);			
			intent.putExtras(b5);
			Bundle b6 = new Bundle();
			b6.putString("selectedid", "");			
			intent.putExtras(b6);
			startActivity(intent);
			finish();
       }
       else  if(sel_modulename.equals("Bathroom"))
       {
    	   System.out.println("111111111111");
    	    Intent intent = new Intent(CreateTemplate.this, LoadBathroom.class);
		    Bundle b2 = new Bundle();
			b2.putString("templatename", sel_tempname);			
			intent.putExtras(b2);
			Bundle b3 = new Bundle();
			b3.putString("currentview", "create");			
			intent.putExtras(b3);
			Bundle b4 = new Bundle();
			b4.putString("modulename", sel_modulename);			
			intent.putExtras(b4);
			Bundle b5 = new Bundle();
			b5.putString("tempoption", sel_tempoption);			
			intent.putExtras(b5);
			Bundle b6 = new Bundle();
			b6.putString("selectedid", "");			
			intent.putExtras(b6);
			startActivity(intent);
			finish();
       }
       else{
    	     Intent intent;
             if(sel_tempoption.equals("Report Section/Module"))
             {
            	 intent = new Intent(CreateTemplate.this, CreateSubModule.class);
             }
             else
             {
            	 intent = new Intent(CreateTemplate.this, CreateModule.class);
             }
                 
				Bundle b2 = new Bundle();
				b2.putString("templatename", sel_tempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", "create");			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", sel_modulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("tempoption", sel_tempoption);			
				intent.putExtras(b5);
				startActivity(intent);
				finish();
       }				
          /* }
           else
           {
        	  deletedialogshow();
           }*/

		}

	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
	    	getmodulename = parent.getItemAtPosition(pos).toString();
	    		
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		startActivity(new Intent(CreateTemplate.this,HomeScreen.class));
		finish();
	}
	public void deletedialogshow() {
		// TODO Auto-generated method stub
		 AlertDialog.Builder deldialog = new AlertDialog.Builder(CreateTemplate.this);				 
	  	   deldialog.setTitle("Delete Template");				 
	  	   deldialog.setMessage("Are you sure, you want to delete the selected "+sel_tempname+" template?\nYou will lose all your saved data.");				 
	  	   deldialog.setIcon(R.drawable.ic_launcher);				 
	  	   deldialog.setNegativeButton("Yes",
				        new DialogInterface.OnClickListener() {
				            public void onClick(DialogInterface dialog, int which) {
				                // Write your code here to execute after dialog
				            	try
								{
									db.hi_db.execSQL("DELETE FROM "+db.CreateTemplate+ "  WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(sel_tempname)+"' and fld_tempoption='"+sel_tempoption+"'");
									System.out.println("DELETE FROM "+db.CreateTemplate+ "  WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(sel_tempname)+"' and fld_tempoption='"+sel_tempoption+"'");
									cf.DisplayToast("Template deleted successfully");
									val="";
									LoadTemplates();
								}
								catch (Exception e) {
									// TODO: handle exception
								}
				            	 dialog.cancel();
				            }
				        });
				
	  	   deldialog.setPositiveButton("No",
				        new DialogInterface.OnClickListener() {
				            public void onClick(DialogInterface dialog, int which) {
				                // Write your code here to execute after dialog
				            	val="";
				            	dialog.cancel();
				            }
				        });
	  	   deldialog.setCancelable(false);
	  	   deldialog.show();
	}
}
