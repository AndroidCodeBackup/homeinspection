package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;




public class NewHotwater extends Activity {
	String gettempname="",getcurrentview="",getmodulename="",gettempoption="",getselectedho="",watervalue="",seltdval="",finalselstrval="",charvalue="",tempchar="",
		   radvalue="",temprad="",str="";
	Spinner spinloadtemplate;
	int k=0,id=0,nid=0,spintouch=0,chk=0;
	View v;
	String arrtempname[];
	String tempname="";
	ArrayAdapter adapter;
	CheckBox chknotapplicable;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	LinearLayout lin;
	String[] hotchar={"One Pipe System","Two Pipe System","Fin Coils","Boiler","Subfloor","Convectors","Radiators","Baseboard"},
			 radiators={"Cast Iron Radiators","Steel Radiators","Manual Fill","Water Level Sight Glass","Bleed Valves"};
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	TextView tvchar,tvrad;
	CheckBox ch[];
	EditText etother;
	Button btntakephoto;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}		
		
		setContentView(R.layout.activity_newhotwater);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(20);
		db.CreateTable(21);
		db.CreateTable(22);
		db.CreateTable(23);
		db.CreateTable(24);
		db.CreateTable(25);
		db.CreateTable(38);
		db.CreateTable(39);
		db.CreateTable(40);
		db.CreateTable(42);
		db.CreateTable(43);
		db.CreateTable(44);
		db.userid();
		
		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(NewHotwater.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							Bundle b4 = new Bundle();
							b4.putString("currentview","takephoto");			
							intent.putExtras(b4);
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getselectedho);			
							intent.putExtras(b5);
							startActivity(intent);
							finish();
			}
		});
		chknotapplicable = (CheckBox) findViewById(R.id.hvacnotappl);		 
		chknotapplicable.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
	                //is chkIos checked?
			if (((CheckBox) v).isChecked())
			{
				chk=1;
				System.out.println("its checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
				
			}
			else
			{
				chk=2;
				System.out.println("its not checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
				
				
			}
	 
		  }
		});

		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/squadaone-regular.ttf"); 
		btnsubmit.setTypeface(font); 
		
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);
									
				}  
			}
		});
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC", null);
            System.out.println("cur="+cur.getCount()+"select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC");
			if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("tempname"+tempname);
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);				
				}  
			}
		});
		
		
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		((TextView)findViewById(R.id.modulename)).setVisibility(v.VISIBLE);
		((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		
		Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(NewHotwater.this,HomeScreen.class));
					finish();
					 
				}
			});
		
		Button	save = (Button)findViewById(R.id.btnsave);
		save.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					try
					{
						if(chknotapplicable.isChecked()==true)
						{
							chk=1;
						}
						else
						{
							chk=2;
						}						
							if(getcurrentview.equals("start"))
							 {
										Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"'  and fld_srid='"+getselectedho+"'", null);
										
										System.out.println("COUNT="+cur.getCount()+"select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"'  and fld_srid='"+getselectedho+"'");										
										if(cur.getCount()==0)
										{
											System.out.println(" INSERT INTO "+db.LoadHotwatersystem+" (fld_templatename,fld_inspectorid,fld_srid,fld_characteristics,fld_radiators,fld_hotwatersysNA) VALUES" +
																						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+db.encode(tempchar)+"','"+db.encode(temprad)+"','"+chk+"')");
											db.hi_db.execSQL(" INSERT INTO "+db.LoadHotwatersystem+" (fld_templatename,fld_inspectorid,fld_srid,fld_characteristics,fld_radiators,fld_hotwatersysNA) VALUES" +
																						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+db.encode(tempchar)+"','"+db.encode(temprad)+"','"+chk+"')");
											
										}
										else
										{
											//if(tempchar.equals("")){tempchar = cur.getString(cur.getColumnIndex("fld_characteristics"));}
										//	if(temprad.equals("")){temprad = cur.getString(cur.getColumnIndex("fld_radiators"));}
											System.out.println("UPDATE  "+db.LoadHotwatersystem+" SET  fld_characteristics='"+db.encode(tempchar)+"',fld_hotwatersysNA='"+chk+"'," +
								   					"fld_radiators='"+db.encode(temprad)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
											db.hi_db.execSQL("UPDATE  "+db.LoadHotwatersystem+" SET  fld_characteristics='"+db.encode(tempchar)+"',fld_hotwatersysNA='"+chk+"'," +
								   					"fld_radiators='"+db.encode(temprad)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
					
										}	
										
							 }
							 else
							 {								
										Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
										if(cur.getCount()==0)
										{
											System.out.println(" INSERT INTO "+db.CreateHotwatersystem+" (fld_templatename,fld_inspectorid,fld_characteristics,fld_radiators,fld_hotwatersysNA) VALUES" +
												    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+db.encode(tempchar)+"','"+db.encode(temprad)+"','"+chk+"')");
											db.hi_db.execSQL(" INSERT INTO "+db.CreateHotwatersystem+" (fld_templatename,fld_inspectorid,fld_characteristics,fld_radiators,fld_hotwatersysNA) VALUES" +
												    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+db.encode(tempchar)+"','"+db.encode(temprad)+"','"+chk+"')");
											
										}
										else
										{
											//if(tempchar.equals("")){tempchar = cur.getString(cur.getColumnIndex("fld_characteristics"));}
											//if(temprad.equals("")){temprad = cur.getString(cur.getColumnIndex("fld_radiators"));}
											System.out.println("UPDATE  "+db.CreateHotwatersystem+" SET  fld_characteristics='"+db.encode(tempchar)+"',fld_hotwatersysNA='"+chk+"'," +
								   					"fld_radiators='"+db.encode(temprad)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
											db.hi_db.execSQL("UPDATE  "+db.CreateHotwatersystem+" SET  fld_characteristics='"+db.encode(tempchar)+"',fld_hotwatersysNA='"+chk+"'," +
								   					"fld_radiators='"+db.encode(temprad)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
										}
							 }
						}
						catch (Exception e) 
						{
								// TODO: handle exception
							System.out.println("insi teeeee"+e.getMessage());
						}
							
					cf.DisplayToast("Hot Water system saved successfully");
					Intent intent = new Intent(NewHotwater.this, NewGasLog.class);
			    	Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("tempoption", gettempoption);			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("selectedid", getselectedho);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
					
				}
			});
		
		
		/*TextView vc = ((TextView)findViewById(R.id.vc));
		vc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent = new Intent(NewHotwater.this, NewSetVC.class);
					Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("submodulename", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("tempoption", gettempoption);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
			}
		});*/
		
		LinearLayout menu = (LinearLayout) findViewById(R.id.hvacmenu);
		menu.addView(new Header_Menu(this, 5, cf,gettempname,getcurrentview,getmodulename,gettempoption,getselectedho,db));		
		
		tvchar = (TextView)findViewById(R.id.characteristicssel);
		tvrad = (TextView)findViewById(R.id.radiatorssel);
		
		TextView tvchar = (TextView)findViewById(R.id.characteristics);
		tvchar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				watervalue="Characteristics";k=1;
				multi_select(watervalue,k);				
			}
		});
		
		TextView tvrad = (TextView)findViewById(R.id.radiators);
		tvrad.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				watervalue="Radiators";k=2;
				multi_select(watervalue,k);				
			}
		});
		
		displayhotwatersystem();
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(NewHotwater.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						cf.DefaultValue_Insert(gettempname,getselectedho);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	private void displayhotwatersystem() {
		// TODO Auto-generated method stub
		String radiators="",characteristics="",hotwatersysNA="";
		Cursor cur;
		if(getcurrentview.equals("start"))
		{
			 cur= db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);	
		}
		else
		{
			 cur= db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
		}System.out.println("cur="+cur.getCount());
		
		 if(cur.getCount()>0)
		 {
			 cur.moveToFirst();
			    try
				{
			    	radiators=db.decode(cur.getString(cur.getColumnIndex("fld_radiators")));
			    	characteristics=db.decode(cur.getString(cur.getColumnIndex("fld_characteristics")));
			    	hotwatersysNA=cur.getString(cur.getColumnIndex("fld_hotwatersysNA"));System.out.println("HOTWATER="+hotwatersysNA);
			    	
			    	if(!radiators.equals(""))
			    	{
			    		tvchar.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+radiators));
			    		tvchar.setVisibility(v.VISIBLE);
			    	}
			    	
			    	if(!characteristics.equals(""))
			    	{
			    		tvrad.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+characteristics));
			    		tvrad.setVisibility(v.VISIBLE);
			    	}
			    	if(hotwatersysNA.equals("1"))
					{
						chknotapplicable.setChecked(true);
						((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE); 		
					}
					else
					{
						chknotapplicable.setChecked(false);
						((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
					}
				}
			 	catch (Exception e) {
					// TODO: handle exception
				}
		}
	}
	private void multi_select(final String watervalue, int s) {
		// TODO Auto-generated method stub
		id=1;
		dbvalue(watervalue);
		final Dialog add_dialog = new Dialog(NewHotwater.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);

		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);
		
		showoptions();
		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(watervalue);
				
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				seltdval = "";
				for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						seltdval += ch[i].getText().toString() + "&#44;";
					}
				}
				
				finalselstrval = "";
				finalselstrval = seltdval;
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{	
					if(watervalue.equals("Characteristics"))
					{
						charvalue=finalselstrval;
						tempchar = charvalue;System.out.println(tempchar);
						tempchar = cf.replacelastendswithcomma(tempchar);System.out.println(tempchar);
						charselected_display();
					}
					else if(watervalue.equals("Radiators"))
					{
						radvalue=finalselstrval;
						temprad = radvalue;
						temprad = cf.replacelastendswithcomma(temprad);
						radselected_display();
					}
					add_dialog.cancel();
					id=0;	
				}	
				
				
			}
				
		});
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    id=0;
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);		
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				  Other_dialogshow(watervalue);
				
			}
		});
		
		add_dialog.setCancelable(false);
		add_dialog.show();
		setvalue(watervalue);
		
	}
	
	protected void Other_dialogshow(final String watervalue2) {
		// TODO Auto-generated method stub
		nid=1;
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.addnewcustom);
		
		etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
		cf.hidekeyboard(etother);
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				nid=0;
				add_dialog.dismiss();
			}
		});
		
		Button btnsave = (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (etother.getText().toString().trim().equals("")) {
					cf.DisplayToast("Please enter the other text");				
				} else {
					
					if(str_arrlst.contains(etother.getText().toString().trim()))
					{
						cf.DisplayToast("Already exists!! Please enter the new text");
						etother.setText("");
						
						
						etother.requestFocus();
				
					}
					
					else
					{
						try
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.SaveAddOther
									+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_ot" +
									"her,fld_templatename) VALUES"
									+ "('"+db.UserId+"','"+getselectedho+"','"+getmodulename+"','"+db.encode(watervalue2)+"','"
									+ db.encode(etother.getText().toString()) + "','"+db.encode(gettempname)+"')");
							
							String addedval = db.encode(etother.getText().toString().trim());
							str_arrlst.add(addedval);
							nid=0;
							cf.DisplayToast("Added successfully");
							add_dialog.cancel();
							
							lin.removeAllViews();
							dbvalue(watervalue2);
							showoptions();
							setvalue(watervalue2);
						
							
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
		
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}

	protected void setvalue(String watervalue2) {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur;
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
				
			System.out.println("count"+cur.getCount());
		    if (cur.getCount() > 0) 
		    {
				cur.moveToFirst();
				str="";
				if(watervalue2.equals("Characteristics"))
				{
						str = db.decode(cur.getString(cur.getColumnIndex("fld_characteristics")));	
				}
				else if(watervalue2.equals("Radiators"))
				{
						str = db.decode(cur.getString(cur.getColumnIndex("fld_radiators")));
				}
				
				if(str.contains(","))
			    {
			    	str = str.replace(",","&#44;");
			    }	
			    
				
			}
		    else
		    {
			
		    	if(watervalue2.equals("Characteristics") && charvalue!=null)
				{
					str = tempchar;							 
				}
				else if(watervalue2.equals("Radiators") && radvalue!=null)
				{
					str = temprad;		
				}
		    }
		    
			    if(!str.trim().equals(""))
				{
					cf.setValuetoCheckbox(ch, str);
				}	
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	protected void radselected_display() {
		// TODO Auto-generated method stub
		tvrad.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+temprad));
		tvrad.setVisibility(v.VISIBLE);
	}

	protected void charselected_display() {
		// TODO Auto-generated method stub
		tvchar.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempchar));
		tvchar.setVisibility(v.VISIBLE);
	}

	private void showoptions() {
		// TODO Auto-generated method stub
		ch = new CheckBox[str_arrlst.size()];		
		for (int i = 0; i < str_arrlst.size(); i++) {
        		ch[i] = new CheckBox(this);
				ch[i].setText(str_arrlst.get(i));
				ch[i].setTextColor(Color.BLACK);
				lin.addView(ch[i]);
				
				ch[i].setOnCheckedChangeListener(new CHListener(i));
		}
	}
	
	class CHListener implements OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		
			
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
			}
			
			
			System.out.println("TOTAL="+seltdval);
		}
		
	}

	private void dbvalue(String watervalue2) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		if(watervalue2.equals("Characteristics"))
		{
			for(int j=0;j<hotchar.length;j++)
			{
				str_arrlst.add(hotchar[j]);
			}	
		}
		if(watervalue2.equals("Radiators"))
		{
			for(int j=0;j<radiators.length;j++)
			{
				str_arrlst.add(radiators[j]);
			}	
		}
		
		try
		{
			 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(watervalue2)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"'",null);
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =0;i<c.getCount();i++,c.moveToNext())
				 {
					 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
				 }
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	protected void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(NewHotwater.this, NewThermostat.class);
    	Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		Bundle b6 = new Bundle();
		b6.putString("selectedid", getselectedho);			
		intent.putExtras(b6);
		startActivity(intent);
		finish();
       
	}
	
	protected void onSaveInstanceState(Bundle ext)
	{		
		ext.putInt("chkvalue", chk);
		ext.putString("watervalue", watervalue);
		ext.putInt("idvalue", id);
		ext.putInt("kvalue", k);
		ext.putString("checkedvalue",seltdval);
		ext.putString("chardata",tempchar);
		ext.putString("raddata",temprad);
		ext.putString("charvalue",charvalue);
		ext.putString("radvalue",radvalue);
		ext.putInt("nidvalue", nid);
		
		if(nid==1)
		{
			ext.putString("newtext", etother.getText().toString());
		}
		
		super.onSaveInstanceState(ext);
	}
	protected void onRestoreInstanceState(Bundle ext)
	{
		id = ext.getInt("idvalue");
		k = ext.getInt("kvalue");
		watervalue = ext.getString("watervalue");
		charvalue = ext.getString("charvalue");
		radvalue = ext.getString("radvalue");
		nid = ext.getInt("nidvalue");
		
		if(id==1)
		{
			multi_select(watervalue, k);
			seltdval = ext.getString("checkedvalue");
			
			if(!seltdval.trim().equals(""))
			{
				cf.setValuetoCheckbox(ch, seltdval);
			}
		}
		
		 tempchar = ext.getString("chardata");
		 if(!tempchar.equals("")&&!tempchar.equals("--Select--"))
		 {
			 tempchar = cf.replacelastendswithcomma(tempchar);
			 charselected_display();
		 }
		 
		 temprad = ext.getString("raddata");
		 if(!temprad.equals("")&&!temprad.equals("--Select--"))
		 {
			 temprad = cf.replacelastendswithcomma(temprad);
			 radselected_display();
		 }
		 
		 if(nid==1)
		 {
			 Other_dialogshow(watervalue);
			 
			 String strnewtext = ext.getString("newtext");
			 etother.setText(strnewtext);
		 }
		 chk = ext.getInt("chkvalue");	
			
			if(chk==1)
			{
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
			}
			else
			{
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
			}
			
		 super.onRestoreInstanceState(ext);
	}
}
