package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.LoginActivity.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;



public class HomeScreen extends Activity {
	Button btncreatetemplate,btnaddvc,btnplaceorder,btneditprofile,btnstartinspection,btncompleted,btnreporttoit,btncheckforudpates,
	btnreset,btnlogout;
	CommonFunction cf;
	DatabaseFunction db;
	Webservice_config wb;
	String oldpass="",newpass="",confpass="",username="",password="",userid="",resetpassword="";
	int resetpwd=0;
	Dialog restdialog;
	Progress_staticvalues p_sv;
	EditText oldpassword,newpassword,confirmpassword;
	private int handler_msg=3;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		wb = new Webservice_config(this);
		
         setContentView(R.layout.activity_home);
     
		 declaration();
	}
	private void declaration() {
		// TODO Auto-generated method stub
		db.userid();
		
		Typeface custom_font = Typeface.createFromAsset(getAssets(),"fonts/squadaone-regular.ttf");
		
		btncreatetemplate = (Button)findViewById(R.id.btn_createtemplate);
	    btncreatetemplate.setTypeface(custom_font);
		btncreatetemplate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(HomeScreen.this,CreateTemplate.class));
				finish();
			}
		});
		
		btnaddvc = (Button)findViewById(R.id.btn_vc);
		btnaddvc.setTypeface(custom_font);
		btnaddvc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(HomeScreen.this,AddVC.class));
				finish();
			}
		});
		
		btnplaceorder = (Button)findViewById(R.id.btn_placeorder);
		btnplaceorder.setTypeface(custom_font);
		btnplaceorder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(HomeScreen.this,PlaceOrder.class));
				finish();
			}
		});
		
		btneditprofile = (Button)findViewById(R.id.btn_editprofile);
		btneditprofile.setTypeface(custom_font);
		btneditprofile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent(HomeScreen.this,Registration.class);
				Bundle b1 = new Bundle();
				b1.putString("userid", db.UserId);			
				in.putExtras(b1);
				startActivity(in);
				finish();
			}
		});
		
		btnstartinspection = (Button)findViewById(R.id.btn_startinspection);
		btnstartinspection.setTypeface(custom_font);
		btnstartinspection.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Intent in =new Intent(HomeScreen.this,StartInspection.class);
				Intent in =new Intent(HomeScreen.this,Dashboard.class);
				//in.putExtra("inspectorid", db.UserId);
				startActivity(in);
				finish();
			}
		});
		
		btncompleted = (Button)findViewById(R.id.btn_completedinspection);
		btncompleted.setTypeface(custom_font);
		btncompleted.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in =new Intent(HomeScreen.this,CompletedInspection.class);
				in.putExtra("inspectorid", db.UserId);
				startActivity(in);
				finish();
			}
		});
		
		btnreset = (Button)findViewById(R.id.btn_resetpassword);
		btnreset.setTypeface(custom_font);
		btnreset.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			      startActivity(new Intent(HomeScreen.this,ResetPassword.class));
			      finish();
			}
		});
		
		btnlogout = (Button)findViewById(R.id.btn_logout);
		btnlogout.setTypeface(custom_font);
		btnlogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					db.hi_db.execSQL("UPDATE "+ db.Registration +"  SET fld_logout='2' where fld_userid='"+db.UserId+"'");
					System.out.println("UPDATE "+ db.Registration +"  SET fld_logout='2' where fld_userid='"+db.UserId+"'");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
	            startActivity(new Intent(HomeScreen.this, LoginActivity.class));
	          //  overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
			}
		});
		
		btnreporttoit = (Button)findViewById(R.id.btn_reporttoit);
		btnreporttoit.setTypeface(custom_font);
		btnreporttoit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
	            startActivity(new Intent(HomeScreen.this, ReporttoIT.class));
	          //  overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
			}
		});
		
		
		try
		{
			Cursor cur1 = db.hi_db.rawQuery("select * from " + db.Registration + " where fld_userid='"+db.UserId+"'",null);
			System.out.println("select * from " + db.Registration + " where fld_userid='"+db.UserId+"'");
			if(cur1!=null)
			{
				cur1.moveToFirst();
				String stragentfirstname = db.decode(cur1.getString(cur1.getColumnIndex("fld_firstname")));
				stragentfirstname += " "+db.decode(cur1.getString(cur1.getColumnIndex("fld_lastname")));
				String stragentphone = db.decode(cur1.getString(cur1.getColumnIndex("fld_companyphone")));
				String stragentemail = db.decode(cur1.getString(cur1.getColumnIndex("fld_companyemail")));
				String stragentcompany = db.decode(cur1.getString(cur1.getColumnIndex("fld_companyname")));
				String stragentheadshot=db.decode(cur1.getString(cur1.getColumnIndex("fld_headshot")));
				String stragencylogo=db.decode(cur1.getString(cur1.getColumnIndex("fld_companylogo")));
				System.out.println("stragentheadshot="+stragentheadshot);
				
				if(!stragentfirstname.equals(""))
				{
				   ((TextView)findViewById(R.id.homescreen_tvagentfirstname)).setText(stragentfirstname);
				}
				
				if(!stragentfirstname.equals(""))
				{
				   ((TextView)findViewById(R.id.homescreen_tvname)).setText(stragentfirstname);
				}
				
				if(!stragentphone.equals(""))
				{
				   ((TextView)findViewById(R.id.homescreen_tvagentcontactno)).setText(stragentphone);
				}
				System.out.println("stragentemail="+stragentemail);
				if(!stragentemail.equals(""))
				{
				  ((TextView)findViewById(R.id.homescreen_tvagentemail)).setText(stragentemail);
				}
				
				if(!stragentcompany.equals(""))
				{
				  ((TextView)findViewById(R.id.homescreen_tvagentcompanyname)).setText(stragentcompany.toUpperCase());
				}
				
				if(!stragentheadshot.equals(""))
				{
					try
				    {
						String FILENAME = "HIHeadshot"+db.UserId +".png";
						File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/"+FILENAME);
						System.out.println("outputFile.exists()"+outputFile.exists());
						if(outputFile.exists())
						{
							BitmapFactory.Options o = new BitmapFactory.Options();
							o.inJustDecodeBounds = true;
							BitmapFactory.decodeStream(new FileInputStream(outputFile),null, o);
							final int REQUIRED_SIZE = 200;
							int width_tmp = o.outWidth, height_tmp = o.outHeight;
							int scale = 1;
							while (true) {
								if (width_tmp / 2 < REQUIRED_SIZE|| height_tmp / 2 < REQUIRED_SIZE)
									break;
								width_tmp /= 2;
								height_tmp /= 2;
								scale *= 2;
							}
							BitmapFactory.Options o2 = new BitmapFactory.Options();
							o2.inSampleSize = scale;
							Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
									outputFile), null, o2);
							BitmapDrawable bmd = new BitmapDrawable(bitmap);
							((ImageView)findViewById(R.id.homescreen_ivheadshot)).setImageDrawable(bmd);
						}
						else
						{
							stragentheadshot="";
						}
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exception in agencylogo is "+e.getMessage());
					}
				}
				
				
				if(!stragencylogo.equals(""))
				{
					try
					{
						String FILENAME = "HILogo"+db.UserId +".png";
						File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/"+FILENAME);
						System.out.println("logofileexists"+outputFile.exists());
						if(outputFile.exists())
						{
							BitmapFactory.Options o = new BitmapFactory.Options();
							o.inJustDecodeBounds = true;
							BitmapFactory.decodeStream(new FileInputStream(outputFile),null, o);
							final int REQUIRED_SIZE = 200;
							int width_tmp = o.outWidth, height_tmp = o.outHeight;
							int scale = 1;
							while (true) {
								if (width_tmp / 2 < REQUIRED_SIZE|| height_tmp / 2 < REQUIRED_SIZE)
									break;
								width_tmp /= 2;
								height_tmp /= 2;
								scale *= 2;
							}
							BitmapFactory.Options o2 = new BitmapFactory.Options();
							o2.inSampleSize = scale;
							Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(outputFile), null, o2);
							BitmapDrawable bmd = new BitmapDrawable(bitmap);
							((ImageView)findViewById(R.id.homescreen_ivlogo)).setImageDrawable(bmd);
						}
						else
						{
							stragencylogo="";
						}
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exception in agencylogo is "+e.getMessage());
					}
				}
				
			}
		 
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	
    
    private static Bitmap decodeFile(String file) {
	    try {
	    	
	    	File f=new File(file);
	    	
	        // Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

	        // The new size we want to scale to
	        final int REQUIRED_SIZE = 150;

	        // Find the correct scale value. It should be the power of 2.
	        int scale = 1;
	        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
	                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
	            scale *= 2;

	        // Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize = scale;
	        o.inJustDecodeBounds = false;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {
	    }

	    return null;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		finish();
	}

}
