package idsoft.idepot.homeinspection;

import java.util.ArrayList;

import idsoft.idepot.homeinspection.LoadNewSetVC.EfficientAdapter;
import idsoft.idepot.homeinspection.LoadNewSetVC.oncheckedlistener;
import idsoft.idepot.homeinspection.LoadNewSetVC.vcclicklistener;
import idsoft.idepot.homeinspection.LoadNewSetVC.EfficientAdapter.ViewHolder;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;



public class Summary extends Activity{
	
	DatabaseFunction db;
	CommonFunction cf;
	String getselectedho="",inspectorid="",str_vccond="",str_cbval="";
	View v;
	LinearLayout li_content,li_values;
	ListView vclist;
	TextView txt1,txt2;
	LinearLayout.LayoutParams lp1;
	String arrstr_vccond[],cbval[],value[],selarr[];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b = this.getIntent().getExtras();
		if(b!=null)
		{
		  getselectedho = b.getString("srid");
		  inspectorid = b.getString("inspectorid");
		}
		setContentView(R.layout.summary);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		db.CreateTable(1);
		db.CreateTable(2);
		db.CreateTable(31);
		db.userid();
		
		declaration();
		Show_subsection_info();
		//setvc();
	}
 
	private void Show_subsection_info() {
		// TODO Auto-generated method stub
		li_content.removeAllViews();
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int wd = metrics.widthPixels;
		int ht = metrics.heightPixels;			
		int totalwith = wd-30;
		int totalheight =  ht-150;
		totalwith = totalwith/4;
		int rem = wd-totalwith;
		int width = wd/2;
		
		
		ScrollView sv = new ScrollView(this);
		li_content.addView(sv);
		
		LinearLayout.LayoutParams lpsub = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, totalheight);
		lpsub.setMargins(10,10,10,0);
		li_content.setLayoutParams(lpsub);			
		
		final LinearLayout lin1 = new LinearLayout(this);
		lin1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(lin1);
		
		LinearLayout.LayoutParams lparams1 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		lparams1.setMargins(5,5,5,5);	
		li_content.setMinimumWidth(totalwith);
		db.CreateTable(13);	
		for(int i=0;i<cf.strarr_module.length;i++)
		{
			
			li_values = new LinearLayout(this);
			li_values.setMinimumHeight(30);
			li_values.setLayoutParams(lparams1);
			
			Cursor cur1 = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_srid='"+getselectedho+"' and fld_module='"+cf.strarr_module[i]+"' and fld_inspectionpoint like '%"+db.encode("Include In Summary") + "%'", null);
			System.out.println("select * from " + db.SaveSetVC + " WHERE fld_srid='"+getselectedho+"' and fld_module='"+cf.strarr_module[i]+"' and fld_inspectionpoint like '%"+db.encode("Include In Summary") + "%'"+cur1.getCount());
			
			if(cur1.getCount()>0)
			{
				lp1 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
				lp1.setMargins(10, 10, 10,10);
				li_values.setOrientation(LinearLayout.VERTICAL);
			    txt1= new TextView(this,null,R.attr.n_pre_layout_Text_view);
			    String source = "<b><font color=#8A0829>MODULE NAME : "+ cf.strarr_module[i]+"</font></b>";
			    txt1.setText(Html.fromHtml(source));
			    lin1.addView(li_values);
				li_values.addView(txt1,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			
			cf.GetCurrentModuleName(cf.strarr_module[i]);
			for(int k=0;k<cf.arrsub.length;k++)
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_srid='"+getselectedho+"' and fld_module='"+cf.strarr_module[i]+"' and fld_submodule='"+cf.arrsub[k]+"' and fld_inspectionpoint like '%"+db.encode("Include In Summary") + "%'", null);
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
					txt2= new TextView(this,null,R.attr.n_pre_layout_Text_view);
					String source1 = "<b><font color=#0B3B24>SUB MODULE NAME : "+ db.decode(cur.getString(cur.getColumnIndex("fld_submodule")))+"</font></b>";
				    txt2.setText(Html.fromHtml(source1));
					
		    		li_values.addView(txt2,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);	    		

		    		LinearLayout.LayoutParams lp5 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
					lp5.setMargins(0,10,0,10);
					txt2.setLayoutParams(lp5);
					
		    		for(int j=0;j<cur.getCount();j++)
					{
		    			String mod = db.decode(cur.getString(cur.getColumnIndex("fld_module")));
		    			String sub_mod = db.decode(cur.getString(cur.getColumnIndex("fld_submodule")));
		    			String primarycondition = db.decode(cur.getString(cur.getColumnIndex("fld_primary")));
		    					    			
		    			String secondarycondition = db.decode(cur.getString(cur.getColumnIndex("fld_secondary")));
		    			secondarycondition = Html.fromHtml(secondarycondition).toString();
		         		if(secondarycondition.endsWith(","))
		         		{
		         			secondarycondition = secondarycondition.substring(0,secondarycondition.length()-1);
		         		}
		         		
		    			String comments = db.decode(cur.getString(cur.getColumnIndex("fld_comments")));
		    			String inspectionpoint = db.decode(cur.getString(cur.getColumnIndex("fld_inspectionpoint")));
		    			inspectionpoint = Html.fromHtml(inspectionpoint).toString();
		         		if(inspectionpoint.endsWith(","))
		         		{
		         			inspectionpoint = inspectionpoint.substring(0,inspectionpoint.length()-1);
		         		}
		         		
		    			String approxrepaircost = db.decode(cur.getString(cur.getColumnIndex("fld_apprrepaircost")));
		    			String contractortype = db.decode(cur.getString(cur.getColumnIndex("fld_contractortype")));
		    			contractortype = Html.fromHtml(contractortype).toString();
		         		if(contractortype.endsWith(","))
		         		{
		         			contractortype = contractortype.substring(0,contractortype.length()-1);
		         		}		         		 
		         		//***Start showing the Primary Condition***//*
		         		
		         		LinearLayout li_overall = new LinearLayout(this);	         			
		         		li_overall.setOrientation(LinearLayout.VERTICAL);
		         		LinearLayout.LayoutParams loverall = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		loverall.setMargins(0, 20, 0,0);
		         		
		         		LinearLayout li_prim = new LinearLayout(this);	         			
		         		li_prim.setOrientation(LinearLayout.HORIZONTAL);
		         		
		         		TextView txtprimary= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtprimary.setText(Html.fromHtml("<b>Primary Condition</b>"));
		         		li_prim.addView(txtprimary,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtprimarycol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtprimarycol.setText(":");
		         		li_prim.addView(txtprimarycol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtprimaryvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtprimaryvalue.setText(primarycondition);
		         		li_prim.addView(txtprimaryvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		li_overall.addView(li_prim, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
		         		//***Start showing the Secondary Condition***//*
		         		
		         		LinearLayout li_sec = new LinearLayout(this);	         			
		         		li_sec.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtsec= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtsec.setText(Html.fromHtml("<b>Secondary Condition</b>"));
		         		li_sec.addView(txtsec,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtseccol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtseccol.setText(":");
		         		li_sec.addView(txtseccol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtsecvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(secondarycondition.equals("")){secondarycondition="N/A";}
		         		txtsecvalue.setText(secondarycondition);
		         		li_sec.addView(txtsecvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		li_overall.addView(li_sec, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
				    	
		         		
		         		//***Start showing the comments***//*
		         		
		         		
		         		LinearLayout li_comm = new LinearLayout(this);        			
		         		li_comm.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtcomm= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtcomm.setText(Html.fromHtml("<b>Comments</b>"));
		         		li_comm.addView(txtcomm,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtcommcol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtcommcol.setText(":");
		         		li_comm.addView(txtcommcol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtcommvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(comments.equals("")){comments="N/A";}
		         		txtcommvalue.setText(comments);
		         		li_comm.addView(txtcommvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		
		         		li_overall.addView(li_comm, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
		         		
		         		
		         		//***Start showing the inspectionpoint***//*
		         		
		         		
		         		LinearLayout li_insppoint = new LinearLayout(this);        			
		         		li_insppoint.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtinspoint= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtinspoint.setText(Html.fromHtml("<b>Inspection Point</b>"));
		         		li_insppoint.addView(txtinspoint,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtinspointcol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtinspointcol.setText(":");
		         		li_insppoint.addView(txtinspointcol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtinspvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(inspectionpoint.equals("")){inspectionpoint="N/A";}
		         		txtinspvalue.setText(inspectionpoint);
		         		li_insppoint.addView(txtinspvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		li_overall.addView(li_insppoint, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
		         		//***Start showing the approxmiate cost***//*
		         		
		         		
		         		LinearLayout li_approst = new LinearLayout(this);        			
		         		li_approst.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtappcost= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtappcost.setText(Html.fromHtml("<b>Approximate Repair Cost</b>"));
		         		li_approst.addView(txtappcost,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtapprcol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtapprcol.setText(":");
		         		li_approst.addView(txtapprcol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtapprvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(approxrepaircost.equals("")){approxrepaircost="N/A";}
		         		txtapprvalue.setText(approxrepaircost);
		         		li_approst.addView(txtapprvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		li_overall.addView(li_approst, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
		         		
		         		
		         		
		         		//***Start showing the contractor type***//*
		         		
		         		
		         		LinearLayout li_contype = new LinearLayout(this);        			
		         		li_contype.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtcontype= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtcontype.setText(Html.fromHtml("<b>Contractor Type</b>"));
		         		li_contype.addView(txtcontype,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtcontypecol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtcontypecol.setText(":");
		         		li_contype.addView(txtcontypecol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtcontypevalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(contractortype.equals("")){contractortype="N/A";}
		         		txtcontypevalue.setText(contractortype);
		         		li_contype.addView(txtcontypevalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		
		         		li_overall.addView(li_contype, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		li_values.addView(li_overall);
		         		li_overall.setLayoutParams(loverall);
				   
	        			RelativeLayout li_values1 =new RelativeLayout(this);   			
	        		
		         		Cursor cur2 = db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getselectedho+"' and fld_module='"+mod+"' and fld_submodule='"+sub_mod+"' and fld_vc='"+db.encode(primarycondition)+"'", null);
		         		if(cur2.getCount()>0)
						{		         			
							cur2.moveToFirst();
							int im_id=0,cap_id=0;
							for(int m =0;m<cur2.getCount();m++,cur2.moveToNext())
							{
								 String imagename = db.decode(cur2.getString(cur2.getColumnIndex("fld_image")));
								 String caption = db.decode(cur2.getString(cur2.getColumnIndex("fld_caption")));
								 Bitmap bm=cf.ShrinkBitmap(imagename, 200, 200);
								 ImageView im=new ImageView(this,null,R.attr.n_pre_layout_imag);
								 im.setImageBitmap(bm);
								 li_values1.addView(im,200,200);
								 im.setId((i+m)+1);
								 System.out.println("im_id="+im.getId()+"what is ="+im_id); 
								 RelativeLayout.LayoutParams lp =(RelativeLayout.LayoutParams) im.getLayoutParams();
								 lp.setMargins(10, 45, 0, 0);
								 
								 if(im_id!=0)// Check the is there any previous image added for this question 
								 {
								
									 if((m%2)!=0 || m ==1) 
									 {
										 lp.addRule(RelativeLayout.BELOW,cap_id-1); // If yes mean we set to right of previous one
										 lp.addRule(RelativeLayout.RIGHT_OF, im_id);
									 }
									 else
									 {
										 lp.addRule(RelativeLayout.BELOW,cap_id);
									 }
								 }
								 System.out.println("totalwith"+totalwith);
								 
							 lp.width=width-40;
							 //lp.height=350;
							 im.setLayoutParams(lp);
							 im_id=im.getId();
							 TextView tv = new TextView(this,null,R.attr.n_pre_layout_Text_view);
							 tv.setGravity(Gravity.CENTER_HORIZONTAL);
							 tv.setText(caption);
							 System.out.println("totalwidth="+wd);
							 li_values1.addView(tv,200,LayoutParams.WRAP_CONTENT);
							 RelativeLayout.LayoutParams rl_p=(android.widget.RelativeLayout.LayoutParams) tv.getLayoutParams();
							 if(im_id!=0)
								 {
									rl_p.addRule(RelativeLayout.BELOW,im_id);
									if((m%2)!=0 || m ==1)
										rl_p.addRule(RelativeLayout.RIGHT_OF, im_id-1);
								 }
							 
							 tv.setId(1000+i+j);
							 tv.setLayoutParams(rl_p);
							 rl_p.width=width-40;
							 cap_id=im.getId();// We use this id to set the relative rule
							 tv.setLayoutParams(rl_p);
							 li_values1.setLayoutParams(lp);
							}
							li_values.addView(li_values1, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
						}	         		
		         			         		
				    	cur.moveToNext();
					}		    		
				}
			}
			}
			
		}		
	
	}
	
	private void setvc() {
		// TODO Auto-generated method stub
		str_vccond="";		
		Cursor cur = db.hi_db.rawQuery("select DISTINCT fld_primary from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_inspectionpoint like '%"+db.encode("Include In Summary") + "%'", null);
		if(cur.getCount()>0)
        {
			cur.moveToFirst();	
			for(int i=0;i<cur.getCount();i++,cur.moveToNext())
        	{
				str_vccond += db.decode(cur.getString(cur.getColumnIndex("fld_primary")))+"~";
				arrstr_vccond = str_vccond.split("~");
				str_cbval += "unchecked"+"~";
				cbval = str_cbval.split("~");
            	vclist.setAdapter(new EfficientAdapter(cbval,arrstr_vccond,this));					
        	}					
        }
	}
	class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
		
		public EfficientAdapter(String[] cbval, String[] arrstrchk, Context context) {
			mInflater = LayoutInflater.from(context);
			value=cbval;
			selarr = arrstrchk;
		}

		public int getCount() {
			int arrlen = 0;
			arrlen = arrstr_vccond.length;
			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			

					    holder = new ViewHolder();
						convertView = mInflater.inflate(R.layout.summaryvclist, null);						
						holder.tveditvc = (TextView)convertView.findViewById(R.id.cb_vcname);
						
						convertView.setTag(holder);
						
						 holder.tveditvc.setText(Html.fromHtml(arrstr_vccond[position]));
						
			return convertView;
		}

		 class ViewHolder {
			TextView tveditvc;
		   
		}

	}
	private void declaration() {
		// TODO Auto-generated method stub
		li_content=(LinearLayout) findViewById(R.id.pre_li_content);
		vclist = (ListView)findViewById(R.id.vclist);		
		((Button)findViewById(R.id.btn_back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Summary.this, StartInspection.class);
				i.putExtra("inspectorid",inspectorid);
				//i.putExtra("selectedid",getselectedho);
				startActivity(i);
				finish();
			}
		});
	}
	@Override
    protected void onSaveInstanceState(Bundle outState) {	 
	  
        outState.putString("inspectorid", inspectorid);
        
        
        super.onSaveInstanceState(outState);
    }
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	inspectorid = savedInstanceState.getString("inspectorid");
    	 
        super.onRestoreInstanceState(savedInstanceState);
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(Summary.this, StartInspection.class);
			i.putExtra("inspectorid",inspectorid);
			//i.putExtra("selectedid",getselectedho);
			startActivity(i);
			finish();
		}

		return super.onKeyDown(keyCode, event);
	}
}