package idsoft.idepot.homeinspection;



import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.IOException;
import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoadBathroom extends Activity{
	 String bathno="",bathname="",bathnameother="",tubs="",sinks="",bathfixture="",showerenclosures="",finalselstrval="",seltdval="",str="",tubtv,showerdb,fixturesdb,sinkdb,toiletdb,otherfeaturesdb="",
			 shower="",othersafetyglass="",safetyglass="",toilet="",drainage="",otherdrainage="",bathother="",otherfeatures="",comments="",strother="",temptub="",tempsink="",
			 tempbathroom="",temptoilet="",tempbathfixtures="",tempotherfeatures="",safetyvalue="",tempsafety="",watersupplyvalue="",tempwatersupply="",othervalue="",tempother="";
	 CheckBox ch[];
	 Progress_staticvalues p_sv;
	 Webservice_config wb;
	 static int handler_msg=2;
	 ScrollView scr;
	 int chk =0,spintouch=0;
	 Spinner spinloadtemplate;
	 Cursor selcur;
	 Intent intent;
	   static String[] bathtubsarr ={"Standard Tub","Whirlpool Tub","Whirlpool Access Panel","Whirlpool Access Panel Sealed","No Access Under Tub","Shower Enclosure(s)","Tub �Shower Enclosure","Metal Faucets","Plastic Faucets","Recent Repairs/Upgrades Noted (Check w/Seller)"};
	    static String[] bathsinksarr ={"Wall Mounted Sink","Floor Mounted Sink","Cabinet Mounted Sink(s)","Metal Faucets","Plastic Faucets","Shut Off Valves Present Under Each Sink","Missing shut off valves","No Active Leakage Noted Under Fixtures","Signs of Past Leakage Noted to Cabinets","Damaged Cabinets/Counter Tops","Recent Repairs/Upgrades Noted (Check w/Seller)"};
	    static String[] bathtoiletarr={"Floor Mounted","Wall Mounted","Bidet Present","Shut Off Valve Present","Cistern Free of Leakage/Damage","No Stains At Base Of Toilet","Flushing Mechanism Operable"};
	    static String[] bathfixturesarr = {"Metal � Enamel Cast","Fiberglass","Plastic","Tile","Granite","Marble Type","Corian"};
	  	String gettempoption="",tubsvalue = "", sinkvalue = "", showersvalue="", toiletsvalue="", fixturesvalue="",getcurrentview="", otherfeaturesvalue="",getbathnameother="",getsafetyshowerother="";
	String gettempname="",getph="",getmodulename="",getbathname="--Select--",getdrainageother="",bathvalue="",selectedval="",
			getsafetyshower="--Select--",getglass="--Select--",getdrainage="--Select--",getbathother="--Select--";
	Spinner spin_bathname,spin_bathshowerenclosure,spin_watersupply,spin_bathother;
	static String[] bathnamearr ={"--Select--","All Bathroom(s)","Hall Bathroom","Master Bathroom","Jack & Jill Bathroom","Half Bathroom","Pool Bathroom","Guest Bathroom","Other"};
	static String[] fixturearr ={"--Select--","Standard Tub","Whirlpool Tub","Tub �Shower Enclosure","No Access Under Tub","Whirlpool Access Panel Sealed",
                                "Metal Faucet","Plastic Faucet","Shower Enclosure(s)","Basin(s)","Pedestal","Wall Mounted","Floor Mounted","Bidet(s)","Sauna",
                                "Steam Bath","Auxiliary Heater","Fan(s)"};
	static String[] safetyglassarr={"Safety Glass Verified","Safety Glass Not Verified","Safety Glass Not Confirmed Due To Visibility issues","Tile Repairs Noted (Check With Seller)","Past Grout Repairs-Check(W/Seller)","Shower Pan(s) Filled � No Leakage Noted"};
	static String[] watersupplyarr={"Shut Off Valves Present Under Each Sink","No Active Leakage Noted Under Fixtures","Signs of Past Leakage Noted to Cabinets",
                                   "Damaged Cabinets/Counter Tops","Recent Repairs/Upgrades Noted (Check w/Seller)"};
	static String[] bathotherarr={"Large Opening Within Shower Stall","Tile Repairs(Check With Seller)","Past Group Repairs-Check(W/Seller)",
								 "Shower Pan(s) Filled � No Leakage Noted","Air Admittance Valves","No Access Under Tub","More Recent TileRepairs(CheckWithSeller)",
                                "No Sill Slopes To Vulnerable Areas","Damaged Cabinets/Counter Tops"};
	static String[] otherfeaturesarr={"Steam Bath","Auxiliary Heater","Exhaust Fan(s)","Sauna"};
	static String[] bathshowerenclosuresarr={"Not Applicable","Standard Fibreglass Enclosure","Tile Enclosure with Lighting/Glazing","Tile Enclosure","Metal Shower Pan","Granite Enclosure","Marble Enclosure","Sliding Door","Side Hung Door","Sloped Floor To Drain","Metal Shower Control","Plastic Shower Control","Sloped Sills present","No Sill Slopes to vulnerable areas","Large opening Within Shower Spray"};
	CommonFunction cf;
	String arrtempname[];
	Button btnback,btnhome,btnsubmit,btntakephoto;
	String error_msg="",title="Submit Template",tempname="";
	ArrayAdapter adapter;
	View v1;
	TextView tvtub,tvsinks,tvshowers,tvtoilets,tvfixtuers,tvbathother,tv_modulename,tvtubnote,tvsinknote,tvbathnote,tvtoiletnote,tvbathfixturesnote,tvotherfeatures,bathsafetytv,
	tvsafety,watersupplytv,bathothertv,tvwatersupply,tvother;
	Dialog dialog;
	ArrayAdapter bathnamearrayadap,bathenclosurearrayadap,bathglassenclosurearrayadap,watersupplyarrayadap,bathotherarrayadap;
	
	EditText ed_otherbathroomname,ed_safetyglass,ed_waterdrainage,ed_drainage,et_bathcomments,etother;
	TableLayout bathtblshow;
	View v;
	int roomCurrent_select_id,vrint=0,editid=0,getid=0,getisedit=0,m=0,bath=0;
	LinearLayout lin;
	static int flag=0,k=0,dialog_other=0;
	DatabaseFunction db;
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getph = b1.getString("selectedid");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");	 
		Bundle b3 = this.getIntent().getExtras();
		getid = b3.getInt("getid");	
		Bundle b4 = this.getIntent().getExtras();
		getisedit = b4.getInt("isedit");
		Bundle b5 = this.getIntent().getExtras();
		getcurrentview = b5.getString("currentview");	
		Bundle b6 = this.getIntent().getExtras();
		if(b6!=null){tubsvalue = b6.getString("tubs");}
		Bundle b7 = this.getIntent().getExtras();
		sinkvalue = b7.getString("sinks");
		Bundle b8 = this.getIntent().getExtras();
		showersvalue = b8.getString("shower");
		Bundle b9 = this.getIntent().getExtras();
		toiletsvalue = b9.getString("toilets");
		Bundle b10 = this.getIntent().getExtras();
		toiletsvalue = b10.getString("fixtures");
		Bundle b11 = this.getIntent().getExtras();
		otherfeaturesvalue = b11.getString("otherfeatures");
		Bundle b12 = this.getIntent().getExtras();
		gettempoption = b12.getString("tempoption");
		System.out.println("getcurrentview in Load Bathroom="+getcurrentview);
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}
		
		setContentView(R.layout.loadtemplate);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		declaration();
		
		/*HorizontalScrollView hv = (HorizontalScrollView)findViewById(R.id.HorizontalScrollView01);  // your HorizontalScrollView inside scrollview
	    hv.setOnTouchListener(new ListView.OnTouchListener() {
	            @Override
	            public boolean onTouch(View v, MotionEvent event) {
	                int action = event.getAction();
	                switch (action) {
	                case MotionEvent.ACTION_DOWN:
	                    // Disallow ScrollView to intercept touch events.
	                    v.getParent().requestDisallowInterceptTouchEvent(true);
	                    break;

	                case MotionEvent.ACTION_UP:
	                    // Allow ScrollView to intercept touch events.
	                    v.getParent().requestDisallowInterceptTouchEvent(false);
	                    break;
	                }

	                // Handle HorizontalScrollView touch events.
	                v.onTouchEvent(event);
	                return true;
	            }
	        });*/
	 
	}

	private void declaration() {
		// TODO Auto-generated method stub
		db.CreateTable(1);
		db.CreateTable(15);
		db.CreateTable(16);
		db.CreateTable(8);
		db.userid();
		
		setContentView(R.layout.activity_createbathroom);	
		btnsubmit = (Button)findViewById(R.id.btn_submit);
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		
        
		try
		{
			Cursor cur = db.hi_db.rawQuery("select DISTINCT(fld_templatename) from " + db.CreateBathroom + " WHERE fld_inspectorid='"+db.UserId+"'", null);
            if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		scrollup();
		
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		
    	((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));    	
    	
    	btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoadBathroom.this,HomeScreen.class));
				finish();
				 
			}
		});
        
		
		TextView vc = ((TextView)findViewById(R.id.vc));
		vc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent = new Intent(LoadBathroom.this, NewSetVC.class);
					Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b6 = new Bundle();
					b6.putString("tempoption", gettempoption);			
					intent.putExtras(b6);
					Bundle b5 = new Bundle();
					b5.putString("submodulename", "");			
					intent.putExtras(b5);
					startActivity(intent);
					finish();
			}
		});
        
		tvtubnote = (TextView)findViewById(R.id.tubsel);
		tvsinknote = (TextView)findViewById(R.id.sinksel);
		tvbathnote = (TextView)findViewById(R.id.bathroomsel);
		tvtoiletnote = (TextView)findViewById(R.id.toiletsel);
		tvbathfixturesnote = (TextView)findViewById(R.id.bathsel);
		tvotherfeatures = (TextView)findViewById(R.id.otherfeaturessel);
		tvsafety = (TextView)findViewById(R.id.safetysel);
		tvwatersupply = (TextView)findViewById(R.id.watersel);
		tvother = (TextView)findViewById(R.id.bathothersel);
	    
		spin_bathname = (Spinner)findViewById(R.id.spin_bathroomname);
		ed_otherbathroomname= (EditText)findViewById(R.id.et_bathnameother);
	    bathnamearrayadap = new ArrayAdapter<String>(LoadBathroom.this,android.R.layout.simple_spinner_item,bathnamearr);
	    bathnamearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spin_bathname.setAdapter(bathnamearrayadap);
	    spin_bathname.setOnItemSelectedListener(new selonitemlistner(1));
	    
	  /*  spin_bathshowerenclosure = (Spinner)findViewById(R.id.spin_showerenclosure);
	    bathglassenclosurearrayadap = new ArrayAdapter<String>(LoadBathroom.this,android.R.layout.simple_spinner_item,safetyglassarr);
	    bathglassenclosurearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spin_bathshowerenclosure.setAdapter(bathglassenclosurearrayadap);
	    spin_bathshowerenclosure.setOnItemSelectedListener(new selonitemlistner(2));
	    ed_safetyglass = (EditText)findViewById(R.id.ed_safetyshower);*/
	    
	    /*spin_watersupply = (Spinner)findViewById(R.id.spin_drainage);
	    watersupplyarrayadap = new ArrayAdapter<String>(LoadBathroom.this,android.R.layout.simple_spinner_item,watersupplyarr);
	    watersupplyarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spin_watersupply.setAdapter(watersupplyarrayadap);
	    spin_watersupply.setOnItemSelectedListener(new selonitemlistner(3));
	    ed_drainage = (EditText)findViewById(R.id.ed_drainage);
	    
	    
	    spin_bathother = (Spinner)findViewById(R.id.spin_bathother);
	    bathotherarrayadap = new ArrayAdapter<String>(LoadBathroom.this,android.R.layout.simple_spinner_item,bathotherarr);
	    bathotherarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spin_bathother.setAdapter(bathotherarrayadap);
	    spin_bathother.setOnItemSelectedListener(new selonitemlistner(4));*/
	    
	    
	    btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(LoadBathroom.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							Bundle b4 = new Bundle();
							b4.putString("currentview",getcurrentview);			
							intent.putExtras(b4);
							Bundle b3 = new Bundle();
							b3.putString("photooption","takephoto");			
							intent.putExtras(b3);
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getph);			
							intent.putExtras(b5);
							startActivity(intent);
							finish();
			}
		});
	    
	    tvtub= (TextView)findViewById(R.id.tubstv);
	    tvtub.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				bathvalue="Tubs";k=1;
				multi_select(bathvalue,k);				
			}
		});
	    
	    tvsinks= (TextView)findViewById(R.id.sinktv);
	    tvsinks.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bathvalue="Sinks/Basins";k=2;
				multi_select(bathvalue,k);
			}
		});
	    
	    
	    tvshowers= (TextView)findViewById(R.id.bathshowerstv);
	    tvshowers.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bathvalue="Bathroom Shower Enclosures";k=3;
				multi_select(bathvalue,k);
			}
		});	    
	    tvtoilets= (TextView)findViewById(R.id.toiletstv);
	    tvtoilets.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bathvalue="Toilets";k=4;
				multi_select(bathvalue,k);
			}
		});
	    
	    tvfixtuers =(TextView)findViewById(R.id.fixturestv);
	    tvfixtuers.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bathvalue="Bath Fixture Materials";k=5;
				multi_select(bathvalue,k);
			}
		});
	    
	    tvbathother =(TextView)findViewById(R.id.othertv);
	    tvbathother.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bathvalue="Other Features";k=6;
				multi_select(bathvalue,k);
			}
		});
	    
	    bathsafetytv= (TextView)findViewById(R.id.bathsafetytv);
	    bathsafetytv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bathvalue="Safety Glass Stamp at Shower Bathroom - Enclosure";k=7;
				multi_select(bathvalue,k);
			}
		});	   
	    
	    watersupplytv= (TextView)findViewById(R.id.waterstv);
	    watersupplytv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bathvalue="Water Supply Drainage";k=8;
				multi_select(bathvalue,k);
			}
		});	   
	    
	    bathothertv= (TextView)findViewById(R.id.bathotherstv);
	    bathothertv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bathvalue="Bath Other";k=9;
				multi_select(bathvalue,k);
			}
		});	   
	    

	    if(getisedit==1)
		{
	    	((Button)findViewById(R.id.btnsave)).setText("Update");
		}
	    et_bathcomments = (EditText)findViewById(R.id.et_bathcomments);
	    
	    bathtblshow =(TableLayout)findViewById(R.id.bathtable);
	    
	    Button btncancel = (Button)findViewById(R.id.btncancel);
	    btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editid=0;tubsvalue="";sinkvalue="";showersvalue="";toiletsvalue="";otherfeaturesvalue="";fixturesvalue="";
				safetyvalue="";watersupplyvalue="";othervalue="";
				clear();
			}
	    });
	    
	    btnsubmit = (Button)findViewById(R.id.btn_submit);
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getph,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getph,getcurrentview,getmodulename,gettempname);				
				}  
			}
		});
	    
	    Button btnsave = (Button)findViewById(R.id.btnsave);
	    btnsave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				if(getbathname.equals("Other"))
				{
					getbathnameother= ed_otherbathroomname.getText().toString();
				}
				else
				{
					getbathnameother="";
				}
				
				/*if(getsafetyshower.equals("Other"))
				{
					getsafetyshowerother= ed_safetyglass.getText().toString();
				}
				else
				{
					getsafetyshowerother="";
				}*/
				
				/*if(getdrainage.equals("Other"))
				{
					getdrainageother= ed_drainage.getText().toString();
				}
				else
				{
					getdrainageother="";
				}*/
				
				if(getbathname.equals("--Select--"))
				{
					Toast.makeText(getApplicationContext(), "Please select Bathroom Name", Toast.LENGTH_LONG).show();
                	
				}
				else
				{
					vrint=2;
					try
					{
						
						if(((Button)findViewById(R.id.btnsave)).getText().toString().equals("Update"))
						{
							if(getcurrentview.equals("start"))
							{
									db.hi_db.execSQL("UPDATE  "+db.LoadBathroom+" SET fld_bathroomname='"+getbathname+"',fld_otherbathroomname='"+db.encode(getbathnameother)+"',"+
								           "fld_tubs='"+db.encode(tubsvalue)+"',fld_sinks='"+db.encode(sinkvalue)+"',"+
										   "fld_showerenclosures='"+db.encode(showersvalue)+"',fld_safetyglass='"+db.encode(safetyvalue)+"',"+
								           "fld_othersafetyglass='"+db.encode(getsafetyshowerother)+"',fld_toilets='"+db.encode(toiletsvalue)+"',fld_bathfixturematerials='"+db.encode(fixturesvalue)+"',"+
										   "fld_supplydrainage='"+db.encode(watersupplyvalue)+"',fld_supplydrainageother='"+db.encode(getdrainageother)+"',fld_bathother='"+db.encode(othervalue)+"',fld_otherfeatures='"+db.encode(otherfeaturesvalue)+"',fld_bathcomments='"+db.encode(et_bathcomments.getText().toString())+"' Where br_id='"+roomCurrent_select_id+"' and fld_srid='"+getph+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
							}
							else
							{
								db.hi_db.execSQL("UPDATE  "+db.CreateBathroom+" SET fld_bathroomname='"+getbathname+"',fld_otherbathroomname='"+db.encode(getbathnameother)+"',"+
								           "fld_tubs='"+db.encode(tubsvalue)+"',fld_sinks='"+db.encode(sinkvalue)+"',"+
										   "fld_showerenclosures='"+db.encode(showersvalue)+"',fld_safetyglass='"+db.encode(safetyvalue)+"',"+
								           "fld_othersafetyglass='"+db.encode(getsafetyshowerother)+"',fld_toilets='"+db.encode(toiletsvalue)+"',fld_bathfixturematerials='"+db.encode(fixturesvalue)+"',"+
										   "fld_supplydrainage='"+db.encode(watersupplyvalue)+"',fld_supplydrainageother='"+db.encode(getdrainageother)+"',fld_bathother='"+db.encode(othervalue)+"',fld_otherfeatures='"+db.encode(otherfeaturesvalue)+"',fld_bathcomments='"+db.encode(et_bathcomments.getText().toString())+"' Where br_id='"+roomCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
							}
							Toast.makeText(getApplicationContext(), "Bathroom updated successfully", Toast.LENGTH_LONG).show();
		                	clear();
		                	showbathroom();
						}
						else
						{
							try
							{ 
								if(getcurrentview.equals("start"))
								{
								
										Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadBathroom + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_bathroomname='"+getbathname+"'", null);
										if(cur.getCount()==0)
										{
											db.hi_db.execSQL("INSERT INTO "+db.LoadBathroom+" (fld_inspectorid,fld_srid,fld_templatename,fld_bathroomname,fld_otherbathroomname,fld_tubs,fld_sinks,fld_showerenclosures,fld_safetyglass,fld_othersafetyglass,fld_toilets,fld_bathfixturematerials,fld_supplydrainage,fld_supplydrainageother,fld_bathother,fld_otherfeatures,fld_bathcomments) VALUES" +
													"('"+db.UserId+"','"+getph+"','"+db.encode(gettempname)+"','"+getbathname+"','"+db.encode(getbathnameother)+"','"+db.encode(tubsvalue)+"','"+db.encode(sinkvalue)+"','"+db.encode(showersvalue)+"','"+db.encode(safetyvalue)+"','"+db.encode(getsafetyshowerother)+"','"+db.encode(toiletsvalue)+"','"+db.encode(fixturesvalue)+"','"+db.encode(watersupplyvalue)+"','"+db.encode(getdrainageother)+"','"+db.encode(othervalue)+"','"+db.encode(otherfeaturesvalue)+"','"+db.encode(et_bathcomments.getText().toString())+"')");

											cf.DisplayToast("Bathroom name saved successfully");
											clear();
						                	showbathroom();
										}
										else
										{
											cf.DisplayToast("Already Exists!! Please select another Bathroom Name.");
										}
								}
								else
								{
									Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateBathroom + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_bathroomname='"+getbathname+"'", null);
									if(cur.getCount()==0)
									{
										
										db.hi_db.execSQL("INSERT INTO "+db.CreateBathroom+" (fld_inspectorid,fld_templatename,fld_bathroomname,fld_otherbathroomname,fld_tubs,fld_sinks,fld_showerenclosures,fld_safetyglass,fld_othersafetyglass,fld_toilets,fld_bathfixturematerials,fld_supplydrainage,fld_supplydrainageother,fld_bathother,fld_otherfeatures,fld_bathcomments) VALUES" +
												"('"+db.UserId+"','"+db.encode(gettempname)+"','"+getbathname+"','"+db.encode(getbathnameother)+"','"+db.encode(tubsvalue)+"','"+db.encode(sinkvalue)+"','"+db.encode(showersvalue)+"','"+db.encode(safetyvalue)+"','"+db.encode(getsafetyshowerother)+"','"+db.encode(toiletsvalue)+"','"+db.encode(fixturesvalue)+"','"+db.encode(watersupplyvalue)+"','"+db.encode(getdrainageother)+"','"+db.encode(othervalue)+"','"+db.encode(otherfeaturesvalue)+"','"+db.encode(et_bathcomments.getText().toString())+"')");

										cf.DisplayToast("Bathroom name saved successfully");
										clear();
					                	showbathroom();
									}
									else
									{
									   cf.DisplayToast("Already Exists!! Please select another Bathroom Name.");
									}
								}
							}
							catch (Exception e) {
								// TODO: handle exception
							}
							
						}
						
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		});
	    if(getcurrentview.equals("start"))
		{
	    	
	    	checkexistingtemplate();	
		}
	    else
	    {
	    	
	    	showbathroom();	
	    }
	    ((TextView)findViewById(R.id.modulename)).setVisibility(v1.VISIBLE);
	}
	private void scrollup() {
		// TODO Auto-generated method stub
	scr = (ScrollView)findViewById(R.id.scr);
	       scr.post(new Runnable() {
	          public void run() {
	              scr.scrollTo(0,0);
	          }
	      }); 
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(LoadBathroom.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						DefaultValue_Insert(gettempname);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	private void DefaultValue_Insert(String gettempname) {
		// TODO Auto-generated method stub
	
			try
			{
				selcur = db.hi_db.rawQuery("select * from " + db.CreateBathroom + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' ", null);
	            int cnt = selcur.getCount();
	            if(cnt>0)
	           
	            	
					if (selcur != null) {
						selcur.moveToFirst();
						db.hi_db.execSQL("Delete from "+db.LoadBathroom+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"'");
						do {
							
							Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadBathroom + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_bathroomname='"+selcur.getString(3)+"'", null);
							
	    		            if(cur.getCount()==0)
	    		            {
    							try
    			            	{
    								
    								
    								
    								db.hi_db.execSQL("INSERT INTO "+db.LoadBathroom+" (fld_inspectorid,fld_srid,fld_templatename,fld_bathroomname,fld_otherbathroomname,fld_tubs,fld_sinks,fld_showerenclosures,fld_safetyglass,fld_othersafetyglass,fld_toilets,fld_bathfixturematerials,fld_supplydrainage,fld_supplydrainageother,fld_bathother,fld_otherfeatures,fld_bathcomments) VALUES" +
    										"('"+db.UserId+"','"+getph+"','"+db.encode(gettempname)+"','"+selcur.getString(3)+"','"+selcur.getString(4)+"','"+selcur.getString(5)+"','"+selcur.getString(6)+"','"+selcur.getString(7)+"','"+selcur.getString(8)+"','"+selcur.getString(9)+"','"+selcur.getString(10)+"','"+selcur.getString(11)+"','"+selcur.getString(12)+"','"+selcur.getString(13)+"','"+selcur.getString(14)+"','"+selcur.getString(15)+"','"+selcur.getString(16)+"')");
    		                 	}
    			            	catch (Exception e) {
    								// TODO: handle exception
    							}
	    		            }
	    		            else
	    		            {
	    		            	
	    		            	db.hi_db.execSQL("UPDATE  "+db.LoadBathroom+" SET fld_otherbathroomname='"+selcur.getString(4)+"',"+
								           "fld_tubs='"+selcur.getString(5)+"',fld_sinks='"+selcur.getString(6)+"',"+
										   "fld_showerenclosures='"+selcur.getString(7)+"',fld_safetyglass='"+selcur.getString(8)+"',"+
								           "fld_othersafetyglass='"+selcur.getString(9)+"',fld_toilets='"+selcur.getString(10)+"',fld_bathfixturematerials='"+selcur.getString(11)+"',"+
										   "fld_supplydrainage='"+selcur.getString(12)+"',fld_supplydrainageother='"+selcur.getString(13)+"'," +
										   		"fld_bathother='"+selcur.getString(14)+"',fld_otherfeatures='"+selcur.getString(15)+"'," +
										   				"fld_bathcomments='"+selcur.getString(16)+"' " +
										   						"Where fld_srid='"+getph+"' and fld_inspectorid='"+db.UserId+"' " +
										   								"and fld_bathroomname='"+getbathname+"'");
	    		            }
						
						}while (selcur.moveToNext());
					}
	            
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		
			showbathroom();
		
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="Template Submission";
			    p_sv.progress_Msg="Please wait..Your template has been submitting.";
		       
				startActivityForResult(new Intent(LoadBathroom.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		      Bathroom_Sub();
			 		      
			 			
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		chk=1;
		    		success_alert();
            	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(LoadBathroom.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public void success_alert() {
		// TODO Auto-generated method stub
       
		AlertDialog al = new AlertDialog.Builder(LoadBathroom.this).setMessage("You've successfully submitted your template.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				startActivity(new Intent(LoadBathroom.this,HomeScreen.class));
				finish();
				
				
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}

	private void Bathroom_Sub() throws IOException, XmlPullParserException {
		
		
		// TODO Auto-generated method stub
			  Cursor c1 = db.hi_db.rawQuery("select * from " + db.CreateBathroom + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
     		  
   	          if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
   	        		SoapObject requestbathroom = new SoapObject(wb.NAMESPACE,"Create_Bathroom");
	                SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    requestbathroom.addProperty("br_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    requestbathroom.addProperty("fld_inspectorid",db.UserId);
	      		    requestbathroom.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		    requestbathroom.addProperty("fld_bathroomname",db.decode(c1.getString(c1.getColumnIndex("fld_bathroomname"))));
	      		    requestbathroom.addProperty("fld_otherbathroomname",db.decode(c1.getString(c1.getColumnIndex("fld_otherbathroomname"))));
	      		    
	      		    String strtubs = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_tubs")))).toString();
	         		if(strtubs.endsWith(","))
	         		{
	         			strtubs = strtubs.substring(0,strtubs.length()-1);
	         		}
	         		requestbathroom.addProperty("fld_tubs",strtubs);
	         		
	         		String strsinks = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_sinks")))).toString();
	         		if(strsinks.endsWith(","))
	         		{
	         			strsinks = strsinks.substring(0,strsinks.length()-1);
	         		}
	         		requestbathroom.addProperty("fld_sinks",strsinks);
	      		    
	         		String strshowerenclos = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_showerenclosures")))).toString();
	         		if(strshowerenclos.endsWith(","))
	         		{
	         			strshowerenclos = strshowerenclos.substring(0,strshowerenclos.length()-1);
	         		}
	         		requestbathroom.addProperty("fld_showerenclosures",strshowerenclos);
	         		
	      		    requestbathroom.addProperty("fld_safetyglass",db.decode(c1.getString(c1.getColumnIndex("fld_safetyglass"))));
	      		    requestbathroom.addProperty("fld_othersafetyglass",db.decode(c1.getString(c1.getColumnIndex("fld_othersafetyglass"))));
	      		    
	      		    String strtoilets = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_toilets")))).toString();
	         		if(strtoilets.endsWith(","))
	         		{
	         			strtoilets = strtoilets.substring(0,strtoilets.length()-1);
	         		}
	         		requestbathroom.addProperty("fld_toilets",strtoilets);
	         		
	         		String strbathfix = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_bathfixturematerials")))).toString();
	         		if(strbathfix.endsWith(","))
	         		{
	         			strbathfix = strbathfix.substring(0,strbathfix.length()-1);
	         		}
	         		requestbathroom.addProperty("fld_bathfixturematerials",strbathfix);
	      		    
	      		    requestbathroom.addProperty("fld_supplydrainage",db.decode(c1.getString(c1.getColumnIndex("fld_supplydrainage"))));
	      		    requestbathroom.addProperty("fld_supplydrainageother",db.decode(c1.getString(c1.getColumnIndex("fld_supplydrainageother"))));
	      		    
	      		    requestbathroom.addProperty("fld_bathother",db.decode(c1.getString(c1.getColumnIndex("fld_bathother"))));
	      		    
	      			String strotherfeat = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_otherfeatures")))).toString();
	         		if(strotherfeat.endsWith(","))
	         		{
	         			strotherfeat = strotherfeat.substring(0,strotherfeat.length()-1);
	         		}
	         		requestbathroom.addProperty("fld_otherfeatures",strotherfeat);
	      		    requestbathroom.addProperty("fld_bathcomments",db.decode(c1.getString(c1.getColumnIndex("fld_bathcomments"))));	
       			      		    
	      		    envelope1.setOutputSoapObject(requestbathroom);	         		
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	                androidHttpTransport1.call(wb.NAMESPACE+"Create_Bathroom", envelope1);
	                      response = (SoapObject)envelope1.getResponse();
	                      
	                      //handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	   
	                    		  try
	 	          				 {
	                    			 
	 	          					db.hi_db.execSQL("UPDATE "+db.CreateBathroom+ " SET fld_sendtoserver='"+response.getProperty("br_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_bathroomname='"+c1.getString(c1.getColumnIndex("fld_bathroomname"))+"'");
	 	          					System.out.println("UPDATE "+db.CreateBathroom+ " SET fld_sendtoserver='"+response.getProperty("br_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_bathroomname='"+c1.getString(c1.getColumnIndex("fld_bathroomname"))+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				   	System.out.println("for loop catch="+e.getMessage());
	 	          				   	handler_msg=0;
	 	          				 }
	 	                    	handler_msg = 0;	  
	                    	  
	                    	 
	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
   	          else
   	          {
   	        	  handler_msg=0;
   	          }
     	  
	}
	
	private void dbvalue(String bathvalue) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		if(bathvalue.equals("Tubs"))
		{
			for(int j=0;j<bathtubsarr.length;j++)
			{
				str_arrlst.add(bathtubsarr[j]);
			}	
		}
		if(bathvalue.equals("Sinks/Basins"))
		{
			for(int j=0;j<bathsinksarr.length;j++)
			{
				str_arrlst.add(bathsinksarr[j]);
			}	
		}
		
		if(bathvalue.equals("Bathroom Shower Enclosures"))
		{
			for(int j=0;j<bathshowerenclosuresarr.length;j++)
			{
				str_arrlst.add(bathshowerenclosuresarr[j]);
			}	
		}
		
		if(bathvalue.equals("Toilets"))
		{
			for(int j=0;j<bathtoiletarr.length;j++)
			{
				str_arrlst.add(bathtoiletarr[j]);
			}	
		}
		
		if(bathvalue.equals("Bath Fixture Materials"))
		{
			for(int j=0;j<bathfixturesarr.length;j++)
			{
				str_arrlst.add(bathfixturesarr[j]);
			}	
		}
		
		if(bathvalue.equals("Other Features"))
		{
			for(int j=0;j<otherfeaturesarr.length;j++)
			{
				str_arrlst.add(otherfeaturesarr[j]);
			}	
		}
		
		if(bathvalue.equals("Safety Glass Stamp at Shower Bathroom - Enclosure"))
		{
			for(int j=0;j<safetyglassarr.length;j++)
			{
				str_arrlst.add(safetyglassarr[j]);
			}	
		}
		
		if(bathvalue.equals("Water Supply Drainage"))
		{
			for(int j=0;j<watersupplyarr.length;j++)
			{
				str_arrlst.add(watersupplyarr[j]);
			}	
		}
		
		if(bathvalue.equals("Bath Other"))
		{
			for(int j=0;j<bathotherarr.length;j++)
			{
				str_arrlst.add(bathotherarr[j]);
			}	
		}
		
		 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(bathvalue)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getph+"'",null);
		 System.out.println("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(bathvalue)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getph+"'");
		 if(c.getCount()>0)
		 {
			 c.moveToFirst();
			 for(int i =0;i<c.getCount();i++,c.moveToNext())
			 {
				 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
			 }
		 }
		
	}
	private void multi_select(final String bathvalue, int s) {
		// TODO Auto-generated method stub
		bath=1;
		dbvalue(bathvalue);
		
		final Dialog add_dialog = new Dialog(LoadBathroom.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);

		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);
		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(bathvalue);
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);
		showsuboption();
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval="";
								for (int i = 0; i < str_arrlst.size(); i++) {
									if (ch[i].isChecked()) {
										seltdval += ch[i].getText().toString() + "&#44;";
									}
								}
								
								
								finalselstrval = "";
								finalselstrval = seltdval;
								if(finalselstrval.equals(""))
								{
									cf.DisplayToast("Please check at least any 1 option to save");
								}
								else
								{	
									if(bathvalue.equals("Tubs"))
									{
										tubsvalue=finalselstrval;
										temptub = tubsvalue;
										temptub = cf.replacelastendswithcomma(temptub);
										tvtubnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+temptub));
										tvtubnote.setVisibility(v.VISIBLE);
									}
									else if(bathvalue.equals("Sinks/Basins"))
									{
										sinkvalue=finalselstrval;
										tempsink = sinkvalue;
										tempsink = cf.replacelastendswithcomma(tempsink);
										tvsinknote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempsink));
										tvsinknote.setVisibility(v.VISIBLE);
									}
									else if(bathvalue.equals("Bathroom Shower Enclosures"))
									{
										showersvalue=finalselstrval;
										tempbathroom = showersvalue;
										tempbathroom = cf.replacelastendswithcomma(tempbathroom);
										tvbathnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempbathroom));
										tvbathnote.setVisibility(v.VISIBLE);
									}
									else if(bathvalue.equals("Toilets"))
									{
										toiletsvalue=finalselstrval;
										temptoilet = toiletsvalue;
										temptoilet = cf.replacelastendswithcomma(temptoilet);
										tvtoiletnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+temptoilet));
										tvtoiletnote.setVisibility(v.VISIBLE);
									}
									else if(bathvalue.equals("Bath Fixture Materials"))
									{
										fixturesvalue=finalselstrval;
										tempbathfixtures = fixturesvalue;
										tempbathfixtures = cf.replacelastendswithcomma(tempbathfixtures);
										tvbathfixturesnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempbathfixtures));
										tvbathfixturesnote.setVisibility(v.VISIBLE);
									}									
									else if(bathvalue.equals("Other Features"))
									{
										otherfeaturesvalue=finalselstrval;	
										tempotherfeatures = otherfeaturesvalue;
										tempotherfeatures = cf.replacelastendswithcomma(tempotherfeatures);
										tvotherfeatures.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempotherfeatures));
										tvotherfeatures.setVisibility(v.VISIBLE);
									}
									else if(bathvalue.equals("Safety Glass Stamp at Shower Bathroom - Enclosure"))
									{
										safetyvalue=finalselstrval;	
										tempsafety = safetyvalue;
										tempsafety = cf.replacelastendswithcomma(tempsafety);
										tvsafety.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempsafety));
										tvsafety.setVisibility(v.VISIBLE);
									}
									else if(bathvalue.equals("Water Supply Drainage"))
									{
										watersupplyvalue=finalselstrval;	
										tempwatersupply = watersupplyvalue;
										tempwatersupply = cf.replacelastendswithcomma(tempwatersupply);
										tvwatersupply.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempwatersupply));
										tvwatersupply.setVisibility(v.VISIBLE);
									}
									else if(bathvalue.equals("Bath Other"))
									{
										othervalue=finalselstrval;	
										tempother = othervalue;
										tempother = cf.replacelastendswithcomma(tempother);
										tvother.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempother));
										tvother.setVisibility(v.VISIBLE);
									}
									
									
									
									
									add_dialog.cancel();
									bath=0;
								}
								
					}
				
		});
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bath=0;
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				dialog_other=1;
				dialogshow(bathvalue);
				
				
			}
		});
		
		add_dialog.setCancelable(false);
		add_dialog.show();
		setvalue(bathvalue);
		/*Intent i = new Intent(LoadBathroom.this,LoadBathroomDialog.class);
		i.putExtra("bathoption", bathoption);
		i.putExtra("templatename", gettempname);				
		i.putExtra("selectedid", getph);
		i.putExtra("modulename", getmodulename);
		i.putExtra("currentview", getcurrentview);
		i.putExtra("isedit", getisedit);		
		if(editid==1 || ((Button)findViewById(R.id.btnsave)).getText().toString().equals("Update"))
		{
			i.putExtra("id", roomCurrent_select_id);
		}
		else
		{
			i.putExtra("id", 0);	
		}
		
		i.putExtra("tubs", tubsvalue);
		i.putExtra("sinks", sinkvalue);
		i.putExtra("shower", showersvalue);		
		i.putExtra("toilets", toiletsvalue);
		i.putExtra("fixtures", fixturesvalue);
		i.putExtra("otherfeatures", otherfeaturesvalue);		
		startActivityForResult(i,s);*/
	}
	private void dialogshow(final String bathvalue) {
		// TODO Auto-generated method stub
		
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.addnewcustom);
		
		etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
		cf.hidekeyboard(etother);
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=0;
				add_dialog.dismiss();
			}
		});
		
		Button btnsave = (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (etother.getText().toString().trim().equals("")) {
					cf.DisplayToast("Please enter the other text");				
				} else {
					if(str_arrlst.contains(etother.getText().toString().trim()))
					{
						cf.DisplayToast("Already exists!! Please enter the new text");
						etother.setText("");
						etother.requestFocus();
					}
					else
					{
						try
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.SaveAddOther
									+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
									+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','','"+db.encode(bathvalue)+"','"
									+ db.encode(etother.getText().toString()) + "','"+db.encode(gettempname)+"')");
							
							String addedval = db.encode(etother.getText().toString());
							str_arrlst.add(addedval);
							dialog_other=0;
							cf.DisplayToast("Added successfully");
							add_dialog.cancel();
							
							lin.removeAllViews();
							dbvalue(bathvalue);
							showsuboption();
							setvalue(bathvalue);
						
							
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
		
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	private void setvalue(String bathvalue) {
		// TODO Auto-generated method stub
		System.out.println("bathvalue="+bathvalue);
		Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadBathroom+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getph+"' and br_id='"+getid+"'", null);
		System.out.println("count"+cur.getCount());
	    if (cur.getCount() > 0) {
			cur.moveToFirst();
			str="";
			if(bathvalue.equals("Tubs"))
			{
				/*if(!tubsvalue.equals(""))
				{
					str=tubsvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_tubs")));	
				//}				 
			}
			else if(bathvalue.equals("Sinks/Basins"))
			{
				/*if(!sinkvalue.equals(""))
				{
					str=sinkvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_sinks")));
				//}
			}
			else if(bathvalue.equals("Bathroom Shower Enclosures"))
			{
				/*if(!showersvalue.equals(""))
				{
					str=showersvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_showerenclosures")));
				//}
			}
			else if(bathvalue.equals("Toilets"))
			{
				/*if(!toiletsvalue.equals(""))
				{
					str=toiletsvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_toilets")));
				//}
			}
			else if(bathvalue.equals("Bath Fixture Materials"))
			{
				/*if(!fixturesvalue.equals(""))
				{
					str=fixturesvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_bathfixturematerials")));
				//}
			}									
			else if(bathvalue.equals("Other Features"))
			{
				/*if(!otherfeaturesvalue.equals(""))
				{
					str=otherfeaturesvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_otherfeatures")));
				//}
			}
			else if(bathvalue.equals("Safety Glass Stamp at Shower Bathroom - Enclosure"))
			{
				/*if(!otherfeaturesvalue.equals(""))
				{
					str=otherfeaturesvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_safetyglass")));
				//}
			}
			else if(bathvalue.equals("Water Supply Drainage"))
			{
				/*if(!otherfeaturesvalue.equals(""))
				{
					str=otherfeaturesvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainage")));
				//}
			}
			else if(bathvalue.equals("Bath Other"))
			{
				/*if(!otherfeaturesvalue.equals(""))
				{
					str=otherfeaturesvalue;
				}
				else
				{*/
					str = db.decode(cur.getString(cur.getColumnIndex("fld_bathother")));
				//}
			}
			
			
				
		}	
	    else
	    {System.out.println("tubsvalue="+tubsvalue+sinkvalue);
	    	
	    	if(bathvalue.equals("Tubs") && tubsvalue!=null)
			{
				str = tubsvalue;							 
			}
			else if(bathvalue.equals("Sinks/Basins") && sinkvalue!=null)
			{
				str = sinkvalue;		
			}
			else if(bathvalue.equals("Bathroom Shower Enclosures") && showersvalue!=null)
			{
				str = showersvalue;		
			}
			else if(bathvalue.equals("Toilets") && toiletsvalue!=null)
			{
				str = toiletsvalue;		
			}
			else if(bathvalue.equals("Bath Fixture Materials") && fixturesvalue!=null)
			{
				str = fixturesvalue;		
			}									
			else if(bathvalue.equals("Other Features") && otherfeaturesvalue!=null)
			{
				str = otherfeaturesvalue;		
			}
			else if(bathvalue.equals("Safety Glass Stamp at Shower Bathroom - Enclosure") && safetyvalue!=null)
			{
				str = safetyvalue;		
			}
			else if(bathvalue.equals("Water Supply Drainage") && watersupplyvalue!=null)
			{
				str = watersupplyvalue;		
			}
			else if(bathvalue.equals("Bath Other") && othervalue!=null)
			{
				str = othervalue;		
			}
	    }System.out.println("str="+str);
	    if(!str.trim().equals(""))
		{
			cf.setValuetoCheckbox(ch, str);
		}	
	}
	private void showsuboption() {
		// TODO Auto-generated method stub
		
			/*try
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadBathroom + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
				System.out.println("showsuboptionselect * from " + db.LoadBathroom + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"'"+cur.getCount());
				if (cur.getCount() > 0) {
					cur.moveToFirst();
					for (int i = 0; i < cur.getCount(); i++, cur.moveToNext()) {
						str = db.decode(cur.getString(cur
								.getColumnIndex("fld_description")));
		                
					}
					
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
			*/
			
		  ch = new CheckBox[str_arrlst.size()];
			
			for (int i = 0; i < str_arrlst.size(); i++) {
	        		ch[i] = new CheckBox(this);
					ch[i].setText(str_arrlst.get(i));
					ch[i].setTextColor(Color.BLACK);
					
					lin.addView(ch[i]);
					ch[i].setOnCheckedChangeListener(new CHListener(i));
					
			}
		/*	if (str != "") {
					cf.setValuetoCheckbox(ch, str);
			}*/
				
		
	}
	class CHListener implements OnCheckedChangeListener, android.widget.CompoundButton.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		
		//seltdval="";
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
			}
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	private void checkexistingtemplate() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadBathroom + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			 
			 if(cur.getCount()==0 && flag==0)
			 {
				defaultinsert();
			 }
			 else
			 {
				 showbathroom();
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	private void defaultinsert() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.CreateBathroom + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 for(int i=0;i<cur.getCount();i++)
				 {
					 db.hi_db.execSQL("INSERT INTO "+db.LoadBathroom+" (fld_inspectorid,fld_srid,fld_templatename,fld_bathroomname,fld_otherbathroomname,fld_tubs,fld_sinks,fld_showerenclosures,fld_safetyglass,fld_othersafetyglass,fld_toilets,fld_bathfixturematerials,fld_supplydrainage,fld_supplydrainageother,fld_bathother,fld_otherfeatures,fld_bathcomments) VALUES" +
								"('"+db.UserId+"','"+getph+"','"+db.encode(gettempname)+"','"+cur.getString(3)+"','"+cur.getString(4)+"','"+cur.getString(5)+"','"+cur.getString(6)+"','"+cur.getString(7)+"','"+cur.getString(8)+"','"+cur.getString(9)+"','"+cur.getString(10)+"','"+cur.getString(11)+"','"+cur.getString(12)+"','"+cur.getString(13)+"','"+cur.getString(14)+"','"+cur.getString(15)+"','"+cur.getString(16)+"')");
						
					 cur.moveToNext();
				 }
				 
				 showbathroom();
			 }
			 else
			 {
				 showbathroom();
			 }
			 
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("catch=="+e.getMessage());
		}
		
		
	}

	private void showbathroom() {
		// TODO Auto-generated method stub
		
		try
		{
			Cursor cur;	
			if(getcurrentview.equals("start"))
			{
				cur= db.hi_db.rawQuery("select * from " + db.LoadBathroom + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' ",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateBathroom + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
	
			if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 bathtblshow.removeAllViews();
				 
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.bath_header, null); 
					
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.lin);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();
				
				 bathtblshow.setVisibility(View.VISIBLE); 
				 bathtblshow.addView(th,lp);
				 
				 
				 for(int i=0;i<cur.getCount();i++)
				 {
					 bathno="";bathname="";bathnameother="";fixturesdb="";tubtv="";sinkdb="";
					 showerdb="";othersafetyglass="";safetyglass="";toiletdb="";drainage="";otherdrainage="";bathother="";otherfeaturesdb="";comments="";
					 
					 
					 bathno=cur.getString(0);
					 bathname=cur.getString(cur.getColumnIndex("fld_bathroomname"));
					 bathnameother=db.decode(cur.getString(cur.getColumnIndex("fld_otherbathroomname")));
					 tubtv=db.decode(cur.getString(cur.getColumnIndex("fld_tubs")));
					 sinkdb=db.decode(cur.getString(cur.getColumnIndex("fld_sinks")));
					 showerdb=db.decode(cur.getString(cur.getColumnIndex("fld_showerenclosures")));
					 safetyglass=db.decode(cur.getString(cur.getColumnIndex("fld_safetyglass")));
					 othersafetyglass=db.decode(cur.getString(cur.getColumnIndex("fld_othersafetyglass")));
					 toiletdb=db.decode(cur.getString(cur.getColumnIndex("fld_toilets")));
					 fixturesdb=db.decode(cur.getString(cur.getColumnIndex("fld_bathfixturematerials")));
					 drainage=db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainage")));
					 otherdrainage=db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainageother")));				 
					 bathother=db.decode(cur.getString(cur.getColumnIndex("fld_bathother")));
					 otherfeaturesdb=db.decode(cur.getString(cur.getColumnIndex("fld_otherfeatures")));
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_bathcomments")));		
					 
					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.bath_listview, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.bath);
				     t.setId(100+i);
				   
					TextView tvbath= (TextView) t.findViewWithTag("bathname");
					if(bathnameother.trim().equals(""))
					{
						tvbath.setText(bathname);
					}
					else
					{
						tvbath.setText(bathname+"("+bathnameother+")");
					}
					
					
					/*TextView tvglass= (TextView) t.findViewWithTag("safetyglass");
					if(safetyglass.equals("--Select--"))
					{
						safetyglass="";
					}
					if(safetyglass.equals("Other"))
					{
						tvglass.setText(safetyglass+" ("+ othersafetyglass +")");
					}
					else
					{
						tvglass.setText(safetyglass);	
					}*/
					
					/*TextView tvdrainage= (TextView) t.findViewWithTag("drainage");
					if(drainage.equals("--Select--"))
					{
						drainage="";
					}
					if(drainage.equals("Other"))
					{
						tvdrainage.setText(drainage+" ("+ otherdrainage +")");
					}
					else
					{
						tvdrainage.setText(drainage);	
					}
					
					
					TextView tvbathother= (TextView) t.findViewWithTag("bathother");
					if(bathother.equals("--Select--"))
					{
						bathother="";
					}
					tvbathother.setText(bathother);*/
					
					TextView tvtub= (TextView) t.findViewWithTag("tubs");
					if(!tubtv.equals(""))
					{
						//tvtub.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Tubs</u></font>"));
						tubtv= tubtv.replace("&#44;",", ");
						if(tubtv.endsWith(", "))
						{
							tubtv = tubtv.substring(0,tubtv.length()-2);
						}
						tvtub.setText(tubtv);
					}
					
					tvtub.setTag(cur.getString(0));
				   /* tvtub.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							viewalert(j,"Tubs");				
						}
					});
					*/
				    TextView tvotherfeatures= (TextView) t.findViewWithTag("otherfeatures");
					if(!otherfeaturesdb.equals(""))
					{
						otherfeaturesdb= otherfeaturesdb.replace("&#44;",", ");
						if(otherfeaturesdb.endsWith(", "))
						{
							otherfeaturesdb = otherfeaturesdb.substring(0,otherfeaturesdb.length()-2);
						}
						tvotherfeatures.setText(otherfeaturesdb);
						//tvotherfeatures.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Other Features</u></font>"));
					}
					tvotherfeatures.setTag(cur.getString(0));
					/*tvotherfeatures.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							viewalert(j,"Other Features");				
						}
					});*/
					
					TextView tvsinks= (TextView) t.findViewWithTag("sinks");
					if(!sinkdb.equals(""))
					{
						//tvsinks.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Sinks</u></font>"));
						sinkdb= sinkdb.replace("&#44;",", ");
						if(sinkdb.endsWith(", "))
						{
							sinkdb = sinkdb.substring(0,sinkdb.length()-2);
						}
						tvsinks.setText(sinkdb);
					}
					tvsinks.setTag(cur.getString(0));
					/*tvsinks.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							viewalert(j,"Sinks/Basins");				
						}
					});*/
					
					TextView tvshowerenclosures= (TextView) t.findViewWithTag("showerenclosures");
					if(!showerdb.equals(""))
					{
						showerdb= showerdb.replace("&#44;",", ");
						if(showerdb.endsWith(", "))
						{
							showerdb = showerdb.substring(0,showerdb.length()-2);
						}
						tvshowerenclosures.setText(showerdb);
						//tvshowerenclosures.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Bathroom Shower Enclosures</u></font>"));
					}
					tvshowerenclosures.setTag(cur.getString(0));
					/*tvshowerenclosures.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							viewalert(j,"Bathroom Shower Enclosures");				
						}
					});*/
					
					
					TextView tvtoilet= (TextView) t.findViewWithTag("toilets");
					if(!toiletdb.equals(""))
					{
						toiletdb= toiletdb.replace("&#44;",", ");
						if(toiletdb.endsWith(", "))
						{
							toiletdb = toiletdb.substring(0,toiletdb.length()-2);
						}
						tvtoilet.setText(toiletdb);
						//tvtoilet.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Toilets</u></font>"));
					}
					tvtoilet.setTag(cur.getString(0));
					/*tvtoilet.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							viewalert(j,"Toilets");				
						}
					});*/
					
					
					TextView tvfixtures= (TextView) t.findViewWithTag("bathfixtrue");
					if(!fixturesdb.equals(""))
					{
						fixturesdb= fixturesdb.replace("&#44;",", ");
						if(fixturesdb.endsWith(", "))
						{
							fixturesdb = fixturesdb.substring(0,fixturesdb.length()-2);
						}
						tvfixtures.setText(toiletdb);
						//tvfixtures.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Bath Fixture Materials</u></font>"));
					}
					tvfixtures.setTag(cur.getString(0));
					/*tvfixtures.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							viewalert(j,"Bath Fixture Materials");				
						}
					});*/
					
					TextView tvsafetyglass= (TextView) t.findViewWithTag("safetyglass");
					if(!safetyglass.equals(""))
					{
						safetyglass= safetyglass.replace("&#44;",", ");
						if(safetyglass.endsWith(", "))
						{
							safetyglass = safetyglass.substring(0,safetyglass.length()-2);
						}
						tvsafetyglass.setText(safetyglass);
						
					}
					tvsafetyglass.setTag(cur.getString(0));
					/*tvsafetyglass.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							 viewalert(j,"Safety Glass Stamp at Shower Bathroom - Enclosure");				
						}
					});*/
					
					TextView tvdrainage= (TextView) t.findViewWithTag("drainage");
					if(!drainage.equals(""))
					{
						drainage= drainage.replace("&#44;",", ");
						if(safetyglass.endsWith(", "))
						{
							drainage = drainage.substring(0,drainage.length()-2);
						}
						tvdrainage.setText(drainage);
						
					}
					tvdrainage.setTag(cur.getString(0));
					/*tvdrainage.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							 viewalert(j,"Water Supply Drainage");				
						}
					});*/
					
					TextView tvbathother= (TextView) t.findViewWithTag("bathother");
					if(!bathother.equals(""))
					{
						bathother= bathother.replace("&#44;",", ");
						if(bathother.endsWith(", "))
						{
							bathother = bathother.substring(0,bathother.length()-2);
						}
						tvbathother.setText(bathother);
						
					}
					tvbathother.setTag(cur.getString(0));
					/*tvbathother.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							 int j=Integer.parseInt(v.getTag().toString());
							 viewalert(j,"Bath Other");				
						}
					});*/
					TextView tvcomments= (TextView) t.findViewWithTag("comments");
					if(!comments.equals(""))
					{
						
						tvcomments.setText(comments);
						//tvcomments.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Comments</u></font>"));
					}
					tvcomments.setTag(cur.getString(0));
					/*tvcomments.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								int j=Integer.parseInt(v.getTag().toString());
								viewalert(j,"Comments");		
							}
						});*/
					
					ImageView edit= (ImageView) t.findViewWithTag("edit");
				 	edit.setId(789456+i);
				 	edit.setTag(cur.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						   int i=Integer.parseInt(v.getTag().toString());
						
						   vrint=1;
						
						   updateroom(i);   scrollup();
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
					
	                ImageView delete=(ImageView) t.findViewWithTag("del");
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(LoadBathroom.this);
							builder.setMessage("Do you want to delete the selected Bathroom details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==roomCurrent_select_id)
													{
														roomCurrent_select_id=0;
													}
													
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadBathroom+" Where br_id='"+i+"'");	
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateBathroom+" Where br_id='"+i+"'");
													}
															
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
													
													
													Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadBathroom + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
													if(cur.getCount()==0)
													{
														flag=1;
													}
													clear();
													
													showbathroom();
													
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					
					l1.removeAllViews();
					
					bathtblshow.addView(t,lp);
					cur.moveToNext();
					 
				 }
			 }
			 else
			 {
				 bathtblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	 	
	}
	
	protected void viewalert(int i,String title) {
		// TODO Auto-generated method stub
		getdbvalues(i);
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.alert);
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(title);
		
		TextView hdr1 = (TextView)add_dialog.findViewById(R.id.txtcontent);
		if(title.equals("Tubs"))
		{
			tubsvalue= tubsvalue.replace("&#44;",", ");
			if(tubsvalue.endsWith(", "))
			{
				tubsvalue = tubsvalue.substring(0,tubsvalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(tubsvalue);
		}
		else if(title.equals("Sinks/Basins"))
		{
			sinkvalue= sinkvalue.replace("&#44;",", ");
			if(sinkvalue.endsWith(", "))
			{
				sinkvalue = sinkvalue.substring(0,sinkvalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(sinkvalue);
		}
		else if(title.equals("Bathroom Shower Enclosures"))
		{
			showersvalue= showersvalue.replace("&#44;",", ");
			if(showersvalue.endsWith(", "))
			{
				showersvalue = showersvalue.substring(0,showersvalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(showersvalue);
		}
		else if(title.equals("Toilets"))
		{
			toiletsvalue= toiletsvalue.replace("&#44;",", ");
			if(toiletsvalue.endsWith(", "))
			{
				toiletsvalue = toiletsvalue.substring(0,toiletsvalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(toiletsvalue);
		}
		else if(title.equals("Bath Fixture Materials"))
		{
			fixturesvalue= fixturesvalue.replace("&#44;",", ");
			if(fixturesvalue.endsWith(", "))
			{
				fixturesvalue = fixturesvalue.substring(0,fixturesvalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(fixturesvalue);
		}
		else if(title.equals("Other Features"))
		{
			otherfeaturesvalue= otherfeaturesvalue.replace("&#44;",", ");
			if(otherfeaturesvalue.endsWith(", "))
			{
				otherfeaturesvalue = otherfeaturesvalue.substring(0,otherfeaturesvalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(otherfeaturesvalue);
		}
		else if(title.equals("Safety Glass Stamp at Shower Bathroom - Enclosure"))
		{
			safetyvalue= safetyvalue.replace("&#44;",", ");
			if(safetyvalue.endsWith(", "))
			{
				safetyvalue = safetyvalue.substring(0,safetyvalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(safetyvalue);
		}
		else if(title.equals("Water Supply Drainage"))
		{
			watersupplyvalue= watersupplyvalue.replace("&#44;",", ");
			if(watersupplyvalue.endsWith(", "))
			{
				watersupplyvalue = watersupplyvalue.substring(0,watersupplyvalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(watersupplyvalue);
		}
		else if(title.equals("Bath Other"))
		{
			othervalue= othervalue.replace("&#44;",", ");
			if(othervalue.endsWith(", "))
			{
				othervalue = othervalue.substring(0,othervalue.length()-2);
			}
			hdr1.setText(Html.fromHtml("<b>You have selected : </b>"));
			hdr1.append(othervalue);
		}
		else if(title.equals("Comments"))
		{						
			hdr1.setText(comments);
		}
		
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				add_dialog.dismiss();
			}
		});
		
		add_dialog.setCancelable(false);
		add_dialog.show();
		
	}
	private void getdbvalues(int i)
	{
		try{
			Cursor cur;
			if(getcurrentview.equals("start"))
			{
				cur= db.hi_db.rawQuery("select * from " + db.LoadBathroom + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and br_id='"+i+"'",null);	
			}
			else
			{
				cur= db.hi_db.rawQuery("select * from " + db.CreateBathroom + " where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and br_id='"+i+"'",null);
			}
			if(cur.getCount()>0)
			{
				cur.moveToFirst();
				
				 bathno="";bathname="";bathnameother="";tubs="";sinks="";bathfixture="";showerenclosures="";
				 shower="";othersafetyglass="";safetyglass="";toilet="";drainage="";otherdrainage="";bathother="";otherfeatures="";comments="";
				 
				 bathno=cur.getString(0);
				 bathname=cur.getString(cur.getColumnIndex("fld_bathroomname"));
				 bathnameother=db.decode(cur.getString(cur.getColumnIndex("fld_otherbathroomname")));
				 tubsvalue=db.decode(cur.getString(cur.getColumnIndex("fld_tubs")));
				 sinkvalue=db.decode(cur.getString(cur.getColumnIndex("fld_sinks")));
				 showersvalue=db.decode(cur.getString(cur.getColumnIndex("fld_showerenclosures")));
				 safetyglass=db.decode(cur.getString(cur.getColumnIndex("fld_safetyglass")));
				 othersafetyglass=db.decode(cur.getString(cur.getColumnIndex("fld_othersafetyglass")));
				 toiletsvalue=db.decode(cur.getString(cur.getColumnIndex("fld_toilets")));
				 fixturesvalue=db.decode(cur.getString(cur.getColumnIndex("fld_bathfixturematerials")));
				 drainage=db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainage")));
				 otherdrainage=db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainageother")));				 
				 bathother=db.decode(cur.getString(cur.getColumnIndex("fld_bathother")));
				 otherfeaturesvalue=db.decode(cur.getString(cur.getColumnIndex("fld_otherfeatures")));
				 comments=db.decode(cur.getString(cur.getColumnIndex("fld_bathcomments")));	
			}
		}catch(Exception e){}
	}
	
	protected void updateroom(int i) {
		// TODO Auto-generated method stub
		Cursor cur;
			editid=1;
			getisedit=1;
			roomCurrent_select_id = i;scrollup();
			try
			{
				if(getcurrentview.equals("start"))
				{
					cur= db.hi_db.rawQuery("select * from " + db.LoadBathroom + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and br_id='"+i+"'",null);	
				}
				else
				{
					cur= db.hi_db.rawQuery("select * from " + db.CreateBathroom + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and br_id='"+i+"'",null);
				}
				
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
						
					 bathno="";bathname="";bathnameother="";tubs="";sinks="";bathfixture="";showerenclosures="";
					 shower="";othersafetyglass="";safetyglass="";toilet="";drainage="";otherdrainage="";bathother="";otherfeatures="";comments="";
					 
					 bathno=cur.getString(0);
					 bathname=cur.getString(cur.getColumnIndex("fld_bathroomname"));
					 bathnameother=db.decode(cur.getString(cur.getColumnIndex("fld_otherbathroomname")));
					 tubsvalue=db.decode(cur.getString(cur.getColumnIndex("fld_tubs")));
					 sinkvalue=db.decode(cur.getString(cur.getColumnIndex("fld_sinks")));
					 showersvalue=db.decode(cur.getString(cur.getColumnIndex("fld_showerenclosures")));
					 safetyglass=db.decode(cur.getString(cur.getColumnIndex("fld_safetyglass")));
					 othersafetyglass=db.decode(cur.getString(cur.getColumnIndex("fld_othersafetyglass")));
					 toiletsvalue=db.decode(cur.getString(cur.getColumnIndex("fld_toilets")));
					 fixturesvalue=db.decode(cur.getString(cur.getColumnIndex("fld_bathfixturematerials")));
					 watersupplyvalue=db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainage")));
					 otherdrainage=db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainageother")));				 
					 othervalue=db.decode(cur.getString(cur.getColumnIndex("fld_bathother")));
					 otherfeaturesvalue=db.decode(cur.getString(cur.getColumnIndex("fld_otherfeatures")));
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_bathcomments")));		
					
					spin_bathname.setSelection(bathnamearrayadap.getPosition(bathname));spin_bathname.setEnabled(false);
					if(bathnameother.trim().equals(""))
					{
						ed_otherbathroomname.setVisibility(v.GONE);
					}
					else
					{
						ed_otherbathroomname.setVisibility(v.VISIBLE);
						ed_otherbathroomname.setText(bathnameother);
					}
					
					/*spin_bathshowerenclosure.setSelection(bathglassenclosurearrayadap.getPosition(safetyglass));
					if(othersafetyglass.trim().equals(""))
					{
						ed_safetyglass.setVisibility(v.INVISIBLE);
					}
					else
					{
						ed_safetyglass.setVisibility(v.VISIBLE);
						ed_safetyglass.setText(othersafetyglass);
					}*/
					/*spin_watersupply.setSelection(watersupplyarrayadap.getPosition(drainage));
					if(otherdrainage.trim().equals(""))
					{
						ed_drainage.setVisibility(v.INVISIBLE);
					}
					else
					{
						ed_drainage.setVisibility(v.VISIBLE);
						ed_drainage.setText(otherdrainage);
					}
					
					
					spin_bathother.setSelection(bathotherarrayadap.getPosition(bathother));*/
					et_bathcomments.setText(comments);	
					
					
					if(!tubsvalue.equals(""))
					{
						tvtubnote.setVisibility(v.VISIBLE);
						if(tubsvalue.endsWith("&#44;"))
						{
							tubsvalue=tubsvalue.substring(0,tubsvalue.length()-5);System.out.println("after="+tubsvalue);
						}
						tvtubnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tubsvalue));
					}
					
					if(!sinkvalue.equals(""))
					{
						tvsinknote.setVisibility(v.VISIBLE);
						
						if(sinkvalue.endsWith("&#44;"))
						{
							sinkvalue=sinkvalue.substring(0,sinkvalue.length()-5);System.out.println("after="+sinkvalue);
						}
						tvsinknote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+sinkvalue));
					}
					  
					
					if(!showersvalue.equals(""))
					{
						tvbathnote.setVisibility(v.VISIBLE);
						if(showersvalue.endsWith("&#44;"))
						{
							showersvalue=showersvalue.substring(0,showersvalue.length()-5);
						}
						tvbathnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+showersvalue));
					}
					
					if(!safetyglass.equals(""))
					{
						tvsafety.setVisibility(v.VISIBLE);
						if(safetyglass.endsWith("&#44;"))
						{
							safetyglass=safetyglass.substring(0,safetyglass.length()-5);
						}
						tvsafety.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+safetyglass));
					}
					
					if(!toiletsvalue.equals(""))
					{
						if(toiletsvalue.endsWith("&#44;"))
						{
							toiletsvalue=toiletsvalue.substring(0,toiletsvalue.length()-5);
						}
						tvtoiletnote.setVisibility(v.VISIBLE);
						tvtoiletnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+toiletsvalue));
					}
					
					if(!fixturesvalue.equals(""))
					{
						tvbathfixturesnote.setVisibility(v.VISIBLE);
						if(fixturesvalue.endsWith("&#44;"))
						{
							fixturesvalue=fixturesvalue.substring(0,fixturesvalue.length()-5);
						}
						tvbathfixturesnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+fixturesvalue));
					}
					
					if(!watersupplyvalue.equals(""))
					{
						tvwatersupply.setVisibility(v.VISIBLE);
						if(watersupplyvalue.endsWith("&#44;"))
						{
							watersupplyvalue=watersupplyvalue.substring(0,watersupplyvalue.length()-5);
						}
						tvwatersupply.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+watersupplyvalue));
					}
					
					if(!othervalue.equals(""))
					{
						tvother.setVisibility(v.VISIBLE);
						if(othervalue.endsWith("&#44;"))
						{
							othervalue=othervalue.substring(0,othervalue.length()-5);
						}
						tvother.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+othervalue));
					}
					
					if(!otherfeaturesvalue.equals(""))
					{
						tvotherfeatures.setVisibility(v.VISIBLE);
						if(otherfeaturesvalue.endsWith("&#44;"))
						{
							otherfeaturesvalue=otherfeaturesvalue.substring(0,otherfeaturesvalue.length()-5);
						}
						tvotherfeatures.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+otherfeaturesvalue));
					}
					
					((Button)findViewById(R.id.btnsave)).setText("Update");
				}
			}catch(Exception e){}
		
	}
	protected void clear() {
		// TODO Auto-generated method stub
		tubsvalue="";sinkvalue="";showersvalue="";toiletsvalue="";fixturesvalue="";otherfeaturesvalue="";safetyvalue="";watersupplyvalue="";othervalue="";
		tubtv="";sinkdb="";toiletdb="";fixturesdb="";otherfeaturesdb="";safetyglass="";drainage="";bathother="";
		spin_bathname.setSelection(0); getbathname="--Select--";spin_bathname.setEnabled(true);vrint=0;
		/*spin_bathshowerenclosure.setSelection(0);*/getglass="--Select--";getbathnameother="";getdrainageother="";getbathother="";getsafetyshowerother="";
		/*spin_watersupply.setSelection(0);getdrainage="--Select--";
		spin_bathother.setSelection(0);getbathother="--Select--";showerdb="";*/
		temptub="";tempsink="";tempbathroom="";tempsafety="";temptoilet="";tempbathfixtures="";tempwatersupply="";tempother="";tempotherfeatures="";
		tvtubnote.setVisibility(v.GONE);
		tvsinknote.setVisibility(v.GONE);
		tvbathnote.setVisibility(v.GONE);
		tvsafety.setVisibility(v.GONE);
		tvtoiletnote.setVisibility(v.GONE);
		tvbathfixturesnote.setVisibility(v.GONE);
		tvwatersupply.setVisibility(v.GONE);
		tvother.setVisibility(v.GONE);
		tvotherfeatures.setVisibility(v.GONE);
		
		((Button)findViewById(R.id.btnsave)).setText("Save and Add More Bathrooms");
		((EditText)findViewById(R.id.et_bathcomments)).setText("");
	}
	class selonitemlistner implements OnItemSelectedListener
	{
       int k=0;
		public selonitemlistner(int i) {
			// TODO Auto-generated constructor stub
			k=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			switch (k) {
			case 1:
				getbathname = arg0.getItemAtPosition(arg2).toString();
				if(getbathname.equals("Other"))
				{
					ed_otherbathroomname.setVisibility(arg1.VISIBLE);
				}
				else
				{
					ed_otherbathroomname.setText("");
					ed_otherbathroomname.setVisibility(arg1.GONE);
				}
				break;
			case 2:
				getsafetyshower = arg0.getItemAtPosition(arg2).toString();
				if(getsafetyshower.equals("Other"))
				{
					ed_safetyglass.setVisibility(arg1.VISIBLE);
				}
				else
				{
					ed_safetyglass.setText("");
					ed_safetyglass.setVisibility(arg1.INVISIBLE);
				}
				break;
			
		
			case 3:
				getdrainage = arg0.getItemAtPosition(arg2).toString();
				
				if(getdrainage.equals("Other"))
				{
					ed_drainage.setVisibility(arg1.VISIBLE);
				}
				else
				{
					ed_drainage.setText("");
					ed_drainage.setVisibility(arg1.INVISIBLE);
				}
				break;
			case 4:
				
				getbathother = arg0.getItemAtPosition(arg2).toString();
				break;
			default:
				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
			try
			{
				switch (requestCode) 
				{
					case 1:
						System.out.println("tubsvvv"+data.getExtras().getString("tubs"));
						if(!data.getExtras().getString("tubs").equals(""))
						{
							tubsvalue=	data.getExtras().getString("tubs");	
						}
					break;
					
					case 2:
						if(!data.getExtras().getString("sinks").equals(""))
						{
							sinkvalue=	data.getExtras().getString("sinks");	
						}					 	
					break;

					case 3:
						if(!data.getExtras().getString("showers").equals(""))
						{
							showersvalue=	data.getExtras().getString("showers");	
						}		
					break;
					
					case 4:
						if(!data.getExtras().getString("toilets").equals(""))
						{
							toiletsvalue=	data.getExtras().getString("toilets");	
						}		
					break;
					
					case 5:
						if(!data.getExtras().getString("fixtures").equals(""))
						{
							fixturesvalue=	data.getExtras().getString("fixtures");	
						}	
					break;
					
					case 6:
						if(!data.getExtras().getString("otherfeatures").equals(""))
						{
							otherfeaturesvalue=	data.getExtras().getString("otherfeatures");	
						}		
					break;
				}
		}
		catch(Exception e)
		{
			System.out.println("no more issues "+e.getMessage());
		}
		
	}*/
	private void back() 
	{
		// TODO Auto-generated method stub
		
			if(getcurrentview.equals("start"))
			{
				db.userid();
				intent = new Intent(LoadBathroom.this, LoadModules.class);
				Bundle b1 = new Bundle();
				b1.putString("selectedid", getph);			
				intent.putExtras(b1);
				
				Bundle b2 = new Bundle();
				b2.putString("inspectorid", db.UserId);			
				intent.putExtras(b2);
				startActivity(intent);System.out.println("finished");
				finish();
			}
			else
			{	
				if(gettempoption.equals("Report Section/Module"))
		        {
		       	  intent = new Intent(LoadBathroom.this, CreateTemplate.class);
		        }
		        else 
		        {
		       	 intent = new Intent(LoadBathroom.this, CreateModule.class);
		        }
				System.out.println("gettempname="+gettempname+"getcurrentview="+getcurrentview);
				Bundle b6 = new Bundle();
				b6.putString("templatename", gettempname);			
				intent.putExtras(b6);System.out.println("sucess");
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("tempoption", gettempoption);			
				intent.putExtras(b5);
				startActivity(intent);System.out.println("finished");
				finish();
			}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	protected void onSaveInstanceState(Bundle ext)
	{
		/* if (p_sv != null) {
			  p_sv.progress_live=false;
		 }*/
		
		ext.putInt("addnew_dialog", dialog_other);
		if(dialog_other==1)
	    {
	      	ext.putString("othertext_index", etother.getText().toString());
	    }
		ext.putString("selected_option", bathvalue);
		
		ext.putInt("flagvalue", flag);		
		/* selectedval="";
	       	for (int i = 0; i < str_arrlst.size(); i++) {
	   			if (ch[i].isChecked()) {
	   				selectedval += ch[i].getText().toString() + "&#44;";
	   			}
	   		}*/
	       	System.out.println("for loop"+seltdval);
	    ext.putInt("alertbath", bath);
	    ext.putString("alertcheck_value", seltdval); 
	   	ext.putInt("alertcheck_index", k);     	
	   	ext.putInt("handler", handler_msg);
	   	ext.putString("tubsvalue",tubsvalue);
	   	ext.putString("sinkvalue",sinkvalue);
	   	ext.putString("tubsdata",temptub);
	   	ext.putString("sinkdata",tempsink);
		super.onSaveInstanceState(ext);
	}
	protected void onRestoreInstanceState(Bundle ext)
	{
		
		dialog_other = ext.getInt("addnew_dialog");
		bathvalue = ext.getString("selected_option");
		tubsvalue = ext.getString("tubsvalue");
		sinkvalue = ext.getString("sinkvalue");
		
		seltdval = ext.getString("alertcheck_value");
		if(ext.getInt("alertbath")==1)
		{
		     multi_select(bathvalue, ext.getInt("alertcheck_index"));System.out.println("rest="+seltdval);
		     if(!seltdval.equals(""))
		     {				
		    	  cf.setValuetoCheckbox(ch, seltdval);
		     }
	    }
		   if(dialog_other==1)
		   {
			   dialogshow(bathvalue);
			   strother = ext.getString("othertext_index");
			   etother.setText(strother);		   
		   }		
		   flag = ext.getInt("flagvalue");
		   handler_msg = ext.getInt("handler");
		   if(handler_msg==0)
	    	{
	    		success_alert();
	    	}
	    	else if(handler_msg==1)
	    	{
	    		failure_alert();
	    	}
		   temptub = ext.getString("tubsdata");
		   if(!temptub.equals("")&&!temptub.equals("--Select--"))
		   {
			   temptub = cf.replacelastendswithcomma(temptub);
			   tvtubnote.setText("Selected: "+temptub);
		   }
		   
		   tempsink = ext.getString("sinkdata");
		   if(!tempsink.equals("")&&!tempsink.equals("--Select--"))
		   {
			   tempsink = cf.replacelastendswithcomma(tempsink);
			   tvsinknote.setText("Selected: "+tempsink);
		   }
		 super.onRestoreInstanceState(ext);
	}
}