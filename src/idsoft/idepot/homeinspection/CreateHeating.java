package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.LoadBathroom.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Currency;
import java.util.Random;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CreateHeating extends Activity {
	String  gettempname="",getcurrentview="",getmodulename="",gettempoption="",getselectedho="",getspinselectedname="",getheatingnum="",getheatingoperation="",getheatingtype="",
			getheatexchanger="",getdistribution="",getcirculator="",getdrain="",getfusepipe="",getdevious="",gethumidifier="",getthermostats="",getheatbrand="",getwatersystemtype="",
			getfueltank="",getasbestos="",strotherheatingnum="",strotherheatingexchanger="",strotherdistribution="",strothercirculator="",strotherdrain="",
			strotherdevious="",strotherhumidifier="",strotherthermostats="",strotherfueltank="",strmanufacturer="",strmodelnumber="",strserialnumber="",strcapacity="",
			strotherwoodstoves="",dbheatingnum="", dbheatingnumother="", dbheatingsystem="", dbmanufacturer="", dbmodelnumber="",getenergysource="",
			getwoodstoves="",getmotorblower="",getradiators="",getsupplyregister="",getreturnregister="",getfurnace="",getfurnfeatures="",
			strcoolingenergysource="",strcoolingnoofzones="",strcoolingage="",getcoolingequipment="",getcoolingenrgysource="",getcenterairmanuf="",getcoolinoofzones=""
	    			  ,getcoolingage="",getconddischarge="",getacsystem="",getgaslog="",strzonename="",strzonetonnage="",
	    					  getzonename="",getzoneunittype="",getzone5yearrep="",gezonetestedfor="",getzonetonnage="",
			strotherheattype="",strenergysource="",strnoofheatsystems="",getheatdeliverytype="",getheatingno="",getheatingage="",strotherheatingage="";;
	TextView tv_seltemplate,tv_modulename,tv_noinfo;
	Button btnback,btnhome,btnheatadd,btnheatcancel,btncooladd,btnzoneadd,btncancel,btncoolcancel,btnzonecancel,btntakephoto;
	Cursor cur;
	String title="",error_msg="",word="",tempname="";
	int  updzone=0, updheating=0,udpcooling=0,submitalert=0,spintouch=0;
	Spinner sp_coolingequipment,sp_coolingenergysource,sp_noofcoolingzones,sp_coolingage,sp_coolingmanufac,sp_condensdisc,sp_coolingactesting,sp_coolingthermostats,sp_coolinggaslog,sp_condensdischarge;
	Spinner sp_zonename,sp_zonetonnage,sp_zonesunittype,sp_zonestestedfor,sp_zones5year;
	ArrayAdapter zonenameadap,zonetonnadap,zonetestedforadap,zone5yearadap,zoneunittypeadap;
	String arrzonename[]={"--Select--","Bonus Room","Main Area","Master Bedroom","Second Floor","Other"},arrzoneunitype[]={"--Select--","Internal Unit","External Unit"},
			arrzone5year[]={"--Select--","High","Low","Medium"},arrzonetestingfor[]={"--Select--","Tested For Cooling","Tested For Heating","Tested For Emergency Heat Only"},
			arrzonetonnage[]={"--Select--",	"1.5 Ton","1.5 Ton/3.5 Ton","2 � 3 Ton","2 � 5 Ton","2 Ton","2 Ton/4 Ton","2.5 Ton","3 Ton","3.5 Ton","4 Ton","4.5 Ton","5 Ton","Other"};
	ArrayAdapter coolinnequiadapp,coolingenersourceadap,noofcoolingzonesadap,coolingageadap,coolingmanuadap,conddiscadap,coolingactestingadap,coolingthermostatsadap,coolinggaslogadap;
	String arrcoolingequip[]={"--Select--","Heat Pump Forced Air","Packaged Central Air - Heat Pump","Air Conditioner Unit","Window Unit","Wall Unit","Swamp Cooler","Water To Air System","Cooling Only System","Uses Heat Ductwork","Refrigerant System","System Aged/Operable"},
	arrcoolenersource[]={"--Select--","Electricity","Solar","Other"},
	arrcoolingmanu[]={"--Select--","Aged","Amana","American Standard","Americanaire","Ao Smith","Apollo"," Aquatherm","Arcoaire","Armstrong","Bryant","Burnham","Carrier","Climate Master","Coleman","Comfort Maker","Crown","Empire","Envirotemp","Frigidaire","Goodman","Hallmark","Heatwave","Heil","Int.Comfort","Intertherm","Janitrol","Kenmore","Lennox","Luxaire","Magic Chef","Monitor","Ninety Two","None","Oneida","Patriot","Payne","Peerless","Presidential","Quattro","Rheem","Rinnai","Ruud","Sears,Singer","Tempstar","Thermopride",
			"Toyostove","Trane","Unknown","Weatherking","Weil Mclean","York","Zephyr"},arrcoolinngzones[]={"--Select--","1","2","3","4","5","6","Other"},
			arrcoolingage[]={"--Select--","1 Year","2 Year","3 Year","4 Year","5 Year","6 Year","7 Year","8 Year","9 Year","10 Year","Other"},
			arrcondendischarge[]={"--Select--","Overflow Condensate Tray","Condensate Pipe To Sump","Condensate Pipe","Condensate Pipe To Exterior","Float Switch/Pump"},
			arrcoolacsystem[]={"--Select--","Reverse Valve Not Checked","Outside Temperature Prohibited Test"},
			arrcoolthermostats[]= {"--Select--","Manual Thermostat","Programmable Thermostat","Set Back Thermostat","Heat Pump Thermostat","Multiple Zone","Separate Heating/Cooling Thermostat","Aquastat"},
			arrgaslog[]={"--Select--","Direct Vent Gas Log","Unvented Gas Log","No Gas To Test Systems","Remote Control Gas Log"};	
	Spinner sp_heatingnumber,sp_heatsystem,sp_heatingtype,sp_heatexchanger,sp_distribution,sp_circulator,sp_draincontrol,sp_fuse,sp_devious,sp_humidifier,
	sp_watersystype,spin_supplyregister,sp_returnregister,sp_furnfeat,sp_thermostats,sp_fueltank,sp_asbestos,sp_energysource,spin_heatingage,sp_heatbrand,sp_furnace,sp_heatdelivery,spin_radiators,sp_radiators,sp_motorblower,sp_woodstoves;
	String[] arrheatingnum={"--Select--","1","2","3","4","5","6","Other"},strheatsytem ={"--Select--","Adequate","Inadequate","Appears Functional","Recommend Replacement"},
			arrheattype={"--Select--","Forced Air","Heat Pump Forced Air","Central Air - Cool/Air Conditioning Only","Packaged Central Air - Heat Pump","Packaged Central Air - Cool/Air Conditioning Only","Wall Unit(s)","Heat Strip","None","Electric Base","Electric Heat","Radiant Floor","Space Heater","Steam Boiler","Circulating Boiler",
	"Coal Furnace Converted To Gas","Coal Furnace Converted To Oil","Hydronic System","Oil Furnace Converted To Gas","Old Oil Furnace As Back Up To Heat Pump","Radiant Ceiling","Other"},arrheatexchanger={"--Select--","1 Burner","2 Burner","3 Burner",
			"4 Burner","5 Burner","6 Burner","Other"},arrdistribution={"--Select--","Hot Water","Steam","Radiator","Metal Duct","One Pipe","Two Pipe","Other"},
			arrcirculator={"--Select--","Pump","Gravity","Other"},arrdrain={"--Select--","Manual","Automatic","Other"},arrfuse={"--Select--","Single Wall","Double Wall","PVC"},
			arrdevious={"--Select--","Temp Guage","Expansion Tank","Pressure Gauge","Other"},arrhumidifier={"--Select--","Aprilaire","Honeywell","Other"},arrthermostats={"--Select--","Individual",
			"Multi Zone","Programmable","Other"},arrfueltank={"--Select--","Above Ground","Below Ground","Not Applicable","Above Ground Oil Tank","Below Ground Oil Tank","Original Oil Tank","Oil Tank Fill Cap and Vent Not Located","Tank Fill and Vent Located","Portable Gas Tank","Gas Tank Below Ground","Underground Gas Tank Fill Valve Not Located","Underground Gas Tank Not Located","Underground Gas Tank"},
			arrasbestos={"--Select--","Yes","No"},arrheatingnumber,arrheatingnumother,arrheatingsysteme ,arrmanufacture,arrmodelnumber,
			arrenergy={"--Select--","Oil","Gas","Electric","Water","Liquid Propane","Kerosene","Solar","Wood","Coal","Corn","Natural Gas","Other"},
					arrheatbrand={"--Select--","Aged","Amana","American Standard","Americanaire","Ao Smith","Apollo","Aquatherm","Arcoaire","Armstrong",
			"Bryant","Burnham","Carrier","Climate Master","Coleman","Comfort Maker","Crown","Empire","Envirotemp","Frigidaire","Goodman",
			"Hallmark","Heatwave","Heil","Int.Comfort","Intertherm","Janitrol","Kenmore","Lennox","Luxaire","Magic Chef","Monitor","Ninety Two",
			"None","Oneida","Patriot","Payne","Peerless","Presidential","Quattro","Rheem","Rinnai","Ruud","Sears","Singer","Tempstar","Thermopride",
			"Toyostove","Trane","Unknown","Weatherking","Weil Mclean","York","Zephyr"},
			arrheatdelivery={"--Select--","Refrigerant","Electric Resistance Heat","Hot Water Heat","Steam Heat","Radiant","Boiler","Forced Air"},
			arrwatersystem={"--Select--","One Pipe System","Two Pipe System","Fin Coils","Boiler","Subfloor","Convectors","Radiators","Baseboard"},
			arrradiators={"--Select--","Cast Iron Radiators","Steel Radiators","Manual Fill","Water Level Sight Glass","Bleed Valves"},
			arrmotorblower={"--Select--","Blower Fan","Direct Drive","Belt Driven"},
			arrwoodstoves={"--Select--","1","2","3","4","5","None","Other"},
			arrregister={"--Select--","Low","High"},
			arrretunrregister={"--Select--","Low","High"},
			arrfurnacefeatures={"--Select--","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner"," Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light"," Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Air Filter"},
			arrfurnace={"--Select--","Air To Air Heat Pump","Water To Air Heat Pump","Forced Air Furnace","Induced Air Furnace","Gravity Furnace","Evaporated Located Over Furnace","Natural Draft System","Condensation Furnace System"};
	ArrayAdapter heatnumarrayadap,heatsystemarrayadap,heattypearrayadap,heatexchangerarrayadap,distributionarrayadap,circulatorarrayadap,drainarrayadap,fusearrayadap,deviousarrayadap,
	watersysarrayadap,returnregarraydap,supplyregarraydap,furnfeatarraydap, humidifierarrayadap,thermostatsarrayadap,fueltankarrayadap,asbestosarrayadap,energysourcearrayadap,agearrayadap,brandarrayadap,furnacearrayadap,heatdeliveryarrayadap,radiatorsarrayadap,motorarrayadap,woodstoveadap;
	EditText et_otherheatingnum,et_manufacturer,et_modelnumber,et_serialnumber,et_capacity,et_otherheatexchanger,et_otherdistribution,et_othercirculator,
	         et_otherdrain,et_otherdevious,et_otherhumidifier,et_otherthermostats,et_otherfueltank;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	View v1;
	Cursor selcur;
	String arrtempname[];
	ArrayAdapter adapter;
	Spinner spinloadtemplate;
	EditText wrdedit;
	static int flag1=0,flag2=0,flag3=0;
	EditText ed_otherheattype,ed_otherenergysource,ed_otherheatignno,ed_otherheatingage,ed_otherwoodstoves,ed_othercoolingeergysource,
	ed_coolingnoofzones,ed_coolingage,ed_otherzonename,ed_otherzonetonnage;
	Progress_staticvalues p_sv;
	private int handler_msg=2;
	ListView heatinglist;
	private static final Random rgenerator = new Random();
	View v;TableLayout hvactblshow,heatingtblshow,coolingtblshow,zonestblshow;
	String HVAC_in_DB[];
	int Heating_select_id,Cooling_select_id,Zones_select_id,vrint=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			
			System.out.println("CreateHeating");
		
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}		
		//setContentView(R.layout.activity_createheating);
		setContentView(R.layout.activity_hvac);
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(20);
		db.CreateTable(21);
		db.CreateTable(22);
		db.CreateTable(23);
		db.CreateTable(24);
		db.CreateTable(25);
		db.userid();
		System.out.println("currentview"+getcurrentview);
		
		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"'", null);
            if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		
    	((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));   
		heatingtblshow= (TableLayout)findViewById(R.id.heathvactable);
		coolingtblshow= (TableLayout)findViewById(R.id.coolhvactable);
		zonestblshow= (TableLayout)findViewById(R.id.Zonestable);
		TextView vc = ((TextView)findViewById(R.id.vc));
		vc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent = new Intent(CreateHeating.this, NewSetVC.class);
					Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", "create");			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("submodulename", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("tempoption", gettempoption);			
					intent.putExtras(b6);
					System.out.println("gettempoptionsubmod="+gettempoption);
					startActivity(intent);
					finish();
			}
		});
		/* Heating declaration starts */
		
		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(CreateHeating.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							Bundle b4 = new Bundle();
							b4.putString("currentview","takephoto");			
							intent.putExtras(b4);
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getselectedho);			
							intent.putExtras(b5);
							startActivity(intent);
							finish();
			}
		});
		
		sp_heatingtype = (Spinner)findViewById(R.id.spin_heattype);
		heattypearrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrheattype);
		heattypearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_heatingtype.setAdapter(heattypearrayadap);
		sp_heatingtype.setOnItemSelectedListener(new myspinselectedlistener(3));
		ed_otherheattype= (EditText)findViewById(R.id.other_heatingtype);
		
		sp_energysource = (Spinner)findViewById(R.id.spin_energysource);
		energysourcearrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrenergy);
		energysourcearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_energysource.setAdapter(energysourcearrayadap);
		sp_energysource.setOnItemSelectedListener(new myspinselectedlistener(2));
		ed_otherenergysource= (EditText)findViewById(R.id.other_energysource);
		
		
		sp_heatingnumber = (Spinner)findViewById(R.id.spin_heatingsystemnum);
		heatnumarrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrheatingnum);
		heatnumarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_heatingnumber.setAdapter(heatnumarrayadap);
		sp_heatingnumber.setOnItemSelectedListener(new myspinselectedlistener(4));
		ed_otherheatignno= (EditText)findViewById(R.id.other_heatingno);
		
		spin_heatingage = (Spinner)findViewById(R.id.spin_heatingage);
		agearrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcoolingage);
		agearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_heatingage.setAdapter(agearrayadap);
		spin_heatingage.setOnItemSelectedListener(new myspinselectedlistener(5));
		ed_otherheatingage= (EditText)findViewById(R.id.other_heatingage);
		
		sp_heatbrand = (Spinner)findViewById(R.id.spin_heatbrand);
		brandarrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrheatbrand);
		brandarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		sp_heatbrand.setAdapter(brandarrayadap);
		sp_heatbrand.setOnItemSelectedListener(new myspinselectedlistener(6));
		
		sp_furnace = (Spinner)findViewById(R.id.spin_furnace);
		furnacearrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrfurnace);
		furnacearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		sp_furnace.setAdapter(furnacearrayadap);
		sp_furnace.setOnItemSelectedListener(new myspinselectedlistener(7));
		
		sp_heatdelivery = (Spinner)findViewById(R.id.spin_heatdelivery);
		heatdeliveryarrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrheatdelivery);
		heatdeliveryarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_heatdelivery.setAdapter(heatdeliveryarrayadap);
		sp_heatdelivery.setOnItemSelectedListener(new myspinselectedlistener(1));
		
		
		sp_watersystype = (Spinner)findViewById(R.id.spin_watersystype);
		watersysarrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrwatersystem);
		watersysarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_watersystype.setAdapter(watersysarrayadap);
		sp_watersystype.setOnItemSelectedListener(new myspinselectedlistener(8));
		
		
		sp_radiators = (Spinner)findViewById(R.id.spin_radiators);
		radiatorsarrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrradiators);
		radiatorsarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_radiators.setAdapter(radiatorsarrayadap);
		sp_radiators.setOnItemSelectedListener(new myspinselectedlistener(9));
		
		
		
		sp_motorblower = (Spinner)findViewById(R.id.spin_motorblowers);
		motorarrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrmotorblower);
		motorarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_motorblower.setAdapter(motorarrayadap);
		sp_motorblower.setOnItemSelectedListener(new myspinselectedlistener(10));
		
		
		sp_woodstoves = (Spinner)findViewById(R.id.spin_noofwoodstoves);
		woodstoveadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrwoodstoves);
		woodstoveadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_woodstoves.setAdapter(woodstoveadap);
		sp_woodstoves.setOnItemSelectedListener(new myspinselectedlistener(11));		
		ed_otherwoodstoves= (EditText)findViewById(R.id.other_woodstoves);
		
		
		sp_fueltank = (Spinner)findViewById(R.id.spin_fueltanklocation);
		fueltankarrayadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrfueltank);
		fueltankarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_fueltank.setAdapter(fueltankarrayadap);
		sp_fueltank.setOnItemSelectedListener(new myspinselectedlistener(12));
		
		
		sp_furnfeat = (Spinner)findViewById(R.id.spin_furnfeatures);
		furnfeatarraydap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrfurnacefeatures);
		furnfeatarraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_furnfeat.setAdapter(furnfeatarraydap);
		sp_furnfeat.setOnItemSelectedListener(new myspinselectedlistener(13));
		
		
		spin_supplyregister = (Spinner)findViewById(R.id.spin_supplyregister);
		supplyregarraydap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrregister);
		supplyregarraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_supplyregister.setAdapter(supplyregarraydap);
		spin_supplyregister.setOnItemSelectedListener(new myspinselectedlistener(14));
		
		
		sp_returnregister = (Spinner)findViewById(R.id.spin_returnregister);
		returnregarraydap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrretunrregister);
		returnregarraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_returnregister.setAdapter(returnregarraydap);
		sp_returnregister.setOnItemSelectedListener(new myspinselectedlistener(15));
		
		
		/* Heating declaration ends */
		
		
		/* Cooling declaration starts */
		
		sp_coolingequipment = (Spinner)findViewById(R.id.spin_coolingequipment);
		coolinnequiadapp = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcoolingequip);
		coolinnequiadapp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_coolingequipment.setAdapter(coolinnequiadapp);
		sp_coolingequipment.setOnItemSelectedListener(new myspinselectedlistener(16));
		
		
		sp_coolingenergysource = (Spinner)findViewById(R.id.spin_coolingenergysource);
		coolingenersourceadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcoolenersource);
		coolingenersourceadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_coolingenergysource.setAdapter(coolingenersourceadap);
		sp_coolingenergysource.setOnItemSelectedListener(new myspinselectedlistener(17));
		ed_othercoolingeergysource= (EditText)findViewById(R.id.other_coolenergysource);
		
		sp_coolingmanufac= (Spinner)findViewById(R.id.spin_coolingmanu);
		coolingmanuadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcoolingmanu);
		coolingmanuadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_coolingmanufac.setAdapter(coolingmanuadap);
		sp_coolingmanufac.setOnItemSelectedListener(new myspinselectedlistener(18));
		
		
		sp_noofcoolingzones = (Spinner)findViewById(R.id.spin_noofcoolingzones);
		noofcoolingzonesadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcoolinngzones);
		noofcoolingzonesadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_noofcoolingzones.setAdapter(noofcoolingzonesadap);
		sp_noofcoolingzones.setOnItemSelectedListener(new myspinselectedlistener(19));
		ed_coolingnoofzones= (EditText)findViewById(R.id.other_coolingzones);
		
		sp_coolingage = (Spinner)findViewById(R.id.spin_coolingage);
		coolingageadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcoolingage);
		coolingageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_coolingage.setAdapter(coolingageadap);
		sp_coolingage.setOnItemSelectedListener(new myspinselectedlistener(20));
		ed_coolingage= (EditText)findViewById(R.id.other_coolingage);
		
		sp_condensdischarge = (Spinner)findViewById(R.id.spin_condensdischarge);
		conddiscadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcondendischarge);
		conddiscadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_condensdischarge.setAdapter(conddiscadap);
		sp_condensdischarge.setOnItemSelectedListener(new myspinselectedlistener(21));
		
		sp_coolingactesting = (Spinner)findViewById(R.id.spin_coolingactesting);
		coolingactestingadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcoolacsystem);
		coolingactestingadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_coolingactesting.setAdapter(coolingactestingadap);		
		sp_coolingactesting.setOnItemSelectedListener(new myspinselectedlistener(22));
		
		sp_coolingthermostats = (Spinner)findViewById(R.id.spin_coolingthermostats);
		coolingthermostatsadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrcoolthermostats);
		coolingthermostatsadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_coolingthermostats.setAdapter(coolingthermostatsadap);
		sp_coolingthermostats.setOnItemSelectedListener(new myspinselectedlistener(23));
		
		sp_coolinggaslog = (Spinner)findViewById(R.id.spin_coolinggaslog);
		coolinggaslogadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrgaslog);
		coolinggaslogadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_coolinggaslog.setAdapter(coolinggaslogadap);
		sp_coolinggaslog.setOnItemSelectedListener(new myspinselectedlistener(24));
		
		/* Cooling declaration ends */
		
		
		/* Zones declaration starts */
		
		
		sp_zonename = (Spinner)findViewById(R.id.spin_zonename);
		zonenameadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrzonename);
		zonenameadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonename.setAdapter(zonenameadap);
		sp_zonename.setOnItemSelectedListener(new myspinselectedlistener(25));
		ed_otherzonename= (EditText)findViewById(R.id.other_zonename);
		
		
		sp_zonesunittype = (Spinner)findViewById(R.id.spin_zonesunittype);
		zoneunittypeadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrzoneunitype);
		zoneunittypeadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonesunittype.setAdapter(zoneunittypeadap);
		sp_zonesunittype.setOnItemSelectedListener(new myspinselectedlistener(26));
		
		sp_zones5year = (Spinner)findViewById(R.id.spin_zone5year);
		zone5yearadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrzone5year);
		zone5yearadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zones5year.setAdapter(zone5yearadap);
		sp_zones5year.setOnItemSelectedListener(new myspinselectedlistener(27));
		
		sp_zonestestedfor = (Spinner)findViewById(R.id.spin_zonestestedfor);
		zonetestedforadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrzonetestingfor);
		zonetestedforadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonestestedfor.setAdapter(zonetestedforadap);
		sp_zonestestedfor.setOnItemSelectedListener(new myspinselectedlistener(28));
		
		sp_zonetonnage = (Spinner)findViewById(R.id.spin_zonestonnage);
		zonetonnadap = new ArrayAdapter<String>(CreateHeating.this,android.R.layout.simple_spinner_item,arrzonetonnage);
		zonetonnadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonetonnage.setAdapter(zonetonnadap);
		sp_zonetonnage.setOnItemSelectedListener(new myspinselectedlistener(29));
		ed_otherzonetonnage= (EditText)findViewById(R.id.other_zontonnage);		
		/* Zones declaration ends */
		
	  	Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(CreateHeating.this,HomeScreen.class));
					finish();
					 
				}
			});
	        
		
			btnsubmit.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(getcurrentview.equals("start"))
					{
						submitalert();
					}
					else
					{
						if(cf.isInternetOn())
						  {
							   new Start_xporting().execute("");
						  }
						  else
						  {
							  cf.DisplayToast("Please enable your internet connection.");
							  
						  } 	
					}  
				}
			});
		
		btncoolcancel = (Button)findViewById(R.id.btn_coolcancel);
		btncoolcancel.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
					clearcooling();			
			}
		});
		
		btnheatcancel = (Button)findViewById(R.id.btn_heatcancel);
		btnheatcancel.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
					clearheating();			
			}
		});
		
		btnzonecancel = (Button)findViewById(R.id.btn_cancelzones);
		btnzonecancel.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
					clearzones();			
			}
		});
		
		btnheatadd = (Button)findViewById(R.id.btn_heatadd);
		btnheatadd.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				if(sp_heatingtype.getSelectedItem().toString().equals("--Select--"))
				{
					 cf.DisplayToast("Please select Heat Type");
				}
				else
				{
					strotherheattype = ed_otherheattype.getText().toString();
					strenergysource = ed_otherenergysource.getText().toString();
					strnoofheatsystems = ed_otherheatignno.getText().toString();
					strotherwoodstoves	= ed_otherwoodstoves.getText().toString();			
					strotherheatingage	= ed_otherheatingage.getText().toString();
					strotherheatingnum= ed_otherheatignno.getText().toString();
					if(((Button)findViewById(R.id.btn_heatadd)).getText().toString().equals("Update"))
					{
						if(getcurrentview.equals("start"))
						{
							db.hi_db.execSQL("UPDATE  "+db.LoadHeating+" SET fld_energysource='"+getenergysource+"',fld_energysourceother='"+db.encode(strenergysource)+"',"+
							           "fld_heatingno='"+db.encode(getheatingno)+"',fld_otherheatingno='"+db.encode(strotherheatingnum)+"',"+
									   "fld_age='"+getheatingage+"',fld_otherage='"+db.encode(strotherheatingage)+"',"+
							           "fld_heatsystembrand='"+getheatbrand+"',fld_furnacetype='"+getfurnace+"',fld_heatdeliverytype='"+getheatdeliverytype+"',fld_motorblower='"+getmotorblower+"',fld_noofstoves='"+getwoodstoves+"',"+
							           "fld_othernoofstoves='"+db.encode(strotherwoodstoves)+"',fld_furnacefeatuers='"+getfurnfeatures+"',fld_supplyregister='"+getsupplyregister+"',fld_retunrregister='"+getreturnregister+"',"+
							           "fld_radiators='"+getradiators+"',fld_watersystemtype='"+getwatersystemtype+"',fld_fueltanklocation='"+getfueltank+"'  Where h_id='"+Heating_select_id+"' and  fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'");
						}
						else
						{
							db.hi_db.execSQL("UPDATE  "+db.CreateHeating+" SET fld_energysource='"+getenergysource+"',fld_energysourceother='"+db.encode(strenergysource)+"',"+
							           "fld_heatingno='"+db.encode(getheatingno)+"',fld_otherheatingno='"+db.encode(strotherheatingnum)+"',"+
									   "fld_age='"+getheatingage+"',fld_otherage='"+db.encode(strotherheatingage)+"',"+
							           "fld_heatsystembrand='"+getheatbrand+"',fld_furnacetype='"+getfurnace+"',fld_heatdeliverytype='"+getheatdeliverytype+"',fld_motorblower='"+getmotorblower+"',fld_noofstoves='"+getwoodstoves+"',"+
							           "fld_othernoofstoves='"+db.encode(strotherwoodstoves)+"',fld_furnacefeatuers='"+getfurnfeatures+"',fld_supplyregister='"+getsupplyregister+"',fld_retunrregister='"+getreturnregister+"',"+
							           "fld_radiators='"+getradiators+"',fld_watersystemtype='"+getwatersystemtype+"',fld_fueltanklocation='"+getfueltank+"'  Where h_id='"+Heating_select_id+"' and  fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
							
						}
			
						
						cf.DisplayToast("Heating system saved successfully");
						clearheating();
	                	displayheating();
					}
					else
					{
					
						try
						{
							if(getcurrentview.equals("start"))
							{
							
									Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+getheatingtype+"'", null);
									if(cur.getCount()==0)
									{
										db.hi_db.execSQL(" INSERT INTO "+db.LoadHeating+" (fld_templatename,fld_inspectorid,fld_srid,fld_heatintype,fld_heatingtypeother,fld_energysource,fld_energysourceother,fld_heatingno,fld_otherheatingno,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_motorblower,fld_noofstoves,fld_othernoofstoves,fld_furnacefeatuers ,fld_supplyregister,fld_retunrregister,fld_radiators,fld_watersystemtype ,fld_fueltanklocation) VALUES" +
											    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+getheatingtype+"','"+db.encode(strotherheattype)+"','"+getenergysource+"','"+db.encode(strenergysource)+"','"+getheatingno+"',"+
												 "'"+db.encode(strnoofheatsystems)+"','"+getheatingage+"','"+db.encode(strotherheatingage)+"','"+getheatbrand+"','"+getfurnace+"','"+getheatdeliverytype+"',"+
											    "'"+getmotorblower+"','"+getwoodstoves+"','"+db.encode(strotherwoodstoves)+"','"+getfurnfeatures+"','"+getsupplyregister+"','"+getreturnregister+"',"+
												"'"+getradiators+"','"+getwatersystemtype+"','"+getfueltank+"')");
										
										cf.DisplayToast("Heating system saved successfully");
										clearheating();
										displayheating();
									}
									else
									{
										 cf.DisplayToast("Already Exists!! Please select another Heat Type");
									}
							}
							else
							{
								Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+getheatingtype+"'", null);
								if(cur.getCount()==0)
								{
									db.hi_db.execSQL(" INSERT INTO "+db.CreateHeating+" (fld_templatename,fld_inspectorid,fld_heatintype,fld_heatingtypeother,fld_energysource,fld_energysourceother,fld_heatingno,fld_otherheatingno,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_motorblower,fld_noofstoves,fld_othernoofstoves,fld_furnacefeatuers ,fld_supplyregister,fld_retunrregister,fld_radiators,fld_watersystemtype ,fld_fueltanklocation) VALUES" +
													    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getheatingtype+"','"+db.encode(strotherheattype)+"','"+getenergysource+"','"+db.encode(strenergysource)+"','"+getheatingno+"',"+
														 "'"+db.encode(strnoofheatsystems)+"','"+getheatingage+"','"+db.encode(strotherheatingage)+"','"+getheatbrand+"','"+getfurnace+"','"+getheatdeliverytype+"',"+
													    "'"+getmotorblower+"','"+getwoodstoves+"','"+db.encode(strotherwoodstoves)+"','"+getfurnfeatures+"','"+getsupplyregister+"','"+getreturnregister+"',"+
														"'"+getradiators+"','"+getwatersystemtype+"','"+getfueltank+"')");
									cf.DisplayToast("Heating system saved successfully");
									clearheating();
									displayheating();
								}
								else
								{
								   cf.DisplayToast("Already Exists!! Please select another Heat Type");
								}
							}
						}
						catch (Exception e) {
							// TODO: handle exception
							System.out.println("addheating="+e.getMessage());
						}
					}
				}
			}
		});
		
		btnzoneadd = (Button)findViewById(R.id.btn_addzones);
		btnzoneadd.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				if(sp_zonename.getSelectedItem().toString().equals("--Select--"))
				{
					 cf.DisplayToast("Please select Zone Name.");
				}
				else
				{
					strzonename = ed_otherzonename.getText().toString();
					strzonetonnage = ed_otherzonetonnage.getText().toString();
					
					if(((Button)findViewById(R.id.btn_addzones)).getText().toString().equals("Update"))
					{
						if(getcurrentview.equals("start"))
						{
							db.hi_db.execSQL("UPDATE  "+db.LoadZones+" SET fla_zone5yearreplacement='"+getzone5yearrep+"'," +
									"fld_zonetestedfor='"+gezonetestedfor+"',fld_unitype='"+getzoneunittype+"',"+
							           "fld_zonetonnage='"+getzonetonnage+"',fld_zonetonnageother='"+db.encode(strzonetonnage)+"' Where z_id='"+Zones_select_id+"' and  fld_inspectorid='"+db.UserId+"'  and fld_srid='"+getselectedho+"'  and fld_templatename='"+db.encode(gettempname)+"'");
						}
						else
						{
							db.hi_db.execSQL("UPDATE  "+db.CreateZones+" SET fla_zone5yearreplacement='"+getzone5yearrep+"'," +
									"fld_zonetestedfor='"+gezonetestedfor+"',fld_unitype='"+getzoneunittype+"',"+
							           "fld_zonetonnage='"+getzonetonnage+"',fld_zonetonnageother='"+db.encode(strzonetonnage)+"' Where z_id='"+Zones_select_id+"' and  fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
						}			
						cf.DisplayToast("Zones saved successfully");
						clearzones();
						displayzones();
					}
					else
					{					
						try
						{
							if(getcurrentview.equals("start"))
							{							
									Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadZones + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zonename='"+getzonename+"'", null);
									if(cur.getCount()==0)
									{
										db.hi_db.execSQL(" INSERT INTO "+db.LoadZones+" (fld_templatename,fld_inspectorid,fld_srid,fld_zonename,fld_zonenameother,fld_unitype,fla_zone5yearreplacement,fld_zonetestedfor,fld_zonetonnage,fld_zonetonnageother) VALUES" +
											    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+getzonename+"','"+db.encode(strzonename)+"','"+getzoneunittype+"','"+getzone5yearrep+"','"+gezonetestedfor+"','"+getzonetonnage+"','"+db.encode(strzonetonnage)+"')");
							
										cf.DisplayToast("Zones saved successfully");
										clearzones();
										displayzones();
									}
									else
									{
										cf.DisplayToast("Already Exists!! Please select another different Zone Name.");
									}
							}
							else
							{
								Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateZones + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zonename='"+getzonename+"'", null);
								if(cur.getCount()==0)
								{
									System.out.println();
									db.hi_db.execSQL(" INSERT INTO "+db.CreateZones+" (fld_templatename,fld_inspectorid,fld_zonename,fld_zonenameother,fld_unitype,fla_zone5yearreplacement,fld_zonetestedfor,fld_zonetonnage,fld_zonetonnageother) VALUES" +
													    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getzonename+"','"+db.encode(strzonename)+"','"+getzoneunittype+"','"+getzone5yearrep+"','"+gezonetestedfor+"','"+getzonetonnage+"','"+db.encode(strzonetonnage)+"')");
									cf.DisplayToast("Zones saved successfully");
									clearzones();
									displayzones();
								}
								else
								{
									cf.DisplayToast("Already Exists!! Please select another different Zone Name.");
								}
							}
						}
						catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
		
		
		
		
		btncooladd = (Button)findViewById(R.id.btn_cooladd);
		btncooladd.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				if(sp_coolingequipment.getSelectedItem().toString().equals("--Select--"))
				{
					 cf.DisplayToast("Please select Cooling Equipment Type");
				}
				else
				{
					strcoolingenergysource = ed_othercoolingeergysource.getText().toString();
					strcoolingnoofzones = ed_coolingnoofzones.getText().toString();
					strcoolingage = ed_coolingage.getText().toString();
					
					if(((Button)findViewById(R.id.btn_cooladd)).getText().toString().equals("Update"))
					{
						if(getcurrentview.equals("start"))
						{
							db.hi_db.execSQL("UPDATE  "+db.LoadCooling+" SET fld_coolingenergysource='"+getcoolingenrgysource+"',fld_coolingenergysourceother='"+db.encode(strcoolingenergysource)+"',"+
							           "fld_centralairmanuf='"+getcenterairmanuf+"',fld_noofcoolingzones='"+getcoolinoofzones+"',"+
									   "fld_noofcoolingzonesother='"+db.encode(strcoolingnoofzones)+"',fld_age='"+getcoolingage+"',"+
							           "fld_ageother='"+db.encode(strcoolingage)+"',fld_condensationdischarge='"+getconddischarge+"',fld_aircondtesting='"+getacsystem+"',fld_thermostat='"+getthermostats+"',fld_gaslog='"+getgaslog+"' " +
							           		"Where c_id='"+Cooling_select_id+"' and  fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'");
						}
						else
						{
							db.hi_db.execSQL("UPDATE  "+db.CreateCooling+" SET fld_coolingenergysource='"+getcoolingenrgysource+"',fld_coolingenergysourceother='"+db.encode(strcoolingenergysource)+"',"+
							           "fld_centralairmanuf='"+getcenterairmanuf+"',fld_noofcoolingzones='"+getcoolinoofzones+"',"+
									   "fld_noofcoolingzonesother='"+db.encode(strcoolingnoofzones)+"',fld_age='"+getcoolingage+"',"+
							           "fld_ageother='"+db.encode(strcoolingage)+"',fld_condensationdischarge='"+getconddischarge+"',fld_aircondtesting='"+getacsystem+"',fld_thermostat='"+getthermostats+"',fld_gaslog='"+getgaslog+"' " +
							           		"Where c_id='"+Cooling_select_id+"' and  fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
							
						}
			
						
						cf.DisplayToast("Cooling system saved successfully");
	                	clearcooling();
	                	displaycooling();
					}
					else
					{
					
						try
						{
							if(getcurrentview.equals("start"))
							{
							
									Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadCooling + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_Coolingequipment='"+getcoolingequipment+"'", null);
									if(cur.getCount()==0)
									{
										db.hi_db.execSQL(" INSERT INTO "+db.LoadCooling+" (fld_templatename,fld_inspectorid,fld_srid,fld_Coolingequipment,fld_coolingenergysource,fld_coolingenergysourceother,fld_centralairmanuf,fld_noofcoolingzones,fld_noofcoolingzonesother,fld_age,fld_ageother,fld_condensationdischarge,fld_aircondtesting,fld_thermostat,fld_gaslog) VALUES" +
											    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+getcoolingequipment+"','"+getcoolingenrgysource+"','"+db.encode(strcoolingenergysource)+"','"+getcenterairmanuf+"','"+getcoolinoofzones+"',"+
												 "'"+db.encode(strcoolingnoofzones)+"','"+getcoolingage+"','"+db.encode(strcoolingage)+"','"+getconddischarge+"','"+getacsystem+"','"+getthermostats+"',"+
											    "'"+getgaslog+"')");
										cf.DisplayToast("Cooling system saved successfully");
										clearcooling();
					                	displaycooling();
									}
									else
									{
										cf.DisplayToast("Already Exists!! Please select another Cooling Equipment Type.");
									}
							}
							else
							{
								Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateCooling + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_Coolingequipment='"+getcoolingequipment+"'", null);
								if(cur.getCount()==0)
								{
									db.hi_db.execSQL(" INSERT INTO "+db.CreateCooling+" (fld_templatename,fld_inspectorid,fld_Coolingequipment,fld_coolingenergysource,fld_coolingenergysourceother,fld_centralairmanuf,fld_noofcoolingzones,fld_noofcoolingzonesother,fld_age,fld_ageother,fld_condensationdischarge,fld_aircondtesting,fld_thermostat,fld_gaslog) VALUES" +
													    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getcoolingequipment+"','"+getcoolingenrgysource+"','"+db.encode(strcoolingenergysource)+"','"+getcenterairmanuf+"','"+getcoolinoofzones+"',"+
														 "'"+db.encode(strcoolingnoofzones)+"','"+getcoolingage+"','"+db.encode(strcoolingage)+"','"+getconddischarge+"','"+getacsystem+"','"+getthermostats+"',"+
													    "'"+getgaslog+"')");
									cf.DisplayToast("Cooling system saved successfully");
									clearcooling();
				                	displaycooling();
								}
								else
								{
								   cf.DisplayToast("Already Exists!! Please select another Cooling Equipment Type.");
								}
							}
						}
						catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
	
		
		if(getcurrentview.equals("start"))
		{
			checkexistingheatingtemplate();checkexistingcoolingtemplate();checkexistingzonestemplate();
		}
		else
		{
			displayheating();displaycooling();displayzones();
		}
		((TextView)findViewById(R.id.modulename)).setVisibility(v1.VISIBLE);
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(CreateHeating.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						//DefaultValue_Insert(gettempname);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	
	private void DefaultValue_Insert(String gettempname) {
		// TODO Auto-generated method stub
		
			try
			{
				selcur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
	            int cnt = selcur.getCount();System.out.println("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'"+"count="+cnt);
	            if(cnt>0)
	            {
	               if (selcur != null)
	               {
						selcur.moveToFirst();
						do {
							Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
	    		            if(cur.getCount()==0)
	    		            {
    							try
    			            	{
    								db.hi_db.execSQL("INSERT INTO "+db.LoadHeating+" (fld_templatename,fld_inspectorid,fld_srid,fld_heatintype,fld_heatingtypeother,fld_energysource,fld_energysourceother,fld_heatingno,fld_otherheatingno,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_motorblower,fld_noofstoves,fld_othernoofstoves,fld_furnacefeatuers ,fld_supplyregister,fld_retunrregister,fld_radiators,fld_watersystemtype ,fld_fueltanklocation) VALUES" +
    									    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+selcur.getString(3)+"','"+selcur.getString(4)+"','"+selcur.getString(5)+"','"+selcur.getString(6)+"','"+selcur.getString(7)+"',"+
    										 "'"+selcur.getString(8)+"','"+selcur.getString(9)+"','"+selcur.getString(10)+"','"+selcur.getString(11)+"','"+selcur.getString(12)+"','"+selcur.getString(13)+"',"+
    									    "'"+selcur.getString(14)+"','"+selcur.getString(15)+"','"+selcur.getString(16)+"','"+selcur.getString(17)+"','"+selcur.getString(18)+"','"+selcur.getString(19)+"',"+
    										"'"+selcur.getString(20)+"','"+selcur.getString(21)+"','"+selcur.getString(22)+"')");
    		                 	}
    			            	catch (Exception e) {
    								// TODO: handle exception
    							}
	    		            }
						}while (selcur.moveToNext());
					}
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		
			displayheating();
		
	}
	
	private void submitalert()
	{
		submitalert=1;
		final Dialog dialog = new Dialog(CreateHeating.this,android.R.style.Theme_Translucent_NoTitleBar);
		//dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.setContentView(R.layout.input_submit_inspection);		
				
		 final String myString[] = getResources().getStringArray(R.array.myArray);
		 word = myString[rgenerator.nextInt(myString.length)];
		 wrdedit = ((EditText) dialog.findViewById(R.id.wrdedt));
		 
		 wrdedit.setText(word);
		 if (word.contains(" ")) { word = word.replace(" ", ""); }
			
		 
		db.userid();
		  ((TextView) dialog.findViewById(R.id.tv1)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"			
							 + db.Userfirstname +" "+db.Userlastname
							+ "</font></b><font color=black> have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
							+ "</font>"));
		 
			((TextView) dialog.findViewById(R.id.tv2)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ db.Userfirstname +" "+db.Userlastname
							+ "</font></b><font color=black> also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
							+ "</font>"));
			
			
			((Button) dialog.findViewById(R.id.submit)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (((RadioButton) dialog.findViewById(R.id.accept)).isChecked() == true) {
						
						if (((EditText) dialog.findViewById(R.id.wrdedt1)).getText().toString().equals("")) 
						{
							cf.DisplayToast("Please enter Word Verification.");
							((EditText) dialog.findViewById(R.id.wrdedt1)).requestFocus();
						} 
						else
						{
							if (((EditText) dialog.findViewById(R.id.wrdedt1)).getText().toString().equals(word.trim().toString())) 
							{
								System.out.println("111");
								dialog.cancel();
								if(cf.isInternetOn())
								  {
									   new Start_xporting().execute("");
								  }
								  else
								  {
									  cf.DisplayToast("Please enable your internet connection.");
									  
								  } 
							} 
							else
							{
								cf.DisplayToast("Please enter valid Word Verification(case sensitive).");
								((EditText) dialog.findViewById(R.id.wrdedt1)).setText("");
								((EditText) dialog.findViewById(R.id.wrdedt1)).requestFocus();
							}
						}
					} 
					else 
					{
						cf.DisplayToast("Please select Accept Radio Button.");
					}

				}
			});			
			
			((Button) dialog.findViewById(R.id.cancel)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});			
			
			((Button) dialog.findViewById(R.id.S_refresh)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					word = myString[rgenerator.nextInt(myString.length)];			
					 ((EditText) dialog.findViewById(R.id.wrdedt)).setText(word);
					if (word.contains(" ")) {
						word = word.replace(" ", "");
					}	
					
				}
			});			
		dialog.show();
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        if(getcurrentview.equals("start"))
		        {
		        	p_sv.progress_title="Inspection Submission";
		        	p_sv.progress_Msg="Please wait..Your Inspection has been submitting.";
		        }
		        else
		        {
		        	p_sv.progress_title="Template Submission";
		        	p_sv.progress_Msg="Please wait..Your template has been submitting.";
		        }
		        
			    
		       
				startActivityForResult(new Intent(CreateHeating.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		System.out.println("insde");
			 			Submit_Heating();
			 		    Submit_Cooling();
			 		    Submit_Zones();
			 		    Setvc();
			 		    Editvc();
			 		      
			 		
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
            	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	private void Setvc() throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
		if(getcurrentview.equals("start"))
		{
			
		
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_templatename='"+gettempname+"' and fld_module='"+db.encode(getmodulename)+"' and fld_srid='"+getselectedho+"'", null);
		System.out.println("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_srid='"+getselectedho+"'"+"Conunt"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Save_Setvc");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("ssv_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
	      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		    request.addProperty("fld_module",db.decode(c1.getString(c1.getColumnIndex("fld_module"))));	      		    
	      		    request.addProperty("fld_submodule",db.decode(c1.getString(c1.getColumnIndex("fld_submodule"))));
	      		    request.addProperty("fld_primary",db.decode(c1.getString(c1.getColumnIndex("fld_primary"))));
	      		    
	      			String str = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_secondary")))).toString();
	         		if(str.endsWith(","))
	         		{
	         			str = str.substring(0,str.length()-1);
	         		}
	         		request.addProperty("fld_secondary",str);
	      		    
	      		    request.addProperty("fld_comments",db.decode(c1.getString(c1.getColumnIndex("fld_comments"))));
	      		    request.addProperty("fld_condition",db.decode(c1.getString(c1.getColumnIndex("fld_condition"))));
	      		    request.addProperty("fld_other",db.decode(c1.getString(c1.getColumnIndex("fld_other"))));
	      		    request.addProperty("fld_checkbox",db.decode(c1.getString(c1.getColumnIndex("fld_checkbox"))));
	      		    request.addProperty("fld_inspectionpoint",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionpoint"))));
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Save_Setvc request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Save_Setvc", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Save_Setvc "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	   
	                    	    	 try
		 	          				 {
	                    	    		db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_sendtoserver='"+response.getProperty("ssv_Id").toString()+"' " +
		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' " +
		 	          									"and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and " +
		 	          											"fld_primary='"+c1.getString(c1.getColumnIndex("fld_primary"))+"' and fld_srid='"+getselectedho+"'");
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          					 System.out.println("sadsf catch="+e.getMessage());
		 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
		}
     	  
	}
	private void Editvc() throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
		if(getcurrentview.equals("start"))
		{
			Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_srid='"+getselectedho+"' and fld_selected='1'", null);
			
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
						SoapObject  request = new SoapObject(wb.NAMESPACE,"Save_Editvc");
						SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
		      		    envelope1.dotNet = true;
		      		    
		      		    request.addProperty("sed_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
		      		    request.addProperty("fld_inspectorid",db.UserId);
		      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
		      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
		      		    request.addProperty("fld_module",db.decode(c1.getString(c1.getColumnIndex("fld_module"))));	      		    
		      		    request.addProperty("fld_submodule",db.decode(c1.getString(c1.getColumnIndex("fld_submodule"))));
		      		    request.addProperty("fld_VCName",db.decode(c1.getString(c1.getColumnIndex("fld_VCName"))));
		      		    request.addProperty("fld_selected",db.decode(c1.getString(c1.getColumnIndex("fld_selected"))));
		      		    
		      		    envelope1.setOutputSoapObject(request);
		         		System.out.println("Save_Editvc request="+request);
		         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
		         		SoapObject response =null;
		         		try
		                {
		         			 androidHttpTransport1.call(wb.NAMESPACE+"Save_Editvc", envelope1);
							
		                      response = (SoapObject)envelope1.getResponse();
		                      System.out.println("Save_Editvc "+response);
		                      handler_msg = 0;	  
		                      if(response.getProperty("StatusCode").toString().equals("0"))
		              		  {
		                    	     try
		 	          				 {	     
		                    	    	 System.out.println("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(c1.getString(c1.getColumnIndex("fld_submodule")))+"' and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"' and fld_srid='"+getselectedho+"'");
		 	          					db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(c1.getString(c1.getColumnIndex("fld_submodule")))+"' and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"' and fld_srid='"+getselectedho+"'");
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          				 }
		 	                    	handler_msg = 0;	  
		              		  }
		              		  else
		              		  {
		              			 error_msg = response.getProperty("StatusMessage").toString();
		              			 handler_msg=1;
		              			
		              		  }
		                }
		                
		                catch(Exception e)
		                {
		                	System.out.println("sadsf"+e.getMessage());
		                     e.printStackTrace();
		                }
		        	
				 } while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
		}
     	  
	}
	private void Submit_Heating() throws IOException, XmlPullParserException {    	
		// TODO Auto-generated method stub
		Cursor c1=null;SoapObject request=null;
		System.out.println("test");
		if(getcurrentview.equals("start"))
		{
			c1 = db.hi_db.rawQuery("select * from " + db.LoadHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"'", null);
		}
		else
		{
			c1 = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
		}
		System.out.println("c1="+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 if(getcurrentview.equals("start"))
					 {
						  request = new SoapObject(wb.NAMESPACE,"Load_Heating"); 
					 }
					 else
					 {
						  request = new SoapObject(wb.NAMESPACE,"Create_Heating");
					 }
   	        		
	                SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    System.out.println("ddsfdsf");
	      		    request.addProperty("h_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		    request.addProperty("fld_heatintype",db.decode(c1.getString(c1.getColumnIndex("fld_heatintype"))));
	      		    request.addProperty("fld_heatingtypeother",db.decode(c1.getString(c1.getColumnIndex("fld_heatingtypeother"))));	      		    
	      		    request.addProperty("fld_energysource",db.decode(c1.getString(c1.getColumnIndex("fld_energysource"))));
	      		    request.addProperty("fld_energysourceother",db.decode(c1.getString(c1.getColumnIndex("fld_energysourceother"))));
	      		    request.addProperty("fld_heatingno",db.decode(c1.getString(c1.getColumnIndex("fld_heatingno"))));	      		    
	      		    request.addProperty("fld_otherheatingno",db.decode(c1.getString(c1.getColumnIndex("fld_otherheatingno"))));
	      		    request.addProperty("fld_age",db.decode(c1.getString(c1.getColumnIndex("fld_age"))));
	      		    request.addProperty("fld_otherage",db.decode(c1.getString(c1.getColumnIndex("fld_otherage"))));	      		   
	      		    request.addProperty("fld_heatsystembrand",db.decode(c1.getString(c1.getColumnIndex("fld_heatsystembrand"))));
	      		    request.addProperty("fld_furnacetype",db.decode(c1.getString(c1.getColumnIndex("fld_furnacetype"))));	      		    
	      		    request.addProperty("fld_heatdeliverytype",db.decode(c1.getString(c1.getColumnIndex("fld_heatdeliverytype"))));
	      		    request.addProperty("fld_motorblower",db.decode(c1.getString(c1.getColumnIndex("fld_motorblower"))));
	      		    request.addProperty("fld_noofstoves",db.decode(c1.getString(c1.getColumnIndex("fld_noofstoves"))));	      		    
	      		    request.addProperty("fld_othernoofstoves",db.decode(c1.getString(c1.getColumnIndex("fld_othernoofstoves"))));
	      		    request.addProperty("fld_furnacefeatuers",db.decode(c1.getString(c1.getColumnIndex("fld_furnacefeatuers"))));	      		    
	      		    request.addProperty("fld_supplyregister",db.decode(c1.getString(c1.getColumnIndex("fld_supplyregister"))));
	      		    request.addProperty("fld_retunrregister",db.decode(c1.getString(c1.getColumnIndex("fld_retunrregister"))));
	      		    request.addProperty("fld_radiators",db.decode(c1.getString(c1.getColumnIndex("fld_radiators"))));	      		    
	      		    request.addProperty("fld_watersystemtype",db.decode(c1.getString(c1.getColumnIndex("fld_watersystemtype"))));
	      		    request.addProperty("fld_fueltanklocation",db.decode(c1.getString(c1.getColumnIndex("fld_fueltanklocation"))));
	      		    if(getcurrentview.equals("start"))
					 {
	      		    	request.addProperty("fld_srid",db.decode(c1.getString(c1.getColumnIndex("fld_srid"))));
					 }
	      		    
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Heating request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			if(getcurrentview.equals("start"))
						 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"Load_Heating", envelope1); 
						 }
						 else
						 {
							 androidHttpTransport1.call(wb.NAMESPACE+"Create_Heating", envelope1);
						 }
	         			  
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Heating "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {	                    	 
	                    		 try
	 	          				 {	  
	                    			 if(getcurrentview.equals("start"))
	            					 {
	                    				 
	                    				 db.hi_db.execSQL("UPDATE "+db.LoadHeating+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"' and fld_srid='"+getselectedho+"'");
	                    				 System.out.println("UPDATE "+db.LoadHeating+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"' and fld_srid='"+getselectedho+"'");	 
	            					 }
	                    			 else
	                    			 {
	                    				 System.out.println("UPDATE "+db.CreateHeating+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"'");
	                    				 db.hi_db.execSQL("UPDATE "+db.CreateHeating+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"'");
	 	 	          					
	                    			 }
	 	          					
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;
	                      }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		 else
    		  {
    			  handler_msg=0;
    		  }
	}
	private void Submit_Cooling() throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
		Cursor c1=null;SoapObject request=null;
		if(getcurrentview.equals("start"))
		{
			c1 = db.hi_db.rawQuery("select * from " + db.LoadCooling + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"'", null);
		}
		else
		{
			c1 = db.hi_db.rawQuery("select * from " + db.CreateCooling + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
		}
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 if(getcurrentview.equals("start"))
					 {
						  request = new SoapObject(wb.NAMESPACE,"Load_Cooling"); 
					 }
					 else
					 {
						  request = new SoapObject(wb.NAMESPACE,"Create_Cooling");
					 }
   	        		
	                SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("c_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		    request.addProperty("fld_Coolingequipment",db.decode(c1.getString(c1.getColumnIndex("fld_Coolingequipment"))));
	      		    request.addProperty("fld_coolingenergysource",db.decode(c1.getString(c1.getColumnIndex("fld_coolingenergysource"))));	      		    
	      		    request.addProperty("fld_coolingenergysourceother",db.decode(c1.getString(c1.getColumnIndex("fld_coolingenergysourceother"))));
	      		    request.addProperty("fld_centralairmanuf",db.decode(c1.getString(c1.getColumnIndex("fld_centralairmanuf"))));
	      		    request.addProperty("fld_noofcoolingzones",db.decode(c1.getString(c1.getColumnIndex("fld_noofcoolingzones"))));	      		    
	      		    request.addProperty("fld_noofcoolingzonesother",db.decode(c1.getString(c1.getColumnIndex("fld_noofcoolingzonesother"))));
	      		    request.addProperty("fld_age",db.decode(c1.getString(c1.getColumnIndex("fld_age"))));   		    
	      		    request.addProperty("fld_ageother",db.decode(c1.getString(c1.getColumnIndex("fld_ageother"))));	      		   
	      		    request.addProperty("fld_condensationdischarge",db.decode(c1.getString(c1.getColumnIndex("fld_condensationdischarge"))));
	      		    request.addProperty("fld_aircondtesting",db.decode(c1.getString(c1.getColumnIndex("fld_aircondtesting"))));	      		    
	      		    request.addProperty("fld_thermostat",db.decode(c1.getString(c1.getColumnIndex("fld_thermostat"))));
	      		    request.addProperty("fld_gaslog",db.decode(c1.getString(c1.getColumnIndex("fld_gaslog"))));
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Cooling request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 if(getcurrentview.equals("start"))
						 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"Create_Cooling", envelope1); 
						 }
						 else
						 {
							 androidHttpTransport1.call(wb.NAMESPACE+"Load_Cooling", envelope1);
						 }
	                
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Cooling "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	     try
	 	          				 {	     
	                    	    	 if(getcurrentview.equals("start"))
	            					 {
	                    	    		 System.out.println("UPDATE "+db.LoadCooling+ " SET fld_sendtoserver='"+response.getProperty("c_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_Coolingequipment='"+c1.getString(c1.getColumnIndex("fld_Coolingequipment"))+"' and fld_srid='"+getselectedho+"'");
	                    				 db.hi_db.execSQL("UPDATE "+db.LoadCooling+ " SET fld_sendtoserver='"+response.getProperty("c_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_Coolingequipment='"+c1.getString(c1.getColumnIndex("fld_Coolingequipment"))+"' and fld_srid='"+getselectedho+"'");
	 	 	          						 
	            					 }
	                    			 else
	                    			 {
	                    				 
	 	 	          					System.out.println("UPDATE "+db.CreateCooling+ " SET fld_sendtoserver='"+response.getProperty("c_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_Coolingequipment='"+c1.getString(c1.getColumnIndex("fld_Coolingequipment"))+"'");
	 	 	          				db.hi_db.execSQL("UPDATE "+db.CreateCooling+ " SET fld_sendtoserver='"+response.getProperty("c_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_Coolingequipment='"+c1.getString(c1.getColumnIndex("fld_Coolingequipment"))+"'");
	                    			 }
	 	          					
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          					 System.out.println("cooling exce="+e.getMessage());
	 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
	}
 private void Submit_Zones() throws IOException, XmlPullParserException {	
	// TODO Auto-generated method stub
	Cursor c1=null;SoapObject request=null;
	if(getcurrentview.equals("start"))
	{
		c1 = db.hi_db.rawQuery("select * from " + db.LoadZones + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"'", null);
	}
	else
	{
		c1 = db.hi_db.rawQuery("select * from " + db.CreateZones + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
	}
		        if(c1.getCount()>0)
	          {
	        	c1.moveToFirst();   	        
			 do
			 {				
				 if(getcurrentview.equals("start"))
				 {
					  request = new SoapObject(wb.NAMESPACE,"Load_Zones"); 
				 }
				 else
				 {
					  request = new SoapObject(wb.NAMESPACE,"Create_Zones");
				 }
	        		
                SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
      		    envelope1.dotNet = true;
      		    
      		    request.addProperty("z_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
      		    request.addProperty("fld_inspectorid",db.UserId);
      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
      		    request.addProperty("fld_zonename",db.decode(c1.getString(c1.getColumnIndex("fld_zonename"))));
      		    request.addProperty("fld_zonenameother",db.decode(c1.getString(c1.getColumnIndex("fld_zonenameother"))));	      		    
      		    request.addProperty("fld_unitype",db.decode(c1.getString(c1.getColumnIndex("fld_unitype"))));
      		    request.addProperty("fla_zone5yearreplacement",db.decode(c1.getString(c1.getColumnIndex("fla_zone5yearreplacement"))));
      		    request.addProperty("fld_zonetestedfor",db.decode(c1.getString(c1.getColumnIndex("fld_zonetestedfor"))));	      		    
      		    request.addProperty("fld_zonetonnage",db.decode(c1.getString(c1.getColumnIndex("fld_zonetonnage"))));
      		    request.addProperty("fld_zonetonnageother",db.decode(c1.getString(c1.getColumnIndex("fld_zonetonnageother"))));
      		    
      		    envelope1.setOutputSoapObject(request);
         		System.out.println("request="+request);
         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
         		SoapObject response =null;
         		try
                {
         			 if(getcurrentview.equals("start"))
    				 {
         				androidHttpTransport1.call(wb.NAMESPACE+"Load_Zones", envelope1);		 
    				 }
         			 else
         			 {
         				androidHttpTransport1.call(wb.NAMESPACE+"Create_Zones", envelope1);
         			 }
                
                      response = (SoapObject)envelope1.getResponse();
                      System.out.println("Zones "+response);
                      handler_msg = 0;	  
                      if(response.getProperty("StatusCode").toString().equals("0"))
              		  {
                    	 
                    		 try
 	          				 {
                    			
                    			  if(getcurrentview.equals("start"))
	            					 {
                    				  db.hi_db.execSQL("UPDATE "+db.LoadZones+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zonename='"+c1.getString(c1.getColumnIndex("fld_zonename"))+"' and fld_srid='"+getselectedho+"'");
       	          					System.out.println("UPDATE "+db.LoadZones+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zonename='"+c1.getString(c1.getColumnIndex("fld_zonename"))+"' and fld_srid='"+getselectedho+"'");
       	          					
       	          						 
	            					 }
	                    			 else
	                    			 {
	                    				 db.hi_db.execSQL("UPDATE "+db.CreateZones+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zonename='"+c1.getString(c1.getColumnIndex("fld_zonename"))+"'");
	      	          					System.out.println("UPDATE "+db.CreateZones+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zonename='"+c1.getString(c1.getColumnIndex("fld_zonename"))+"'");
	                    			 }
 	          					
 	          				 }
 	          				 catch (Exception e) {
 	          					// TODO: handle exception
 	          				 }
 	                    	handler_msg = 0;	  
                    	  
                    	 
                      	
              		  }
              		  else
              		  {
              			 error_msg = response.getProperty("StatusMessage").toString();
              			 handler_msg=1;
              			
              		  }
                }
                
                catch(Exception e)
                {
                	System.out.println("sadsf"+e.getMessage());
                     e.printStackTrace();
                }
        	} while (c1.moveToNext());
	          }
	          else
	          {
	        	  handler_msg=0;
	          }
 	  
}
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(CreateHeating.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public void success_alert() {
		// TODO Auto-generated method stub
		String msg=""; 
       if(getcurrentview.equals("start"))
       {
    	   msg="You've successfully submitted your Inspection";
       }
       else
       {
    	   msg="You've successfully submitted your template";
       }
		AlertDialog al = new AlertDialog.Builder(CreateHeating.this).setMessage(msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				startActivity(new Intent(CreateHeating.this,HomeScreen.class));
				finish();
				
				
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	private void checkexistingheatingtemplate() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadHeating + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(cur.getCount()==0 && flag1==0)
			 {
				defaultheatinginsert();
			 }
			 else
			 {
				 displayheating();
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	private void checkexistingcoolingtemplate() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadCooling + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(cur.getCount()==0 && flag2==0)
			 {
				defaultcoolinginsert();
			 }
			 else
			 {
				 displaycooling();
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	private void checkexistingzonestemplate() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadZones + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(cur.getCount()==0 && flag3==0)
			 {
				defaultzonesinsert();
			 }
			 else
			 {
				 displayzones();
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	private void defaultheatinginsert() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.CreateHeating + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			  if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 for(int i=0;i<cur.getCount();i++)
				 {
						db.hi_db.execSQL("INSERT INTO "+db.LoadHeating+" (fld_templatename,fld_inspectorid,fld_srid,fld_heatintype,fld_heatingtypeother,fld_energysource,fld_energysourceother,fld_heatingno,fld_otherheatingno,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_motorblower,fld_noofstoves,fld_othernoofstoves,fld_furnacefeatuers ,fld_supplyregister,fld_retunrregister,fld_radiators,fld_watersystemtype ,fld_fueltanklocation) VALUES" +
							    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+cur.getString(3)+"','"+cur.getString(4)+"','"+cur.getString(5)+"','"+cur.getString(6)+"','"+cur.getString(7)+"',"+
								 "'"+cur.getString(8)+"','"+cur.getString(9)+"','"+cur.getString(10)+"','"+cur.getString(11)+"','"+cur.getString(12)+"','"+cur.getString(13)+"',"+
							    "'"+cur.getString(14)+"','"+cur.getString(15)+"','"+cur.getString(16)+"','"+cur.getString(17)+"','"+cur.getString(18)+"','"+cur.getString(19)+"',"+
								"'"+cur.getString(20)+"','"+cur.getString(21)+"','"+cur.getString(22)+"')");
					 
				
							cur.moveToNext();
				 }
			 }
			 displayheating();
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("defaultheatinginsert"+e.getMessage());
		}
		
		
	}
	private void defaultcoolinginsert() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.CreateCooling + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 for(int i=0;i<cur.getCount();i++)
				 {					 
						db.hi_db.execSQL(" INSERT INTO "+db.LoadCooling+" (fld_templatename,fld_inspectorid,fld_srid,fld_Coolingequipment,fld_coolingenergysource,fld_coolingenergysourceother,fld_centralairmanuf,fld_noofcoolingzones,fld_noofcoolingzonesother,fld_age,fld_ageother,fld_condensationdischarge,fld_aircondtesting,fld_thermostat,fld_gaslog) VALUES" +
							    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+cur.getString(3)+"','"+cur.getString(4)+"','"+cur.getString(5)+"','"+cur.getString(6)+"','"+cur.getString(7)+"',"+
								 "'"+cur.getString(8)+"','"+cur.getString(9)+"','"+cur.getString(10)+"','"+cur.getString(11)+"','"+cur.getString(12)+"','"+cur.getString(13)+"',"+
							    "'"+cur.getString(14)+"')");
						cur.moveToNext();
				 }
			 }
			 displaycooling();
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("Cooling="+e.getMessage());
		}
	}
	private void defaultzonesinsert() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.CreateZones + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 for(int i=0;i<cur.getCount();i++)
				 {					 
					 db.hi_db.execSQL(" INSERT INTO "+db.LoadZones+" (fld_templatename,fld_inspectorid,fld_srid,fld_zonename,fld_zonenameother,fld_unitype,fla_zone5yearreplacement,fld_zonetestedfor,fld_zonetonnage,fld_zonetonnageother) VALUES" +
							    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+cur.getString(3)+"','"+cur.getString(4)+"','"+cur.getString(5)+"','"+cur.getString(6)+"','"+cur.getString(7)+"','"+cur.getString(8)+"','"+cur.getString(9)+"')");
					 cur.moveToNext();
				 }
			 }
			 displayzones();
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("EE="+e.getMessage());
		}
	}

	protected void displaycooling() {
		// TODO Auto-generated method stub
		try
		{
			
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadCooling + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateCooling + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
			System.out.println("Cooling="+cur.getCount());
			
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 try
					{
						coolingtblshow.removeAllViews();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				 
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.coolinglin);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();
			
				 coolingtblshow.setVisibility(View.VISIBLE); 
				 
				 coolingtblshow.addView(th,lp); 
						 
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String Coolingequipment="",coolingenergysource="",coolingenergysourceother="",centralairmanuf="",noofcoolingzones="",
							 noofcoolingzonesother="",age="",ageother="",condensationdischarge="",aircondtesting="",thermostat="",gaslog="";
					 
					 Coolingequipment=cur.getString(cur.getColumnIndex("fld_Coolingequipment"));
					 coolingenergysource=cur.getString(cur.getColumnIndex("fld_coolingenergysource"));
					 coolingenergysourceother=db.decode(cur.getString(cur.getColumnIndex("fld_coolingenergysourceother")));
					 centralairmanuf=db.decode(cur.getString(cur.getColumnIndex("fld_centralairmanuf")));
					 noofcoolingzones=db.decode(cur.getString(cur.getColumnIndex("fld_noofcoolingzones")));
					 noofcoolingzonesother=db.decode(cur.getString(cur.getColumnIndex("fld_noofcoolingzonesother")));
					 age=cur.getString(cur.getColumnIndex("fld_age"));
					 ageother=db.decode(cur.getString(cur.getColumnIndex("fld_ageother")));
					 condensationdischarge=cur.getString(cur.getColumnIndex("fld_condensationdischarge"));
					 aircondtesting=cur.getString(cur.getColumnIndex("fld_aircondtesting"));					 
					 thermostat=cur.getString(cur.getColumnIndex("fld_thermostat"));
					 gaslog=db.decode(cur.getString(cur.getColumnIndex("fld_gaslog")));
					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.coolingvalue);
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     
				     
				     TextView tvcoolingequip= (TextView) t.findViewWithTag("CoolingEquiment");
				     tvcoolingequip.setText(Coolingequipment);				     
				     
				     
				     TextView tvenergysource= (TextView) t.findViewWithTag("Energysource");
				     if(coolingenergysource.equals("--Select--"))
					 {
				    	 coolingenergysource="";
					 }
				     if(coolingenergysource.equals("Other"))
					 {
				    	 tvenergysource.setText(coolingenergysourceother);	
					 }
					 else
					 {
						 tvenergysource.setText(coolingenergysource);
					 }
				     
				     TextView tvcenterairmanuf= (TextView) t.findViewWithTag("CenterAirManuf");
				     if(centralairmanuf.equals("--Select--"))
					 {
				    	 centralairmanuf="";
					 }
				     tvcenterairmanuf.setText(centralairmanuf);
				     
				     TextView tvcoolingnoofzones= (TextView) t.findViewWithTag("NoofCoolingZones");
				     if(noofcoolingzones.equals("--Select--"))
					 {
				    	 noofcoolingzones="";
					 }
				     if(noofcoolingzones.equals("Other"))
					 {
				    	 tvcoolingnoofzones.setText(noofcoolingzonesother);	
					 }
					 else
					 {
						 tvcoolingnoofzones.setText(noofcoolingzones);
					 }
				     			     
				     
				     TextView tvcoolingage= (TextView) t.findViewWithTag("Age");
				     if(age.equals("--Select--"))
					 {
				    	 age="";
					 }
				     if(age.equals("Other"))
					 {
				    	 tvcoolingage.setText(ageother);	
					 }
					 else
					 {
						 tvcoolingage.setText(age);
					 }
				     
				     
				     TextView tvconddischarge= (TextView) t.findViewWithTag("CondensationDischarge");
				     if(condensationdischarge.equals("--Select--"))
					 {
				    	 condensationdischarge="";
					 }
				     tvconddischarge.setText(condensationdischarge);
				     
				     TextView tvactesting= (TextView) t.findViewWithTag("ACTesting");		
				     if(aircondtesting.equals("--Select--"))
					 {
				    	 aircondtesting="";
					 }
				     tvactesting.setText(aircondtesting);
				     
				     TextView tvthermostats= (TextView) t.findViewWithTag("Thermostats");	
				     if(thermostat.equals("--Select--"))
					 {
				    	 thermostat="";
					 }
				     tvthermostats.setText(thermostat);
				     
				     
				     TextView tvgaslog= (TextView) t.findViewWithTag("Gaslog");
				     if(gaslog.equals("--Select--"))
					 {
				    	 gaslog="";
					 }
				     tvgaslog.setText(gaslog);System.out.println("came gaslog ere");
				   		     
					ImageView edit= (ImageView) t.findViewWithTag("editcooling");
				 	edit.setId(789456+i);
				 	edit.setTag(cur.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						   int i=Integer.parseInt(v.getTag().toString());
						   udpcooling=1;
						   updatecooling(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
	             
	                ImageView delete=(ImageView) t.findViewWithTag("delcooling");  
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(CreateHeating.this);
							builder.setMessage("Do you want to delete the selected Cooling details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==Cooling_select_id)
													{
														Cooling_select_id=0;
													}
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadCooling+" Where c_id='"+i+"'");	
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateCooling+" Where c_id='"+i+"'");
													}
																										
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
													
													Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadCooling + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname).toLowerCase()+"'",null);
													if(cur.getCount()==0)
													{
														flag2=1;
													}
													
													clearcooling();
													
													displaycooling();
													
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					l1.removeAllViews();
					
					coolingtblshow.addView(t,lp);
					cur.moveToNext();
					
				 }
			 }
			 else
			 {
				 coolingtblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("display cooling error"+e.getMessage());
		}
	}
	protected void updatecooling(int i) {
		// TODO Auto-generated method stub

		Cooling_select_id=i;
			Cursor cur;
				((Button)findViewById(R.id.btn_cooladd)).setText("Update");
			try{
				if(getcurrentview.equals("start"))
				{
					cur= db.hi_db.rawQuery("select * from " + db.LoadCooling + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and c_id='"+i+"'",null);	
				}
				else
				{
					cur= db.hi_db.rawQuery("select * from " + db.CreateCooling + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and c_id='"+i+"'",null);
				}
				
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
					
					 String Coolingequipment="",coolingenergysource="",coolingenergysourceother="",centralairmanuf="",noofcoolingzones="",
							 noofcoolingzonesother="",age="",ageother="",condensationdischarge="",aircondtesting="",thermostat="",gaslog="";
					 
					 Coolingequipment=cur.getString(cur.getColumnIndex("fld_Coolingequipment"));
					 coolingenergysource=cur.getString(cur.getColumnIndex("fld_coolingenergysource"));
					 coolingenergysourceother=db.decode(cur.getString(cur.getColumnIndex("fld_coolingenergysourceother")));
					 centralairmanuf=db.decode(cur.getString(cur.getColumnIndex("fld_centralairmanuf")));
					 noofcoolingzones=db.decode(cur.getString(cur.getColumnIndex("fld_noofcoolingzones")));
					 noofcoolingzonesother=db.decode(cur.getString(cur.getColumnIndex("fld_noofcoolingzonesother")));
					 age=cur.getString(cur.getColumnIndex("fld_age"));
					 ageother=db.decode(cur.getString(cur.getColumnIndex("fld_ageother")));
					 condensationdischarge=cur.getString(cur.getColumnIndex("fld_condensationdischarge"));
					 aircondtesting=cur.getString(cur.getColumnIndex("fld_aircondtesting"));					 
					 thermostat=cur.getString(cur.getColumnIndex("fld_thermostat"));
					 gaslog=db.decode(cur.getString(cur.getColumnIndex("fld_gaslog")));
					 
					 System.out.println("Coolingequipment"+Coolingequipment);
					 sp_coolingequipment.setSelection(coolinnequiadapp.getPosition(Coolingequipment));sp_coolingequipment.setEnabled(false);
				
					 sp_coolingenergysource.setSelection(coolingenersourceadap.getPosition(coolingenergysource));
					 if(coolingenergysource.equals("Other"))
					 {
						 ed_othercoolingeergysource.setVisibility(View.VISIBLE);
						 ed_othercoolingeergysource.setText(coolingenergysourceother);
						 
					 }
					 else
					 {
						 ed_othercoolingeergysource.setVisibility(View.INVISIBLE);
					 }
					 sp_coolingmanufac.setSelection(coolingmanuadap.getPosition(centralairmanuf));
					 
					 sp_noofcoolingzones.setSelection(noofcoolingzonesadap.getPosition(noofcoolingzones));
					 if(noofcoolingzones.equals("Other"))
					 {
						 ed_coolingnoofzones.setVisibility(View.VISIBLE);
						 ed_coolingnoofzones.setText(noofcoolingzonesother);
						 
					 }
					 else
					 {
						 ed_coolingnoofzones.setVisibility(View.INVISIBLE);
					 }
					 
					 sp_coolingage.setSelection(coolingageadap .getPosition(age));
					 if(age.equals("Other"))
					 {
						 ed_coolingage.setVisibility(View.VISIBLE);
						 ed_coolingage.setText(ageother);
						 
					 }
					 else
					 {
						 ed_coolingage.setVisibility(View.INVISIBLE);
					 }
					 
					 sp_condensdischarge.setSelection(conddiscadap.getPosition(condensationdischarge));
					 sp_coolingactesting.setSelection(coolingactestingadap.getPosition(aircondtesting));
					 sp_coolingthermostats.setSelection(coolingthermostatsadap.getPosition(thermostat));
					 sp_coolinggaslog.setSelection(coolinggaslogadap.getPosition(gaslog));
					 
				}
			}catch(Exception e){}
		
	
	}
		private void displayheating() {
		// TODO Auto-generated method stub
		try
		{
			
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadHeating + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
		
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 try
					{
						heatingtblshow.removeAllViews();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.heatinglist);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();
			
				 heatingtblshow.setVisibility(View.VISIBLE); 
				 
				 heatingtblshow.addView(th,lp); 			 
				
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String heatingtype="",heatingtypeother="",energysource="",energysourceother="",heatingno="",otherheatingno="",age="",
							 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",noofstoves="",othernoofstoves="",
							 furnacefeatuers="",supplyregister="",retunrregister="",radiators="",watersystemtype="",fueltanklocation="";
					 
					 heatingtype=cur.getString(cur.getColumnIndex("fld_heatintype"));
					 heatingtypeother=db.decode(cur.getString(cur.getColumnIndex("fld_heatingtypeother")));
					 energysource=db.decode(cur.getString(cur.getColumnIndex("fld_energysource")));
					 energysourceother=db.decode(cur.getString(cur.getColumnIndex("fld_energysourceother")));
					 heatingno=db.decode(cur.getString(cur.getColumnIndex("fld_heatingno")));
					 otherheatingno=db.decode(cur.getString(cur.getColumnIndex("fld_otherheatingno")));
					 age=cur.getString(cur.getColumnIndex("fld_age"));
					 otherage=db.decode(cur.getString(cur.getColumnIndex("fld_otherage")));
					 heatsystembrand=cur.getString(cur.getColumnIndex("fld_heatsystembrand"));
					 furnacetype=cur.getString(cur.getColumnIndex("fld_furnacetype"));					 
					 heatdeliverytype=cur.getString(cur.getColumnIndex("fld_heatdeliverytype"));
					 motorblower=db.decode(cur.getString(cur.getColumnIndex("fld_motorblower")));
					 noofstoves=cur.getString(cur.getColumnIndex("fld_noofstoves"));
					 othernoofstoves=db.decode(cur.getString(cur.getColumnIndex("fld_othernoofstoves")));
					 furnacefeatuers=cur.getString(cur.getColumnIndex("fld_furnacefeatuers"));
					 supplyregister=db.decode(cur.getString(cur.getColumnIndex("fld_supplyregister")));
					 retunrregister=cur.getString(cur.getColumnIndex("fld_retunrregister"));					 
					 radiators=cur.getString(cur.getColumnIndex("fld_radiators"));
					 watersystemtype=db.decode(cur.getString(cur.getColumnIndex("fld_watersystemtype")));					 
					 fueltanklocation=cur.getString(cur.getColumnIndex("fld_fueltanklocation"));
					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.heatingvalue);
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     
				     
				     TextView tvheattype= (TextView) t.findViewWithTag("HeatType");
				     if(heatingtype.equals("Other"))
					 {
				    	 tvheattype.setText(heatingtypeother);	
					 }
					 else
					 {
						 tvheattype.setText(heatingtype);
					 }
				     
				     TextView tvenergsource= (TextView) t.findViewWithTag("EnergySource");
				     if(energysource.equals("--Select--"))
					 {
				    	 energysource="";
					 }
				     if(energysource.equals("Other"))
					 {
				    	 tvenergsource.setText(energysourceother);	
					 }
					 else
					 {
						 tvenergsource.setText(energysource);
					 }
				     
				     TextView tvnoofheatsystems= (TextView) t.findViewWithTag("NoofHeatSystems");
				     if(heatingno.equals("--Select--"))
					 {
				    	 heatingno="";
					 }
				     if(heatingno.equals("Other"))
					 {
				    	 tvnoofheatsystems.setText(otherheatingno);	
					 }
					 else
					 {
						 tvnoofheatsystems.setText(heatingno);
					 }
				     
				     
				     TextView tvage= (TextView) t.findViewWithTag("Age");
				     if(age.equals("--Select--"))
					 {
				    	 age="";
					 }
				     if(age.equals("Other"))
					 {
				    	 tvage.setText(otherage);	
					 }
					 else
					 {
						 tvage.setText(age);
					 }
				     
										
					TextView tvbrand= (TextView) t.findViewWithTag("HeatsysBrand");
					if(heatsystembrand.equals("--Select--"))
					{
						heatsystembrand="";
					}
					tvbrand.setText(heatsystembrand);
					
					TextView tvfurnace= (TextView) t.findViewWithTag("FurnaceType");
					if(furnacetype.equals("--Select--"))
					{
						furnacetype="";
					}
					tvfurnace.setText(furnacetype);
					
					TextView tvheatdeliverytype= (TextView) t.findViewWithTag("HeatDeliveryType");
					if(heatdeliverytype.equals("--Select--"))
					{
						heatdeliverytype="";
					}
					tvheatdeliverytype.setText(heatdeliverytype);
					
					TextView tvmotorblower= (TextView) t.findViewWithTag("MotorBlower");
					if(motorblower.equals("--Select--"))
					{
						motorblower="";
					}
					tvmotorblower.setText(motorblower);
					
					 TextView tvwoodstoves= (TextView) t.findViewWithTag("WoodStoves");
					 if(noofstoves.equals("--Select--"))
					 {
						 noofstoves="";
					 }
				     if(noofstoves.equals("Other"))
					 {
				    	 tvwoodstoves.setText(othernoofstoves);	
					 }
					 else
					 {
						 tvwoodstoves.setText(noofstoves);
					 }
					
				     TextView tvfurnfeatures= (TextView) t.findViewWithTag("FurnaceFeatures");
					 if(furnacefeatuers.equals("--Select--"))
					 {
						furnacefeatuers="";
					 }
					 tvfurnfeatures.setText(furnacefeatuers);
						
					 TextView tvsupplyregister= (TextView) t.findViewWithTag("SupplyRegister");
					 if(supplyregister.equals("--Select--"))
					 {
						 supplyregister="";
					 }
					 tvsupplyregister.setText(supplyregister);
						
						
					 TextView tvretunrregister= (TextView) t.findViewWithTag("ReturnRegister");
					 if(retunrregister.equals("--Select--"))
					 {
						 retunrregister="";
					 }
					 tvretunrregister.setText(retunrregister);
					 
					 TextView tvradiators= (TextView) t.findViewWithTag("Radiators");
					 if(radiators.equals("--Select--"))
					 {
						 radiators="";
					 }
					 tvradiators.setText(radiators);
						
						
					 TextView tvwatersystype= (TextView) t.findViewWithTag("WaterSystemType");
					 if(watersystemtype.equals("--Select--"))
					 {
						 watersystemtype="";
					 }
					 tvwatersystype.setText(watersystemtype);
					 
					 TextView tvfueltank= (TextView) t.findViewWithTag("FuelTankLoc");
					 if(fueltanklocation.equals("--Select--"))
					 {
						 fueltanklocation="";
					 }
					 tvfueltank.setText(fueltanklocation);
					
					
					ImageView edit= (ImageView) t.findViewWithTag("edit");
				 	edit.setId(789456+i);
				 	edit.setTag(cur.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						   int i=Integer.parseInt(v.getTag().toString());
						   updheating=1;
						   updateheating(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });System.out.println("delete");
	             
	                ImageView delete=(ImageView) t.findViewWithTag("del");  
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(CreateHeating.this);
							builder.setMessage("Do you want to delete the selected Heating details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==Heating_select_id)
													{
														Heating_select_id=0;
													}
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadHeating+" Where h_id='"+i+"'");	
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateHeating+" Where h_id='"+i+"'");
													}
																										
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
													Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadHeating + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname).toLowerCase()+"'",null);
													if(cur.getCount()==0)
													{
														flag1=1;
													}
													
													clearheating();
													
													displayheating();
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					
					
					
					
					l1.removeAllViews();
					
					heatingtblshow.addView(t,lp);
					cur.moveToNext();
					
				 }
			 }
			 else
			 {
				 heatingtblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	 	
	}
	
		protected void updateheating(int i) {
			// TODO Auto-generated method stub
				Cursor cur;
				Heating_select_id=i;
				 
					((Button)findViewById(R.id.btn_heatadd)).setText("Update");
				try{
					if(getcurrentview.equals("start"))
					{
						cur= db.hi_db.rawQuery("select * from " + db.LoadHeating + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and h_id='"+i+"'",null);	
					}
					else
					{
						cur= db.hi_db.rawQuery("select * from " + db.CreateHeating + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and h_id='"+i+"'",null);
					}
					
					if(cur.getCount()>0)
					{
						cur.moveToFirst();
						
						 String heatingtype="",heatingtypeother="",energysource="",energysourceother="",heatingno="",otherheatingno="",age="",
								 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",noofstoves="",othernoofstoves="",
								 furnacefeatuers="",supplyregister="",retunrregister="",radiators="",watersystemtype="",fueltanklocation="";
						 
						 heatingtype=cur.getString(cur.getColumnIndex("fld_heatintype"));
						 heatingtypeother=cur.getString(cur.getColumnIndex("fld_heatingtypeother"));
						 energysource=db.decode(cur.getString(cur.getColumnIndex("fld_energysource")));
						 energysourceother=db.decode(cur.getString(cur.getColumnIndex("fld_energysourceother")));
						 heatingno=db.decode(cur.getString(cur.getColumnIndex("fld_heatingno")));
						 otherheatingno=db.decode(cur.getString(cur.getColumnIndex("fld_otherheatingno")));
						 age=cur.getString(cur.getColumnIndex("fld_age"));
						 otherage=db.decode(cur.getString(cur.getColumnIndex("fld_otherage")));
						 heatsystembrand=cur.getString(cur.getColumnIndex("fld_heatsystembrand"));
						 furnacetype=cur.getString(cur.getColumnIndex("fld_furnacetype"));					 
						 heatdeliverytype=cur.getString(cur.getColumnIndex("fld_heatdeliverytype"));
						 motorblower=db.decode(cur.getString(cur.getColumnIndex("fld_motorblower")));
						 noofstoves=cur.getString(cur.getColumnIndex("fld_noofstoves"));
						 othernoofstoves=db.decode(cur.getString(cur.getColumnIndex("fld_othernoofstoves")));
						 furnacefeatuers=cur.getString(cur.getColumnIndex("fld_furnacefeatuers"));
						 supplyregister=db.decode(cur.getString(cur.getColumnIndex("fld_supplyregister")));
						 retunrregister=cur.getString(cur.getColumnIndex("fld_retunrregister"));					 
						 radiators=cur.getString(cur.getColumnIndex("fld_radiators"));
						 watersystemtype=db.decode(cur.getString(cur.getColumnIndex("fld_watersystemtype")));					 
						 fueltanklocation=cur.getString(cur.getColumnIndex("fld_fueltanklocation"));
						 
						 sp_heatingtype.setSelection(heattypearrayadap.getPosition(heatingtype));sp_heatingtype.setEnabled(false);
						 if(heatingtype.equals("Other"))
						 {
							 ed_otherheattype.setText(heatingtypeother);
							 ed_otherheattype.setVisibility(View.VISIBLE);
						 }
						 else
						 {
							 ed_otherheattype.setVisibility(View.INVISIBLE);
						 }
						 sp_energysource.setSelection(energysourcearrayadap.getPosition(energysource));
						 if(energysource.equals("Other"))
						 {
							 ed_otherenergysource.setVisibility(View.VISIBLE);
							 ed_otherenergysource.setText(energysourceother);
							 
						 }
						 else
						 {
							 ed_otherenergysource.setVisibility(View.INVISIBLE);
						 }
						 
						 
						 if(!energysource.equals("--Select--"))
						 {
							 sp_fueltank.setSelection(fueltankarrayadap.getPosition(fueltanklocation));
							 ((LinearLayout)findViewById(R.id.lin02)).setVisibility(View.VISIBLE);
						 }
						 else
						 {
							 ((LinearLayout)findViewById(R.id.lin02)).setVisibility(View.GONE);
						 }
						 
						 
						 
						 
						 sp_heatingnumber.setSelection(heatnumarrayadap.getPosition(heatingno));
						 if(heatingno.equals("Other"))
						 {
							 ed_otherheatignno.setVisibility(View.VISIBLE);
							 ed_otherheatignno.setText(otherheatingno);
						 }
						 else
						 {
							 ed_otherheatignno.setVisibility(View.INVISIBLE);
						 }
						 spin_heatingage.setSelection(agearrayadap.getPosition(age));
						 if(age.equals("Other"))
						 {
							 ed_otherheatingage.setVisibility(View.VISIBLE);
							 ed_otherheatingage.setText(otherage);
						 }
						 else
						 {
							 ed_otherheatingage.setVisibility(View.INVISIBLE);
						 }
						 
						 sp_heatbrand.setSelection(brandarrayadap.getPosition(heatsystembrand));
						 sp_furnace.setSelection(furnacearrayadap.getPosition(furnacetype));
						 sp_heatdelivery.setSelection(heatdeliveryarrayadap.getPosition(heatdeliverytype));
						 sp_motorblower.setSelection(motorarrayadap.getPosition(motorblower));
						 sp_woodstoves.setSelection(woodstoveadap.getPosition(noofstoves));
						 if(noofstoves.equals("Other"))
						 {
							 ed_otherwoodstoves.setVisibility(View.VISIBLE);
							 ed_otherwoodstoves.setText(othernoofstoves);
						 }
						 else
						 {
							 ed_otherwoodstoves.setVisibility(View.INVISIBLE);
						 }
						 
						 sp_furnfeat.setSelection(furnfeatarraydap.getPosition(furnacefeatuers));
						 spin_supplyregister.setSelection(supplyregarraydap.getPosition(supplyregister));
						 sp_returnregister.setSelection(returnregarraydap.getPosition(retunrregister));
						 
						  if(heatdeliverytype.equals("Hot Water Heat") || heatdeliverytype.equals("Boiler") ||  heatdeliverytype.equals("Radiant"))
			    		 {	
							 ((LinearLayout)findViewById(R.id.lin01)).setVisibility(View.VISIBLE);
							 sp_radiators.setSelection(radiatorsarrayadap.getPosition(radiators));
							 if(heatdeliverytype.equals("Radiant"))
							 {
								 ((TextView)findViewById(R.id.watersystemtit)).setVisibility(View.GONE);
								 ((TextView)findViewById(R.id.watersyscolon)).setVisibility(View.GONE);
								 sp_watersystype.setVisibility(View.GONE);
							 }
							 else
							 {
								 ((TextView)findViewById(R.id.watersystemtit)).setVisibility(View.VISIBLE);
								 ((TextView)findViewById(R.id.watersyscolon)).setVisibility(View.VISIBLE);
								 sp_watersystype.setVisibility(View.VISIBLE);
								 System.out.println("watersystemtype"+watersystemtype);
								 sp_watersystype.setSelection(watersysarrayadap.getPosition(watersystemtype));
							 }
			    		 }
						 else
						 {
							 ((LinearLayout)findViewById(R.id.lin01)).setVisibility(View.GONE);
						 }
						 
						 
					}
				}catch(Exception e){}
			
		}
		private void displayzones() {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur;
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadZones + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateZones + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 try
					{
					 zonestblshow.removeAllViews();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.zones);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();
			
				 zonestblshow.setVisibility(View.VISIBLE); 
				 
				 zonestblshow.addView(th,lp); 
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String zonename="",zonenameother="",untitype="",yearreplacement="",testedfor="",tonnage="",tonnageother="";
					 
					 zonename=cur.getString(cur.getColumnIndex("fld_zonename"));
					 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_zonenameother")));
					 untitype=cur.getString(cur.getColumnIndex("fld_unitype"));
					 yearreplacement=cur.getString(cur.getColumnIndex("fla_zone5yearreplacement"));
					 testedfor =cur.getString(cur.getColumnIndex("fld_zonetestedfor"));
					 tonnage = cur.getString(cur.getColumnIndex("fld_zonetonnage"));
					 tonnageother = db.decode(cur.getString(cur.getColumnIndex("fld_zonetonnageother")));
					
					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.zonelin);
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     
				     
				     TextView tvhzonename= (TextView) t.findViewWithTag("Zonename");
				     if(zonename.equals("Other"))
					 {
				    	 tvhzonename.setText(zonenameother);	
					 }
					 else
					 {
						 tvhzonename.setText(zonename);
					 }
				     
				     TextView tvuntitype= (TextView) t.findViewWithTag("Unittype");
				     if(untitype.equals("--Select--"))
					 {
				    	 untitype="";
					 }
				     tvuntitype.setText(untitype);
				     
				     TextView tvyearrep= (TextView) t.findViewWithTag("yearreplacement");
				     if(yearreplacement.equals("--Select--"))
					 {
				    	 yearreplacement="";
					 }
				     tvyearrep.setText(yearreplacement);
				     
				     
				     TextView tvtestedfor= (TextView) t.findViewWithTag("Testedfor");
				     if(testedfor.equals("--Select--"))
					 {
				    	 testedfor="";
					 }
				     tvtestedfor.setText(testedfor);					 
				     
				     
				     TextView tvtonnage= (TextView) t.findViewWithTag("Tonnage");
				     if(tonnage.equals("--Select--"))
					 {
				    	 tonnage="";
					 }
				     if(tonnage.equals("Other"))
					 {
				    	 tvtonnage.setText(tonnageother);	
					 }
					 else
					 {
						 tvtonnage.setText(tonnage);
					 }System.out.println("tonname");
				     
							
					ImageView edit= (ImageView) t.findViewWithTag("editzones");
				 	edit.setId(789456+i);
				 	edit.setTag(cur.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						   int i=Integer.parseInt(v.getTag().toString());
						   updzone=1;
						   updatezones(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
	             
	                ImageView delete=(ImageView) t.findViewWithTag("delzones");  
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(CreateHeating.this);
							builder.setMessage("Do you want to delete the selected Zones details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==Zones_select_id)
													{
														Zones_select_id=0;
													}
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadZones+" Where z_id='"+i+"'");	
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateZones+" Where z_id='"+i+"'");
													}
																										
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
													Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadZones + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname).toLowerCase()+"'",null);
													if(cur.getCount()==0)
													{
														flag3=1;
													}
													clearzones();
													
													displayzones();
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					
					
					l1.removeAllViews();
					
					zonestblshow.addView(t,lp);
					cur.moveToNext();
					
				 }
			 }
			 else
			 {
				 zonestblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	 	
	}
	protected void updatezones(int i) {
		// TODO Auto-generated method stub
		Cursor cur;
		Zones_select_id=i;
	   ((Button)findViewById(R.id.btn_addzones)).setText("Update");
		try{
			if(getcurrentview.equals("start"))
			{
				cur= db.hi_db.rawQuery("select * from " + db.LoadZones + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'",null);
			}
			else
			{
				cur= db.hi_db.rawQuery("select * from " + db.CreateZones+ " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'",null);
			}
			
			if(cur.getCount()>0)
			{
				cur.moveToFirst();
				
				 String zonename="",zonenameother="",untitype="",yearreplacement="",testedfor="",tonnage="",tonnageother="";
				 
				 zonename=cur.getString(cur.getColumnIndex("fld_zonename"));
				 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_zonenameother")));
				 untitype=cur.getString(cur.getColumnIndex("fld_unitype"));
				 yearreplacement=cur.getString(cur.getColumnIndex("fla_zone5yearreplacement"));
				 testedfor = cur.getString(cur.getColumnIndex("fld_zonetestedfor"));
				 tonnage =cur.getString(cur.getColumnIndex("fld_zonetonnage"));
				 tonnageother = db.decode(cur.getString(cur.getColumnIndex("fld_zonetonnageother")));
				 
				 sp_zonename.setSelection(zonenameadap.getPosition(zonename));sp_zonename.setEnabled(false);
				if(zonename.equals("Other"))
				 {
					 ed_otherzonename.setText(zonenameother);
					 ed_otherzonename.setVisibility(View.VISIBLE);
				 }
				 else
				 {
					 ed_otherzonename.setVisibility(View.INVISIBLE);
				 }
				 sp_zonesunittype.setSelection(zoneunittypeadap.getPosition(untitype));
				 sp_zones5year.setSelection(zone5yearadap.getPosition(yearreplacement));
				 sp_zonestestedfor.setSelection(zonetestedforadap.getPosition(testedfor));;
				 
				 sp_zonetonnage.setSelection(zonetonnadap.getPosition(tonnage));;
				 if(tonnage.equals("Other"))
				 {
					 ed_otherzonetonnage.setText(tonnageother);
					 ed_otherzonetonnage.setVisibility(View.VISIBLE);
					 ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
				 }
				 else
				 {
					 ed_otherzonename.setVisibility(View.INVISIBLE);
					 ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
				 }System.out.println("teslastt");
			}
		}catch(Exception e){
			
			System.out.println("tets catch="+e.getMessage());
		}
	}

	class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
	
		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
			
		}

		public int getCount() {
			int arrlen = 0;
			
				arrlen = arrheatingnumber.length;
				return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
				holder = new ViewHolder();
System.out.println("testttt");
				convertView = mInflater.inflate(R.layout.listview, null);
				holder.text = (TextView) convertView.findViewById(R.id.TextView01);
				holder.text2 = (TextView) convertView.findViewById(R.id.TextView02);
				holder.text3 = (TextView) convertView.findViewById(R.id.TextView04);
				holder.createtxt = (TextView) convertView.findViewById(R.id.createbtn);
				holder.deletetxt = (TextView) convertView.findViewById(R.id.deletebtn);
				
				convertView.setTag(holder);
						
				if(arrheatingnumber[position].contains("null")||arrheatingnumother[position].contains("null")||arrheatingnumother[position].contains("")
						||arrheatingsysteme[position].contains("null")||arrmanufacture[position].contains("")
						||arrmodelnumber[position].contains("") ) {
					arrheatingnumber[position] = arrheatingnumber[position].replace("null", "");
					arrheatingnumother[position] = arrheatingnumother[position].replace("null", "");
					arrheatingsysteme[position] = arrheatingsysteme[position].replace("null", "");
					arrmanufacture[position] = arrmanufacture[position].replace("", "N/A");
					arrmodelnumber[position] = arrmodelnumber[position].replace("", "N/A");
				
					if(arrheatingnumber[position].equals("Other"))
					{
						arrheatingnumber[position] = arrheatingnumber[position]+arrheatingnumother[position];
					}
					System.out.println("arrmodelnumber[position]="+arrmodelnumber[position]);
					holder.text.setText(Html.fromHtml(arrheatingnumber[position]+"|"+arrheatingsysteme[position]+"|"+arrmanufacture[position]+"|"+arrmodelnumber[position]));
					
				}
				else
				{
					holder.text.setText(Html.fromHtml(arrheatingnumber[position]+"|"+arrheatingsysteme[position]+"|"+arrmanufacture[position]+"|"+arrmodelnumber[position]));
				
				}
			/*	holder.createtxt.setOnClickListener(new CreateClickListner(position));
				holder.deletetxt.setOnClickListener(new DeleteClickListner(position));*/
				
				holder.text.setText(Html.fromHtml(arrheatingnumber[position]+"|"+arrheatingsysteme[position]));
		
			return convertView;
		}

		class ViewHolder {
			TextView text,createtxt,deletetxt,text2,text3;
		
		}

	}
	public void clicker(View v)
	{
		switch (v.getId()) 
		{
	
			case R.id.shwheating:	
				flag1=1;
					hideotherlayouts();
					((TableLayout)findViewById(R.id.heatlin)).setVisibility(View.VISIBLE);
					((ImageView)findViewById(R.id.hdwheating)).setVisibility(View.VISIBLE);
					((ImageView)findViewById(R.id.shwheating)).setVisibility(View.GONE);
				
				break;
			case R.id.hdwheating:
				flag1=0;
				((TableLayout)findViewById(R.id.heatlin)).setVisibility(View.GONE);
				((ImageView)findViewById(R.id.hdwheating)).setVisibility(View.GONE);
				((ImageView)findViewById(R.id.shwheating)).setVisibility(View.VISIBLE);
			break;
			case R.id.shwcooling:
				flag2=1;
				hideotherlayouts();
				((TableLayout)findViewById(R.id.coolinglin)).setVisibility(View.VISIBLE);
				((ImageView)findViewById(R.id.hdcooling)).setVisibility(View.VISIBLE);
				((ImageView)findViewById(R.id.shwcooling)).setVisibility(View.GONE);
			
			break;
			case R.id.hdcooling:
				flag2=0;
				((TableLayout)findViewById(R.id.coolinglin)).setVisibility(View.GONE);
				((ImageView)findViewById(R.id.hdcooling)).setVisibility(View.GONE);
				((ImageView)findViewById(R.id.shwcooling)).setVisibility(View.VISIBLE);
				
			case R.id.shwzones:	
				flag3=1;
				hideotherlayouts();
				((TableLayout)findViewById(R.id.zoneslin)).setVisibility(View.VISIBLE);
				((ImageView)findViewById(R.id.hdzones)).setVisibility(View.VISIBLE);
				((ImageView)findViewById(R.id.shwzones)).setVisibility(View.GONE);
			
			break;
			case R.id.hdzones:
				flag3=0;
				((TableLayout)findViewById(R.id.zoneslin)).setVisibility(View.GONE);
				((ImageView)findViewById(R.id.hdzones)).setVisibility(View.GONE);
				((ImageView)findViewById(R.id.shwzones)).setVisibility(View.VISIBLE);
		break;
	
		}
	}
	private void hideotherlayouts() {
		// TODO Auto-generated method stub
		((TableLayout)findViewById(R.id.coolinglin)).setVisibility(View.GONE);
		((TableLayout)findViewById(R.id.heatlin)).setVisibility(View.GONE);
		((TableLayout)findViewById(R.id.zoneslin)).setVisibility(View.GONE);
		
		((ImageView)findViewById(R.id.shwheating)).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.shwcooling)).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.shwzones)).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.hdwheating)).setVisibility(View.GONE);
		((ImageView)findViewById(R.id.hdcooling)).setVisibility(View.GONE);
		((ImageView)findViewById(R.id.hdzones)).setVisibility(View.GONE);
		
		
	}
	
	protected void clearheating() {
		// TODO Auto-generated method stub
		sp_heatingtype.setSelection(0);getheatingtype="";strotherheattype="";sp_heatingtype.setEnabled(true);
		sp_energysource.setSelection(0);getenergysource="";strenergysource="";
		sp_heatingnumber.setSelection(0);getheatingno="";strotherheatingnum="";
		spin_heatingage.setSelection(0);getheatingage="";strotherheatingage="";
		sp_heatbrand.setSelection(0);getheatbrand="";
		sp_furnace.setSelection(0);getfurnace="";
		sp_heatdelivery.setSelection(0);getheatdeliverytype="";
		sp_motorblower.setSelection(0);getmotorblower="";
		sp_watersystype.setSelection(0);getwatersystemtype="";
		sp_radiators.setSelection(0);getradiators="";
		sp_woodstoves.setSelection(0);getwoodstoves="";strotherwoodstoves="";
		sp_fueltank.setSelection(0);getfueltank="";
		sp_furnfeat.setSelection(0);getfurnfeatures="";
		spin_supplyregister.setSelection(0);getsupplyregister="";	
		sp_returnregister.setSelection(0); getreturnregister="";
		
		
		ed_otherheattype.setText("");ed_otherenergysource.setText("");ed_otherheatignno.setText("");ed_otherheatingage.setText("");ed_otherwoodstoves.setText("");
		sp_energysource.setSelection(0);
		ed_otherheattype.setVisibility(View.INVISIBLE);
		ed_otherenergysource.setVisibility(View.INVISIBLE);
		ed_otherheatignno.setVisibility(View.INVISIBLE);
		ed_otherheatingage.setVisibility(View.INVISIBLE);
		ed_otherwoodstoves.setVisibility(View.INVISIBLE);
		
		((Button)findViewById(R.id.btn_heatadd)).setText("Save/Add more Heating system");
	}
	protected void clearzones() {
		// TODO Auto-generated method stub
		sp_zonename.setSelection(0);getzonename="";strzonename="";sp_zonename.setEnabled(true);
		sp_zonesunittype.setSelection(0);getzoneunittype="";
		sp_zones5year.setSelection(0);getzone5yearrep="";
		sp_zonestestedfor.setSelection(0);gezonetestedfor="";
		sp_zonetonnage.setSelection(0);getzonetonnage="";strzonetonnage="";		
		
		ed_otherzonename.setText("");ed_otherzonetonnage.setText("");((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
		ed_otherzonename.setVisibility(View.INVISIBLE);
		ed_otherenergysource.setVisibility(View.INVISIBLE);
		
		((Button)findViewById(R.id.btn_addzones)).setText("Save/Add more Zones");
	}
	protected void clearcooling() {
		// TODO Auto-generated method stub
		sp_coolingequipment.setSelection(0);getcoolingequipment="";sp_coolingequipment.setEnabled(true);
		sp_coolingenergysource.setSelection(0);getcoolingenrgysource="";strenergysource="";
		sp_coolingmanufac.setSelection(0);getcenterairmanuf="";
		sp_noofcoolingzones.setSelection(0);getcoolinoofzones="";strcoolingnoofzones="";
		sp_coolingage.setSelection(0);getcoolingage="";
		sp_condensdischarge.setSelection(0);getconddischarge="";
		sp_coolingactesting.setSelection(0);getacsystem="";
		sp_coolingthermostats.setSelection(0);getthermostats="";
		sp_coolinggaslog.setSelection(0);getgaslog="";
		
		ed_coolingage.setVisibility(View.INVISIBLE);
		ed_coolingnoofzones.setVisibility(View.INVISIBLE);
		ed_othercoolingeergysource.setVisibility(View.INVISIBLE);
		
		((Button)findViewById(R.id.btn_cooladd)).setText("Save/Add more Cooling system");
	}
	class myspinselectedlistener implements OnItemSelectedListener
	{
         int var=0;
        
		public myspinselectedlistener(int i) {
			// TODO Auto-generated constructor stub
			var=i;
			 getspinselectedname="";
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			getspinselectedname = arg0.getItemAtPosition(arg2).toString();
			switch(var)
			{
				case 1:
					 getheatdeliverytype = getspinselectedname;
					 if(getheatdeliverytype.equals("Hot Water Heat") || getheatdeliverytype.equals("Boiler") ||  getheatdeliverytype.equals("Radiant"))
		    		 {		
						 ((LinearLayout)findViewById(R.id.lin01)).setVisibility(View.VISIBLE);
						 if(getheatdeliverytype.equals("Radiant"))
						 {
							 ((TextView)findViewById(R.id.watersystemtit)).setVisibility(View.GONE);
							 ((TextView)findViewById(R.id.watersyscolon)).setVisibility(View.GONE);
							 sp_watersystype.setVisibility(View.GONE);
						 }
						 else
						 {
							 ((TextView)findViewById(R.id.watersystemtit)).setVisibility(View.VISIBLE);
							 ((TextView)findViewById(R.id.watersyscolon)).setVisibility(View.VISIBLE);
							 sp_watersystype.setVisibility(View.VISIBLE);
						 }
		    		 }
					 else
					 {
						 ((LinearLayout)findViewById(R.id.lin01)).setVisibility(View.GONE);
					 }
		    			  
				break;
				case 2:
					 getenergysource = getspinselectedname;
					 if(!getenergysource.equals("--Select--"))
		    		 {			
						 if(getenergysource.equals("Other"))
				    	  {
							 ed_otherenergysource.setVisibility(arg1.VISIBLE);
				    		 ed_otherenergysource.requestFocus();
				    		 strenergysource = ed_otherenergysource.getText().toString();
				    	  }
				    	  else
				    	  {
				    		  ed_otherenergysource.setText("");
				    		  ed_otherenergysource.setVisibility(arg1.INVISIBLE);
				    	  }
						 ((LinearLayout)findViewById(R.id.lin02)).setVisibility(View.VISIBLE);
		    		 }
					 else
					 {
						((LinearLayout)findViewById(R.id.lin02)).setVisibility(View.GONE);
					 }
		    			  
				break;
				case 3:
					 getheatingtype = getspinselectedname;
			    	  Cursor cur;
			    	  try
						{
			    		  if(getheatingtype.equals("Other"))
			    		  {
			    			  ed_otherheattype.setVisibility(arg1.VISIBLE);
			    			  ed_otherheattype.requestFocus();
			    			  strotherheattype = ed_otherheattype.getText().toString();
			    			  cur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatingtypeother='"+strotherheattype+"'", null);
			    		  }
			    		  else
			    		  {
			    			  ed_otherheattype.setVisibility(arg1.INVISIBLE);
			    			  cur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+getheatingtype+"'", null);
			    		  }
					   }
			    	  catch (Exception e) {
						// TODO: handle exception
					  }
				break;
				  case 4:
			    	  getheatingno = getspinselectedname;
			    	  if(getheatingno.equals("Other"))
			    	  {
			    		  ed_otherheatignno.setVisibility(arg1.VISIBLE);
			    		  ed_otherheatignno.requestFocus();
			    		  strotherheatingnum = ed_otherheatignno.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_otherheatignno.setText("");
			    		  ed_otherheatignno.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;	
				  case 5:
			    	  getheatingage = getspinselectedname;
			    	  if(getheatingage.equals("Other"))
			    	  {
			    		  ed_otherheatingage.setVisibility(arg1.VISIBLE);
			    		  ed_otherheatingage.requestFocus();
			    		  strotherheatingage = ed_otherheatingage.getText().toString();
			    	  }
			    	  else
			    	  {

			    		  ed_otherheatingage.setText("");
			    		  ed_otherheatingage.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
				  case 6:
					  getheatbrand = getspinselectedname;
			    	  break;
			   
			      case 7:
			    	  getfurnace = getspinselectedname;
			    	  break;
			    	  
			      case 8:			    	  
			    	  getwatersystemtype = getspinselectedname;
			    	  break;
			      case 9:
			    	  getradiators = getspinselectedname;
			    	  break;
			    	  
			      case 10:
			    	  getmotorblower = getspinselectedname;
			    	  break;
			    	  
			      case 11:
			    	  getwoodstoves = getspinselectedname;
			    	  if(getwoodstoves.equals("Other"))
			    	  {
			    		  ed_otherwoodstoves.setVisibility(arg1.VISIBLE);
			    		  ed_otherwoodstoves.requestFocus();
			    		  strotherwoodstoves = ed_otherwoodstoves.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_otherwoodstoves.setText("");
			    		  ed_otherwoodstoves.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
			      case 12:
			    	  getfueltank = getspinselectedname;
			    	  break;  
			      case 13:
			    	  getfurnfeatures = getspinselectedname;
			    	  break;  
			    	  
			      case 14:
			    	  getsupplyregister = getspinselectedname;
			    	  break;  
			    	  
			      case 15:
			    	  getreturnregister = getspinselectedname;
			    	  break;  
			    	  
			      case 16:
			    	  getcoolingequipment = getspinselectedname;
			    	  break;
			      case 17:
			    	  getcoolingenrgysource= getspinselectedname;
			    	  if(getcoolingenrgysource.equals("Other"))
			    	  {
			    		  ed_othercoolingeergysource.setVisibility(arg1.VISIBLE);
			    		  ed_othercoolingeergysource.requestFocus();
			    		  strcoolingenergysource = ed_othercoolingeergysource.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_othercoolingeergysource.setText("");
			    		  ed_othercoolingeergysource.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  
			    	  break;
			      case 18:
			    	  getcenterairmanuf= getspinselectedname;
			    	  break;
			      case 19:
			    	  getcoolinoofzones= getspinselectedname;
			    	  if(getcoolinoofzones.equals("Other"))
			    	  {
			    		  ed_coolingnoofzones.setVisibility(arg1.VISIBLE);
			    		  ed_coolingnoofzones.requestFocus();
			    		  strcoolingnoofzones = ed_coolingnoofzones.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_coolingnoofzones.setText("");
			    		  ed_coolingnoofzones.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
			      case 20:
			    	  getcoolingage= getspinselectedname;
			    	  if(getcoolingage.equals("Other"))
			    	  {
			    		  ed_coolingage.setVisibility(arg1.VISIBLE);
			    		  ed_coolingage.requestFocus();
			    		  strcoolingage = ed_coolingage.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_coolingage.setText("");
			    		  ed_coolingage.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
			      case 21:
			    	  getconddischarge= getspinselectedname;
			    	  break;
			      case 22:
			    	  getacsystem= getspinselectedname;
			    	  break;
			      case 23:
			    	  getthermostats= getspinselectedname;
			    	  break;
			      case 24:
			    	  getgaslog= getspinselectedname;
			    	  break;
			      case 25:
			    	  getzonename= getspinselectedname;
			    	  if(getzonename.equals("Other"))
			    	  {
			    		  ed_otherzonename.setVisibility(arg1.VISIBLE);
			    		  ed_otherzonename.requestFocus();
			    		  strzonename = ed_otherzonename.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_otherzonename.setText("");
			    		  ed_otherzonename.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
			      case 26:
			    	  getzoneunittype= getspinselectedname;
			    	  break;
			      case 27:
			    	  getzone5yearrep= getspinselectedname;
			    	  break;
			      case 28:
			    	  gezonetestedfor= getspinselectedname;
			    	  break;
			      case 29:
			    	  getzonetonnage= getspinselectedname;
			    	  if(getzonetonnage.equals("Other"))
			    	  {
			    		  ed_otherzonetonnage.setVisibility(arg1.VISIBLE);
			    		  ed_otherzonetonnage.requestFocus();
			    		  strzonetonnage = ed_otherzonetonnage.getText().toString();
			    		  ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
			    	  }
			    	  else
			    	  {
			    		  ed_otherzonetonnage.setText("");
			    		  ed_otherzonetonnage.setVisibility(arg1.INVISIBLE);
			    		  ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
			    	  }
			    	  break;
			    	  
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	protected void back() {
		// TODO Auto-generated method stub
		Intent intent;
		
		if(getcurrentview.equals("start"))
		{
			db.userid();
			intent = new Intent(CreateHeating.this, LoadModules.class);
			Bundle b1 = new Bundle();
			b1.putString("selectedid", getselectedho);			
			intent.putExtras(b1);
			
			Bundle b2 = new Bundle();
			b2.putString("inspectorid", db.UserId);			
			intent.putExtras(b2);
			startActivity(intent);System.out.println("finished");
			finish();
		}
		else
		{	
			 if(gettempoption.equals("Report Section/Module"))
		        {
		       	  intent = new Intent(CreateHeating.this, CreateTemplate.class);
		        }
		        else
		        {
		       	 intent = new Intent(CreateHeating.this, CreateModule.class);
		        }
		        System.out.println("gettemp="+gettempoption);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("tempoption", gettempoption);			
				intent.putExtras(b5);
				startActivity(intent);
				finish();
		}
		
       
	}
	protected void onSaveInstanceState(Bundle ext)
	{		
		 if (p_sv != null) {
			  p_sv.progress_live=false;
		 }
		ext.putInt("flagvalue1", flag1);
		ext.putInt("flagvalue2", flag2);
		ext.putInt("flagvalue3", flag3);
		
		ext.putInt("updflagheating", updheating);
		ext.putInt("updflagcooling", udpcooling);
		ext.putInt("updflagzone", updzone);
		ext.putInt("handler", handler_msg);
		super.onSaveInstanceState(ext);
	}
	protected void onRestoreInstanceState(Bundle ext)
	{
		flag1 = ext.getInt("flagvalue1");
		if(flag1==1)
		{
			hideotherlayouts();
			((TableLayout)findViewById(R.id.heatlin)).setVisibility(View.VISIBLE);
			((ImageView)findViewById(R.id.hdwheating)).setVisibility(View.VISIBLE);
			((ImageView)findViewById(R.id.shwheating)).setVisibility(View.GONE);
		}
		flag2 = ext.getInt("flagvalue2");
		if(flag2==1)
		{
			hideotherlayouts();
			((TableLayout)findViewById(R.id.coolinglin)).setVisibility(View.VISIBLE);
			((ImageView)findViewById(R.id.hdcooling)).setVisibility(View.VISIBLE);
			((ImageView)findViewById(R.id.shwcooling)).setVisibility(View.GONE);
		}
		flag3 = ext.getInt("flagvalue3");
		if(flag3==1)
		{
			hideotherlayouts();
			((TableLayout)findViewById(R.id.zoneslin)).setVisibility(View.VISIBLE);
			((ImageView)findViewById(R.id.hdzones)).setVisibility(View.VISIBLE);
			((ImageView)findViewById(R.id.shwzones)).setVisibility(View.GONE);
		}
		
		if(ext.getInt("updflagheating")==1)
		{
			((Button)findViewById(R.id.btn_heatadd)).setText("Update");
		}
		if(ext.getInt("updflagcooling")==1)
		{
			((Button)findViewById(R.id.btn_cooladd)).setText("Update");
		}
		if(ext.getInt("updflagzone")==1)
		{
			((Button)findViewById(R.id.btn_addzones)).setText("Update");
		}
		
		 handler_msg = ext.getInt("handler");System.out.println("hanlermess="+handler_msg);
		  if(handler_msg==0)
	    	{
	    		success_alert();
	    	}
	    	else if(handler_msg==1)
	    	{
	    		failure_alert();
	    	}
		 super.onRestoreInstanceState(ext);
	}
}

