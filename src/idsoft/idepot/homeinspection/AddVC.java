package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DataBaseHelperVC;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;


public class AddVC extends Activity{
	Button btn_back,btn_save,btn_clear,btn_primary,btn_secondary;
	Spinner sp_module,sp_submodule,sp_primaryvc;
	ImageView loadcomments;
	ArrayAdapter moduleadap,primaryadap,secondaryadap,submodulearraydap;
	EditText addvc;
	String getsectext="",getinsptext="",getconttext="";
	boolean load_comment=true;
	int sec_dialog=0;;
	CommonFunction cf;
	DatabaseFunction db;
	LinearLayout lin;
	String getmodulename="",getsubmodulename="",getprimaryvc="",getsecondaryvc="",strrd_cond="",getinspectionpoint="",getconttype="",str_ip="",str_cont="",strother="",strcomments="",strchkval="",str="",strip="",
			seltdval="",selcontval="",selectvalinsppoint="",selectvalseccond="",selectvalcont="";
	String[] primaryarr,secondaryyarr,module,submodulearr;
	EditText etcomments,otheret,etother;
	TextView tvcomment,sp_inspectionpoint,sp_secondaryvc,sp_conttype;
	boolean rgcond=false;
	RadioGroup rgcnd;
	String get_scon="";
	DataBaseHelperVC dbh;
	View v;
	CheckBox[] ch;
	int insp_dialog=0,dialog_other=0,cont_dialog=0;
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	//public int[] chkbxid = {R.id.chk1,R.id.chk2,R.id.chk3,R.id.chk4};
	//public CheckBox[] cb = new CheckBox[chkbxid.length];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
         setContentView(R.layout.activity_vc);
         cf = new CommonFunction(this);
         db = new DatabaseFunction(this);
         
         db.CreateTable(1);
         db.CreateTable(5);
         db.CreateTable(29);
         db.CreateTable(35);
         db.userid();
                 
       final  ScrollView scr = (ScrollView)findViewById(R.id.submodulelist);
       scr.post(new Runnable() {
          public void run() {
              scr.scrollTo(0,0);
          }
      }); 
        
        sp_module = (Spinner)findViewById(R.id.spin_module);
        sp_submodule = (Spinner)findViewById(R.id.spin_submodule);
        sp_submodule.setEnabled(false);
        sp_submodule.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(3));
        
        module = getResources().getStringArray(R.array.module);
  		moduleadap = new ArrayAdapter<String>(AddVC.this,android.R.layout.simple_spinner_item,module);
  		moduleadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
  		sp_module.setAdapter(moduleadap);
  		
  		
  		
  		
  		sp_module.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(1));
         
        sp_primaryvc = (Spinner)findViewById(R.id.spin_primaryvc);
        sp_primaryvc.setEnabled(false);
  
        etcomments = (EditText)findViewById(R.id.comments);
        tvcomment = (TextView)findViewById(R.id.tvcomment);
        etcomments.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
    			if(s.toString().startsWith(" "))
    			{
    				etcomments.setText("");
    			}
    			cf.ShowingLimit(s.toString(),tvcomment,1000); 
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				

			}
		});
        
        sp_primaryvc.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(2));
      
        
		sp_inspectionpoint = (TextView)findViewById(R.id.spin_inspectionpoint);
		sp_inspectionpoint.setText("--Select--");
		sp_inspectionpoint.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("click ="+getinspectionpoint);
				insp_dialog=1;
				showalert();
			}
		});
  		
         btn_back = (Button)findViewById(R.id.btn_back);
         btn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(AddVC.this,HomeScreen.class));
				finish();
			}
		});
         
         btn_save = (Button)findViewById(R.id.ok);
         btn_save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 strcomments = etcomments.getText().toString();
				 
				 if(getmodulename.equals("--Select--"))
				 {
					 cf.DisplayToast("Please select Module");
				 }
				 else if(getsubmodulename.equals("--Select--"))
				 {
					 cf.DisplayToast("Please select Sub Module");
				 }
				 else
				 {
					 if(getprimaryvc.equals("--Select--"))
					 {
						 cf.DisplayToast("Please select Primary Condition");
					 }
					 else 
					 {
						 /*if(get_scon.equals(""))
						 {
							 cf.DisplayToast("Please select Secondary Condition");
						 }
						 else
						 {*/
							 System.out.println("strcomments="+strcomments);
							 if(strcomments.equals(""))
							 {
								 cf.DisplayToast("Please enter Comments");
							 }
							 else
							 {
								/* if(strrd_cond.equals(""))
								 {
									 cf.DisplayToast("Please select Condition");
								 }
								 else
								 {
									 if(strrd_cond.equals("Other"))
									 {
										 if(strother.equals(""))
										 {
											 cf.DisplayToast("Please enter the Other text");
										 }
										 else
										 {*/
											 checkboxvalidation();
									/*	 }
									 }
									 else
									 {
										 checkboxvalidation();
									 }
								 }*/
							 }
						 //}
					 }
				 }
			}
		});
         
         btn_clear = (Button)findViewById(R.id.clear);
         btn_clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clearall();
				 
			}
		});
     
         btn_primary = (Button)findViewById(R.id.addnewprimary);
         btn_primary.setOnClickListener(new OnClickListener() {			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 				if(sp_module.getSelectedItem().toString().equals("--Select--"))
				{
					cf.DisplayToast("Please select Module");
				}
				else
				{
					addnewprimcondition(1,"Primary Condition");
					//primaryconditionindb(db.encode(sp_module.getSelectedItem().toString()));
				}
 			}
 		});
         
         btn_secondary = (Button)findViewById(R.id.addnewsecondary);
         btn_secondary.setOnClickListener(new OnClickListener() {
 			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 				if(sp_module.getSelectedItem().toString().equals("--Select--"))
				{
					cf.DisplayToast("Please select Module");
				}
				else
				{
					addnewprimcondition(2,"Secondary Condition");
				}
 				
 			}
 		});
     	sp_conttype = (TextView)findViewById(R.id.spin_contractortype);
		sp_conttype.setText("--Select--");
		sp_conttype.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cont_dialog=1;
				showcontdialog();
			}
		});        
         
        sp_secondaryvc = (TextView)findViewById(R.id.spin_secondaryvc);
        sp_secondaryvc.setEnabled(false);
 		sp_secondaryvc.setText("--Select--");
 		sp_secondaryvc.setOnClickListener(new OnClickListener() {
 			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 				sec_dialog=1;
 				dbsecvalue();
 				showsec_alert();
 			}
 		});
 		loadcomments = (ImageView)findViewById(R.id.loadcomments);
 		loadcomments.setOnClickListener(new OnClickListener() { 			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 		
 					if(!sp_module.getSelectedItem().toString().equals("--Select--"))
 					{
 						if(load_comment)
 						{
 							load_comment=false;
 							int loc1[] = new int[2];
 							v.getLocationOnScreen(loc1);
 							
 							Intent in =new Intent(AddVC.this,LoadComments.class);
 						 	in.putExtra("mod_name", sp_module.getSelectedItem().toString());
 						 	in.putExtra("submod_name", sp_submodule.getSelectedItem().toString());
 							//in.putExtra("submod_name", sp_submodule.getSelectedView().toString());
 							in.putExtra("vcname", sp_primaryvc.getSelectedItem().toString());
 							in.putExtra("length", 12);
 							in.putExtra("max_length", 1000);
 							in.putExtra("xfrom", loc1[0]+10);
 							in.putExtra("yfrom", loc1[1]+10);
 							startActivityForResult(in, 34);
 						}
 					}
 					else
 					{
 						cf.DisplayToast("Please select Module");
 					}
 			
 			}
 		});
 		
 		
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
 		try
 		{
 			System.out.println("rrrr="+requestCode);
 			if(requestCode==34)
 			{
 				System.out.println("DSdfds");
 				load_comment=true;
 				if(resultCode==RESULT_OK)
 				{
 					String comm = ((EditText)findViewById(R.id.comments)).getText().toString();
 					((EditText)findViewById(R.id.comments)).setText(comm + data.getExtras().getString("Comments"));System.out.println("dfff");
	 			}
 			}
 		}
 		catch (Exception e) {
 			// TODO: handle exception
 			System.out.println("the erro was "+e.getMessage());
 		}
 	}/**on activity result for the load comments ends**/
	protected void showcontdialog() {
		// TODO Auto-generated method stub
		str="";
		final Dialog dialog = new Dialog(AddVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		LinearLayout lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Contractor Type");
		
		if(getconttype.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.conttypedisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.conttypedisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.conttypedisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getconttype;
		}

		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);
		
		Button addnew = ((Button) dialog.findViewById(R.id.addnew));
		addnew.setOnClickListener(new OnClickListener() {
            @Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
            	dialogshow();
            }
		});
		
		 ch = new CheckBox[cf.contractortype_arr.length];
		for (int i = 0; i < cf.contractortype_arr.length; i++) {			
			ch[i] = new CheckBox(AddVC.this);
		    ch[i].setText(cf.contractortype_arr[i]);
			ch[i].setTextColor(Color.BLACK);
			lin.addView(ch[i]);		
		}

		if (str != "") {
			cf.setValuetoCheckbox(ch, str);
			((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+str));
		}
		
		for (int i = 0; i < cf.contractortype_arr.length; i++) {
			if (ch[i].isChecked()) {
				seltdval += ch[i].getText().toString() + "&#44;";
			}
		}

		getconttype = seltdval;

		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				seltdval = "";
				for (int i = 0; i < cf.contractortype_arr.length; i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
			
				getconttype = seltdval;
				System.out.println("Okstrgetconttype"+getconttype);cont_dialog=0;
				((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
				String conttxt = replacelastendswithcomma(getconttype);System.out.println("TTTT="+getconttype);
				((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+conttxt));
				dialog.dismiss();
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cont_dialog=0;dialog.dismiss();	
				if(getconttype.equals(""))
				{
					((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.GONE);
				}
			}
		});
		
		
		dialog.show();
	}
	protected void dialogshow() {
		// TODO Auto-generated method stub
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(AddVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.addnewcustom);
		
		etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
		cf.hidekeyboard(etother);
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		strother = etother.getText().toString();
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//CreateSubModule.sp=0;
				dialog_other=0;
				add_dialog.dismiss();
			}
		});
		
		Button btnsave = (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {
            String seltdval="";
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strother = etother.getText().toString();
				if (etother.getText().toString().trim().equals("")) {
					cf.DisplayToast("Please enter the other text");
					
				} else {
					System.out.println("insideelse");
					if(str_arrlst.contains(strother))
					{
						cf.DisplayToast("Already exists!! Please enter the new text");
						etother.setText("");
						etother.requestFocus();
					}
					else
					{
						try
						{
							
							Cursor cur = db.hi_db.rawQuery("select * from " + db.ADDNewCondition + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(sp_module.getSelectedItem().toString())+"' and fld_visiblecondition='"+db.encode(etother.getText().toString())+"'", null);
				           System.out.println("ddff"+cur.getCount());
							if(cur.getCount()==0)
				            {System.out.println("zero");
								System.out.println("fdsfds"+db.encode(getmodulename));
								System.out.println(" INSERT INTO "
										+ db.ADDNewCondition
										+ " (fld_inspectorid,fld_module,fld_visiblecondition,fld_isviscondprimorsecond) VALUES"
										+ "('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(strother)+"','3')");
				            	db.hi_db.execSQL(" INSERT INTO "
										+ db.ADDNewCondition
										+ " (fld_inspectorid,fld_module,fld_visiblecondition,fld_isviscondprimorsecond) VALUES"
										+ "('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(strother)+"','3')");
				            	
				            	cf.DisplayToast("Contractor Type added successfully");
								add_dialog.dismiss();
								cf.hidekeyboard(addvc);
								
				            }
				            else
							{
				            	cf.DisplayToast("Condition already exists.");
								add_dialog.dismiss();
							}
							
						}catch (Exception e) {
							// TODO: handle exception
							System.out.println("sdds"+e.getMessage());
						}
					}
				}
			}
		});
		
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	protected void showsec_alert() {
		sec_dialog=1;
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(AddVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Secondary Condition");
		
		secshow();
		
		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);
		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				String seltdval = "";
				for (int i = 0; i < str_arrlst.size(); i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
				
				String finalselstrval = "";
				finalselstrval = seltdval;
				System.out.println("final"+finalselstrval);
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{			
					getsecondaryvc = seltdval;
					secshow();
					dialog.dismiss();sec_dialog=0;
				}
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sec_dialog=0;dialog.dismiss();	
				System.out.println("getsecondaryvc cancel"+getsecondaryvc);
				if(getsecondaryvc.equals(""))
				{
					((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);
				}
				else
				{
					((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);
				}
			}
		});

		
		Button btnother = (Button) dialog.findViewById(R.id.addnew);
		btnother.setVisibility(v.GONE);
		
		
		dialog.show();
	}
	private void secshow() {
		// TODO Auto-generated method stub
		if(getsecondaryvc.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.secconddisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.secconddisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.secconddisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getsecondaryvc;
		}
		
		ch = new CheckBox[str_arrlst.size()];
		for (int i = 0; i < str_arrlst.size(); i++) {

			ch[i] = new CheckBox(AddVC.this);
			ch[i].setText(str_arrlst.get(i));
			ch[i].setTextColor(Color.BLACK);
			lin.addView(ch[i]);
		}		
		

		if (str != "") {System.out.println("inside"+str);
			cf.setValuetoCheckbox(ch, str);
			((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);
			String replaceendcommaseccond= replacelastendswithcomma(str);
			((TextView) findViewById(R.id.secconddisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaseccond));
		}
		else
		{
			((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);
		}
	}
	
	protected void dbsecvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		//System.out.println("secondaryyarr"+secondaryyarr.length);
		/*for(int j=0;j<secondaryyarr.length;j++)
		{
			System.out.println("sec="+secondaryyarr[j]);
			
			 str_arrlst.add(secondaryyarr[j]);
	
		}*/
		
		try {
			dbh = new DataBaseHelperVC(AddVC.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from VCRANGE where RANGETABLE='"+sp_module.getSelectedItem().toString()+"'",null);
			System.out.println("CCC="+cur.getCount());
			cur.moveToFirst();
			for(int k =0;k<cur.getCount();k++,cur.moveToNext())
			{
				 str_arrlst.add(db.decode(cur.getString(cur.getColumnIndex("VCTEXT"))));
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		
		try
		{
			 Cursor c=db.hi_db.rawQuery("select * from " + db.ADDNewCondition + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(sp_module.getSelectedItem().toString())+"' and fld_isviscondprimorsecond='2'",null);
			 System.out.println("testsdbsec="+c.getCount());
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =0;i<c.getCount();i++,c.moveToNext())
				 {
					 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_visiblecondition"))));
				 }
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	protected void addnewprimcondition(final int id,final String title) {
		// TODO Auto-generated method stub
		
		final Dialog add_dialog = new Dialog(AddVC.this);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.editcustomvc);
		
		((TextView)add_dialog.findViewById(R.id.header)).setText(title);
		((TextView)add_dialog.findViewById(R.id.note)).setVisibility(v.VISIBLE);
		
		addvc = (EditText)add_dialog.findViewById(R.id.et_addnew);
		//addvc.setText(title);
		
		addvc.addTextChangedListener(new CustomTextWatcher(addvc));
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				add_dialog.dismiss();
				
			}
		});
		
		Button btnsave= (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(sp_module.getSelectedItem().toString().equals("--Select--"))
				{
					cf.DisplayToast("Please select Module");
				}
				else
				{
					if(addvc.getText().toString().trim().equals(""))
					{
						cf.DisplayToast("Please enter the "+title);
					}
					else
					{
							try
							{
							    Cursor cur = db.hi_db.rawQuery("select * from " + db.ADDNewCondition + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(sp_module.getSelectedItem().toString())+"' and fld_submodule='"+db.encode(sp_submodule.getSelectedItem().toString())+"' and fld_visiblecondition='"+db.encode(addvc.getText().toString())+"'", null);
					            if(cur.getCount()==0)
					            {
					            	db.hi_db.execSQL(" INSERT INTO "
											+ db.ADDNewCondition
											+ " (fld_inspectorid,fld_module,fld_submodule,fld_visiblecondition,fld_isviscondprimorsecond) VALUES"
											+ "('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(getsubmodulename)+"','"+db.encode(addvc.getText().toString())+"','"+id+"')");
					            	
					            	cf.DisplayToast(title+" added successfully");
									add_dialog.dismiss();
									cf.hidekeyboard(addvc);
									if(id==1)
									{
										primaryconditionindb(db.encode(getmodulename));
									}
					            }
					            else
								{
					            	cf.DisplayToast("Condition already exists.");
									add_dialog.dismiss();
								}
								
								
							}
							catch (Exception e) 
							{
								// TODO: handle exception
							}
					}
				}
			}
		});
		
		add_dialog.show();
	}
	protected void checkboxvalidation() {
		// TODO Auto-generated method stub
		System.out.println("getinspectionpoint checkboxvalidation="+getinspectionpoint);
			 if(getinspectionpoint.equals("") && ((TextView)findViewById(R.id.inspectionvaluedisplay)).getVisibility()==View.GONE)
			 {
				 cf.DisplayToast("Please select Inspection Point");
			 }
			 else
			 {
				 try
					{
					    Cursor cur = db.hi_db.rawQuery("select * from " + db.AddVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_primary='"+db.encode(getprimaryvc)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'", null);
			            if(cur.getCount()==0)
			            {
			            	System.out.println(" INSERT INTO "
								+ db.AddVC
								+ " (fld_inspectorid,fld_module,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype) VALUES"
								+ "('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(getprimaryvc)+"','"
								+db.encode(getsecondaryvc)+"','"+db.encode(strcomments)+"','','','"+strchkval+"','"+db.encode(getinspectionpoint)+"','"+ db.encode(((EditText) findViewById(R.id.edapprcost)).getText().toString())+"','"+db.encode(getconttype)+"')");
			            	db.hi_db.execSQL(" INSERT INTO "
									+ db.AddVC
									+ " (fld_inspectorid,fld_module,fld_submodule,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype) VALUES"
									+ "('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(getsubmodulename)+"','"+db.encode(getprimaryvc)+"','"
									+db.encode(getsecondaryvc)+"','"+db.encode(strcomments)+"','','','"+strchkval+"','"+db.encode(getinspectionpoint)+"','"+ db.encode(((EditText) findViewById(R.id.edapprcost)).getText().toString())+"','"+db.encode(getconttype)+"')");
			            	
			            }
			            else
			            {
			            	System.out.println("UPDATE "+db.AddVC+ " SET fld_secondary='"+db.encode(getsecondaryvc)+"',"
			            			+"fld_comments='"+db.encode(strcomments)+"',fld_checkbox='"+strchkval+"',"
			            			+"fld_inspectionpoint='"+db.encode(getinspectionpoint)+"',fld_apprrepaircost='"+ db.encode(((EditText) findViewById(R.id.edapprcost)).getText().toString())+"',fld_contractortype='"+db.encode(getconttype)+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_primary='"+db.encode(getprimaryvc)+"' and fld_module='"+db.encode(getmodulename)+"'");
			            	db.hi_db.execSQL("UPDATE "+db.AddVC+ " SET fld_secondary='"+db.encode(getsecondaryvc)+"',"
			            			+"fld_comments='"+db.encode(strcomments)+"',fld_checkbox='"+strchkval+"',"
			            			+"fld_inspectionpoint='"+db.encode(getinspectionpoint)+"',fld_apprrepaircost='"+ db.encode(((EditText) findViewById(R.id.edapprcost)).getText().toString())+"',fld_contractortype='"+db.encode(getconttype)+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_primary='"+db.encode(getprimaryvc)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'");
						
			            }
			            
					    cf.DisplayToast("Added successfully");
			            clearall();
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("tttt="+e.getMessage());
					}
			 }
	}

	private void clearall() {
		// TODO Auto-generated method stub
		System.out.println("dsfosddjs");
		 getinspectionpoint="";getconttype="";getsecondaryvc="";
		 ((EditText) findViewById(R.id.edapprcost)).setText("");	
		 etcomments.setText("");
         ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		 	((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.GONE);
		    ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);
		    sp_primaryvc.setSelection(0);
		    sp_module.setSelection(0);
		    sp_submodule.setSelection(0);
	}

	private void clear() {
		// TODO Auto-generated method stub
		System.out.println("ccllclclc");
		 getinspectionpoint="";getconttype="";getsecondaryvc="";
		 ((EditText) findViewById(R.id.edapprcost)).setText("");	
		 etcomments.setText("");
         ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.GONE);
		 ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);		
	}

	protected void showalert() {
		// TODO Auto-generated method stub
		str="";
		final Dialog dialog = new Dialog(AddVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Inspection Point");
		System.out.println("uuu"+getinspectionpoint);
		showinspoint();
		/*if(getinspectionpoint.equals(""))
		{
			str=str;
		}
		else
		{
			str=getinspectionpoint;
		}*/
		
		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);
		
		((Button) dialog.findViewById(R.id.addnew)).setVisibility(v.GONE);
		
		 /*ch = new CheckBox[cf.insp_arr.length];
		for (int i = 0; i < cf.insp_arr.length; i++) {
			
			ch[i] = new CheckBox(AddVC.this);
		    ch[i].setText(cf.insp_arr[i]);
			ch[i].setTextColor(Color.BLACK);
			lin.addView(ch[i]);
		
		}

		if (str != "") {
			cf.setValuetoCheckbox(ch, str);
			((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+str));
		}
		
		for (int i = 0; i < cf.insp_arr.length; i++) {
			if (ch[i].isChecked()) {
				seltdval += ch[i].getText().toString() + "&#44;";
			}
		}

		getinspectionpoint = seltdval;*/		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval = "";
				for (int i = 0; i < cf.insp_arr.length; i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
			
				getinspectionpoint = seltdval;
				showinspoint();
				dialog.dismiss();insp_dialog=0;
			}
		});
		
		/*btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval = "";
				for (int i = 0; i < cf.insp_arr.length; i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
			
				getinspectionpoint = seltdval;
				System.out.println("Okstr"+getinspectionpoint);insp_dialog=0;
				dialog.dismiss();
			}
		});
*/				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				insp_dialog=0;dialog.dismiss();
				if(getinspectionpoint.equals(""))
				{
					((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
				}
			}
		});
		
		
		dialog.show();
	}
	private void showinspoint() {
		// TODO Auto-generated method stub
		if(getinspectionpoint.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.inspectionvaluedisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getinspectionpoint;
		}
		
	
		
		ch = new CheckBox[cf.insp_arr.length];
		for (int i = 0; i < cf.insp_arr.length; i++) {
			
			ch[i] = new CheckBox(AddVC.this);
			System.out.println("value=="+cf.insp_arr[i]);
		    ch[i].setText(cf.insp_arr[i]);
			ch[i].setTextColor(Color.BLACK);
			lin.addView(ch[i]);
			System.out.println("what lin add");
		}
/*
		if (str != "") {
			cf.setValuetoCheckbox(ch, str);
			((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+str));
		}
		
		for (int i = 0; i < cf.insp_arr.length; i++) {
			if (ch[i].isChecked()) {
				seltdval += ch[i].getText().toString() + "&#44;";
			}
		}

		getinspectionpoint = seltdval;
		
		ch = new CheckBox[cf.insp_arr.length];
		for (int i = 0; i < cf.insp_arr.length; i++) {
			
			ch[i] = new CheckBox(AddVC.this);
		    ch[i].setText(cf.insp_arr[i]);
			ch[i].setTextColor(Color.BLACK);
			lin.addView(ch[i]);
		
		}
		*/
		/*for (int i = 0; i < cf.insp_arr.length; i++) {
			if (ch[i].isChecked()) {
				seltdval += ch[i].getText().toString() + "&#44;";
			}
		}*/
		System.out.println("what is str="+str);
		if (str != "") {
			System.out.println("camer her");
			 cf.setValuetoCheckbox(ch, str);System.out.println("endss");
			 String replaceendcommaionspoint= replacelastendswithcomma(str);System.out.println("DDDDD");
 			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
		}
		else
		{
			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		}
		
	}
	private String replacelastendswithcomma(String realword) {
		// TODO Auto-generated method stub
		
		if(realword.contains("&#44;"))
		{
			realword = realword.replace("&#44;",",");
		}
		if(realword.endsWith(","))
		{
			realword = realword.substring(0,realword.length()-1);
		}
		else
		{
			realword = realword;	
		}	
		return realword;
	}
	private void primaryconditionindb(String getmodulename)
	{
		String primvalue="--Select--,";
		cf.GetCurrentModuleName(getmodulename);
		
		try {
			dbh = new DataBaseHelperVC(AddVC.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from VCRANGE where RANGETABLE='"+sp_module.getSelectedItem().toString()+"'",null);
			System.out.println("CCC="+cur.getCount());
			cur.moveToFirst();
			for(int k =0;k<cur.getCount();k++,cur.moveToNext())
			{
				//System.out.println("VCTEXT="+db.decode(cur.getString(cur.getColumnIndex("VCTEXT"))));
				primvalue += db.decode(cur.getString(cur.getColumnIndex("VCTEXT")))+",";;
				 //primaryarr[k]=db.decode(cur.getString(cur.getColumnIndex("VCTEXT")));
				 System.out.println("PPP="+primvalue);
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}


		
		//primaryarr = cf.submodvcid;	
		/*System.out.println("primaryarr"+primaryarr.length);
		primaryadap = new ArrayAdapter<String>(AddVC.this,android.R.layout.simple_spinner_item,primaryarr);
		primaryadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	sp_primaryvc.setAdapter(primaryadap);*/
		
	 	try
		{
			 Cursor c=db.hi_db.rawQuery("select * from " + db.ADDNewCondition + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(sp_module.getSelectedItem().toString())+"' and fld_submodule='"+db.encode(sp_submodule.getSelectedItem().toString())+"' and fld_isviscondprimorsecond='1'",null);
			 System.out.println("cccc="+c.getCount());
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =1;i<c.getCount()+1;i++,c.moveToNext())
				 {
					 /*primaryarr[0]="--Select--";
					 primaryarr[i] =db.decode(c.getString(c.getColumnIndex("fld_visiblecondition")));
					 System.out.println("primaryarr[i]"+primaryarr[i]);*/
					 primvalue += db.decode(c.getString(c.getColumnIndex("fld_visiblecondition")))+",";
				 }
			 }
			 else
			 {
				 //primaryarr[0]="--Select--";
				 
			 }
			  //String[] temparr = primvalue.split(",");
			  for(int l=0;l<primvalue.length();l++)
			  {
				  primaryarr = primvalue.split(",");
			  }
			 
			 primaryadap = new ArrayAdapter<String>(AddVC.this,android.R.layout.simple_spinner_item,primaryarr);
				primaryadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 	sp_primaryvc.setAdapter(primaryadap);
			 
			 
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		     
		int j;
		   public MyOnItemSelectedListenerdata(int i) {
			// TODO Auto-generated constructor stub
			   j=i;
		}

		public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
		if(j==1)
			{
				getmodulename = parent.getItemAtPosition(pos).toString();
				System.out.println("tets11");
				if(!getmodulename.equals("--Select--"))				
				{
					sp_submodule.setEnabled(true);
					cf.GetCurrentModuleName(getmodulename);System.out.println("LENG="+cf.arrsub.length);
					
					
					submodulearr= new String[cf.arrsub.length+1];System.out.println("LENTH="+submodulearr.length);
					submodulearr[0] = "--Select--";
					for(int i=0;i<cf.arrsub.length;i++)
					{
						submodulearr[i+1] = cf.arrsub[i];
					}
							
					submodulearraydap = new ArrayAdapter<String>(AddVC.this,android.R.layout.simple_spinner_item,submodulearr);
			  		submodulearraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			  		sp_submodule.setAdapter(submodulearraydap);				
					
					sp_primaryvc.setEnabled(true);
					primaryconditionindb(getmodulename);
				 	if(!getprimaryvc.equals("--Select--") && !getprimaryvc.equals(""))
				 	{
				 		sp_primaryvc.setSelection(primaryadap.getPosition(getprimaryvc));
				 	}
				 	
				 	
					secondaryyarr = cf.submodvcid;	
				}	
				
			}
			else if(j==2)
			{
				
				getprimaryvc = parent.getItemAtPosition(pos).toString();
				if(!getmodulename.equals("--Select--"))				
				{
					sp_secondaryvc.setEnabled(true);
					try
					{
					    Cursor cur = db.hi_db.rawQuery("select * from " + db.AddVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_primary='"+db.encode(getprimaryvc)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'", null);
					    System.out.println("select * from " + db.AddVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_primary='"+db.encode(getprimaryvc)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'"+cur.getCount());
			            int cnt = cur.getCount();
			            if(cnt>0)
			            {
			            	cur.moveToFirst();
			            	
						 	String get_pcon = cur.getString(cur.getColumnIndex("fld_primary"));
						 	int pc = primaryadap.getPosition(get_pcon);
			            	sp_primaryvc.setSelection(pc);		            	
			            				            	
			            	String getcomments = db.decode(cur.getString(cur.getColumnIndex("fld_comments")));
			            	etcomments.setText(getcomments);
			            	((EditText) findViewById(R.id.edapprcost)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_apprrepaircost"))));
			            	getsecondaryvc = db.decode(cur.getString(cur.getColumnIndex("fld_secondary")));
			            	
			            	if(!getsecondaryvc.equals(""))
			            	{
			            		
			            		((TextView) findViewById(R.id.secconddisplay)).setVisibility(view.VISIBLE);
			            		String replaceendcommaseccond= replacelastendswithcomma(getsecondaryvc);
			            		((TextView) findViewById(R.id.secconddisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaseccond));
			            	}
			            	getconttype = db.decode(cur.getString(cur.getColumnIndex("fld_contractortype")));
			            	if(!getconttype.equals(""))
			            	{
			            		((TextView) findViewById(R.id.conttypedisplay)).setVisibility(view.VISIBLE);
			            		String replaceendcommacontype= replacelastendswithcomma(getconttype);
			            		((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommacontype));
			            	}
			            	getinspectionpoint = db.decode(cur.getString(cur.getColumnIndex("fld_inspectionpoint")));
			            	if(!getinspectionpoint.equals(""))
			            	{
			            		((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(view.VISIBLE);
			            		String replaceendcommainspoint= replacelastendswithcomma(getinspectionpoint);
			            		((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommainspoint));
			            	}
			            	
			            }
			            else
			            {
			            	clear();			            	
			            	
						 	System.out.println("ends"+strcomments);
						 	if(!strcomments.equals(""))
						 	{
						 	    etcomments.setText(strcomments);
						 	}
						 	
			            }
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
			else if(j==3)
			{
				getsubmodulename = parent.getItemAtPosition(pos).toString();
				System.out.println("gesubmodul="+getsubmodulename);
			}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	 @Override
	    protected void onSaveInstanceState(Bundle outState) 
	    {
	        outState.putString("module_index", getmodulename);
	        outState.putString("pc_index", getprimaryvc);	       
	        
	        outState.putInt("sec_index", sec_dialog);System.out.println("testsec="+sec_dialog);
	        if(sec_dialog==1)
	        {
	        	selectvalseccond="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalseccond += ch[i].getText().toString() + "&#44;";
					}
				}
	        	outState.putString("spoption_index", selectvalseccond);
	        }
	        else
	        {
	        	outState.putString("spoption_index", getsecondaryvc);
	        }
	        
	        outState.putInt("insp_index", insp_dialog);
	        if(insp_dialog==1)
	        {
	        	selectvalinsppoint="";
	        	for (int i = 0; i < cf.insp_arr.length; i++) {
					if (ch[i].isChecked()) {
						selectvalinsppoint += ch[i].getText().toString() + "&#44;";
					}
				}
	        	outState.putString("spinspoption_index", selectvalinsppoint);
	        }
	        else
	        {
	        	System.out.println("getinspectionpoint onSaveInstanceState"+getinspectionpoint);
	        	outState.putString("spinspoption_index", getinspectionpoint);
	        }
	        
	        outState.putInt("cont_index", cont_dialog);
	        if(cont_dialog==1)
	        {
	        	selectvalcont="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalcont += ch[i].getText().toString() + "&#44;";
					}
				}
	        	outState.putString("contoption_index", selectvalcont);
	        }
	        else
	        {
	        	outState.putString("contoption_index", getconttype);
	        }
	        System.out.println("ETComme="+etcomments.getText().toString());
	        outState.putString("comments_index", etcomments.getText().toString());
	        outState.putString("appr_cost",((EditText) findViewById(R.id.edapprcost)).getText().toString()); 
	        outState.putString("get_sectext", ((TextView) findViewById(R.id.secconddisplay)).getText().toString());
	        outState.putString("get_insptext", ((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString());
	        outState.putString("get_conttext", ((TextView) findViewById(R.id.conttypedisplay)).getText().toString());
	      
	        super.onSaveInstanceState(outState);
	 }
	 @Override
	    protected void onRestoreInstanceState(Bundle savedInstanceState) {
		   int len=0;
		   getmodulename = savedInstanceState.getString("module_index");
		   if(!getmodulename.equals(""))
		   {
			   sp_module.setSelection(moduleadap.getPosition(getmodulename));
		   }
		   getprimaryvc = savedInstanceState.getString("pc_index");
		   
		   sec_dialog = savedInstanceState.getInt("sec_index");
		   getsecondaryvc = savedInstanceState.getString("spoption_index");
		   if(sec_dialog==1)
		   {   showsec_alert();
			   if(!getsecondaryvc.equals(""))
			   {				
			    	  cf.setValuetoCheckbox(ch, getsecondaryvc);
			   }
		   }
          
		   insp_dialog = savedInstanceState.getInt("insp_index");
		   getinspectionpoint = savedInstanceState.getString("spinspoption_index");System.out.println("getinspectionpoint onrestore="+getinspectionpoint);
		   if(insp_dialog==1)
		   {			  
			   showalert();
		   }	   
		   		   
		   cont_dialog = savedInstanceState.getInt("cont_index");
		   getconttype= savedInstanceState.getString("contoption_index");
		   if(cont_dialog==1)
		   {			  
			   showcontdialog();
		   }
		   strcomments = savedInstanceState.getString("comments_index");
		   getsectext =  savedInstanceState.getString("get_sectext");
		
		   if(!getsectext.equals("") && !getsectext.equals("Selected : "))
		   {
			   ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);
			   ((TextView) findViewById(R.id.secconddisplay)).setText(getsectext);
		   }
		   
		   getinsptext =  savedInstanceState.getString("get_insptext");
		   if(!getinsptext.equals("") && !getinsptext.equals("Selected : "))
		   {
			   ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			   ((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(getinsptext);
		   }
		   else
		   {
			   System.out.println("onskoosos");
		   }
		   
		   
		   getconttext =  savedInstanceState.getString("get_conttext");
		   
		   if(!getconttext.equals("") && !getconttext.equals("Selected : "))
		   {
			   ((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
			   ((TextView) findViewById(R.id.conttypedisplay)).setText(getconttext);
		   }
		   
		   ((EditText) findViewById(R.id.edapprcost)).setText(savedInstanceState.getString("appr_cost"));
		   etcomments.setText(savedInstanceState.getString("comments_index"));
		   
		   super.onRestoreInstanceState(savedInstanceState);
	    }
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		startActivity(new Intent(AddVC.this,HomeScreen.class));
		finish();
	}
}
