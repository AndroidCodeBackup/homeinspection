/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : Submit.java
   * Creation History:
      Created By : Rakki s on 11/22/2012
   * Modification History:
      Last Modified By : Gowri on 12/15/2012
   ************************************************************  
*/

package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DataBaseHelperVC;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.io.IOException;
import java.util.ArrayList;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoadComments extends Activity {
	CommonFunction cf;
	DataBaseHelperVC dbh;
String insp_name="",mod_name="",submod_name="",vcname="";
String[] s={"-Select-","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1"};
int fromx=0,fromy=0;
int inti=0,animat=0;
String selectedtag="";
int lenth=0,max_length=500;
DatabaseFunction db;

Bundle b;
LinearLayout com_li_fill;
RelativeLayout com_li;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.load_comments);
		cf=new CommonFunction(this);
		db = new DatabaseFunction(this);
		com_li_fill=(LinearLayout) findViewById(R.id.com_com_li);
		com_li=(RelativeLayout) findViewById(R.id.contentwrp);
		b=getIntent().getExtras();
		if(b!=null)
		{
			mod_name=b.getString("mod_name");
			submod_name=b.getString("submod_name");
			vcname=b.getString("vcname");
			fromx=b.getInt("xfrom");System.out.println("from="+fromx);
			fromy=b.getInt("yfrom");System.out.println("xsd="+fromy);
			lenth=b.getInt("length");
			max_length=b.getInt("max_length");			
		}
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int wd = metrics.widthPixels;System.out.println("WD="+wd);
		int ht = metrics.heightPixels;			
		int totalwith = wd-70;
		int totalheight =  ht-50;
		//totalwith = totalwith/4;
		//int rem = wd-totalwith;
		//int width = wd/3;System.out.println("wddd="+width);
		
		
		/*try {
			dbh = new DataBaseHelperVC(LoadComments.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB1.rawQuery("select * from TBL_VCCOMMENTS where RANGETABLE='"+mod_name+"' and VCTEXT='"+vcname+"'",null);
			System.out.println("CCC="+cur.getCount());
			cur.moveToFirst();
			for(int k =0;k<cur.getCount();k++,cur.moveToNext())
			{
				System.out.println("VCTEXT="+db.decode(cur.getString(cur.getColumnIndex("VCTEXT"))));
				primaryarr[k]=db.decode(cur.getString(cur.getColumnIndex("VCTEXT")));
				 System.out.println("PPP="+primvalue);
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}*/
		dbh = new DataBaseHelperVC(LoadComments.this);

		try {
			dbh.createDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SQLiteDatabase newDB = dbh.openDataBase();
		
		db.userid();
		
		Cursor c = newDB.rawQuery("select * from TBL_VCCOMMENTS where RANGETABLE='"+mod_name+"' and VCTEXT='"+vcname+"'",null);
	System.out.println("select Distinct(VCCOMMENT) from TBL_VCCOMMENTS where RANGETABLE='"+mod_name+"' and VCTEXT='"+vcname+"'"+c.getCount());	

	s= new String[c.getCount()];
	if(c.getCount()>0)
	{int j=0;
	c.moveToFirst();
		do
		{
			s[j]=db.decode(c.getString(c.getColumnIndex("VCCOMMENT")));
			j++;
		}while(c.moveToNext());
		TextView tv;
		LinearLayout li;
		CheckedTextView ch;
		LinearLayout tbl= (LinearLayout) findViewById(R.id.com_lis);
		tbl.removeAllViews();
		for(int i=0;i<s.length;i++)
		{
			
			li=new LinearLayout(this);
			li.setGravity(Gravity.CENTER_VERTICAL);
			//li.setOnClickListener(new liClick(i,0));
			li.setOrientation(LinearLayout.HORIZONTAL);
			li.setPadding(20, 5, 10, 5);
			 tv= new TextView(this);
			 tv.setText((i+1)+". ");
			 tv.setTextColor(Color.BLACK); 
			 tv.setGravity(Gravity.CENTER_VERTICAL);
			 li.addView(tv,30,LayoutParams.WRAP_CONTENT);
			 
			 LinearLayout li_tv= new LinearLayout(this);
			 
			/* tv= new TextView(this);
			 tv.setText(s[i]);
			 tv.setWidth(0);
			 
			 tv.setTextColor(Color.BLACK);
			 li_tv.addView(tv,cf.wd-380,LayoutParams.WRAP_CONTENT);
			 li.addView(li_tv,cf.wd-380,LayoutParams.WRAP_CONTENT);*/
			 
			 ch= new CheckedTextView(this,null,R.attr.checked_text);
			 ch.setTag("checkbox_"+i);
			 ch.setText(s[i]);
			 ch.setTextColor(Color.BLACK);
			ch.setOnClickListener(new liClick(i,1));
			 ch.setGravity(Gravity.CENTER_VERTICAL);
			 li.addView(ch,totalwith,LayoutParams.WRAP_CONTENT);
			 tbl.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 if(i<s.length-1)
			 {
				 View v=new View(this);
				 v.setBackgroundColor(getResources().getColor(R.color.for_line));
				 tbl.addView(v,LayoutParams.FILL_PARENT,1);
			 }
			 RelativeLayout.LayoutParams li_p=new RelativeLayout.LayoutParams(cf.wd-300,totalheight);
			 ((LinearLayout) findViewById(R.id.com_com_li)).setLayoutParams(li_p);
			
		}
	}else
	{

		if(vcname.equals("--Select--"))
		{
			vcname="";
		}
		Cursor cur = db.hi_db.rawQuery("select * from " + db.AddVC+ " WHERE fld_inspectorid='"+ db.UserId + "' and fld_module='"+ mod_name + "' and " +
				"(fld_primary='"+ vcname+ "' or fld_submodule='"+ db.encode(submod_name) + "')",null);
		System.out.println("select * from " + db.AddVC+ " WHERE fld_inspectorid='"+ db.UserId + "' and fld_module='"+ mod_name + "' and " +
				"(fld_primary='"+ vcname+ "' or fld_submodule='"+ db.encode(submod_name) + "')"+cur.getCount());
		s= new String[cur.getCount()];
		if(cur.getCount()>0)
		{int j=0;
		cur.moveToFirst();
			do
			{
				s[j]=db.decode(cur.getString(cur.getColumnIndex("fld_comments")));
				j++;
			}while(cur.moveToNext());
			TextView tv;
			LinearLayout li;
			CheckedTextView ch;
			LinearLayout tbl= (LinearLayout) findViewById(R.id.com_lis);
			tbl.removeAllViews();
			for(int i=0;i<s.length;i++)
			{
				
				li=new LinearLayout(this);
				li.setGravity(Gravity.CENTER_VERTICAL);
				//li.setOnClickListener(new liClick(i,0));
				li.setOrientation(LinearLayout.HORIZONTAL);
				li.setPadding(20, 5, 10, 5);
				 tv= new TextView(this);
				 tv.setText((i+1)+". ");
				 tv.setTextColor(Color.BLACK); 
				 tv.setGravity(Gravity.CENTER_VERTICAL);
				 li.addView(tv,30,LayoutParams.WRAP_CONTENT);
				 
				 LinearLayout li_tv= new LinearLayout(this);
				 
				 
				 ch= new CheckedTextView(this,null,R.attr.checked_text);
				 ch.setTag("checkbox_"+i);
				 ch.setText(s[i]);
				 ch.setTextColor(Color.BLACK);
				ch.setOnClickListener(new liClick(i,1));
				 ch.setGravity(Gravity.CENTER_VERTICAL);
				 li.addView(ch,totalwith,LayoutParams.WRAP_CONTENT);
				 tbl.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 if(i<s.length-1)
				 {
					 View v=new View(this);
					 v.setBackgroundColor(getResources().getColor(R.color.for_line));
					 tbl.addView(v,LayoutParams.FILL_PARENT,1);
				 }
				 RelativeLayout.LayoutParams li_p=new RelativeLayout.LayoutParams(cf.wd-300,totalheight);
				 ((LinearLayout) findViewById(R.id.com_com_li)).setLayoutParams(li_p);
				
			}
		}
		else
		{
			cf.DisplayToast("Comments not available.");
			finish();	
		}		
	}
		
	}
	class liClick implements android.view.View.OnClickListener
	{
		int id,type;
		liClick(int id,int type)
		{
			this.id=id;
			this.type=type;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			System.out.println("there iss isses "+v.getTag()+"the type "+type);
			if(type==0)
			{
				if(((CheckedTextView) v.findViewWithTag("checkbox_"+id)).isChecked())
				{
					((CheckedTextView) v.findViewWithTag("checkbox_"+id)).setChecked(false);
					selectedtag.replace("checkbox_"+id+"~", "");
					
				}
				else
				{
					((CheckedTextView) v.findViewWithTag("checkbox_"+id)).setChecked(true);
					selectedtag="checkbox_"+id+"~";
				}
				
				
			}
			else
			{
			
			
				if(!((CheckedTextView) v).isChecked())
				{
					if(lenth>=max_length)
					{
						//cf.ShowToast("You have exceeded the maximum limit.", 0);
						
					}
					else
					{
						selectedtag+="checkbox_"+id+"~";
						((CheckedTextView) v).setChecked(true);
						lenth=lenth+s[id].length();
					}
				}
				else
				{
					
					selectedtag=selectedtag.replace("checkbox_"+id+"~", "");
					lenth=lenth-s[id].length();
					((CheckedTextView) v).setChecked(false);
				}
			
			//}
			
			System.out.println("the elected are "+selectedtag);
				Intent in = new Intent();
				b.putString("Comments",s[id]);
				in.putExtras(b);
				setResult(RESULT_OK, in);
			
		//	Scaleoutanimation(fromx,fromy);
				
		
		}
		}
	}
	
	public void clicker(View v)
	{
		if(v.getId()==R.id.load_com_close)
		{
			if(animat==0)
			{
				Intent in = new Intent();
				b.putString("Comments","");
				in.putExtras(b);
				setResult(RESULT_CANCELED, in);
				Scaleoutanimation(fromx,fromy);
				//cf.ShowToast("you have not  selcted any comments", 0);
			}
		}
		else if(v.getId()==R.id.ok)
		{
			if(selectedtag.length()>1)
			{
				selectedtag=selectedtag.substring(0,selectedtag.length()-1);
				String[] sel=selectedtag.split("~");
				String comts="";
				for(int i=0;i<sel.length;i++)
				{
					
					sel[i]=sel[i].replace("checkbox_", "");
					if(comts.equals(""))
					{
						comts+=s[Integer.parseInt(sel[i])];
					}
					else
					{
						comts+=" "+s[Integer.parseInt(sel[i])];
					}
				}
				Intent in = new Intent();System.out.println("CCCComment="+comts);
				b.putString("Comments",comts);
				in.putExtras(b);
				setResult(RESULT_OK, in);
			
				Scaleoutanimation(fromx,fromy);
			}
			else
			{
				cf.DisplayToast("Please select atleast one comment.");
			}
		}
		else
		{
			
		}
	}
	public void Scaleinanimation(float xfrom, float yfrom) {
		AnimationSet an= new AnimationSet(false);
        ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,com_li_fill.getWidth()/2,com_li_fill.getHeight()/2);
       scale.setDuration(500);
       an.addAnimation(scale);
        TranslateAnimation trans=new TranslateAnimation(TranslateAnimation.ABSOLUTE,xfrom-(com_li_fill.getWidth()/2), TranslateAnimation.ABSOLUTE,0,
    		   TranslateAnimation.ABSOLUTE,yfrom-(com_li_fill.getHeight()/2),TranslateAnimation.ABSOLUTE,0); 
       trans.setDuration(500);
       trans.setStartOffset(10);
       an.setFillAfter(true);
       an.addAnimation(trans);
       an.setAnimationListener(new animat_lis(1));
       com_li_fill.startAnimation(an);
    }
	public void Scaleoutanimation(float xfrom, float yfrom) {
	      
		
		
	       AnimationSet an= new AnimationSet(false);
	       ScaleAnimation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f,com_li_fill.getWidth()/2,com_li_fill.getHeight()/2);
	       scale.setDuration(500);
	       an.addAnimation(scale);
	       TranslateAnimation trans=new TranslateAnimation(TranslateAnimation.ABSOLUTE,0, TranslateAnimation.ABSOLUTE,xfrom-(com_li_fill.getWidth()/2),
	    		   TranslateAnimation.ABSOLUTE,0,TranslateAnimation.ABSOLUTE,yfrom-(com_li_fill.getHeight()/2)); 
	       trans.setDuration(500);
	       trans.setStartOffset(10);
	       an.setFillAfter(true);
	       an.addAnimation(trans);
	       com_li_fill.startAnimation(an);
	       an.setAnimationListener(new animat_lis(0));
	       
	    }
		
	
	public void onWindowFocusChanged(boolean hasFocus)
	{
		if(inti==0)
		{
			Scaleinanimation(fromx,fromy);
		 inti=1;
		}
	}
	class animat_lis implements AnimationListener
	{int type;
		animat_lis(int type)
		{
			this.type=type;
		}
		
		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			animat=1;
		}
		
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			animat=0;
			if(type==0)
			{
				finish();
			}
		}

		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			/*if(animat==0)
			{
				Intent in = new Intent();
				in.putExtra("Comments","");
				setResult(RESULT_CANCELED, in);
				Scaleoutanimation(fromx,fromy);
				//LinearLayout scr=(LinearLayout) findViewById(R.id.cont);
		    	//View v1 = scr.getRootView();
		        
				//cf.ShowToast("you have not  selcted any comments", 0);
			}
			*/
			return true;

		}
		return super.onKeyDown(keyCode, event);
	}
	
}