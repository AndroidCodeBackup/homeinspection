package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.GraphicsView;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;


public class InVoice extends Activity {
	DatabaseFunction db;
	CommonFunction cf;
	Webservice_config wb;
	String inspectorid="",srid="",str="",presentatinsptxt="",title="",error_msg="",strintitials,insparrivaltxt="",inspatticaccessopeningstxt="",filepath="",selectedImagePath="",picname="",
			inspcallpriorarrivingtxt="",inspidbadgetxt="",inspreferaltxt="",inspcourteoustxt="",inspquestionstxt="",inspshoescleantxt="",inspreviewtxt="",extStorageDirectory="",otherpresent="";
	ScrollView parentscr;
	String paymentvalue="", cardtypeval="";
	View v;
	DatePickerDialog datePicker;	
	int[] chkcardtypeid ={R.id.cardtype1,R.id.cardtype2,R.id.cardtype3,R.id.cardtype4};
	CheckBox[] chkcardtype = new CheckBox[chkcardtypeid.length];
	
	int[] chkpayid ={R.id.payment1,R.id.payment2,R.id.payment3};
	CheckBox[] chkpayment = new CheckBox[chkpayid.length];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 db = new DatabaseFunction(this);
		 cf = new CommonFunction(this);
		 wb = new Webservice_config(this);
		
		 
		 Bundle b = this.getIntent().getExtras();
		 if(b!=null)
		 {
		   inspectorid = b.getString("inspectorid");
		   srid = b.getString("srid");
		 }
		 setContentView(R.layout.invoice);		
		  
		 for(int i=0;i<chkcardtypeid.length;i++)
		 {
			 chkcardtype[i] = (CheckBox)findViewById(chkcardtypeid[i]);			 
		 }
		 
		 for(int i=0;i<chkpayid.length;i++)
		 {
			 chkpayment[i] = (CheckBox)findViewById(chkpayid[i]);
			 
		 }
		 
		 
		 Button btnback = (Button)findViewById(R.id.btn_back);
		 btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
				
			}
		});
		 db.userid();
		  
		 parentscr = (ScrollView)findViewById(R.id.scr);
			
		  
		 Button btnsubmit = (Button)findViewById(R.id.submit);
		 btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				submitdata();
						}
		});
		 
		 
		 Button btnclear = (Button)findViewById(R.id.clear);
		 btnclear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clearinvoicedata();	
			}
		});
				 
		 
		 ((Button)findViewById(R.id.invoice_getdate)).setOnClickListener(new OnClickListener() {
	 			
	 			@Override
	 			public void onClick(View v) {
	 				// TODO Auto-generated method stub
	 				showDialog(0);
	 			}
	 		});
		 
		 showsavedvalues();
		 
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			// set date picker as current date
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			datePicker = new DatePickerDialog (this, datePickerListener, year, month, day); 
			   
			return datePicker;

		}
		return null;
	}
	
	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			int year = selectedYear;
			int month = selectedMonth;
			int day = selectedDay;
			
			((EditText)findViewById(R.id.invoicedate)).setText(new StringBuilder().append(month + 1).append("/").append(day).append("/").append(year));
			
			final Calendar c = Calendar.getInstance();
			datePicker.updateDate(c.get(Calendar.YEAR),c.get(Calendar.MONTH), c.get(Calendar.DATE));
		}
	};
	
	private void clearinvoicedata() 
	{
		 ((EditText)findViewById(R.id.occupied)).setText("");
		 ((EditText)findViewById(R.id.utilitieson)).setText("");
		 ((EditText)findViewById(R.id.addtionalfeeno)).setText("");				 
		 ((EditText)findViewById(R.id.annualinspection)).setText("");
		 ((EditText)findViewById(R.id.shippingandhandling)).setText("");
		 ((EditText)findViewById(R.id.eocoverage)).setText("");				 
		 ((EditText)findViewById(R.id.homeguide)).setText("");
		 ((EditText)findViewById(R.id.specialnotes)).setText("");		
		 ((EditText)findViewById(R.id.invoiceno)).setText("");
		 ((EditText)findViewById(R.id.invoicedate)).setText("");
		 ((EditText)findViewById(R.id.stdhomeinspection)).setText("");				 
		 ((EditText)findViewById(R.id.salestax)).setText("");
		 ((EditText)findViewById(R.id.totalfees)).setText("");
		 ((EditText)findViewById(R.id.amountreceived)).setText("");
		 ((EditText)findViewById(R.id.balancedue)).setText("");
		
		 for(int i=0;i<chkpayid.length;i++)
		 {
			 chkpayment[i].setChecked(false);
		 }
		 
		 
		 for(int i=0;i<chkcardtypeid.length;i++)
		 {
			 chkcardtype[i].setChecked(false);
		 }
	}

	protected void submitdata() {
		// TODO Auto-generated method stub
		/*String et_propaddr = ((EditText)findViewById(R.id.inspectionpropaddr)).getText().toString();		
		String et_inspcity = ((EditText)findViewById(R.id.inspectioncity)).getText().toString();
		String et_inspstate = ((EditText)findViewById(R.id.inspectionstate)).getText().toString();
		String et_inspzip = ((EditText)findViewById(R.id.inspectionzip)).getText().toString();*/
		String et_invoiceno = ((EditText)findViewById(R.id.invoiceno)).getText().toString();
		String et_invoicedate = ((EditText)findViewById(R.id.invoicedate)).getText().toString();
		String et_occupied = ((EditText)findViewById(R.id.occupied)).getText().toString();
		String et_utilitieson = ((EditText)findViewById(R.id.utilitieson)).getText().toString();
		String et_addifees = ((EditText)findViewById(R.id.addtionalfeeno)).getText().toString();
		String et_annuinsp = ((EditText)findViewById(R.id.annualinspection)).getText().toString();		
		String et_shippingandhandling = ((EditText)findViewById(R.id.shippingandhandling)).getText().toString();
		String et_eocoverage = ((EditText)findViewById(R.id.eocoverage)).getText().toString();
		String et_homeguide = ((EditText)findViewById(R.id.homeguide)).getText().toString();
		String et_specialnotes = ((EditText)findViewById(R.id.specialnotes)).getText().toString();
		
		
		String et_stdhomeinspection = ((EditText)findViewById(R.id.stdhomeinspection)).getText().toString();
		String et_crawlfees = "0";//((EditText)findViewById(R.id.crawlspacefees)).getText().toString();
		String et_subtotal = "0";//((EditText)findViewById(R.id.subtotal)).getText().toString();
		String et_salestax = ((EditText)findViewById(R.id.salestax)).getText().toString();
		String et_totalfees = ((EditText)findViewById(R.id.totalfees)).getText().toString();
		String et_amtreceived = ((EditText)findViewById(R.id.amountreceived)).getText().toString();		
		String et_balancedue = ((EditText)findViewById(R.id.balancedue)).getText().toString();
		db.CreateTable(41);
		
		paymentvalue= cf.getselected_chk(chkpayment);
		cardtypeval= cf.getselected_chk(chkcardtype);
		try
		{
			 Cursor cur = db.hi_db.rawQuery("select * from " + db.Invoice + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
			 if(cur.getCount()>0)
			 {
				 db.hi_db.execSQL("UPDATE "+ db.Invoice +"  SET fld_invoiceno='"+ db.encode(et_invoiceno) + "',fld_invoicedate='"+ db.encode(et_invoicedate) + "',fld_occupied='"+ db.encode(et_occupied)  + "',fld_utilitieson='"+ db.encode(et_utilitieson) + "'," +
	            			"fld_additionalfees='"+ db.encode(et_addifees) + "',fld_annualinspection='"+db.encode(et_annuinsp)+ "',fld_shippingandhandling='"+db.encode(et_shippingandhandling) + "',fld_eocoverage='"+db.encode(et_eocoverage)+"'," +
	            			"fld_homeguide='"+db.encode(et_homeguide)+"',fld_payment='"+db.encode(paymentvalue)+"',fld_cardtype='"+db.encode(cardtypeval)+"'," +
	            			"fld_specialnotes='"+db.encode(et_specialnotes)+"',fld_standardhomeinsp='"+db.encode(et_stdhomeinspection)+"',fld_crawlfees='"+db.encode(et_crawlfees)+"'," +
	            			"fld_subtotal='"+db.encode(et_subtotal)+"',fld_salestax='"+db.encode(et_salestax)+"',fld_totalfees='"+db.encode(et_totalfees)+"'," +
	            			"fld_amountreceived='"+db.encode(et_amtreceived)+"',fld_balancedue='"+db.encode(et_balancedue)+"' where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'");
			 }
			 else
			 {
				 db.hi_db.execSQL("INSERT INTO "
	  						+ db.Invoice
	  						+ " (fld_inspectorid,fld_srid,fld_invoiceno,fld_invoicedate,fld_occupied,fld_utilitieson," +
	  						"fld_additionalfees,fld_annualinspection,fld_shippingandhandling,fld_eocoverage,fld_homeguide,fld_payment,fld_cardtype,fld_specialnotes," +
	  						"fld_standardhomeinsp,fld_crawlfees,fld_subtotal,fld_salestax,fld_totalfees,fld_amountreceived,fld_balancedue)"
	  						+ " VALUES ('" + inspectorid + "','"+srid+ "',"+
	  						"'"+ db.encode(et_invoiceno) + "','"+ db.encode(et_invoicedate)  + "','"+ db.encode(et_occupied) + "','"+ db.encode(et_utilitieson)  + "',"+
	  						"'"+ db.encode(et_addifees) + "','"+db.encode(et_annuinsp)+ "','"+db.encode(et_shippingandhandling) + "','"+db.encode(et_eocoverage)+"','"+db.encode(et_homeguide)+"'," +
	  						"'"+db.encode(paymentvalue)+"','"+db.encode(cardtypeval)+"','"+db.encode(et_specialnotes)+"'," +
	  						"'"+db.encode(et_stdhomeinspection)+"','"+db.encode(et_crawlfees)+"','"+db.encode(et_subtotal)+"','"+db.encode(et_salestax)+"','"+db.encode(et_totalfees)+"','"+db.encode(et_amtreceived)+"','"+db.encode(et_balancedue)+"')");
	  		  
			 }
			 cf.DisplayToast("Invoice has been submitted successfully");
		}
		catch (Exception e) {
			
			// TODO: handle exception
			System.out.println("tets ss="+e.getMessage());
		}
	}
	
	private void showsavedvalues() {
		// TODO Auto-generated method stub
		System.out.println("inside");
		try
		{
			 Cursor cur = db.hi_db.rawQuery("select * from " + db.Invoice + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 ((EditText)findViewById(R.id.invoiceno)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_invoiceno"))));
				 ((EditText)findViewById(R.id.invoicedate)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_invoicedate"))));
				 ((EditText)findViewById(R.id.occupied)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_occupied"))));
				 ((EditText)findViewById(R.id.utilitieson)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_utilitieson"))));
				 ((EditText)findViewById(R.id.addtionalfeeno)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_additionalfees"))));				 
				 ((EditText)findViewById(R.id.annualinspection)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_annualinspection"))));
				 ((EditText)findViewById(R.id.shippingandhandling)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_shippingandhandling"))));
				 ((EditText)findViewById(R.id.eocoverage)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_eocoverage"))));				 
				 ((EditText)findViewById(R.id.homeguide)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_homeguide"))));
				 ((EditText)findViewById(R.id.specialnotes)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_specialnotes"))));		
				 
				 ((EditText)findViewById(R.id.stdhomeinspection)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_standardhomeinsp"))));				 
				// ((EditText)findViewById(R.id.crawlspacefees)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_crawlfees"))));
				// ((EditText)findViewById(R.id.subtotal)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_subtotal"))));				 
				 ((EditText)findViewById(R.id.salestax)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_salestax"))));
				 ((EditText)findViewById(R.id.totalfees)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_totalfees"))));
				 ((EditText)findViewById(R.id.amountreceived)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_amountreceived"))));
				 ((EditText)findViewById(R.id.balancedue)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_balancedue"))));
				 
				 System.out.println("payment="+db.decode(cur.getString(cur.getColumnIndex("fld_payment"))));
				 System.out.println("cardtype="+db.decode(cur.getString(cur.getColumnIndex("fld_cardtype"))));
					if(!cur.getString(cur.getColumnIndex("fld_payment")).equals(""))
			 		{
			 			cf.setvaluechk1(db.decode(cur.getString(cur.getColumnIndex("fld_payment"))),chkpayment,((EditText)findViewById(R.id.dummy)));
			 		}
					
					if(!cur.getString(cur.getColumnIndex("fld_cardtype")).equals(""))
			 		{
			 			cf.setvaluechk1(db.decode(cur.getString(cur.getColumnIndex("fld_cardtype"))),chkcardtype,((EditText)findViewById(R.id.dummy)));
			 		}
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	

	
	protected void back() {
		// TODO Auto-generated method stub
		Intent i = new Intent(InVoice.this,StartInspection.class);
		i.putExtra("inspectorid", inspectorid);
		startActivity(i);
		finish();
	}

	
	protected void onSaveInstanceState(Bundle outState) 
	{
		outState.putString("PresentatInspection", presentatinsptxt);
		outState.putString("OtherPresentatInspection", otherpresent);
		
		
		
		super.onSaveInstanceState(outState);
	}
	
	 protected void onRestoreInstanceState(Bundle savedInstanceState) {
	
		 
		 super.onRestoreInstanceState(savedInstanceState);
	 }
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			back();
			
		}

		return super.onKeyDown(keyCode, event);
	}
	

}
