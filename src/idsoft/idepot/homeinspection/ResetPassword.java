package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;



public class ResetPassword extends Activity{
	CommonFunction cf;
	Webservice_config wb;
	DatabaseFunction db;
	EditText et_oldpassword,et_newpassword,et_confirmpassword;
	String str_oldpassword="",str_newpassword="",str_confirmpassword="",error_msg="",title="Reset Password";;
	boolean upperFound_newpass=false,upperFound_conpass=false;
	Progress_staticvalues p_sv;
	private int handler_msg=2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();
		
	}

	private void initialize() {
		// TODO Auto-generated method stub
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db = new DatabaseFunction(this);
		setContentView(R.layout.resetpassword);
		
		et_oldpassword = (EditText)findViewById(R.id.resetpassword_oldpassword);
		et_newpassword = (EditText)findViewById(R.id.resetpassword_newpassword);
		et_confirmpassword = (EditText)findViewById(R.id.resetpassword_confirmpassword);
		
		et_confirmpassword.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        boolean handled = false;
		        if (actionId == EditorInfo.IME_ACTION_GO) {
		            submitpassword();
		            handled = true;
		        }
		        return handled;
		    }
		});
		
		((Button)findViewById(R.id.resetpassword_back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(ResetPassword.this,HomeScreen.class));
				//overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
			}
		});
		
        ((Button)findViewById(R.id.resetpassword_clear)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				et_oldpassword.setText("");
				et_newpassword.setText("");
				et_confirmpassword.setText("");
				et_oldpassword.requestFocus();
			}
		});
        
        ((Button)findViewById(R.id.resetpassword_submit)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				submitpassword();
			}
        });
	}
	
	protected void submitpassword() {
		// TODO Auto-generated method stub
		str_oldpassword = et_oldpassword.getText().toString();
		str_newpassword = et_newpassword.getText().toString();
		str_confirmpassword = et_confirmpassword.getText().toString();
		
		if(str_oldpassword.trim().equals(""))
		{
			cf.DisplayToast("Please enter current password");et_oldpassword.requestFocus();
		}
		else
		{
			if(str_newpassword.trim().equals(""))
			{
				cf.DisplayToast("Please enter new password");et_newpassword.requestFocus();
			}
			else
			{
				if(str_newpassword.length()<10)
				{
					cf.DisplayToast("New password must be 10 characters long");et_newpassword.requestFocus();
				}
				else
				{
					//Checks for edittext contains a Upper Case Character
					for (char c : str_newpassword.toCharArray()) {
					    if (Character.isUpperCase(c)) {
					        upperFound_newpass = true;
					       break;
					    }
					}
					if(!upperFound_newpass)
					{
						cf.DisplayToast("New password must contain at least one uppercase letter (A-Z)");
					}
					else
					{
						if(str_confirmpassword.trim().equals(""))
						{
							cf.DisplayToast("Please enter confirm password");et_confirmpassword.requestFocus();
						}
						else
						{
							if(str_confirmpassword.length()<10)
							{
								cf.DisplayToast("Confirm password must be 10 characters long");et_confirmpassword.requestFocus();
							}
							else
							{
								//Checks for edittext contains a Upper Case Character
								for (char c : str_confirmpassword.toCharArray()) {
								    if (Character.isUpperCase(c)) {
								        upperFound_conpass = true;
								       break;
								    }
								}
								if(!upperFound_conpass)
								{
									cf.DisplayToast("Confirm password must contain at least one uppercase letter (A-Z)");
								}
								else
								{
									//checking password and confirm password match
									if(!str_newpassword.equals(str_confirmpassword))
									{
										cf.DisplayToast("New Password and Confirm Password do not match");
									}
									else
									{
										  if(cf.isInternetOn())
										  {
											  new Start_xporting().execute("");
										  }
										  else
										  {
											  cf.DisplayToast("Please enable your internet connection.");
										  }
									}
								}
							}
						}
					}
					
				}
				
			}
		}
	
	}

	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="Reset password";
			    p_sv.progress_Msg="Please wait..Your request has been submitting.";
				
				startActivityForResult(new Intent(ResetPassword.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
					
			 		Send_record_to_server();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		                return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	System.out.println("hand"+handler_msg);
		    	if(handler_msg==0)
		    	{
		    		success_alert();		    		
		    		
		    	}
		    	else if(handler_msg==1)
		    	{
		    		
		    		failure_alert();
		    		
		    	}
		    	
		    }
	 }
	
	private void Send_record_to_server() throws IOException, XmlPullParserException {
		
		db.userid();
		 
	    SoapObject request = new SoapObject(wb.NAMESPACE,"CHANGEPASSWORD");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
 		envelope.dotNet = true;
 		
 		request.addProperty("Username",db.UserName);
 		request.addProperty("Currentpwd",str_oldpassword);
 		request.addProperty("Newpwd",str_confirmpassword);
 		
 		envelope.setOutputSoapObject(request);
 		System.out.println("request="+request);
 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
 		SoapObject response = null;
        try
        {
              androidHttpTransport.call(wb.NAMESPACE+"CHANGEPASSWORD", envelope);
              response = (SoapObject)envelope.getResponse();
              System.out.println("CHANGEPASSWORDresult "+response);
              
              //statuscode ==0 -> sucess && statuscode !=0 -> failure
              if(response.getProperty("StatusCode").toString().equals("0"))
      		  {
              	handler_msg=0;
      		  }
      		  else
      		  {
      			 error_msg = response.getProperty("StatusMessage").toString();
      			 handler_msg=1;
      			
      		  }
        }
        catch(Exception e)
        {
        	handler_msg=0;
             e.printStackTrace();
        }
        
       
   }
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(ResetPassword.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public void success_alert() {
		// TODO Auto-generated method stub
       
		AlertDialog al = new AlertDialog.Builder(ResetPassword.this).setMessage("You've successfully reset your password. An email has been sent to your registered email.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				db.userid();
				handler_msg = 2 ;
				Intent intent=new Intent(ResetPassword.this,LoginActivity.class);
				intent.putExtra("status", "resetpassword");
				intent.putExtra("username", db.UserName);
				intent.putExtra("password", str_confirmpassword);
				startActivity(intent);
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
				
				try
      			{
      				db.hi_db.execSQL("UPDATE "+ db.Registration +"  SET fld_logout='2' where fld_userid='"+db.UserId+"'");
      			    System.out.println("UPDATE "+ db.Registration +"  SET fld_logout='2' where fld_userid='"+db.UserId+"'");
      				
      			}
      			catch (Exception e) {
      				// TODO: handle exception
      			}
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {	 
	   
        outState.putInt("handler", handler_msg);
        outState.putString("error", error_msg);
        outState.putString("username",db.UserName);
        outState.putString("password",str_confirmpassword);
        super.onSaveInstanceState(outState);
    }
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	
    	
    	handler_msg = savedInstanceState.getInt("handler");
    	error_msg = savedInstanceState.getString("error");
    	db.UserName = savedInstanceState.getString("username");
    	str_confirmpassword = savedInstanceState.getString("password");
    	if(handler_msg==0)
    	{
    		success_alert();
    	}
    	else if(handler_msg==1)
    	{
    		failure_alert();
    	}
         
        super.onRestoreInstanceState(savedInstanceState);
    }

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(ResetPassword.this,HomeScreen.class));
			//overridePendingTransition(R.anim.left_in, R.anim.right_out);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
