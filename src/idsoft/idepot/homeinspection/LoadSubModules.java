package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class LoadSubModules extends Activity{
	String result_val="",gettempname="",getph="",strBase64,getmodulename="",str_module="",str_cbval="",word="",error_msg="",title="",tempname="",submitrdtext="",enterword="";
	TextView tv_seltemplate,tv_modulename,tv_header;
	Button btnback,btnhome,btnsubmit,btntakephoto;
	EditText wrdedit, enterwordedit;
	Spinner spinloadtemplate;
	ArrayAdapter adapter;
	double total;
	int pos,newid=0,selectionid=0,spintouch=0,finalsubmit=0;
	ListView modulelist;
	String[] arrstrmodule,cbval,value,arrdescription,vcarr;
	View v;
	double Totaltmp = 0.0;
	CommonFunction cf;
	Webservice_config wb;
	Boolean chkbool=false;
	int submitalert=0,sub_total=0;
	DatabaseFunction db;
	private int handler_msg=2;
	Dialog dialog=null;
	Bitmap bitmap;
	RadioGroup submitrdgp;
	Progress_staticvalues p_sv;
	byte[] byteArray;
	Cursor selcur,seleditvc;
	String arrtempname[];
	EfficientAdapter effadap;
	String[] crawlspace={"Crawlspace Area - Characteristics","Crawlspace - Access","Crawlspace - Main Beam","Crawlspace - Floors","Crawlspace - Exposed Foundation Wall"},
			basement={"Basement - Size","Basement - Access","Basement - Stairs","Basement - Floors","Basement - Exposed Columns and Beams","Basement - Walls/Ceilings",
			"Basement - Exposed Floor Systems","Basement - Drain Tile To","Basement - Heat/Cool(HVAC)","Basement - Finishes"},electricmeter={"Main Panel Meter Located Inside",
			"Main Panel Meter Located Outside","Main Panel Meter"},extsiding={"Masonry/Plaster","Siding/Paneling","Wall Shingles","Accessibility"},
			skylights={"# of Skylights","Skylight Type"};
	private static final Random rgenerator = new Random();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getph = b1.getString("selectedid");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");		
	
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(4);
		db.CreateTable(7);
		db.CreateTable(28);
		db.userid();
				
		setContentView(R.layout.activity_createsubmodule);
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}
		
	/*	tv_seltemplate = (TextView)findViewById(R.id.seltemplate);
		tv_seltemplate.setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));*/
		tv_modulename = (TextView)findViewById(R.id.modulename);
		tv_modulename.setText("Module Name : "+getmodulename);
	//	tv_header = (TextView)findViewById(R.id.header);
		
		
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC", null);
            if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		((TextView)findViewById(R.id.note)).setText("Note: All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		
		//tv_header.setText(" Start Inspection ");
		btnsubmit = (Button)findViewById(R.id.btn_submit);
		btnsubmit.setText("Submit Section");
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				submitalert=1;
				submitalert();
			}
		});
		
		
		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(LoadSubModules.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							
							Bundle b4 = new Bundle();
							b4.putString("photooption","takephoto");			
							intent.putExtras(b4);
							
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getph);			
							intent.putExtras(b5);
							
							startActivity(intent);
							finish();
		             
						
			}
		});
		
		btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoadSubModules.this,HomeScreen.class));
				finish();
			}
		});
		
		modulelist = (ListView)findViewById(R.id.modulelist);
		tv_modulename.setVisibility(v.VISIBLE);
		
		cf.GetCurrentModuleName(getmodulename);
		default_insert();
		
		
		
		Display();
		//DefaultValue_Insert(gettempname);
		
		try
		{
			Cursor c1 = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' " +
					"and fld_module='"+db.encode(getmodulename)+"'", null);
            if(c1.getCount()>0)
            {
            	c1.moveToFirst();
            	String seltemplate = db.decode(c1.getString(c1.getColumnIndex("fld_templatename")));
            	int spinnerPosition = adapter.getPosition(seltemplate);
        		spinloadtemplate.setSelection(spinnerPosition);
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}  
		
	}
	private void default_insert() {
		// TODO Auto-generated method stub
		for(int i=0;i<cf.arrsub.length;i++)
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	        if(cur.getCount()==0)
	        {
	        	try 
	        	{	        		
	        		db.hi_db.execSQL("INSERT INTO "+db.SaveSubModule+" (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_NA,fld_description) VALUES" +
							"('"+db.UserId+"','"+getph+"','"+db.encode(getmodulename)+"','"+db.encode(cf.arrsub[i])+"','"+db.encode(gettempname)+"','1','')");
	            }
	        	catch (Exception e) {
					// TODO: handle exception
	        		System.out.println("e="+e.getMessage());
				}
	        }
		}		
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(LoadSubModules.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						DefaultValue_Insert(gettempname);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	private void submitalert()
	{
		
		AlertDialog.Builder popupDialog = new AlertDialog.Builder(LoadSubModules.this);
    	popupDialog.setTitle("Confirmation");
    	popupDialog.setMessage("An Internet connection is required to submit this section. Would you like to proceed. You can also submit all sections as once from the inspection main dashboard.");
    	popupDialog.setIcon(R.drawable.ic_launcher);
    	popupDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() 
    	{
		    public void onClick(DialogInterface dialog1, int which) 
		    {
		    	dialog1.cancel();
		    	submitalert=0;
		    	finalsubmit=1;
				finalsubmit();
		      	 
		    }
    	});
		
    	popupDialog.setNegativeButton("No",new DialogInterface.OnClickListener() 
    	{
		     public void onClick(DialogInterface dialog1, int which) 
		     {
		       // Write your code here to execute after dialog
		                dialog1.cancel();submitalert=0;
		     }
    	});
		 
    	popupDialog.show();
		
		
	}
	protected void finalsubmit() {
		// TODO Auto-generated method stub
		 dialog = new Dialog(LoadSubModules.this,android.R.style.Theme_Translucent_NoTitleBar);
		//dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.setContentView(R.layout.input_submit_inspection);		
				
		 final String myString[] = getResources().getStringArray(R.array.myArray);
		 word = myString[rgenerator.nextInt(myString.length)];
		 wrdedit = ((EditText) dialog.findViewById(R.id.wrdedt));
		 enterwordedit = ((EditText) dialog.findViewById(R.id.wrdedt1));
		 
		 wrdedit.setText(word);
		 if (word.contains(" ")) { word = word.replace(" ", ""); }
			
		 submitrdgp = (RadioGroup)dialog.findViewById(R.id.submitradio);
		 submitrdgp.setOnCheckedChangeListener(new submitoncheckedlistener());
		 
		db.userid();
		  ((TextView) dialog.findViewById(R.id.tv1)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"			
							 + db.Userfirstname +" "+db.Userlastname
							+ "</font></b><font color=black> have completed this service in accordance with the required standards and processes and confirm that I am properly credentialed and qualified to conduct and submit this data collection service. I have made every attempt to collect the data required, have spoken with the necessary parties as needed and conducted the necessary research to ensure that the scope of service is achieved."
							+ "</font>"));
		 
			((TextView) dialog.findViewById(R.id.tv2)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ db.Userfirstname +" "+db.Userlastname
							+ "</font></b><font color=black> also understand that failure to properly conduct this service and submit the verifiable data per scope, is possible grounds for termination with your client and/or employer."
							+ "</font>"));
			enterword = enterwordedit.getText().toString();
			
			((Button) dialog.findViewById(R.id.submit)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (((RadioButton) dialog.findViewById(R.id.accept)).isChecked() == true) {
						
						if (((EditText) dialog.findViewById(R.id.wrdedt1)).getText().toString().equals("")) 
						{
							cf.DisplayToast("Please enter Word Verification.");
							((EditText) dialog.findViewById(R.id.wrdedt1)).requestFocus();
						} 
						else
						{
							if (((EditText) dialog.findViewById(R.id.wrdedt1)).getText().toString().equals(word.trim().toString())) 
							{
								
								dialog.cancel();
								if(cf.isInternetOn())
								  {
									   new Start_xporting().execute("");
								  }
								  else
								  {
									  cf.DisplayToast("Please enable your internet connection.");
									  
								  } 
							} 
							else
							{
								cf.DisplayToast("Please enter valid Word Verification(case sensitive).");
								((EditText) dialog.findViewById(R.id.wrdedt1)).setText("");
								((EditText) dialog.findViewById(R.id.wrdedt1)).requestFocus();
							}
						}
					} 
					else 
					{
						cf.DisplayToast("Please select Accept Radio Button.");
					}

				}
			});			
			
			((Button) dialog.findViewById(R.id.cancel)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();finalsubmit=0;submitrdtext="";
				}
			});			
			
			((Button) dialog.findViewById(R.id.S_refresh)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					word = myString[rgenerator.nextInt(myString.length)];			
					 ((EditText) dialog.findViewById(R.id.wrdedt)).setText(word);
					if (word.contains(" ")) {
						word = word.replace(" ", "");
					}	
					
				}
			});			
		dialog.show();		
	}
	class Start_xporting extends AsyncTask <String, String, String> {		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();	
		        p_sv.progress_live=true;
		        p_sv.progress_title="Inspection Submission";
			    p_sv.progress_Msg="Please wait..Your Inspection has been submitting.";      
				startActivityForResult(new Intent(LoadSubModules.this,Progress_dialogbox.class), Static_variables.progress_code);		       
		    }		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 			//Submit_Policyholderinformation();
			 			//Save_Invoice();
			 			//Save_Homewarranty();
			 		     Submit_Template();
			 		     Save_Editvc();
			 		     Save_Setvc();
			 		     Submit_Photos();
			 		    //Submit_Cse();	
			 		   ///Submit_PIA();
			 		     //Send_CoverPhoto();
			 		    
			 		   
			 		   
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		finalsubmit=0;submitrdtext="";
		    		success_alert();
            	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		finalsubmit=1;
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	private void Submit_Cse() throws IOException, XmlPullParserException 
	{
		db.CreateTable(36);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.CSE + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getph+"'", null);
		System.out.println("select * from " + db.CSE + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getph+"'"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"CustomerEvaluationSurvey");
					
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("cse_Id",0);
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
	      		    request.addProperty("fld_inspectorname",db.decode(c1.getString(c1.getColumnIndex("fld_inspectorname"))));
	      		    request.addProperty("fld_inspectiondate",db.decode(c1.getString(c1.getColumnIndex("fld_inspectiondate"))));	      		    
	      		    request.addProperty("fld_reportno",db.decode(c1.getString(c1.getColumnIndex("fld_reportno"))));
	      		    request.addProperty("fld_present",db.decode(c1.getString(c1.getColumnIndex("fld_present"))));
	      		    request.addProperty("fld_otherpresent",db.decode(c1.getString(c1.getColumnIndex("fld_otherpresent"))));
	      		    request.addProperty("fld_arrive",db.decode(c1.getString(c1.getColumnIndex("fld_arrive"))));
	      		    request.addProperty("fld_prior",db.decode(c1.getString(c1.getColumnIndex("fld_prior"))));
	      		    request.addProperty("fld_badge",db.decode(c1.getString(c1.getColumnIndex("fld_badge"))));
	      		    request.addProperty("fld_shoes",db.decode(c1.getString(c1.getColumnIndex("fld_shoes"))));
	      		    request.addProperty("fld_listen",db.decode(c1.getString(c1.getColumnIndex("fld_listen"))));
	      		    request.addProperty("fld_preinspection",db.decode(c1.getString(c1.getColumnIndex("fld_preinspection"))));
	      		    request.addProperty("fld_difference",db.decode(c1.getString(c1.getColumnIndex("fld_difference"))));
	      		    request.addProperty("fld_final",db.decode(c1.getString(c1.getColumnIndex("fld_final"))));
	      		    request.addProperty("fld_confident",db.decode(c1.getString(c1.getColumnIndex("fld_confident"))));
	      		    request.addProperty("fld_rate",db.decode(c1.getString(c1.getColumnIndex("fld_rate"))));
	      		    request.addProperty("fld_comments",db.decode(c1.getString(c1.getColumnIndex("fld_comments"))));
	      		    
	      		  String imagename = db.decode(c1.getString(c1.getColumnIndex("fld_signature")));
	      		    if(!imagename.equals(""))
	      		    {
	      	 			Bitmap bm=ShrinkBitmap(imagename, 400, 400);
	      	 			if(bm!=null)							
	      				{
	      					ByteArrayOutputStream out = new ByteArrayOutputStream();
	      					bm.compress(CompressFormat.PNG, 100, out);
	      					byte[] raw = out.toByteArray();
	      					request.addProperty("fld_signature", raw);
	      				}
	      	 			else
	      	 			{
	      	 				request.addProperty("fld_signature","");
	      	 			}
	      	 		}
	      	 		else
	      	 		{
	      				request.addProperty("fld_signature","");
	      			}
    		    
		      		    envelope1.setOutputSoapObject(request);
		      		    MarshalBase64 marshal = new MarshalBase64();
		      		    marshal.register(envelope1);
	      		    
	      		    request.addProperty("fld_date",db.decode(c1.getString(c1.getColumnIndex("fld_date"))));
	      		    
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("CustomerEvaluationSurvey request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"CustomerEvaluationSurvey", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("CustomerEvaluationSurvey "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	   
	                    	    	/* try
		 	          				 {
	                    	    		db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_sendtoserver='"+response.getProperty("ssv_Id").toString()+"' " +
		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' " +
		 	          									"and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and " +
		 	          											"fld_primary='"+c1.getString(c1.getColumnIndex("fld_primary"))+"' and fld_srid='"+getph+"'");
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          					 System.out.println("sadsf catch="+e.getMessage());
		 	          				 }*/
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
   }
	private void Submit_Photos() throws IOException, XmlPullParserException {			
		db.userid();		
		 cf.GetCurrentModuleName(getmodulename);
		   Bitmap bitmap=null;
	 		
	 		for(int j=0;j<cf.arrsub.length;j++)
			{
		  try
 		  {
				Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_inspectorid='"+db.UserId+"' " +
						"and fld_srid='"+getph+"'", null);
			  
 			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' " +
 					" and fld_module='"+getmodulename+"' and fld_submodule='"+cf.arrsub[j]+"'", null);
	       System.out.println("select * from " + db.SaveVCImage + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' " +
					" and fld_module='"+getmodulename+"' and fld_submodule='"+cf.arrsub[j]+"'");
 			/*System.out.println("coun imahe="+cur.getCount());*/
	        if(cur.getCount()>0)
	        {
	        	 cur.moveToFirst();	        	
	        	 for(int i=0;i<cur.getCount();i++,cur.moveToNext())
	        	 {
	        	 
	        	       		
	         		
	         		
	         		String imagepath = db.decode(cur.getString(cur.getColumnIndex("fld_image")));
	         		
				
						File f = new File(imagepath);
		        		if (f.exists()) {
		        			try {
		        				
		        				SoapObject request = new SoapObject(wb.NAMESPACE,"Save_VCImage");
		    	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 
		    	      		    envelope.dotNet = true;
		    	      		    
		    	        		request.addProperty("si_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
		    	         		request.addProperty("fld_inspectorid",db.UserId);
		    	         		request.addProperty("fld_srid",db.decode(cur.getString(cur.getColumnIndex("fld_srid"))));
		    	         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
		    	         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
		    	         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
		    	         		request.addProperty("fld_caption",db.decode(cur.getString(cur.getColumnIndex("fld_caption"))));
		    	         		request.addProperty("fld_vc",db.decode(cur.getString(cur.getColumnIndex("fld_vc"))));	      
		    	         		 /*Cursor c2 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
		    	 	      				" and fld_module='"+db.encode(getmodulename)+"' and fld_srid='"+getph+"' " +
		    	 	      						"and fld_VCName='"+c1.getString(c1.getColumnIndex("fld_vcname"))+"'", null);
		    	 	      		  if(c2.getCount()>0)
		    	 	      		  {
		    	 	      			  c2.moveToFirst();
		    	 	      			  request.addProperty("fld_ed_id",c2.getString(c2.getColumnIndex("fld_sendtoserver")));
		    	 	      		  }
		    	 	      		  else
		    	 	      		  {
		    	 	      			request.addProperty("fld_ed_id",c1.getString(c1.getColumnIndex("fld_ed_id")));  
		    	 	      		  }
		        				*/
		        				if(bitmap!=null)
		    					{
		    						bitmap.recycle();
		    					}
		    					bitmap = cf.ShrinkBitmap(imagepath, 400, 400);
		    					if(bitmap!=null)							
		    					{
		    						ByteArrayOutputStream out = new ByteArrayOutputStream();
		    						bitmap.compress(CompressFormat.PNG, 100, out);
		    						byte[] raw = out.toByteArray();
		    						request.addProperty("fld_image", raw);
		    					}
		    					else
		    					{
		    						request.addProperty("fld_image","");
		    					}
		    					
		    					
		    					envelope.setOutputSoapObject(request);
		    	         		System.out.println("Save_VCImage="+request);
		    	         		MarshalBase64 marshal = new MarshalBase64();
		    	        		marshal.register(envelope);
		    	         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
		    	         		SoapObject response = null;
		    	                try
		    	                {
		    	                      androidHttpTransport.call(wb.NAMESPACE+"Save_VCImage", envelope);
		    	                      response = (SoapObject)envelope.getResponse();
		    	                      System.out.println("Save_VCImage response"+response);
		    	                      
		    	                      if(response.getProperty("StatusCode").toString().equals("0"))
		    	              		  {
		    	                    	  
		    	                    	 try
		    	          				 {
		    	                    		 System.out.println("UPDATE "+db.SaveVCImage+ " SET fld_sendtoserver='"+response.getProperty("si_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+cf.arrsub[j]+"' and fld_srid='"+getph+"'");
		    	                    		db.hi_db.execSQL("UPDATE "+db.SaveVCImage+ " SET fld_sendtoserver='"+response.getProperty("si_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+cf.arrsub[j]+"' and fld_srid='"+getph+"'");
		    	          				 }
		    	          				 catch (Exception e) {
		    	          					// TODO: handle exception
		    	          				 }
		    	                    	handler_msg = 0;		                      	
		    	              		  }
		    	              		  else
		    	              		  {
		    	              			
		    	              			 error_msg = response.getProperty("StatusMessage").toString();
		    	              			 handler_msg=1;
		    	              			
		    	              		  }
		    	                }
		    	                catch(Exception e)
		    	                {
		    	                     e.printStackTrace();
		    	                }
		    					
							} catch (Exception e) {
								// TODO: handle exception
								
							}
						}
						else
						{
							
							
						}
					
	         		
	         		
	        	 }
	        }
    	}
 		catch (Exception e) {
			// TODO: handle exception
		}
			}
   }
	private void Save_Setvc() throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
		db.CreateTable(12);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' and fld_srid='"+getph+"'", null);
		System.out.println("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_module='"+getmodulename+"' and fld_srid='"+getph+"'"+"Conunt"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Save_Setvc");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("ssv_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));    		    
	      		  
	      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		    request.addProperty("fld_module",db.decode(c1.getString(c1.getColumnIndex("fld_module"))));	      		    
	      		    request.addProperty("fld_submodule",db.decode(c1.getString(c1.getColumnIndex("fld_submodule"))));
	      		    request.addProperty("fld_primary",db.decode(c1.getString(c1.getColumnIndex("fld_primary"))));
	      		    
	      			String str = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_secondary")))).toString();
	         		if(str.endsWith(","))
	         		{
	         			str = str.substring(0,str.length()-1);
	         		}
	         		request.addProperty("fld_secondary",str);
	      		    
	      		    request.addProperty("fld_comments",db.decode(c1.getString(c1.getColumnIndex("fld_comments"))));
	      		    request.addProperty("fld_condition",db.decode(c1.getString(c1.getColumnIndex("fld_condition"))));
	      		    request.addProperty("fld_other",db.decode(c1.getString(c1.getColumnIndex("fld_other"))));
	      		    request.addProperty("fld_checkbox",db.decode(c1.getString(c1.getColumnIndex("fld_checkbox"))));
	      		    request.addProperty("fld_inspectionpoint",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionpoint"))));
	      		    
	      		  request.addProperty("fld_apprrepaircost",db.decode(c1.getString(c1.getColumnIndex("fld_apprrepaircost"))));
	      		  request.addProperty("fld_contractortype",db.decode(c1.getString(c1.getColumnIndex("fld_contractortype"))));
	      		  request.addProperty("fld_photooption",db.decode(c1.getString(c1.getColumnIndex("fld_printphoto"))));
	      		    
	      		  
	      		  Cursor 	c2 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
	      				" and fld_module='"+db.encode(getmodulename)+"' and fld_srid='"+getph+"' " +
	      						"and fld_VCName='"+c1.getString(c1.getColumnIndex("fld_vcname"))+"'", null);
	      		  if(c2.getCount()>0)
	      		  {
	      			  c2.moveToFirst();
	      			  request.addProperty("fld_ed_id",c2.getString(c2.getColumnIndex("fld_sendtoserver")));
	      		  }
	      		  else
	      		  {
	      			request.addProperty("fld_ed_id",c1.getString(c1.getColumnIndex("fld_ed_id")));  
	      		  }
	      		  
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Save_Setvc request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Save_Setvc", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Save_Setvc "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	   
	                    	    	 try
		 	          				 {	                    	    		 
	                    	    		 System.out.println("UPDATE "+db.SaveSetVC+ " SET fld_ed_id='"+response.getProperty("fld_ed_id").toString()+"', fld_sendtoserver='"+response.getProperty("ssv_Id").toString()+"' " +
			 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' " +
	 	          									"and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and " +
	 	          											"fld_primary='"+c1.getString(c1.getColumnIndex("fld_primary"))+"' and fld_srid='"+getph+"'");
	                    	    		 
	                    	    		db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_ed_id='"+response.getProperty("fld_ed_id").toString()+"', fld_sendtoserver='"+response.getProperty("ssv_Id").toString()+"' " +
		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' " +
		 	          									"and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and " +
		 	          											"fld_primary='"+c1.getString(c1.getColumnIndex("fld_primary"))+"' and fld_srid='"+getph+"'");
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          					 System.out.println("sadsf catch="+e.getMessage());
		 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
	}
	private void Submit_PIA() throws IOException, XmlPullParserException 
	{
		db.CreateTable(37);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.PIA + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getph+"'", null);
		System.out.println("Submit_PIA"+"Cout="+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 SoapObject request = new SoapObject(wb.NAMESPACE,"PreinspectionAgreement");
		                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
		      		    envelope.dotNet = true;
		      		    
		      		    request.addProperty("pia_Id",0);
		      		    request.addProperty("fld_inspectorid",db.UserId);
		      		    request.addProperty("fld_srid",getph);
		      		   
		      		    String imagename = db.decode(c1.getString(c1.getColumnIndex("fld_signature")));
		      		    if(!imagename.equals(""))
		      		    {
		      	 			Bitmap bm=ShrinkBitmap(imagename, 400, 400);
		      	 			if(bm!=null)							
		      				{
		      					ByteArrayOutputStream out = new ByteArrayOutputStream();
		      					bm.compress(CompressFormat.PNG, 100, out);
		      					byte[] raw = out.toByteArray();
		      					request.addProperty("fld_signature", raw);
		      				}
		      	 			else
		      	 			{
		      	 				request.addProperty("fld_signature","");
		      	 			}
		      	 		}
		      	 		else
		      	 		{
		      				request.addProperty("fld_signature","");
		      			}
	      		    
			      		    envelope.setOutputSoapObject(request);
			      		    MarshalBase64 marshal = new MarshalBase64();
			      		    marshal.register(envelope);
			         		System.out.println("PreinspectionAgreement request="+request);
			         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
			         		SoapObject response =null;
			         		try
			                {
			         			 androidHttpTransport1.call(wb.NAMESPACE+"PreinspectionAgreement", envelope);
								
			                      response = (SoapObject)envelope.getResponse();
			                      System.out.println("PreinspectionAgreement "+response);
			                      handler_msg = 0;	  
			                      if(response.getProperty("StatusCode").toString().equals("0"))
			              		  {
			                    	     handler_msg = 0;	  
			              		  }
			              		  else
			              		  {
			              			 error_msg = response.getProperty("StatusMessage").toString();
			              			 handler_msg=1;
			              			
			              		  }
			                }
			                
			                catch(Exception e)
			                {
			                	System.out.println("sads ocver photof"+e.getMessage());
			                     e.printStackTrace();
			                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
   }
	
	
	public void Send_CoverPhoto() {
		// TODO Auto-generated method stub
		db.CreateTable(34);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.CoverPhoto + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getph+"'", null);
		System.out.println("Send_CoverPhoto"+"Cout="+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Save_ImageforHomeInspection");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("El_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("Elevation_ID",1);
	      		    
	      		    request.addProperty("InspectorID",db.UserId);
	      		  	request.addProperty("SRID",getph);      		    
	      		  
	      		    request.addProperty("Image_Description",db.decode(c1.getString(c1.getColumnIndex("fld_caption"))));
	      			System.out.println("testtt="+request);
	      		    String imagename = db.decode(c1.getString(c1.getColumnIndex("fld_image")));
	      		    if(!imagename.equals(""))
	      	 		{
	      	 			Bitmap bm=ShrinkBitmap(imagename, 400, 400);
	      	 			if(bm!=null)							
	      				{
	      					ByteArrayOutputStream out = new ByteArrayOutputStream();
	      					bm.compress(CompressFormat.PNG, 100, out);
	      					byte[] raw = out.toByteArray();
	      					request.addProperty("Image_Name", raw);
	      				}
	      	 			else
	      	 			{
	      	 				request.addProperty("Image_Name","");
	      	 			}
	      	 		}
	      	 		else
	      	 		{
	      				request.addProperty("Image_Name","");
	      			}
	      		    
	      		    envelope1.setOutputSoapObject(request);
	      		    MarshalBase64 marshal = new MarshalBase64();
	      		    marshal.register(envelope1);
	         		System.out.println("Save_ImageforHomeInspection request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Save_ImageforHomeInspection", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Save_ImageforHomeInspection "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	     try
	 	          				 {	     
	                    	    	 System.out.println("UPDATE "+db.CoverPhoto+ " SET fld_sendtoserver='"+response.getProperty("El_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.CoverPhoto+ " SET fld_sendtoserver='"+response.getProperty("El_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sads ocver photof"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
	}
	private void Save_Editvc() throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
		db.CreateTable(11);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_srid='"+getph+"' " +
						"and fld_selected='1'", null);
		
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Save_Editvc");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("sed_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
	      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		    request.addProperty("fld_module",db.decode(c1.getString(c1.getColumnIndex("fld_module"))));	      		    
	      		    request.addProperty("fld_submodule",db.decode(c1.getString(c1.getColumnIndex("fld_submodule"))));
	      		    request.addProperty("fld_VCName",db.decode(c1.getString(c1.getColumnIndex("fld_VCName"))));
	      		    request.addProperty("fld_selected",db.decode(c1.getString(c1.getColumnIndex("fld_selected"))));
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Save_Editvc request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Save_Editvc", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Save_Editvc "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	     try
	 	          				 {	     
	                    	    	 System.out.println("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"' and fld_srid='"+getph+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"' and fld_srid='"+getph+"'");
	 	          					System.out.println("REPSID="+response.getProperty("sed_Id").toString());
	 	          					
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
	}
	private void Save_Invoice() throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
		db.CreateTable(41);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.Invoice + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getph+"'", null);
		
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Invoice");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("invoice_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
	      		    request.addProperty("fld_invoiceno",db.decode(c1.getString(c1.getColumnIndex("fld_invoiceno"))));	
	      		    request.addProperty("fld_invoicedate",db.decode(c1.getString(c1.getColumnIndex("fld_invoicedate"))));
	      		    request.addProperty("fld_occupied",db.decode(c1.getString(c1.getColumnIndex("fld_occupied"))));
	      		    request.addProperty("fld_utilitieson",db.decode(c1.getString(c1.getColumnIndex("fld_utilitieson"))));	      		    
	      		    request.addProperty("fld_additionalfees",db.decode(c1.getString(c1.getColumnIndex("fld_additionalfees"))));
	      		    request.addProperty("fld_annualinspection",db.decode(c1.getString(c1.getColumnIndex("fld_annualinspection"))));
	      		    request.addProperty("fld_shippingandhandling",db.decode(c1.getString(c1.getColumnIndex("fld_shippingandhandling"))));
	      		    
	      		    
	      		    request.addProperty("fld_eocoverage",db.decode(c1.getString(c1.getColumnIndex("fld_eocoverage"))));
	      		    request.addProperty("fld_homeguide",db.decode(c1.getString(c1.getColumnIndex("fld_homeguide"))));	      		    
	      		    request.addProperty("fld_payment",db.decode(c1.getString(c1.getColumnIndex("fld_payment"))));
	      		    request.addProperty("fld_cardtype",db.decode(c1.getString(c1.getColumnIndex("fld_cardtype"))));
	      		    request.addProperty("fld_specialnotes",db.decode(c1.getString(c1.getColumnIndex("fld_specialnotes"))));
	      		    
	      		    request.addProperty("fld_standardhomeinsp",db.decode(c1.getString(c1.getColumnIndex("fld_standardhomeinsp"))));
	      		    request.addProperty("fld_crawlfees","0");	      		    
	      		    request.addProperty("fld_subtotal","0");
	      		    request.addProperty("fld_salestax",db.decode(c1.getString(c1.getColumnIndex("fld_salestax"))));
	      		    request.addProperty("fld_totalfees",db.decode(c1.getString(c1.getColumnIndex("fld_totalfees"))));
	      		    request.addProperty("fld_amountreceived",db.decode(c1.getString(c1.getColumnIndex("fld_amountreceived"))));
	      		    request.addProperty("fld_balancedue",db.decode(c1.getString(c1.getColumnIndex("fld_balancedue"))));
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Invoice request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Invoice", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Save_Editvc "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	     try
	 	          				 {	     
	                    	    	 System.out.println("UPDATE "+db.Invoice+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.Invoice+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
	}
	private void Save_Homewarranty() throws IOException, XmlPullParserException {    	
		// TODO Auto-generated method stub
		db.CreateTable(41);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.HomeWarranty + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getph+"'", null);		
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Home_Warranty");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		   // request.addProperty("h_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
	      		    request.addProperty("fld_homewarrantoption",db.decode(c1.getString(c1.getColumnIndex("fld_homewarrantoption"))));
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Home_Warranty request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Home_Warranty", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Home_Warranty "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	    /* try
	 	          				 {	     
	                    	    	 System.out.println("UPDATE "+db.HomeWarranty+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.HomeWarranty+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }*/
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
	}
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(LoadSubModules.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public void success_alert() {
		// TODO Auto-generated method stub
       
		AlertDialog al = new AlertDialog.Builder(LoadSubModules.this).setMessage("You've successfully submitted your Inspection.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				  try
    				 {
					  System.out.println("UPDATE "+db.OrderInspection+ " SET fld_status='2' WHERE fld_orderedid='"+getph+"'");
					  
    					db.hi_db.execSQL("UPDATE "+db.OrderInspection+ " SET fld_status='2' WHERE fld_orderedid='"+getph+"'");
    				 }
    				 catch (Exception e) {
    					// TODO: handle exception
    				 }
				Intent in = new Intent(LoadSubModules.this,CompletedInspection.class);
				Bundle b5 = new Bundle();
				b5.putString("inspectorid", db.UserId);			
				in.putExtras(b5);
				startActivity(in);
				finish();
				
				
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	
	
	private void Submit_Policyholderinformation() throws IOException, XmlPullParserException {
		
		db.userid();
		try
		  {
			Cursor cur = db.hi_db.rawQuery("select * from " + db.OrderInspection + " WHERE fld_inspectorid='"+db.UserId+"' and fld_orderedid='"+getph+"' ", null);
	        System.out.println("select * from " + db.OrderInspection + " WHERE fld_inspectorid='"+db.UserId+"' and fld_orderedid='"+getph+"' "+"CUR="+cur.getCount());
			if(cur.getCount()>0)
	        {
	        	cur.moveToFirst();	     
	    SoapObject request = new SoapObject(wb.NAMESPACE,"OrderInspection");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
 		envelope.dotNet = true;
 		
 		request.addProperty("OrderId",cur.getString(cur.getColumnIndex("fld_orderedid")));
 		request.addProperty("fld_inspectioncompany",db.decode(cur.getString(cur.getColumnIndex("fld_inspectioncompany"))));
 		request.addProperty("fld_policynumber",db.decode(cur.getString(cur.getColumnIndex("fld_policynumber"))));
 		
 		request.addProperty("fld_squarefootage",cur.getString(cur.getColumnIndex("fld_squarefootage")));
 		request.addProperty("fld_noofstories",cur.getString(cur.getColumnIndex("fld_noofstories")));
 		request.addProperty("fld_inspectiondate",cur.getString(cur.getColumnIndex("fld_inspectiondate")));
 		request.addProperty("fld_inspectiontime",cur.getString(cur.getColumnIndex("fld_inspectiontime")));
 		request.addProperty("fld_yearofconstruction",cur.getString(cur.getColumnIndex("fld_yearofconstruction")));
 		
 		request.addProperty("fld_otheryearofconstruction",cur.getString(cur.getColumnIndex("fld_otheryearofconstruction")));
 		request.addProperty("fld_inspectionfee",cur.getString(cur.getColumnIndex("fld_inspectionfee")));
 		request.addProperty("fld_firstname",db.decode(cur.getString(cur.getColumnIndex("fld_firstname"))));
 		request.addProperty("fld_lastname",db.decode(cur.getString(cur.getColumnIndex("fld_lastname"))));
 		request.addProperty("fld_email",db.decode(cur.getString(cur.getColumnIndex("fld_email"))));
 		request.addProperty("fld_phone",db.decode(cur.getString(cur.getColumnIndex("fld_phone"))));

 		request.addProperty("fld_inspectionaddress1",db.decode(cur.getString(cur.getColumnIndex("fld_inspectionaddress1"))));
 		request.addProperty("fld_inspectionaddress2",db.decode(cur.getString(cur.getColumnIndex("fld_inspectionaddress2"))));

 		request.addProperty("fld_inspectioncity",db.decode(cur.getString(cur.getColumnIndex("fld_inspectioncity"))));
 		request.addProperty("fld_inspectionstate",cur.getString(cur.getColumnIndex("fld_inspectionstate")));
 		
 		request.addProperty("fld_zip",cur.getString(cur.getColumnIndex("fld_zip")));
 		request.addProperty("fld_flag",cur.getString(cur.getColumnIndex("fld_flag")));

 		
 		request.addProperty("fld_mailingaddress1",db.decode(cur.getString(cur.getColumnIndex("fld_mailingaddress1"))));
 		request.addProperty("fld_mailingaddress2",db.decode(cur.getString(cur.getColumnIndex("fld_mailingaddress2"))));
 		request.addProperty("fld_mailingcity",db.decode(cur.getString(cur.getColumnIndex("fld_mailingcity"))));
 		request.addProperty("fld_mailingstate",db.decode(cur.getString(cur.getColumnIndex("fld_mailingstate"))));
 		request.addProperty("fld_mailingcounty",db.decode(cur.getString(cur.getColumnIndex("fld_mailingcounty"))));
 		request.addProperty("fld_mailingzip",cur.getString(cur.getColumnIndex("fld_mailingzip")));

 		request.addProperty("fld_inspectorid",db.UserId);
 		request.addProperty("fld_status",1); //For place order 1 = new record placing
 		
 		envelope.setOutputSoapObject(request);
 		System.out.println("request="+request);
 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
 		SoapObject response = null;
        try
        {
              androidHttpTransport.call(wb.NAMESPACE+"OrderInspection", envelope);
              response = (SoapObject)envelope.getResponse();
              System.out.println("OrderInspection "+response);
              
              
        }
        catch(Exception e)
        {
             e.printStackTrace();
        }
 		
		} 
		  } catch(Exception e)
	        {
	             e.printStackTrace();
	        }
	 }
   
private void Submit_Template() throws IOException, XmlPullParserException {
		db.userid();
		for(int i=0;i<cf.arrsub.length;i++)
		{
 			
		  try
 		  {
 			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'", null);
	        System.out.println("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'"+"CUR="+cur.getCount());
 			if(cur.getCount()>0)
	        {
	        	cur.moveToFirst();	        	
	        	
	        	/*for(int i=0;i<cur.getCount();i++,cur.moveToNext())
	        	 * 
	        	{*/
	        	    SoapObject request = new SoapObject(wb.NAMESPACE,"Save_SubModule");
	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope.dotNet = true;
	      		    
	        		request.addProperty("ssm_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
	         		request.addProperty("fld_inspectorid",db.UserId);
	         		request.addProperty("fld_srid",db.decode(cur.getString(cur.getColumnIndex("fld_srid"))));
	         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
	         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
	         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
	         		request.addProperty("fld_NA",cur.getInt(cur.getColumnIndex("fld_NA")));
	         		String str = Html.fromHtml(db.decode(cur.getString(cur.getColumnIndex("fld_description")))).toString();
	         		if(str.endsWith(","))
	         		{
	         			str = str.substring(0,str.length()-1);
	         		}
	         		request.addProperty("fld_description",str);
	         		
	         		envelope.setOutputSoapObject(request);
	         		//System.out.println("request="+request);
	         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
	         		SoapObject response = null;
	                try
	                {
	                      androidHttpTransport.call(wb.NAMESPACE+"Save_SubModule", envelope);
	                      response = (SoapObject)envelope.getResponse();
	                      //System.out.println("Save_SubModule "+response);
	                      
	                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	 
	                    		  try
	 	          				 {
	 	          					db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET fld_sendtoserver='"+response.getProperty("ssm_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	                    	  
	                    	 
	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                catch(Exception e)
	                {
	                     e.printStackTrace();
	                }
	        	}
	        else
	        {
	        	handler_msg = 0;
	        }
	  	}
 		catch (Exception e) {
			// TODO: handle exception
		}
 		
		}      
       
   }
	private void Display() {
		// TODO Auto-generated method stub
		
		String str="";
		/*for(int i=0;i<cf.arrsub.length;i++)
		{
			str += cf.arrsub[i]+",";
		}*/
		
			try
			{
				//Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'  and fld_module='"+db.encode(getmodulename)+"' and fld_srid='"+getph+"'", null);
				Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_srid='"+getph+"'", null);
				System.out.println("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+getmodulename+"' and fld_srid='"+getph+"'");
	            int cnt = cur.getCount();
	            if(cnt>0)
	            {
	            	cur.moveToFirst();
					if (cur != null) {
						cbval=  new String[cur.getCount()];
						arrstrmodule = new String[cur.getCount()];
						arrdescription= new String[cur.getCount()];
						int j=0;
						do {
							String incval =cur.getString(cur.getColumnIndex("fld_NA"));
							String suboption =db.decode(cur.getString(cur.getColumnIndex("fld_submodule")));
							
						//	str_module += suboption+"~";
							//arrstrmodule = str_module.split("~");
							arrstrmodule[j] = suboption;
							arrdescription[j] =db.decode(cur.getString(cur.getColumnIndex("fld_description")));
							System.out.println("incval="+suboption+" "+incval);
							
							if(incval.equals("1"))
				            {
								cbval[j] = "checked";
								
					        }
				            else
				            {
				            	cbval[j] = "unchecked";
				            	//str_cbval += "unchecked"+"~";
								
						    }
							j++;
							
						} while (cur.moveToNext());
						/*cbval = str_cbval.split("~");
						System.out.println();*/
						
						effadap = new EfficientAdapter(cbval, this);
						modulelist.setAdapter(effadap);
						System.out.println("selectionid"+selectionid);
						if(selectionid!=0)
						{
							modulelist.setSelection(selectionid);
						}
					}
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		//}
		
	}
	private void DefaultValue_Insert(String gettempname) {
		// TODO Auto-generated method stub
		cf.GetCurrentModuleName(getmodulename);
		for(int i=0;i<cf.arrsub.length;i++)
		{
			try
			{
				selcur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	            int cnt = selcur.getCount();System.out.println("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_module='"+db.encode(getmodulename)+"'"+"count="+cnt);
	            if(cnt==0)
	            {
	            	//Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
	            	Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"'", null);
		            if(cur.getCount()==0)
		            {
		            	try 
		            	{
		            		db.hi_db.execSQL("INSERT INTO "+db.SaveSubModule+" (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_NA,fld_description) VALUES" +
									"('"+db.UserId+"','"+getph+"','"+db.encode(getmodulename)+"','"+db.encode(cf.arrsub[i])+"','"+db.encode(gettempname)+"','1','')");
	                    }
		            	catch (Exception e) {
							// TODO: handle exception
		            		System.out.println("e="+e.getMessage());
						}
		            }
	            }
	            else
	            {
	            	
					if (selcur != null) {
						selcur.moveToFirst();System.out.println("cn="+cnt);
						do {
							String incval =selcur.getString(selcur.getColumnIndex("fld_NA"));
							String defvalue =db.decode(selcur.getString(selcur.getColumnIndex("fld_description")));
							
							//Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
							Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	    		            if(cur.getCount()==0)
	    		            {
    							try
    			            	{
    			            		db.hi_db.execSQL("INSERT INTO "+db.SaveSubModule+" (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES" +
    										"('"+db.UserId+"','"+getph+"','"+db.encode(getmodulename)+"','"+db.encode(cf.arrsub[i])+"','"+db.encode(defvalue)+"','"+db.encode(gettempname)+"','"+incval+"')");
    		                 	}
    			            	catch (Exception e) {
    								// TODO: handle exception
    							}
	    		            }
	    		            else
	    		            {
	    		            	System.out.println("UPDATE "+db.SaveSubModule+ " SET fld_NA='"+incval+"',fld_templatename='"+db.encode(gettempname)+"',fld_description='"+db.encode(defvalue)+"' WHERE fld_inspectorid='"+db.UserId+"' " +
	    		            			"and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'");
	    		            	db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET fld_NA='"+incval+"',fld_templatename='"+db.encode(gettempname)+"',fld_description='"+db.encode(defvalue)+"' WHERE fld_inspectorid='"+db.UserId+"' " +
	    		            			"and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'");
	    		            }
						
						}while (selcur.moveToNext());
					}
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
		/*	cf.GetVC(getmodulename,cf.arrsub[i]);
			vcarr = cf.submodvcid;
			try
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+getmodulename+"' and fld_submodule='"+cf.arrsub[i]+"'", null);
	            int cnt = cur.getCount();System.out.println("cnt="+cnt+"select * from " + db.CreateEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+cf.arrsub[i]+"'");
	            if(cnt>0)
	            {
	            	Cursor cur1 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+cf.arrsub[i]+"'", null);
	                System.out.println("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+cf.arrsub[i]+"'"+cur1.getCount());
	                cur.moveToFirst();
	                if (cur != null) 
	                {
	                	 do 
	                	 {
							    String defvalue =db.decode(cur.getString(cur.getColumnIndex("fld_VCName")));
							    String inc =cur.getString(cur.getColumnIndex("fld_selected"));
							    if(cur1.getCount()==0)
							    {
							    	db.hi_db.execSQL(" INSERT INTO "
		    							+ db.SaveEditVC
		    							+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_VCName,fld_templatename,fld_selected) VALUES"
		    							+ "('"+db.UserId+"','"+getph+"','"+db.encode(getmodulename)+"','"+cf.arrsub[i]+"','"+db.encode(defvalue)+"','"
		    							+ db.encode(gettempname)+"','"+inc+"')");
							    }
							    else
							    {
							    		db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_selected='"+inc+"' WHERE fld_inspectorid='"+db.UserId+"' and" +
					            				" fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_VCName='"+db.encode(defvalue)+"' and fld_submodule='"+cf.arrsub[i]+"'");
					            }				    						    
	                	  }while (cur.moveToNext());
	               }	
	            	
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
			try
			{
			 Cursor c1=db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+cf.arrsub[i]+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
	         System.out.println("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+cf.arrsub[i]+"' and fld_templatename='"+db.encode(gettempname)+"'"+c1.getCount());
			 if(c1.getCount()>0)
			 {
				 if (c1 != null) {
					 c1.moveToFirst();
						do {
						    String primvalue =c1.getString(c1.getColumnIndex("fld_primary"));
						    String secvalue =c1.getString(c1.getColumnIndex("fld_secondary"));
						    String commvalue =c1.getString(c1.getColumnIndex("fld_comments"));
						    String inspvalue =c1.getString(c1.getColumnIndex("fld_inspectionpoint"));
						    String apprcostvalue =c1.getString(c1.getColumnIndex("fld_apprrepaircost"));
						    String conttypevalue =c1.getString(c1.getColumnIndex("fld_contractortype"));
						    Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+cf.arrsub[i]+"' and fld_srid='"+getph+"' " +
							 		"and fld_module='"+getmodulename+"' and fld_templatename='"+gettempname+"'", null);
						    if(cur.getCount()==0)
						    {
						    	System.out.println(" INSERT INTO "
										+ db.SaveSetVC
										+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype) VALUES"
										+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+cf.arrsub[i]+"','"+gettempname+"','"+primvalue+"','"
										+secvalue+"','"+commvalue+"','','','','"+inspvalue+"','"+apprcostvalue+"','"+conttypevalue+"')");
						    	db.hi_db.execSQL(" INSERT INTO "
										+ db.SaveSetVC
										+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype) VALUES"
										+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+cf.arrsub[i]+"','"+gettempname+"','"+primvalue+"','"
										+secvalue+"','"+commvalue+"','','','','"+inspvalue+"','"+apprcostvalue+"','"+conttypevalue+"')");
						    }
						    else
						    {
						    	System.out.println("UPDATE "+db.SaveSetVC+ " SET fld_secondary='"+secvalue+"',"
				            			+"fld_comments='"+commvalue+"',"
				            			+"fld_inspectionpoint='"+inspvalue+"'," +
				            					"fld_apprrepaircost='"+apprcostvalue+"',fld_contractortype='"+conttypevalue+"' " +
				            							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+cf.arrsub[i]+"' and fld_primary='"+primvalue+"'");
						    	db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_secondary='"+secvalue+"',"
				            			+"fld_comments='"+commvalue+"',"
				            			+"fld_inspectionpoint='"+inspvalue+"'," +
				            					"fld_apprrepaircost='"+apprcostvalue+"',fld_contractortype='"+conttypevalue+"' " +
				            							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+cf.arrsub[i]+"' and fld_primary='"+primvalue+"'");
						    }
						    	
						}while(c1.moveToNext());
				 }
			 }
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println("excepton setvc"+e.getMessage());
			}*/
			
			
		}
		Display();
		
	}
	protected void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(LoadSubModules.this, LoadModules.class);
		Bundle b2 = new Bundle();
		b2.putString("selectedid", getph);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("inspectorid", db.UserId);			
		intent.putExtras(b3);
		startActivity(intent);
		finish();
	}
	class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		int bi,ci,si,ei,ski;
		View v2;
		
		public EfficientAdapter(String[] cbval, Context context) {
			mInflater = LayoutInflater.from(context);
			value=cbval;
	
		}

		public int getCount() {
			int arrlen = 0;
			
			arrlen = arrstrmodule.length;		
	
			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			
					    holder = new ViewHolder();
					   
					    
						convertView = mInflater.inflate(R.layout.custommodulelist, null);	
						holder.rel = (RelativeLayout)convertView.findViewById(R.id.rel);
						holder.moduletext = (TextView) convertView.findViewById(R.id.submodule);
						holder.cbsubmodule = (CheckBox)convertView.findViewById(R.id.cb_submodule);
						holder.spinsubmodule = (TextView)convertView.findViewById(R.id.spin_submodule);
						holder.spinsubmodule.setText("--Select--");
						holder.vcsubmodule = (TextView)convertView.findViewById(R.id.vc_submodule);
						holder.selectedatapoints = (TextView)convertView.findViewById(R.id.selected_data);
						holder.selectedatapoints.setVisibility(v.GONE);
						System.out.println("textt="+arrdescription[position]);
						
						if (!arrdescription[position].equals("null") && !arrdescription[position].equals("")) 
						{
							arrdescription[position] = arrdescription[position].replace("null", "");
							holder.selectedatapoints.setVisibility(v.VISIBLE);
							String selectedtxt = cf.replacelastendswithcomma(arrdescription[position]);
							holder.selectedatapoints.setText(Html.fromHtml("<b>Selected : </b>"+selectedtxt));						
						}
						else
						{
							holder.selectedatapoints.setVisibility(v.GONE);
						}
						
						
						 convertView.setTag(holder);				 
						 holder.cbsubmodule.setOnCheckedChangeListener(new oncheckedlistener(position,holder.cbsubmodule,holder.spinsubmodule,holder.vcsubmodule));
						 holder.cbsubmodule.setOnTouchListener(new OntouchListener(position,holder.cbsubmodule,holder.spinsubmodule,holder.vcsubmodule));
						 holder.spinsubmodule.setOnClickListener(new onclicklistener(position,holder.cbsubmodule));
						 holder.vcsubmodule.setOnClickListener(new vcclicklistener(position,holder.cbsubmodule,holder.spinsubmodule,holder.vcsubmodule));
						 System.out.println("value[position]="+value[position]);
						 if(value[position].equals("checked"))
						 {
							 holder.cbsubmodule.setChecked(true);holder.cbsubmodule.setTextColor(Color.parseColor("#7CB34F"));
						 }
						 else
						 {
							 holder.cbsubmodule.setChecked(false);
						 }
					
						if (arrstrmodule[position].contains("null")) {
							arrstrmodule[position] = arrstrmodule[position].replace("null", "");
							holder.moduletext.setText(Html.fromHtml(arrstrmodule[position]));
						
						}
						else
						{
							holder.moduletext.setText(Html.fromHtml(arrstrmodule[position]));
							
						}		
						
						if (arrstrmodule[position].equals("Basement"))
						{
							bi = position;
						}
						if (arrstrmodule[position].equals("Crawlspace"))
						{
							ci = position;
						}
						if(arrstrmodule[position].equals("Siding/Surface Materials") && getmodulename.equals("Exterior"))
						{
							si=position;
						}
						if (arrstrmodule[position].equals("Electric Meter Location"))
						{
							ei = position;
						}
						if (arrstrmodule[position].equals("Skylights"))
						{
							ski = position;
						}
						
						if (arrstrmodule[position].equals("Basement")||arrstrmodule[position].equals("Crawlspace")||(arrstrmodule[position].equals("Siding/Surface Materials")&& getmodulename.equals("Exterior"))
								||arrstrmodule[position].equals("Electric Meter Location")||arrstrmodule[position].equals("Skylights"))
						{
							 holder.spinsubmodule.setVisibility(convertView.INVISIBLE);
							 holder.vcsubmodule.setVisibility(convertView.INVISIBLE);
							
						}
						if (arrstrmodule[position].equals("Basement - Size")||arrstrmodule[position].equals("Basement - Access")
								||arrstrmodule[position].equals("Basement - Stairs") ||arrstrmodule[position].equals("Basement - Floors")
								||arrstrmodule[position].equals("Basement - Exposed Columns and Beams") ||arrstrmodule[position].equals("Basement - Walls/Ceilings")
								||arrstrmodule[position].equals("Basement - Exposed Floor Systems")||arrstrmodule[position].equals("Basement - Drain Tile To")
								||arrstrmodule[position].equals("Basement - Heat/Cool(HVAC)")||arrstrmodule[position].equals("Basement - Finishes")
								||arrstrmodule[position].equals("Crawlspace Area - Characteristics")||arrstrmodule[position].equals("Crawlspace - Access")
								||arrstrmodule[position].equals("Crawlspace - Main Beam")||arrstrmodule[position].equals("Crawlspace - Floors")
								||arrstrmodule[position].equals("Crawlspace - Exposed Foundation Wall")||arrstrmodule[position].equals("Masonry/Plaster")
								||arrstrmodule[position].equals("Siding/Paneling")||arrstrmodule[position].equals("Wall Shingles")
								||arrstrmodule[position].equals("Accessibility")||arrstrmodule[position].equals("Main Panel Meter Located Inside")
								||arrstrmodule[position].equals("Main Panel Meter Located Outside")||arrstrmodule[position].equals("Main Panel Meter")
								||arrstrmodule[position].equals("# of Skylights")||arrstrmodule[position].equals("Skylight Type"))
						{
							 if(value[bi].equals("checked"))
							 {
								 if (arrstrmodule[position].equals("Basement - Size")||arrstrmodule[position].equals("Basement - Access")
								    ||arrstrmodule[position].equals("Basement - Stairs") ||arrstrmodule[position].equals("Basement - Floors")
									||arrstrmodule[position].equals("Basement - Exposed Columns and Beams") ||arrstrmodule[position].equals("Basement - Walls/Ceilings")
									||arrstrmodule[position].equals("Basement - Exposed Floor Systems")||arrstrmodule[position].equals("Basement - Drain Tile To")
									||arrstrmodule[position].equals("Basement - Heat/Cool(HVAC)")||arrstrmodule[position].equals("Basement - Finishes")
									)
									{
								        holder.rel.setVisibility(v.GONE);
									}
							
							 }
							 else
							 {
								   if (arrstrmodule[position].equals("Basement - Size")||arrstrmodule[position].equals("Basement - Access")
								    ||arrstrmodule[position].equals("Basement - Stairs") ||arrstrmodule[position].equals("Basement - Floors")
									||arrstrmodule[position].equals("Basement - Exposed Columns and Beams") ||arrstrmodule[position].equals("Basement - Walls/Ceilings")
									||arrstrmodule[position].equals("Basement - Exposed Floor Systems")||arrstrmodule[position].equals("Basement - Drain Tile To")
									||arrstrmodule[position].equals("Basement - Heat/Cool(HVAC)")||arrstrmodule[position].equals("Basement - Finishes")
									)
									{
								        holder.rel.setVisibility(v.VISIBLE);
									}
								 
							 }
							 if(value[ci].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("Crawlspace Area - Characteristics")||arrstrmodule[position].equals("Crawlspace - Access")
									||arrstrmodule[position].equals("Crawlspace - Main Beam")||arrstrmodule[position].equals("Crawlspace - Floors")
									||arrstrmodule[position].equals("Crawlspace - Exposed Foundation Wall"))
								 {
									    holder.rel.setVisibility(v.GONE);
									    
									
									 
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("Crawlspace Area - Characteristics")||arrstrmodule[position].equals("Crawlspace - Access")
									||arrstrmodule[position].equals("Crawlspace - Main Beam")||arrstrmodule[position].equals("Crawlspace - Floors")
									||arrstrmodule[position].equals("Crawlspace - Exposed Foundation Wall"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
							 
							/* if(value[ci].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("Crawlspace Area - Characteristics")||arrstrmodule[position].equals("Crawlspace - Access")
									||arrstrmodule[position].equals("Crawlspace - Main Beam")||arrstrmodule[position].equals("Crawlspace - Floors")
									||arrstrmodule[position].equals("Crawlspace - Exposed Foundation Wall"))
								 {
									 //value[position]="checked";
									 holder.rel.setVisibility(v.GONE);
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("Crawlspace Area - Characteristics")||arrstrmodule[position].equals("Crawlspace - Access")
									||arrstrmodule[position].equals("Crawlspace - Main Beam")||arrstrmodule[position].equals("Crawlspace - Floors")
									||arrstrmodule[position].equals("Crawlspace - Exposed Foundation Wall"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
							 */
							 if(value[si].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("Masonry/Plaster")
									||arrstrmodule[position].equals("Siding/Paneling")||arrstrmodule[position].equals("Wall Shingles")
									||arrstrmodule[position].equals("Accessibility"))
								 {
									 holder.rel.setVisibility(v.GONE);
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("Masonry/Plaster")
											||arrstrmodule[position].equals("Siding/Paneling")||arrstrmodule[position].equals("Wall Shingles")
											||arrstrmodule[position].equals("Accessibility"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
							 
							 if(value[ei].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("Main Panel Meter Located Inside")
								    ||arrstrmodule[position].equals("Main Panel Meter Located Outside")||arrstrmodule[position].equals("Main Panel Meter"))
								 {
									 holder.rel.setVisibility(v.GONE);
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("Main Panel Meter Located Inside")
										    ||arrstrmodule[position].equals("Main Panel Meter Located Outside")||arrstrmodule[position].equals("Main Panel Meter"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
							 
							 if(value[ski].equals("checked"))
							 {
								 if(arrstrmodule[position].equals("# of Skylights")||arrstrmodule[position].equals("Skylight Type"))
								 {
									 holder.rel.setVisibility(v.GONE);
								 }
							 }
							 else
							 {
								 if(arrstrmodule[position].equals("# of Skylights")||arrstrmodule[position].equals("Skylight Type"))
								 {
									 holder.rel.setVisibility(v.VISIBLE);
								 }
							 }
						}
						System.out.println("posi="+position);
			return convertView;
		}

		 class ViewHolder {
			TextView moduletext,spinsubmodule,vcsubmodule,selectedatapoints;
			ImageView moduleimg;
			CheckBox cbsubmodule;
			RelativeLayout rel;
		   
		}

	}
	
	 class OntouchListener implements OnTouchListener{
		    public int id =0;
			public TextView spmod,vcmod;
			public CheckBox cbmod;

		public OntouchListener(int position, CheckBox cbsubmodule,
				TextView spinsubmodule, TextView vcsubmodule) {
			// TODO Auto-generated constructor stub
			
			id=position;
			cbmod=cbsubmodule;
			spmod=spinsubmodule;
			vcmod=vcsubmodule;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if(cbmod.isChecked())
			{
			    chkbool=true;
			}
			else
			{
				chkbool=false;
			}
			return false;
		}
		 
	 }
	class vcclicklistener implements OnClickListener
	{
		public int id =0;
		public TextView spmod,vcmod;
		public CheckBox cbmod;
		
		public vcclicklistener(int position, CheckBox cbsubmodule, TextView spinsubmodule, TextView vcsubmodule) {
			// TODO Auto-generated constructor stub
			id=position;
			cbmod=cbsubmodule;
			spmod=spinsubmodule;
			vcmod=vcsubmodule;
			
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(!cbmod.isChecked())
			{System.out.println("ends="+getph);
				Intent intent = new Intent(LoadSubModules.this, LoadNewSetVC.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", "start");			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("submodulename", arrstrmodule[id]);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("selectedid", getph);			
				intent.putExtras(b6);
				startActivity(intent);
				finish();
				
			}
		}
		
	}
	class onclicklistener implements OnClickListener{

		public int id =0;
		public CheckBox cbmod;
		
		public onclicklistener(int position, CheckBox cbsubmodule) {
			// TODO Auto-generated constructor stub
			id = position;
			cbmod=cbsubmodule;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			System.out.println("inside onclicklistener lsiterner");
			if(!cbmod.isChecked())
			{
			   // new Load_Show_Dialog(arrstrmodule[id],gettempname,getmodulename,getph,LoadSubModules.this);
				 Intent i;
				newid=id;
				if(arrstrmodule[id].equals("Washing Machine Present") || arrstrmodule[id].equals("Dryer Present") || arrstrmodule[id].equals("Dishwasher Present")
						|| arrstrmodule[id].equals("Disposal Present")|| arrstrmodule[id].equals("Oven Present")|| arrstrmodule[id].equals("Exhaust Fan System Present")
						|| arrstrmodule[id].equals("Refrigerator Present")|| arrstrmodule[id].equals("Microwave Present")|| arrstrmodule[id].equals("Trash Compactor Present")
						|| arrstrmodule[id].equals("Bread Warmer Present") ||  arrstrmodule[id].equals("Range top Present") ||  arrstrmodule[id].equals("Lawn Sprinkler system Present")
						|| arrstrmodule[id].equals("Water Softener Present") || arrstrmodule[id].equals("Water heater Present") || arrstrmodule[id].equals("Attic/House fan Present")
						|| arrstrmodule[id].equals("Door bell Present"))
				{
					i = new Intent(LoadSubModules.this,AppLoadShowDialog.class);
					i.putExtra("currentview", "start");
				}
				else if(arrstrmodule[id].equals("Pool Features") || arrstrmodule[id].equals("Pool Lighting")
						|| arrstrmodule[id].equals("Pool Equipment") || arrstrmodule[id].equals("Plumbing Systems")
						|| arrstrmodule[id].equals("Pool Pumping System")
						|| arrstrmodule[id].equals("Pool Cleaning Equipment") || arrstrmodule[id].equals("Water Treatment")
						|| arrstrmodule[id].equals("Water Heating Systems") || arrstrmodule[id].equals("Timers/Clocks/Controls")
						|| arrstrmodule[id].equals("Electrical Service") || arrstrmodule[id].equals("Pool Cover(s)")
						|| arrstrmodule[id].equals("Safety Equipment")|| arrstrmodule[id].equals("Water Chemistry Test")
						|| arrstrmodule[id].equals("General Comments/Observations"))
				{
					i = new Intent(LoadSubModules.this,LoadPoolShowDialog.class);
					i.putExtra("currentview", "start");
				}
				else
				{
					System.out.println("TTTTTTTT");
					i = new Intent(LoadSubModules.this,LoadShowDialog.class);
					i.putExtra("currentview","start");
				}
			   					System.out.println("clickends="+getph+gettempname+"getph"+getph+arrstrmodule[id]);
				i.putExtra("arrstr", arrstrmodule[id]);
				i.putExtra("id",id);
				i.putExtra("templatename", gettempname);
				i.putExtra("modulename", getmodulename);
				i.putExtra("selectedid", getph);
				startActivityForResult(i, 101);			
			}
		}
		
	}
	 @Override  
     protected void onActivityResult(int requestCode, int resultCode, Intent data)  
     {  
               super.onActivityResult(requestCode, resultCode, data);  
                   
                // check if the request code is same as what is passed  here it is 2  
                      if(requestCode==101)  
                       {  
                    	  gettempname=data.getStringExtra("templatename");  
                    	  getmodulename=data.getStringExtra("modulename");
                    	  getph=data.getStringExtra("selectedid");
                    	  selectionid=data.getExtras().getInt("id");
                    	  Display();
                    	  effadap.notifyDataSetChanged();	
                    	 
                    	  
                       }  
   
   }  
	 @Override
	    protected void onSaveInstanceState(Bundle outState) {
		
	        outState.putInt("id_index", newid);
	        outState.putStringArray("module_index", arrstrmodule);
	        outState.putString("temp_index", gettempname);
	        outState.putString("modulename_index", getmodulename);
	        outState.putInt("handler", handler_msg);
	        outState.putInt("submitalert", submitalert);
	        outState.putInt("finalsubmit", finalsubmit);
	        if(finalsubmit==1)
	        {
		        outState.putString("radiotext", submitrdtext);
		        outState.putString("captcha", word);System.out.println("wordonsave="+word+submitrdtext+enterword);
		        outState.putString("enterword", ((EditText)dialog.findViewById(R.id.wrdedt1)).getText().toString());
	        }
	        super.onSaveInstanceState(outState);
	  }
	 @Override
	    protected void onRestoreInstanceState(Bundle savedInstanceState) {
		 String resword="",reenterword="";
		   newid = savedInstanceState.getInt("id_index");
		   arrstrmodule = savedInstanceState.getStringArray("module_index");
		   gettempname = savedInstanceState.getString("temp_index");
		   getmodulename = savedInstanceState.getString("modulename_index");
		   handler_msg = savedInstanceState.getInt("handler");System.out.println("handler_msg"+handler_msg);
		   submitalert = savedInstanceState.getInt("submitalert");System.out.println("handler_msg"+handler_msg);
		   
		   finalsubmit = savedInstanceState.getInt("finalsubmit");
		   if(finalsubmit==1)
		   {
			   submitrdtext = savedInstanceState.getString("radiotext");
			    resword = savedInstanceState.getString("captcha");
			    reenterword = savedInstanceState.getString("enterword");
			   System.out.println("submitalert="+submitalert+submitrdtext);
		   }
		   System.out.println("word="+reenterword);
		   if(submitalert==1)
		   {
			   submitalert();
			  
			   //wrdedit.setText(word);
		   }
		   if(finalsubmit==1)
		   {
			   finalsubmit();
			   if(!submitrdtext.equals(""))
			   {
			      ((RadioButton)submitrdgp.findViewWithTag(submitrdtext)).setChecked(true);
			   }
			   if(!resword.equals(""))
			   {
			       wrdedit.setText(resword);
			       word=resword;
			   }
			   if(!reenterword.equals(""))
			   {
			       enterwordedit.setText(reenterword);
			       enterword = reenterword;
			   }
		   }
		   if(handler_msg==0)
	    	{
	    		success_alert();
	    	}
	    	else if(handler_msg==1)
	    	{
	    		failure_alert();
	    	}
		   super.onRestoreInstanceState(savedInstanceState);
	    }
	 class submitoncheckedlistener implements android.widget.RadioGroup.OnCheckedChangeListener
	 {

		public submitoncheckedlistener() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			System.out.println("radiobutton");
			 RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		        	submitrdtext= checkedRadioButton.getText().toString().trim();
		        }
			System.out.println("submitrdtext="+submitrdtext);
		}
		 
	 }
	 class oncheckedlistener implements OnCheckedChangeListener{
			public int id =0;
			public TextView spmod,vcmod;
			public CheckBox cbmod;
			
			public oncheckedlistener(int position, CheckBox cbsubmodule, TextView spinsubmodule, TextView vcsubmodule) {
				// TODO Auto-generated constructor stub
				id=position;
				cbmod=cbsubmodule;
				spmod=spinsubmodule;
				vcmod=vcsubmodule;
				
			}

			

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				System.out.println("checkbox");
				effadap.notifyDataSetChanged();			
				if(isChecked)
				{
					if(cf.arrsub[id].equals("Crawlspace")||cf.arrsub[id].equals("Basement") || cf.arrsub[id].equals("Electric Meter Location")
							|| cf.arrsub[id].equals("Skylights") || (cf.arrsub[id].equals("Siding/Surface Materials")
									&& getmodulename.equals("Exterior")))
					{

						String[] strr = null;
						if(cf.arrsub[id].equals("Crawlspace"))
						{
							strr = crawlspace;
						}
						else if(cf.arrsub[id].equals("Basement"))
						{
							strr = basement;
						}
						else if(cf.arrsub[id].equals("Electric Meter Location"))
						{
							strr = electricmeter;
						}
						else if(cf.arrsub[id].equals("Siding/Surface Materials"))
						{
							strr = extsiding;
						}
						else if(cf.arrsub[id].equals("Skylights"))
						{
							strr = skylights;
						}
						showcrawl(cbmod,id,strr);
					}
					else
					{
						System.out.println("otherelse");
							try
							{
								System.out.println("cf.arrsub[id]"+cf.arrsub[id]);
								Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"'", null);
					            int cnt = cur.getCount();
					            if(cnt>0)
					            {
					            	cur.moveToFirst();
									String defaultval =db.decode(cur.getString(cur.getColumnIndex("fld_description")));
									//System.out.println("defaultval="+defaultval);
									if(defaultval.equals(""))
									{
										spmod.setBackgroundResource(R.drawable.btn_dropdown_disabled);
										vcmod.setTextColor(Color.parseColor("#C0C0C0"));
										vcmod.setText(Html.fromHtml("Add/Edit Condition"));
										value[id]="checked";
										try
										{
											db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET fld_NA='1' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"'");
										}
										catch (Exception e) {
											// TODO: handle exception
										}
									}
					               else
					               {
					            	
					            	AlertDialog.Builder popupDialog = new AlertDialog.Builder(LoadSubModules.this);
					            	popupDialog.setTitle("Override...");
					            	popupDialog.setMessage("If you click yes, you will override the data saved to NA.\nDo you want to proceed?");
					            	popupDialog.setIcon(R.drawable.ic_launcher);
					            	popupDialog.setPositiveButton("Yes",
									        new DialogInterface.OnClickListener() {
									            public void onClick(DialogInterface dialog, int which) {
									               
									            	spmod.setBackgroundResource(R.drawable.btn_dropdown_disabled);
									            	vcmod.setTextColor(Color.parseColor("#C0C0C0"));
													vcmod.setText(Html.fromHtml("Add/Edit Condition"));
													cbmod.setChecked(true);
													value[id]="checked";
													try
													{
														db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET fld_NA='1',fld_description='' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"'");
														Display();
														effadap.notifyDataSetChanged();
														 modulelist.setSelection(id);
													}
													catch (Exception e) {
														// TODO: handle exception
													}
									            	 dialog.cancel();
									            }
									        });
									
					            	popupDialog.setNegativeButton("No",
									        new DialogInterface.OnClickListener() {
									            public void onClick(DialogInterface dialog, int which) {
									                // Write your code here to execute after dialog
									            	cbmod.setChecked(false);
									                dialog.cancel();
									            }
									        });
									 
					            	popupDialog.show();
					            }
							 }
							}
							catch (Exception e) {
								// TODO: handle exception
							}
					}
					
				
				}
				else
				{
					try
					{
						db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET fld_NA='0' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"'");
						//System.out.println("UPDATE "+db.SaveSubModule+ " SET fld_NA='0' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname+"' and fld_submodule='"+db.encode(cf.arrsub[id]+"' and fld_srid='"+getph+"'");
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					
					spmod.setBackgroundResource(R.drawable.btn_dropdown_normal);
					vcmod.setTextColor(Color.parseColor("#3F00FF"));
					vcmod.setText(Html.fromHtml("<u>Add/Edit Condition</u>"));
					value[id]="unchecked";
					
					if(chkbool)
					{
						chkbool=false;
						Boolean flag=true;
						if(arrstrmodule[id].equals("Siding/Surface Materials") && getmodulename.equals("Exterior"))
						{
							flag=false;
						}
						if(!arrstrmodule[id].equals("Basement")&&!arrstrmodule[id].equals("Crawlspace")  && !arrstrmodule[id].equals("Skylights")
								&& !arrstrmodule[id].equals("Electric Meter Location") && flag.equals(true))
						{
							Intent i;
							if(arrstrmodule[id].equals("Washing Machine Present") || arrstrmodule[id].equals("Dryer Present") || arrstrmodule[id].equals("Dishwasher Present") 
									|| arrstrmodule[id].equals("Disposal Present")|| arrstrmodule[id].equals("Oven Present")|| arrstrmodule[id].equals("Exhaust Fan System Present")
									|| arrstrmodule[id].equals("Refrigerator Present")|| arrstrmodule[id].equals("Microwave Present")|| arrstrmodule[id].equals("Trash Compactor Present")
									|| arrstrmodule[id].equals("Bread Warmer Present") ||  arrstrmodule[id].equals("Range top Present") ||  arrstrmodule[id].equals("Lawn Sprinkler system Present")
									|| arrstrmodule[id].equals("Water Softener Present") || arrstrmodule[id].equals("Water heater Present") || arrstrmodule[id].equals("Attic/House fan Present")
									|| arrstrmodule[id].equals("Door bell Present"))
							{
								i = new Intent(LoadSubModules.this,AppLoadShowDialog.class);
							}
							else if(arrstrmodule[id].equals("Pool Features") || arrstrmodule[id].equals("Pool Lighting")
									|| arrstrmodule[id].equals("Pool Equipment") || arrstrmodule[id].equals("Plumbing Systems")
									|| arrstrmodule[id].equals("Pool Pumping System")
									|| arrstrmodule[id].equals("Pool Cleaning Equipment") || arrstrmodule[id].equals("Water Treatment")
									|| arrstrmodule[id].equals("Water Heating Systems") || arrstrmodule[id].equals("Timers/Clocks/Controls")
									|| arrstrmodule[id].equals("Electrical Service") || arrstrmodule[id].equals("Pool Cover(s)")
									|| arrstrmodule[id].equals("Safety Equipment")|| arrstrmodule[id].equals("Water Chemistry Test")
									|| arrstrmodule[id].equals("General Comments/Observations"))
							{
								i = new Intent(LoadSubModules.this,LoadPoolShowDialog.class);
								i.putExtra("currentview", "start");
							}
							else
							{
							     i = new Intent(LoadSubModules.this,LoadShowDialog.class);
							     i.putExtra("currentview", "start");
							}
							System.out.println("loadshowdialogends="+gettempname+"newid="+id);
							    i.putExtra("arrstr", arrstrmodule[id]);
							    i.putExtra("id",id);
								i.putExtra("templatename", gettempname);
								i.putExtra("modulename", getmodulename);
								i.putExtra("selectedid", getph);
							 	startActivityForResult(i, 101);
						}
					}
				}
				
			}

			
			
		
	 }
	 public Bitmap ShrinkBitmap(String string, int i, int j) {
			// TODO Auto-generated method stub
			  
				try {
					BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
					bmpFactoryOptions.inJustDecodeBounds = true;
				    bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);

					int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
							/ (float) j);
					int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
							/ (float) i);
	               
					if (heightRatio > 1 || widthRatio > 1) {
						if (heightRatio > widthRatio) {
							bmpFactoryOptions.inSampleSize = heightRatio;
						} else {
							bmpFactoryOptions.inSampleSize = widthRatio;
						}
					}

					bmpFactoryOptions.inJustDecodeBounds = false;
					bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);
					//System.out.println("bitmap="+bitmap);
					return bitmap;
				} catch (Exception e) {
				System.out.println("catch="+e.getMessage());
					return bitmap;
				}
				
				

		}

	 @Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
		
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				back();
				
			}

			return super.onKeyDown(keyCode, event);
		}		

	public void showcrawl(final CheckBox cbmod, final int id, final String[] strr) {
		// TODO Auto-generated method stub
		AlertDialog.Builder popupDialog = new AlertDialog.Builder(LoadSubModules.this);
    	popupDialog.setTitle("Override...");
    	popupDialog.setMessage("Are you sure you want to overwrite the saved data?");
    	popupDialog.setIcon(R.drawable.ic_launcher);
    	popupDialog.setPositiveButton("Yes",
		        new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		               
		            	
						cbmod.setChecked(true);
						try
						{
							db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET fld_NA='1',fld_description='' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_submodule='"+cf.arrsub[id]+"' and fld_srid='"+getph+"'");
							value[id]="checked";
							for(int i=0;i<strr.length;i++)
							{
								db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET fld_NA='1',fld_description='' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_submodule='"+strr[i]+"' and fld_srid='"+getph+"'");
							
							}
							Display();
							effadap.notifyDataSetChanged();
							 modulelist.setSelection(id);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
		            	 dialog.cancel();
		            }
		        });
		
    	popupDialog.setNegativeButton("No",
		        new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		                // Write your code here to execute after dialog
		            	cbmod.setChecked(false);
		                dialog.cancel();
		            }
		        });
		 
    	popupDialog.show();
	}
	
}
