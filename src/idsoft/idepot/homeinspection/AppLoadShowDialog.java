package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.ShowDialog.CHListener;
import idsoft.idepot.homeinspection.support.ArrayModuleList;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.util.ArrayList;




import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;


public class AppLoadShowDialog extends Activity{
	String arrstrname="",gettempname="",getmodulename="",str="",getph="",seltdval="",strother="",finalselstrval="",selectedval="",getcurretview="";
	DatabaseFunction db;
	CommonFunction cf;
	int dialog_other=0,getid=0;
	ArrayModuleList ar;
	LinearLayout lin;
	EditText etother;
    CheckBox[] ch;
    Button btnother;
    RadioGroup apprdg;
    View v;
    String apprdval="";
    static ArrayList<String> str_arrlst = new ArrayList<String>();
    String[] appwasher={"240 Volts","120 Volts","Average Condition","Good Condition","Aged/Operable Condition","Combined With Dryer"},appdryer={"240 Volts","120 Volts",
    		           "Average Condition","Good Condition","Aged/Operable Condition","Vented To Exterior","Not Vented"},
    		           apprangetop={"Gas","Electric","Corning Top","Grill","Check Gas Stove Clearance from Combustibles","Average Condition","Good Condition","Aged/Operable Condition"},
    		   applawnsprinkler={"One Zone","Multiple Zones","Valves Not Located - Inspected","Home Owner Install? (Budget Repairs)","Rain Sensor(Not Tested)","Manual Controls","Programable Controls","Lawn Sprinkler System"},
    		   appatticfan={"1","2","3","4","Operated at time of inspection","Inoperable at time of inspection"},
    		   appdoorbell={"Operated at time of inspection","Inoperable at time of inspection"},
    		   appwatersoftener={"Operated at time of inspection","Inoperable at time of inspection","Beyond scope of inspection"},    		   
    		 appdishwasher={"Average Condition","Good Condition","Aged/Operable Condition"},appoven={"Electric","Gas","Self Contained","Cabinet Mounted",
    		"Self Cleaning(Not Tested)","Double Oven Present","Convection Oven Not Tested","Average Condition","Good Condition","Aged/Operable Condition"},
    		appexhaust={"Exhaust to Outside","Filtered Only System(Recirc)","Average Condition","Good Condition","Aged/Operable Condition"},
    		appref={"Frost Free","Ice Maker","Fountain","Ice Dispensor","Dispensor","Average Condition","Good Condition","Aged/Operable Condition"};
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		System.out.println("AppLoadShowDialog");
		    Bundle b = this.getIntent().getExtras();
			arrstrname = b.getString("arrstr");
			Bundle b1 = this.getIntent().getExtras();
			gettempname = b1.getString("templatename");
			Bundle b2 = this.getIntent().getExtras();
			getmodulename = b2.getString("modulename");
			Bundle b3 = this.getIntent().getExtras();
			getph = b3.getString("selectedid");
			Bundle b4 = this.getIntent().getExtras();
			getid = b4.getInt("id");
            Bundle b5 = this.getIntent().getExtras();
			getcurretview = b5.getString("currentview");
		    setContentView(R.layout.appcustomizedialog);
		    
		   
		    ar = new ArrayModuleList(this,getmodulename);
			cf = new CommonFunction(this);
			db = new DatabaseFunction(this);
			
			db.CreateTable(1);
			db.CreateTable(3);
			db.CreateTable(8);
			db.CreateTable(7);
			db.userid();
			
			Declaration();
			
	}

	private void Declaration() {
		// TODO Auto-generated method stub
		dbvalue();
		show_alert();

		 
		setvalue();
	}

	private void show_alert() {
		// TODO Auto-generated method stub
		lin = (LinearLayout)findViewById(R.id.chk);
		
		TextView hdr = (TextView)findViewById(R.id.header);
		hdr.setText(arrstrname);
		btnother = (Button)findViewById(R.id.addnew);
		apprdg = (RadioGroup)findViewById(R.id.rdg);
		apprdg.setOnCheckedChangeListener(new OnCheckedChangeListener() {					

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				 RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
				 boolean isChecked = checkedRadioButton.isChecked();
				 
				 if (isChecked)
				 {
					apprdval = checkedRadioButton.getText().toString().trim();
					 if(apprdval.equals("Yes"))
					 {
						 int len=0;String[] name = null;
						 
						 lin.setVisibility(v.VISIBLE);
						 btnother.setVisibility(v.VISIBLE);
						 
						 ch = new CheckBox[str_arrlst.size()];
						 lin.removeAllViews();
						 for(int i=0;i<str_arrlst.size();i++)
						 {
							 ch[i] = new CheckBox(AppLoadShowDialog.this);
							 ch[i].setText(str_arrlst.get(i));
							 ch[i].setTextColor(Color.parseColor("#000000"));
							 ch[i].setOnCheckedChangeListener(new CHListener(i));
							 lin.addView(ch[i]);
						 }
						 
					 }
					 else
					 {
						 lin.setVisibility(v.GONE);
						 btnother.setVisibility(v.GONE);
					 }
				 }
			}
		});

		Button btnok = (Button) findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				seltdval="";
				if(apprdval.equals(""))
				{
					 if(arrstrname.equals("Washing Machine Present"))
					 {
						 cf.DisplayToast("Please select Washing Maching Present option");
					 }
					 else if(arrstrname.equals("Range top Present"))
					 {
						 cf.DisplayToast("Please select Range top Present option");
					 }
					 else if(arrstrname.equals("Lawn Sprinkler system Present"))
					 {
						 cf.DisplayToast("Please select Lawn Sprinkler system Present option");
					 }
					 else if(arrstrname.equals("Water Softener Present"))
					 {
						 cf.DisplayToast("Please select Water Softener Present option");
					 }
					 else if(arrstrname.equals("Water heater Present"))
					 {
						 cf.DisplayToast("Please select Water heater Present option");
					 }
					 else if(arrstrname.equals("Attic/House fan Present"))
					 {
						 cf.DisplayToast("Please select Attic/House fan Present option");
					 }
					 else if(arrstrname.equals("Door bell Present"))
					 {
						 cf.DisplayToast("Please select Door bell Present option");
					 }
					 else if(arrstrname.equals("Dryer Present"))
					 {
						 cf.DisplayToast("Please select Dryer Present option");
					 }
					 else if(arrstrname.equals("Dishwasher Present"))
					 {
						 cf.DisplayToast("Please select Dishwasher Present option");
					 }
					 else if(arrstrname.equals("Disposal Present"))
					 {
						 cf.DisplayToast("Please select Disposal Present option");
					 }
					 else if(arrstrname.equals("Oven Present"))
					 {
						 cf.DisplayToast("Please select Oven Present option");
					 }
					 else if(arrstrname.equals("Exhaust Fan System Present"))
					 {
						 cf.DisplayToast("Please select Exhaust Fan System Present option");
					 }
					 else if(arrstrname.equals("Refrigerator Present"))
					 {
						 cf.DisplayToast("Please select Refrigerator Present option");
					 }
					 else if(arrstrname.equals("Microwave Present"))
					 {
						 cf.DisplayToast("Please select Microwave Present option");
					 }
					 else if(arrstrname.equals("Trash Compactor Present"))
					 {
						 cf.DisplayToast("Please select Trash Compactor Present option");
					 }
					 
					 
					 else if(arrstrname.equals("Bread Warmer Present"))
					 {
						 cf.DisplayToast("Please select Bread Warmer Present option");
					 }
				}
				else
				{
					if(apprdval.equals("No"))
					{
						    Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"'", null);
						
							//Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+gettempname+"'", null);
							if (cur.getCount() > 0) {
								db.hi_db.execSQL("UPDATE "
										+ db.SaveSubModule
										+ " SET fld_description='No'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'");
							}
							else
							{
								db.hi_db.execSQL(" INSERT INTO "
										+ db.SaveSubModule
										+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES"
										+ "('"+db.UserId+"','"+getph+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','No','"+db.encode(gettempname)+"','0')");
							}
							cf.DisplayToast("Added successfully");
							back();
					}
					else
					{
								for (int i = 0; i < str_arrlst.size(); i++) {
									if (ch[i].isChecked()) {
										seltdval += ch[i].getText().toString() + "&#44;";
									}
								}
							
								finalselstrval = "";
								finalselstrval = seltdval;
								if(finalselstrval.equals(""))
								{
									cf.DisplayToast("Please check at least any 1 option to save");
								}
								else
								{
									try
									{
										//Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"'", null);
										Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"'", null);
										if (cur.getCount() > 0) 
										{
											db.hi_db.execSQL("UPDATE "
													+ db.SaveSubModule
													+ " SET fld_description='"
													+ db.encode(finalselstrval)
													+ "'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'");
										}
										else
										{
											db.hi_db.execSQL(" INSERT INTO "
													+ db.SaveSubModule
													+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES"
													+ "('"+db.UserId+"','"+getph+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(finalselstrval)+"','"+db.encode(gettempname)+"','0')");
										}
										cf.DisplayToast("Added successfully");
										back();
										
									}
									catch (Exception e) {
										// TODO: handle exception
									}
								}
					}
				}
				
				
			}
		});
		
		Button btnvc = (Button) findViewById(R.id.setvc);
		btnvc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("loadshowdialog="+getph+gettempname);
				Intent intent = new Intent(AppLoadShowDialog.this, LoadNewSetVC.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", "create");			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("submodulename", arrstrname);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("selectedid", getph);			
				intent.putExtras(b6);
				startActivity(intent);
				finish();
				
			}
		});
		
		Button btncancel = (Button) findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("insid eback");
				back();
				
			}
		});
		
		
		
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=1;
				dialogshow();
				
				
			}
		});
	}
	class CHListener implements OnCheckedChangeListener, android.widget.CompoundButton.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		System.out.println("seltdvaonCheckedChangedl"+seltdval);
			if(ch[clkpod].isChecked())
			{
				//seltdval += ch[clkpod].getText().toString() + "&#44;";
				
				
			}
		}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	private void setvalue() {
		// TODO Auto-generated method stub
		Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getph+"'", null);
	System.out.println("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'"+cur.getCount());
		if (cur.getCount() > 0) {
			cur.moveToFirst();
			String str = db.decode(cur.getString(cur.getColumnIndex("fld_description")));
			System.out.println("str"+str);
			if(str.equals("No"))
			{
				((RadioButton) apprdg.findViewWithTag("No")).setChecked(true);
			}
			else
			{
				System.out.println("what is str="+str);
				if(!str.trim().equals(""))
				{
					((RadioButton)apprdg.findViewWithTag("yes")).setChecked(true);
					cf.setValuetoCheckbox(ch, str);
				}	
			}
		}
	}
	protected void dialogshow() {
	// TODO Auto-generated method stub
	cf.hideskeyboard();
	final Dialog add_dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
	add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	add_dialog.setCancelable(false);
	add_dialog.getWindow().setContentView(R.layout.addnewcustom);
	
	etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
	cf.hidekeyboard(etother);
	etother.addTextChangedListener(new CustomTextWatcher(etother));
	
	Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
	btncancel.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog_other=0;
			add_dialog.dismiss();
		}
	});
	
	Button btnsave = (Button) add_dialog.findViewById(R.id.save);
	btnsave.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (etother.getText().toString().trim().equals("")) {
				cf.DisplayToast("Please enter the other text");
				
			} else {
				if(str_arrlst.contains(etother.getText().toString().trim()))
				{
					cf.DisplayToast("Already exists!! Please enter the new text");
					etother.setText("");
					etother.requestFocus();
				}
				else
				{
					try
					{
						db.hi_db.execSQL(" INSERT INTO "
								+ db.SaveAddOther
								+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
								+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+arrstrname+"','"
								+ db.encode(etother.getText().toString()) + "','"+gettempname+"')");
						
						String addedval = db.encode(etother.getText().toString());
						str_arrlst.add(addedval);
						dialog_other=0;
						cf.DisplayToast("Added successfully");
						add_dialog.cancel();
						
						lin.removeAllViews();
						dbvalue();
						show();
					
						
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		}
	});
	
	
	add_dialog.setCancelable(false);
	add_dialog.show();
}



	private void dbvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		 if(arrstrname.equals("Washing Machine Present"))
		 {
			 for(int j=0;j<appwasher.length;j++)
				{
					str_arrlst.add(appwasher[j]);
				}	
		 }
		 else if(arrstrname.equals("Dryer Present"))
		 {
			 for(int j=0;j<appdryer.length;j++)
				{
					str_arrlst.add(appdryer[j]);
				}	
		 }
		 else if(arrstrname.equals("Range top Present"))
		 {
			
			 for(int j=0;j<apprangetop.length;j++)
				{
					str_arrlst.add(apprangetop[j]);
				}	
		 }
		 else if(arrstrname.equals("Lawn Sprinkler system Present"))
		 {
			 for(int j=0;j<applawnsprinkler.length;j++)
				{
					str_arrlst.add(applawnsprinkler[j]);
				}	
		 }
		 else if(arrstrname.equals("Water Softener Present") || arrstrname.equals("Water heater Present"))
		 {
			 for(int j=0;j<appwatersoftener.length;j++)
				{
					str_arrlst.add(appwatersoftener[j]);
				}	
		 }
		 else if(arrstrname.equals("Attic/House fan Present"))
		 {
			 for(int j=0;j<appatticfan.length;j++)
				{
					str_arrlst.add(appatticfan[j]);
				}	
		 }
		 else if(arrstrname.equals("Door bell Present"))
		 {
			 for(int j=0;j<appdoorbell.length;j++)
				{
					str_arrlst.add(appdoorbell[j]);
				}	
		 }
		 else if(arrstrname.equals("Dishwasher Present")|| arrstrname.equals("Disposal Present")|| arrstrname.equals("Microwave Present")
				 || arrstrname.equals("Trash Compactor Present") || arrstrname.equals("Bread Warmer Present"))
		 {
			 for(int j=0;j<appdishwasher.length;j++)
				{
					str_arrlst.add(appdishwasher[j]);
				}	
		 }
		 else if(arrstrname.equals("Oven Present"))
		 {
			 for(int j=0;j<appoven.length;j++)
				{
					str_arrlst.add(appoven[j]);
				}	
		 }
		 else if(arrstrname.equals("Exhaust Fan System Present"))
		 {
			 for(int j=0;j<appexhaust.length;j++)
				{
					str_arrlst.add(appexhaust[j]);
				}	
		 }
		 else if(arrstrname.equals("Refrigerator Present"))
		 {
			 for(int j=0;j<appref.length;j++)
				{
					str_arrlst.add(appref[j]);
				}	
		 }
		 
		 try
			{
				 Cursor c1=db.hi_db.rawQuery("select * from " + db.AddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_module='"+db.encode(getmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
				 
				 System.out.println("dd"+c1.getCount());
				 if(c1.getCount()>0)
				 {
					 if (c1 != null) {
						 c1.moveToFirst();
							do {
							    String defvalue =db.decode(c1.getString(c1.getColumnIndex("fld_other")));
							    str_arrlst.add(defvalue);
							    Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_templatename='"+gettempname+"'", null);
								if(cur.getCount()==0)
		    		            {
		    		            	db.hi_db.execSQL(" INSERT INTO "
											+ db.SaveAddOther
											+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
											+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+arrstrname+"','"
											+ db.encode(defvalue) + "','"+gettempname+"')");
		    		            	
		    		            }
		    		            else
		    		            {
		    		            	 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'",null);
		    						 if(c.getCount()>0)
		    						 {
		    							 c.moveToFirst();
		    							 for(int i =0;i<c.getCount();i++,c.moveToNext())
		    							 {
		    								 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
		    							 }
		    						 }
		    		            }
							
							}while (c1.moveToNext());
						}
				 }
				 else
				 {
					 System.out.println("test="+str_arrlst.size());
					 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'",null);
					 if(c.getCount()>0)
					 {
						 c.moveToFirst();
						 for(int i =0;i<c.getCount();i++,c.moveToNext())
						 {
							 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
						 }
					 }
				 }
			}catch (Exception e) {
				// TODO: handle exception
			}	
		
	}
	private void show() {
		// TODO Auto-generated method stub
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(arrstrname)+"'", null);
			if (cur.getCount() > 0) {
				cur.moveToFirst();
				for (int i = 0; i < cur.getCount(); i++, cur.moveToNext()) {
					str = db.decode(cur.getString(cur
							.getColumnIndex("fld_description")));
	                
				}
				
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ch = new CheckBox[str_arrlst.size()];
		
		for (int i = 0; i < str_arrlst.size(); i++) {
        		ch[i] = new CheckBox(this);
				ch[i].setText(str_arrlst.get(i));
				ch[i].setTextColor(Color.BLACK);
				ch[i].setOnCheckedChangeListener(new CHListener(i));
				lin.addView(ch[i]);
				
		}
		if (str != "") {
				cf.setValuetoCheckbox(ch, str);
		}
			
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}

	private void back() {
		// TODO Auto-generated method stub
				
		Intent i = new Intent(AppLoadShowDialog.this,LoadSubModules.class);						
		i.putExtra("templatename", gettempname);
		i.putExtra("selectedid", getph);
		i.putExtra("modulename", getmodulename);
		i.putExtra("id", getid);
		i.putExtra("currentview", getcurretview);
		System.out.println("templatename"+gettempname+"getph"+getph+"getmodulename"+getmodulename);
		setResult(101,i);
		finish();
	}

	public void onSaveInstanceState(Bundle outState)
	{		 
        outState.putInt("other_index", dialog_other);
        outState.putString("radoi_value", apprdval);
       if(apprdval.equals("Yes"))
       {
    	   selectedval="";
	       	for (int i = 0; i < str_arrlst.size(); i++) {
	   			if (ch[i].isChecked()) {
	   				selectedval += ch[i].getText().toString() + "&#44;";
	   			}
	   		}
	       	
       }outState.putString("checkbox_index", selectedval);   
       	     
    	
        if(dialog_other==1)
        {
          outState.putString("othertext_index", etother.getText().toString());
        }
        super.onSaveInstanceState(outState);
    }
   protected void onRestoreInstanceState(Bundle savedInstanceState) {
	   dialog_other = savedInstanceState.getInt("other_index");
	   apprdval = savedInstanceState.getString("radoi_value");
	   if(dialog_other==1)
	   {
		   dialogshow();
		   strother = savedInstanceState.getString("othertext_index");
		   etother.setText(strother);		   
	   }
	   
	   if(apprdval.equals("No"))
	   {
		   ((RadioButton) apprdg.findViewWithTag("No")).setChecked(true);
		   lin.setVisibility(View.GONE);
	   }
	   else  if(apprdval.equals("Yes"))
	   {
		   ((RadioButton) apprdg.findViewWithTag("yes")).setChecked(true);
	   }
	   
	   seltdval = savedInstanceState.getString("checkbox_index");
	   if(!seltdval.equals(""))
	   {
		   cf.setValuetoCheckbox(ch, seltdval);
	   }
	   super.onRestoreInstanceState(savedInstanceState);
    }
}
