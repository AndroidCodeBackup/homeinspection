package idsoft.idepot.homeinspection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.idepot.homeinspection.NewSetVC.EfficientAdapter;
import idsoft.idepot.homeinspection.NewSetVC.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;



import android.app.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


public class LoadNewSetVC extends Activity{
	String gettempname="",getcurrentview="",getmodulename="",getsubmodulename="",str_vccond="",str_edtid="",
			str_cbval="",chksel="",getph="";
	TextView tv_seltemplate,tv_modulename,tv_header,tv_submodulename;
	Button btnback,btnhome,btnsubmit;
	ListView vclist;
	Cursor cur1;
	CommonFunction cf;
	DatabaseFunction db;
	Webservice_config wb;
	int vcedit_index=0,newid=0,vcchk=0;
	Bitmap bitmap;
	Progress_staticvalues p_sv;
	EditText etvc;
	String error_msg="",title="";
	String[] vcarr,arrstr_vccond,value,cbval,arrstrchk,selarr,arrstreditid;
	private int handler_msg=2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b4 = this.getIntent().getExtras();
		getsubmodulename = b4.getString("submodulename");
		Bundle b5 = this.getIntent().getExtras();
		getph = b5.getString("selectedid");
		System.out.println("loadnewsetvc="+getph);
		setContentView(R.layout.activity_setvc);
		
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		wb = new Webservice_config(this);
		
		db.CreateTable(1);
		db.CreateTable(10);
		db.CreateTable(11);
		db.userid();
		
		
		
		tv_seltemplate = (TextView)findViewById(R.id.seltemplate);
		tv_seltemplate.setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
		tv_modulename = (TextView)findViewById(R.id.modulename);
		tv_modulename.setText("Module Name : "+getmodulename);
		vclist = (ListView)findViewById(R.id.vclist);
		tv_submodulename = (TextView)findViewById(R.id.submodulename);
		tv_submodulename.setText(getsubmodulename);
		//tv_header = (TextView)findViewById(R.id.header);
		//tv_header.setText(" Start Inspection - Visible Conditions ");
		
		btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoadNewSetVC.this,HomeScreen.class));
				finish();
			}
		});
	/*	btnsubmit = (Button)findViewById(R.id.btn_submit);
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				  if(cf.isInternetOn())
				  {
					   new Start_xporting().execute("");
				  }
				  else
				  {
					  cf.DisplayToast("Please enable your internet connection.");
					  
				  } 
			}
		});*/
		
		cf.GetVC(getmodulename,getsubmodulename);
		vcarr = cf.submodvcid;
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"'", null);
            int cnt = cur.getCount();System.out.println("cnt="+cnt+"select * from " + db.CreateEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'");
            if(cnt==0)
            {
            	System.out.println("inside etth");
            	Cursor cur1 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'", null);
            	System.out.println("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'"+"Waht is count="+cur1.getCount());
                int cnt1 = cur1.getCount();
                
                if(cnt1==0)
                {System.out.println("insert");
                	for(int i=0;i<vcarr.length;i++)
                	{
                		System.out.println("vcarr"+vcarr[i]);
                		if(!vcarr[i].equals("--Select--"))
                		{

                			System.out.println(" INSERT INTO "
		    							+ db.SaveEditVC
		    							+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_VCName,fld_templatename,fld_selected) VALUES"
		    							+ "('"+db.UserId+"','"+getph+"','"+db.encode(getmodulename)+"','"+db.encode(getsubmodulename)+"','"+db.encode(vcarr[i])+"','"
		    							+ db.encode(gettempname)+"','0')");
		                		db.hi_db.execSQL(" INSERT INTO "
		    							+ db.SaveEditVC
		    							+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_VCName,fld_templatename,fld_selected) VALUES"
		    							+ "('"+db.UserId+"','"+getph+"','"+db.encode(getmodulename)+"','"+db.encode(getsubmodulename)+"','"+db.encode(vcarr[i])+"','"
		    							+ db.encode(gettempname)+"','0')");
                		}
                	}
                }
            }
            else
            {
            	Cursor cur1 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'", null);
                System.out.println("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'"+cur1.getCount());
                cur.moveToFirst();
                if (cur != null) 
                {
                	 do 
                	 {
						    String defvalue =db.decode(cur.getString(cur.getColumnIndex("fld_VCName")));
						    String inc =cur.getString(cur.getColumnIndex("fld_selected"));
						    if(cur1.getCount()==0)
						    {
						    	db.hi_db.execSQL(" INSERT INTO "
	    							+ db.SaveEditVC
	    							+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_VCName,fld_templatename,fld_selected) VALUES"
	    							+ "('"+db.UserId+"','"+getph+"','"+db.encode(getmodulename)+"','"+db.encode(getsubmodulename)+"','"+db.encode(defvalue)+"','"
	    							+ db.encode(gettempname)+"','"+inc+"')");
						    }
						    else
						    {
						    		db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_selected='"+inc+"' WHERE fld_inspectorid='"+db.UserId+"' and" +
				            				" fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_VCName='"+db.encode(defvalue)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'");
				            }				    						    
                	  }while (cur.moveToNext());
               }
            }
			
            
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		LoadVC();
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="Visible Condition Submission";
			    p_sv.progress_Msg="Please wait..Your template has been submitting.";
		       
				startActivityForResult(new Intent(LoadNewSetVC.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		     
			 		Submit_VisibleConditions();
			 		Submit_Photos();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	 private void Submit_VisibleConditions() throws IOException, XmlPullParserException {
			
			db.userid();
			
			  try
	 		  {
	 			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_selected='1'", null);
		        System.out.println("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+getsubmodulename+"'");
	 			System.out.println("coun="+cur.getCount());
		        if(cur.getCount()>0)
		        {
		        	 cur.moveToFirst();	        	
		        	 for(int i=0;i<cur.getCount();i++,cur.moveToNext())
		        	 {
		        	 
		        	    SoapObject request = new SoapObject(wb.NAMESPACE,"Save_Editvc");
		                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
		      		    envelope.dotNet = true;
		      		    
		        		request.addProperty("sed_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
		         		request.addProperty("fld_inspectorid",db.UserId);
		         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
		         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
		         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
		         		request.addProperty("fld_VCName",db.decode(cur.getString(cur.getColumnIndex("fld_VCName"))));
		         		request.addProperty("fld_selected",cur.getInt(cur.getColumnIndex("fld_selected")));
		         		
		         		envelope.setOutputSoapObject(request);
		         		System.out.println("Save_Editvcrequest="+request);
		         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
		         		SoapObject response = null;
		                try
		                {
		                      androidHttpTransport.call(wb.NAMESPACE+"Save_Editvc", envelope);
		                      response = (SoapObject)envelope.getResponse();
		                      System.out.println("Save_Editvc "+response);
		                      
		                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
		                      if(response.getProperty("StatusCode").toString().equals("0"))
		              		  {
		                    	 try
		          				 {
		                    		 System.out.println("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"'");
		          					db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+getsubmodulename+"' and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"'");
		          				 }
		          				 catch (Exception e) {
		          					// TODO: handle exception
		          				 }
		                    	handler_msg = 0;
		                      	
		              		  }
		              		  else
		              		  {
		              			 error_msg = response.getProperty("StatusMessage").toString();
		              			 handler_msg=1;
		              			
		              		  }
		                }
		                catch(Exception e)
		                {
		                     e.printStackTrace();
		                }
		        	 }
		        }
	    	}
	 		catch (Exception e) {
				// TODO: handle exception
			}
	  
	   }
	 private void Submit_Photos() throws IOException, XmlPullParserException {			
			db.userid();			
			  try
	 		  {
	 			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+getsubmodulename+"'", null);
		        System.out.println("select * from " + db.SaveVCImage + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+getsubmodulename+"'");
	 			System.out.println("coun imahe="+cur.getCount());
		        if(cur.getCount()>0)
		        {
		        	 cur.moveToFirst();System.out.println("fDFDFdfd");	        	
		        	 for(int i=0;i<cur.getCount();i++,cur.moveToNext())
		        	 {
		        	 System.out.println("ddf");
		        	    SoapObject request = new SoapObject(wb.NAMESPACE,"Save_VCImage");
		                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 
		      		    envelope.dotNet = true;
		      		    
		        		request.addProperty("si_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
		         		request.addProperty("fld_inspectorid",db.UserId);System.out.println("what is ="+request);
		         		request.addProperty("fld_srid",db.decode(cur.getString(cur.getColumnIndex("fld_srid"))));
		         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
		         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
		         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));System.out.println("what is 333="+request);
		         		request.addProperty("fld_caption",db.decode(cur.getString(cur.getColumnIndex("fld_caption"))));
		         		request.addProperty("fld_vc",db.decode(cur.getString(cur.getColumnIndex("fld_vc"))));	         		
		         		
		         		
		         		String imagepath = db.decode(cur.getString(cur.getColumnIndex("fld_image")));System.out.println("homesi"+imagepath);
		         		
						if(!imagepath.equals(""))
						{
							File f = new File(imagepath);
			        		if (f.exists()) {
			        			try {
			        				if(bitmap!=null)
			    					{
			    						bitmap.recycle();
			    					}
			    					bitmap = cf.ShrinkBitmap(imagepath, 400, 400);
			    					if(bitmap!=null)							
			    					{
			    						ByteArrayOutputStream out = new ByteArrayOutputStream();
			    						bitmap.compress(CompressFormat.PNG, 100, out);
			    						byte[] raw = out.toByteArray();
			    						request.addProperty("fld_image", raw);
			    					}
			    					else
			    					{
			    						request.addProperty("fld_image","");
			    					}
								} catch (Exception e) {
									// TODO: handle exception
									
								}
							}
							else
							{
								request.addProperty("fld_image", "");
							}
						}
						else
						{
							request.addProperty("fld_image", "");
						}
		         		
		         		envelope.setOutputSoapObject(request);
		         		System.out.println("Save_VCImage="+request);
		         		MarshalBase64 marshal = new MarshalBase64();
		        		marshal.register(envelope);
		         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
		         		SoapObject response = null;
		                try
		                {
		                      androidHttpTransport.call(wb.NAMESPACE+"Save_VCImage", envelope);
		                      response = (SoapObject)envelope.getResponse();
		                      System.out.println("Save_VCImage "+response);
		                      
		                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
		                      if(response.getProperty("StatusCode").toString().equals("0"))
		              		  {
		                    	 try
		          				 {
		                    		 System.out.println("UPDATE "+db.SaveVCImage+ " SET fld_sendtoserver='"+response.getProperty("si_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_srid='"+getph+"'");
		          					db.hi_db.execSQL("UPDATE "+db.SaveVCImage+ " SET fld_sendtoserver='"+response.getProperty("si_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_srid='"+getph+"'");
		          				 }
		          				 catch (Exception e) {
		          					// TODO: handle exception
		          				 }
		                    	handler_msg = 0;		                      	
		              		  }
		              		  else
		              		  {
		              			 error_msg = response.getProperty("StatusMessage").toString();
		              			 handler_msg=1;
		              			
		              		  }
		                }
		                catch(Exception e)
		                {
		                     e.printStackTrace();
		                }
		        	 }
		        }
	    	}
	 		catch (Exception e) {
				// TODO: handle exception
			}
	  
	   }
	 public void failure_alert() {
			// TODO Auto-generated method stub
	      
			AlertDialog al = new AlertDialog.Builder(LoadNewSetVC.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					handler_msg = 2 ;
				}
			}).setTitle(title+" Failure").create();
			al.setCancelable(false);
			al.show();
		}

		public void success_alert() {
			// TODO Auto-generated method stub
	       
			AlertDialog al = new AlertDialog.Builder(LoadNewSetVC.this).setMessage("You've successfully submitted your Inspection.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
					handler_msg = 2 ;
					startActivity(new Intent(LoadNewSetVC.this,HomeScreen.class));
					finish();
					
					
					
				}
			}).setTitle(title+" Success").create();
			al.setCancelable(false);
			al.show();
		}
	private void LoadVC() {
		// TODO Auto-generated method stub
		
		str_vccond="";chksel="";str_cbval="";
		try
		{
			cur1 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_VCName!='--Select--'", null);
			System.out.println("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_VCName!='--Select--'");
            int cnt1 = cur1.getCount();System.out.println("savedit="+cnt1);
            if(cnt1==0)
            {
            	Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'", null);
                int cnt = cur.getCount();
                if(cnt>0)
                {
                	cur.moveToFirst();
                	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
                	{
                		str_vccond += db.decode(cur.getString(cur.getColumnIndex("fld_VCName"))) + "~";
                		chksel += cur.getString(cur.getColumnIndex("fld_selected")) + "~";
                		
                		str_cbval += "unchecked"+"~";
                		
                		/*if(chksel.equals("1"))
                		{
                		    str_cbval += "checked"+"~";
                		}
                		else
                		{
                			str_cbval += "unchecked"+"~";
                		}*/
                		//str_cbval += "unchecked"+"~";
                		System.out.println("str_vccond="+str_vccond);
                		arrstr_vccond = str_vccond.split("~");
                	
                		arrstrchk = chksel.split("~");
                		cbval = str_cbval.split("~");
                		
                		vclist.setAdapter(new EfficientAdapter(cbval,arrstrchk,this));
                		vclist.setVisibility(View.VISIBLE);
                    	((TextView)findViewById(R.id.novc)).setVisibility(View.GONE);
                	}
                }
                else
                {
                	vclist.setVisibility(View.GONE);
                	((TextView)findViewById(R.id.novc)).setVisibility(View.VISIBLE);
                }
            }
            else
            {
            	cur1.moveToFirst();
            	for(int i=0;i<cur1.getCount();i++,cur1.moveToNext())
            	{
            		str_vccond += db.decode(cur1.getString(cur1.getColumnIndex("fld_VCName"))) + "~";
            		chksel += cur1.getString(cur1.getColumnIndex("fld_selected")) + "~";
            		str_edtid += cur1.getString(cur1.getColumnIndex("sed_Id")) + "~";
            		System.out.println("des"+cur1.getString(cur1.getColumnIndex("fld_selected")));
            		if(cur1.getString(cur1.getColumnIndex("fld_selected")).equals("1"))
            		{
            		    str_cbval += "checked"+"~";
            		}
            		else
            		{
            			str_cbval += "unchecked"+"~";
            		}
            		
            		System.out.println("str_vccondted="+str_vccond);
            		arrstr_vccond = str_vccond.split("~");
            		arrstreditid = str_edtid.split("~");
            		arrstrchk = chksel.split("~");
            		cbval = str_cbval.split("~");
            		
            		vclist.setAdapter(new EfficientAdapter(cbval,arrstrchk,this));
            		vclist.setVisibility(View.VISIBLE);
                	((TextView)findViewById(R.id.novc)).setVisibility(View.GONE);
            	}
            }
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
		
		public EfficientAdapter(String[] cbval, String[] arrstrchk, Context context) {
			mInflater = LayoutInflater.from(context);
			value=cbval;
			selarr = arrstrchk;
		}

		public int getCount() {
			int arrlen = 0;
			arrlen = arrstr_vccond.length;
			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			

					    holder = new ViewHolder();
						convertView = mInflater.inflate(R.layout.customvclist, null);						
						holder.cbvcname = (CheckBox)convertView.findViewById(R.id.cb_vcname);
						holder.tveditvc = (TextView)convertView.findViewById(R.id.tv_editvc);
						holder.tveditvc.setTextColor(Color.parseColor("#3F00FF"));
						holder.tveditvc.setText(Html.fromHtml("<u>Edit Visible Condition</u>"));
						
						convertView.setTag(holder);
						if(selarr[position].equals("1"))
						 {
							 holder.cbvcname.setChecked(true);
						 }
						 else
						 {
							 holder.cbvcname.setChecked(false);
						 }
						
						
						/*System.out.println("pos="+value[position]);
					     if(value[position].equals("checked"))
						 {
							 holder.cbvcname.setChecked(true);
						 }
						 else
						 {
							 holder.cbvcname.setChecked(false);
						 }
						*/
						
						 holder.cbvcname.setOnCheckedChangeListener(new oncheckedlistener(position,holder.cbvcname,holder.tveditvc));
						 holder.tveditvc.setOnClickListener(new vcclicklistener(position,holder.cbvcname,holder.tveditvc));
						/* if(value[position].equals("checked"))
						 {
							 holder.cbvcname.setChecked(true);
						 }
						 else
						 {
							 holder.cbvcname.setChecked(false);
						 }*/
						 
							
						if (arrstr_vccond[position].contains("null")||arrstrchk[position].contains("null") || arrstreditid[position].contains("null")
								||cbval[position].contains("null")) {
							arrstr_vccond[position] = arrstr_vccond[position].replace("null", "");
							arrstrchk[position] = arrstrchk[position].replace("null", "");
							cbval[position] = cbval[position].replace("null", "");
							arrstreditid[position] = arrstreditid[position].replace("null","");
							holder.cbvcname.setText(Html.fromHtml(arrstr_vccond[position]));
						
						}
						else
						{
							holder.cbvcname.setText(Html.fromHtml(arrstr_vccond[position]));
							
						}
						
			
			return convertView;
		}

		 class ViewHolder {
			TextView tveditvc;
			CheckBox cbvcname;
		   
		}

	}
	class vcclicklistener implements OnClickListener{
		public int id =0;
		public TextView tvmod;
		public CheckBox cbmod;

		public vcclicklistener(int position, CheckBox cbvcname,
				TextView tveditvc) {
			// TODO Auto-generated constructor stub
			id=position;
			cbmod=cbvcname;
			tvmod=tveditvc;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			vcedit_index=1;
			editvcdialogshow(id);
			
		}
		
	}
	class oncheckedlistener implements OnCheckedChangeListener {
		public int id =0;
		public TextView tvmod;
		public CheckBox cbmod;
		
		public oncheckedlistener(int position, CheckBox cbsubmodule, TextView spinsubmodule) {
			// TODO Auto-generated constructor stub
			id=position;
			cbmod=cbsubmodule;
			tvmod=spinsubmodule;
		
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			newid=id;
			final String vcname = arrstr_vccond[newid];
			final String editid = arrstreditid[newid];System.out.println("editid"+editid);
			if(isChecked && vcchk==0)
			{
				
				try
				{
					db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_selected='1' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_VCName='"+db.encode(vcname)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				System.out.println("getphtest+"+getph);
				    Intent intent = new Intent(LoadNewSetVC.this, LoadSaveVC.class);
					Bundle b1 = new Bundle();
					b1.putString("templatename", gettempname);			
					intent.putExtras(b1);
					Bundle b2 = new Bundle();
					b2.putString("modulename", getmodulename);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("submodulename", getsubmodulename);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("currentview", getcurrentview);			
					intent.putExtras(b4);System.out.println("getph="+getph);
					Bundle b5 = new Bundle();
					b5.putString("selectedid", getph);			
					intent.putExtras(b5);
					Bundle b7 = new Bundle();
					b7.putString("selectedvc",vcname);			
					intent.putExtras(b7);
					Bundle b8 = new Bundle();
					b8.putString("vceditid",editid);			
					intent.putExtras(b8);
					startActivity(intent);
					finish();
			}
			else if(!isChecked)
			{
				System.out.println("idelse="+id);
				AlertDialog.Builder builder = new AlertDialog.Builder(LoadNewSetVC.this);
			    builder.setTitle("Pick option")
			           .setItems(R.array.pickoption, new DialogInterface.OnClickListener() {
			               public void onClick(DialogInterface dialog, int which) {
			               // The 'which' argument contains the index position
			               // of the selected item
			            	  
			            	   if(which==0)
			            	   {System.out.println("getphedit="+getph+"editid"+editid);
			            		    Intent intent = new Intent(LoadNewSetVC.this, LoadSaveVC.class);
				   					Bundle b1 = new Bundle();
				   					b1.putString("templatename", gettempname);			
				   					intent.putExtras(b1);
				   					Bundle b2 = new Bundle();
				   					b2.putString("modulename", getmodulename);			
				   					intent.putExtras(b2);
				   					Bundle b3 = new Bundle();
				   					b3.putString("submodulename", getsubmodulename);			
				   					intent.putExtras(b3);
				   					Bundle b4 = new Bundle();
				   					b4.putString("currentview", getcurrentview);			
				   					intent.putExtras(b4);
				   					Bundle b5 = new Bundle();
				   					b5.putString("selectedid", getph);			
				   					intent.putExtras(b5);
				   					Bundle b7 = new Bundle();
				   					b7.putString("selectedvc", vcname);			
				   					intent.putExtras(b7);
				   					Bundle b8 = new Bundle();
									b8.putString("vceditid",editid);			
									intent.putExtras(b8);				   					
				   					startActivity(intent);
				   					finish();
			            	   }
			            	   else
			            	   {
			            		  
			            		   AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoadNewSetVC.this);
			                       alertDialogBuilder.setTitle("Delete");
			                       alertDialogBuilder
			                               .setMessage("Un-checking this condition will delete all comments attached to the specified Visible condition. Do you want to remove this condition?")
			                               .setCancelable(false)
			                               .setPositiveButton("Ok",
			                                       new DialogInterface.OnClickListener() {
			                                           public void onClick(DialogInterface dialog,
			                                                   int id) {
			                                           	
			                       							try
			                       							{
			                       								
			                       								db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_selected='0' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_VCName='"+db.encode(vcname)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'");
			                       							}
			                       							catch (Exception e) {
			                       								// TODO: handle exception
			                       							}
			                       							
			                       							try {
			                       								db.hi_db.execSQL("DELETE FROM " + db.SaveSetVC
			                       										+ " WHERE fld_inspectorid ='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' and fld_primary='"+db.encode(vcname)+"' and fld_submodule='"+getsubmodulename+"'");
			                       							} catch (Exception e) {

			                       							}
			                       							dialog.cancel();
			                                              
			                                           }
			                                       })
			                               .setNegativeButton("Cancel",
			                                       new DialogInterface.OnClickListener() {
			                                           public void onClick(DialogInterface dialog,
			                                                   int i1d) {
			                                        	   vcchk=1;
			                                           	 cbmod.setChecked(true);
			                                           	
			                                           	 dialog.cancel();vcchk=0;
			                                           }
			                                       });
			                       AlertDialog alertDialog = alertDialogBuilder.create();
			                       alertDialog.show();
			            	   }
			            	  
			           }
			    });
			builder.create();
			builder.show();

				
	        }
			
		}
	
    }

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	public void editvcdialogshow(int id) {
		// TODO Auto-generated method stub
		
		newid=id;
		final String vcname = arrstr_vccond[newid];
		final Dialog add_dialog = new Dialog(LoadNewSetVC.this);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.editcustomvc);
		
		etvc = (EditText)add_dialog.findViewById(R.id.et_addnew);
		etvc.setText(arrstr_vccond[newid]);
		
		etvc.addTextChangedListener(new CustomTextWatcher(etvc));
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				add_dialog.dismiss();vcedit_index=0;
			}
		});
		
		Button btnsave= (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(etvc.getText().toString().trim().equals(""))
				{
					cf.DisplayToast("Please enter the condition");
				}
				else
				{
					
					try
					{
						System.out.println("UPDATE "+db.SaveEditVC+ " SET fld_VCName='"+db.encode(etvc.getText().toString())+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+getmodulename+"' and fld_VCName='"+db.encode(vcname)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'");
						db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_VCName='"+db.encode(etvc.getText().toString())+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+getmodulename+"' and fld_VCName='"+db.encode(vcname)+"' and fld_submodule='"+db.encode(getsubmodulename)+"'");
						
					    Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(vcname)+"'", null);
					    System.out.println("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(vcname)+"'"+cur.getCount());
			            int cnt = cur.getCount();
			            if(cnt>0)
			            {
			            	System.out.println("UPDATE "+db.SaveSetVC+ " SET fld_primary='"+db.encode(etvc.getText().toString())+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+getmodulename+"' and fld_primary='"+db.encode(vcname)+"' and fld_submodule='"+getsubmodulename+"'");
					           db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_primary='"+db.encode(etvc.getText().toString())+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+getmodulename+"' and fld_primary='"+db.encode(vcname)+"' and fld_submodule='"+getsubmodulename+"'");
			            }
						cf.DisplayToast("Condition updated successfully");
						add_dialog.dismiss();vcedit_index=0;
						LoadVC();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					
						/*try
						{
							db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_VCName='"+db.encode(etvc.getText().toString())+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' and fld_VCName='"+db.encode(vcname)+"' and fld_submodule='"+getsubmodulename+"'");
							cf.DisplayToast("Condition updated successfully");
							add_dialog.dismiss();vcedit_index=0;
							LoadVC();
						}
						catch (Exception e) {
							// TODO: handle exception
						}*/
				
				}
				
				
			}
		});
		
		add_dialog.show();
	
	}
	
	 @Override
	    protected void onSaveInstanceState(Bundle outState) {
		  outState.putInt("editvc_index", vcedit_index);
		  outState.putInt("editid_index", newid);
		  if(vcedit_index==1)
		  {
		    outState.putString("editvccomments_index", etvc.getText().toString());
		  }
		 super.onSaveInstanceState(outState);
	 
	 }
	 @Override
	    protected void onRestoreInstanceState(Bundle savedInstanceState) {
		 vcedit_index = savedInstanceState.getInt("editvc_index");
		 newid = savedInstanceState.getInt("editid_index");
		 if(vcedit_index==1)
		 {
			 editvcdialogshow(newid);
			 String etvccomments = savedInstanceState.getString("editvccomments_index");
			 etvc.setText(etvccomments);
		 }
		 
	 }
	private void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(LoadNewSetVC.this, LoadSubModules.class);
		Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("selectedid", getph);			
		intent.putExtras(b5);
		startActivity(intent);
		finish();
		
		
	}
}
