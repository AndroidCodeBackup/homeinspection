package idsoft.idepot.homeinspection;


import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;

import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;


public class ReporttoIT extends Activity {
	CommonFunction cf;
	DatabaseFunction db;
String image_path="";
int image_count=0;
int handler_msg=0,k=0;
String error_msg="";
Webservice_config wb;
String selected[];
Progress_staticvalues p_sv;
ImageView im[]=new ImageView[3];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_to_it);
		db=new DatabaseFunction(this);
		cf=new CommonFunction(this);
		wb=new Webservice_config(this);		
		db.userid();
		if(!db.UserId.equals(""))
		{
			((EditText)findViewById(R.id.email_id)).setText(db.UserEmail);
			
		}

		im[0]=(ImageView) findViewById(R.id.it_image_1);
		im[1]=(ImageView) findViewById(R.id.it_image_2);
		im[2]=(ImageView) findViewById(R.id.it_image_3);
		cf.getDeviceDimensions();
		set_height_width();System.out.println("getwithheight");
		System.out.println("ttttttttttt");
	}
private void set_height_width() {
		// TODO Auto-generated method stub
	int width=450;
	
	if(cf.wd<450)
		width=cf.wd;	
	LinearLayout.LayoutParams lp =(LayoutParams) im[0].getLayoutParams();
	lp.width=width/3;
	lp.height=width/3;
	im[0].setLayoutParams(lp);
	lp =(LayoutParams) im[1].getLayoutParams();
	lp.width=width/3;
	lp.height=width/3;
	im[1].setLayoutParams(lp);
	lp =(LayoutParams) im[2].getLayoutParams();
	lp.width=width/3;
	lp.height=width/3;
	im[2].setLayoutParams(lp);
	System.out.println("DSDDDDDDDDDD");
	}
public void clicker(View v)
{
	switch (v.getId()) {
	case R.id.it_remove:
		findViewById(R.id.it_remove).setVisibility(View.GONE);
		image_path="";
		im[0].setImageBitmap(null);
		im[1].setImageBitmap(null);
		im[2].setImageBitmap(null);
		im[0].setVisibility(View.INVISIBLE);
		im[1].setVisibility(View.INVISIBLE);
		im[2].setVisibility(View.INVISIBLE);
		findViewById(R.id.it_attachment).setVisibility(View.GONE);
	break;
	case R.id.it_back:
		startActivity(new Intent(ReporttoIT.this,HomeScreen.class));
		finish();
	break;
	case R.id.it_clear:
		findViewById(R.id.it_remove).setVisibility(View.GONE);
		image_path="";
		im[0].setImageBitmap(null);
		im[1].setImageBitmap(null);
		im[2].setImageBitmap(null);
		im[0].setVisibility(View.INVISIBLE);
		im[1].setVisibility(View.INVISIBLE);
		im[2].setVisibility(View.INVISIBLE);
		findViewById(R.id.it_attachment).setVisibility(View.GONE);
		if(!db.UserId.equals(""))
		{
			((EditText)findViewById(R.id.email_id)).setText(db.UserEmail);
			
		}
		((EditText)findViewById(R.id.it_comments)).setText("");
	break;
	case R.id.it_send:
		
		if(!((EditText)findViewById(R.id.email_id)).getText().toString().trim().equals(""))
		{
			if(cf.eMailValidation(((EditText)findViewById(R.id.email_id)).getText().toString().trim()))
			{
				if(!((EditText)findViewById(R.id.it_comments)).getText().toString().trim().equals(""))
				{
					if(cf.isInternetOn()==true)
					  {
						new Start_xporting().execute("");
					  }
					  else
					  {
						  //finish();
						  cf.DisplayToast("Internet Connection is not available.");
						  
					  } 
				}
				else
				{
					cf.DisplayToast("Please enter Comments.");
				}
			}
			else
			{
				cf.DisplayToast("Please enter valid From Address.");
			}
		}
		else
		{
			cf.DisplayToast("Please enter From Address.");
		}		
	break;
	case R.id.it_attach:
		System.out.println(" comes correct");
		Intent i = new Intent(ReporttoIT.this,Select_phots.class);
		i.putExtra("Total_Maximumcount", 3);
		i.putExtra("Maximumcount", 0);
		if(!image_path.equals(""))
		{
		String path[]=image_path.split(",");
		i.putExtra("Selectedvalue", path);
			if(path.length<3)
			{
				startActivityForResult(i, 121);
			}
			else
			{
				cf.DisplayToast("You can add only three images");
			}
		}
		else
		{
			startActivityForResult(i, 121);
		}
    break;

	default:
		break;
	}
}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	// TODO Auto-generated method stub
	super.onActivityResult(requestCode, resultCode, data);
	if(resultCode == RESULT_OK)
	{
		 
		if(requestCode == 121)
		{
		findViewById(R.id.it_attachment).setVisibility(View.VISIBLE);
		image_path="";
		selected=data.getExtras().getStringArray("Selected_array");System.out.println("selected"+selected.length);
		image_count=selected.length;
		int width=450;
		
		if(cf.wd<450)
			width=cf.wd;
		for(int i=0;i<selected.length;i++)
		{
			image_path+=selected[i]+",";System.out.println("DSD"+selected[i]);
			cf.getDeviceDimensions();
			im[i].setImageBitmap(cf.ShrinkBitmap(selected[i], width-40/3, width-40/3));
			
			im[i].setVisibility(View.VISIBLE);
			
		}
		if(image_path.contains(","))
		{
			image_path=image_path.substring(0, image_path.length()-1);
			///((TextView)findViewById(R.id.it_attach)).setText("You have selected "+image_count+" images click here to edit selection");
			findViewById(R.id.it_remove).setVisibility(View.VISIBLE);
			
		}
		}
		}
	
}
class Start_xporting extends AsyncTask <String, String, String> {

    @Override
    public void onPreExecute() {
        p_sv.progress_live=true;
        p_sv.progress_title="Reporting to IT";
        p_sv.progress_Msg="Please wait we are submitting all IT issues you experience to our IT support for review. It may take few minutes.";
        startActivityForResult(new Intent(ReporttoIT.this,Progress_dialogbox.class), Static_variables.progress_code);
      
      
      super.onPreExecute();
    }
 @Override
    protected String doInBackground(String... aurl) {

        	try {
        		
        		  p_sv.progress_live=true;
    		      p_sv.progress_title="SENDING FEEDBACK TO IT TEAM";
    		      p_sv.progress_Msg="Please wait we are sending your feedback to our IT team, it may take few minutes.";
    		      sendinfo();
				//handler_msg=1;
        	//	p_sv.progress_toast="Sent feedback to it team.";
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				handler_msg=0;
				
				p_sv.progress_toast="Downloaded Failed";
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				handler_msg=0;
				e.printStackTrace();
				p_sv.progress_toast="Downloaded Failed";
				handler_msg=0;
			}
        	
                return aurl[0];
    }

    
	private void sendinfo() throws IOException, XmlPullParserException {
	// TODO Auto-generated method stub
		 String deviceId="",model= "",manuf = "",devversion= "",apiLevel= "",date= "";
		SoapObject request = new SoapObject(wb.NAMESPACE,"SendReportToIT");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		String version_name="0";
		try {
			version_name = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			deviceId = Settings.System.getString(getContentResolver(), Settings.System.ANDROID_ID);
			model = android.os.Build.MODEL;
			manuf = android.os.Build.MANUFACTURER;
			devversion = android.os.Build.VERSION.RELEASE;
			apiLevel = android.os.Build.VERSION.SDK;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy:HHmmss");
			date =sdf.format(new Date());
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			
		}
		
		request.addProperty("InspectorID",db.UserId);
		request.addProperty("ApplicationVersion",version_name);
		request.addProperty("DeviceName",manuf+" model="+model);
		request.addProperty("APILevel",apiLevel);
		request.addProperty("InspectorName",db.Userfirstname);
		request.addProperty("Comments",((EditText) findViewById(R.id.it_comments)).getText().toString().trim());
		request.addProperty("ContactNumber",db.UserPhone);
		request.addProperty("ContactEmail",((EditText) findViewById(R.id.email_id)).getText().toString().trim());
		request.addProperty("ReportDate",date);
		System.out.println("imagepath="+image_path);
		if(!image_path.equals(""))
		{	
			String[] path=image_path.split(",");
			Bitmap bm;
			if(path.length>0)
			{
				bm=cf.ShrinkBitmap(path[0], 400, 400);
	 			if(bm!=null)							
				{
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					bm.compress(CompressFormat.PNG, 100, out);
					byte[] raw = out.toByteArray();
					request.addProperty("imgByte1", raw);
				}else
				{
					request.addProperty("imgByte1", "");
				}
				request.addProperty("ImageNameWithExtension1",path[0].substring(path[0].lastIndexOf("/")+1));
			}
			else
			{
				request.addProperty("imgByte1","");
				request.addProperty("ImageNameWithExtension1","");
				request.addProperty("imgByte2","");
				request.addProperty("ImageNameWithExtension2","");
				request.addProperty("imgByte3","");
				request.addProperty("ImageNameWithExtension3","");
			}
			if(path.length>1)
			{
				bm=cf.ShrinkBitmap(path[1], 400, 400);
	 			if(bm!=null)							
				{
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					bm.compress(CompressFormat.PNG, 100, out);
					byte[] raw = out.toByteArray();
					request.addProperty("imgByte2", raw);
				}else
				{
					request.addProperty("imgByte2", "");
				}
				request.addProperty("ImageNameWithExtension2",path[1].substring(path[1].lastIndexOf("/")+1));
			}
			else
			{
				
				request.addProperty("imgByte2","");
				request.addProperty("ImageNameWithExtension2","");
				request.addProperty("imgByte3","");
				request.addProperty("ImageNameWithExtension3","");
			}
			if(path.length>2)
			{
				bm=cf.ShrinkBitmap(path[2], 400, 400);
	 			if(bm!=null)							
				{
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					bm.compress(CompressFormat.PNG, 100, out);
					byte[] raw = out.toByteArray();
					request.addProperty("imgByte3", raw);
				}else
				{
					request.addProperty("imgByte3", "");
				}
				request.addProperty("ImageNameWithExtension3",path[2].substring(path[2].lastIndexOf("/")+1));
			}
			else
			{
				
				request.addProperty("imgByte3","");
				request.addProperty("ImageNameWithExtension3","");
			}
		}
		else
		{
		request.addProperty("imgByte1","");
		request.addProperty("ImageNameWithExtension1","");
		request.addProperty("imgByte2","");
		request.addProperty("ImageNameWithExtension2","");
		request.addProperty("imgByte3","");
		request.addProperty("ImageNameWithExtension3","");
		}
		
		envelope.setOutputSoapObject(request);
		System.out.println(" the request"+request);
		MarshalBase64 marshal = new MarshalBase64();
		marshal.register(envelope);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		androidHttpTransport.call(wb.NAMESPACE+"SendReportToIT",envelope);
		System.out.println(" the result sendreport"+envelope.getResponse());
	    String result1 = envelope.getResponse().toString();
		//SoapObject 	result1 = (SoapObject) envelope.getResponse();
		//System.out.println("result test"+result1);
	//	System.out.println("TEST="+result1.getProperty("Status").toString());
		
	    
	    
		System.out.println("result1="+result1);
		if(result1.equals("true"))
		{
			System.out.println("test111");
			handler_msg=1;
		}
		else
		{
			System.out.println("test222");
			handler_msg=0;
			//error_msg=result1.getProperty("Errormsg").toString();
		}
}
	protected void onProgressUpdate(String... progress) {
    }

    @Override
    protected void onPostExecute(String unused) {
    	p_sv.progress_live=false;
    	System.out.println("started called in post excuted"+handler_msg);
    	if(handler_msg==0 || handler_msg==2)
		{
    		k=2;
			failure();
    		
		}
		else
		{
			k=1;
               success();
			
		}
    	  
    }
}
 @Override
    protected void onSaveInstanceState(Bundle outState) {
	 System.out.println("imame="+image_path);
	   outState.putStringArray("photo_index", selected);
	   outState.putInt("kval",k);
       super.onSaveInstanceState(outState);
    }
 public void success() {
	// TODO Auto-generated method stub
	 AlertDialog al = new AlertDialog.Builder(ReporttoIT.this).setMessage("Thank you for submitting your IT issue. We will review this and get back to you as soon as possible.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override   
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				k=0;
				startActivity(new Intent(ReporttoIT.this,HomeScreen.class));
 			finish();
			}
		}).setTitle("Success").create();
		al.show();
}
public void failure() {
	// TODO Auto-generated method stub
	 AlertDialog al = new AlertDialog.Builder(ReporttoIT.this).setMessage("Sorry you have some problem in connecting server. Please try again.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override   
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//finish();
				k=0;
			}
		}).setTitle("Connection Failure").create();
		al.show();
}
@Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
	 selected = savedInstanceState.getStringArray("photo_index");
	 k = savedInstanceState.getInt("kval");
	 System.out.println("kvalout="+k);
	 if(k==2)
     {
   	  failure();
     }
     else if(k==1)
     {
   	   success();
     }
	 int width=450;
		
	/*	if(cf.wd<450)
			width=cf.wd;
		System.out.println("length"+selected.length);
	 for(int i=0;i<selected.length;i++)
		{
		 System.out.println("test"+selected[i]);
			image_path+=selected[i]+",";System.out.println("DSD"+selected[i]);
			cf.getDeviceDimensions();
			im[i].setImageBitmap(cf.ShrinkBitmap(selected[i], width-40/3, width-40/3));
			
			im[i].setVisibility(View.VISIBLE);System.out.println("testttt");
			
		}*/
        super.onRestoreInstanceState(savedInstanceState);
    }
 @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(ReporttoIT.this, HomeScreen.class);
			startActivity(i);
			finish();
			
		}

		return super.onKeyDown(keyCode, event);
	}
}