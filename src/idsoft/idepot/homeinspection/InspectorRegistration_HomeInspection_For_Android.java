package idsoft.idepot.homeinspection;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import android.app.Activity;

public class InspectorRegistration_HomeInspection_For_Android implements KvmSerializable
{
    public int Id;
    public String firstname;
    public String Description;
    
    public InspectorRegistration_HomeInspection_For_Android(){
    	
    }
    

    public InspectorRegistration_HomeInspection_For_Android(int categoryId, String name, String description) {
        
    	Id = categoryId;
    //	fld_userid = name;
        Description = description;
    }


    public Object getProperty(int arg0) {
        
        switch(arg0)
        {
        case 0:
            return Id;
        case 1:
            //return fld_userid;
        case 2:
            return Description;
        }
        
        return null;
    }

    public int getPropertyCount() {
        return 3;
    }

    public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo info) {
        switch(index)
        {
        case 0:
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "Id";
            break;
        case 1:
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "fld_userid";
            break;
        case 2:
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "Description";
            break;
        default:break;
        }
    }

    public void setProperty(int index, Object value) {
        switch(index)
        {
        case 0:
        	Id = Integer.parseInt(value.toString());
            break;
        case 1:
        //	fld_userid = value.toString();
            break;
        case 2:
            Description = value.toString();
            break;
        default:
            break;
        }
    }
}
