package idsoft.idepot.homeinspection;


import idsoft.idepot.homeinspection.LoadModules.EfficientAdapter;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import org.w3c.dom.Text;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ReportPreview extends Activity {

LinearLayout li_content,li_values5;
String insp_name,insp_temp_id,table_name,current_m,policy_id,module_name,srid="",inspectorid="",str_module="";
Cursor c,c_val;
String arrstrmodule[],module[],submodule[];
int start=0;
int end=0;
DatabaseFunction db;
CommonFunction cf;
String modulename="";
View v;
LinearLayout li_values;
TextView txt1,txt2,txt3,txt4,txt5,txt6,txt7,txt8,txt9;

LinearLayout.LayoutParams lp1;
int Create=1;
public String[] strarr_module={"Structure","Exterior","Roofing","Garage/Carport","Plumbing System","Electrical System","HVAC",
		"Kitchen","Bathroom","Interior","Porches/Screens/Enclosures","Attic","Swimming Pools","Wells/Pump Equipment","Cabana/Pool House","Appliances and Systems","Visible Conditions"};
String sub_module_info[][];
LinearLayout li_ho;
Button cur_sel_b ;
int current_sub_pos;

Bitmap bm;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reportpreview);
		cf=new CommonFunction(this);
		li_ho=(LinearLayout) findViewById(R.id.sub_section_names);
		li_content=(LinearLayout) findViewById(R.id.pre_li_content);
		db= new DatabaseFunction(this);
		cf=new CommonFunction(this);
		
		
		db.userid();
		Bundle b =getIntent().getExtras();
		Create=1;
		if(b!=null)
		{
			srid=b.getString("srid");
			inspectorid = b.getString("inspectorid");		
		}
		Show_subsection_info();System.out.println("what is modulr="+modulename);
		if(modulename.equals(""))
		{
			cf.GetCurrentModuleName("Structure");
			showsubsection("Structure");
		}
		else
		{
			cf.GetCurrentModuleName(modulename);
			showsubsection(modulename);
		}
		
		
	}
private void Show_subsection_info() {
	// TODO Auto-generated method stub
	for(int i=0;i<strarr_module.length;i++)
	{
			Button bt=new Button(this,null,R.attr.n_header_text);
			bt.setTextColor(getResources().getColor(R.color.n_header_text_color));
			bt.setText(strarr_module[i]);
			bt.setId(i);
			cf.set_header_fonts(bt);
			li_ho.addView(bt,LayoutParams.WRAP_CONTENT,40);
			LayoutParams lp =(LayoutParams) bt.getLayoutParams();
			lp.setMargins(5, 0, 5, 0);
			bt.setLayoutParams(lp);
			if(i==0)
			{
				((TextView)findViewById(R.id.pre_tv_header)).setText(strarr_module[i]);
				bt.setTextColor(getResources().getColor(R.color.n_button_input_page_select_color));
				cur_sel_b=bt;
				current_sub_pos=i;
			}
			if(i+1!=strarr_module.length)
			{
				View v =new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.n_linesperator));
				li_ho.addView(v,1,LayoutParams.FILL_PARENT);
			}
			bt.setOnClickListener(new btn_click_listener(i));
			
	}
}
class btn_click_listener implements OnClickListener
{
	int pos;
	public btn_click_listener(int i)
	{
		pos=i;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		cur_sel_b.setTextColor(getResources().getColor(R.color.n_header_text_color));
		cur_sel_b=(Button) v;
		modulename = cur_sel_b.getText().toString();
		cur_sel_b.setTextColor(getResources().getColor(R.color.n_button_input_page_select_color));
		current_sub_pos=pos;
		((TextView)findViewById(R.id.pre_tv_header)).setText(modulename);
		cf.GetCurrentModuleName(modulename);
		showsubsection(modulename);
		
	}	


}
private void showsubsection(String modulename)
{
	li_content.removeAllViews();
	DisplayMetrics metrics = new DisplayMetrics();
	getWindowManager().getDefaultDisplay().getMetrics(metrics);
	int wd = metrics.widthPixels;
	int ht = metrics.heightPixels;			
	int totalwith = wd-30;
	int totalheight =  ht-150;
	totalwith = totalwith/4;
	int rem = wd-totalwith;
	int width = wd/2;
	
	
	ScrollView sv = new ScrollView(this);
	li_content.addView(sv);
	
	RelativeLayout.LayoutParams lpsub = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, totalheight);
	lpsub.setMargins(10,50,10,0);
	li_content.setLayoutParams(lpsub);			
	
	final LinearLayout lin1 = new LinearLayout(this);
	lin1.setOrientation(LinearLayout.VERTICAL);
	sv.addView(lin1);
	
	LinearLayout.LayoutParams lparams1 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
	lparams1.setMargins(5,5,5,5);	
	System.out.println("modulename"+modulename);
	
	if(modulename.equals("HVAC"))
	{
		 li_values = new LinearLayout(this);
			li_values.setLayoutParams(lparams1);
			lp1 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			lp1.setMargins(10, 10, 10,10);
			li_values.setOrientation(LinearLayout.VERTICAL);
			 lin1.addView(li_values);
		
		
		db.CreateTable(23);
		
		Cursor c= db.hi_db.rawQuery("select * from " + db.LoadHVAC + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+srid+"' ",null);
		if(c.getCount()>0)
		 {
			 c.moveToFirst();
			
			 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null); 
			 LinearLayout th1= (LinearLayout)h1.findViewById(R.id.heatinglist);th1.setVisibility(v.VISIBLE);
			 TableLayout.LayoutParams lp1 = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 lp1.setMargins(0, 0, 2, 0);
			 h1.removeAllViews();
			 li_values.addView(th1,lp1); 

			
			 for(int i=0;i<c.getCount();i++)
			 {
				 String hvactype="",hvactypeother="",energysource="",energysourceother="",gasshutoff="",gasshutoffother="",age="",fueltankloc="",fluetype="",
						 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",
						 furnacefeatuers="",supplyregister="",retunrregister="",fueltanklocation="",
								 noofzones="",zoneno="",zonename="",zonenameother="",blowermotor="",testedfor="",airhandlerfeatures="",
								 ducworktype="",multizonesystem="",tonnage="",tonnageother,btu,btuother,thermotype,thermolocation,
										 externalunit,internalunit,extunitreplprob,extunitreplprobother,intunitreplprob,intunitreplprobother,ampextunit,ampintunit,emerheatampdraw,supplytemp,returntemp,conddischarge;
				 
				 
				 zoneno=db.decode(c.getString(c.getColumnIndex("fld_zoneno")));System.out.println("zonen="+zoneno);
				 
				 zonename=db.decode(c.getString(c.getColumnIndex("fld_zonename")));
				 zonenameother=db.decode(c.getString(c.getColumnIndex("fld_otherzonename")));
				 hvactype=db.decode(c.getString(c.getColumnIndex("fld_hvactype")));
				 hvactypeother=db.decode(c.getString(c.getColumnIndex("fld_hvactypeother")));
				 energysource=db.decode(c.getString(c.getColumnIndex("fld_energysource")));
				 energysourceother=db.decode(c.getString(c.getColumnIndex("fld_energysourceother")));
				 gasshutoff=db.decode(c.getString(c.getColumnIndex("fld_gasshutoff")));
				 gasshutoffother=db.decode(c.getString(c.getColumnIndex("fld_gasshutoffother")));
				 fueltankloc=db.decode(c.getString(c.getColumnIndex("fld_fueltankloc")));
				 fluetype=db.decode(c.getString(c.getColumnIndex("fld_fluetype")));
				 age=db.decode(c.getString(c.getColumnIndex("fld_age")));
				 otherage=db.decode(c.getString(c.getColumnIndex("fld_otherage")));					 
				 heatsystembrand=db.decode(c.getString(c.getColumnIndex("fld_heatsystembrand")));
				 furnacetype=db.decode(c.getString(c.getColumnIndex("fld_furnacetype")));
				 heatdeliverytype=db.decode(c.getString(c.getColumnIndex("fld_heatdeliverytype")));				 
				 blowermotor=db.decode(c.getString(c.getColumnIndex("fld_blowermotor")));
				 testedfor=db.decode(c.getString(c.getColumnIndex("fld_testedfor")));					 
				 airhandlerfeatures=db.decode(c.getString(c.getColumnIndex("fld_airhandlerfeatures")));					 
				 tonnage=db.decode(c.getString(c.getColumnIndex("fld_tonnage")));
				 tonnageother=db.decode(c.getString(c.getColumnIndex("fld_tonnageother")));
				 btu=db.decode(c.getString(c.getColumnIndex("fld_btu")));					 
				 btuother=db.decode(c.getString(c.getColumnIndex("fld_btuother")));
				 
				 LinearLayout l2= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamicviewlist, null);
				 LinearLayout t = (LinearLayout)l2.findViewById(R.id.heatingvalue);
				 t.setVisibility(v.VISIBLE);
			     t.setId(100+i);
			     
			     TextView tvzoenno= (TextView) t.findViewWithTag("No");
			     tvzoenno.setText(zoneno);
			     
			     TextView tvzonename= (TextView) t.findViewWithTag("Zonename");
			     if(zonename.equals("--Select--"))
				 {
			    	 zonename="";
				 }
			     System.out.println("zoenname="+zonename);
			     if(zonename.equals("Other"))
				 {
			    	 tvzonename.setText(zonenameother);	
				 }
				 else
				 {
					 tvzonename.setText(zonename);
				 }
			     
			     TextView tvheattype= (TextView) t.findViewWithTag("hvactype");
			     if(hvactype.equals("Other"))
				 {
			    	 tvheattype.setText(hvactypeother);	
				 }
				 else
				 {
					 tvheattype.setText(hvactype);
				 }
			     
			     TextView tvenergsource= (TextView) t.findViewWithTag("energysource");
			     if(energysource.equals("--Select--"))
				 {
			    	 energysource="";
				 }
			     if(energysource.equals("Other"))
				 {
			    	 tvenergsource.setText(energysourceother);	
				 }
				 else
				 {
					 tvenergsource.setText(energysource);
				 }
			     
			     TextView tvgasshutoff= (TextView) t.findViewWithTag("gasshutoff");
			     if(gasshutoff.equals("--Select--"))
				 {
			    	 gasshutoff="";
				 }
			     if(gasshutoff.equals("Other"))
				 {
			    	 tvgasshutoff.setText(gasshutoffother);	
				 }
				 else
				 {
					 tvgasshutoff.setText(gasshutoff);
				 }
			     
			     
			     TextView tvfueltank= (TextView) t.findViewWithTag("fueltanklocation");
			     if(fueltankloc.equals("--Select--"))
				 {
			    	 fueltankloc="";
				 }
			     if(fueltankloc.equals("Other"))
				 {
			    	 tvfueltank.setText(gasshutoffother);	
				 }
				 else
				 {
					 tvfueltank.setText(fueltankloc);
				 }
			     
			     
			     TextView tvage= (TextView) t.findViewWithTag("age");
			     if(age.equals("--Select--"))
				 {
			    	 age="";
				 }
			     if(age.equals("Other"))
				 {
			    	 tvage.setText(otherage);	
				 }
				 else
				 {
					 tvage.setText(age);
				 }
									
				TextView tvbrand= (TextView) t.findViewWithTag("systembrand");
				if(heatsystembrand.equals("--Select--"))
				{
					heatsystembrand="";
				}
				tvbrand.setText(heatsystembrand);
				
				TextView tvfurnace= (TextView) t.findViewWithTag("furnacetype");
				if(furnacetype.equals("--Select--"))
				{
					furnacetype="";
				}
				tvfurnace.setText(furnacetype);
				
				TextView tvtestedfor= (TextView) t.findViewWithTag("testedfor");
				if(testedfor.equals("--Select--"))
				{
					testedfor="";
				}
				tvtestedfor.setText(testedfor);
				
				TextView tvheatdeliverytype= (TextView) t.findViewWithTag("heatdeliverytype");
				if(heatdeliverytype.equals("--Select--"))
				{
					heatdeliverytype="";
				}
				tvheatdeliverytype.setText(heatdeliverytype);
				
				TextView tvmotorblower= (TextView) t.findViewWithTag("blowermotor");
				if(blowermotor.equals("--Select--"))
				{
					blowermotor="";
				}
				tvmotorblower.setText(blowermotor);
				
				  TextView tvbtu= (TextView) t.findViewWithTag("btu");
				     if(btu.equals("--Select--"))
					 {
				    	 btu="";
					 }
				     if(btu.equals("Other"))
					 {
				    	 tvbtu.setText(btuother);	
					 }
					 else
					 {
						 tvbtu.setText(btu);
					 }
				     
				     TextView tvtonnage= (TextView) t.findViewWithTag("tonnage");
				     if(tonnage.equals("--Select--"))
					 {
				    	 tonnage="";
					 }
				     if(tonnage.equals("Other"))
					 {
				    	 tvtonnage.setText(tonnageother);	
					 }
					 else
					 {
						 tvtonnage.setText(tonnage);
					 }
				 
				 
				 TextView tvfluetype= (TextView) t.findViewWithTag("fluetype");
				 if(fluetype.equals("--Select--"))
				 {
					 fluetype="";
				 }
				 tvfluetype.setText(fluetype);
				 
				 TextView tvairhanlderfeat= (TextView) t.findViewWithTag("airhanlderfeat");
				 if(airhandlerfeatures.equals("--Select--"))
				 {
					 airhandlerfeatures="";
				 }
				 tvairhanlderfeat.setText(airhandlerfeatures);
				l2.removeAllViews();
				
				li_values.addView(t,lp1);
				c.moveToNext();
				
			 }
		 }
		else
		{/*
			 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_headernew, null); 
			 LinearLayout th1= (LinearLayout)h1.findViewById(R.id.heatinglist);th1.setVisibility(v.VISIBLE);
			 TableLayout.LayoutParams lp1 = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 
			 lp1.setMargins(0, 0, 2, 0);
			 h1.removeAllViews();
			 li_values.addView(th1,lp1); */
		}
		
		db.CreateTable(46);
		Cursor c1= db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+srid+"' ",null);
		if(c1.getCount()>0)
		 {
		 
			 c1.moveToFirst();
		
			 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null);
			 LinearLayout th= (LinearLayout)h1.findViewById(R.id.ductworkhdrlin);th.setVisibility(v.VISIBLE);
			 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 lp.setMargins(0, 0, 2, 0);
			 h1.removeAllViews();
			 li_values.addView(th,lp); 
			 
			 
			 for(int i=0;i<c1.getCount();i++)
			 {
				 String supplyregister="",retunrregister="",zoneno="",ducworktype="",multizonesystem="",comments="";
				 
				 zoneno=c1.getString(c1.getColumnIndex("fld_zoneno"));					 
				 supplyregister=db.decode(c1.getString(c1.getColumnIndex("fld_supplyregister")));
				 retunrregister=db.decode(c1.getString(c1.getColumnIndex("fld_retunrregister")));					 
				 ducworktype=db.decode(c1.getString(c1.getColumnIndex("fld_ducworktype")));
				 multizonesystem=db.decode(c1.getString(c1.getColumnIndex("fld_multizonesystem")));
				 comments=db.decode(c1.getString(c1.getColumnIndex("fld_ductworkcomment")));	
				 
				 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamicviewlist, null);
				 LinearLayout t = (LinearLayout)l1.findViewById(R.id.ductworklin);
				 t.setVisibility(v.VISIBLE);
			     t.setId(100+i);
			     
			 	TextView tvno= (TextView) t.findViewWithTag("No");
			 	tvno.setText(zoneno);				     
			     
			     
			     TextView tvductowrktype= (TextView) t.findViewWithTag("ductowrktype");
			     if(!ducworktype.equals(""))
				 {
			    	 tvductowrktype.setText(ducworktype);	
				 }			     
			     
			     TextView tvsupplyreg= (TextView) t.findViewWithTag("suppluregisterslocated");
			     if(!supplyregister.equals(""))
				 {
			    	 tvsupplyreg.setText(supplyregister);	
				 }
			     
			     TextView tvreturnreg= (TextView) t.findViewWithTag("returnregisterslcoated");
			     if(!retunrregister.equals(""))
				 {
			    	 tvreturnreg.setText(retunrregister);	
				 }
			     
			     TextView tvmultizonesystem= (TextView) t.findViewWithTag("multizonesystem");
			     if(!multizonesystem.equals(""))
				 {
			    	 tvmultizonesystem.setText(multizonesystem);	
				 }
			     
			     TextView tvcomments= (TextView) t.findViewWithTag("comments");
			     if(!comments.equals(""))
				 {
			    	 tvcomments.setText(comments);	
				 }

			
				l1.removeAllViews();
				li_values.addView(t,lp);
				c1.moveToNext();
				
			 }
		
		 } 
		 else
		 {
			 /*LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null);
			 LinearLayout th= (LinearLayout)h1.findViewById(R.id.ductworkhdrlin);th.setVisibility(v.VISIBLE);
			 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 lp.setMargins(0, 0, 2, 0);
			 h1.removeAllViews();
			 li_content.addView(th,lp); */
		 }
		 db.CreateTable(48);
		
		  	 Cursor  cur= db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
		  	 System.out.println("LoadSystembrands="+cur.getCount());
		   if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				
				 
				 LinearLayout h2= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null); 
				 LinearLayout th2= (LinearLayout)h2.findViewById(R.id.systembrandshdr);th2.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp2 = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp2.setMargins(0, 0, 2, 0);
				 h2.removeAllViews();			
				 li_values.addView(th2,lp2); 
				 
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String externalunit,internalunit,extunitreplprob,extunitreplprobother,intunitreplprob,intunitreplprobother,comments,
					 ampextunit,ampintunit,emerheatampdraw,supplytemp,returntemp,conddischarge,noofzones,zoneno, zonename,zonenameother;
					 
					System.out.println("mo of zones");
					 noofzones=cur.getString(cur.getColumnIndex("fld_noofzones"));System.out.println("DDD="+noofzones);
					 zoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));					 
					 zonename=cur.getString(cur.getColumnIndex("fld_zonename"));System.out.println("ddd"+zonename);
					 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_zonenameother")));
					 externalunit=db.decode(db.decode(cur.getString(cur.getColumnIndex("fld_externalunit"))));
					 internalunit=db.decode(cur.getString(cur.getColumnIndex("fld_internalunit")));					 
					 extunitreplprob=cur.getString(cur.getColumnIndex("fld_extunitreplprob"));
					 extunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_extunitreplprobother")));					 
					 intunitreplprob=cur.getString(cur.getColumnIndex("fld_intunitreplprob"));					 
					 intunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_intunitreplprobother")));
					 ampextunit=db.decode(cur.getString(cur.getColumnIndex("fld_ampextunit")));					 
					 ampintunit=db.decode(cur.getString(cur.getColumnIndex("fld_ampintunit")));
					 emerheatampdraw=db.decode(db.decode(cur.getString(cur.getColumnIndex("fld_emerheatampdraw"))));					 
					 supplytemp=db.decode(cur.getString(cur.getColumnIndex("fld_supplytemp")));
					 returntemp=db.decode(cur.getString(cur.getColumnIndex("fld_returntemp")));					 
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_systembrandcomments")));	
					
					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamicviewlist, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.systembrandslin);t.setVisibility(v.VISIBLE);
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				 	TextView tvno= (TextView) t.findViewWithTag("No");
					 if(zoneno.equals("--Select--"))
						{
						 zoneno="";
						}
					 tvno.setText(zoneno);	
				     
				   
				     
				     TextView tvzonename= (TextView) t.findViewWithTag("Zonename");
				     if(zonename.equals("--Select--"))
					 {
				    	 zonename="";
					 }
				     if(zonename.equals("Other"))
					 {
				    	 tvzonename.setText(zonenameother);	
					 }
					 else
					 {
						 tvzonename.setText(zonename);
					 }
				     
				     TextView tvnoofzones= (TextView) t.findViewWithTag("noofzones");
						if(noofzones.equals("--Select--"))
						{
							noofzones="";
						}
						tvnoofzones.setText(noofzones);
												
										
					
					 TextView tvexternalunit= (TextView) t.findViewWithTag("externalunitsn");
					 tvexternalunit.setText(externalunit);
					 TextView tvinternalunit= (TextView) t.findViewWithTag("internalunitsn");System.out.println("internalunit"+internalunit);
					 tvinternalunit.setText(internalunit);
					 
					 TextView tvexternalunitreppob= (TextView) t.findViewWithTag("externalunitrepprob");
					 if(extunitreplprob.equals("--Select--"))
					 {
						 extunitreplprob="";
					 }
					 tvexternalunitreppob.setText(extunitreplprob);System.out.println("extunitreplprob"+extunitreplprob);
					 
					 TextView tvinternalunitreppob= (TextView) t.findViewWithTag("internalunitrepprob");
					 if(intunitreplprob.equals("--Select--"))
					 {
						 intunitreplprob="";
					 }
					 tvinternalunitreppob.setText(intunitreplprob);System.out.println("intunitreplprob"+intunitreplprob);
					 
					
					 TextView tvampdrawextunit= (TextView) t.findViewWithTag("ampdrawextunit");
					 tvampdrawextunit.setText(ampextunit);System.out.println("ampextunit"+ampextunit);
					 TextView tvampdrawintunit= (TextView) t.findViewWithTag("ampdrawintunit");
					 tvampdrawintunit.setText(ampintunit);System.out.println("amo="+ampintunit);
					 TextView tvemerheatampdraw= (TextView) t.findViewWithTag("emerheatampdraw");
					 tvemerheatampdraw.setText(emerheatampdraw);System.out.println("emerheatampdraw"+emerheatampdraw);
					 TextView tvsupplytemp= (TextView) t.findViewWithTag("supplytemp");
					 tvsupplytemp.setText(supplytemp);System.out.println("supplytemp"+supplytemp);
					 TextView tvreturntemp= (TextView) t.findViewWithTag("returntemp");
					 tvreturntemp.setText(returntemp);
					 System.out.println("returntemp"+returntemp);
					 TextView tvductworkcomments= (TextView) t.findViewWithTag("comments");System.out.println("comments"+comments);
						
					 tvductworkcomments.setText(comments);
				     
							
					
					
					l1.removeAllViews();
					
					li_values.addView(t,lp2);
					cur.moveToNext();
					
				 }
			 }
		   else
		   {
			 /*  LinearLayout h3= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null); 
				 LinearLayout th3= (LinearLayout)h3.findViewById(R.id.systembrandshdr);th3.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp3 = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp3.setMargins(0, 0, 2, 0);
				 h3.removeAllViews();*/
		   }
		   db.CreateTable(48);
			
		  	 Cursor  c2= db.hi_db.rawQuery("select * from " + db.LoadThermostat + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
		  	 System.out.println("LoadThermostat="+c2.getCount());
		   if(c2.getCount()>0)
			 {
				 c2.moveToFirst();
				
				 System.out.println("geteare thn pe");
				 LinearLayout h2= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null); 
				 LinearLayout th2= (LinearLayout)h2.findViewById(R.id.thermostathdr);th2.setVisibility(v.VISIBLE);System.out.println("gfdgfg");
				 TableLayout.LayoutParams lp2 = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);System.out.println("fggg");
				 lp2.setMargins(0, 0, 2, 0);
				 h2.removeAllViews();			
				 li_values.addView(th2,lp2); System.out.println("gggg");
				 
				 for(int i=0;i<c2.getCount();i++)
				 {

					 String zoneno="",thermotypevalue="",thermolocation="",comments="";
					 zoneno=c2.getString(c2.getColumnIndex("fld_zoneno"));	System.out.println("zoneno"+zoneno);		 
					 thermotypevalue=db.decode(c2.getString(c2.getColumnIndex("fld_thermotype")));				System.out.println("e="+thermotypevalue);	
					 thermolocation=c2.getString(c2.getColumnIndex("fld_thermolocation"));
					 comments=db.decode(c2.getString(c2.getColumnIndex("fld_thermostatcomments")));
					
					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamicviewlist, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.thermostatlin);t.setVisibility(v.VISIBLE);
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     TextView tvzoneno= (TextView) t.findViewWithTag("zoneno");System.out.println("tvzoneno");
					 if(zoneno.equals("--Select--"))
						{
						 zoneno="";
						}
					 tvzoneno.setText(zoneno);						
										
					 TextView tvthermotype= (TextView) t.findViewWithTag("thermotype");System.out.println("thermotypevalue"+thermotypevalue);
					 if(thermotypevalue.equals("--Select--"))
					 {
						 thermotypevalue="";
					 }
					 tvthermotype.setText(thermotypevalue);
					 
					 TextView tvthermoloc= (TextView) t.findViewWithTag("thermolocation");
					 if(thermolocation.equals("--Select--"))
					 {
						 thermolocation="";
					 }
					 tvthermoloc.setText(thermolocation);
					 
					 TextView tvthermostatcomments= (TextView) t.findViewWithTag("comments");
						
					 tvthermostatcomments.setText(comments);System.out.println("comments");
				     
							
					
					
					l1.removeAllViews();
					
					li_values.addView(t,lp2);
					cur.moveToNext();
					
				 }
			 }
		   else
		   {
			   /*LinearLayout h3= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null); 
				 LinearLayout th3= (LinearLayout)h3.findViewById(R.id.systembrandshdr);th3.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp3 = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp3.setMargins(0, 0, 2, 0);
				 h3.removeAllViews();*/
		   }
		   
		   
		   
	}
	else if(modulename.equals("Bathroom"))
	{
		db.CreateTable(16);
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadBathroom + " where fld_inspectorid='"+inspectorid+"' and  fld_srid='"+srid+"'",null);
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst(); 
				 
				 
				 li_values = new LinearLayout(this);
					li_values.setLayoutParams(lparams1);
					lp1 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
					lp1.setMargins(10, 10, 10,10);
					li_values.setOrientation(LinearLayout.VERTICAL);
					 lin1.addView(li_values);
				 
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null);					
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.bathheaderlin);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(0, 0, 2, 0);
				 h1.removeAllViews();
				 li_values.addView(th,lp);

				 for(int i=0;i<cur.getCount();i++)
				 {
					 String bathno="",bathname="",bathnameother="",bathfixture="",tubtv="",sinkdb="",
					 showerdb="",othersafetyglass="",safetyglass="",toiletdb="",drainage="",otherdrainage="",bathother="",otherfeaturesdb="",comments="";
					 
					 
					 bathno=cur.getString(0);
					 bathname=cur.getString(cur.getColumnIndex("fld_bathroomname"));
					 bathnameother=db.decode(cur.getString(cur.getColumnIndex("fld_otherbathroomname")));
					 tubtv=db.decode(cur.getString(cur.getColumnIndex("fld_tubs")));
					 sinkdb=db.decode(cur.getString(cur.getColumnIndex("fld_sinks")));
					 showerdb=db.decode(cur.getString(cur.getColumnIndex("fld_showerenclosures")));
					 safetyglass=db.decode(cur.getString(cur.getColumnIndex("fld_safetyglass")));
					 othersafetyglass=db.decode(cur.getString(cur.getColumnIndex("fld_othersafetyglass")));
					 toiletdb=db.decode(cur.getString(cur.getColumnIndex("fld_toilets")));
					 bathfixture=db.decode(cur.getString(cur.getColumnIndex("fld_bathfixturematerials")));
					 drainage=db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainage")));
					 otherdrainage=db.decode(cur.getString(cur.getColumnIndex("fld_supplydrainageother")));				 
					 bathother=db.decode(cur.getString(cur.getColumnIndex("fld_bathother")));
					 otherfeaturesdb=db.decode(cur.getString(cur.getColumnIndex("fld_otherfeatures")));
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_bathcomments")));		
					 
					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamicviewlist, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.bath_listview);t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
					
				 	TextView tvno= (TextView) t.findViewWithTag("No");
				 	tvno.setText(String.valueOf(i+1));
					
				     
					TextView tvbath= (TextView) t.findViewWithTag("bathname");
					tvbath.setText(bathname);
					
					
					TextView tvbathfixtures= (TextView) t.findViewWithTag("bathfixtures");
					bathfixture= bathfixture.replace("&#44;",", ");
					if(bathfixture.endsWith(", "))
					{
						bathfixture = bathfixture.substring(0,bathfixture.length()-2);
					}
					tvbathfixtures.setText(bathfixture);
					
					TextView tvtubs= (TextView) t.findViewWithTag("tubs");
					tubtv= tubtv.replace("&#44;",", ");
					if(tubtv.endsWith(", "))
					{
						tubtv = tubtv.substring(0,tubtv.length()-2);
					}
					tvtubs.setText(tubtv);
					
					TextView tvsinks= (TextView) t.findViewWithTag("sinks");
					sinkdb= sinkdb.replace("&#44;",", ");
					if(sinkdb.endsWith(", "))
					{
						sinkdb = sinkdb.substring(0,sinkdb.length()-2);
					}
					tvsinks.setText(sinkdb);
					
					TextView tvshowerenclosures= (TextView) t.findViewWithTag("bathshower");
					if(!showerdb.equals(""))
					{
						showerdb= showerdb.replace("&#44;",", ");
						if(showerdb.endsWith(", "))
						{
							showerdb = showerdb.substring(0,showerdb.length()-2);
						}
						tvshowerenclosures.setText(showerdb);
					}
					
					TextView tvtoilet= (TextView) t.findViewWithTag("toilets");
					if(!toiletdb.equals(""))
					{
						toiletdb= toiletdb.replace("&#44;",", ");
						if(toiletdb.endsWith(", "))
						{
							toiletdb = toiletdb.substring(0,toiletdb.length()-2);
						}
						tvtoilet.setText(toiletdb);
						//tvtoilet.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Toilets</u></font>"));
					}
					
					
					TextView tvdrainage= (TextView) t.findViewWithTag("drainage");
					if(drainage.equals("--Select--"))
					{
						drainage="";
					}
					if(drainage.equals("Other"))
					{
						tvdrainage.setText(drainage+" ("+ otherdrainage +")");
					}
					else
					{
						tvdrainage.setText(drainage);	
					}
					
					TextView tvbathother= (TextView) t.findViewWithTag("bathother");
					if(bathother.equals("--Select--"))
					{
						bathother="";
					}
					tvbathother.setText(bathother);
					
					
					TextView tvglass= (TextView) t.findViewWithTag("safetyglass");
					if(safetyglass.equals("--Select--"))
					{
						safetyglass="";
					}
					if(safetyglass.equals("Other"))
					{
						tvglass.setText(safetyglass+" ("+ othersafetyglass +")");
					}
					else
					{
						tvglass.setText(safetyglass);	
					}
					TextView tvotherfeatures= (TextView) t.findViewWithTag("otherfeatures");
					if(!otherfeaturesdb.equals(""))
					{
						otherfeaturesdb= otherfeaturesdb.replace("&#44;",", ");
						if(otherfeaturesdb.endsWith(", "))
						{
							otherfeaturesdb = otherfeaturesdb.substring(0,otherfeaturesdb.length()-2);
						}
						tvotherfeatures.setText(tubtv);
						//tvotherfeatures.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Other Features</u></font>"));
					}
					
					TextView tvcomments= (TextView) t.findViewWithTag("comments");
					if(!comments.equals(""))
					{
						
						tvcomments.setText(comments);
						//tvcomments.setText(Html.fromHtml("<font color='#0000FF'><u>Click to View Comments</u></font>"));
					}
					l1.removeAllViews();
					
					li_values.addView(t,lp);
					cur.moveToNext();
					 
				 }
			 }
			 else
			 {
				/* LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.dynamiclistview_header, null);					
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.bathheaderlin);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(0, 0, 2, 0);
				 h1.removeAllViews(); 
				 li_content.addView(th,lp);*/
			 }
	}
	else if(modulename.equals("Visible Conditions"))
	{
		li_content.setMinimumWidth(totalwith);
		db.CreateTable(13);	
		for(int i=0;i<strarr_module.length;i++)
		{
			
			li_values = new LinearLayout(this);
			li_values.setMinimumHeight(30);
			li_values.setLayoutParams(lparams1);
			
			Cursor cur1 = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_srid='"+srid+"' and fld_module='"+strarr_module[i]+"'", null);
			if(cur1.getCount()>0)
			{
				lp1 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
				lp1.setMargins(10, 10, 10,10);
				li_values.setOrientation(LinearLayout.VERTICAL);
			    txt1= new TextView(this,null,R.attr.n_pre_layout_Text_view);
			    String source = "<b><font color=#8A0829>MODULE NAME : "+ strarr_module[i]+"</font></b>";
			    txt1.setText(Html.fromHtml(source));
			    lin1.addView(li_values);
				li_values.addView(txt1,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			
			cf.GetCurrentModuleName(strarr_module[i]);
			for(int k=0;k<cf.arrsub.length;k++)
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_srid='"+srid+"' and fld_module='"+strarr_module[i]+"' and fld_submodule='"+cf.arrsub[k]+"'", null);
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
					txt2= new TextView(this,null,R.attr.n_pre_layout_Text_view);
					String source1 = "<b><font color=#0B3B24>SUB MODULE NAME : "+ db.decode(cur.getString(cur.getColumnIndex("fld_submodule")))+"</font></b>";
				    txt2.setText(Html.fromHtml(source1));
					
		    		li_values.addView(txt2,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);	    		

		    		LinearLayout.LayoutParams lp5 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
					lp5.setMargins(0,10,0,10);
					txt2.setLayoutParams(lp5);
					
		    		for(int j=0;j<cur.getCount();j++)
					{
		    			String mod = db.decode(cur.getString(cur.getColumnIndex("fld_module")));
		    			String sub_mod = db.decode(cur.getString(cur.getColumnIndex("fld_submodule")));
		    			String primarycondition = db.decode(cur.getString(cur.getColumnIndex("fld_primary")));
		    					    			
		    			String secondarycondition = db.decode(cur.getString(cur.getColumnIndex("fld_secondary")));
		    			secondarycondition = Html.fromHtml(secondarycondition).toString();
		         		if(secondarycondition.endsWith(","))
		         		{
		         			secondarycondition = secondarycondition.substring(0,secondarycondition.length()-1);
		         		}
		         		
		    			String comments = db.decode(cur.getString(cur.getColumnIndex("fld_comments")));
		    			String inspectionpoint = db.decode(cur.getString(cur.getColumnIndex("fld_inspectionpoint")));
		    			inspectionpoint = Html.fromHtml(inspectionpoint).toString();
		         		if(inspectionpoint.endsWith(","))
		         		{
		         			inspectionpoint = inspectionpoint.substring(0,inspectionpoint.length()-1);
		         		}
		         		
		    			String approxrepaircost = db.decode(cur.getString(cur.getColumnIndex("fld_apprrepaircost")));
		    			String contractortype = db.decode(cur.getString(cur.getColumnIndex("fld_contractortype")));
		    			contractortype = Html.fromHtml(contractortype).toString();
		         		if(contractortype.endsWith(","))
		         		{
		         			contractortype = contractortype.substring(0,contractortype.length()-1);
		         		}		         		 
		         		//***Start showing the Primary Condition***//*
		         		
		         		LinearLayout li_overall = new LinearLayout(this);	         			
		         		li_overall.setOrientation(LinearLayout.VERTICAL);
		         		LinearLayout.LayoutParams loverall = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		loverall.setMargins(0, 20, 0,0);
		         		
		         		LinearLayout li_prim = new LinearLayout(this);	         			
		         		li_prim.setOrientation(LinearLayout.HORIZONTAL);
		         		
		         		TextView txtprimary= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtprimary.setText(Html.fromHtml("<b>Primary Condition</b>"));
		         		li_prim.addView(txtprimary,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtprimarycol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtprimarycol.setText(":");
		         		li_prim.addView(txtprimarycol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtprimaryvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtprimaryvalue.setText(primarycondition);
		         		li_prim.addView(txtprimaryvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		li_overall.addView(li_prim, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
		         		//***Start showing the Secondary Condition***//*
		         		
		         		LinearLayout li_sec = new LinearLayout(this);	         			
		         		li_sec.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtsec= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtsec.setText(Html.fromHtml("<b>Secondary Condition</b>"));
		         		li_sec.addView(txtsec,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtseccol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtseccol.setText(":");
		         		li_sec.addView(txtseccol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtsecvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(secondarycondition.equals("")){secondarycondition="N/A";}
		         		txtsecvalue.setText(secondarycondition);
		         		li_sec.addView(txtsecvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		li_overall.addView(li_sec, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
				    	
		         		
		         		//***Start showing the comments***//*
		         		
		         		
		         		LinearLayout li_comm = new LinearLayout(this);        			
		         		li_comm.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtcomm= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtcomm.setText(Html.fromHtml("<b>Comments</b>"));
		         		li_comm.addView(txtcomm,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtcommcol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtcommcol.setText(":");
		         		li_comm.addView(txtcommcol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtcommvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(comments.equals("")){comments="N/A";}
		         		txtcommvalue.setText(comments);
		         		li_comm.addView(txtcommvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		
		         		li_overall.addView(li_comm, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
		         		
		         		
		         		//***Start showing the inspectionpoint***//*
		         		
		         		
		         		LinearLayout li_insppoint = new LinearLayout(this);        			
		         		li_insppoint.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtinspoint= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtinspoint.setText(Html.fromHtml("<b>Inspection Point</b>"));
		         		li_insppoint.addView(txtinspoint,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtinspointcol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtinspointcol.setText(":");
		         		li_insppoint.addView(txtinspointcol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtinspvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(inspectionpoint.equals("")){inspectionpoint="N/A";}
		         		txtinspvalue.setText(inspectionpoint);
		         		li_insppoint.addView(txtinspvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		li_overall.addView(li_insppoint, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
		         		//***Start showing the approxmiate cost***//*
		         		
		         		
		         		LinearLayout li_approst = new LinearLayout(this);        			
		         		li_approst.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtappcost= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtappcost.setText(Html.fromHtml("<b>Approximate Cost</b>"));
		         		li_approst.addView(txtappcost,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtapprcol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtapprcol.setText(":");
		         		li_approst.addView(txtapprcol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtapprvalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(approxrepaircost.equals("")){approxrepaircost="N/A";}
		         		txtapprvalue.setText(approxrepaircost);
		         		li_approst.addView(txtapprvalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		li_overall.addView(li_approst, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		
		         		
		         		
		         		
		         		//***Start showing the contractor type***//*
		         		
		         		
		         		LinearLayout li_contype = new LinearLayout(this);        			
		         		li_contype.setOrientation(LinearLayout.HORIZONTAL);
		         		TextView txtcontype= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtcontype.setText(Html.fromHtml("<b>Contractor Type</b>"));
		         		li_contype.addView(txtcontype,totalwith-20,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtcontypecol= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		txtcontypecol.setText(":");
		         		li_contype.addView(txtcontypecol,10,LayoutParams.WRAP_CONTENT);
		         		
		         		TextView txtcontypevalue= new TextView(this,null,R.attr.n_pre_layout_Text_view);
		         		if(contractortype.equals("")){contractortype="N/A";}
		         		txtcontypevalue.setText(contractortype);
		         		li_contype.addView(txtcontypevalue,rem-45,LayoutParams.WRAP_CONTENT);
		         		
		         		li_overall.addView(li_contype, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		         		li_values.addView(li_overall);
		         		li_overall.setLayoutParams(loverall);
				   
	        			RelativeLayout li_values1 =new RelativeLayout(this);   			
	        		
		         		Cursor cur2 = db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+srid+"' and fld_module='"+mod+"' and fld_submodule='"+sub_mod+"' and fld_vc='"+db.encode(primarycondition)+"'", null);
		         		if(cur2.getCount()>0)
						{		         			
							cur2.moveToFirst();
							int im_id=0,cap_id=0;
							for(int m =0;m<cur2.getCount();m++,cur2.moveToNext())
							{
								 String imagename = db.decode(cur2.getString(cur2.getColumnIndex("fld_image")));
								 String caption = db.decode(cur2.getString(cur2.getColumnIndex("fld_caption")));
								 Bitmap bm=cf.ShrinkBitmap(imagename, 200, 200);
								 ImageView im=new ImageView(this,null,R.attr.n_pre_layout_imag);
								 im.setImageBitmap(bm);
								 li_values1.addView(im,200,200);
								 im.setId((i+m)+1);
								 System.out.println("im_id="+im.getId()+"what is ="+im_id); 
								 RelativeLayout.LayoutParams lp =(RelativeLayout.LayoutParams) im.getLayoutParams();
								 lp.setMargins(10, 45, 0, 0);
								 
								 if(im_id!=0)// Check the is there any previous image added for this question 
								 {
								
									 if((m%2)!=0 || m ==1) 
									 {
										 lp.addRule(RelativeLayout.BELOW,cap_id-1); // If yes mean we set to right of previous one
										 lp.addRule(RelativeLayout.RIGHT_OF, im_id);
									 }
									 else
									 {
										 lp.addRule(RelativeLayout.BELOW,cap_id);
									 }
								 }
								 System.out.println("totalwith"+totalwith);
								 
							 lp.width=width-40;
							 //lp.height=350;
							 im.setLayoutParams(lp);
							 im_id=im.getId();
							 TextView tv = new TextView(this,null,R.attr.n_pre_layout_Text_view);
							 tv.setGravity(Gravity.CENTER_HORIZONTAL);
							 tv.setText(caption);
							 System.out.println("totalwidth="+wd);
							 li_values1.addView(tv,200,LayoutParams.WRAP_CONTENT);
							 RelativeLayout.LayoutParams rl_p=(android.widget.RelativeLayout.LayoutParams) tv.getLayoutParams();
							 if(im_id!=0)
								 {
									rl_p.addRule(RelativeLayout.BELOW,im_id);
									if((m%2)!=0 || m ==1)
										rl_p.addRule(RelativeLayout.RIGHT_OF, im_id-1);
								 }
							 
							 tv.setId(1000+i+j);
							 tv.setLayoutParams(rl_p);
							 rl_p.width=width-40;
							 cap_id=im.getId();// We use this id to set the relative rule
							 tv.setLayoutParams(rl_p);
							 li_values1.setLayoutParams(lp);
							}
							li_values.addView(li_values1, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
						}	         		
		         			         		
				    	cur.moveToNext();
					}		    		
				}
			}
			}
			
		}		
	}
	else
	{		
			TextView txt1,txt_val,txt_colon;
	 				
			
			for(int i=0;i<cf.arrsub.length;i++)
			{
					li_values = new LinearLayout(this);
					li_values.setMinimumHeight(30);
					li_values.setOrientation(LinearLayout.HORIZONTAL);
					li_values.setLayoutParams(lparams1);					
					
					txt1= new TextView(this,null,R.attr.n_pre_layout_Text_view);
					txt1.setText(cf.arrsub[i]);
					lin1.addView(li_values);
					li_values.addView(txt1,totalwith,LayoutParams.WRAP_CONTENT);
					 
					 //***Start showing the inspected values***//*
					 	 txt_val= new TextView(this,null,R.attr.n_pre_layout_Text_view_value);
					 	 txt_colon= new TextView(this,null,R.attr.n_pre_layout_Text_view);
					 	try
					 	{
					 		String Sql="Select * from "+db.SaveSubModule+" where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"' and fld_module='"+db.encode(modulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'";
					 		Cursor	 c2 = db.hi_db.rawQuery(Sql, null);
						 	if(c2.getCount()>0)
						 	{
						 		c2.moveToFirst();
						 		String description = db.decode(c2.getString(c2.getColumnIndex("fld_description")));
						 		
						 		if(description.contains("&#44;"))
						 		{
						 			description = description.replace("&#44;",", ");			 			
						 		}
						 		if(description.endsWith(", "))
				 				{
						 			description = description.substring(0,description.length()-2);
				 				}
						 		if(description.equals(""))
				 				{
						 			description = "N/A";
				 				}
						 		txt_val.setText(description);	
						 	}
						 	else
						 	{
						 		txt_val.setText("N/A");
						 	}
							
					 	}
					 	catch (Exception e) {
							// TODO: handle exception
					 		
						}
					 	txt_colon.setText(":");
					 	txt_val.setMinimumWidth(rem);
					 	li_values.addView(txt_colon,10,LayoutParams.WRAP_CONTENT);
						LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						lp3.setMargins(5,0,5,0);
						txt_colon.setLayoutParams(lp3);
						
						li_values.addView(txt_val,rem,LayoutParams.WRAP_CONTENT);
						LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
						lp2.setMargins(5,0,5,0);
						txt_val.setLayoutParams(lp2);
			}
	
	}
}
private void Add_dynamic_values(LinearLayout li_values) {
	// TODO Auto-generated method stub
	try
	{
		Cursor c_par=db.hi_db.rawQuery("Select * from "+db.LoadHeating+ " WHERE fld_srid='"+srid+"' and fld_inspectorid='"+inspectorid+"'",null);
		if(c_par.getCount()>0)
		{
			c_par.moveToFirst();
			//tab_name=c_par.getString(c_par.getColumnIndex("In_D_value_table_name"));
			/**Start Create Layouts***/
				LinearLayout li_par =new LinearLayout(this);
				li_par.setOrientation(LinearLayout.VERTICAL);
				li_par.setPadding(1, 1, 1, 1);
				li_par.setBackgroundDrawable(getResources().getDrawable(R.drawable.n_preview_dynamic_values));
				li_values.addView(li_par, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
				
				LinearLayout li_sub = new LinearLayout(this);
				li_sub.setOrientation(LinearLayout.HORIZONTAL);
				li_par.addView(li_sub, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
				
				TextView no =new TextView(this,null,R.attr.n_pre_layout_Text_view);
				no.setText("No");
				no.setBackgroundDrawable(getResources().getDrawable(R.drawable.n_preview_header_dynamic));
				//c_header.moveToFirst();
				li_sub.addView(no, 60,LayoutParams.FILL_PARENT);
				
				if(c_par.getColumnCount()>5)
				{
					//***Double row value showing started****//*
					LinearLayout li_sub_par= new LinearLayout(this);
					li_sub_par.setOrientation(LinearLayout.VERTICAL);
					li_sub.addView(li_sub_par,LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
					LinearLayout li_sub_val = null;
					
					int divider=0,count=c_par.getColumnCount();
					//***Get divider value to split that values into tow rows ***//*
					if(count%2==0)
					{
						divider=count/2;
						
					}
					else
					{
						divider=(count+1)/2;
						count+=1;
					}
					//***Get divider value to split that values into tow rows ends. Showing header value***//*
					for(int i=4;i<c_par.getColumnCount();i++,c_par.moveToNext())
					{
						if(i==4 ||i==divider ) 
						{
							li_sub_val=new LinearLayout(this);
							li_sub_val.setOrientation(LinearLayout.HORIZONTAL);
							li_sub_par.addView(li_sub_val, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
						}
						TextView tv= new TextView(this,null,R.attr.n_pre_layout_Text_view);
						if(i!=c_par.getColumnCount())
							tv.setText(c_par.getColumnName(i));
						tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.n_preview_header_dynamic));//***Set header back ground***//*
						li_sub_val.addView(tv,LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
						LinearLayout.LayoutParams lp = (LayoutParams) tv.getLayoutParams();
						lp.weight=1;
						tv.setLayoutParams(lp);
					}
					//**Showing header value ends. Showing matrix value Starts**//*
					
				}
				else
				{
					/**Single row values showing started. Showing header value**/
					for(int i=0;i<c_par.getColumnCount();i++,c_par.moveToNext())
					{
						TextView tv= new TextView(this,null,R.attr.n_pre_layout_Text_view);
						tv.setText(c_par.getColumnName(i));
						li_sub.addView(tv,LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
						LinearLayout.LayoutParams lp = (LayoutParams) tv.getLayoutParams();
						lp.weight=1;
						tv.setLayoutParams(lp);
						tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.n_preview_header_dynamic));
					}
				}	
		}
		c_par.close();
	}
	catch (Exception e) {
		// TODO: handle exception
		System.out.println("DDDD"+e.getMessage());
	}
}
public void clicker(View v)
{
	switch (v.getId()) {
	case R.id.pre_bt_close:
		Intent intent = new Intent(ReportPreview.this, LoadModules.class);
		Bundle b2 = new Bundle();
		b2.putString("selectedid", srid);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("inspectorid", inspectorid);			
		intent.putExtras(b3);
		startActivity(intent);
		finish();
	break;
	case R.id.pre_bt_next:
		if((current_sub_pos+1)!=strarr_module.length)
		{
		current_sub_pos++;
		
		cur_sel_b.setTextColor(getResources().getColor(R.color.n_header_text_color));
		cur_sel_b=(Button) li_ho.findViewById(current_sub_pos);
		cur_sel_b.setTextColor(getResources().getColor(R.color.n_button_input_page_select_color));
		current_sub_pos=current_sub_pos;
		modulename = cur_sel_b.getText().toString();
		((TextView)findViewById(R.id.pre_tv_header)).setText(modulename);
		cf.GetCurrentModuleName(modulename);
		showsubsection(modulename);
		}
		((HorizontalScrollView)findViewById(R.id.sub_section_hs)).smoothScrollTo(cur_sel_b.getLeft(),0);
		break;
	case R.id.pre_bt_previous:
		if(current_sub_pos!=0)
		{
			current_sub_pos--;
		cur_sel_b.setTextColor(getResources().getColor(R.color.n_header_text_color));
		cur_sel_b=(Button) li_ho.findViewById(current_sub_pos);
		cur_sel_b.setTextColor(getResources().getColor(R.color.n_button_input_page_select_color));
		current_sub_pos=current_sub_pos;
		modulename = cur_sel_b.getText().toString();
		((TextView)findViewById(R.id.pre_tv_header)).setText(modulename);
		cf.GetCurrentModuleName(modulename);
		showsubsection(modulename);
		}
		((HorizontalScrollView)findViewById(R.id.sub_section_hs)).smoothScrollTo(cur_sel_b.getLeft(),0);
		break;
	default:
		break;
	}
	
}

@Override
protected void onSaveInstanceState(Bundle savedInstanceState)
{
	  savedInstanceState.putInt("current_pos", current_sub_pos);
	  savedInstanceState.putString("headertext", modulename);
	
	  super.onSaveInstanceState(savedInstanceState);
}

@Override
protected void onRestoreInstanceState(Bundle savedInstanceState) 
{
	current_sub_pos = savedInstanceState.getInt("current_pos");
	modulename = savedInstanceState.getString("headertext");
	
	cur_sel_b.setTextColor(getResources().getColor(R.color.n_header_text_color));
	cur_sel_b=(Button) li_ho.findViewById(current_sub_pos);
	cur_sel_b.setTextColor(getResources().getColor(R.color.n_button_input_page_select_color));
	((TextView)findViewById(R.id.pre_tv_header)).setText(modulename);

	cf.GetCurrentModuleName(modulename);
	showsubsection(modulename);
	((HorizontalScrollView)findViewById(R.id.sub_section_hs)).smoothScrollTo(cur_sel_b.getLeft(),0);
	  super.onRestoreInstanceState(savedInstanceState);
}
@Override
public boolean onKeyDown(int keyCode, KeyEvent event) {
	// replaces the default 'Back' button action

	if (keyCode == KeyEvent.KEYCODE_BACK) {
		Intent intent = new Intent(ReportPreview.this, LoadModules.class);
		Bundle b2 = new Bundle();
		b2.putString("selectedid", srid);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("inspectorid", inspectorid);			
		intent.putExtras(b3);
		startActivity(intent);
		finish();
		
	}

	return super.onKeyDown(keyCode, event);
}	
}
