package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.ArrayModuleList;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.util.ArrayList;



import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class ShowDialog extends Activity {
	String arrstrname="",gettempname="",getmodulename="",str="",getcurretview="",gettempoption="",seltdval="",strchk="",strother="",foundstr="";
	DatabaseFunction db;
	CommonFunction cf;
	ArrayModuleList ar;
	LinearLayout lin;
	int dialog_other=0,getid=0;
	Dialog dialog;
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	CheckBox[] ch;
	RadioButton[] rd;
	String[] s,arrstrchk; 
	EditText etother;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		
		    Bundle b = this.getIntent().getExtras();
			arrstrname = b.getString("arrstr");
			Bundle b1 = this.getIntent().getExtras();
			gettempname = b1.getString("templatename");
			Bundle b2 = this.getIntent().getExtras();
			getmodulename = b2.getString("modulename");
			Bundle b3 = this.getIntent().getExtras();
			getcurretview = b3.getString("currentview");
			Bundle b4 = this.getIntent().getExtras();
			gettempoption = b4.getString("tempoption");
			Bundle b5 = this.getIntent().getExtras();
			getid = b5.getInt("id");System.out.println("Show Dialog getcurretview"+getcurretview+"getid"+getid);
			
		    setContentView(R.layout.customizedialog);
		    
		    ar = new ArrayModuleList(this,getmodulename);
			cf = new CommonFunction(this);
			db = new DatabaseFunction(this);
			
			db.CreateTable(1);
			db.CreateTable(3);
			db.CreateTable(4);
			db.userid();
			Declaration();
			
			System.out.println("declarationends");
	}

	private void Declaration() {
		// TODO Auto-generated method stub
         dbvalue();
		
		 show_alert();
	}
	private void dbvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		cf.GetCurrentModuleName(getmodulename);		
		
		for(int j=0;j<cf.arrsub.length;j++)
		{
			if(arrstrname.equals(cf.arrsub[j]))
			{
				for(int i=j;i<=j;i++)
				{
					s = ar.arrload[i];		
					
					for(int k=0;k<s.length;k++)
					 {
						 str_arrlst.add(s[k]);
					 }
				}
			}
		}		
		
		try
		{
			 Cursor c=db.hi_db.rawQuery("select * from " + db.AddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =0;i<c.getCount();i++,c.moveToNext())
				 {
					 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
				 }
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void show_alert() {

		
	/*	dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);*/
	
		lin = (LinearLayout) findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)findViewById(R.id.header);
		hdr.setText(arrstrname);
	
		show();				
		
		Button btncancel = (Button) findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//CreateSubModule.sp=0;
				back();
				//dialog.dismiss();
			}
		});
		
		Button btnvc = (Button) findViewById(R.id.setvc);
		btnvc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(ShowDialog.this, NewSetVC.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", "create");			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("submodulename", arrstrname);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", gettempoption);			
				intent.putExtras(b6);
				System.out.println("gettempoptionsubmod="+gettempoption);
				startActivity(intent);
				finish();
				
			}
		});
		
		Button btnsave = (Button)findViewById(R.id.ok);
		btnsave.setOnClickListener(new OnClickListener() {
			String seltdval = "";
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(arrstrname.equals("Main Panel Meter Located Inside")||arrstrname.equals("Main Panel Meter Located Outside") 
						|| arrstrname.equals("Fire Separation Present")|| arrstrname.equals("Moisture Stains Present")
						|| arrstrname.equals("Typical Cracks") || arrstrname.equals("Fire Door")
						|| arrstrname.equals("Auto Closure") || arrstrname.equals("Water Meter Flow Detector Stable")
						|| arrstrname.equals("5 Year Wiring Replacement Probability")
						|| arrstrname.equals("Supply Registers Located")
						|| arrstrname.equals("Return Registers Located")|| arrstrname.equals("Safety Glass Satmp at Shower Enclosure")
						|| arrstrname.equals("Spa - Whirlpool Style/Type")|| arrstrname.equals("Water Clarity")|| arrstrname.equals("Pool Shape")
						)
				{
					for (int i = 0; i < str_arrlst.size(); i++) {
						if (rd[i].isChecked()) {
							seltdval = rd[i].getText().toString();
						}
					}
				}
				else
				{
					for (int i = 0; i < str_arrlst.size(); i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
				}
				
				String finalselstrval = "";
				finalselstrval = seltdval;
				System.out.println("final"+finalselstrval);
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{
					try
					{
						Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
						if (cur.getCount() > 0) {
							db.hi_db.execSQL("UPDATE "
									+ db.CreateDefault
									+ " SET fld_description='"
									+ db.encode(finalselstrval)
									+ "'  WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'");
						}
						else
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.CreateDefault
									+ " (fld_inspectorid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES"
									+ "('"+db.UserId+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(finalselstrval)+"','"+db.encode(gettempname)+"','0')");
						}
					//	CreateSubModule.sp=0;
						cf.DisplayToast("Added successfully");
						back();
						//dialog.dismiss();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		});
		
		Button btnother = (Button)findViewById(R.id.addnew);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=1;
				dialogshow();
			}
		});
		
		
		//dialog.show();
	}
	protected void dialogshow() {
		// TODO Auto-generated method stub
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(ShowDialog.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.addnewcustom);
		
		etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
		cf.hidekeyboard(etother);
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		strother = etother.getText().toString();
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//CreateSubModule.sp=0;
				dialog_other=0;
				add_dialog.dismiss();
			}
		});
		
		Button btnsave = (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {
String seltdval="";
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etother.getText().toString().trim().equals("")) {
					cf.DisplayToast("Please enter the other text");
					
				} else {
					if(str_arrlst.contains(strother.trim().toLowerCase()))
					{
						cf.DisplayToast("Already exists!! Please enter the new text");
						etother.setText("");
						etother.requestFocus();
					}
					else
					{
						try
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.AddOther
									+ " (fld_inspectorid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
									+ "('"+db.UserId+"','"+getmodulename+"','"+arrstrname+"','"
									+ db.encode(etother.getText().toString()) + "','"+db.encode(gettempname)+"')");
							
							String addedval = db.encode(etother.getText().toString());
							System.out.println("addedval="+addedval);
							str_arrlst.add(addedval);
						//	CreateSubModule.sp=0;
							dialog_other=0;
							cf.DisplayToast("Added successfully");
							add_dialog.cancel();
							
							lin.removeAllViews();
							dbvalue();
							show();
						
							
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
		
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}

	public void onSaveInstanceState(Bundle outState) {
		 
	        outState.putInt("other_index", dialog_other);
	        if(dialog_other==1)
	        {
	         outState.putString("othertext_index", etother.getText().toString());
	        }
	        
	        
	        outState.putString("checkbox_index", seltdval);	      
	       
	        super.onSaveInstanceState(outState);
	    }
	 protected void onRestoreInstanceState(Bundle savedInstanceState) {
		   dialog_other = savedInstanceState.getInt("other_index");
		   if(dialog_other==1)
		   {
			   dialogshow();
			   strother = savedInstanceState.getString("othertext_index");
			   etother.setText(strother);
			 
		   }
		  
		   seltdval = savedInstanceState.getString("checkbox_index");
		   if(!seltdval.equals(""))
		   {
			   cf.setValuetoCheckbox(ch, seltdval);
			  /*  if(seltdval.contains("&#44;"))
				{
					String[] val= seltdval.split("&#44;");
					for(int i=0;i<ch.length;i++)
					{
						for(int j=0;j<val.length;j++)
						{
							if(ch[i].getText().toString().trim().equals(val[j].trim()))
							{
								System.out.println("comesinsidechkbxtrue"+i);
								ch[i].setChecked(true);
								
							}
						}
					}
				}
				else
				{
					for(int i=0;i<ch.length;i++)
					{
						
							if(ch[i].getText().toString().equals(seltdval.trim()))
							{
								ch[i].setChecked(true);
							}
						
					}
				}*/
		   }
		 
		   super.onRestoreInstanceState(savedInstanceState);
	    }
	private void show() {
		// TODO Auto-generated method stub
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(arrstrname)+"'", null);
			if (cur.getCount() > 0) {
				cur.moveToFirst();
				for (int i = 0; i < cur.getCount(); i++, cur.moveToNext()) {
					str = db.decode(cur.getString(cur
							.getColumnIndex("fld_description")));
	                
				}
			
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		if(arrstrname.equals("Main Panel Meter Located Inside")||arrstrname.equals("Main Panel Meter Located Outside") 
				|| arrstrname.equals("Fire Separation Present")|| arrstrname.equals("Moisture Stains Present")
				|| arrstrname.equals("Typical Cracks") || arrstrname.equals("Fire Door")
				|| arrstrname.equals("Auto Closure") || arrstrname.equals("Water Meter Flow Detector Stable")
				|| arrstrname.equals("5 Year Wiring Replacement Probability")
				||  arrstrname.equals("Supply Registers Located")
				|| arrstrname.equals("Return Registers Located")|| arrstrname.equals("Safety Glass Satmp at Shower Enclosure")
				|| arrstrname.equals("Spa - Whirlpool Style/Type") || arrstrname.equals("Water Clarity")|| arrstrname.equals("Pool Shape")
				)
			{
				rd = new RadioButton[str_arrlst.size()];
				RadioGroup rg = new RadioGroup(this);
				rg.setOrientation(RadioGroup.VERTICAL);
				for (int i = 0; i < str_arrlst.size(); i++) {

					rd[i] = new RadioButton(this);
					rg.addView(rd[i]);
					rd[i].setText(str_arrlst.get(i));
					rd[i].setTextColor(Color.BLACK);
					lin.removeAllViews();
					lin.addView(rg);
					
				}

				if (str != "") {
					cf.setValuetoRadio(rd, str);
				}
			}
			else
			{		
				ch = new CheckBox[str_arrlst.size()];
				//System.out.println("str_arrlst.size()"+str_arrlst.size());
				if(arrstrname.equals("Floor/Slab"))
				{
					try
					{
						Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='Foundation Type'", null);
						if (cur.getCount() > 0) {
							cur.moveToFirst();
								foundstr = db.decode(cur.getString(cur
										.getColumnIndex("fld_description")));
				         
						
						}
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				if(arrstrname.equals("Septic Tank Present"))
				{
					try
					{
						Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode("Drainage Service")+"'", null);
						if (cur.getCount() > 0) {
							cur.moveToFirst();
								foundstr = db.decode(cur.getString(cur
										.getColumnIndex("fld_description")));
				         
						
						}
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				for (int i = 0; i < str_arrlst.size(); i++) {
		        		ch[i] = new CheckBox(this);
						ch[i].setText(str_arrlst.get(i));
						ch[i].setChecked(true);//Need to remove this(9/2/2015)
						ch[i].setTextColor(Color.BLACK);
						lin.addView(ch[i]);
						
						if(foundstr.contains("Slab on Grade"))
						{
							if(ch[i].getText().toString().equals("Poured Concrete Slab"))
							{
								ch[i].setChecked(true);
							}
						}
						System.out.println("foundstr="+foundstr);
						if(foundstr.contains("Public System - Municipality"))
						{
							System.out.println("ccc"+ch[i].getText().toString());
							if(ch[i].getText().toString().equals("Poured Concrete Slab") || ch[i].getText().toString().equals("No"))
							{
								ch[i].setChecked(true);
							}
						}
						ch[i].setOnCheckedChangeListener(new CHListener(i));
				
						
				}
		
				if (str != "") {
						cf.setValuetoCheckbox(ch, str);
				}
			}
	}
	class CHListener implements OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
				
				
			}
		}
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		//back();
	}

	private void back() {
		// TODO Auto-generated method stub
		Intent i = new Intent(ShowDialog.this,CreateSubModule.class);						
		i.putExtra("templatename", gettempname);
		i.putExtra("currentview", getcurretview);System.out.println("back"+getcurretview);
		i.putExtra("modulename", getmodulename);
		i.putExtra("tempoption", gettempoption);
		i.putExtra("id",getid);
		//startActivity(i);
		setResult(101,i);
		finish();
	}
}
