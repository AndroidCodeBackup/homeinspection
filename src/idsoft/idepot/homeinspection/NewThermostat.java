package idsoft.idepot.homeinspection;

import java.util.ArrayList;

import idsoft.idepot.homeinspection.CreateHeating.myspinselectedlistener;
import idsoft.idepot.homeinspection.LoadBathroom.CHListener;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;

public class NewThermostat extends Activity {
	String gettempname="",getcurrentview="",getmodulename="",gettempoption="",getselectedho="",fueltankvalue="",finalselstrval="",tempfueltankloc="",getnoofzones="",getheatbrand="",
			seltdval="",tempfluetype="",temptestedfor="",tempairhandlerfeat="",tempthermolocation="",tempthermotype="",tempductworktype="",tempconddischarge="",getzoneno="",
					getfurnace="",getmotorblower="",getsupplyregister="",getreturnregister="",getmultizonesystem="",str="",fluetypevalue="",hvacvalue="",
							conddischangevalue="",thermolocationvalue="",thermostattypevalue="",ductworktypevalue="",airhandlerfeatvalue="",testedforvalue="";
	ScrollView scr;
	TableLayout heatingtblshow;
	Spinner spinloadtemplate;
	View v;
	LinearLayout lin;
	String arrtempname[];
	String tempname="";
	ArrayAdapter adapter;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	CheckBox chknotapplicable;
	Button btntakephoto;
	TextView thermostatlocationtv,thermostattypetv;
	int hvac=0,hvacCurrent_select_id,editid=0,getisedit=0,spintouch=0,chk=0;
	static int k=0;
	TextView thermolocationspinner,thermotypespinner;
	CheckBox ch[];
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	Spinner spin_zoneno,sp_thermotype,sp_thermolocation;
	String getspinselectedname="",selectvalthermotype="",selectvalthermoloc="",getthermotype="",getthermoloc="";
	int thermotype_dialog=0,thermolocation_dialog=0;
	
	String[] arrthermolocation={"Hall", "Master Bedroom","Living Room", "Upstairs","Bonus", "Other"},
				arrthermotype={"Manual Thermostat","Programmable Thermostat","Set Back Thermostat","Heat Pump Thermostat","Multiple Zone","Separate Heating/Cooling Thermostat","Aquastat"},
						arrnoofzones={"--Select--","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
	ArrayAdapter thermolocationadap,thermotypeadap,zonenoarrayadap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");System.out.println("modulename="+getmodulename);
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}		
		
		setContentView(R.layout.activity_newthermostat);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(49);
		db.CreateTable(50); 
		db.userid();
		
		Button btnsave = (Button)findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) 
				{
					// TODO Auto-generated method stub
					savehvacnotappl(getcurrentview);
					Intent intent = new Intent(NewThermostat.this, NewHotwater.class);
			    	Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("tempoption", gettempoption);			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("selectedid", getselectedho);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
					
				}
			});
		chknotapplicable = (CheckBox) findViewById(R.id.hvacnotappl);		 
		chknotapplicable.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
	                //is chkIos checked?
			if (((CheckBox) v).isChecked())
			{
				chk=1;
				System.out.println("its checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
				
			}
			else
			{
				chk=2;
				System.out.println("its not checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
				
				
			}
	 
		  }
		});

		
		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/squadaone-regular.ttf"); 
		btnsubmit.setTypeface(font); 
		
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);
									
				}  
			}
		});
		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(NewThermostat.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							Bundle b4 = new Bundle();
							b4.putString("currentview","takephoto");			
							intent.putExtras(b4);
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getselectedho);			
							intent.putExtras(b5);
							startActivity(intent);
							finish();
			}
		});
		
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC", null);
            System.out.println("cur="+cur.getCount()+"select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC");
			if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);				
				}  
			}
		});
		
		
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(NewThermostat.this,HomeScreen.class));
					finish();
					 
				}
			});
		heatingtblshow =((TableLayout)findViewById(R.id.themostatlayout));


		thermotypespinner = (TextView)findViewById(R.id.spin_thermotype);
		thermotypespinner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				thermotype_dialog=1;
				hvacvalue="Thermostat Type";k=6;
				multi_select(hvacvalue,k);				
			}
		});
		
		thermolocationspinner= (TextView)findViewById(R.id.spin_thermolocation);
		thermolocationspinner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				thermolocation_dialog=1;
				hvacvalue="Thermostat Location";k=7;
				multi_select(hvacvalue,k);				
			}
		});
		
		thermostattypetv = (TextView)findViewById(R.id.thermotypetv);
		thermostatlocationtv = (TextView)findViewById(R.id.thermoloctypetv);
		
		
		spin_zoneno = (Spinner)findViewById(R.id.spin_zoneno);
		zonenoarrayadap = new ArrayAdapter<String>(NewThermostat.this,android.R.layout.simple_spinner_item,arrnoofzones);
		zonenoarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_zoneno.setAdapter(zonenoarrayadap);
		spin_zoneno.setOnItemSelectedListener(new myspinselectedlistener(16));
		
		Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
		
		Button save = (Button)findViewById(R.id.btnsave);
		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getzoneno.equals("--Select--"))
				{
					cf.DisplayToast("Please select the Zone No.");
				}
				else
				{
					 try
						{
							 if(getcurrentview.equals("start"))
							 {
								 if(((Button)findViewById(R.id.btnsave)).getText().toString().equals("Update"))
								 {
									 System.out.println("UPDATE  "+db.LoadThermostat+" SET  fld_thermotype='"+tempthermotype+"',"+
									           "fld_thermolocation='"+tempthermolocation+"',fld_thermostatcomments='"+((EditText)findViewById(R.id.comments)).getText().toString()+"'" +" Where z_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
									 db.hi_db.execSQL("UPDATE  "+db.LoadThermostat+" SET  fld_thermotype='"+tempthermotype+"',"+
									           "fld_thermolocation='"+tempthermolocation+"',fld_thermostatcomments='"+((EditText)findViewById(R.id.comments)).getText().toString()+"'" +" Where z_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
				
										thermostatcommon();
								 }
								 else
								 {
										Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadThermostat + " WHERE fld_inspectorid='"+db.UserId+"'  and fld_srid='"+getselectedho+"' and fld_zoneno='"+getzoneno+"'", null);
										if(cur.getCount()==0)
										{

											db.hi_db.execSQL(" INSERT INTO "+db.LoadThermostat+" (fld_templatename,fld_inspectorid,fld_srid,fld_thermotype,fld_thermolocation,fld_zoneno,fld_thermostatcomments) VALUES" +
																						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+tempthermotype+"','"+tempthermolocation+"','"+getzoneno+"','"+((EditText)findViewById(R.id.comments)).getText().toString()+"')");
											
											thermostatcommon();savehvacnotappl(getcurrentview);
										}
										else
										{
										
											 cf.DisplayToast("Already Exists!! Please select another Zone No.");
										}
								 }
							 }
							 else
							 {
								 if(((Button)findViewById(R.id.btnsave)).getText().toString().equals("Update"))
									{
											db.hi_db.execSQL("UPDATE  "+db.CreateThermostat+" SET  fld_thermotype='"+tempthermotype+"',"+
									           "fld_thermolocation='"+tempthermolocation+"',fld_thermostatcomments='"+((EditText)findViewById(R.id.comments)).getText().toString()+"'" +" Where z_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
										
											thermostatcommon();
											
									}
									else
									{
										try
										{
												Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateThermostat + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'", null);
												System.out
														.println("select * from " + db.CreateThermostat + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'"+cur.getCount());
												if(cur.getCount()==0)
												{
													System.out
															.println(" INSERT INTO "+db.CreateThermostat+" (fld_templatename,fld_inspectorid,fld_thermotype,fld_thermolocation,fld_zoneno,fld_thermostatcomments) VALUES" +
														    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+tempthermotype+"','"+tempthermolocation+"','"+getzoneno+"','"+((EditText)findViewById(R.id.comments)).getText().toString()+"')");
													
													db.hi_db.execSQL(" INSERT INTO "+db.CreateThermostat+" (fld_templatename,fld_inspectorid,fld_thermotype,fld_thermolocation,fld_zoneno,fld_thermostatcomments) VALUES" +
														    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+tempthermotype+"','"+tempthermolocation+"','"+getzoneno+"','"+((EditText)findViewById(R.id.comments)).getText().toString()+"')");
			
													thermostatcommon();savehvacnotappl(getcurrentview);
												}
												else
												{
													
													 cf.DisplayToast("Already Exists!! Please select another Zone No.");
												}
										}
										catch (Exception e) {
											// TODO: handle exception
											 System.out.println("insi teeeee"+e.getMessage());
										}
									}
							 }
						 
						}
						catch (Exception e) {
							// TODO: handle exception
							 System.out.println("insi sdasdaseeeea"+e.getMessage());
						}
					

					 
				}
			
			}
		});
		

		Button	btncancel = (Button)findViewById(R.id.btncancel);
		btncancel.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					clearthermostat();					 
				}
		});
		
		((TextView)findViewById(R.id.modulename)).setVisibility(v.VISIBLE);
		((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		
		LinearLayout menu = (LinearLayout) findViewById(R.id.hvacmenu);
		menu.addView(new Header_Menu(this, 4, cf,gettempname,getcurrentview,getmodulename,gettempoption,getselectedho,db));
		displaythermostat();shownotpplicable();
	}
	private void shownotpplicable() {
		// TODO Auto-generated method stub
		Cursor cur1;
		if(getcurrentview.equals("start"))
		{
			  cur1= db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' ",null);
		}
		else
		{
			  cur1= db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable+ " where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+getselectedho+"' ",null);	
		}
		System.out.println("cur11="+cur1.getCount());
		if(cur1.getCount()>0)
		{
			cur1.moveToFirst();
			String navalue = cur1.getString(cur1.getColumnIndex("fld_thermoNA"));System.out.println("tes="+navalue);
			if(navalue.equals("1"))
			{System.out.println("inside checked");
				chknotapplicable.setChecked(true);
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE); 		
			}
			else
			{System.out.println(" fdsfdsf");
				chknotapplicable.setChecked(false);
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
			}
		}
		
	}
	private void savehvacnotappl(String currentview) {
		// TODO Auto-generated method stub
		try
		{
			if(chknotapplicable.isChecked()==true)
			{
				chk=1;
			}
			else
			{
				chk=2;
			}
			
			Cursor cur2;
			if(currentview.equals("start"))
			{
				 cur2= db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);	
			}
			else
			{
				 cur2= db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
			
			
			if(cur2.getCount()==0)
			{
				if(currentview.equals("start"))
				{
					db.hi_db.execSQL(" INSERT INTO "+db.HVACNotApplicable+" (fld_templatename,fld_inspectorid,fld_srid,fld_thermoNA) VALUES" +
															    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+chk+"')");
				}
				else
				{
					db.hi_db.execSQL(" INSERT INTO "+db.CreateHVACNotApplicable+" (fld_templatename,fld_inspectorid,fld_thermoNA) VALUES" +
						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+chk+"')");
				}
						
			}
			else
			{
				if(currentview.equals("start"))
				{
					db.hi_db.execSQL("UPDATE  "+db.HVACNotApplicable+" SET fld_thermoNA='"+chk+"' Where fld_inspectorid='"+db.UserId+"' and fld_Srid='"+getselectedho+"'");
				}
				else
				{
					db.hi_db.execSQL("UPDATE  "+db.CreateHVACNotApplicable+" SET fld_thermoNA='"+chk+"' Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"'");
				}
				
			
			}
		}
		catch(Exception e1)
		{
			System.out.println("not appl="+e1.getMessage());
		}
	}
	private void thermostatcommon()
	{
		cf.DisplayToast("Thermostat saved successfully");
		((Button)findViewById(R.id.btnsave)).setText("Save and Add more");
		clearthermostat();
		displaythermostat();
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(NewThermostat.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						DefaultValue_Insert(gettempname,getselectedho);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	private void DefaultValue_Insert(String gettempname,String getselectedho) {
		try
		{
			Cursor selcur2 = db.hi_db.rawQuery("select * from " + db.CreateThermostat + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' ", null);
            int cnt = selcur2.getCount();
            if(cnt>0)
           
            	
				if (selcur2 != null) {
					selcur2.moveToFirst();
					db.hi_db.execSQL("Delete from "+db.LoadThermostat+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
					do {
						
						Cursor cur2 = db.hi_db.rawQuery("select * from " + db.LoadThermostat + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
						
    		            if(cur2.getCount()==0)
    		            {
							try
			            	{
								
								
								
								db.hi_db.execSQL("INSERT INTO "+db.LoadThermostat+" (fld_templatename,fld_srid,fld_inspectorid,fld_thermotype,fld_thermolocation,fld_thermostatcomments) VALUES" +
										"('"+db.encode(gettempname)+"','"+getselectedho+"','"+db.UserId+"','"+selcur2.getString(4)+"','"+selcur2.getString(5)+"','"+selcur2.getString(6)+"','"+selcur2.getString(7)+"')");
		                 	
								System.out.println("error catch completed");
			            	}
			            	catch (Exception e) {
								// TODO: handle exception
			            		System.out.println("error catch11="+e.getMessage());
							}
    		            }
    		           
					
					}while (selcur2.moveToNext());
				}
            
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void clearthermostat() {
		// TODO Auto-generated method stub
		spin_zoneno.setEnabled(true);
		spin_zoneno.setSelection(0);
		thermostattypetv.setVisibility(v.GONE);
		thermostatlocationtv.setVisibility(v.GONE);
		((EditText)findViewById(R.id.comments)).setText("");
		((Button)findViewById(R.id.btnsave)).setText("Save and Add More");
	}

	private void displaythermostat() {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur;
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadThermostat + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateThermostat + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
			
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 try
					{
						heatingtblshow.removeAllViews();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.systembrand_header, null); 
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.thermostat_header);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();
			
				 heatingtblshow.setVisibility(View.VISIBLE);				 
				 heatingtblshow.addView(th,lp); 			 
				
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String zoneno="",thermotypevalue="",thermolocation="",comments="";
					 
					
					 zoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));			 
					 thermotypevalue=db.decode(cur.getString(cur.getColumnIndex("fld_thermotype")));					
					 thermolocation=cur.getString(cur.getColumnIndex("fld_thermolocation"));
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_thermostatcomments")));

					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.systembrand_listview, null);
					 TableRow t = (TableRow)l1.findViewById(R.id.thermostatvalue);
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     
				      
					 TextView tvzoneno= (TextView) t.findViewWithTag("zoneno");
					 if(zoneno.equals("--Select--"))
						{
						 zoneno="";
						}
					 tvzoneno.setText(zoneno);						
										
					 TextView tvthermotype= (TextView) t.findViewWithTag("thermotype");
					 if(thermotypevalue.equals("--Select--"))
					 {
						 thermotypevalue="";
					 }
					 tvthermotype.setText(thermotypevalue);
					 
					 TextView tvthermoloc= (TextView) t.findViewWithTag("thermolocation");
					 if(thermolocation.equals("--Select--"))
					 {
						 thermolocation="";
					 }
					 tvthermoloc.setText(thermolocation);
					 
					 TextView tvthermostatcomments= (TextView) t.findViewWithTag("comments");
						
					 tvthermostatcomments.setText(comments);
					 
					 ImageView edit= (ImageView) t.findViewWithTag("edit");
					 	edit.setId(789456+i);
					 	edit.setTag(cur.getString(0));
		                edit.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
							try
							{
							   int i=Integer.parseInt(v.getTag().toString());
								((Button)findViewById(R.id.btnsave)).setText("Update");
								 clearthermostat();
								 updatehvac(i);   
							}
							catch (Exception e) {
								// TODO: handle exception
							}
							}
		                });
	             
	                ImageView delete=(ImageView) t.findViewWithTag("del"); 
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(NewThermostat.this);
							builder.setMessage("Do you want to delete the selected Zone details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadThermostat+" Where z_id='"+i+"'");
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateThermostat+" Where z_id='"+i+"'");
													}
													
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
													thermostatcommon();
													
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					
					
					
					
					l1.removeAllViews();
					
					heatingtblshow.addView(t,lp);
					cur.moveToNext();
					
				 }
			 }
			 else
			 {
				 heatingtblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	 	
	}
	private void scrollup() {
		// TODO Auto-generated method stub
	scr = (ScrollView)findViewById(R.id.scr);
	       scr.post(new Runnable() {
	          public void run() {
	              scr.scrollTo(0,0);
	          }
	      }); 
	}
	protected void updatehvac(int i) {
		// TODO Auto-generated method stub
		Cursor cur;
			editid=1;
			getisedit=1;
			hvacCurrent_select_id = i;scrollup();
			try
			{
				
				if(getcurrentview.equals("start"))
				{
					  cur= db.hi_db.rawQuery("select * from " + db.LoadThermostat + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'",null);
				}
				else
				{
					 cur= db.hi_db.rawQuery("select * from " + db.CreateThermostat + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'", null);
				}
					
				System.out.println("count for thermostat="+cur.getCount());				
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
						
					 String zoneno="",thermotype="",thermolocation="",comments="";
					 
					 zoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));					 
					 tempthermotype=db.decode(cur.getString(cur.getColumnIndex("fld_thermotype")));					 
					 tempthermolocation=db.decode(cur.getString(cur.getColumnIndex("fld_thermolocation")));
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_thermostatcomments")));
					
				 	 spin_zoneno.setSelection(zonenoarrayadap.getPosition(zoneno));spin_zoneno.setEnabled(false);
					
				 	if(!tempthermotype.equals(""))
					 {
				 		thermostattypetv.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempthermotype));
				 		thermostattypetv.setVisibility(v.VISIBLE); 
					 }
					 else
					 {
						 thermostattypetv.setVisibility(v.GONE);	 
					 }
					 
				 	if(!tempthermolocation.equals(""))
					 {
				 		thermostatlocationtv.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempthermolocation));
						 thermostatlocationtv.setVisibility(v.VISIBLE); 
					 }
					 else
					 {
						 thermostatlocationtv.setVisibility(v.GONE);	 
					 }
					 
					
					((EditText)findViewById(R.id.comments)).setText(comments);

					((Button)findViewById(R.id.btnsave)).setText("Update");
				}
			}catch(Exception e){}
		
	}
	private void dbvalue(String bathvalue) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		System.out.println("bathvalue"+bathvalue);
		if(bathvalue.equals("Thermostat Type"))
		{
			for(int j=0;j<arrthermotype.length;j++)
			{
				str_arrlst.add(arrthermotype[j]);
			}	
		}
		
		if(bathvalue.equals("Thermostat Location"))
		{
			for(int j=0;j<arrthermolocation.length;j++)
			{
				str_arrlst.add(arrthermolocation[j]);
			}	
		}	
	}
	
	private void multi_select(final String hvacvalue, final int s) {
		// TODO Auto-generated method stub
		hvac=1;
		dbvalue(hvacvalue);
		final Dialog add_dialog = new Dialog(NewThermostat.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);

		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);
		
		 		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(hvacvalue);
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);
		btnother.setVisibility(v.GONE);
		if(s==6)
		{
			showsuboption(tempthermotype,thermostattypetv);
		}
		else if(s==7)
		{
			showsuboption(tempthermolocation,thermostatlocationtv);
		}
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval="";
								for (int i = 0; i < str_arrlst.size(); i++) {
									if (ch[i].isChecked()) {
										seltdval += ch[i].getText().toString() + "&#44;";
									}
								}
								
								
								finalselstrval = "";
								finalselstrval = seltdval;
								if(finalselstrval.equals(""))
								{
									cf.DisplayToast("Please check at least any 1 option to save");
								}
								else
								{	
									 if(hvacvalue.equals("Thermostat Type"))
									{
										thermostattypevalue=finalselstrval;
										tempthermotype = thermostattypevalue;
										tempthermotype = cf.replacelastendswithcomma(tempthermotype);
										thermostattypetv.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempthermotype));
										thermostattypetv.setVisibility(v.VISIBLE);
									}
									
									
									else if(hvacvalue.equals("Thermostat Location"))
									{
										thermolocationvalue=finalselstrval;
										tempthermolocation = thermolocationvalue;
										tempthermolocation = cf.replacelastendswithcomma(tempthermolocation);
										thermostatlocationtv.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempthermolocation));
										thermostatlocationtv.setVisibility(v.VISIBLE);
									}
									
									add_dialog.cancel();
									if(s==6){thermotype_dialog=0;}
									else if(s==7){thermolocation_dialog=0;}
									hvac=0;
								}
								
					}
				
		});

		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hvac=0;if(s==6){thermotype_dialog=0;}
				else if(s==7){thermolocation_dialog=0;}
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		
		setvalue(hvacvalue);
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	
	private void setvalue(String hvacvalue) {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur=null;
			if(getcurrentview.equals("start"))
			{
				cur= db.hi_db.rawQuery("select * from " + db.LoadThermostat + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"' and fld_zoneno='"+spin_zoneno.getSelectedItem().toString()+"'",null);
			}
			else
			{
				cur = db.hi_db.rawQuery("select * from " + db.CreateThermostat+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+spin_zoneno.getSelectedItem().toString()+"'", null);
			}
		    if(cur.getCount()>0)
		    {
				cur.moveToFirst();
				if(hvacvalue.equals("Thermostat Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_thermotype")));	
				}
				else if(hvacvalue.equals("Thermostat Location"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_thermolocation")));	
				}
				
				if(str.contains(","))
			    {
			    	str = str.replace(",","&#44;");
			    }	
			}
		    else
		    {
			
		    	if(hvacvalue.equals("Thermostat Type") && hvacvalue!=null)
				{
					str = thermostattypevalue;
				}
				else if(hvacvalue.equals("Thermostat Location") && hvacvalue!=null)
				{
					str  = thermolocationvalue;
				}
				
		    }
			    if(!str.trim().equals(""))
				{
					cf.setValuetoCheckbox(ch, str);
				}	
		}catch (Exception e) {
			// TODO: handle exception
		}
	    	
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	private void showsuboption(String selectedvalue,TextView tv) {
		// TODO Auto-generated method stub
		if(selectedvalue.trim().equals(""))
		{
			if(tv.getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(tv.getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	tv.getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=selectedvalue;
		}
		

		  ch = new CheckBox[str_arrlst.size()];
			
			for (int i = 0; i < str_arrlst.size(); i++) {
	        		ch[i] = new CheckBox(this);
					ch[i].setText(str_arrlst.get(i));
					ch[i].setTextColor(Color.BLACK);
					
					lin.addView(ch[i]);
					ch[i].setOnCheckedChangeListener(new CHListener(i));
					
			}
		
		

		if (str != "") {
			
			 cf.setValuetoCheckbox(ch, str);
			 String replaceendcommaionspoint= replacelastendswithcomma(str);
			 tv.setVisibility(v.VISIBLE);
			 tv.setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
		}
		else
		{
			tv.setVisibility(v.GONE);
		}
	}
	private String replacelastendswithcomma(String realword) {
		// TODO Auto-generated method stub
		
		if(realword.contains("&#44;"))
		{
			realword = realword.replace("&#44;",",");
		}
		if(realword.endsWith(","))
		{
			realword = realword.substring(0,realword.length()-1);
		}
		else
		{
			realword = realword;	
		}	
		return realword;
	}
	class CHListener implements OnCheckedChangeListener, android.widget.CompoundButton.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		
		//seltdval="";
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
			}
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	class myspinselectedlistener implements OnItemSelectedListener
	{
         int var=0;
        
		public myspinselectedlistener(int i) {
			// TODO Auto-generated constructor stub
			var=i;
			 getspinselectedname="";
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			getspinselectedname = arg0.getItemAtPosition(arg2).toString();
			switch(var)
			{
				
				  case 16:
					  getzoneno = getspinselectedname;
			    	  break;
				
			    	  
			    	  
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	protected void back() {
		// TODO Auto-generated method stub
				
		Intent intent = new Intent(NewThermostat.this, NewSystembrand.class);
    	Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		Bundle b6 = new Bundle();
		b6.putString("selectedid", getselectedho);			
		intent.putExtras(b6);
		startActivity(intent);
		finish();
		
       
	}
	protected void onSaveInstanceState(Bundle ext)
	{		
		ext.putInt("chkvalue", chk);
			ext.putInt("thermotypedialog",thermotype_dialog);	System.out.println("thermotype_dialog="+thermotype_dialog);
			ext.putInt("index",k);System.out.println("what is k="+k);
			ext.putString("columnname", hvacvalue);
			if(thermotype_dialog==1)
		    {
	        	selectvalthermotype="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalthermotype += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("thermotypeoption_index", selectvalthermotype);
	        }
	        else
	        {
	        	ext.putString("thermotypeoption_index", tempthermotype);
	        }	
			
			System.out.println("EXT="+ext.getString("thermotypeoption_index"));
	        
			 ext.putInt("thermolocdialog",thermolocation_dialog);	
			 if(thermolocation_dialog==1)
		        {
				 selectvalthermoloc="";
		        	for (int i = 0; i < str_arrlst.size(); i++) {
						if (ch[i].isChecked()) {
							selectvalthermoloc += ch[i].getText().toString() + "&#44;";
						}
					}
		        	ext.putString("thermotypelocoption_index", selectvalthermoloc);
		        }
		        else
		        {
		        	ext.putString("thermotypelocoption_index", tempthermolocation);
		        }

		    ext.putString("get_thermotypetext", ((TextView) findViewById(R.id.thermotypetv)).getText().toString());
		    ext.putString("get_thermoloctext", ((TextView) findViewById(R.id.thermoloctypetv)).getText().toString());
		super.onSaveInstanceState(ext);
	}
	protected void onRestoreInstanceState(Bundle ext)
	{
		chk = ext.getInt("chkvalue");
		
		if(chk==1)
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
		}
		else
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
		}
		thermotype_dialog = ext.getInt("thermotypedialog");		System.out.println("thermotype_dialog"+thermotype_dialog);
		tempthermotype = ext.getString("thermotypeoption_index");System.out.println("restore="+ext.getInt("index"));
	    if(thermotype_dialog==1)
	   {
		   hvacvalue = ext.getString("columnname");
		   multi_select(ext.getString("columnname"),ext.getInt("index"));
		   if(!tempthermotype.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, tempthermotype);
		   }
	   }
	
	   getthermotype = ext.getString("get_thermotypetext");
	   if(!getthermotype.equals(""))
	   {
		   
		   ((TextView) findViewById(R.id.thermotypetv)).setVisibility(v.VISIBLE);
		   ((TextView) findViewById(R.id.thermotypetv)).setText(getthermotype);
	   }
	   
	   
	   
	   thermolocation_dialog = ext.getInt("thermolocdialog");	System.out.println("thermolocation_dialog"+thermolocation_dialog);	
		tempthermolocation = ext.getString("thermotypelocoption_index");
	    if(thermolocation_dialog==1)
	   {
		   hvacvalue = ext.getString("columnname");
		   multi_select(ext.getString("columnname"),ext.getInt("index"));
		   if(!tempthermolocation.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, tempthermolocation);
		   }
	   }
	
	   getthermoloc = ext.getString("get_thermoloctext");
	   if(!getthermoloc.equals(""))
	   {
		   
		   ((TextView) findViewById(R.id.thermoloctypetv)).setVisibility(v.VISIBLE);
		   ((TextView) findViewById(R.id.thermoloctypetv)).setText(getthermoloc);
	   }
	   
	   
		 super.onRestoreInstanceState(ext);
	}
}
