package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DataBaseHelper;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Email_report extends Activity {
String policy_id,inspection_id,insp_type_name;
EditText mail_id,title,comment;
CommonFunction cf;
DatabaseFunction db;
Progress_staticvalues p_sv;
Webservice_config wb;
int handler_msg=0;
String error_msg="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.emailreport);
		Bundle b = getIntent().getExtras();
		cf =new CommonFunction(this);
		db=new DatabaseFunction(this);
		wb=new Webservice_config(this);
		if(b!=null)
		{
			policy_id=b.getString("policy_id");
		}
		System.out.println("fdfdsf");
		db.userid();System.out.println("dsfdfsf");
		mail_id=(EditText) findViewById(R.id.email_email_id);
		title=(EditText) findViewById(R.id.email_subject);
		comment=(EditText) findViewById(R.id.email_comments);
		((TextView) findViewById(R.id.email_email_txt)).setText(Html.fromHtml(cf.redstar+" Email id"));
		((TextView) findViewById(R.id.email_subject_txt)).setText(Html.fromHtml(cf.redstar+" Subject"));
	}
public void clicker(View v)
{
	switch (v.getId()) {
	case R.id.email_cancel:
		finish();
	break;
	case R.id.email_clear:
		mail_id.setText("");
		title.setText("");
		comment.setText("");
	break;
	case R.id.email_send:
		if(!mail_id.getText().toString().trim().equals(""))
		{
			if(mail_id.getText().toString().trim().contains(","))
			{
				String id[]=mail_id.getText().toString().trim().split(",");
				for(int i=0;i<id.length;i++)
				{
					if(!cf.eMailValidation(id[i]))
					{
						cf.DisplayToast("One or more email id is not valid, Please check");
						mail_id.requestFocus();
						return;
					}
				}
			}
			else
			{
				if(!cf.eMailValidation(mail_id.getText().toString().trim()))
				{
					cf.DisplayToast("Please enter valid email id");
					mail_id.requestFocus();
					return;
				}
			}
			if(!title.getText().toString().trim().equals(""))
			{
				
			}
			else
			{
				
				cf.DisplayToast("Please enter subject");
				title.requestFocus();
				return;
			}
		}
		else
		{
			cf.DisplayToast("Please enter email id");
			mail_id.requestFocus();
			return;
		}
		new start_import().execute(mail_id.getText().toString().trim(),title.getText().toString().trim(),comment.getText().toString().trim(),db.Userfirstname+" "+db.Userlastname);
	break;
	
	default:
		break;
	}
}
class start_import extends AsyncTask<String, String, String[]>
{
	@Override
	protected String[] doInBackground(String... params) {
		// TODO Auto-generated method stub
		System.out.println(" comes correct1");
			p_sv.progress_live=true;
	        p_sv.progress_title="EMAIL YOUR REPORT";
	        p_sv.progress_Msg="Please wait we are sending email, It may take few minutes";
	        startActivityForResult(new Intent(Email_report.this,Progress_dialogbox.class), Static_variables.progress_code);
			try
			{
				email_report(params[0],params[1],params[2],params[3]);
					//finish();
				
				
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println(" error "+e.getMessage());
				handler_msg=0;
			}
		
		return params;
	}

	private boolean email_report(String email_id,String sub,String commnet,String inspname) throws IOException, XmlPullParserException {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(wb.NAMESPACE,"DRB_Reportsreadymail");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		
		request.addProperty("ReportName",db.UserName);
		request.addProperty("ClientName",inspname);
		//request.addProperty("Inspectiontypeid",inspection_id);
		request.addProperty("InspectorID",db.UserId);
		request.addProperty("PolicyID",policy_id);
		request.addProperty("EmailID",email_id);
		request.addProperty("Cc","");
		request.addProperty("Subject",sub);
		request.addProperty("Comments",commnet);
		envelope.setOutputSoapObject(request);
		System.out.println(" the request"+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		androidHttpTransport.call(wb.NAMESPACE+"DRB_Reportsreadymail",envelope);
		System.out.println(" the result"+envelope.getResponse());
		SoapObject 	result1 = (SoapObject) envelope.getResponse();
		//03-19 13:01:44.814: I/System.out(370):  the resultanyType{Errormsg=Successfuly Update Category Status.; Status=true; }
		//System.out.println(" the result ="+result1.getProperty("Status").toString());
		if(result1.getProperty("Status").toString().trim().equals("true"))
		{
			
			
			//show_values();
			handler_msg=1;
			//cf.show_toast("Status updated successfully", 0);
			return true;
		} 
		else
		{
			handler_msg=2;
			error_msg=result1.getProperty("Errormsg").toString();
			//System.out.println("propkdsbfj");
			//cf.show_toast("Sorry we have some proble on sending mail", 0);
		}
		return false;
	}

	@Override
	protected void onPostExecute(String[] result) {
		// TODO Auto-generated method stub
		
	//	show_values();
		if(handler_msg==0)
		{
			AlertDialog al = new AlertDialog.Builder(Email_report.this).setMessage("Sorry you have some problem in connecting server please try again").setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					finish();
				}
			}).setTitle("Connection Failure").create();
    		al.show();
    		
		}
		else if(handler_msg==2)
		{
			AlertDialog al = new AlertDialog.Builder(Email_report.this).setMessage("Sorry we have problem in process your input please contact our admin. Error Messages="+error_msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {					
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					finish();
				}
			}).setTitle("Connection Failure").create();
    		al.show();
		}
		else
		{
			cf.DisplayToast("Email sent successfully");
			finish();
		}

		p_sv.progress_live=false;
		//finish();
		super.onPostExecute(result);
	}


	
}

}
