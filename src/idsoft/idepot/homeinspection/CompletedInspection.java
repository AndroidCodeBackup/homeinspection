package idsoft.idepot.homeinspection;

import java.io.Externalizable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.idepot.homeinspection.LoadSubModules.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;



public class CompletedInspection extends Activity {
	static Context con;
	static DatabaseFunction db;
	static CommonFunction cf;
	static Progress_staticvalues p_sv;static File file;
	static int handler_msg=2;
	static String title="",error_msg="",selectedho ="",policyno="";
	static ListView startlst_module;
	String start_module="";
	static String inspectorid="";
	static String str_firstname="";
	
	static String PDFPATH="";
	static String str_lastname="",str_inspectionaddress1="", str_inspectioncity="",str_inspectionstate="",str_inspectioncounty="", str_inspectionzip="",
			str_policynumber="", str_inspectiondate="", str_inspectiontime="",str_status="",str_seachtext="",str_srid="";
	static String[] arrfname, arrlname, arraddress, arrcity, arrstate, arrcounty, arrzip, arrpolicynumber, arrdate, arrtime, arrsrid,arrstatus;
	EditText et_searchtext;
	static Webservice_config wb;
	static TextView tvnote,tvcountid;
	static View v;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 db = new DatabaseFunction(this);
		 cf = new CommonFunction(this);
		 wb = new Webservice_config(this);
		 con = CompletedInspection.this;
		 
		 Bundle b = this.getIntent().getExtras();
		 if(b!=null)
		 {
		   inspectorid = b.getString("inspectorid");
		 }
			
		setContentView(R.layout.activity_start);
		db.CreateTable(28);
		db.CreateTable(30);
		db.userid();
		declaration();
	}
	private void declaration() {
		// TODO Auto-generated method stub
		startlst_module = (ListView) findViewById(R.id.startlist);
		tvnote = (TextView)findViewById(R.id.noteid);
		tvcountid = (TextView)findViewById(R.id.countid);
		
		((TextView)findViewById(R.id.note)).setVisibility(v.VISIBLE);
		
		listfromdb();		
	
		((Button)findViewById(R.id.btn_back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(CompletedInspection.this, HomeScreen.class));
				finish();
			}
		});
		 ((TextViewPlus)findViewById(R.id.header)).setText("Completed Inspection");
      
       
       et_searchtext = (EditText)findViewById(R.id.searchtext);
       
       
       ((ImageView)findViewById(R.id.clear)).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			et_searchtext.setText("");str_seachtext="";cf.hidekeyboard(et_searchtext);
			listfromdb();
		}
	   });
       
       ((ImageView)findViewById(R.id.search)).setOnClickListener(new OnClickListener() {
   		
   		@Override
   		public void onClick(View v) {
   			// TODO Auto-generated method stub
   			str_seachtext = et_searchtext.getText().toString();cf.hidekeyboard(et_searchtext);
   			listfromdb();
   		}
   	   });
       
       
       et_searchtext.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        boolean handled = false;
		        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
		        	str_seachtext = et_searchtext.getText().toString();
		        	cf.hidekeyboard(et_searchtext);
		   			listfromdb();
		            handled = true;
		        }
		        return handled;
		    }
		});
       
	}
	private static void listfromdb() {
		// TODO Auto-generated method stub
		Cursor cur1 = null;
		str_firstname="";str_lastname="";str_inspectionaddress1="";str_inspectioncity="";str_inspectionstate="";
		str_inspectionzip="";str_policynumber="";str_inspectiondate="";str_inspectiontime="";str_status="";
		System.out.println("RTE");
		try
		{
			if(str_seachtext.trim().equals(""))
			{
			   cur1 = db.hi_db.rawQuery("select * from " + db.OrderInspection + " where fld_inspectorid='"+inspectorid+"' and (fld_status='2' or fld_status='3')",null);
			}
			else
			{
				cur1 = db.hi_db.rawQuery("select * from " + db.OrderInspection + " where fld_inspectorid='"+inspectorid+"' and (fld_status='2' or fld_status='3') and (fld_firstname like '%"+db.encode(str_seachtext) + "%' or fld_lastname like '%"+db.encode(str_seachtext) + "%' or fld_policynumber like '%"+db.encode(str_seachtext) + "%')",null);
			}
			if(cur1!=null)
			{
				tvcountid.setText("Total no. of records : "+cur1.getCount());
				if(cur1.getCount()==0)
				{
					tvnote.setText("No records found for your search criteria.");
					startlst_module.setVisibility(v.GONE);
				}
				else
				{
					System.out.println("RTE else");
					tvnote.setText("Note :  FIRST NAME | LAST NAME | INSPECTION ADDRESS1 | INSPECTION CITY | INSPECTION STATE | INSPECTION ZIP | POLICY NUMBER | INSPECTION DATE | INSPECTION TIME");
					startlst_module.setVisibility(v.VISIBLE);
					cur1.moveToFirst();
					arrfname=new String[cur1.getCount()];
					arrlname=new String[cur1.getCount()];
					arraddress=new String[cur1.getCount()];
					arrcity=new String[cur1.getCount()];
					arrstate=new String[cur1.getCount()];
					arrzip=new String[cur1.getCount()];
					arrpolicynumber=new String[cur1.getCount()];
					arrdate=new String[cur1.getCount()];
					arrtime=new String[cur1.getCount()];
					arrsrid=new String[cur1.getCount()];
					arrstatus=new String[cur1.getCount()];
					for(int i=0;i<cur1.getCount();i++,cur1.moveToNext())
					{
						arrfname[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_firstname")));
						arrlname[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_lastname")));
						arraddress[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectionaddress1")));
						arrcity[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectioncity")));
						arrstate[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectionstate")));
						arrzip[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_zip")));
						arrpolicynumber[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_policynumber")));
						arrdate[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectiondate")));
						arrtime[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectiontime")));
						arrsrid[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_orderedid")));	
						arrstatus[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_status")));
					}
					startlst_module.setAdapter(new EfficientAdapter(con));
				}
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("error="+e.getMessage());
		}
	}
	public static class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
		
		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
			
		}

		public int getCount() {
			int arrlen = 0;
			arrlen = arrfname.length;System.out.println("fdf"+arrlen);
			

			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.completedlist, null);
				holder.text = (TextView) convertView.findViewById(R.id.TextView01);
				holder.text2 = (Button) convertView.findViewById(R.id.start);
				holder.text2.setVisibility(convertView.VISIBLE);
				
				
				holder.viewreportbtn = (Button) convertView.findViewById(R.id.viewreport);
				holder.editreportbtn = (Button) convertView.findViewById(R.id.editreport);
				holder.emailreportbtn = (Button) convertView.findViewById(R.id.emailreport);

				if (arrstatus[position].contains("2"))
				{
					holder.viewreportbtn.setVisibility(convertView.GONE);					
					holder.editreportbtn.setVisibility(convertView.GONE);					
					holder.emailreportbtn.setVisibility(convertView.GONE);
					holder.text2.setVisibility(convertView.VISIBLE);
				}
				else
				{
					holder.viewreportbtn.setVisibility(convertView.VISIBLE);					
					holder.editreportbtn.setVisibility(convertView.VISIBLE);					
					holder.emailreportbtn.setVisibility(convertView.VISIBLE);
					holder.text2.setVisibility(convertView.GONE);
				}
				
				convertView.setTag(holder);
				
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
				holder.text.setText(Html.fromHtml(arrfname[position]+" | "+arrlname[position])+" | "+arraddress[position]+" | "+ 
					(arrcity[position].equals("")? "N/A":arrcity[position]) +" | "+(arrstate[position].equals("--Select--")? "N/A":arrstate[position]) +" | "+
					(arrzip[position].equals("")? "N/A":arrzip[position]) +" | "+(arrpolicynumber[position].equals("")? "N/A":arrpolicynumber[position]) +" | "+
					(arrdate[position].equals("")? "N/A":arrdate[position]) +" | "+(arrtime[position].equals("")? "N/A":arrtime[position]));
				
			
				holder.viewreportbtn.setOnClickListener(new OnClickListener() {					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						 selectedho = arrsrid[position];
						 
						 	 System.out.println("came here");
							 	Cursor c = db.hi_db.rawQuery("select * from " + db.Downloaded_document + " where fld_srid='"+selectedho+"'",null);
							 	System.out.println("select * from " + db.Downloaded_document + " where fld_srid='"+selectedho+"'"+c.getCount());
								if(c.getCount()>0)
								{
									c.moveToFirst();
									String path=db.decode(c.getString(c.getColumnIndex("down_loc_path")));
									
									System.out.println(" path comes correct="+path);
									File file = new File(path);System.out.println("fiel exist="+file.exists());
									if(file.exists())
									{
										show_pdf_file(file,path);
										
									}
									else
									{
										download_pdf_fromserver(selectedho);
									}
								}
								else
								{
									download_pdf_fromserver(selectedho);
								}
						 
					}
				});

				holder.editreportbtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectedho=arrsrid[position];
						try
		 				 {
							 System.out.println("UPDATE "+db.OrderInspection+ " SET fld_status='2' WHERE fld_orderedid='"+selectedho +"'");
							db.hi_db.execSQL("UPDATE "+db.OrderInspection+ " SET fld_status='2' WHERE fld_orderedid='"+selectedho +"'");
							Intent in = new Intent(con,StartInspection.class);
							in.putExtra("inspectorid", db.UserId);
							con.startActivity(in);
		 				 }
		 				 catch (Exception e) {
		 					// TODO: handle exception
		 				 }
						//new Start_Reediting().execute("");
					}
				});
				holder.emailreportbtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub													
						Intent in = new Intent(con,Email_report.class);
						in.putExtra("policy_id", arrsrid[position]);
						con.startActivity(in);
					}
				});
				
				holder.text2.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						 selectedho = arrsrid[position];
						 policyno= arrpolicynumber[position];
						
							 System.out.println("came here else");
							 if(cf.isInternetOn())
							  {
								   generatepdf();
							  }
							  else
							  {
								  cf.DisplayToast("Please enable your internet connection.");
								  
							  }
						 
						 /*try
						 {
							 db.hi_db.execSQL("UPDATE "+db.OrderInspection+ " SET fld_status='3' WHERE fld_orderedid='"+selectedho+"'");
						 }
						 catch (Exception e) {
							// TODO: handle exception
						}*/
						
						 /* Intent intent = new Intent(con,LoadModules.class);
						 intent.putExtra("selectedid", selectedho);
						 intent.putExtra("inspectorid", inspectorid);
				         con.startActivity(intent);
						 ((Activity) con).finish();*/
					}
				});
			
			return convertView;
		}
		static class ViewHolder {
			TextView text;
			Button text2,viewreportbtn,editreportbtn,emailreportbtn;
		}
	}
	private static void download_pdf_fromserver(String policy_id) {
		// TODO Auto-generated method stub
		if(cf.isInternetOn())
		  {
			new Start_xporting().execute(policy_id);// this will call the db function
		  }
		  else
		  {
			  cf.DisplayToast("Please enable your internet connection.");
			  
		  } 
		
		//System.out.println("comes correctly wrong ly to the downlod page ");
	}
	static class Start_Reediting extends AsyncTask <String, String, String> {		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();	
		        p_sv.progress_live=true;
		        p_sv.progress_title="Editing Report";
			    p_sv.progress_Msg="Please wait..Downloading report from the server.";      
				((Activity) con).startActivityForResult(new Intent(con,Progress_dialogbox.class), Static_variables.progress_code);		       
		    }		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		System.out.println("insid edobanckckc");
			 		      Retrieve_ModuleDataPoints();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
      	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	private static void Retrieve_ModuleDataPoints() throws IOException, XmlPullParserException {
		try{
			String srid="",module="",submodule="",templatename="",description="";
			SoapObject request = new SoapObject(wb.NAMESPACE, "Save_SubModule");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			//request.addProperty("SRID", cf.Homeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

			androidHttpTransport.call(wb.NAMESPACE+"Save_SubModule", envelope);
			SoapObject result1 = (SoapObject) envelope.getResponse();System.out.println("resultpolict"+result1);
			 if(String.valueOf(result1).equals("null") || String.valueOf(result1).equals(null) || String.valueOf(result1).equals("anytype{}"))
			 {
					
			 }
			 else
			 {
				 srid = String.valueOf(result1.getProperty("fld_srid"));
				 module = String.valueOf(result1.getProperty("fld_module"));
				 submodule = String.valueOf(result1.getProperty("fld_submodule"));
				 templatename = String.valueOf(result1.getProperty("fld_templatename"));
				 description = String.valueOf(result1.getProperty("fld_description"));
				 Cursor c1 = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " where fld_inspectorid='" + db.UserId + "' and fld_srid='"+srid+"'",null);
				 if(c1.getCount() == 0)
				 {
					 db.hi_db.execSQL("INSERT INTO "+db.SaveSubModule+" (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_NA,fld_description) VALUES" +
								"('"+db.UserId+"','"+srid+"','"+db.encode(module)+"','"+db.encode(submodule)+"','"+db.encode(templatename)+"','1','"+db.encode(description)+"')");
				 }		
				 else
				 {
					 db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET  WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(templatename)+"' " +
					 		"and fld_module='"+db.encode(module)+"' and fld_submodule='"+db.encode(submodule)+"' and fld_srid='"+srid+"'");
				 }
			 }
		} 
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("catch ni submobule =");
		}
    }
	private static void Retrieve_Heating() throws IOException, XmlPullParserException {
		try{
			String srid="",module="",submodule="",templatename="",description="",heatingtype="",heatingtypeother="",energysource="",energysourceother="",
					heatingno="",otherheatingno="",age="",otherage="",heatsystembrand="",furnacetype="",heatdelivertype="",motorblower="",noofstoves="",
					othernoofstoves="";
			SoapObject request = new SoapObject(wb.NAMESPACE, "Load_Heating");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			//request.addProperty("SRID", cf.Homeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

			androidHttpTransport.call(wb.NAMESPACE+"Save_SubModule", envelope);
			SoapObject result1 = (SoapObject) envelope.getResponse();System.out.println("resultpolict"+result1);
			 if(String.valueOf(result1).equals("null") || String.valueOf(result1).equals(null) || String.valueOf(result1).equals("anytype{}"))
			 {
					
			 }
			 else
			 {
				 srid = String.valueOf(result1.getProperty("fld_srid"));
				 module = String.valueOf(result1.getProperty("fld_module"));
				 submodule = String.valueOf(result1.getProperty("fld_submodule"));
				 templatename = String.valueOf(result1.getProperty("fld_templatename"));
				 description = String.valueOf(result1.getProperty("fld_description"));
				 Cursor c1 = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " where fld_inspectorid='" + db.UserId + "' and fld_srid='"+srid+"'",null);
				 if(c1.getCount() == 0)
				 {
					 db.hi_db.execSQL("INSERT INTO "+db.SaveSubModule+" (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_NA,fld_description) VALUES" +
								"('"+db.UserId+"','"+srid+"','"+db.encode(module)+"','"+db.encode(submodule)+"','"+db.encode(templatename)+"','1','"+db.encode(description)+"')");
				 }		
				 else
				 {
					 db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET  WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(templatename)+"' " +
					 		"and fld_module='"+db.encode(module)+"' and fld_submodule='"+db.encode(submodule)+"' and fld_srid='"+srid+"'");
				 }
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("catch ni submobule =");
		}
    }
	
	 static void generatepdf() {
		// TODO Auto-generated method stub
		new Start_xporting().execute("");
	}

	static class Start_xporting extends AsyncTask <String, String, String> {		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();	
		        p_sv.progress_live=true;
		        p_sv.progress_title="Generating PDF";
			    p_sv.progress_Msg="Please wait..Generating PDF";      
				((Activity) con).startActivityForResult(new Intent(con,Progress_dialogbox.class), Static_variables.progress_code);		       
		    }		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		System.out.println("insid edobanckckc");
			 		Submit_Policyholderinfo();
			 		      Generate_PDF();
					System.out.println("handler="+handler_msg);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	System.out.println("what s han"+handler_msg);
		    	
		    	if(handler_msg==1)
		    	{
		    		success_alert();	
		    	} 
		    	else if(handler_msg==0)
		    	{
		    		failure_alert();		    		
		    	}
		    	
		    }
	}
	private static void Submit_Policyholderinfo() throws IOException, XmlPullParserException {
		
		// TODO Auto-generated method stub
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.OrderInspection + " WHERE fld_inspectorid='"+db.UserId+"' and fld_orderedid='"+selectedho+"'", null);
		System.out.println("select * from " + db.OrderInspection + " WHERE fld_inspectorid='"+db.UserId+"' and fld_orderedid='"+selectedho+"'");
		System.out.println("DDD="+c1.getCount());
			  if(c1.getCount()>0)
		          {
				  c1.moveToFirst();
				  
				  
				  SoapObject request = new SoapObject(wb.NAMESPACE,"OrderInspection");
			        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
			 		envelope.dotNet = true;
			 		
			 		request.addProperty("OrderId",c1.getString(c1.getColumnIndex("fld_orderedid")));
			 		request.addProperty("fld_inspectioncompany",db.decode(c1.getString(c1.getColumnIndex("fld_inspectioncompany"))));
			 		request.addProperty("fld_policynumber",db.decode(c1.getString(c1.getColumnIndex("fld_policynumber"))));
			 		request.addProperty("fld_squarefootage",c1.getString(c1.getColumnIndex("fld_squarefootage")));
			 		request.addProperty("fld_noofstories",c1.getString(c1.getColumnIndex("fld_noofstories")));
			 		request.addProperty("fld_inspectiondate",db.decode(c1.getString(c1.getColumnIndex("fld_inspectiondate"))));
			 		request.addProperty("fld_inspectiontime",db.decode(c1.getString(c1.getColumnIndex("fld_inspectiontime"))));
			 		request.addProperty("fld_yearofconstruction",db.decode(c1.getString(c1.getColumnIndex("fld_yearofconstruction"))));
			 		request.addProperty("fld_otheryearofconstruction",db.decode(c1.getString(c1.getColumnIndex("fld_otheryearofconstruction"))));
			 		request.addProperty("fld_inspectionfee",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionfee"))));
			 		request.addProperty("fld_firstname",db.decode(c1.getString(c1.getColumnIndex("fld_firstname"))));
			 		request.addProperty("fld_lastname",db.decode(c1.getString(c1.getColumnIndex("fld_lastname"))));
			 		request.addProperty("fld_email",db.decode(c1.getString(c1.getColumnIndex("fld_email"))));
			 		request.addProperty("fld_phone",db.decode(c1.getString(c1.getColumnIndex("fld_phone"))));
			 		request.addProperty("fld_inspectionaddress1",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionaddress1"))));
			 		request.addProperty("fld_inspectionaddress2",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionaddress2"))));
			 		request.addProperty("fld_inspectioncity",db.decode(c1.getString(c1.getColumnIndex("fld_inspectioncity"))));
			 		request.addProperty("fld_inspectionstate",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionstate"))));
			 		request.addProperty("fld_inspectioncounty",db.decode(c1.getString(c1.getColumnIndex("fld_inspectioncounty"))));
			 		request.addProperty("fld_zip",db.decode(c1.getString(c1.getColumnIndex("fld_zip"))));
			 		request.addProperty("fld_flag",db.decode(c1.getString(c1.getColumnIndex("fld_flag"))));
			 		request.addProperty("fld_mailingaddress1",db.decode(c1.getString(c1.getColumnIndex("fld_mailingaddress1"))));
			 		request.addProperty("fld_mailingaddress2",db.decode(c1.getString(c1.getColumnIndex("fld_mailingaddress2"))));
			 		request.addProperty("fld_mailingcity",db.decode(c1.getString(c1.getColumnIndex("fld_mailingcity"))));
			 		request.addProperty("fld_mailingstate",db.decode(c1.getString(c1.getColumnIndex("fld_mailingstate"))));
			 		request.addProperty("fld_mailingcounty",db.decode(c1.getString(c1.getColumnIndex("fld_mailingcounty"))));
			 		request.addProperty("fld_mailingzip",db.decode(c1.getString(c1.getColumnIndex("fld_mailingzip"))));
			 		request.addProperty("fld_inspectorid",db.UserId);
			 		request.addProperty("fld_status",1); //For place order 1 = new record placing
			 		
			 		envelope.setOutputSoapObject(request);
			 		System.out.println("request="+request);
			 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
			 		SoapObject response = null;
			        try
			        {
			              androidHttpTransport.call(wb.NAMESPACE+"OrderInspection", envelope);
			              response = (SoapObject)envelope.getResponse();
			              System.out.println("OrderInspection "+response);
			              
			              //statuscode ==0 -> sucess && statuscode !=0 -> failure
			              if(response.getProperty("StatusCode").toString().equals("0"))
			      		  {
			      		  
			          		
			              	 handler_msg=0;
			      		  }
			      		  else
			      		  {
			      			 error_msg = response.getProperty("StatusMessage").toString();
			      			 handler_msg=1;
			      			
			      		  }
			        }
			        catch(Exception e)
			        {
			             e.printStackTrace();
			        }
			        
				  
		          }
			  else
			  {
				  handler_msg=0;
			  }
	 	  
	}
	public static void failure_alert() {
		// TODO Auto-generated method stub
   
		AlertDialog al = new AlertDialog.Builder(con).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public static void success_alert() {
		// TODO Auto-generated method stub
		AlertDialog al = new AlertDialog.Builder(con).setMessage("Inspection Report Downloaded Successfully").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//startActivity(new Intent(Submit_inspection.this,HomeScreen.class));
				handler_msg = 2 ;
				 try
 				 {
					  System.out.println("UPDATE "+db.OrderInspection+ " SET fld_status='3' WHERE fld_orderedid='"+selectedho +"'");
					  
 					db.hi_db.execSQL("UPDATE "+db.OrderInspection+ " SET fld_status='3' WHERE fld_orderedid='"+selectedho +"'");
 					listfromdb();
 				 }
 				 catch (Exception e) {
 					// TODO: handle exception
 				 }
				Cursor c_doc = db.hi_db.rawQuery("select * from " + db.Downloaded_document + " where fld_srid='"+selectedho+"'",null);
				System.out.println("select * from " + db.Downloaded_document + " where fld_srid='"+selectedho+"'");
	            if(c_doc.getCount()>0)
	            {
	            	c_doc.moveToFirst();
	            	String path=db.decode(c_doc.getString(c_doc.getColumnIndex("down_loc_path")));
	            	File f= new File(path);
	            	System.out.println(" comes in pdf view"+f.exists());
	            	if(f.exists())
	            		show_pdf_file(f, path);
	            }
	           c_doc.close();
    		
			}
		}).setTitle("DOWNLOAD SUCCESS").create();
		al.show();
		
		/*AlertDialog al = new AlertDialog.Builder(con).setMessage("You've successfully submitted your Inspection.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				  try
 				 {
					  System.out.println("UPDATE "+db.OrderInspection+ " SET fld_status='3' WHERE fld_orderedid='"+selectedho +"'");
					  
 					db.hi_db.execSQL("UPDATE "+db.OrderInspection+ " SET fld_status='3' WHERE fld_orderedid='"+selectedho +"'");
 				 }
 				 catch (Exception e) {
 					// TODO: handle exception
 				 }
			
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();*/
	}
	public static void show_pdf_file(File file,String path_loc) {
		// TODO Auto-generated method stub
        System.out.println(" called correctly"+file);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
			System.out.println("try");
            (con).startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {System.out.println("catch");
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(con,ViewPdfFile.class);
			intentview.putExtra("path",path_loc);
			(con).startActivity(intentview);
			// finish();
        }
		
	}
	private static void Generate_PDF() throws IOException, XmlPullParserException {
		
		 SoapObject request = new SoapObject(wb.NAMESPACE,"Generate_HomeInspectionPdf");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("Filename","test");
			request.addProperty("Policynumber",policyno);
			request.addProperty("SRID",selectedho);
			
			System.out.println("request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"Generate_HomeInspectionPdf",envelope);
			SoapObject 	result1 = (SoapObject) envelope.getResponse();
			System.out.println("genratePDFimag Stuts "+result1);
			if(result1!=null)
			{
				System.out.println("reusl-"+result1);
					if(result1.getProperty("StatusCode").toString().equals("0"))
					{
						System.out.println("Ddd");
						PDFPATH = result1.getProperty("StatusCode").toString();
							p_sv.progress_Msg="Please wait we are downloading your report, it may take few minutes.";
							if(Download_report(result1.getProperty("PDFURL").toString()))
							{
								System.out.println("success");
								handler_msg=1;
							}
							else
							{
								handler_msg=0;
							}					
					}
					else
					{
						 handler_msg=0;
					}
			}			
			else
			{
				 handler_msg=0;
			}
	}
	private static boolean Download_report(String url) throws IOException, XmlPullParserException {
		// TODO Auto-generated method stub
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();System.out.println("et="+extStorageDirectory);
		//File folder = new File(extStorageDirectory,"DownloadedPdfFile/Dynamic_report_builder");
		String[] path_dir =("DownloadedPdfFile/HI").split("/");
    	File f_par=new File(Environment.getExternalStorageDirectory().toString());
    	/**Creating folder for the path**/
    	for(int i =0;i<path_dir.length;i++)
    	{
    		f_par=new File(f_par,path_dir[i]);
    		f_par.mkdir();
    		//System.out.println(" the path"+path_dir[i]);
    	}
    	/**Creating folder for the path**/
		
		
		
		String Path =extStorageDirectory+"/DownloadedPdfFile/HI/"+selectedho+".pdf";System.out.println("path"+Path);
		/**Creating folder for the path**/
		 file = new File(Path);System.out.println("le"+file);
		 try {
				file.createNewFile();
				        FileOutputStream f = new FileOutputStream(file);
			            URL u = new URL(url);
			            HttpURLConnection c = (HttpURLConnection) u.openConnection();
			            c.setRequestMethod("GET");
			            //c.setDoOutput(true);
			            c.connect();
			 
			            InputStream in = c.getInputStream();
			 
			            byte[] buffer = new byte[1024];
			            int len1 = 0;
			            while ((len1 = in.read(buffer)) > 0) {
			                f.write(buffer, 0, len1);
			            }
			            f.close();
			            Cursor c_doc = db.hi_db.rawQuery("select * from " + db.Downloaded_document + " where fld_srid='"+selectedho+"'",null);
			            String sql="";
			            if(c_doc.getCount()<=0)
			            {
			            	sql=" INSERT INTO "+db.Downloaded_document+" (fld_inspectorid,fld_srid,down_loc_path,fld_down_server_path,down_status,fld_down_created_date) values " +
			            			"('"+db.UserId+"','"+selectedho+"','"+db.encode(Path)+"','"+db.encode(url)+"','true','')";
			            }
			            else
			            {
			            	sql=" UPDATE "+db.Downloaded_document+" SET down_loc_path='"+db.encode(Path)+"',fld_down_server_path='"+db.encode(url)+"'" +
			            			" WHERE fld_srid='"+selectedho+"'";
			            }
			            c_doc.close();
			            System.out.println(" the pdf path="+sql);
			db.hi_db.execSQL(sql);
			} catch (IOException e1) {
				e1.printStackTrace();
				return false;
			}
			
			return true;
	}
	/*public void onActivityResult(int requestCode, int resultCode, Intent data) {

	    super.onActivityResult(requestCode, resultCode, data);
	   
	    System.out.println(" the onActivityResult code "+resultCode+"/"+requestCode+"/"+RESULT_OK);
	  
	    if(requestCode==24)
	    {
	    	
	    	 System.out.println("teststatic=");
	    	 
	    		System.out.println("PDFPATH="+file);
	    		 Uri path = Uri.fromFile(file);
		        	Intent intent = new Intent(Intent.ACTION_VIEW);
		        	intent.setDataAndType(path, "application/pdf");
		        	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	 
	    	 Intent in_compl=new Intent(this,Completed_inspection_report_list.class);
	    	 in_compl.putExtra("Inspection_type_id", insp_temp_id);
	    	 in_compl.putExtra("Inspection_type_name", insp_name);
			 startActivity(in_compl);
	    } 
	}*/
	@Override
    protected void onSaveInstanceState(Bundle outState) {	 
	  
        outState.putString("searchtext", str_seachtext);
        
        super.onSaveInstanceState(outState);
    }
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	str_seachtext = savedInstanceState.getString("searchtext");
    	listfromdb();
    	
         
        super.onRestoreInstanceState(savedInstanceState);
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(CompletedInspection.this, HomeScreen.class));
			finish();
			
		}

		return super.onKeyDown(keyCode, event);
	}
}
