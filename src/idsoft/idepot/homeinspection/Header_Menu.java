package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.text.AttributedCharacterIterator.Attribute;

import android.R.color;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class Header_Menu extends LinearLayout {
	static Context con;
	static int menu_no;
	Button menus[] = new Button[9];
	TextView tvvclink, tvduplicate;
	View v;
	static Cursor ccount,carray,c1;
	static Dialog add_dialog;
	static CommonFunction cf;static int k=0;
	static DatabaseFunction db;
	static String headgettempname="";
	static String headgetcurrentview="";
	static String headgetmodulename="";
	static String headgettempoption="";
	static String headgetselectedho="",appendzoneno="",selectedzoneno="";
	static Spinner spnzoneno;
	static Class moveclass;
	//static Button btnsave;
	static Class genclassnames[]={NewHVAC1.class,NewDuctwork.class,NewSystembrand.class, NewThermostat.class,NewHotwater.class,NewGasLog.class,NewWoodStove.class};
	
	
	public Header_Menu(Context context, int menu, CommonFunction cf, String gettempname, String getcurrentview, String getmodulename, 
			String gettempoption, String getselectedho,DatabaseFunction db) {
		super(context);
		con = context;
		menu_no = menu;
		this.cf = cf;
		this.db =db;
		headgettempname = gettempname;
		headgetcurrentview = getcurrentview;
		headgetmodulename = getmodulename;
		headgettempoption = gettempoption;
		headgetselectedho = getselectedho;
		
		create_menu();
		// TODO Auto-generated constructor stub
	}

	public Header_Menu(Context context, Attribute attr, int menu,
			CommonFunction cf) {
		super(context);
		con = context;
		menu_no = menu;
		this.cf = cf;this.db = db;
		create_menu();
		// TODO Auto-generated constructor stub
	}

	private void create_menu() {
		// TODO Auto-generated method stub
		LayoutInflater ly = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ly.inflate(R.layout.header_menu, this, true);

		menus[0] = (Button) findViewById(R.id.hvac);
		menus[1] = (Button) findViewById(R.id.ductwork);
		menus[2] = (Button) findViewById(R.id.systembrands);
		menus[3] = (Button) findViewById(R.id.thermostats);
		menus[4] = (Button) findViewById(R.id.hotwater);
		menus[5] = (Button) findViewById(R.id.gaslog);
		menus[6] = (Button) findViewById(R.id.woodstove);
		menus[7] = (Button) findViewById(R.id.duplicate);
		menus[8] = (Button) findViewById(R.id.deletehvac);
		if(menu_no==5 || menu_no==6 || menu_no==7)
		{
			menus[7].setVisibility(v.GONE);
			menus[8].setVisibility(v.GONE);
		}
		
			tvvclink = (TextView) findViewById(R.id.vc);
			tvvclink.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					Intent intent = new Intent(con, NewSetVC.class);
					Bundle b2 = new Bundle();
					b2.putString("templatename", headgettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", headgetcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", headgetmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("submodulename", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("tempoption", headgettempoption);			
					intent.putExtras(b6);
					con.startActivity(intent);
					((Activity) con).finish();
					
				  
				}
			});
		
		switch (menu_no) {
		case 1:
			cf.boolhvac=true;
            unselect();
			menus[0].setBackgroundColor(getResources().getColor(R.color.selected_color));
			menus[0].setTypeface(Typeface.DEFAULT_BOLD);
			break;
		case 2:
			cf.boolhvac=false;
			unselect();
			menus[1].setBackgroundColor(getResources().getColor(R.color.selected_color));
			menus[1].setTypeface(Typeface.DEFAULT_BOLD);
			break;
		case 3:
			cf.boolhvac=false;
			unselect();
			menus[2].setBackgroundColor(getResources().getColor(R.color.selected_color));
			menus[2].setTypeface(Typeface.DEFAULT_BOLD);
			break;
		case 4:
			cf.boolhvac=false;
			unselect();
			menus[3].setBackgroundColor(getResources().getColor(R.color.selected_color));
			menus[3].setTypeface(Typeface.DEFAULT_BOLD);
			break;
		case 5:
			cf.boolhvac=false;
			unselect();
			menus[4].setBackgroundColor(getResources().getColor(R.color.selected_color));
			menus[4].setTypeface(Typeface.DEFAULT_BOLD);
			break;
		case 6:
			cf.boolhvac=false;
			unselect();
			menus[5].setBackgroundColor(getResources().getColor(R.color.selected_color));
			menus[5].setTypeface(Typeface.DEFAULT_BOLD);
			break;
		case 7:
			cf.boolhvac=false;
			unselect();
			menus[6].setBackgroundColor(getResources().getColor(R.color.selected_color));
			menus[6].setTypeface(Typeface.DEFAULT_BOLD);
			break;	
		default:
			System.out.println("comes wrong");
			break;
		}
		menus[0].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("tets new hvac");
				Intent intent = new Intent(con, NewHVAC1.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", headgettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", headgetcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", headgetmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("selectedid", headgetselectedho);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", headgettempoption);			
				intent.putExtras(b6);
				con.startActivity(intent);
				((Activity) con).finish();
			}
		});
		
		menus[1].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(con, NewDuctwork.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", headgettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", headgetcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", headgetmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("selectedid", headgetselectedho);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", headgettempoption);			
				intent.putExtras(b6);
				con.startActivity(intent);
				((Activity) con).finish();
			}
		});
		
		menus[2].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(con, NewSystembrand.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", headgettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", headgetcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", headgetmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("selectedid", headgetselectedho);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", headgettempoption);			
				intent.putExtras(b6);
				con.startActivity(intent);
				((Activity) con).finish();
			}
		});
		
		menus[3].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(con, NewThermostat.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", headgettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", headgetcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", headgetmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("selectedid", headgetselectedho);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", headgettempoption);			
				intent.putExtras(b6);
				con.startActivity(intent);
				((Activity) con).finish();
			}
		});
		
		menus[4].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(con, NewHotwater.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", headgettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", headgetcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", headgetmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("selectedid", headgetselectedho);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", headgettempoption);			
				intent.putExtras(b6);
				con.startActivity(intent);
				((Activity) con).finish();
			}
		});
		
		menus[5].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(con, NewGasLog.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", headgettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", headgetcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", headgetmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("selectedid", headgetselectedho);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", headgettempoption);			
				intent.putExtras(b6);
				con.startActivity(intent);
				((Activity) con).finish();
			}
		});
		
		menus[6].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(con, NewWoodStove.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", headgettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", headgetcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", headgetmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("selectedid", headgetselectedho);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", headgettempoption);			
				intent.putExtras(b6);
				con.startActivity(intent);
				((Activity) con).finish();
			}
		});
		menus[7].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				show_alert(1);
			}
		});
		menus[8].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("inside delete");
				show_alert(2);
			/*	AlertDialog.Builder builder = new AlertDialog.Builder(con);
				builder.setMessage("Do you want to delete the all the HVAC details?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										
										try
										{
											db.hi_db.execSQL("Delete from "+db.LoadDuctwork+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+headgetselectedho+"'");
											db.hi_db.execSQL("Delete from "+db.LoadHVAC+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+headgetselectedho+"'");
											db.hi_db.execSQL("Delete from "+db.LoadThermostat+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+headgetselectedho+"'");
											db.hi_db.execSQL("Delete from "+db.LoadSystembrands+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+headgetselectedho+"'");
											goclass(menu_no);
										}
										catch (Exception e) {
											// TODO: handle exception
										}
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				
				AlertDialog al=builder.create();
				al.setTitle("Confirmation");
				al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();*/
				//builder.show();
			}
		});		
	}
	public static void show_alert(final int iden){
		
		// TODO Auto-generated method stub
		Button btnsave;
		k=1;appendzoneno="";
		//add_dialog = new Dialog(con);
		add_dialog = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
		//add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.duplicatealert);System.out.println("duplciate alert");
		if(headgetcurrentview.equals("start")){
			ccount = db.hi_db.rawQuery("select MAX(fld_zoneno) as a from " + db.LoadHVAC + " where fld_srid='"+headgetselectedho+"'", null);
		}
		else
		{
			ccount = db.hi_db.rawQuery("select MAX(fld_zoneno) as a from " + db.CreateHVAC + " where fld_templatename='"+db.encode(headgettempname)+"'", null);
		}
		System.out.println("ccoun="+ccount.getCount());
		ccount.moveToFirst();
		final int maxzoneno = ccount.getInt(ccount.getColumnIndex("a"));System.out.println("mm="+maxzoneno);		
		final int maxzonenosaved= maxzoneno+1;
		
		 btnsave= (Button) add_dialog.findViewById(R.id.duplicate);
		
		if(iden==1)
		{
			((TextView) add_dialog.findViewById(R.id.header)).setText("DUPLICATION");
			((TextView) add_dialog.findViewById(R.id.compinsplabel)).setText("DUPLICATING ZONE "+maxzonenosaved+" TO ZONE");
			btnsave.setText("Duplicate");
			
		}
		else
		{
			((TextView) add_dialog.findViewById(R.id.header)).setText("DELETE");

			((TextView) add_dialog.findViewById(R.id.compinsplabel)).setText("Please Select the Zone No to delete the zone details in HVAC system, Ductwork, System brands and Thermostat tab.");
			btnsave.setText("Delete");
		}
		
		if(headgetcurrentview.equals("start"))
		{
			c1 = db.hi_db.rawQuery("select * from " + db.LoadHVAC + " where fld_srid='"+headgetselectedho+"' " +
					"and fld_zoneno!='--Select--'", null);System.out.println("test="+c1.getCount()+"select * from " + db.LoadHVAC + " where fld_srid='"+headgetselectedho+"' " +
					"and fld_zoneno!='--Select--'");
		}
		else
		{
			c1 = db.hi_db.rawQuery("select * from " + db.CreateHVAC + " where fld_templatename='"+db.encode(headgettempname)+"' " +
					"and fld_zoneno!='--Select--'", null);System.out.println("test="+c1.getCount()+"select * from " + db.LoadHVAC + " where fld_srid='"+headgetselectedho+"' " +
					"and fld_zoneno!='--Select--'");
		}
		
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			int i=0;
			do
			{
				if(c1.getString(c1.getColumnIndex("fld_zoneno")).equals("Other"))
				{
					appendzoneno += c1.getString(c1.getColumnIndex("fld_otherzoneno"))+"~";System.out.println("APPEND="+appendzoneno);
				}
				else
				{
					System.out.println("inside else"+appendzoneno);
					appendzoneno += c1.getString(c1.getColumnIndex("fld_zoneno"))+"~";	System.out.println("ehrre="+appendzoneno);
				}	
				i++;
			}while(c1.moveToNext());
						
			System.out.println("appene="+appendzoneno);
			appendzoneno = "--Select--"+"~"+appendzoneno;
 			String arrzonenospn1[] = appendzoneno.split("~"); 			
			final Spinner spnzoneno=(Spinner) add_dialog.findViewById(R.id.sp1);	
			ArrayAdapter ad1 =new ArrayAdapter(con,android.R.layout.simple_spinner_item, arrzonenospn1);
			ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnzoneno.setAdapter(ad1);
			spnzoneno.setOnItemSelectedListener(new OnItemSelectedListener() 
	        {

	            @Override
	            public void onItemSelected(AdapterView<?> arg0, View arg1,
	                    int arg2, long arg3)
	            {
	            	selectedzoneno= spnzoneno.getSelectedItem().toString();
	            

	        		/*Cursor c1 = db.hi_db.rawQuery("select * from " + db.LoadHVAC + " where fld_srid='"+headgetselectedho+"' " +
	        				"and fld_zoneno='"+maxzonenosaved+"'" +
	        						"", null);System.out.println("test="+c1.getCount()+"select * from " + db.LoadHVAC + " where fld_srid='"+headgetselectedho+"' " +
	        				"and fld_zoneno!='--Select--'");
	        		if(c1.getCount()>0)
	        		{
	        			c1.moveToFirst();
	        			int i=0;
	        			do
	        			{
	        				if(c1.getString(c1.getColumnIndex("fld_zoneno")).equals("Other"))
	        				{
	        					appendzoneno += c1.getString(c1.getColumnIndex("fld_otherzoneno"))+"~";System.out.println("APPEND="+appendzoneno);
	        				}
	        				else
	        				{
	        					System.out.println("inside else"+appendzoneno);
	        					appendzoneno += c1.getString(c1.getColumnIndex("fld_zoneno"))+"~";	System.out.println("ehrre="+appendzoneno);
	        				}	
	        				i++;
	        			}while(c1.moveToNext());
	        						
	        		}
	        		else
	        		{
	        			
	        		}	  */          	
	            }
	            @Override
	            public void onNothingSelected(AdapterView<?> arg0) {
	                // TODO Auto-generated method stub

	            }
	        });

		}
		else
		{
			String arrzonenospn[]={"--Select--"};
			final Spinner spnzoneno=(Spinner) add_dialog.findViewById(R.id.sp1);	
			ArrayAdapter ad1 =new ArrayAdapter(con,android.R.layout.simple_spinner_item, arrzonenospn);
			ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnzoneno.setAdapter(ad1);
		}				
				
		System.out.println("Purchase");
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);System.out.println("Purchascancle");
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				k=0;
				add_dialog.dismiss();
			}
		});
		
	
		btnsave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(iden==1)
				{
					try
					{
						if(!selectedzoneno.equals("--Select--"))
						{
									Cursor c1=null,c2=null,c3=null,c4=null,c5=null,c6=null,c7=null,c8=null;
								if(headgetcurrentview.equals("start"))
								{
									c1 = db.hi_db.rawQuery("select * from " + db.LoadHVAC + " where fld_srid='"+headgetselectedho+"' " +
					        				"and fld_zoneno='"+selectedzoneno+"'", null);	
								}
								else
								{
									c1 = db.hi_db.rawQuery("select * from " + db.CreateHVAC + " where fld_templatename='"+headgettempname+"' " +
					        				"and fld_zoneno='"+selectedzoneno+"'", null);	
								}
								
				        		if(c1.getCount()>0)
				        		{
				        			c1.moveToFirst();
				        			if(headgetcurrentview.equals("start"))
				    				{
				        				 c2 = db.hi_db.rawQuery("select * from " + db.LoadHVAC + " where fld_srid='"+headgetselectedho+"' " +
				                				"and fld_zoneno='"+maxzonenosaved+"'",null);	
				    				}
				    				else
				    				{
				    					c2 = db.hi_db.rawQuery("select * from " + db.CreateHVAC + " where fld_templatename='"+headgettempname+"' " +
				    	        				"and fld_zoneno='"+selectedzoneno+"'", null);	
				    				}
				    				
				        			if(c2.getCount()==0)
				            		{	
				        				if(headgetcurrentview.equals("start"))
				        				{
				            			
					            			db.hi_db.execSQL(" INSERT INTO "+db.LoadHVAC+" (fld_templatename,fld_inspectorid,fld_srid,fld_zoneno,fld_otherzoneno,fld_zonename,fld_otherzonename,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype," +
													"fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures," +
													"fld_tonnage,fld_tonnageother,fld_btu,fld_btuother) VALUES" +
																						    "('"+c1.getString(1)+"','"+db.UserId+"','"+c1.getString(3)+"','"+maxzonenosaved+"','"+c1.getString(9)+"','"+c1.getString(10)+"','"+c1.getString(11)+"','"+c1.getString(4)+"','"+c1.getString(5)+"','"+c1.getString(6)+"','"+c1.getString(7)+"','"+c1.getString(12)+"','"+c1.getString(13)+"','"+c1.getString(14)+"','"+c1.getString(15)+"',"+
													 "'"+c1.getString(16)+"','"+c1.getString(17)+"','"+c1.getString(18)+"','"+c1.getString(19)+"','"+c1.getString(20)+"','"+c1.getString(21)+"','"+c1.getString(22)+"','"+c1.getString(23)+"',"+
												    "'"+c1.getString(24)+"','"+c1.getString(25)+"','"+c1.getString(26)+"','"+c1.getString(27)+"')");	
				        				}
				        				else
				        				{
				        					db.hi_db.execSQL(" INSERT INTO "+db.CreateHVAC+" (fld_templatename,fld_inspectorid,fld_srid,fld_zoneno,fld_otherzoneno,fld_zonename,fld_otherzonename,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype," +
													"fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures," +
													"fld_tonnage,fld_tonnageother,fld_btu,fld_btuother) VALUES" +
																						    "('"+c1.getString(1)+"','"+db.UserId+"','"+c1.getString(3)+"','"+maxzonenosaved+"','"+c1.getString(9)+"','"+c1.getString(10)+"','"+c1.getString(11)+"','"+c1.getString(4)+"','"+c1.getString(5)+"','"+c1.getString(6)+"','"+c1.getString(7)+"','"+c1.getString(12)+"','"+c1.getString(13)+"','"+c1.getString(14)+"','"+c1.getString(15)+"',"+
													 "'"+c1.getString(16)+"','"+c1.getString(17)+"','"+c1.getString(18)+"','"+c1.getString(19)+"','"+c1.getString(20)+"','"+c1.getString(21)+"','"+c1.getString(22)+"','"+c1.getString(23)+"',"+
												    "'"+c1.getString(24)+"','"+c1.getString(25)+"','"+c1.getString(26)+"','"+c1.getString(27)+"')");	
				        				}
				            		
				            		}	
				            		System.out.println("started duct");
				            		
				            		if(headgetcurrentview.equals("start"))
				            		{
				            			 c3 = db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " where fld_srid='"+headgetselectedho+"' " +
				                 				"and fld_zoneno='"+selectedzoneno+"'", null);System.out.println("started duct c3="+c3.getCount());
				            		}
				            		else
				            		{
				            			 c3 = db.hi_db.rawQuery("select * from " + db.CreateDuctwork + " where fld_templatename='"+headgettempname+"' " +
				                  				"and fld_zoneno='"+selectedzoneno+"'", null);System.out.println("started duct c3="+c3.getCount());
				            		}
				            		if(c3.getCount()>0)
				            		{
				            			c3.moveToFirst();
				            			if(headgetcurrentview.equals("start"))
				                		{
				            				 c4 = db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " where fld_srid='"+headgetselectedho+"' " +
				                    				"and fld_zoneno='"+maxzonenosaved+"'",null);	
				                		}
				            			else
				            			{
				            				 c4 = db.hi_db.rawQuery("select * from " + db.CreateDuctwork + " where fld_templatename='"+headgettempname+"' " +
				                    				"and fld_zoneno='"+maxzonenosaved+"'",null);
				            			}
				            			
				            			System.out.println("ccc="+c4.getCount());
				                		if(c4.getCount()==0)
				                		{
				                			if(headgetcurrentview.equals("start"))
				                    		{
				                				db.hi_db.execSQL(" INSERT INTO "+db.LoadDuctwork+" (fld_templatename,fld_inspectorid,fld_srid,fld_zoneno,fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_ductworkcomment) VALUES" +
				    						    "('"+c3.getString(1)+"','"+db.UserId+"','"+c3.getString(3)+"','"+maxzonenosaved+"','"+c3.getString(5)+"','"+c3.getString(6)+"','"+c3.getString(7)+"','"+c3.getString(8)+"','"+c3.getString(9)+"')");
				                    		}
				                			else
				                			{
				                				db.hi_db.execSQL(" INSERT INTO "+db.CreateDuctwork+" (fld_templatename,fld_inspectorid,fld_zoneno,fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_ductworkcomment) VALUES" +
				            						    "('"+c3.getString(1)+"','"+db.UserId+"','"+maxzonenosaved+"','"+c3.getString(4)+"','"+c3.getString(5)+"','"+c3.getString(6)+"','"+c3.getString(7)+"','"+c3.getString(8)+"')");
				                           }
				                			
				                		}
				            		}
				            		
				            		
				            		
				
				            		if(headgetcurrentview.equals("start"))
				            		{
				            			c5 = db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " where fld_srid='"+headgetselectedho+"' " +
				                				"and fld_zoneno='"+selectedzoneno+"'", null);
				            		}
				            		else
				            		{
				            			c5 = db.hi_db.rawQuery("select * from " + db.CreateSystembrands + " where fld_templatename='"+headgettempname+"' " +
				                				"and fld_zoneno='"+selectedzoneno+"'", null);
				            		} 
				            		if(c5.getCount()>0)
				            		{
				            			c5.moveToFirst();
				            			if(headgetcurrentview.equals("start"))
				                		{
				            				 c6 = db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " where fld_srid='"+headgetselectedho+"' " +
				                				"and fld_zoneno='"+maxzonenosaved+"'",null);
				                		}
				            			else
				            			{
				            				 c6 = db.hi_db.rawQuery("select * from " + db.CreateSystembrands + " where fld_templatename='"+headgettempname+"' " +
				                     				"and fld_zoneno='"+maxzonenosaved+"'",null);
				            			}
				            			
				                		if(c6.getCount()==0)
				                		{
				                			if(headgetcurrentview.equals("start"))
				                    		{
				                				
				                			db.hi_db.execSQL(" INSERT INTO "+db.LoadSystembrands+" (fld_templatename,fld_inspectorid,fld_srid,fld_noofzones,fld_zoneno,fld_zonename," +
													"fld_zonenameother,fld_externalunit,fld_internalunit,fld_extunitreplprob,fld_extunitreplprobother,fld_intunitreplprob," +
													"fld_intunitreplprobother,fld_ampextunit,fld_ampintunit,fld_emerheatampdraw,fld_supplytemp,fld_returntemp,fld_systembrandcomments) VALUES" +
														 "('"+c5.getString(1)+"','"+db.UserId+"','"+c5.getString(3)+"','"+c5.getString(7)+"','"+maxzonenosaved+"','"+c5.getString(5)+"'," +
														 		"'"+c5.getString(6)+"','"+c5.getString(8)+"','"+c5.getString(9)+"','"+c5.getString(10)+"','"+c5.getString(11)+"','"+c5.getString(12)+"'," +
				                    					"'"+c5.getString(13)+"','"+c5.getString(14)+"','"+c5.getString(15)+"','"+c5.getString(16)+"','"+c5.getString(17)+"','"+c5.getString(18)+"','"+c5.getString(19)+"')");
				                    		}
				                			else
				                			{
				                				db.hi_db.execSQL(" INSERT INTO "+db.CreateSystembrands+" (fld_templatename,fld_inspectorid,fld_noofzones,fld_zoneno,fld_zonename," +
				    									"fld_zonenameother,fld_externalunit,fld_internalunit,fld_extunitreplprob,fld_extunitreplprobother,fld_intunitreplprob," +
				    									"fld_intunitreplprobother,fld_ampextunit,fld_ampintunit,fld_emerheatampdraw,fld_supplytemp,fld_returntemp,fld_systembrandcomments) VALUES" +
				    										 "('"+c5.getString(1)+"','"+db.UserId+"','"+c5.getString(6)+"','"+maxzonenosaved+"','"+c5.getString(4)+"'," +
				    										 		"'"+c5.getString(5)+"','"+c5.getString(7)+"','"+c5.getString(8)+"','"+c5.getString(9)+"','"+c5.getString(10)+"','"+c5.getString(11)+"'," +
				                        					"'"+c5.getString(12)+"','"+c5.getString(13)+"','"+c5.getString(14)+"','"+c5.getString(15)+"','"+c5.getString(16)+"','"+c5.getString(17)+"','"+c5.getString(18)+"')");
				                    				
				                			}
										}
				            		}
				            		
				            		if(headgetcurrentview.equals("start"))
				            		{
				            			 c7 = db.hi_db.rawQuery("select * from " + db.LoadThermostat + " where fld_srid='"+headgetselectedho+"' " +
				                				"and fld_zoneno='"+selectedzoneno+"'", null);
				            		}
				            		else
				            		{
				            			c7 = db.hi_db.rawQuery("select * from " + db.CreateThermostat + " where fld_templatename='"+headgettempname+"' " +
				                				"and fld_zoneno='"+selectedzoneno+"'", null);
				            		}
				            		
				            		
				            		if(c7.getCount()>0)
				            		{
				            			c7.moveToFirst();
				            			
				            			if(headgetcurrentview.equals("start"))
				                		{
				            				c8 = db.hi_db.rawQuery("select * from " + db.LoadThermostat + " where fld_srid='"+headgetselectedho+"' " +
				                    				"and fld_zoneno='"+maxzonenosaved+"'",null);
				                		}
				            			else
				            			{
				            				c8 = db.hi_db.rawQuery("select * from " + db.CreateThermostat + " where fld_templatename='"+headgettempname+"' " +
				                    				"and fld_zoneno='"+maxzonenosaved+"'",null);
				            			}
				            			 
				                		if(c8.getCount()==0)
				                		{
				                			if(headgetcurrentview.equals("start"))
				                    		{
				                			db.hi_db.execSQL(" INSERT INTO "+db.LoadThermostat+" (fld_templatename,fld_inspectorid,fld_srid,fld_thermotype,fld_thermolocation,fld_zoneno,fld_thermostatcomments) VALUES" +
												    "('"+c7.getString(1)+"','"+db.UserId+"','"+c7.getString(3)+"','"+c7.getString(5)+"','"+c7.getString(6)+"','"+maxzonenosaved+"','')");
				                			
				                    		}
				                			else
				                			{
				                				db.hi_db.execSQL(" INSERT INTO "+db.CreateThermostat+" (fld_templatename,fld_inspectorid,fld_thermotype,fld_thermolocation,fld_zoneno,fld_thermostatcomments) VALUES" +
				    								    "('"+c7.getString(1)+"','"+db.UserId+"','"+c7.getString(4)+"','"+c7.getString(5)+"','"+maxzonenosaved+"','')");
				                    				
				                			}
				    					
				                		}
				            		}
				            		
				            		cf.DisplayToast("HVAC System duplicated successfully.");
				            		goclass(menu_no);
				            		
				        		}
				        		else
				        		{
				        			
				        		}	
						}
						else
						{
							cf.DisplayToast("Please select Zone No");
						}
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("error"+e.getMessage());
					}
				}
				else
				{
					if(selectedzoneno.equals("--Select--"))
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(con);
						builder.setMessage("Do you want to delete the selected Zone details?")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
	
											public void onClick(DialogInterface dialog,
													int id) {
												
												try
												{
													db.hi_db.execSQL("Delete from "+db.LoadDuctwork+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+headgetselectedho+"' and fld_zoneno='"+selectedzoneno+"'");
													db.hi_db.execSQL("Delete from "+db.LoadHVAC+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+headgetselectedho+"' and fld_zoneno='"+selectedzoneno+"'");
													db.hi_db.execSQL("Delete from "+db.LoadThermostat+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+headgetselectedho+"' and fld_zoneno='"+selectedzoneno+"'");
													db.hi_db.execSQL("Delete from "+db.LoadSystembrands+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+headgetselectedho+"' and fld_zoneno='"+selectedzoneno+"'");
													cf.DisplayToast("Deleted successfully.");
													goclass(menu_no);
												}
												catch (Exception e) {
													// TODO: handle exception
												}
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
													int id) {
												dialog.cancel();
											}
										});
						
						AlertDialog al=builder.create();
						al.setTitle("Confirmation");
						al.setIcon(R.drawable.alertmsg);
						al.setCancelable(false);
						al.show();
					}
					else
					{
						cf.DisplayToast("Please select Zone No");	
					}
					
				}
					
			}
		});
		
		add_dialog.show();
}		
	public static void goclass(int i) {


		// TODO Auto-generated method stub
		switch (i) {
		case 1:/** HVAC **/
		      moveclass = genclassnames[0];	
			break;
		case 2:/** DUCTWORK **/
			  moveclass = genclassnames[1];	
			break;
		case 3:/** SYSTEM BRANDS **/
			  moveclass = genclassnames[2];	
			break;
		case 4:/** THERMOSTAT **/
			  moveclass = genclassnames[3];	
			break;
		default:
			break;
			
		}
		
		Intent intent = new Intent(con,moveclass);
		Bundle b2 = new Bundle();
		b2.putString("templatename", headgettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", headgetcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", headgetmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("selectedid", headgetselectedho);			
		intent.putExtras(b5);
		Bundle b6 = new Bundle();
		b6.putString("tempoption", headgettempoption);			
		intent.putExtras(b6);
		con.startActivity(intent);
		((Activity) con).finish();
	}
	private void unselect() {
		// TODO Auto-generated method stub
		menus[0].setBackgroundColor(getResources().getColor(R.color.unselected_color));
		menus[1].setBackgroundColor(getResources().getColor(R.color.unselected_color));
		menus[2].setBackgroundColor(getResources().getColor(R.color.unselected_color));
		menus[3].setBackgroundColor(getResources().getColor(R.color.unselected_color));
		menus[4].setBackgroundColor(getResources().getColor(R.color.unselected_color));
		menus[5].setBackgroundColor(getResources().getColor(R.color.unselected_color));
		menus[6].setBackgroundColor(getResources().getColor(R.color.unselected_color));		
	}
}