package idsoft.idepot.homeinspection;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import android.R.string;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ExportAlert extends Activity {
	DatabaseFunction db;
	CommonFunction cf;

CheckBox ins_chk[]=new CheckBox[16];
ImageView ins_sta_im[]=new ImageView[16];
String insp_boo[] = { "", "", "", "", "","", "", "", "", "","", "", "", "", "",""};

CheckBox main_chk[]=new CheckBox[5];
ImageView main_sta_im[]=new ImageView[5];
String main_boo[] = { "", "", "", "", ""};
RelativeLayout module_layout;


String tmp="";
/****Error log starts****/
LinearLayout par_log,chaild_log;
ImageView log_expend,module_expend,ex_expend_general;

Cursor c1;
String selected;
String result="",s1="",getph="";
int s=0;
String[]   separated=null;
public void onCreate(Bundle savedInstanceState)
{
	System.out.println("ONCREAE EXPORTALER");
	super.onCreate(savedInstanceState);
	setContentView(R.layout.export_alert);
	Bundle b =getIntent().getExtras();
	cf=new CommonFunction(this);
	getph=b.getString("Srid");System.out.println("Gpee="+getph);
	result=b.getString("result");
	
	par_log =(LinearLayout) findViewById(R.id.ex_lay_error_log);
	chaild_log =(LinearLayout) findViewById(R.id.list_error);
	module_layout =(RelativeLayout) findViewById(R.id.ex_module_lin);
	
	ex_expend_general =(ImageView) findViewById(R.id.ex_expend_general);System.out.println("DD");
	ex_expend_general.setOnClickListener(new expend(0));
	System.out.println("SSSS");
	
	
	
/*	ins_sta_im[16]=(ImageView) findViewById(R.id.ex_sta_CSE);
	ins_sta_im[17]=(ImageView) findViewById(R.id.ex_sta_PIA);
	ins_sta_im[18]=(ImageView) findViewById(R.id.ex_sta_INV);
	ins_sta_im[19]=(ImageView) findViewById(R.id.ex_sta_PH);
	ins_sta_im[20]=(ImageView) findViewById(R.id.ex_sta_FB);*/
	
	
	log_expend =(ImageView) findViewById(R.id.ex_expend_show_error);
	log_expend.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(log_expend.getTag().toString().equals("plus"))
			{
				chaild_log.setVisibility(View.VISIBLE);
				log_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
				log_expend.setTag("minus");
			}
			else
			{
				chaild_log.setVisibility(View.GONE);
				log_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				log_expend.setTag("plus");
			}
		}
	});
	set_main_check();
	set_all_check();
	if(result!=null)
	{
		if(result.equals("true"))
		{
			set_results(b);
		}
	}
}
class expend implements OnClickListener
{
	int i;
	expend(int i)
	{
		this.i=i;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		System.out.println("the i value="+i+"v.getTag()="+v.getTag());
		if(v.getTag().equals("plus"))
		{
			module_layout.setVisibility(View.VISIBLE);System.out.println("!11");
			ex_expend_general.setImageDrawable(getResources().getDrawable(R.drawable.minus));System.out.println("222");
			ex_expend_general.setTag("minus");System.out.println("333");
			
		}
		else
		{
			System.out.println("DFDFD");
			module_layout.setVisibility(View.GONE);System.out.println("SDFDSFDS");
			ex_expend_general.setImageDrawable(getResources().getDrawable(R.drawable.plus));
			ex_expend_general.setTag("plus");
			
		}
		System.out.println("DDD");
	}
	
}
public void select_all()
{
	System.out.println("DDDDddddsele");
	for(int m=0;m<ins_chk.length;m++)
	{
		ins_chk[m].setChecked(true);
	}	
	System.out.println("RRR");
}

private void set_results(Bundle data) {
	// TODO Auto-generated method stub
	String[] insp_boo=data.getStringArray("insp");
	String erro_log=data.getString("error");
	String[] errorinsp=data.getStringArray("errorinsp");
	
	if(!erro_log.equals(""))
	{
		par_log.setVisibility(View.VISIBLE);
		String temp[]=erro_log.split("@");
		chaild_log.removeAllViews();
		for(int i=0;i<temp.length;i++)
		{
			TextView tv =new TextView(this);
			tv.setBackgroundColor(getResources().getColor(R.color.Erro_background));
			tv.setTextColor(getResources().getColor(R.color.Erro_text));
			tv.setText(temp[i]);
			tv.setWidth(0);
			View v=new View(this);
			
			chaild_log.addView(tv,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			chaild_log.addView(v,LayoutParams.FILL_PARENT,1);
		}
	}
	else
	{
		par_log.setVisibility(View.GONE);
	}
	
	
	
	
				for(int i=0;i<insp_boo.length;i++)
				{
					boolean head_sta=true;
					if(insp_boo[i].equals("true"))
					{
						ins_chk[i].setChecked(true);
						ins_sta_im[i].setVisibility(View.VISIBLE);
						ins_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
						
						System.out.println("insp_boo="+insp_boo[i]+"errorinsp="+errorinsp[i]);
						if(errorinsp[i].equals("false"))
						{
							System.out.println("eeeeeeeeee="+errorinsp[i]);
							ins_chk[i].setChecked(true);
							head_sta=false;
							ins_sta_im[i].setVisibility(View.VISIBLE);
							ins_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
						}				
											
					}
					else if(insp_boo[i].equals("false"))
					{
						ins_chk[i].setChecked(true);
						head_sta=false;
						ins_sta_im[i].setVisibility(View.VISIBLE);
						ins_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					}
					
					/*
					if(head_sta)
					{
						ins_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					}
					else
					{
						ins_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					}*/					
				}
}
private void set_all_check() 
{
	ins_chk[0]=(CheckBox) findViewById(R.id.ex_chk_structure);
	ins_chk[1]=(CheckBox) findViewById(R.id.ex_chk_exterior);
	ins_chk[2]=(CheckBox) findViewById(R.id.ex_chk_roofing);
	ins_chk[3]=(CheckBox) findViewById(R.id.ex_chk_attic);
	ins_chk[4]=(CheckBox) findViewById(R.id.ex_chk_garagereport);	
	ins_chk[5]=(CheckBox) findViewById(R.id.ex_chk_plumbingsystem);
	ins_chk[6]=(CheckBox) findViewById(R.id.ex_chk_electricalsystem);
	ins_chk[7]=(CheckBox) findViewById(R.id.ex_chk_hvac);
	ins_chk[8]=(CheckBox) findViewById(R.id.ex_chk_kitchen);
	ins_chk[9]=(CheckBox) findViewById(R.id.ex_chk_appliances);
	ins_chk[10]=(CheckBox) findViewById(R.id.ex_chk_bathroom);
	ins_chk[11]=(CheckBox) findViewById(R.id.ex_chk_interior);
	ins_chk[12]=(CheckBox) findViewById(R.id.ex_chk_porchesscreensenclos);	
	ins_chk[13]=(CheckBox) findViewById(R.id.ex_chk_swimmingpools);
	ins_chk[14]=(CheckBox) findViewById(R.id.ex_chk_waterwells);	
	ins_chk[15]=(CheckBox) findViewById(R.id.ex_chk_cabanappolhouse);
	
	
	
	ins_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_structure);System.out.println("SSSSdsds");
	ins_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_exterior);
	ins_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_roofing);
	ins_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_attic);
	ins_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_garagereport);System.out.println("SSS");
	ins_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_plumbingsystem);System.out.println("PPP");
	ins_sta_im[6]=(ImageView) findViewById(R.id.ex_sta_electricalsystem);System.out.println("EEE");
	ins_sta_im[7]=(ImageView) findViewById(R.id.ex_sta_hvac);
	ins_sta_im[8]=(ImageView) findViewById(R.id.ex_sta_kitchen);
	ins_sta_im[9]=(ImageView) findViewById(R.id.ex_sta_appliances);
	ins_sta_im[10]=(ImageView) findViewById(R.id.ex_sta_bathroom);
	ins_sta_im[11]=(ImageView) findViewById(R.id.ex_sta_interior);	
	ins_sta_im[12]=(ImageView) findViewById(R.id.ex_sta_swimmingpools);
	ins_sta_im[13]=(ImageView) findViewById(R.id.ex_sta_swimmingpools);
	ins_sta_im[14]=(ImageView) findViewById(R.id.ex_sta_waterwells);
	ins_sta_im[15]=(ImageView) findViewById(R.id.ex_sta_cabanappolhouse);
	
	/*ins_chk[16]=(CheckBox) findViewById(R.id.ex_chk_CSE);	
	ins_chk[17]=(CheckBox) findViewById(R.id.ex_chk_PIA);
	ins_chk[18]=(CheckBox) findViewById(R.id.ex_chk_Invoice);	
	ins_chk[19]=(CheckBox) findViewById(R.id.ex_chk_PH);
	ins_chk[20]=(CheckBox) findViewById(R.id.ex_chk_FB);*/	
}

private void set_main_check() 
{
	//main_chk[0]=(CheckBox) findViewById(R.id.ex_chk_module);
	main_chk[0]=(CheckBox) findViewById(R.id.ex_chk_CSE);
	main_chk[1]=(CheckBox) findViewById(R.id.ex_chk_PIA);
	main_chk[2]=(CheckBox) findViewById(R.id.ex_chk_Invoice);	
	main_chk[3]=(CheckBox) findViewById(R.id.ex_chk_PH);
	main_chk[4]=(CheckBox) findViewById(R.id.ex_chk_FB);
	
}
public void clicker(View v)
{
	System.out.println("v.getD="+v.getId());
	
	switch(v.getId())
	{

		case R.id.clear:
			clear_all_selection("ALL");
		break;
		case R.id.close:
			System.out.println("insid close");
			Intent in1 = new Intent(this, LoadModules.class);System.out.println("22 close");
			in1.putExtra("selectedid", getph);
			startActivity(in1);System.out.println("close start");
			finish();
			
		break;
		case R.id.cancel:
			Intent in2 = new Intent(this, LoadModules.class);
			in2.putExtra("selectedid", getph);
			startActivity(in2);
			finish();			
		break;
		case R.id.export:
			for(int i =0;i<ins_chk.length;i++)
			{
				System.out.println("i="+i);
				if(ins_chk[i].isChecked())
				{
					System.out.println(i+"is checked");
					send_export_insp();
					return;
				}
			}
			for(int i =0;i<main_chk.length;i++)
			{
				System.out.println("maini="+i);
				if(main_chk[i].isChecked())
				{
					System.out.println(i+"is checked");
					send_export_insp();
					return;
				}
			}
			cf.DisplayToast("Please select altleast one module to export");
		break;
		case R.id.ex_chk_module:
			if(((CheckBox)v).isChecked())
			{
				module_layout.setVisibility(View.VISIBLE);System.out.println("chk module");
				ex_expend_general.setImageDrawable(getResources().getDrawable(R.drawable.minus));System.out.println("egeneral");
				ex_expend_general.setTag("minus");System.out.println("minuss ");
				select_all();
			}
			else
			{
				clear_all_selection();
			}
		break;
		
	}
}
private void clear_all_selection(String insp) {
	// TODO Auto-generated method stub
		for(int i=0;i<ins_chk.length;i++)
		{
			ins_chk[i].setChecked(false);
		}
		
		ins_chk[0].setChecked(false);
		for(int i=0;i<main_chk.length;i++)
		{
			main_chk[i].setChecked(false);
		}
		((CheckBox)findViewById(R.id.ex_chk_module)).setChecked(false);
	
	
}
	private void send_export_insp() {
	// TODO Auto-generated method stub) 
		//System.out.println("send export info");
			for(int m=0;m<ins_chk.length;m++)
			{
						if(ins_chk[m].isChecked())
						{
							insp_boo[m]="true";
						}
						else
						{
							insp_boo[m]="";
						}
					//	System.out.println("insi="+insp_boo[m]);
			}
			
			for(int m=0;m<main_chk.length;m++)
			{
						if(main_chk[m].isChecked())
						{
							main_boo[m]="true";
						}
						else
						{
							main_boo[m]="";
						}
						//System.out.println("main="+main_boo[m]);
			}
			
			Intent n= new Intent();
			n.putExtra("insp", insp_boo);
			n.putExtra("main", main_boo);
			setResult(RESULT_OK,n);
			finish();
	}
	private void clear_all_selection()
	{
		for(int m=0;m<ins_chk.length;m++)
		{
			ins_chk[m].setChecked(false);
		}	
		((CheckBox)findViewById(R.id.ex_chk_module)).setChecked(false);
	}

	public void module_click(View v)
	{
		for(int m=0;m<ins_chk.length;m++)
		{
			if(ins_chk[m].isChecked())
			{
				ins_chk[m].setChecked(true);
			}
		}	
		
		for(int m=0;m<main_chk.length;m++)
		{
			if(main_chk[m].isChecked())
			{
				main_chk[m].setChecked(true);
			}
		}	
	}
	
	/*@Override
    protected void onSaveInstanceState(Bundle outState) {	 
	  
        outState.putInt("exportalert", LoadModules.exportalert);
        outState.putStringArray("insp",insp_boo);
        super.onSaveInstanceState(outState);
    }
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	System.out.println("ONESTRR");
    	LoadModules.exportalert = savedInstanceState.getInt("exportalert");
    	System.out.println("seconree"+LoadModules.exportalert);
    	if(LoadModules.exportalert==2)
    	{
    		Intent n= new Intent();
			n.putExtra("insp", savedInstanceState.getStringArray("insp"));
			setResult(RESULT_OK,n);
			finish();
    	}
    	
         
        super.onRestoreInstanceState(savedInstanceState);
    }*/
}