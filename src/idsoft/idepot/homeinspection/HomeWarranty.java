package idsoft.idepot.homeinspection;



import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class HomeWarranty extends Activity{
	String getph="",strss="",inspectorid="";
	RadioGroup rdg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		
		    Bundle b = this.getIntent().getExtras();
			getph = b.getString("selectedid");
			inspectorid = b.getString("inspectorid");
			
			setContentView(R.layout.warrantyinfo);
			
			 rdg = (RadioGroup)findViewById(R.id.homerdg);
			((RadioButton)rdg.findViewWithTag("Yes")).setChecked(true);
			
			Button btnclose = (Button)findViewById(R.id.close);
			btnclose.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(HomeWarranty.this, LoadModules.class);
					Bundle b2 = new Bundle();
					b2.putString("selectedid", getph);		
					b2.putString("inspectorid", inspectorid);
					intent.putExtras(b2);
					startActivity(intent);
					finish();
				}
			});
			
			TextView txt = (TextView)findViewById(R.id.txt);
			String str = "<b>This home is eligible for a one year Home Warranty Offer!. Items included in the one year home warranty if purchased are:\n</b>" +
					"\t Furnaces, Boilers, Heat Pumps, Central Air Conditioners, Electrical Systems, Thermostats, Water Heater System, Plumbing, Polybutylene Lines, " +
					"Sump Pumps, Whirlpools, Dishwasher, Food Waste Disposer, Cooking Range/Oven, Microwave, Kitchen Refrigerator, Trash Compactor, " +
					"Plumbing Fixtures and Faucets, Domestic Water Softner, Well Water System, Fireplace Gas Burner, Attic and Exhausts Fans, Humidifiers, Dehumidifiers, " +
					"Lighting Fixtures, Septic Lines, Ejector Pump";
			txt.setText(Html.fromHtml(str));
			
			rdg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					RadioButton rd = (RadioButton) group.findViewById(checkedId);
					strss = rd.getText().toString();
					
				}
			});
	}
	
	public void onSaveInstanceState(Bundle outState) {
		 
        outState.putString("radio", strss);
        super.onSaveInstanceState(outState);
	}
	
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		strss = savedInstanceState.getString("radio");
		
		((RadioButton)rdg.findViewWithTag(strss)).setChecked(true);
		super.onRestoreInstanceState(savedInstanceState);
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	}
}
