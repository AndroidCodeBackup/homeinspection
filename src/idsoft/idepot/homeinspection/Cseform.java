package idsoft.idepot.homeinspection;

import java.io.ByteArrayOutputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.idepot.homeinspection.LoadSubModules.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.GraphicsView;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class Cseform extends Activity {
	DatabaseFunction db;
	CommonFunction cf;
	Webservice_config wb;
	String inspectorid="",srid="",str="",presentatinsptxt="",title="",error_msg="",strintitials,insparrivaltxt="",inspatticaccessopeningstxt="",filepath="",selectedImagePath="",picname="",
			inspcallpriorarrivingtxt="",inspidbadgetxt="",inspreferaltxt="",inspcourteoustxt="",inspquestionstxt="",inspshoescleantxt="",inspreviewtxt="",extStorageDirectory="",otherpresent="";
	ScrollView parentscr;
	String rateprofessionaltxt="",inspdate="",fullpath="";
	GraphicsView initialview;
	boolean initialbool = false,insparrivalbool=false,inspcallpriorarrivingbool=false,inspidbadgebool=false,inspatticaccessopeningsbool=false,
	        inspreviewbool=false,inspshoescleanbool=false,inspquestionsbool=false,inspcourteousbool=false,inspreferalbool=false;
	RadioGroup insparrivalrdgp,inspcallpriorarrivingrdgp,inspidbadgerdgp,inspatticaccessopeningsrdgp,inspreviewrdgp,
	inspshoescleanrdgp,inspquestionsrdgp,inspcourteousrdgp,inspreferalrdgp;
	Progress_staticvalues p_sv;
	private int handler_msg=2;
	int[] chkpresentid ={R.id.chk_policyholder,R.id.chk_rep,R.id.contractor,R.id.chk_client,R.id.chk_other};
	CheckBox[] chkpresent = new CheckBox[chkpresentid.length];
	View v;
	BitmapDrawable bmdregister;
	DatePickerDialog datePicker;
	static int ot=0;
	byte[] initialsignature,byteArray;
	RatingBar ratingbarprofessional;
	OutputStream outStream;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 db = new DatabaseFunction(this);
		 cf = new CommonFunction(this);
		 wb = new Webservice_config(this);
		
		 
		 Bundle b = this.getIntent().getExtras();
		 if(b!=null)
		 {
		   inspectorid = b.getString("inspectorid");
		   srid = b.getString("srid");
		 }
		 setContentView(R.layout.activity_cse);		
		 
		 for(int i=0;i<chkpresentid.length;i++)
		 {
			 chkpresent[i] = (CheckBox)findViewById(chkpresentid[i]);
			 
		 }
		
		 
		 Button btnback = (Button)findViewById(R.id.btn_back);
		 btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
				
			}
		});
		 db.userid();
		 TextView txtnote = (TextView)findViewById(R.id.note);
		 String source = "Dear Property Owner,"+"<br/>Thank you for allowing us access to your property to conduct your building inspection. "+"<br/><br/> Your feedback is very important to" +
		 		" us, and enables us and our field representatives, to maintain the level of professionalism, quality and customer service you deserve."+"<br/><br/> Thank you in advance for your cooperation.";
		 txtnote.setText(Html.fromHtml(source));
		 
		 db.CreateTable(28);
		 db.CreateTable(36);
		 
		 try
			{
				 Cursor cur = db.hi_db.rawQuery("select * from " + db.OrderInspection + " where fld_inspectorid='"+inspectorid+"' and fld_orderedid='"+srid+"' and fld_status='1'",null);
				 if(cur.getCount()>0)
				 {
					 cur.moveToFirst();
					 inspdate = cur.getString(cur.getColumnIndex("fld_inspectiondate"));
					 ((TextView)findViewById(R.id.inspectiondate)).setText(inspdate);	 
				 }
			}
		 catch (Exception e) {
			// TODO: handle exception
		}
		 ((EditText)findViewById(R.id.inspectorname)).setText(db.Userfirstname+" "+db.Userlastname);
		 
		 parentscr = (ScrollView)findViewById(R.id.scr);
			
		 initialview = (GraphicsView)findViewById(R.id.initials);
		 initialview.setDrawingCacheEnabled(true);
			
		 initialview.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					parentscr.requestDisallowInterceptTouchEvent(true);
					initialbool = true;
					return false;
				}
			});
		 
		 
		 Button btnsubmit = (Button)findViewById(R.id.submit);
		 btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 presentatinsptxt= cf.getselected_chk(chkpresent);
				 String presentothrval =(chkpresent[chkpresent.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.other)).getText().toString():""; 
				 presentatinsptxt+=presentothrval;
					
				 
					 if(chkpresent[4].isChecked())
					 {
						 if(((EditText)findViewById(R.id.other)).getText().toString().trim().equals(""))
						 {
							 cf.DisplayToast("Please enter the other text for Present at Inspection");
							 ((EditText)findViewById(R.id.other)).requestFocus();
						 }
						 else
						 {
							 submitdata();
						 }
						
					 }
					 else
					 {
						submitdata();
					 }
					
					/*if(cf.isInternetOn())
					  {
						   new Start_xporting().execute("");
					  }
					  else
					  {
						  
						  cf.DisplayToast("Please enable your internet connection.");
						  
					  }*/
				}
		});
		
		 Button btn_signclear = (Button)findViewById(R.id.btn_signatureclear);
		 btn_signclear.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					signclear();

				}
			});
		 	 
		
		 insparrivalrdgp = (RadioGroup)findViewById(R.id.insparrival);
		 insparrivalrdgp.setOnCheckedChangeListener(new checklistenetr(2));
		 
		 inspcallpriorarrivingrdgp = (RadioGroup)findViewById(R.id.inspcallpriorarriving);		 
		 inspcallpriorarrivingrdgp.setOnCheckedChangeListener(new checklistenetr(3));
		 
		 inspidbadgerdgp = (RadioGroup)findViewById(R.id.inspidbadge);
		 inspidbadgerdgp.setOnCheckedChangeListener(new checklistenetr(4));
		 
		 inspatticaccessopeningsrdgp = (RadioGroup)findViewById(R.id.inspatticaccessopenings);
		 inspatticaccessopeningsrdgp.setOnCheckedChangeListener(new checklistenetr(5));
		 
		 inspreviewrdgp = (RadioGroup)findViewById(R.id.inspreview);
		 inspreviewrdgp.setOnCheckedChangeListener(new checklistenetr(6));
		 
		 inspshoescleanrdgp = (RadioGroup)findViewById(R.id.inspshoesclean);
		 inspshoescleanrdgp.setOnCheckedChangeListener(new checklistenetr(7));
		 
		 inspquestionsrdgp = (RadioGroup)findViewById(R.id.inspquestions);
		 inspquestionsrdgp.setOnCheckedChangeListener(new checklistenetr(8));
		 
		 inspcourteousrdgp = (RadioGroup)findViewById(R.id.inspcourteous);
		 inspcourteousrdgp.setOnCheckedChangeListener(new checklistenetr(9));
		 
		 inspreferalrdgp = (RadioGroup)findViewById(R.id.inspreferal);
		 inspreferalrdgp.setOnCheckedChangeListener(new checklistenetr(10));
		 
		 ratingbarprofessional = (RatingBar) findViewById(R.id.rating_professional);
		 ratingbarprofessional.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

				@Override
				public void onRatingChanged(RatingBar ratingBar,
						float rating, boolean fromUser) {
					// TODO Auto-generated method stub
					System.out.println("the rating is " + rating);
					rateprofessionaltxt = String.valueOf(rating);

				}
			});
		 
		 
		 
		 ((Button)findViewById(R.id.orderinsp_getdate)).setOnClickListener(new OnClickListener() {
	 			
	 			@Override
	 			public void onClick(View v) {
	 				// TODO Auto-generated method stub
	 				showDialog(0);
	 			}
	 		});
		 
		 ((Button)findViewById(R.id.clear)).setOnClickListener(new OnClickListener() {
	 			
	 			@Override
	 			public void onClick(View v) {
	 				((EditText)findViewById(R.id.inspectorname)).setText(db.Userfirstname+" "+db.Userlastname);
	 				 ((TextView)findViewById(R.id.inspectiondate)).setText(inspdate);
	 				 ((EditText)findViewById(R.id.reportno)).setText("");
	 				 for(int i=0;i<chkpresentid.length;i++)
	 				 {
	 					 chkpresent[i].setChecked(false);
	 				 }
	 				 Cseform.this.ot=0;
	 				((CheckBox)findViewById(R.id.chk_client)).setChecked(true);
	 				((EditText)findViewById(R.id.other)).setVisibility(v.GONE);otherpresent="";
	 				((EditText)findViewById(R.id.other)).setText("");
	 				insparrivalbool=true;inspcallpriorarrivingbool=true;
	 				inspidbadgebool=true;inspatticaccessopeningsbool=true;
	 				inspreviewbool=true;inspshoescleanbool=true;
	 				inspquestionsbool=true;inspcourteousbool=true;
	 				inspreferalbool=true;
	 				if(insparrivalbool)
	 				{
	 					try
	 					{
	 				       insparrivalrdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				if(inspcallpriorarrivingbool)
	 				{
	 					try
	 					{
	 				        inspcallpriorarrivingrdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				if(inspidbadgebool)
	 				{
	 					try
	 					{
	 				        inspidbadgerdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				if(inspatticaccessopeningsbool)
	 				{
	 					try
	 					{
	 						inspatticaccessopeningsrdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				if(inspreviewbool)
	 				{
	 					try
	 					{
	 						inspreviewrdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				if(inspshoescleanbool)
	 				{
	 					try
	 					{
	 						inspshoescleanrdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				if(inspquestionsbool)
	 				{
	 					try
	 					{
	 						inspquestionsrdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				if(inspcourteousbool)
	 				{
	 					try
	 					{
	 						inspcourteousrdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				if(inspreferalbool)
	 				{
	 					try
	 					{
	 						inspreferalrdgp.clearCheck();
	 					}
	 					catch (Exception e) {
							// TODO: handle exception
						}
	 				}
	 				ratingbarprofessional.setRating(0);
	 				((EditText)findViewById(R.id.comments)).setText("");
	 				((EditText)findViewById(R.id.etdate)).setText("");
	 				signclear();
	 				
	 				presentatinsptxt="";
	 				insparrivaltxt="";
	 				inspcallpriorarrivingtxt="";
	 				inspidbadgetxt="";
	 				inspatticaccessopeningstxt="";
	 				
	 				inspreviewtxt="";
	 				inspshoescleantxt="";
	 				inspquestionstxt="";
	 				inspcourteoustxt="";
	 			    inspreferaltxt="";
	 			}
		 });
		 
		 showsavedvalues();
		 
	}
	
	

	protected void submitdata() {
		// TODO Auto-generated method stub
		String et_inspectorname = ((EditText)findViewById(R.id.inspectorname)).getText().toString();
		String et_inspectiondate = ((TextView)findViewById(R.id.inspectiondate)).getText().toString();
		String et_reportno = ((EditText)findViewById(R.id.reportno)).getText().toString();
		String et_other="";
		if(chkpresent[4].isChecked())
		{
			et_other = ((EditText)findViewById(R.id.other)).getText().toString();
		}
		String et_comments = ((EditText)findViewById(R.id.comments)).getText().toString();
		String et_submitdate = ((EditText)findViewById(R.id.etdate)).getText().toString();
		
		if (GraphicsView.s == 1) 
		{
			//initialview.setBackgroundColor(Color.TRANSPARENT);
			signature();
		}

		
		try
		{
			 Cursor cur = db.hi_db.rawQuery("select * from " + db.CSE + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
			 if(cur.getCount()>0)
			 {
				 System.out.println("UPDATE "+ db.CSE +"  SET fld_inspectorname='"+db.encode(et_inspectorname) + "',fld_inspectiondate='"+et_inspectiondate+"',fld_reportno='"+db.encode(et_reportno)+"'," +
	            			"fld_present='"+presentatinsptxt + "',fld_otherpresent='"+ db.encode(et_other) + "',fld_arrive='"+ insparrivaltxt + "',fld_prior='"+inspcallpriorarrivingtxt + "'," +
	            			"fld_badge='"+ inspidbadgetxt + "',fld_shoes='"+inspatticaccessopeningstxt+ "',fld_listen='"+inspreviewtxt + "',fld_preinspection='"+inspshoescleantxt+"'," +
	            			"fld_difference='"+inspquestionstxt+"',fld_final='"+inspcourteoustxt+"',fld_confident='"+inspreferaltxt+"'," +
	            			"fld_rate='"+rateprofessionaltxt+"',fld_comments='"+db.encode(et_comments)+"',fld_signature='"+db.encode(fullpath)+"'," +
	            			"fld_date='"+et_submitdate+"' where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'");
				 db.hi_db.execSQL("UPDATE "+ db.CSE +"  SET fld_inspectorname='"+db.encode(et_inspectorname) + "',fld_inspectiondate='"+et_inspectiondate+"',fld_reportno='"+db.encode(et_reportno)+"'," +
	            			"fld_present='"+presentatinsptxt + "',fld_otherpresent='"+ db.encode(et_other) + "',fld_arrive='"+ insparrivaltxt + "',fld_prior='"+inspcallpriorarrivingtxt + "'," +
	            			"fld_badge='"+ inspidbadgetxt + "',fld_shoes='"+inspatticaccessopeningstxt+ "',fld_listen='"+inspreviewtxt + "',fld_preinspection='"+inspshoescleantxt+"'," +
	            			"fld_difference='"+inspquestionstxt+"',fld_final='"+inspcourteoustxt+"',fld_confident='"+inspreferaltxt+"'," +
	            			"fld_rate='"+rateprofessionaltxt+"',fld_comments='"+db.encode(et_comments)+"',fld_signature='"+db.encode(fullpath)+"'," +
	            			"fld_date='"+et_submitdate+"' where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'");
			 }
			 else
			 {
				 db.hi_db.execSQL("INSERT INTO "
	  						+ db.CSE
	  						+ " (fld_inspectorid,fld_srid,fld_inspectorname,fld_inspectiondate,fld_reportno,fld_present,fld_otherpresent,fld_arrive,fld_prior,fld_badge,fld_shoes,fld_listen," +
	  						"fld_preinspection,fld_difference,fld_final,fld_confident,fld_rate,fld_comments,fld_signature,fld_date)"
	  						+ " VALUES ('" + inspectorid + "','"+srid+ "','"+db.encode(et_inspectorname)+"','"+et_inspectiondate+"',"+
	  						"'"+db.encode(et_reportno) + "','"+ presentatinsptxt + "','"+ db.encode(et_other) + "','"+insparrivaltxt + "',"+
	  						"'"+ inspcallpriorarrivingtxt + "','"+inspidbadgetxt+ "','"+inspatticaccessopeningstxt + "','"+inspreviewtxt+"','"+inspshoescleantxt+"','"+inspquestionstxt+"'," +
	  						"'"+inspcourteoustxt+"','"+inspreferaltxt+"','"+rateprofessionaltxt+"','"+db.encode(et_comments)+"'," +
	  						"'"+db.encode(fullpath)+"','"+et_submitdate+"')");
	  		  
			 }
			 cf.DisplayToast("Customer Evaluation Survey has been submitted successfully");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.chk_other:
			   if(chkpresent[4].isChecked())
			  {
				   Cseform.this.ot=1;
				   ((EditText)findViewById(R.id.other)).setVisibility(v.VISIBLE);
				   ((EditText)findViewById(R.id.other)).requestFocus();
				  
			  }
			  else
			  {
				  
				  ((EditText)findViewById(R.id.other)).setText("");
				  ((EditText)findViewById(R.id.other)).setVisibility(v.GONE);
				  Cseform.this.ot=0;
			  }
			break;
		}
	}
	 class checklistner implements OnClickListener	  {


		  private static final int visibility = 0;

		@Override
		  public void onClick(View v) {

		   // TODO Auto-generated method stub
			  switch(v.getId()){
			  
			  }
		}
	 }
	protected void signature() {
		// TODO Auto-generated method stub
		Bitmap bm = initialview.getDrawingCache();								
		if(bm!=null)
		{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
			byteArray = stream.toByteArray();					
			
			 String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
			 fullpath = extStorageDirectory+"/HI/CSESignature/"+inspectorid+"/"+srid+"/"+ "CSESignature"+"_"+srid+".jpg";
			 String st = "/HI/CSESignature/"+inspectorid+"/"+srid+"/";
			 File folder = new File(extStorageDirectory,st);
			 if(!folder.exists())
			 {
			    folder.mkdirs();
			 }
			
			
			String _fileName = "CSESignature"+"_"+srid;
			filepath = _fileName.toString() + ".jpg";		
			
			File file = new File(folder, String.valueOf(filepath));
			
	  	System.out.println("_file="+file.exists());
		
		try {
		
			outStream = new FileOutputStream(file);
			
			outStream.write(byteArray);
			outStream.flush();
			outStream.close();
			
			
			
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("IOException"+e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("ee"+e.getMessage());
			e.printStackTrace();
		}
		
			selectedImagePath = filepath.toString();
			String[] bits = selectedImagePath.split("/");
			picname = bits[bits.length - 1];
			
		}
		
	}
	private void showsavedvalues() {
		// TODO Auto-generated method stub
		System.out.println("inside");
		try
		{
			 Cursor cur = db.hi_db.rawQuery("select * from " + db.CSE + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 ((EditText)findViewById(R.id.inspectorname)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_inspectorname"))));
				 if(cur.getString(cur.getColumnIndex("fld_inspectiondate")).equals(""))
				 {
					 ((TextView)findViewById(R.id.inspectiondate)).setText("N/A");	 
				 }
				 else
				 {
					 ((TextView)findViewById(R.id.inspectiondate)).setText(cur.getString(cur.getColumnIndex("fld_inspectiondate")));
				 }
				 
				 ((EditText)findViewById(R.id.reportno)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_reportno"))));
				 System.out.println("reportno");
				 presentatinsptxt = cur.getString(cur.getColumnIndex("fld_present"));
				 System.out.println("presentatinsptxt="+presentatinsptxt);
				//System.out.println();
				 if(presentatinsptxt.equals(""))
				 {
					 ((CheckBox)findViewById(R.id.chk_client)).setChecked(true);
				 }
				 else
				 {
					cf.setvaluechk1(presentatinsptxt,chkpresent,((EditText)findViewById(R.id.other)));
				 }
				 
				 otherpresent = db.decode(cur.getString(cur.getColumnIndex("fld_otherpresent")));
				 if(!otherpresent.trim().equals(""))
				 {
					 chkpresent[4].setChecked(true);
					 str ="Other";
					
					 ((EditText)findViewById(R.id.other)).setText(otherpresent);
				 }
				 System.out.println("otsave="+Cseform.this.ot);
				 if(Cseform.this.ot==1)
				 {
					 ((EditText)findViewById(R.id.other)).setVisibility(v.VISIBLE);
				 }
				 insparrivaltxt = cur.getString(cur.getColumnIndex("fld_arrive"));
				 if(!insparrivaltxt.equals(""))
				 {
				    ((RadioButton)insparrivalrdgp.findViewWithTag(insparrivaltxt)).setChecked(true);
				 }
				 
				 inspcallpriorarrivingtxt = cur.getString(cur.getColumnIndex("fld_prior"));
				 if(!inspcallpriorarrivingtxt.equals(""))
				 {
				    ((RadioButton)inspcallpriorarrivingrdgp.findViewWithTag(inspcallpriorarrivingtxt)).setChecked(true);
				 }
				 
				 inspidbadgetxt = cur.getString(cur.getColumnIndex("fld_badge"));
				 if(!inspidbadgetxt.equals(""))
				 {
				  ((RadioButton)inspidbadgerdgp.findViewWithTag(inspidbadgetxt)).setChecked(true);
				 }
				 
				 inspatticaccessopeningstxt = cur.getString(cur.getColumnIndex("fld_shoes"));
				 if(!inspatticaccessopeningstxt.equals(""))
				 {
				   ((RadioButton)inspatticaccessopeningsrdgp.findViewWithTag(inspatticaccessopeningstxt)).setChecked(true);
				 }
				 
				 inspreviewtxt = cur.getString(cur.getColumnIndex("fld_listen"));
				 if(!inspreviewtxt.equals(""))
				 {
					 ((RadioButton)inspreviewrdgp.findViewWithTag(inspreviewtxt)).setChecked(true);
				 }
				 
				 inspshoescleantxt = cur.getString(cur.getColumnIndex("fld_preinspection"));
				 if(!inspshoescleantxt.equals(""))
				 {
					 ((RadioButton)inspshoescleanrdgp.findViewWithTag(inspshoescleantxt)).setChecked(true);
				 }
				 
				 inspquestionstxt = cur.getString(cur.getColumnIndex("fld_difference"));
				 if(!inspquestionstxt.equals(""))
				 {
					 ((RadioButton)inspquestionsrdgp.findViewWithTag(inspquestionstxt)).setChecked(true);
				 }
				 
				 inspcourteoustxt = cur.getString(cur.getColumnIndex("fld_final"));
				 if(!inspcourteoustxt.equals(""))
				 {
					 ((RadioButton)inspcourteousrdgp.findViewWithTag(inspcourteoustxt)).setChecked(true);
				 }
				 
				 inspreferaltxt = cur.getString(cur.getColumnIndex("fld_confident"));
				 if(!inspreferaltxt.equals(""))
				 {
					 ((RadioButton)inspreferalrdgp.findViewWithTag(inspreferaltxt)).setChecked(true);
				 }
				 
				 rateprofessionaltxt = cur.getString(cur.getColumnIndex("fld_rate"));
				 if(!rateprofessionaltxt.equals(""))
				 {
				    ratingbarprofessional.setRating(Float.parseFloat(rateprofessionaltxt));
				 }
				 System.out.println("rate");
				 ((EditText)findViewById(R.id.comments)).setText(db.decode(cur.getString(cur.getColumnIndex("fld_comments"))));
				 ((EditText)findViewById(R.id.etdate)).setText(cur.getString(cur.getColumnIndex("fld_date")));
				 System.out.println("savesavr");
				 selectedImagePath = db.decode(cur.getString(cur.getColumnIndex("fld_signature")));System.out.println("selectedImagePath="+selectedImagePath);
				 
				 //File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/PropertyOwnerSignature/"+inspectorid+"/"+srid+"/"+selectedImagePath);
				 File outputFile = new File(selectedImagePath);
					System.out.println("outputFile.exists()"+outputFile.exists());
						if(outputFile.exists())
					{
						bmdregister=getbitmap(outputFile);
						if(bmdregister!=null)
						{
							initialview.setBackgroundDrawable(bmdregister);
						}		
					}
					
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private BitmapDrawable getbitmap(File OutputFile)
	{
		bmdregister=null;
		try {
			BitmapFactory.Options o1 = new BitmapFactory.Options();
			o1.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(OutputFile),null, o1);
		
		final int REQUIRED_SIZE = 200;
		int width_tmp1 = o1.outWidth, height_tmp1 = o1.outHeight;
		int scale1 = 1;
		while (true) {
			if (width_tmp1 / 2 < REQUIRED_SIZE
					|| height_tmp1 / 2 < REQUIRED_SIZE)
				break;
			width_tmp1 /= 2;
			height_tmp1 /= 2;
			scale1 *= 2;
		}
		BitmapFactory.Options o3 = new BitmapFactory.Options();
		o3.inSampleSize = scale1;
		Bitmap bitmap1 = BitmapFactory.decodeStream(new FileInputStream(
				OutputFile), null, o3);
		
		bmdregister = new BitmapDrawable(bitmap1);
		return bmdregister;
		} catch (Exception e) {
		System.out.println("catch="+e.getMessage());
			return bmdregister;
		}
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		 this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						presentatinsptxt= checkedRadioButton.getText().toString().trim();
						if(presentatinsptxt.equals("Other"))
						{
							((EditText)findViewById(R.id.other)).setVisibility(v.VISIBLE);
						}						
						else
						{
							((EditText)findViewById(R.id.other)).setVisibility(v.GONE);
						}
						break;
						
					case 2:
						insparrivalbool=false;
						insparrivaltxt= checkedRadioButton.getText().toString().trim();
						 if(insparrivaltxt.equals("No"))
						 {
							 ((RadioButton)inspcallpriorarrivingrdgp.findViewWithTag("N/A")).setChecked(true);
						 }
					
					 break;
					 
					case 3:
						inspcallpriorarrivingbool=false;
						inspcallpriorarrivingtxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					case 4:
						inspidbadgebool=false;
						inspidbadgetxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					case 5:
						inspatticaccessopeningsbool=false;
						inspatticaccessopeningstxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					 
					case 6:
						inspreviewbool=false;
						inspreviewtxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					case 7:
						inspshoescleanbool=false;
						inspshoescleantxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					case 8:
						inspquestionsbool=false;
						inspquestionstxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					case 9:
						inspcourteousbool=false;
						inspcourteoustxt= checkedRadioButton.getText().toString().trim();
					 break;
					
					case 10:
						inspreferalbool=false;
						inspreferaltxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					  
					 
		          }
		        }
		}
	 }
	protected Dialog onCreateDialog(int id) 
	{
		switch (id) 
		{
			case 0:
			// set date picker as current date
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			datePicker = new DatePickerDialog (this, datePickerListener, year, month, day); 			   
			return datePicker;

		}
		return null;
	}
	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			int year = selectedYear;
			int month = selectedMonth;
			int day = selectedDay;			
			((EditText)findViewById(R.id.etdate)).setText(new StringBuilder().append(month + 1).append("/").append(day).append("/").append(year));
			}
	};

	protected void signclear() {
		// TODO Auto-generated method stub
		 File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/PropertyOwnerSignature/"+inspectorid+"/"+srid+"/"+selectedImagePath);
		 System.out.println("outputFile.exists()"+outputFile.exists());
				if(outputFile.exists())
			{
					outputFile.delete();
			}
		selectedImagePath="";
		initialview.clear();
		initialview.setBackgroundColor(Color.parseColor("#CBCBCB"));
		initialbool=false;
		GraphicsView.s = 0;
		
		
	}
	class Start_xporting extends AsyncTask <String, String, String> {		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="CSE Form Submission";
			    p_sv.progress_Msg="Please wait..Your CSE Form has been submitting.";      
				startActivityForResult(new Intent(Cseform.this,Progress_dialogbox.class), Static_variables.progress_code);		       
		    }		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		
			 		    Submit_Cse();					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
         	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	public void success_alert() {
		// TODO Auto-generated method stub
       
		AlertDialog al = new AlertDialog.Builder(Cseform.this).setMessage("You've successfully submitted your CSE Form.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				  
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(Cseform.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}
	
	protected void back() {
		// TODO Auto-generated method stub
		Intent i = new Intent(Cseform.this,StartInspection.class);
		i.putExtra("inspectorid", inspectorid);
		startActivity(i);
		finish();
	}

	private void Submit_Cse() throws IOException, XmlPullParserException 
	{
		  try
 		  {
	        	    SoapObject request = new SoapObject(wb.NAMESPACE,"GenerateCSE_PDF");
	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope.dotNet = true;
	      		    
	      		    
	      		    request.addProperty("PolicyNumber",srid);
	      		    request.addProperty("InspectorName",((EditText)findViewById(R.id.inspectorname)).getText().toString());
	      		    request.addProperty("InspectorID",inspectorid);
	      		    request.addProperty("InspectionDate",((TextView)findViewById(R.id.inspectiondate)).getText().toString());
	      		    if(presentatinsptxt.equals("Other"))
	      		    {
	      		    	request.addProperty("PresentatInspection",((EditText)findViewById(R.id.other)).getText().toString());
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("PresentatInspection",presentatinsptxt);	
	      		    }
	      		    
	      		    
	      		    request.addProperty("Report",((EditText)findViewById(R.id.reportno)).getText().toString());
	      		    request.addProperty("Inspector_arrive",insparrivaltxt);
	      		    request.addProperty("Inspector_notify",inspcallpriorarrivingtxt);
	      		    request.addProperty("Inspector_badge",inspidbadgetxt);
	      		    request.addProperty("Attic_open",inspatticaccessopeningstxt);
	      		    request.addProperty("Paperwork_ready",inspreviewtxt);
	      		    request.addProperty("Inspector_shoe",inspshoescleantxt);
	      		    request.addProperty("Listen_concerns",inspquestionstxt);
	      		    request.addProperty("Inspector_courteous",inspcourteoustxt);
	      		    request.addProperty("Confident_inspectioncompany",inspreferaltxt);
	      		    request.addProperty("Rate_professional",rateprofessionaltxt);
	      		    request.addProperty("Comments",((EditText)findViewById(R.id.comments)).getText().toString());
	      		  
	      		    
		      		  if(initialbool)
		      		  {
			      			initialview.setDrawingCacheEnabled(true);
			      			Bitmap insignature=Bitmap.createBitmap(initialview.getDrawingCache());
			      			
			      			ByteArrayOutputStream is=new ByteArrayOutputStream();
			      			insignature.compress(Bitmap.CompressFormat.PNG, 0, is);
			      			initialsignature=is.toByteArray();
			      			initialview.setDrawingCacheEnabled(false);
			      			request.addProperty("signature",initialsignature);
		      		  }
		      		  else
		      		  {
		      			request.addProperty("signature","");
		      		  }
		      		  request.addProperty("SubmitDate",((EditText)findViewById(R.id.etdate)).getText().toString());
		      		  
		      		  
		         		envelope.setOutputSoapObject(request);
		         		MarshalBase64 marshal = new MarshalBase64();
		        		marshal.register(envelope);
		         		System.out.println("request="+request);
		         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
		         		SoapObject response = null;
	         		
		         		try
		         		{
	                      androidHttpTransport.call(wb.NAMESPACE+"GenerateCSE_PDF", envelope);
	                      response = (SoapObject)envelope.getResponse();
	                      System.out.println("GenerateCSE_PDF "+response);
	                      
	                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	                      {
	                    	  	String path = response.getProperty("PDFURL").toString();System.out.println("path "+path);
	                    		String[] filenamesplit = path.split("/");
								System.out.println("the path is "+path);
								String viewfilename = filenamesplit[filenamesplit.length - 1];
								System.out.println("The File Name is " + viewfilename);
								String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
								File folder = new File(extStorageDirectory,"HI/CSEForm");
								folder.mkdir();
								File file = new File(folder, viewfilename);
								try {
									file.createNewFile();
									Downloader.DownloadFile(path, file);
									 View_Pdf_File(viewfilename,path);
								} catch (IOException e1) {
									e1.printStackTrace();
									System.out.println("e1="+e1.getMessage());
								}
	                    	                		  
	 	                    	handler_msg = 0;                   	  
	                    	 	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                catch(Exception e)
	                {
	                     e.printStackTrace();
	                }
	        	
		}      
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("error in cse form webservice"+e.getMessage());
		}
   }
	private void View_Pdf_File(String filename,String filepath) {
		// TODO Auto-generated method stub
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/HI/CSEForm/" + filename);
		Uri path = Uri.fromFile(file);
		System.out.println("pathdd="+path);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		try 
		{
			startActivity(intent);

		} catch (ActivityNotFoundException e) {
			System.out.println("e="+e.getMessage());
			Intent intentview = new Intent(Cseform.this, ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			
		}
	}
	protected void onSaveInstanceState(Bundle outState) 
	{
		outState.putString("PresentatInspection", presentatinsptxt);
		outState.putString("OtherPresentatInspection", otherpresent);
		outState.putInt("otid",Cseform.this.ot);
		outState.putString("Inspector_arrive", insparrivaltxt);
		outState.putString("Inspector_notify", inspcallpriorarrivingtxt);
		outState.putString("Inspector_badge", inspidbadgetxt);
		outState.putString("Attic_open", inspatticaccessopeningstxt);
		
		outState.putString("Paperwork_ready", inspreviewtxt);
		outState.putString("Inspector_shoe", inspshoescleantxt);
		outState.putString("Listen_concerns", inspquestionstxt);
		outState.putString("Inspector_courteous", inspcourteoustxt);
		outState.putString("Confident_inspectioncompany", inspreferaltxt);
		outState.putBoolean("graphic", initialbool);
		
		if(initialbool)
		{
			 if(initialview.isDrawingCacheEnabled())
			 {			 System.out.println("signinside");
			     initialview.setSaveEnabled(true);
				 Bitmap bmsignature=Bitmap.createBitmap(initialview.getDrawingCache());
				 ByteArrayOutputStream bos=new ByteArrayOutputStream();
				 bmsignature.compress(Bitmap.CompressFormat.PNG, 0, bos);
				 initialsignature=bos.toByteArray();
					
				 outState.putByteArray("bsignature", initialsignature);
			 }
		    //signature();
			//outState.putString("imagepath", selectedImagePath);
		}
		 
		
		
		super.onSaveInstanceState(outState);
	}
	
	 protected void onRestoreInstanceState(Bundle savedInstanceState) {
		 System.out.println("onrestore");
		 presentatinsptxt = savedInstanceState.getString("PresentatInspection");
		 if(!presentatinsptxt.equals(""))
		 {
			 cf.setvaluechk1(presentatinsptxt,chkpresent,((EditText)findViewById(R.id.other)));
		 }
		 otherpresent = savedInstanceState.getString("OtherPresentatInspection");
		 Cseform.this.ot = savedInstanceState.getInt("otid");
		 System.out.println("onres"+Cseform.this.ot);
		/* if(chkpresent[4].isChecked())
		 {
			System.out.println("inside");
				 ((EditText)findViewById(R.id.other)).setText(otherpresent);
				 ((EditText)findViewById(R.id.other)).setVisibility(v.VISIBLE);
		
		 }
		 else
		 {System.out.println("else");
			 ((EditText)findViewById(R.id.other)).setVisibility(v.GONE);
		 }
		 */
		 insparrivaltxt = savedInstanceState.getString("Inspector_arrive");System.out.println("insparrivaltxt="+insparrivaltxt);
		 if(!insparrivaltxt.equals(""))
		 {
		    ((RadioButton)insparrivalrdgp.findViewWithTag(insparrivaltxt)).setChecked(true);
		 }
		 else
		 {
			 insparrivalbool=true;
			 if(insparrivalbool)
			 {
				 try
				 {
					 insparrivalrdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 inspcallpriorarrivingtxt = savedInstanceState.getString("Inspector_notify");
		 if(!inspcallpriorarrivingtxt.equals(""))
		 {
		    ((RadioButton)inspcallpriorarrivingrdgp.findViewWithTag(inspcallpriorarrivingtxt)).setChecked(true);
		 }
		 else
		 {
			 inspcallpriorarrivingbool=true;
			 if(inspcallpriorarrivingbool)
			 {
				 try
				 {
					 inspcallpriorarrivingrdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 inspidbadgetxt = savedInstanceState.getString("Inspector_badge");
		 if(!inspidbadgetxt.equals(""))
		 {
		    ((RadioButton)inspidbadgerdgp.findViewWithTag(inspidbadgetxt)).setChecked(true);
		 }
		 else
		 {
			 inspidbadgebool=true;
			 if(inspidbadgebool)
			 {
				 try
				 {
					 inspidbadgerdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 inspatticaccessopeningstxt = savedInstanceState.getString("Attic_open");
		 if(!inspatticaccessopeningstxt.equals(""))
		 {
		    ((RadioButton)inspatticaccessopeningsrdgp.findViewWithTag(inspatticaccessopeningstxt)).setChecked(true);
		 }
		 else
		 {
			 inspatticaccessopeningsbool=true;
			 if(inspatticaccessopeningsbool)
			 {
				 try
				 {
					 inspatticaccessopeningsrdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 inspreviewtxt = savedInstanceState.getString("Paperwork_ready");
		 if(!inspreviewtxt.equals(""))
		 {
		    ((RadioButton)inspreviewrdgp.findViewWithTag(inspreviewtxt)).setChecked(true);
		 }
		 else
		 {
			 inspreviewbool=true;
			 if(inspreviewbool)
			 {
				 try
				 {
					 inspreviewrdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 inspshoescleantxt = savedInstanceState.getString("Inspector_shoe");
		 if(!inspshoescleantxt.equals(""))
		 {
		    ((RadioButton)inspshoescleanrdgp.findViewWithTag(inspshoescleantxt)).setChecked(true);
		 }
		 else
		 {
			 inspshoescleanbool=true;
			 if(inspshoescleanbool)
			 {
				 try
				 {
					 inspshoescleanrdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 inspquestionstxt = savedInstanceState.getString("Listen_concerns");
		 if(!inspquestionstxt.equals(""))
		 {
		    ((RadioButton)inspquestionsrdgp.findViewWithTag(inspquestionstxt)).setChecked(true);
		 }
		 else
		 {
			 inspquestionsbool=true;
			 if(inspquestionsbool)
			 {
				 try
				 {
					 inspquestionsrdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 inspcourteoustxt = savedInstanceState.getString("Inspector_courteous");
		 if(!inspcourteoustxt.equals(""))
		 {
		    ((RadioButton)inspcourteousrdgp.findViewWithTag(inspcourteoustxt)).setChecked(true);
		 }
		 else
		 {
			 inspcourteousbool=true;
			 if(inspcourteousbool)
			 {
				 try
				 {
					 inspcourteousrdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 inspreferaltxt = savedInstanceState.getString("Confident_inspectioncompany");
		 if(!inspreferaltxt.equals(""))
		 {
		    ((RadioButton)inspreferalrdgp.findViewWithTag(inspreferaltxt)).setChecked(true);
		 }
		 else
		 {
			 inspreferalbool=true;
			 if(inspreferalbool)
			 {
				 try
				 {
					 inspreferalrdgp.clearCheck();
				 }
				 catch(Exception e)
				 {
					 
				 }
			 }
		 }
		 
		 initialbool = savedInstanceState.getBoolean("graphic");
		 System.out.println("initialbool="+initialbool);
		  if(initialbool)
	      {
	    	  initialsignature = savedInstanceState.getByteArray("bsignature");
	    	  if(initialsignature!=null)
		      {
	    	  Bitmap bitmap=BitmapFactory.decodeByteArray(initialsignature, 0, initialsignature.length);
			   
		       Bitmap bmsignature=Bitmap.createBitmap(bitmap);
		       Bitmap mutableBitmap = bmsignature.copy(Bitmap.Config.ARGB_8888, true);
		       Canvas c = new Canvas(mutableBitmap);
		       
		       Paint textPaint = new Paint();
		     
		       c.drawBitmap(bmsignature, 0, 0, textPaint);

		       initialview.buildDrawingCache();
		       initialview.setDrawingCacheEnabled(true);
		       bmdregister = new BitmapDrawable(bitmap);
		     	if(bmdregister!=null)
				{
		     		initialview.setBackgroundDrawable(bmdregister);
				}	
		      }
	      }
		/* if(initialbool)
		 {
			 selectedImagePath = savedInstanceState.getString("imagepath");
			 System.out.println("onrestore"+selectedImagePath);
			 File outputFile = new File(selectedImagePath);
				if(outputFile.exists())
				{
					bmdregister=getbitmap(outputFile);
					if(bmdregister!=null)
					{
						initialview.setBackgroundDrawable(bmdregister);
					}		
				}
		 }*/
		 //showsavedvalues();
		 
		 super.onRestoreInstanceState(savedInstanceState);
	 }
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			back();
			
		}

		return super.onKeyDown(keyCode, event);
	}
	
	

}
