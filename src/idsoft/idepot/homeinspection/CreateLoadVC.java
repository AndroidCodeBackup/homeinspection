package idsoft.idepot.homeinspection;


import idsoft.idepot.homeinspection.CreateSubModule.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DataBaseHelperVC;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.IOException;
import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CreateLoadVC extends Activity{
	String gettempname="",getcurrentview="",getmodulename="",strrd_cond="",getinspectionpoint="",get_scon="",str_ip="",
			getprimaryvc="",getsecondaryvc="",getsubmodulename="",strother="",strcomments="",getph="",strchkval="",error_msg="",
			getvc="",gettempoption="",str="",selectedval="",selectvalseccond="",selectvalinsppoint="",title="Template Submission";
	
	TextView tv_seltemplate,tv_modulename,tvcomment,sp_inspectionpoint,sp_secondaryvc,sp_conttype;
	Button btnback,btnhome,btn_clear,btn_save;
	Spinner sp_primaryvc;
	ArrayAdapter primaryadap,secondaryadap;
	ImageView loadcomments;
	String[] arr_pvc,secondaryyarr;
	CommonFunction cf;
	Webservice_config wb;
	String getconttype="",seltdval="",selectvalcont="",getsectext="",getinsptext="",getconttext="",getvctemp="";
	Cursor scur;
	DataBaseHelperVC dbh;
	DatabaseFunction db;
	int insp_dialog=0,dialog_other=0,sec_dialog=0,cont_dialog=0;
	EditText etcomments,otheret,etother;
	RadioGroup rgcnd;
	boolean rgcond=false;
	LinearLayout lin;
	boolean load_comment=true;
	private int handler_msg=2;
	Progress_staticvalues p_sv;
	CheckBox[] ch;
	View v;
	public int[] chkbxid = {R.id.chk1,R.id.chk2,R.id.chk3,R.id.chk4};
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	//public CheckBox[] cb = new CheckBox[chkbxid.length];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getmodulename = b1.getString("modulename");
		Bundle b2 = this.getIntent().getExtras();
		getsubmodulename = b2.getString("submodulename");
		Bundle b3 = this.getIntent().getExtras();
		getcurrentview = b3.getString("currentview");
		Bundle b4 = this.getIntent().getExtras();
		getph = b4.getString("selectedid");
		Bundle b6 = this.getIntent().getExtras();
		getvc = b6.getString("selectedvc");
		Bundle b7 = this.getIntent().getExtras();
		gettempoption = b7.getString("tempoption");
		System.out.println("getcurrentview="+getcurrentview);
		setContentView(R.layout.activity_createvc);
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(5);
		db.CreateTable(6);
		db.CreateTable(17);
		db.userid();
		((HorizontalScrollView)findViewById(R.id.sub_section_hs)).setVisibility(v.GONE);
		
		tv_seltemplate = (TextView)findViewById(R.id.seltemplate);
		tv_seltemplate.setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
		tv_modulename = (TextView)findViewById(R.id.modulename);
		tv_modulename.setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		
		TextView tv_submodulename = (TextView)findViewById(R.id.submodulename);
		if(getmodulename.equals("Bathroom") || getmodulename.equals("HVAC"))
		{
			tv_submodulename.setText(getmodulename);
		}
		else
		{
			tv_submodulename.setText(getsubmodulename);	
		}
		
		
		   final  ScrollView scr = (ScrollView)findViewById(R.id.submodulelist);
	       scr.post(new Runnable() {
	          public void run() {
	              scr.scrollTo(0,0);
	          }
	      }); 
		
		/*modlinear = (LinearLayout)findViewById(R.id.modlin);
		if(getcurrentview.equals("create"))
		{
			modlinear.setVisibility(v.VISIBLE);
		}
		else
		{
			modlinear.setVisibility(v.GONE);
		}*/
		
		loadcomments = (ImageView)findViewById(R.id.loadcomments);
 		loadcomments.setOnClickListener(new OnClickListener() { 			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 		
 					
 						if(load_comment)
 						{
 							load_comment=false;
 							int loc1[] = new int[2];
 							v.getLocationOnScreen(loc1);
 							
 							Intent in =new Intent(CreateLoadVC.this,LoadComments.class);
 						 	in.putExtra("mod_name", getmodulename);
 							in.putExtra("submod_name", getsubmodulename);
 							in.putExtra("vcname", getvc);
 							in.putExtra("length", 12);
 							in.putExtra("max_length", 1000);
 							in.putExtra("xfrom", loc1[0]+10);
 							in.putExtra("yfrom", loc1[1]+10);
 							startActivityForResult(in, 34);
 						}
 			
 			}
 		});
		
		btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(CreateLoadVC.this,HomeScreen.class));
				finish();
			}
		});
		
		/*arr_pvc = new String[1];
		arr_pvc[0]=getvc;		 
		getprimaryvc = arr_pvc[0];*/
		
		
		try {
			dbh = new DataBaseHelperVC(CreateLoadVC.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			getvctemp = getvc;
			if(getvc.contains("'"))
			{
				getvctemp = getvctemp.replace("'","''");				
			}

			
			Cursor cur = newDB.rawQuery("select * from VCRANGE where RANGETABLE='"+getmodulename+"' and VCTEXT='"+getvctemp+"'",null);
			if(cur.getCount()>0)
			{
				cur.moveToFirst();
				String vcid = db.decode(cur.getString(cur.getColumnIndex("VCTYPE")));
				Cursor cur1 = newDB.rawQuery("select DISTINCT(VCTEXT) from TBL_VCCOMMENTS where RANGETABLE='"+getmodulename+"' and VCTYPE='"+vcid+"'",null);
				if(cur1.getCount()>0)
				{
					cur1.moveToFirst();
					arr_pvc =new String[cur1.getCount()+1]; 
					arr_pvc[0]="--Select--";
					for(int k =0;k<cur1.getCount();k++,cur1.moveToNext())
					{
						arr_pvc[k+1] = db.decode(cur1.getString(cur1.getColumnIndex("VCTEXT")));
					}
				}
				else
				{
					arr_pvc = new String[2];
					arr_pvc[0]="--Select--";
					arr_pvc[1]=getvc;
				}
			}
			else
			{
				arr_pvc = new String[2];
				arr_pvc[0]="--Select--";
				arr_pvc[1]=getvc;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		
		sp_primaryvc = (Spinner)findViewById(R.id.spin_primaryvc);
		primaryadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arr_pvc);
		primaryadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_primaryvc.setAdapter(primaryadap);
		sp_primaryvc.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(1));
		
		cf.GetCurrentModuleName(getmodulename);
		cf.GetVC(getmodulename,getsubmodulename);
		secondaryyarr = cf.submodvcid;System.out.println("secarr="+secondaryyarr.length);
		/*sp_secondaryvc = (Spinner)findViewById(R.id.spin_secondaryvc);
	 	secondaryadap = new ArrayAdapter<String>(CreateLoadVC.this,android.R.layout.simple_spinner_item,secondaryyarr);
	 	secondaryadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	sp_secondaryvc.setAdapter(secondaryadap);
	 	sp_secondaryvc.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(2));*/
	 	
	 	etcomments = (EditText)findViewById(R.id.comments);
        tvcomment = (TextView)findViewById(R.id.tvcomment);
        etcomments.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
    			if(s.toString().startsWith(" "))
    			{
    				etcomments.setText("");
    			}
    			cf.ShowingLimit(s.toString(),tvcomment,1000); 
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				

			}
		});
        
        rgcnd = (RadioGroup)findViewById(R.id.rgcond);
		otheret = (EditText)findViewById(R.id.othertxt);
		otheret.addTextChangedListener(new CustomTextWatcher(otheret));
		rgcnd.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				    RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
			        boolean isChecked = checkedRadioButton.isChecked();
			        if (isChecked)
			        {
			         	strrd_cond= checkedRadioButton.getText().toString().trim();
			         	if(strrd_cond.equals("Other"))
			         	{
			         		otheret.setVisibility(v.VISIBLE);
			         		otheret.requestFocus();
			         	}
			         	else
			         	{
			         		otheret.setVisibility(v.GONE);
			         	}
			         	rgcond=false;
					
				   }
			}
		});
		
		/*for(int i=0;i<chkbxid.length;i++)
		{
			cb[i] = (CheckBox)findViewById(chkbxid[i]);
		}*/
		
		sp_conttype = (TextView)findViewById(R.id.spin_contractortype);
		sp_conttype.setText("--Select--");
		sp_conttype.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cont_dialog=1;
				showcontdialog();
			}
		});   
		
		sp_inspectionpoint = (TextView)findViewById(R.id.spin_inspectionpoint);
		sp_inspectionpoint.setText("--Select--");
		sp_inspectionpoint.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				insp_dialog=1;
				dbvalue();
				showalert();
			}
		});
		
		sp_secondaryvc = (TextView)findViewById(R.id.spin_secondaryvc);
		sp_secondaryvc.setText("--Select--");
		sp_secondaryvc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sec_dialog=1;
				dbsecvalue();
				showsec_alert();
			}
		});
		
		btn_save = (Button)findViewById(R.id.ok);
        btn_save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 strother = otheret.getText().toString();
				 strcomments = etcomments.getText().toString();
				 
				 /* if(get_scon.trim().equals(""))
				  {
							 cf.DisplayToast("Please select Secondary Condition");
				  }
				  else
				  {*/
					 /*if(strcomments.equals(""))
					 {
						 cf.DisplayToast("Please enter Comments");
					 }
					 else
					 {
						 if(strrd_cond.equals(""))
						 {
							 cf.DisplayToast("Please select Condition");
						 }
						 else
						 {
							 if(strrd_cond.equals("Other"))
							 {
								 if(strother.equals(""))
								 {
									 cf.DisplayToast("Please enter the Other text");
								 }
								 else
								 {*/
									 checkboxvalidation();
						/*		 }
							 }
							 else
							 {
								 checkboxvalidation();
							 }
						 }
					 }*/
				// }
		    }
		
		});
        
        btn_clear = (Button)findViewById(R.id.clear);
        btn_clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				clear();
				sp_primaryvc.setSelection(0);
				 
			}
		});
		
		SetValue();
	}
	protected void showcontdialog() {
		// TODO Auto-generated method stub
		str="";
		final Dialog dialog = new Dialog(CreateLoadVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		LinearLayout lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Contractor Type");

		if(getconttype.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.conttypedisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.conttypedisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.conttypedisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getconttype;
		}
		
		
		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);
		
		((Button) dialog.findViewById(R.id.addnew)).setVisibility(v.GONE);
		
		 ch = new CheckBox[cf.contractortype_arr.length];
		for (int i = 0; i < cf.contractortype_arr.length; i++) {
			
			ch[i] = new CheckBox(CreateLoadVC.this);
		    ch[i].setText(cf.contractortype_arr[i]);
			ch[i].setTextColor(Color.BLACK);
			ch[i].setChecked(true);//Need to remove this(9/2/2015)
			lin.addView(ch[i]);
		
		}

		if (str != "") {
			cf.setValuetoCheckbox(ch, str);
			String replaceendcommaconttype= replacelastendswithcomma(str);
			((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaconttype));
		}
		
		for (int i = 0; i < cf.contractortype_arr.length; i++) {
			if (ch[i].isChecked()) {
				seltdval += ch[i].getText().toString() + "&#44;";
			}
		}

		getconttype = seltdval;

		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				seltdval = "";
				for (int i = 0; i < cf.contractortype_arr.length; i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
			
				getconttype = seltdval;
				cont_dialog=0;
				((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
				String replaceendcommaconttype = replacelastendswithcomma(getconttype);
				((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaconttype));
				dialog.dismiss();
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cont_dialog=0;dialog.dismiss();	
			}
		});
		
		
		dialog.show();
	}
	protected void dbsecvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		
		for(int j=0;j<secondaryyarr.length;j++)
		{
			 str_arrlst.add(secondaryyarr[j]);
	
		}		
	}

	protected void showsec_alert() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(CreateLoadVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Secondary Condition");
		
		secshow();
		
		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);
		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				String seltdval = "";
				for (int i = 0; i < str_arrlst.size(); i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
				
				String finalselstrval = "";
				finalselstrval = seltdval;
				System.out.println("final"+finalselstrval);
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{			
					getsecondaryvc = seltdval;
					System.out.println("getsec="+getsecondaryvc);
					secshow();
					dialog.dismiss();sec_dialog=0;
				}
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sec_dialog=0;dialog.dismiss();	
			}
		});
		
		Button btnother = (Button) dialog.findViewById(R.id.addnew);
		btnother.setVisibility(v.GONE);
		
		
		dialog.show();
	}

	private void secshow() {
		// TODO Auto-generated method stub
		System.out.println("getsecondasec="+getsecondaryvc);
		if(getsecondaryvc.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.secconddisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.secconddisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.secconddisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getsecondaryvc;
		}
		System.out.println("Str="+str);
		ch = new CheckBox[str_arrlst.size()];
		for (int i = 0; i < str_arrlst.size(); i++) {

			ch[i] = new CheckBox(CreateLoadVC.this);
			ch[i].setText(str_arrlst.get(i));
			ch[i].setTextColor(Color.BLACK);
			ch[i].setChecked(true);//Need to remove this(9/2/2015)
			lin.addView(ch[i]);
		}		
		

		if (str != "") 
		{			
			cf.setValuetoCheckbox(ch, str);
			String replaceendcommaseccond= replacelastendswithcomma(str);
			((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);			
			((TextView) findViewById(R.id.secconddisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaseccond));
		}
		else
		{
			((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);
		}
	}

	private String replacelastendswithcomma(String realword) {
		// TODO Auto-generated method stub
		
		if(realword.contains("&#44;"))
		{
			realword = realword.replace("&#44;",",");
		}
		if(realword.endsWith(","))
		{
			realword = realword.substring(0,realword.length()-1);
		}
		else
		{
			realword = realword;	
		}	
		return realword;
	}
	protected void dbvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		cf.GetCurrentModuleName(getmodulename);		
		
		for(int j=0;j<cf.insp_arr.length;j++)
		{
			 str_arrlst.add(cf.insp_arr[j]);
	
		}		
		
		try
		{
			 Cursor c=db.hi_db.rawQuery("select * from " + db.AddOtherIP + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =0;i<c.getCount();i++,c.moveToNext())
				 {
					 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
				 }
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	protected void onSaveInstanceState(Bundle outState) {
		 String strip="";
	        outState.putString("module_index", getmodulename);
	        outState.putString("pc_index", getprimaryvc);
	        outState.putString("sc_index", getsecondaryvc);
	        outState.putString("comments_index", etcomments.getText().toString());
	        
	        outState.putInt("insp_index", insp_dialog);
	        if(insp_dialog==1)
	        {
	        	selectvalinsppoint="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalinsppoint += ch[i].getText().toString() + "&#44;";
					}
				}
	        	outState.putString("spinspoption_index", selectvalinsppoint);
	        }
	        else
	        {
	        	outState.putString("spinspoption_index", getinspectionpoint);
	        }

	        outState.putInt("sec_index", sec_dialog);
	        if(sec_dialog==1)
	        {
	        	selectvalseccond="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalseccond += ch[i].getText().toString() + "&#44;";
					}
				}
	        	outState.putString("spoption_index", selectvalseccond);
	        }
	        else
	        {
	        	outState.putString("spoption_index", getsecondaryvc);
	        }
	        
	        outState.putInt("cont_index", cont_dialog);
	        if(cont_dialog==1)
	        {
	        	selectvalcont="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalcont += ch[i].getText().toString() + "&#44;";
					}
				}
	        	outState.putString("contoption_index", selectvalcont);
	        }
	        else
	        {
	        	outState.putString("contoption_index", getconttype);
	        }
	        
	        outState.putInt("other_index", dialog_other);
	        if(dialog_other==1)
	        {
	           outState.putString("othertext_index", etother.getText().toString());
	        }
	        
	        outState.putString("get_sectext", ((TextView) findViewById(R.id.secconddisplay)).getText().toString());
	        outState.putString("get_insptext", ((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString());
	        outState.putString("get_conttext", ((TextView) findViewById(R.id.conttypedisplay)).getText().toString());
	        
	        outState.putInt("handler", handler_msg);
	        outState.putString("error", error_msg);
	        super.onSaveInstanceState(outState);
	 }
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		   getmodulename = savedInstanceState.getString("module_index");
		   getprimaryvc = savedInstanceState.getString("pc_index");
		   getsecondaryvc = savedInstanceState.getString("sc_index");
		   strcomments = savedInstanceState.getString("comments_index");
	       
		   
		   insp_dialog = savedInstanceState.getInt("insp_index");
		   getinspectionpoint = savedInstanceState.getString("spinspoption_index");
		   if(insp_dialog==1)
		   {
			   
			   showalert();
			  
			   if(!getinspectionpoint.equals(""))
			   {				
			    	  cf.setValuetoCheckbox(ch, getinspectionpoint);
			   }
		   }
		   
		   sec_dialog = savedInstanceState.getInt("sec_index");
		   get_scon = savedInstanceState.getString("spoption_index");
		   if(sec_dialog==1)
		   {
			   showsec_alert();
			   
			   if(!get_scon.equals(""))
			   {				
			    	  cf.setValuetoCheckbox(ch, get_scon);
			   }
		   }
		   
		   
		   cont_dialog = savedInstanceState.getInt("cont_index");
		   getconttype= savedInstanceState.getString("contoption_index");
		   if(cont_dialog==1)
		   {			  
			   showcontdialog();
		   }
		   
		   
		   
		   getsectext =  savedInstanceState.getString("get_sectext");
		   if(!getsectext.equals(""))
		   {
			   
			   ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);
			   ((TextView) findViewById(R.id.secconddisplay)).setText(getsectext);
		   }
		   
		   getinsptext =  savedInstanceState.getString("get_insptext");
		   if(!getinsptext.equals(""))
		   {
			
			   ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			   ((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(getinsptext);
		   }
		   
		   getconttext =  savedInstanceState.getString("get_conttext");
		   if(!getconttext.equals(""))
		   {
			   ((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
			   ((TextView) findViewById(R.id.conttypedisplay)).setText(getconttext);
		   }
		   
		   
		   dialog_other = savedInstanceState.getInt("other_index");
		   if(dialog_other==1)
		   {
			   dialogshow();
			   strother = savedInstanceState.getString("othertext_index");
			   etother.setText(strother);
			 
		   }
		   
		   handler_msg = savedInstanceState.getInt("handler");
	       error_msg = savedInstanceState.getString("error");
	       
	        if(handler_msg==0)
	    	{
	    		success_alert();
	    	}
	    	else if(handler_msg==1)
	    	{
	    		failure_alert();
	    	}
	        
		   super.onRestoreInstanceState(savedInstanceState);
	    }

	private void clearall() {
		// TODO Auto-generated method stub
		System.out.println("dsfosddjs");
		 getinspectionpoint="";getconttype="";getsecondaryvc="";
		 ((EditText) findViewById(R.id.approximaterepaircost)).setText("");	
		 etcomments.setText("");
         ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		 	((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.GONE);
		    ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);
		    sp_primaryvc.setSelection(0);
		    
	}
	protected void checkboxvalidation() {
		// TODO Auto-generated method stub
		//String strchkval = cf.getvaluefromchk(cb);
		 
		 /*if(strchkval.equals(""))
		 {
			 cf.DisplayToast("Please select at least any one checkbox option");
		 }
		 else
		 {*/
			 if(getinspectionpoint.trim().equals(""))
			 {
				 cf.DisplayToast("Please select Inspection Point");
			 }
			 else
			 {
				 try
					{
					    Cursor cur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(getprimaryvc)+"'", null);
			            System.out.println("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(getprimaryvc)+"'");
					    int cnt = cur.getCount();System.out.println("cnt="+cnt);
			            if(cnt==0)
			            {
			            	System.out.println("INSERT INTO "
									+ db.SetVC
									+ " (fld_inspectorid,fld_module,fld_submodule,fld_templatename,fld_vcname,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype) VALUES"
									+ "('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(getsubmodulename)+"','"+db.encode(gettempname)+"','"+db.encode(getprimaryvc)+"','"
									+db.encode(getsecondaryvc)+"','"+db.encode(strcomments)+"','"+strrd_cond+"','"+db.encode(strother)+"','"+strchkval+"','"+db.encode(getinspectionpoint)+"','"+db.encode(((EditText)findViewById(R.id.approximaterepaircost)).getText().toString())+"','"+db.encode(getconttype)+"')");
			            	db.hi_db.execSQL("INSERT INTO "
										+ db.SetVC
										+ " (fld_inspectorid,fld_module,fld_submodule,fld_templatename,fld_vcname,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype) VALUES"
										+ "('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(getsubmodulename)+"','"+db.encode(gettempname)+"','"+db.encode(getvc)+"','"+db.encode(getprimaryvc)+"','"
										+db.encode(getsecondaryvc)+"','"+db.encode(strcomments)+"','"+strrd_cond+"','"+db.encode(strother)+"','"+strchkval+"','"+db.encode(getinspectionpoint)+"','"+db.encode(((EditText)findViewById(R.id.approximaterepaircost)).getText().toString())+"','"+db.encode(getconttype)+"')");
			            }
			            else
			            {
			            	System.out.println("UPDATE "+db.SetVC+ " SET fld_primary='"+db.encode(getprimaryvc)+"',fld_vcname='"+db.encode(getvc)+"',fld_secondary='"+db.encode(getsecondaryvc)+"',"
			            			+"fld_comments='"+db.encode(strcomments)+"',fld_condition='"+strrd_cond+"',fld_other='"+db.encode(strother)+"',fld_checkbox='"+strchkval+"',"
			            			+"fld_inspectionpoint='"+db.encode(getinspectionpoint)+"',fld_apprrepaircost='"+db.encode(((EditText)findViewById(R.id.approximaterepaircost)).getText().toString())+"',fld_contractortype='"+db.encode(getconttype)+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(getprimaryvc)+"'");
			            	db.hi_db.execSQL("UPDATE "+db.SetVC+ " SET fld_primary='"+db.encode(getprimaryvc)+"',fld_vcname='"+db.encode(getvc)+"',fld_secondary='"+db.encode(getsecondaryvc)+"',"
			            			+"fld_comments='"+db.encode(strcomments)+"',fld_condition='"+strrd_cond+"',fld_other='"+db.encode(strother)+"',fld_checkbox='"+strchkval+"',"
			            			+"fld_inspectionpoint='"+db.encode(getinspectionpoint)+"',fld_apprrepaircost='"+db.encode(((EditText)findViewById(R.id.approximaterepaircost)).getText().toString())+"',fld_contractortype='"+db.encode(getconttype)+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(getprimaryvc)+"'");
						
			            }
			            back();
			            /*  if(cf.isInternetOn())
						  {
							   new Start_xporting().execute("");
						  }
						  else
						  {
							  cf.DisplayToast("Please enable your internet connection.");
							  
						  } */
						 
					   
					}
					catch (Exception e) {
						// TODO: handle exception
					}
			 }
		// }
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="Template Submission";
			    p_sv.progress_Msg="Please wait..Your template has been submitting.";
		       
				startActivityForResult(new Intent(CreateLoadVC.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		      Submit_Template();
			 		     
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=2;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=2;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
            	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	
    private void Submit_Template() throws IOException, XmlPullParserException {
		
		db.userid();
 		
 		 try
 		  {
 			 Cursor cur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(getprimaryvc)+"'", null);
	         if(cur.getCount()>0)
	         {
	        	cur.moveToFirst();	       	
	        	
	        	
	        	    SoapObject request = new SoapObject(wb.NAMESPACE,"CreateTemplateVC");
	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope.dotNet = true;
	      		    
	        		request.addProperty("sv_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
	         		request.addProperty("fld_inspectorid",db.UserId);
	         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
	         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
	         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
	         		request.addProperty("fld_primary",db.decode(cur.getString(cur.getColumnIndex("fld_primary"))));
	         		String str = Html.fromHtml(db.decode(cur.getString(cur.getColumnIndex("fld_secondary")))).toString();
	         		if(str.endsWith(","))
	         		{
	         			str = str.substring(0,str.length()-1);
	         		}
	         		request.addProperty("fld_secondary",str);
	         		request.addProperty("fld_comments",db.decode(cur.getString(cur.getColumnIndex("fld_comments"))));
	         		String inspstr = Html.fromHtml(db.decode(cur.getString(cur.getColumnIndex("fld_inspectionpoint")))).toString();
	         		if(inspstr.endsWith(","))
	         		{
	         			inspstr = inspstr.substring(0,inspstr.length()-1);
	         		}
	         		request.addProperty("fld_inspectionpoint",inspstr);
	         	
	         		envelope.setOutputSoapObject(request);
	         		System.out.println("request="+request);
	         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
	         		SoapObject response = null;
	                try
	                {
	                      androidHttpTransport.call(wb.NAMESPACE+"CreateTemplateVC", envelope);
	                      response = (SoapObject)envelope.getResponse();
	                      System.out.println("CreateTemplateVCresult "+response);
	                      
	                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	 
	                    		  try
	 	          				 {
	 	          					db.hi_db.execSQL("UPDATE "+db.SetVC+ " SET fld_sendtoserver='"+response.getProperty("sv_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_primary='"+db.encode(getprimaryvc)+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	                    	  
	                    	 
	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                catch(Exception e)
	                {
	                     e.printStackTrace();
	                }
	        	//}
	         }
	  	}
 		catch (Exception e) {
			// TODO: handle exception
		}
 	    
       
   }
	
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(CreateLoadVC.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public void success_alert() {
		// TODO Auto-generated method stub
       
		AlertDialog al = new AlertDialog.Builder(CreateLoadVC.this).setMessage("You've successfully submitted your template.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				clear();
	            sp_primaryvc.setSelection(0);
	            back();
				handler_msg = 2 ;
				
				
				
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	private void clear() {
		// TODO Auto-generated method stub
		 getinspectionpoint="";getconttype="";getsecondaryvc="";
		 ((EditText) findViewById(R.id.approximaterepaircost)).setText("");	
		 etcomments.setText("");str="";
         ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.GONE);
		 ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);	
	}
	private void SetValue() {
		// TODO Auto-generated method stub
		try
		{
			//scur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_primary='"+db.encode(getvc)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_VCName!='--Select--'", null);
			scur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_vcname='"+db.encode(getvc)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
		    System.out.println("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_vcname='"+db.encode(getvc)+"' and fld_templatename='"+db.encode(gettempname)+"'");
			int scnt = scur.getCount();
			System.out.println("scnt="+scnt);
			if(scnt==0)
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.AddVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"'  and fld_primary='"+db.encode(getvc)+"'", null);
				System.out.println("select * from " + db.AddVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"'  and fld_primary='"+db.encode(getvc)+"'");
				int cnt = cur.getCount();
				System.out.println("cnt="+cnt);
				if(cnt>0)
				{
					GetLoad(cur);
					show();showcontdialog();showalert();
				}
			
		}
		else
		{
			GetLoad(scur);
			
		}
	  }
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void GetLoad(Cursor cur) {
		// TODO Auto-generated method stub
		cur.moveToFirst();
    	String get_pcon = db.decode(cur.getString(cur.getColumnIndex("fld_primary")));
    	int pc = primaryadap.getPosition(get_pcon);
    	sp_primaryvc.setSelection(pc);    	
    	
    	getsecondaryvc = db.decode(cur.getString(cur.getColumnIndex("fld_secondary")));System.out.println("get_pcon"+getsecondaryvc);
    	if(!getsecondaryvc.equals(""))
    	{
    		String replaceendcommaseccond= replacelastendswithcomma(getsecondaryvc);
    		((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.secconddisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaseccond));
    	}
    	
    	String getcomments = db.decode(cur.getString(cur.getColumnIndex("fld_comments")));
    	etcomments.setText(getcomments);
    	
    	getinspectionpoint = db.decode(cur.getString(cur.getColumnIndex("fld_inspectionpoint")));
    	if(!getinspectionpoint.equals(""))
    	{
    		String replaceendcommaionspoint= replacelastendswithcomma(getinspectionpoint);
    		((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
    	}
    	
    	getconttype = db.decode(cur.getString(cur.getColumnIndex("fld_contractortype")));
    	if(!getconttype.equals(""))
    	{
    		String replaceendcommaconttype= replacelastendswithcomma(getconttype);
	    	((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaconttype));
    	}  
    	
    	
    	String getapprcost = db.decode(cur.getString(cur.getColumnIndex("fld_apprrepaircost")));
    	((EditText)findViewById(R.id.approximaterepaircost)).setText(getapprcost);
    	
	}
	protected void showalert() {
		// TODO Auto-generated method stub		
		final Dialog dialog = new Dialog(CreateLoadVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Inspection Point");
		
		show();
		
		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);
		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				String seltdval = "";
				for (int i = 0; i < str_arrlst.size(); i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
				}
				
				String finalselstrval = "";
				finalselstrval = seltdval;
				System.out.println("final"+finalselstrval);
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{			
					getinspectionpoint = seltdval;
					show();
					dialog.dismiss();insp_dialog=0;					
				}
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				insp_dialog=0;dialog.dismiss();	
			}
		});
		
		
		Button btnother = (Button) dialog.findViewById(R.id.addnew);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=1;
				dialogshow();
			}
		});
		
		
		dialog.show();
	}
	protected void dialogshow() {
		// TODO Auto-generated method stub
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(CreateLoadVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.addnewcustom);
		
		etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
		cf.hidekeyboard(etother);
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		strother = etother.getText().toString();
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//CreateSubModule.sp=0;
				dialog_other=0;
				add_dialog.dismiss();
			}
		});
		
		Button btnsave = (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {
            String seltdval="";
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strother = etother.getText().toString();
				if (etother.getText().toString().trim().equals("")) {
					cf.DisplayToast("Please enter the other text");
					
				} else {
					System.out.println("str_arrlst="+str_arrlst+" tescst"+strother.trim());
					if(str_arrlst.contains(strother.trim().toLowerCase()))
					{
						cf.DisplayToast("Already exists!! Please enter the new text");
						etother.setText("");
						etother.requestFocus();
					}
					else
					{
						try
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.AddOtherIP
									+ " (fld_inspectorid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
									+ "('"+db.UserId+"','"+getmodulename+"','"+getsubmodulename+"','"
									+ db.encode(etother.getText().toString()) + "','"+db.encode(gettempname)+"')");
							
							String addedval = db.encode(etother.getText().toString());
							System.out.println("addedval="+addedval);
							str_arrlst.add(addedval);
						//	CreateSubModule.sp=0;
							dialog_other=0;
							cf.DisplayToast("Added successfully");
							add_dialog.cancel();
							
							lin.removeAllViews();
							dbvalue();
							show();
							
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
		
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	protected void show() {
		// TODO Auto-generated method stub
		System.out.println("getinspectionpoint"+getinspectionpoint);
		if(getinspectionpoint.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.inspectionvaluedisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getinspectionpoint;
		}
		
		ch = new CheckBox[str_arrlst.size()];
		for (int i = 0; i < str_arrlst.size(); i++) {

			ch[i] = new CheckBox(CreateLoadVC.this);
			ch[i].setText(str_arrlst.get(i));
			ch[i].setTextColor(Color.BLACK);
			ch[i].setChecked(true);//Need to remove this(9/2/2015)
			lin.addView(ch[i]);
		}		
		
		if (str != "") 
		{
			 cf.setValuetoCheckbox(ch, str);
			 String replaceendcommaionspoint= replacelastendswithcomma(str);
 			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
		}
		else
		{
			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		}
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		int j;
		public MyOnItemSelectedListenerdata(int i) {
			// TODO Auto-generated constructor stub
			j=i;
		}

		public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
		  
			if(j==1)
			{
				getprimaryvc = parent.getItemAtPosition(pos).toString();
			}
			else
			{
				getsecondaryvc = parent.getItemAtPosition(pos).toString();
			}
	   }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	private void back() {
		// TODO Auto-generated method stub
		
		/*try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(getprimaryvc)+"'", null);
            System.out.println("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_primary='"+db.encode(getprimaryvc)+"'");
		    int cnt = cur.getCount();System.out.println("cnt="+cnt);
            if(cnt==0)
            {
            	cf.DisplayToast("Please save visible condition");
            }
            else
            {*/
            	Intent intent = new Intent(CreateLoadVC.this, NewSetVC.class);
        		Bundle b2 = new Bundle();
        		b2.putString("templatename", gettempname);			
        		intent.putExtras(b2);
        		Bundle b3 = new Bundle();
        		b3.putString("currentview", getcurrentview);			
        		intent.putExtras(b3);
        		Bundle b4 = new Bundle();
        		b4.putString("modulename", getmodulename);			
        		intent.putExtras(b4);
        		Bundle b5 = new Bundle();
        		b5.putString("tempoption", gettempoption);			
        		intent.putExtras(b5);
        		Bundle b6 = new Bundle();
        		b6.putString("submodulename", getsubmodulename);			
        		intent.putExtras(b6);
        		startActivity(intent);
        		finish();
        /*    }
		}
		catch (Exception e) {
			// TODO: handle exception
		}*/
		
		
		
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
 		try
 		{
 			if(requestCode==34)
 			{
 				load_comment=true;
 				if(resultCode==RESULT_OK)
 				{
 					String comm = ((EditText)findViewById(R.id.comments)).getText().toString();
 					((EditText)findViewById(R.id.comments)).setText(comm + data.getExtras().getString("Comments"));
	 			}
 			}
 		}
 		catch (Exception e) {
 			// TODO: handle exception
 			System.out.println("the erro was "+e.getMessage());
 		}
 	}/**on activity result for the load comments ends**/
	
}
