package idsoft.idepot.homeinspection.support;



import idsoft.idepot.homeinspection.R;
import android.content.Context;



public class ArrayModuleList {
	Context context;
	public static String[][] arrload;
	public int[] structure_submodid = {R.array.structuretype,R.array.foundationtype,R.array.bearingwall,R.array.nonbearingwall,R.array.beam,
			R.array.columns,R.array.floors,R.array.ceilings,R.array.roofstructure,R.array.atticaccess,R.array.atticaccesstype,R.array.basement,
			R.array.basement_size,R.array.basement_access,
			R.array.basement_stairs,R.array.basement_floor,R.array.basement_coils,R.array.basement_wc,R.array.basement_efloors,
			R.array.basement_drain,R.array.basement_heat,R.array.basement_finishes,R.array.crawlspace,R.array.crawlspace_characteristics,
			R.array.crawlspace_access,R.array.crawlspace_mainbeam,R.array.crawlspace_floor,R.array.crawlspace_foundation
			};
	public int[] exterior_submodid = {
			R.array.driveways,
			R.array.walkways,
			R.array.windows,
			R.array.windows_glazing,
			R.array.exteriordoor_style,
			R.array.exteriordoor_glazing,
			R.array.electricmeter_outlets,
			R.array.surfacematerials,			
			R.array.masonry,
			R.array.siding,			
			R.array.wallshingles,
			R.array.deck,
			R.array.frontporch,
			R.array.electricmeterlocation,
			R.array.mainpanelmeterinside,
			R.array.mainpanelmeteroutside,
			R.array.extmainpanelmeter,		
			R.array.gasmeter,
			R.array.steps,
			R.array.balcony,
			R.array.stairways,
			R.array.fences,
			R.array.yardwalls,
			R.array.retainingwalls,
			R.array.accessibility};
	
	public int[] garage_submodid={R.array.garagetype,R.array.garagesiding,R.array.garagewindows,R.array.garageexteriordoortype,R.array.garagedoor,
			R.array.garagedoor_type,R.array.garagedoor_opener,R.array.safetyreverse_present,R.array.gutters,R.array.downspouts,R.array.trim,R.array.floor,
			R.array.sillplates,R.array.receptaclesoperable,R.array.firepresent,R.array.firedoor};
	public int[] plumbing_submodid={R.array.waterservice,R.array.watermain,R.array.watermeter,R.array.waterpressure,R.array.wateroff,R.array.waterdetector,
			R.array.watercondition,R.array.supplypipes,R.array.drainageservice,R.array.septictank,R.array.drainfield,R.array.drains,R.array.maindrain,
			R.array.otherplumbing};
	public int[] electrical_submodid={R.array.serviceentrance,R.array.voltagerating,R.array.sericeconductors,R.array.mainpanellocation,R.array.mainpaneltype,
			R.array.mainpanelmanufacturer,R.array.mainpanelamps,R.array.subpanels,R.array.subpanelsmanufacturer,R.array.serviceground,R.array.volt120,
			R.array.volt240,R.array.fiveyear,R.array.gfi,R.array.afip,R.array.outlets120,R.array.lightswitches,R.array.lightfixtures,R.array.ceilingfan,
			R.array.smokedetectors,R.array.carbon,R.array.otherelectrical};
	public int[] heating_submodid={R.array.energytype,R.array.deliverytype,R.array.noofzones,R.array.furnacetype,
			R.array.flue,R.array.blower,R.array.furnace,R.array.supplyregister,R.array.returnregister};
	public int[] cooling_submodid={R.array.coolingzones,R.array.Tonnage,R.array.characteristics,R.array.condensation,
			R.array.coolingtesting,R.array.otherunits};
	public int[] kitchen_submodid={R.array.kitchen,R.array.kitchensinktype,R.array.cabinetry,R.array.disposal,R.array.hotwater};
	public int[] bathroom_submodid={R.array.fixture,R.array.bathmaterials,R.array.safetyglass,R.array.bathother};
	public int[] interior_submodid={R.array.vinylfloor,R.array.walls,R.array.intceilings,R.array.intwindows,R.array.intdoors,R.array.intstairs,R.array.fireplace};
	public int[] porches_submodid={R.array.intporches};
	public int[] pool_submodid={R.array.poolstyle,R.array.whirlpoolstyle,R.array.waterclarity,R.array.poolshape,R.array.poolshellmaterial,R.array.pooldeck,
			R.array.poolcoping,R.array.poolfeatures,R.array.poollighting,R.array.poolequipment,R.array.plumbingsystems,R.array.poolpumping,R.array.filtering,
			R.array.poolcleaning,R.array.watertreatment,R.array.waterheatingsystem,R.array.timers,R.array.electricalservice,R.array.poolcovers,
			R.array.safety,R.array.waterchemistry,R.array.generalcomments};
	public int[] well_submodid={R.array.pumptype,R.array.accessories,R.array.well};
	public int[] cabana_submodid={R.array.cabroofcovering,R.array.cabsidingstyle,R.array.cabplumbingsupply,R.array.cabwaterheater,R.array.cabheattype};
	public int[] app_submodid={R.array.washer,R.array.dryer,R.array.dishwasher,R.array.appdisposal,R.array.oven,R.array.fan,R.array.refrigerator,
			R.array.microwave,R.array.trash,R.array.breadwarmer,R.array.rangetop,R.array.lawn,R.array.watersoftener,R.array.waterheater,R.array.atticfan,R.array.doorbell};
	public int[] attic_submodid={R.array.accessto,R.array.visibleinsulation,R.array.insulationthickness,
			R.array.ventilation,R.array.atticfans,R.array.turbines,R.array.firewalls};
	public int[] roof_submodid={R.array.inspectedfrom,R.array.rooftype,R.array.roofcoveringtype,R.array.roofshingles,R.array.tile,
			R.array.builtup,R.array.rollroofing,R.array.flashings,R.array.skylights,R.array.skylightsnum,R.array.skylighttype,R.array.roofcomplex,
			R.array.leak,R.array.y5year,R.array.parts,R.array.fascia,R.array.chimneystructure,R.array.guttersdownspouts,R.array.atticventilation,R.array.roofvisibleinsulation};
	public int[] submodid;
	public String getcurrentmodname="";
	
	public ArrayModuleList(Context con, String currentmodulename) {
		// TODO Auto-generated constructor stub
		context = con;
		getcurrentmodname=currentmodulename;
		
		if(getcurrentmodname.equals("Structure"))
		{
			submodid = structure_submodid;
			
		}
		else if(getcurrentmodname.equals("Exterior"))
		{
			submodid = exterior_submodid;
		}
		else if(getcurrentmodname.equals("Garage/Carport"))
		{
			submodid = garage_submodid;
		}
		else if(getcurrentmodname.equals("Plumbing System"))
		{
			submodid = plumbing_submodid;
		}
		else if(getcurrentmodname.equals("Electrical System"))
		{
			submodid = electrical_submodid;
		}
		else if(getcurrentmodname.equals("Heating System"))
		{
			submodid = heating_submodid;
		}
		/*else if(getcurrentmodname.equals("Central Air Conditioning System"))
		{
			submodid = cooling_submodid;
		}*/
		else if(getcurrentmodname.equals("Kitchen"))
		{
			submodid = kitchen_submodid;
		}
		else if(getcurrentmodname.equals("Bathroom"))
		{
			submodid = bathroom_submodid;
		}
		else if(getcurrentmodname.equals("Interior"))
		{
			submodid = interior_submodid;
		}
		else if(getcurrentmodname.equals("Porches/Screens/Enclosures"))
		{
			submodid = porches_submodid;
		}
		else if(getcurrentmodname.equals("Swimming Pools"))
		{
			submodid = pool_submodid;
		}
		else if(getcurrentmodname.equals("Wells/Pump Equipment"))
		{
			submodid = well_submodid;
		}
		else if(getcurrentmodname.equals("Cabana/Pool House"))
		{
			submodid = cabana_submodid;
		}
		else if(getcurrentmodname.equals("Appliances and Systems"))
		{
			submodid = app_submodid;
		}
		else if(getcurrentmodname.equals("Attic"))
		{
			submodid = attic_submodid;
		}
		else if(getcurrentmodname.equals("Roofing"))
		{
			submodid = roof_submodid;
		}
		
		arrload=new String[submodid.length][];System.out.println("submodid.length="+submodid.length);
		for(int i=0;i<submodid.length;i++)
		{
			arrload[i]=con.getResources().getStringArray(submodid[i]);
		}
		
		
	}

}
