package idsoft.idepot.homeinspection.support;

import idsoft.idepot.homeinspection.R;

import java.util.ArrayList;





import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;


public class MyExpandableAdapter extends BaseExpandableListAdapter
{

    private Activity activity;
    private ArrayList<Object> childtems;
    private LayoutInflater inflater;
    View v;
    String[] chkval;
    EditText et;
    private ArrayList<String> parentItems, child;

    // constructor
    public MyExpandableAdapter(ArrayList<String> parents, ArrayList<Object> childern)
    {
        this.parentItems = parents;
        this.childtems = childern;
    }

    public void setInflater(LayoutInflater inflater, Activity activity)
    {
        this.inflater = inflater;
        this.activity = activity;
    }
   
    // method getChildView is called automatically for each child view.
    //  Implement this method as per your requirement
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
    {

        child = (ArrayList<String>) childtems.get(groupPosition);

        CheckBox textView = null;
        

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_view, null);
        }
   
         // get the textView reference and set the value
        textView = (CheckBox) convertView.findViewById(R.id.textViewChild);
        textView.setText(child.get(childPosition));
        
        et  = (EditText)convertView.findViewById(R.id.edt);
        textView.setOnCheckedChangeListener(new oncheckedlistener(childPosition,child.get(childPosition)));
        
         /*if(child.get(childPosition).equals("Other"))
		 {
        	 et.setVisibility(v.VISIBLE);
			
		 }
		 else
		 {
			 et.setVisibility(v.GONE);
		 }*/
        
        /*textView.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				
					if(isChecked)
					{
						if(buttonView.getText().toString().trim().equals("Other"))
						{
							
							chkval[childPosition]="checked";
						    et.setVisibility(v.VISIBLE);
						}
					}
					else
					{
						chkval[childPosition]="unchecked";
						et.setVisibility(v.GONE);
					}
				
			}
		});
        */
       

        // set the ClickListener to handle the click event on child item
        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
            
                Toast.makeText(activity, child.get(childPosition),
                        Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }
class oncheckedlistener implements OnCheckedChangeListener
{
      int id;
      String str;
	public oncheckedlistener(int childPosition, String string) {
		// TODO Auto-generated constructor stub
		id = childPosition;
		str = string;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if(isChecked)
		{
			if(buttonView.getText().toString().trim().equals("Other"))
			{
			       et.setVisibility(v.VISIBLE);
			}
			
		}
		else
		{
			et.setVisibility(v.GONE);
		}
	
	}
	
}
    // method getGroupView is called automatically for each parent item
    // Implement this method as per your requirement
    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, final ViewGroup parent)
    {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.parent_view, null);
        }
           /* CheckBox chk = (CheckBox)convertView.findViewById(R.id.textViewGroupName);
            chk.setText(parentItems.get(groupPosition));
            chk.setChecked(isExpanded);*/
        
        ((CheckedTextView) convertView).setText(parentItems.get(groupPosition));
        ((CheckedTextView) convertView).setChecked(isExpanded);
        
         

        return convertView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
    	int j=0;
    	try
    	{
    		j =  ((ArrayList<String>) childtems.get(groupPosition)).size();
    	}
    	catch (Exception e) {
			// TODO: handle exception
		}
        return j;
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        return null;
    }

    @Override
    public int getGroupCount()
    {
        return parentItems.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition)
    {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition)
    {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return 0;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return false;
    }

	
}