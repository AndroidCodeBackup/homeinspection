package idsoft.idepot.homeinspection.support;



import idsoft.idepot.homeinspection.R;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.ContactsContract.Contacts.Data;
import android.view.KeyEvent;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Progress_dialogbox extends Activity {
Progress_staticvalues p_sv;
PowerManager.WakeLock wl=null;
CommonFunction cf;
DatabaseFunction db;
String previous="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.progress_alert);
		PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
		 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
		 wl.acquire();
		
		cf=new CommonFunction(this);
	/*	db=new DatabaseFunction(this);
		db.hi_db = this.openOrCreateDatabase(db.MY_DATABASE_NAME, 1, null);*/
		set_width();		
		
		((TextView)findViewById(R.id.txthead)).setText(p_sv.progress_title);
		((TextView)findViewById(R.id.prog_text)).setText(p_sv.progress_Msg);
		previous=p_sv.progress_Msg;
		new show_progress_bar().execute("");
		
		
	}
	class show_progress_bar extends AsyncTask <String, String, String> {

	    @Override
	    public void onPreExecute() {
	     	super.onPreExecute();
	    }
	 @Override
	    protected String doInBackground(String... aurl) {
		 while (p_sv.progress_live) {
		//	 System.out.println("loop called");
			 try {
				 Thread.sleep(1000);
				 if(!previous.equals(p_sv.progress_Msg))
				 {
					 
					 publishProgress("");// this will call the on progress update
					 previous=p_sv.progress_Msg;
					 
				 }
			 } catch(Exception e){};
			
		}
			return null;
	    }

	    protected void onProgressUpdate(String... progress) {
	    	try
	    	{
	    	((TextView)findViewById(R.id.prog_text)).setText(p_sv.progress_Msg);
	    	}catch (Exception e) {
				// TODO: handle exception
	    		System.out.println(" the isseu"+e.getMessage());
			}
	    	
	    }

	    @Override
	    protected void onPostExecute(String unused) {
	    	
	    	finish();
		

	    }
	}


	private void set_width() {
		// TODO Auto-generated method stub
		cf.getDeviceDimensions();
		RelativeLayout.LayoutParams lp=(android.widget.RelativeLayout.LayoutParams) findViewById(R.id.parent).getLayoutParams();
		if(cf.wd>500)
		{
			lp.width=500;
		}
		else
		{
			lp.width=cf.wd;
		}
		findViewById(R.id.parent).setLayoutParams(lp);	
	}

	
	@Override
	public void finish() {
		// TODO Auto-generated method stub
		p_sv.progress_live=false;
		if(wl!=null)
		{
			
		wl.release();
		wl=null;
		}
		if(!p_sv.progress_toast.equals(""))
			cf.DisplayToast(p_sv.progress_toast);
		super.finish();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
			
		}
			
			
			


		return super.onKeyDown(keyCode, event);
	}
}
