package idsoft.idepot.homeinspection.support;

import idsoft.idepot.homeinspection.CompletedInspection;
import idsoft.idepot.homeinspection.CreateSubModule;
import idsoft.idepot.homeinspection.HomeScreen;
import idsoft.idepot.homeinspection.LoadSubModules;
import idsoft.idepot.homeinspection.NewHVAC1;
import idsoft.idepot.homeinspection.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Currency;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CommonFunction {
  public Context context;
  public String redstar="<font color=red> * </font>",getselectedsrid="",getcurrentview="",getmodulename="",gettempname="";
  public int mYear,mMonth,mDay,mDayofWeek,selmDayofWeek,hours,minutes,amorpm,seconds,mHour, mMinute, mSeconds,wd,ht;
  public String newphone="",cd="",ct="";
  public String[] strarr_module={"Structure","Exterior","Roofing","Attic","Garage/Carport","Plumbing System","Electrical System","HVAC",
			"Kitchen","Appliances and Systems","Bathroom","Interior","Porches/Screens/Enclosures","Swimming Pools","Wells/Pump Equipment","Cabana/Pool House"};
  public String[] structure_subtypes={"Structure Type","Foundation Type","Bearing Walls","Non-Bearing Walls","Beams","Columns", "Floor/Slab",
		  "Ceilings","Roof Structure","Attic Access","Attic Access Type","Basement","Basement - Size","Basement - Access",
		  "Basement - Stairs","Basement - Floors","Basement - Exposed Columns and Beams","Basement - Walls/Ceilings",
		  "Basement - Exposed Floor Systems","Basement - Drain Tile To","Basement - Heat/Cool(HVAC)","Basement - Finishes","Crawlspace",
		  "Crawlspace Area - Characteristics",
		  "Crawlspace - Access","Crawlspace - Main Beam","Crawlspace - Floors","Crawlspace - Exposed Foundation Wall"};
  
  public int[] structurevcarr = {R.array.structuretype_vc,R.array.foundationtype_vc,R.array.bearingwalls_vc,R.array.nonbearingwalls_vc,R.array.beams_vc,
		  R.array.columns_vc,R.array.floorslab_vc,R.array.ceilings_vc,R.array.roofstructure_vc,R.array.atticaccess_vc,R.array.atticaccesstype_vc,
		  R.array.basement_vc,R.array.basementsize_vc,R.array.basementaccess_vc,R.array.basementstairs_vc,R.array.basementfloor_vc,
		  R.array.basementexposedcolandbeams_vc,R.array.basementexposedwallsceilings_vc,R.array.basementexposedfloorsystems_vc,
		  R.array.basementdraintileto_vc,R.array.basementhvac_vc,R.array.basementfinishes_vc,R.array.crawlspace_vc,R.array.crawlspacecharacteristics_vc,		  
		  R.array.crawlspaceaccess_vc,R.array.crawlspacemainbeam_vc,R.array.crawlspacefloors_vc,R.array.crawlspaceexposedfoundationwall_vc};
  
  public String[] exterior_subtypes={
		  "Driveways",
		  "Walkways/Patios/Paths/Yard Areas",
		  "Windows","Windows - Glazing",
		  "Exterior Door - Style", "Exterior Door - Glazing", "Exterior Electric Outlets",
		  "Siding/Surface Materials",  "Masonry/Plastered Walls", "Wood Siding/Paneling", "Wall Shingles",
		  "Deck",
		  "Front Porch",
		  "Electric Meter Location","Main Panel Meter Located Inside", "Main Panel Meter Located Outside","Main Panel Meter",
		  "Gas Meter Location",
		  "Steps",
		  "Balcony",
		  "Exterior Stairways",
		  "Fences and Walls",
		  "Yardwalls",
		  "Retaining Wall",
		  "Accessibility / Limitations"};
  public int[] exteriorvcarr = {R.array.driveways_vc,R.array.walkways_vc,R.array.windows_vc,R.array.exteriordoorstyle_vc,R.array.exteriordoorstyle_vc,
		  R.array.exteriordoorglazing_vc,R.array.exteriorelectricoutlets_vc,R.array.siding_vc,R.array.masonry_vc,R.array.woodsiding_vc,R.array.wallshingles_vc,
		  R.array.deck_vc,R.array.frontporch_vc,R.array.electricmeter_vc,R.array.mainpanelinside_vc,R.array.mainpaneloutside_vc,R.array.mainpanelmeter,
		  R.array.gasmeterlocation_vc,R.array.steps_vc,R.array.balcony_vc,R.array.exteriorstaiways_vc,R.array.fencesandwalls_vc,R.array.yardwalls_vc,R.array.retainingwalls_vc,		  
		  R.array.acceesibilitylocation_vc};
  
  public String[] roof_subtypes={"Inspected From","Roof Type","Roof Covering Type","Roof Shingles","Tile Roof System","Built Up Roof System",
		  "Roll Roofing","Flashings","Skylights","# of Skylights","Skylight Type","Roof Complexity","Leak Probability","5 Year Replacement Probability",
		  "Parts Not Visible/Walked Due To","Fascia/Soffit","Chimney Structure","Gutters and Downspouts","Attic Ventilation","Visible Insulation"};
  
  public int[] roofingvcarr = {R.array.inspectedfrom_vc,R.array.rooftype_vc,R.array.roofcoveringtype_vc,R.array.roofshingles_vc,R.array.tileroofsystem_vc,
		  R.array.builtuproofsystem_vc,R.array.rollroofing_vc,R.array.flashings_vc,R.array.skylights_vc,R.array.noofskylights_vc,R.array.skylighttype_vc,
		  R.array.roofcomplexity_vc,R.array.leakprobability_vc,R.array.yearrepprob_vc,R.array.partsnotvisible_vc,R.array.fasciasoffit_vc,R.array.chimneystruct_vc,
		  R.array.gutters_vc,R.array.atticventilation_vc,R.array.visibleinsulation_vc};
  
  
  public String[] garage_subtypes={"Garage/Carport Type","Siding/Surface Materials","Windows","Exterior Service Door","Garage Door",
		  "Garage Door Type","Garage Door Automatic Opener","Safety Reverse Present","Gutters/Eaves Trough","Downspouts","Trim","Garage Floor",
		  "Sill Plates",
		  "Electrical Receptacles - Operable","Fire Separation Present","Fire Door To Main Dwelling Present"};
  
  public int[] garagevcarr = {R.array.garagecarporttype_vc,R.array.sidingsurface_vc,R.array.windowsgarage_vc,R.array.exteriorservicedoor_vc,R.array.garagedoor_vc,
		  R.array.garagedoortype_vc,R.array.garagedoorautomaticopener_vc,R.array.safetreversepresent_vc,R.array.gutterseavestrough_vc,R.array.downspouts_vc,R.array.trim_vc,
		  R.array.garagefloor_vc,R.array.sillplates_vc,R.array.electricalreceptacles_vc,R.array.fireseppresent,R.array.firedoordwellpresent};
  
  public String[] plumbing_subtypes={"Water Supply Source","Water Main","Water Meter Location", "Water Pressure","Water Turned Off","Water Meter Flow Detector Stable",
		  "Water Conditioner Present","Supply Pipes","Drainage Service","Septic Tank Present","Drain Field Location","Drains/Vent Piping","Main Drain Clean Out Location",
		  "Other Plumbing Features"};
  
  public int[] plumbingvcarr = {R.array.watersupplysource_vc,R.array.watermain_vc,R.array.watermeterloc_vc,R.array.waterpreesure_vc,R.array.waterturnedoff_vc,
		  R.array.watermeterflowdetstable_vc,R.array.waterconditinerpresent_vc,R.array.supplypipes_vc,R.array.drainageservice_vc,R.array.septictankpresent_vc,R.array.drainfieldloc_vc,
		  R.array.drainsventpiping_vc,R.array.maindraincleanoutloc_vc,R.array.otherplumbingfeatures_vc};
  
  public String[] electrical_subtypes={"Service Entrance","Voltage Rating","Service Entrance Conductors","Main Panel Location","Main Panel Type","Main Panel Manufacturer",
		  "Main Panel Amps","Sub Panel(s) Present","Sub Panel Manufacturer","Service Grounding To","120 Volt Wiring Type","240 Volt Wiring","5 Year Wiring Replacement Probability",
		  "Ground Fault Interruption Present","Arc Fault Interruption Present","120 Outlets","Light Switches","Light Fixtures","Ceiling Fans Present","Smoke Detectors",
		  "Carbonmonoxide Monitors","Other Electrical Fixtures/Systems"};
  
  
  public int[] electricalvcarr = {R.array.serviceentrance_vc,R.array.voltagerating_vc,R.array.serventrancecond_vc,R.array.mainpaneleloc_vc,R.array.mainpaneltype_vc,R.array.mainpanelmanu_vc,
		  R.array.mainpanelamps_vc,R.array.subpanelpresent_vc,R.array.subpanelmanu_vc,R.array.servicegroundingto_vc,R.array.wiringtype_vc,R.array.wiring_vc,R.array.yearreplprob_vc,
		  R.array.grndfaultpres_vc,R.array.arcfaultintpres_vc,R.array.outlets_vc,R.array.lightswitches_vc,R.array.lightfixtures,R.array.ceilingfanspres_Vc,R.array.smokedetectors,
		  R.array.carbonmonoxidemonitors_vc,R.array.otherelctrical_vc};
  
  
  public String[] heating_subtypes={"Energy Type","Delivery Type","# of Heat Zones","Furnace Type","Flue","Motor Blower",
		  "Furnace Features","Supply Registers Located","Return Registers Located"};
  public String[] cooling_subtypes={"# of Cooling Zones","Tonnage","Characteristics","Condensation","Cooling Testing","Other Units"};
  
  public String[] kitchen_subtypes={"Kitchen Sink(s)","Kitchen Sink Type/Features","Cabinetry/Counter Tops","Disposal Switch Location","Instant Hot Water Feature Present"};
  public int[] kitchenvcarr = {R.array.kitchensink_vc,R.array.kitchensinktypefeat_vc,R.array.cabinetrycountertops_vc,R.array.disposalswitchloc_vc,R.array.instanthotwaterfeatpres_vc};
  
  
  public String[] bathroom_subtypes={"Fixtures","Bath Materials","Safety Glass Stamp at Shower Enclosure","Bath Other"};
  
  public String[] interior_subtypes={"Floor Finishes","Wall Finish","Ceiling Finish","Windows","Interior Doors","Internal Stairs","Fireplace"};
  public int[] interiorvcarr = {R.array.floorfinishes_vc,R.array.wallfinish_vc,R.array.ceilingsfinish_vc,R.array.windows_vc,R.array.interiordoors_vc,R.array.internalstairs_vc,R.array.fireplace_vc};
  
  
  
  public String[] porches_subtypes={"Porches/Screens/Enclosures"};
  public int[] porchesvcvcarr = {R.array.porchesscreensenc_vc};
  
  public String[] pool_subtypes={"Pool Style/Type","Spa - Whirlpool Style/Type","Water Clarity","Pool Shape","Pool Shell Material","Pool Deck","Pool Coping",
		  "Pool Features","Pool Lighting","Pool Equipment","Plumbing Systems","Pool Pumping System","Filtering System","Pool Cleaning Equipment",
		  "Water Treatment","Water Heating Systems","Timers/Clocks/Controls","Electrical Service","Pool Cover(s)","Safety Equipment","Water Chemistry Test","General Comments/Observations"};
  
  public int[] poolvcvcarr = {R.array.poolstyle_vc,R.array.spa_vc,R.array.waterclarity_vc,R.array.poolshape_vc,R.array.poolshellmaterial_vc,R.array.pooldeck_vc,
		  R.array.poolcoping_vc,R.array.poolfeatures_vc,R.array.poollighting_vc,R.array.poolequipment_vc,R.array.plumbingsytem_vc,R.array.poolplumbingsystem,R.array.filteringsystem_vc,R.array.poolcleaningequip_vc,
		  R.array.watertreatment_vc,R.array.waterheatingsystem_vc,R.array.timers_vc,R.array.electricalservice_vc,R.array.poolcover_vc,R.array.safetyequip_vc,R.array.waterchemistry_vc,R.array.generalcomments_vc};
  
  public String[] well_subtypes={"Pump Type","Accessories","Well Type"};
  public int[] wellvcarr = {R.array.pumptype_vc,R.array.accessores_vc,R.array.welltype_vc};
  
  
  public String[] cabana_subtypes={"Roof Covering","Siding Style","Plumbing Supply","Water Heater Power Source","HVAC"};
  public int[] cabanavcarr = {R.array.roofocvering_vc,R.array.sidingstyle_vc,R.array.plumbingsupply_vc,R.array.waterheaterpowersource_vc,R.array.hvac_vc};
  
  public String[] app_subtypes={"Washing Machine Present","Dryer Present","Dishwasher Present","Disposal Present","Oven Present","Exhaust Fan System Present",
		  "Refrigerator Present","Microwave Present","Trash Compactor Present","Bread Warmer Present","Range top Present","Lawn Sprinkler system Present","Water Softener Present","Water heater Present","Attic/House fan Present","Door bell Present"};
  
  public int[] appvcarr = {R.array.washingmacpres_vc,R.array.dryerpres_vc,R.array.dishwasherpres_vc,R.array.disposalpres_vc,R.array.ovenpresent_vc,R.array.exhaustfansys_vc,
		  R.array.refrpres_vc,R.array.microwavepres_vc,R.array.trashcompactorpres_vc,R.array.breadwarmpres_vc, R.array.rangetoppres_vc, R.array.lawnsprinklersys_vc, R.array.watersoftenerpres_vc,R.array.waterheaterpres_vc,R.array.attichousepanpres_vc,R.array.doorbellpres_vc};
  
		  
  public String[] attic_subtypes={"Restricted Access To","Visible Insulation","Insulation Thickness","Ventilation",
		  "# of Attic Fans","# of Turbines Vents","Firewalls/Ceilings"};
 
  public int[] atticvcarr = {R.array.restrictedaccessto_vc,R.array.atticvisibleinsulation_vc,R.array.insulation_thickness_vc,R.array.ventilation_vc,
		  R.array.noofatticfans_vc,R.array.turbinesvents_vc,R.array.firewallsceilings_vc};
  
  
  
  
  public int[] vcnames ;
  public String[] arrsub;
  public String[] submodvcid;
  public String[] insp_arr ={"Not Visible","Not Applicable","Repair/Replace","Further Evaluation","Monitor Over Time","Obtain Disclosure","Engineering",
		  "Major Condition","Minor Condition","Standard Disclosure","Additional Evaluation Advised Prior To Closing","Unknown/Subjective Judgement",
		  "Safety Hazard","Include In Summary","Yes","No"};
 
  
  public String[] contractortype_arr ={"Additions","Air Conditioning & Cooling","Appliances","Architects & Designers","Barns Sheds & Outbuildings","Basements","Bath","Cabinets","Carpenters",
		  "Ceilings","Central Heating System","Cleaning Services","Closets & Garage Organizers","Commercial Property Maintenance","Concreate","Countertops",
		  "Deck & Porches","Demolition","Disability Alternation","Doors","Drywall","Ducts & Vents","Electrical","Fences","Fireplaces","Flooring","Foundation",
		  "Framing","Garage & Carport","Garden Feature","Gas Piping","Gutters","Handyman Services","Heating","Home Automation","Home Builders","Hot Tubs & Spas",
		  "Inspection Services","Insulation","Interior Decorators & Designers","Kitchen","Land Clearing /Bldg Site Perp","Landscaping","Lawn Care","Lighting",
		  "Locksmith","Major Home Repairs & Restoration","Mirrors","Mobile Homes","Mold & Mildew","Multiple Room Renovation","Painting & Staining",
		  "Patios/Walks/Walls/etc","Paving","Pest Control","Phone Computer & Entertainment","Plaster & Stuco","Plumbing","Powerwashing & Sandblasting","Roofs",
		  "Sauna","Security","Septic System","Siding","Skylight","Snow Removal","Solar Energy","SunRooms","Surveyors & Engineers","Swimming Pools","Tiles",
		  "Trash & Hazardous Materials Removal","Open Protection/Shutters","Wallpaper & Wallcoverings","Water Heater & Tanks","Water Supply & Wells","Waterproofing",
		  "Welding","Window Covering(Bind Shades)","Windows","Wind-Hurricane Proofing"};
  
  
  
  
  public int[] vcarr = {R.array.structure_vc,R.array.exterior_vc,R.array.garage_vc,R.array.plumbing_vc,R.array.electrical_vc,
		  R.array.heating_vc,R.array.air_vc,R.array.kitchen_vc,R.array.bathroom_vc,R.array.interior_vc,R.array.interior_vc,R.array.pool_vc,
		  R.array.waterwells_vc,R.array.appliance_vc,R.array.attic_vc,R.array.roof_vc};
  DatabaseFunction db;
  Webservice_config wb;
  static public int chkstat=0;
  static public Boolean boolhvac = false;
  public Random rgenerator = new Random();
	  public String word="";
	public EditText wrdedit;
	public Progress_staticvalues p_sv;
	public int handler_msg=2;
	public String error_msg="",title="";
	
	public CommonFunction(Context con) {
		// TODO Auto-generated constructor stub
		context = con;
		db = new DatabaseFunction(con);
		wb = new Webservice_config(con);
	}
	public void DisplayToast(String string) {
		// TODO Auto-generated method stub
		      
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        View toastRoot = inflater.inflate(R.layout.toast, null);
	          
	        Toast toast = new Toast(context);
	        TextView tv = (TextView) toastRoot.findViewById(R.id.toasttext);
	        tv.setText(string);
	        toast.setView(toastRoot);
	        toast.setGravity(Gravity.CENTER_VERTICAL,0, 10);
	        toast.setDuration(Toast.LENGTH_SHORT);
	        toast.show();
	}
	public void getcalender()
	{
		Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

	}
	public boolean PhonenumValidation(String strcontactphone) {
		// TODO Auto-generated method stub
		if (strcontactphone.length() == 13) {
			StringBuilder sVowelBuilder = new StringBuilder(strcontactphone);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ")");
			sVowelBuilder.insert(8, "-");
			newphone = sVowelBuilder.toString();
			return true;
            
		} else {
			return false;
		}

	}
	public boolean EmailValidation(String stremail) {
		// TODO Auto-generated method stub
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(stremail);

		return matcher.matches();
	}
	public void GetCalender() {
		// TODO Auto-generated method stub
			    final Calendar c = Calendar.getInstance();
				mYear = c.get(Calendar.YEAR);
				mMonth = c.get(Calendar.MONTH);
				mDay = c.get(Calendar.DAY_OF_MONTH);
				
				mDayofWeek = c.get(Calendar.DAY_OF_WEEK);
				hours = c.get(Calendar.HOUR);
				minutes = c.get(Calendar.MINUTE);
				seconds = c.get(Calendar.SECOND);
				amorpm = c.get(Calendar.AM_PM);
				
				cd = mDay + "/" + (mMonth + 1) + "/" + mYear;
				
				final Calendar T = Calendar.getInstance();
				mHour = T.get(Calendar.HOUR_OF_DAY);
				mMinute = T.get(Calendar.MINUTE);
				mSeconds = T.get(Calendar.SECOND);
				ct = mHour + ":" + mMinute + ":" + mSeconds;
		
	
	}
	public void HideKeyboard(EditText et_tempname) {
		// TODO Auto-generated method stub
			  InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
			  imm.hideSoftInputFromWindow(et_tempname.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
	}
	public void setValuetoCheckbox(CheckBox[] checkbox, String value) {
		// TODO Auto-generated method stub
		if(value.endsWith("&"))
		{
			System.out.println("ends with="+value);
		}
		if(value.contains("&#44;"))
		{
			String[] val= value.split("&#44;");
			for(int i=0;i<checkbox.length;i++)
			{
				for(int j=0;j<val.length;j++)
				{
					if(checkbox[i].getText().toString().trim().equals(val[j].trim()))
					{
						checkbox[i].setChecked(true);
					}
				}
			}
		}
		else if(value.contains(","))
		{
			String[] val= value.split(",");
			for(int i=0;i<checkbox.length;i++)
			{
				for(int j=0;j<val.length;j++)
				{
					if(checkbox[i].getText().toString().trim().equals(val[j].trim()))
					{
						checkbox[i].setChecked(true);
					}
				}
			}
		}
		else
		{
			for(int i=0;i<checkbox.length;i++)
			{
					if(checkbox[i].getText().toString().equals(value.trim()))
					{
						checkbox[i].setChecked(true);
					}
				
			}
		}
	}
	public void GetCurrentModuleName(String getmodulename) {
		// TODO Auto-generated method stub
		System.out.println("test"+getmodulename);
		if(getmodulename.equals("Structure"))
		{
			arrsub = structure_subtypes;System.out.println("arrsub"+arrsub.length+"submodule");
			/*for(int i=0;i<arrsub.length;i++)
			{
				if(getmodulename.equals("Structure"))
				System.out.println("structure_subtypes"+structurevcarr.length+"arrsub="+arrsub[i]);
				submodvcid = context.getResources().getStringArray(structurevcarr[i]);System.out.println("struc="+submodvcid.length);
			}*/
			//submodvcid = context.getResources().getStringArray(vcarr[0]);
		}
		else if(getmodulename.equals("Exterior"))
		{
			arrsub = exterior_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[1]);
		}
		else if(getmodulename.equals("Garage/Carport"))
		{
			arrsub = garage_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[2]);
		}
		else if(getmodulename.equals("Plumbing System"))
		{
			arrsub = plumbing_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[3]);
		}
		else if(getmodulename.equals("Electrical System"))
		{
			arrsub = electrical_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[4]);
		}
		else if(getmodulename.equals("HVAC"))
		{
			arrsub = heating_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[5]);
		}
		else if(getmodulename.equals("Central Air Conditioning System"))
		{
			arrsub = cooling_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[6]);
		}
		else if(getmodulename.equals("Kitchen"))
		{
			arrsub = kitchen_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[7]);
		}
		else if(getmodulename.equals("Bathroom"))
		{
			arrsub = bathroom_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[8]);
		}
		else if(getmodulename.equals("Interior"))
		{
			arrsub = interior_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[9]);
		}
		else if(getmodulename.equals("Porches/Screens/Enclosures"))
		{
			arrsub = porches_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[10]);
		}
		else if(getmodulename.equals("Swimming Pools"))
		{
			arrsub = pool_subtypes;
		//	submodvcid = context.getResources().getStringArray(vcarr[11]);
		}
		else if(getmodulename.equals("Wells/Pump Equipment"))
		{
			arrsub = well_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[12]);
		}
		else if(getmodulename.equals("Cabana/Pool House"))
		{
			arrsub = cabana_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[12]);
		}
		else if(getmodulename.equals("Appliances and Systems"))
		{
			arrsub = app_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[13]);
		}
		else if(getmodulename.equals("Attic"))
		{
			arrsub = attic_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[14]);
		}
		else if(getmodulename.equals("Roofing"))
		{
			arrsub = roof_subtypes;
			//submodvcid = context.getResources().getStringArray(vcarr[15]);
		}
	}
	public void GetVC(String getmodulename,String submodule) {
		// TODO Auto-generated method stub
		if(getmodulename.equals("Structure"))
		{
			arrsub = structure_subtypes;
			getvcname(submodule,structurevcarr);
		}
		else if(getmodulename.equals("Exterior"))
		{
			arrsub = exterior_subtypes;
			getvcname(submodule,exteriorvcarr);
		}
		else if(getmodulename.equals("Roofing"))
		{
			arrsub = roof_subtypes;
			getvcname(submodule,roofingvcarr);
		}
		else if(getmodulename.equals("Garage/Carport"))
		{
			arrsub = garage_subtypes;
			getvcname(submodule,garagevcarr);
		}
		else if(getmodulename.equals("Plumbing System"))
		{
			arrsub = plumbing_subtypes;
			getvcname(submodule,plumbingvcarr);
		}
		else if(getmodulename.equals("Electrical System"))
		{
			arrsub = electrical_subtypes;
			getvcname(submodule,electricalvcarr);
		}
		else if(getmodulename.equals("HVAC"))
		{
			arrsub = heating_subtypes;
			submodvcid = context.getResources().getStringArray(vcarr[5]);
		}
		else if(getmodulename.equals("Central Air Conditioning System"))
		{
			arrsub = cooling_subtypes;
			submodvcid = context.getResources().getStringArray(vcarr[6]);
		}
		else if(getmodulename.equals("Kitchen"))
		{
			arrsub = kitchen_subtypes;
			getvcname(submodule,kitchenvcarr);
		}
		else if(getmodulename.equals("Bathroom"))
		{
			arrsub = bathroom_subtypes;
			submodvcid = context.getResources().getStringArray(vcarr[8]);
		}
		else if(getmodulename.equals("Interior"))
		{
			arrsub = interior_subtypes;
			getvcname(submodule,interiorvcarr);
			
		}
		else if(getmodulename.equals("Porches/Screens/Enclosures"))
		{
			arrsub = porches_subtypes;
			getvcname(submodule,porchesvcvcarr);
			
		}
		else if(getmodulename.equals("Swimming Pools"))
		{
			arrsub = pool_subtypes;
			getvcname(submodule,poolvcvcarr);
		}
		else if(getmodulename.equals("Wells/Pump Equipment"))
		{
			arrsub = well_subtypes;
			getvcname(submodule,wellvcarr);
		}
		else if(getmodulename.equals("Cabana/Pool House"))
		{
			arrsub = cabana_subtypes;
			getvcname(submodule,cabanavcarr);
		}
		else if(getmodulename.equals("Appliances and Systems"))
		{
			arrsub = app_subtypes;
			getvcname(submodule,appvcarr);
		}
		else if(getmodulename.equals("Attic"))
		{
			arrsub = attic_subtypes;
			getvcname(submodule,atticvcarr);
		}
	}
	public void getvcname(String submodule,int vcarr[]) {
		// TODO Auto-generated method stub
System.out.println("arrsub"+vcarr.length);
		for(int i=0;i<arrsub.length;i++)
		{
			System.out.println("submodule="+submodule);
			System.out.println("arrsub[i]="+arrsub[i]);
			if(submodule.equals(arrsub[i]))
			{
				System.out.println("structure_subtypes"+structurevcarr.length+"arrsub="+arrsub[i]+"i="+i);
				submodvcid = context.getResources().getStringArray(vcarr[i]);System.out.println("struc="+submodvcid.length);
			}
		}
	}
	public void setvaluerd(RadioGroup radioGroup, String string) {
		// TODO Auto-generated method stub
		for(int i=0;i<radioGroup.getChildCount();i++)
		{
			if(radioGroup.getChildAt(i) instanceof RadioButton)
			{
				if(((RadioButton)radioGroup.getChildAt(i)).getText().toString().trim().equals(string))
				{
					((RadioButton)radioGroup.getChildAt(i)).setChecked(true);
				}
			}
		}
	}
	public void setTouchListener(View v) {
		// TODO Auto-generated method stub
		if(v instanceof ViewGroup)
		{
			ViewGroup vg=(ViewGroup)v;
			for(int i=0;i<vg.getChildCount();i++)
			{
				if((vg.getChildAt(i)) instanceof ViewGroup)
				{
					setTouchListener(vg.getChildAt(i));
				}
				else if((vg.getChildAt(i)) instanceof EditText)
				{
					EditText ed=(EditText)(vg.getChildAt(i));
					ed.setOnTouchListener(new TouchFoucs(ed));
					
				}
			}
		}
	}
	public void setValuetoRadio(RadioButton[] radio, String value) {
		// TODO Auto-generated method stub
		System.out.println("value="+value);
		for(int i=0;i<radio.length;i++)
		{
			   if(radio[i].getText().toString().equals(value.trim()))
				{
					radio[i].setChecked(true);
					return;
				} 
			 
			 
		}
	}
	public void hidekeyboard(EditText et_defectcomments) {
		// TODO Auto-generated method stub
		  InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		  imm.hideSoftInputFromWindow(et_defectcomments.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
		 
	}
	public void hideskeyboard() {
		// TODO Auto-generated method stub
		
			InputMethodManager imm = (InputMethodManager)context.getSystemService(Activity.INPUT_METHOD_SERVICE);
		    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		
	}
	public void ShowingLimit(String string, TextView tvcomment, int i) {
		// TODO Auto-generated method stub
		int length=string.toString().length();
		
	    int percent=(int)((length)*(100.00/Double.parseDouble(String.valueOf(i))));
	    
	    if(length==i)
	    {
	    	tvcomment.setText(Html.fromHtml("   "+percent+" % Exceed limit"));
	    	tvcomment.setTextColor(Color.RED);
	    }
	    else
	    {
	    	tvcomment.setText("   "+percent+" %");
	    	tvcomment.setTextColor(Color.parseColor("#9900FF"));
	    }

	}
	public String getvaluefromchk(CheckBox[] chk) {
		// TODO Auto-generated method stub
		String txt="";
		for(int i=0;i<chk.length;i++)
		{
			if(chk[i].isChecked())
				txt+=chk[i].getText().toString().trim()+"&#44;";
		}
		if(txt.length()>1)
		{
			txt=txt.substring(0,txt.lastIndexOf("&#44;"));
		}
		System.out.println("txt="+txt);
		return txt;
	}
	public boolean isInternetOn() {
		// TODO Auto-generated method stub
		boolean chk = false;
		ConnectivityManager conMgr = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			chk = true;
		} else {
			chk = false;
		}
System.out.println("chk="+chk);
		return chk;
	}
	public void getDeviceDimensions() {
		// TODO Auto-generated method stub
		DisplayMetrics metrics = new DisplayMetrics();
		((Activity) this.context).getWindowManager().getDefaultDisplay().getMetrics(metrics);

		wd = metrics.widthPixels;
		ht = metrics.heightPixels;
	}
	public void setvaluechkwithother(String typechk_val, CheckBox[] typechk,EditText editText) {
			// TODO Auto-generated method stub
			System.out.println("typechk_val"+typechk_val);
			if(!typechk_val.equals(""))
			{
				if(typechk_val.endsWith("&"))
				{
					typechk_val = typechk_val.substring(0,typechk_val.length()-1);
				}
				
				if(typechk_val.contains("&#44;"))
				{
					typechk_val=typechk_val.replace("&#44;", "&#94;");
				}
				else
				{
					typechk_val = typechk_val;
				}
				
				
				String k=typechk_val.replace("&#94;", "&#126;");
				String temp[]=k.split("&#126;");
				
				int j=0;
				do{
					System.out.println("typechk"+typechk.length);
					for(int i=0;i<typechk.length;i++)
					{
						System.out.println("temp[j]"+temp[j]+"typechk="+typechk[i].getText().toString().trim());
						if(temp[j].trim().equals(typechk[i].getText().toString().trim()))
						{
							typechk[i].setChecked(true);
							j++;
							if(j==temp.length)
							{
								return;
							}
						}
						
					}
					if(j!=temp.length)
					{
						
						typechk[typechk.length-1].setChecked(true); // no matches found for the last value in the split text so we set the other to tru  
						editText.setVisibility(View.VISIBLE);
						editText.setText(temp[j]);
						j++;	
					}
				}while(j<temp.length);
			}
		}
	public boolean eMailValidation(CharSequence stringemailaddress1) {

		// TODO Auto-generated method stub
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(stringemailaddress1);

		return matcher.matches();

	}
	public 	Bitmap ShrinkBitmap(String file, int width, int height) { try {
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
	
		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);
	
		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}
	
		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		return bitmap;
	} catch (Exception e) {
		return null;
	}
	}
	public void set_header_fonts(View v) {
		// TODO Auto-generated method stub
		
		Typeface tf =	Typeface.createFromAsset(context.getAssets(),"fonts/squadaone-regular.ttf");
		set_font(v,tf);
	}
	public boolean check_Status(String result) {
		// TODO Auto-generated method stub
		try
		{
			if(result!=null)
			{
			if(result.trim().equals("true"))
			{
				return true;
			}
			else
			{
				return false;
			}
			}
			else
			{
				return false;
			}
			
		}
		catch (Exception e)
		{
			return false;
		}
	}
	private void set_font(View v,Typeface tf)
	{
		if(v instanceof TextView)
			((TextView) v).setTypeface(tf);
		else if(v instanceof EditText)
			((EditText) v).setTypeface(tf);
		else if(v instanceof RadioButton)
			((RadioButton) v).setTypeface(tf);
		else if(v instanceof CheckBox)
			((CheckBox) v).setTypeface(tf);
		else if(v instanceof Button)
			((Button) v).setTypeface(tf);
	}
	public String replacelastendswithcomma(String realword) {
		// TODO Auto-generated method stub
		if(realword.contains("&#44;"))
		{
			realword = realword.replace("&#44;",",");
		}
		if(realword.endsWith(","))
		{
			realword = realword.substring(0,realword.length()-1);
		}
		else
		{
			if(realword.contains("Other"))
			{
				if(realword.contains("&#94;"))
				{
					realword = realword.replace("&#94;","(");
					realword +=")";
				}
			}
			else
			{
				realword = realword;
			}
		}	
		return realword;
	}
	
	public void HVACexport(String srid,String currentview,String modulename,String templatename)
	{
		System.out.println("gettemplatename"+templatename);
		db.userid();
		getselectedsrid = srid;
		getcurrentview = currentview;
		getmodulename = modulename; 
		gettempname = templatename;
		System.out.println("gettempa="+gettempname);
		if(isInternetOn())
		  {
			   new Start_xporting().execute("");
		  }
		  else
		  {
			  DisplayToast("Please enable your internet connection.");
			  
		  } 
	}
	public void showsubmitalert(String srid,String currentview,String modulename,String tempname) {
		db.userid();
		getselectedsrid = srid;
		getcurrentview = currentview;
		getmodulename = modulename; 
		gettempname = tempname;
		 final Dialog dialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
		//dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.setContentView(R.layout.input_submit_inspection);		
				
		 final String myString[] = context.getResources().getStringArray(R.array.myArray);
		 word = myString[rgenerator.nextInt(myString.length)];
		 wrdedit = ((EditText) dialog.findViewById(R.id.wrdedt));
		 
		 wrdedit.setText(word);
		 if (word.contains(" ")) { word = word.replace(" ", ""); }
			
		 
		db.userid();
		  ((TextView) dialog.findViewById(R.id.tv1)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"			
							 + db.Userfirstname +" "+db.Userlastname
							+ "</font></b><font color=black> have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
							+ "</font>"));
		 
			((TextView) dialog.findViewById(R.id.tv2)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ db.Userfirstname +" "+db.Userlastname
							+ "</font></b><font color=black> also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
							+ "</font>"));
			
			
			((Button) dialog.findViewById(R.id.submit)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (((RadioButton) dialog.findViewById(R.id.accept)).isChecked() == true) {
						
						if (((EditText) dialog.findViewById(R.id.wrdedt1)).getText().toString().equals("")) 
						{
							DisplayToast("Please enter Word Verification.");
							((EditText) dialog.findViewById(R.id.wrdedt1)).requestFocus();
						} 
						else
						{
							if (((EditText) dialog.findViewById(R.id.wrdedt1)).getText().toString().equals(word.trim().toString())) 
							{
								System.out.println("111");
								dialog.cancel();
								if(isInternetOn())
								  {
									   new Start_xporting().execute("");
								  }
								  else
								  {
									  DisplayToast("Please enable your internet connection.");
									  
								  } 
							} 
							else
							{
								DisplayToast("Please enter valid Word Verification(case sensitive).");
								((EditText) dialog.findViewById(R.id.wrdedt1)).setText("");
								((EditText) dialog.findViewById(R.id.wrdedt1)).requestFocus();
							}
						}
					} 
					else 
					{
						DisplayToast("Please select Accept Radio Button.");
					}

				}
			});			
			
			((Button) dialog.findViewById(R.id.cancel)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});			
			
			((Button) dialog.findViewById(R.id.S_refresh)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					word = myString[rgenerator.nextInt(myString.length)];			
					 ((EditText) dialog.findViewById(R.id.wrdedt)).setText(word);
					if (word.contains(" ")) {
						word = word.replace(" ", "");
					}	
					
				}
			});			
		dialog.show();
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		     
		       	p_sv.progress_Msg="Please wait..Your template has been submitting.";
		        
			    
		     System.out.println("");
				((Activity) context).startActivityForResult(new Intent(context,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		System.out.println("insde");
			 		if(getmodulename.equals("HVAC"))
			 		{
				 		Submit_HVAC();
				 		Submit_Ductwork();
				 		Submit_Systembrands();
				 		Submit_Thermostat();
				 		Submit_Hotwatersystem();
				 		Submit_NotApplicable();
			 		} 
			 		else if(getmodulename.equals("Bathroom"))
			 		{
			 			Submit_Bathroom();
			 		}
			 		      
			 		
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
      	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	public void Submit_Bathroom() throws IOException, XmlPullParserException 
	{
		Cursor c1;SoapObject requestbathroom;
		db.CreateTable(15);
		db.CreateTable(16);
		if(getcurrentview.equals("start"))
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.LoadBathroom + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'", null);	
			 	System.out.println("select * from " + db.LoadBathroom + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'"+c1.getCount());
		}
		else
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.CreateBathroom + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+gettempname+"'", null);
		}
		
	          if(c1.getCount()>0)
	          {
	        	 c1.moveToFirst();   	        
				 do
				 {		
					 if(getcurrentview.equals("start"))
				 	 {
						  requestbathroom = new SoapObject(wb.NAMESPACE,"Load_Bathroom");
				 	 }
					 else
					 {
						  requestbathroom = new SoapObject(wb.NAMESPACE,"Create_Bathroom");
					 }				 
					 
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
					envelope1.dotNet = true;
    		    
	    		    requestbathroom.addProperty("br_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	    		    requestbathroom.addProperty("fld_inspectorid",db.UserId);
	    		    if(getcurrentview.equals("start"))
				 	 {
	    		    	requestbathroom.addProperty("fld_srid",getselectedsrid);
				 	 }
	    		    requestbathroom.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	    		    requestbathroom.addProperty("fld_bathroomname",db.decode(c1.getString(c1.getColumnIndex("fld_bathroomname"))));
	    		    requestbathroom.addProperty("fld_otherbathroomname",db.decode(c1.getString(c1.getColumnIndex("fld_otherbathroomname"))));
	    		    
		    		String strtubs = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_tubs")))).toString();
		       		if(strtubs.endsWith(","))
		       		{
		       			strtubs = strtubs.substring(0,strtubs.length()-1);
		       		}
		       		if(strtubs.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_tubs",strtubs.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		  	requestbathroom.addProperty("fld_tubs",strtubs);
	      		    }
		       	
		       		
		       		String strsinks = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_sinks")))).toString();
		       		if(strsinks.endsWith(","))
		       		{
		       			strsinks = strsinks.substring(0,strsinks.length()-1);
		       		}
		       		if(strsinks.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_sinks",strsinks.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_sinks",strsinks);	
	      		    }
		       		
		       		
		    		    
		       		String strshowerenclos = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_showerenclosures")))).toString();
		       		if(strshowerenclos.endsWith(","))
		       		{
		       			strshowerenclos = strshowerenclos.substring(0,strshowerenclos.length()-1);
		       		}
		       		if(strsinks.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_showerenclosures",strshowerenclos.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_showerenclosures",strshowerenclos);	
	      		    }
		       		
		       		String strsafetyglass = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_safetyglass")))).toString();
		       		if(strsafetyglass.endsWith(","))
		       		{
		       			strsafetyglass = strsafetyglass.substring(0,strsafetyglass.length()-1);
		       		}
		       		if(strsafetyglass.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_safetyglass",strsafetyglass.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_safetyglass",strsafetyglass);	
	      		    }
		       		
		       		
		       		
		       		
		       		if(db.decode(c1.getString(c1.getColumnIndex("fld_othersafetyglass"))).contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_othersafetyglass",db.decode(c1.getString(c1.getColumnIndex("fld_othersafetyglass"))).replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_othersafetyglass",db.decode(c1.getString(c1.getColumnIndex("fld_othersafetyglass"))));
	      		    }
		    		
		       		
		       		String strtoilets = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_toilets")))).toString();
		       		if(strtoilets.endsWith(","))
		       		{
		       			strtoilets = strtoilets.substring(0,strtoilets.length()-1);
		       		}
		       		if(strtoilets.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_toilets",strtoilets.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_toilets",strtoilets);	
	      		    }

		       		
		       		String strbathfix = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_bathfixturematerials")))).toString();
		       		if(strbathfix.endsWith(","))
		       		{
		       			strbathfix = strbathfix.substring(0,strbathfix.length()-1);
		       		}
		       		if(strbathfix.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_bathfixturematerials",strbathfix.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_bathfixturematerials",strbathfix);
	      		    }
		       		
		       		
		       		String strsupplydrainage = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_supplydrainage")))).toString();
		       		if(strsupplydrainage.endsWith(","))
		       		{
		       			strsupplydrainage = strsupplydrainage.substring(0,strsupplydrainage.length()-1);
		       		}
		       		if(strsupplydrainage.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_supplydrainage",strsupplydrainage.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_supplydrainage",strsupplydrainage);	
	      		    }
		       		
		       		
		       		if(db.decode(c1.getString(c1.getColumnIndex("fld_supplydrainageother"))).contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_supplydrainageother",db.decode(c1.getString(c1.getColumnIndex("fld_supplydrainageother"))).replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_supplydrainageother",db.decode(c1.getString(c1.getColumnIndex("fld_supplydrainageother"))));
	      		    }
		       		
		       		String strbathother = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_bathother")))).toString();
		       		if(strbathother.endsWith(","))
		       		{
		       			strbathother = strbathother.substring(0,strbathother.length()-1);
		       		}
		       		if(strbathother.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_bathother",strbathother.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_bathother",strbathother);	
	      		    }
		       		
		    		    
		    		String strotherfeat = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_otherfeatures")))).toString();
		       		if(strotherfeat.endsWith(","))
		       		{
		       			strotherfeat = strotherfeat.substring(0,strotherfeat.length()-1);
		       		}
		       		if(strotherfeat.contains(","))
	      		    {
	      		    	requestbathroom.addProperty("fld_otherfeatures",strotherfeat.replace(",", ", "));
	      		    }
	      		    else
	      		    {
	      		    	requestbathroom.addProperty("fld_otherfeatures",strotherfeat);
	      		    }

		    		    requestbathroom.addProperty("fld_bathcomments",db.decode(c1.getString(c1.getColumnIndex("fld_bathcomments"))));	
		 		
		    		    System.out.println("Bathroom request="+requestbathroom);
		    		    envelope1.setOutputSoapObject(requestbathroom);
		       		
		       		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
		       		SoapObject response =null;
		       		try
		            {
			       		 if(getcurrentview.equals("start"))
					 	 {
			       			androidHttpTransport1.call(wb.NAMESPACE+"Load_Bathroom", envelope1);
					 	 }
						 else
						 {
							 androidHttpTransport1.call(wb.NAMESPACE+"Create_Bathroom", envelope1);
						 }					 
		                 
			       		 response = (SoapObject)envelope1.getResponse();System.out.println("REAPONSE="+response);
		                 if(response.getProperty("StatusCode").toString().equals("0"))
		                 {
			                 	  try
				          		  {
			                 		 if(getcurrentview.equals("start"))
		             	    		 {
			                 			 System.out.println("UPDATE "+db.LoadBathroom+ " SET fld_sendtoserver='"+response.getProperty("br_id").toString()+"' " +
			 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedsrid+"' and fld_bathroomname='"+c1.getString(c1.getColumnIndex("fld_bathroomname"))+"'");
		             	    			  db.hi_db.execSQL("UPDATE "+db.LoadBathroom+ " SET fld_sendtoserver='"+response.getProperty("br_id").toString()+"' " +
			 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedsrid+"' and fld_bathroomname='"+c1.getString(c1.getColumnIndex("fld_bathroomname"))+"'");
		             	    	     }
		             	    		 else
		             	    		 {
		             	    			db.hi_db.execSQL("UPDATE "+db.CreateBathroom+ " SET fld_sendtoserver='"+response.getProperty("br_id").toString()+"' " +
		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_bathroomname='"+c1.getString(c1.getColumnIndex("fld_bathroomname"))+"'");
		             	    		 }
				          		  }
		          				  catch (Exception e) 
		          				  {
		          					// TODO: handle exception
		          				   	System.out.println("for loop catch="+e.getMessage());
		          				   	handler_msg=0;
		          				  }
		                    	handler_msg = 0;	  
		                 }
		            	 else
		            	 {
		            			 error_msg = response.getProperty("StatusMessage").toString();
		            			 handler_msg=1;		            			
		            	 }
		              }
              
              catch(Exception e)
              {
              	System.out.println("sadsf"+e.getMessage());
                   e.printStackTrace();
              }
				 } while (c1.moveToNext());
	          }
	          else
	          {
	        	  handler_msg=0;
	          }
	  
	}
	
	
	public void Submit_Hotwatersystem() throws IOException, XmlPullParserException 
	{
		SoapObject request=null;
		db.CreateTable(39);
		db.CreateTable(43);
		Cursor c1=null;
		if(getcurrentview.equals("start"))
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'", null);	
			 	System.out.println("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'"+c1.getCount());
		}
		else
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+gettempname+"'", null);
		}
		System.out.println("coun=t"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 if(getcurrentview.equals("start"))
					 {
						   request = new SoapObject(wb.NAMESPACE,"Load_Hotwatersystem");
					 }
					 else
					 {
						   request = new SoapObject(wb.NAMESPACE,"Create_Hotwatersystem");
					 }
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    
	      		    request.addProperty("hws_id",c1.getString(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_templatename",gettempname);
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    if(getcurrentview.equals("start"))
	      		    {
	      		    	request.addProperty("fld_srid",getselectedsrid);
	      		    }
	      		  
	      		    request.addProperty("fld_characteristics",db.decode(c1.getString(c1.getColumnIndex("fld_characteristics"))));	      		    
	      		    request.addProperty("fld_radiators",db.decode(c1.getString(c1.getColumnIndex("fld_radiators"))));
	      		    request.addProperty("fld_gaslog",db.decode(c1.getString(c1.getColumnIndex("fld_gaslog"))));
	      		    request.addProperty("fld_gasloglocation",db.decode(c1.getString(c1.getColumnIndex("fld_gasloglocation"))));
	      		    
	      		    if(db.decode(c1.getString(c1.getColumnIndex("fld_noofwoodstoves"))).equals("--Select--"))
	      		    {
	      		    	request.addProperty("fld_noofwoodstoves","");	
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_noofwoodstoves",db.decode(c1.getString(c1.getColumnIndex("fld_noofwoodstoves"))));	
	      		    }
	      		    
	      		    request.addProperty("fld_fluetype",db.decode(c1.getString(c1.getColumnIndex("fld_fluetype"))));
	      		   
	      		    request.addProperty("fld_hotwatersysNA",c1.getInt(c1.getColumnIndex("fld_hotwatersysNA")));
	      		    request.addProperty("fld_gaslogNA",c1.getInt(c1.getColumnIndex("fld_gaslogNA")));
	      		    request.addProperty("fld_woodstoveNA",c1.getInt(c1.getColumnIndex("fld_woodstoveNA")));
	      		    
	      		  
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println(getcurrentview+"Hotwatersystem request="+request);
	         			         		
	         		         		
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			System.out.println("gggg"+getcurrentview);
	         			 if(getcurrentview.equals("start"))
	 	      		     {
	         				 System.out.println("Cama");
	         				 androidHttpTransport1.call(wb.NAMESPACE+"Load_Hotwatersystem", envelope1);System.out.println("ppp");
						 }
	         			 else
	         			 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"Create_Hotwatersystem", envelope1);
	         			 }System.out.println();
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println(getcurrentview+"Hotwatersystem "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	  
	                    	  if(getcurrentview.equals("start"))
             	    		 {
             	    			 System.out.println("UPDATE "+db.LoadHotwatersystem+ " SET fld_sendtoserver='"+response.getProperty("hws_id").toString()+"' " +
		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedsrid+"'");
             	    			 db.hi_db.execSQL("UPDATE "+db.LoadHotwatersystem+ " SET fld_sendtoserver='"+response.getProperty("hws_id").toString()+"' " +
	 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedsrid+"'");
             	    	     }
             	    		 else
             	    		 {
             	    			db.hi_db.execSQL("UPDATE "+db.CreateHotwatersystem+ " SET fld_sendtoserver='"+response.getProperty("hws_id").toString()+"' " +
 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
             	    			 
             	    		 }       
	                    	   	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
   }
	
	public void Submit_NotApplicable() throws IOException, XmlPullParserException 
	{
		SoapObject request=null;
		db.CreateTable(51);
		db.CreateTable(52);
		Cursor c1=null;
		if(getcurrentview.equals("start"))
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'", null);	
		}
		else
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+gettempname+"'", null);
		}
		System.out.println("coun=t"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 if(getcurrentview.equals("start"))
					 {
						   request = new SoapObject(wb.NAMESPACE,"LoadHVACNotApplicable");
					 }
					 else
					 {
						   request = new SoapObject(wb.NAMESPACE,"CreateHVACNotApplicable");
					 }
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    
	      		    request.addProperty("hvac_id",c1.getString(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_templatename",gettempname);
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    if(getcurrentview.equals("start"))
	      		    {
	      		    	request.addProperty("fld_srid",getselectedsrid);
	      		    } 
	      		    request.addProperty("fld_HVACNA",c1.getInt(c1.getColumnIndex("fld_HVACNA")));	      		    
	      		    request.addProperty("fld_SysbrandsNA",c1.getInt(c1.getColumnIndex("fld_SysbrandsNA")));
	      		    request.addProperty("fld_thermoNA",c1.getInt(c1.getColumnIndex("fld_thermoNA")));
	      		    request.addProperty("fld_ductworkNA",c1.getInt(c1.getColumnIndex("fld_ductworkNA")));
	      		  
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println(getcurrentview+"HVACNotApplicable request="+request);
	         			         		
	         		         		
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			
	         			 if(getcurrentview.equals("start"))
	 	      		     {
	         				 System.out.println("Cama");
	         				 androidHttpTransport1.call(wb.NAMESPACE+"LoadHVACNotApplicable", envelope1);System.out.println("ppp");
						 }
	         			 else
	         			 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"CreateHVACNotApplicable", envelope1);
	         			 }System.out.println();
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println(getcurrentview+"HVACNotApplicable "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	  
	                    	  if(getcurrentview.equals("start"))
             	    		 {
             	    			 System.out.println();
             	    			 db.hi_db.execSQL("UPDATE "+db.HVACNotApplicable+ " SET fld_sendtoserver='"+response.getProperty("hvac_id").toString()+"' " +
	 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedsrid+"'");
             	    	     }
             	    		 else
             	    		 {
             	    			db.hi_db.execSQL("UPDATE "+db.CreateHVACNotApplicable+ " SET fld_sendtoserver='"+response.getProperty("hvac_id").toString()+"' " +
 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
             	    			 
             	    		 }       
	                    	   	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
   }

	public void Submit_Thermostat() throws IOException, XmlPullParserException 
	{
		db.CreateTable(49);
		db.CreateTable(50);
		Cursor c1=null;
		SoapObject  request=null;
		if(getcurrentview.equals("start"))
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.LoadThermostat + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'", null);	
		}
		else
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.CreateThermostat+ " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+gettempname+"'", null);
		}
		System.out.println("coun=t"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 if(getcurrentview.equals("start"))
					 {
						 request = new SoapObject(wb.NAMESPACE,"Load_Thermostat");
					 }
					 else
					 {
						 request = new SoapObject(wb.NAMESPACE,"Create_Thermostat");
					 }
					
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("z_id",c1.getString(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_templatename",gettempname);
	      		     if(getcurrentview.equals("start"))
					 {
	      		    	 request.addProperty("fld_srid",getselectedsrid);
					 }
	      		     
	      		     
				   request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_zoneno",db.decode(c1.getString(c1.getColumnIndex("fld_zoneno"))));	      		    
	      		    request.addProperty("fld_thermotype",db.decode(c1.getString(c1.getColumnIndex("fld_thermotype"))));
	      		    request.addProperty("fld_thermolocation",db.decode(c1.getString(c1.getColumnIndex("fld_thermolocation"))));
	      		    request.addProperty("fld_thermostatcomments",db.decode(c1.getString(c1.getColumnIndex("fld_thermostatcomments"))));
	      		    
	      		   
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println(getcurrentview+"Thermo request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 if(getcurrentview.equals("start"))
	         			 {
	         				 androidHttpTransport1.call(wb.NAMESPACE+"Load_Thermostat", envelope1);
	         			 }
	         			 else
	         			 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"Create_Thermostat", envelope1);
	         			 }
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println(getcurrentview+"Thermostat "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	   
	                    	    	 try
		 	          				 {
	                    	    		 if(getcurrentview.equals("start"))
	                    	    		 {
	                    	    			 db.hi_db.execSQL("UPDATE "+db.LoadThermostat+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and " +
	     		 	          											"fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'  and fld_srid='"+getselectedsrid+"'");
	                    	    	     }
	                    	    		 else
	                    	    		 {
	                    	    				db.hi_db.execSQL("UPDATE "+db.CreateThermostat+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and " +
	     		 	          											"fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'");
	                    	    		 }                 	    		
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          					 System.out.println("sadsf catch="+e.getMessage());
		 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
   }
	public void Submit_Systembrands() throws IOException, XmlPullParserException 
	{
		db.CreateTable(47);
		db.CreateTable(48);
		Cursor c1=null;
		SoapObject  request=null;
		if(getcurrentview.equals("start"))
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'", null);	
		}
		else
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.CreateSystembrands+ " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+gettempname+"'", null);
		}
		System.out.println("coun=t"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 if(getcurrentview.equals("start"))
					 {
						 request = new SoapObject(wb.NAMESPACE,"Load_Systembrands");
					 }
					 else
					 {
						 request = new SoapObject(wb.NAMESPACE,"Create_Systembrands");
					 }
					
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("z_id",c1.getString(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_templatename",gettempname);
	      		     if(getcurrentview.equals("start"))
					 {
	      		    	 request.addProperty("fld_srid",getselectedsrid);
					 }
	      		     
	      		     
				   request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_zoneno",db.decode(c1.getString(c1.getColumnIndex("fld_zoneno"))));	      		    
	      		    request.addProperty("fld_zonename",db.decode(c1.getString(c1.getColumnIndex("fld_zonename"))));
	      		    request.addProperty("fld_zonenameother",db.decode(c1.getString(c1.getColumnIndex("fld_zonenameother"))));

	      		    request.addProperty("fld_noofzones",db.decode(c1.getString(c1.getColumnIndex("fld_noofzones"))));
	      		    request.addProperty("fld_externalunit",db.decode(c1.getString(c1.getColumnIndex("fld_externalunit"))));
	      		    request.addProperty("fld_internalunit",db.decode(c1.getString(c1.getColumnIndex("fld_internalunit"))));
	      		    request.addProperty("fld_extunitreplprob",db.decode(c1.getString(c1.getColumnIndex("fld_extunitreplprob"))));
	      		    request.addProperty("fld_extunitreplprobother",db.decode(c1.getString(c1.getColumnIndex("fld_extunitreplprobother"))));
	      		    request.addProperty("fld_intunitreplprob",db.decode(c1.getString(c1.getColumnIndex("fld_intunitreplprob"))));	      		    
	      		    request.addProperty("fld_intunitreplprobother",db.decode(c1.getString(c1.getColumnIndex("fld_intunitreplprobother"))));
	      		    request.addProperty("fld_ampextunit",db.decode(c1.getString(c1.getColumnIndex("fld_ampextunit"))));
	      		    request.addProperty("fld_ampintunit",db.decode(c1.getString(c1.getColumnIndex("fld_ampintunit"))));
	      		    request.addProperty("fld_emerheatampdraw",db.decode(c1.getString(c1.getColumnIndex("fld_emerheatampdraw"))));
	      		    request.addProperty("fld_supplytemp",db.decode(c1.getString(c1.getColumnIndex("fld_supplytemp"))));
	      		    request.addProperty("fld_returntemp",db.decode(c1.getString(c1.getColumnIndex("fld_returntemp"))));
	      		    request.addProperty("fld_systembrandcomments",db.decode(c1.getString(c1.getColumnIndex("fld_systembrandcomments"))));
	      		    
	      		   
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println(getcurrentview+"Systbrands request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 if(getcurrentview.equals("start"))
	         			 {
	         				 androidHttpTransport1.call(wb.NAMESPACE+"Load_Systembrands", envelope1);
	         			 }
	         			 else
	         			 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"Create_Systembrands", envelope1);
	         			 }
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println(getcurrentview+"Systembrands "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	   
	                    	    	 try
		 	          				 {
	                    	    		 if(getcurrentview.equals("start"))
	                    	    		 {
	                    	    			 System.out.println("UPDATE "+db.LoadSystembrands+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and  " +
	     		 	          											"fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'  and fld_srid='"+getselectedsrid+"'");
	                    	    			 db.hi_db.execSQL("UPDATE "+db.LoadSystembrands+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and " +
	     		 	          											"fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'  and fld_srid='"+getselectedsrid+"'");
	                    	    	     }
	                    	    		 else
	                    	    		 {
	                    	    				db.hi_db.execSQL("UPDATE "+db.CreateSystembrands+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and " +
	     		 	          											"fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'");
	                    	    		 }                 	    		
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          					 System.out.println("sadsf catch="+e.getMessage());
		 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
   }
	public void Submit_Ductwork() throws IOException, XmlPullParserException 
	{
		db.CreateTable(45);
		db.CreateTable(46);
		Cursor c1=null;
		SoapObject  request=null;
		if(getcurrentview.equals("start"))
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'", null);	
		}
		else
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.CreateDuctwork + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+gettempname+"'", null);
		}
		System.out.println("coun=t"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 if(getcurrentview.equals("start"))
					 {
						 request = new SoapObject(wb.NAMESPACE,"Load_Ductwork");
					 }
					 else
					 {
						 request = new SoapObject(wb.NAMESPACE,"Create_Ductwork");
					 }
					
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("z_id",c1.getString(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_templatename",gettempname);
	      		     if(getcurrentview.equals("start"))
					 {
	      		    	 request.addProperty("fld_srid",getselectedsrid);
					 }
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_zoneno",db.decode(c1.getString(c1.getColumnIndex("fld_zoneno"))));	      		    
	      		    request.addProperty("fld_supplyregister",db.decode(c1.getString(c1.getColumnIndex("fld_supplyregister"))));
	      		    request.addProperty("fld_retunrregister",db.decode(c1.getString(c1.getColumnIndex("fld_retunrregister"))));
	      		    request.addProperty("fld_ducworktype",db.decode(c1.getString(c1.getColumnIndex("fld_ducworktype"))));
	      		    request.addProperty("fld_multizonesystem",db.decode(c1.getString(c1.getColumnIndex("fld_multizonesystem"))));
	      		    request.addProperty("fld_ductworkcomment",db.decode(c1.getString(c1.getColumnIndex("fld_ductworkcomment"))));
	      		   
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println(getcurrentview+"Duct request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 if(getcurrentview.equals("start"))
	         			 {
	         				 androidHttpTransport1.call(wb.NAMESPACE+"Load_Ductwork", envelope1);
	         			 }
	         			 else
	         			 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"Create_Ductwork", envelope1);
	         			 }
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println(getcurrentview+"Duct "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	   
	                    	    	 try
		 	          				 {
	                    	    		
	                    	    		 
	                    	    		 if(getcurrentview.equals("start"))
	                    	    		 {
	                    	    			 System.out.println("UPDATE "+db.LoadDuctwork+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'  and fld_srid='"+getselectedsrid+"'");
	                    	    			 db.hi_db.execSQL("UPDATE "+db.LoadDuctwork+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'  and fld_srid='"+getselectedsrid+"'");
	                    	    	     }
	                    	    		 else
	                    	    		 {
	                    	    			 System.out.println("UPDATE "+db.CreateDuctwork+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and " +
	     		 	          											"fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'");
	                    	    				db.hi_db.execSQL("UPDATE "+db.CreateDuctwork+ " SET fld_sendtoserver='"+response.getProperty("z_id").toString()+"' " +
	     		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and " +
	     		 	          											"fld_zoneno='"+c1.getString(c1.getColumnIndex("fld_zoneno"))+"'");
	                    	    		 }                 	    		
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          					 System.out.println("sadsf catch="+e.getMessage());
		 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
   }
	public void Submit_HVAC() throws IOException, XmlPullParserException 
	{
		db.CreateTable(38);db.CreateTable(42);
		Cursor c1=null;
		SoapObject request=null;
		System.out.println("gettempname"+gettempname);
		if(getcurrentview.equals("start"))
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.LoadHVAC + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'", null);
			 	System.out.println("select * from " + db.LoadHVAC + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_srid='"+getselectedsrid+"'");
		}
		else
		{
			 	c1 = db.hi_db.rawQuery("select * from " + db.CreateHVAC + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+gettempname+"'", null);
			 	System.out.println("select * from " + db.CreateHVAC + " WHERE fld_inspectorid='"+db.UserId+"' " +
					"and fld_templatename='"+gettempname+"'");
		}
		System.out.println("coun=t"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {		
					 if(getcurrentview.equals("start"))
					 {
						 request = new SoapObject(wb.NAMESPACE,"Load_HVAC");
					 }
					 else
					 {
						 request = new SoapObject(wb.NAMESPACE,"Create_HVAC");
					 }
					 					 
					
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("h_id",c1.getString(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_templatename",gettempname);
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		     if(getcurrentview.equals("start"))
					 {
	      		    	request.addProperty("fld_srid",getselectedsrid);
					 }
	      		    request.addProperty("fld_hvactype",db.decode(c1.getString(c1.getColumnIndex("fld_hvactype"))));
	      		    request.addProperty("fld_hvactypeother",db.decode(c1.getString(c1.getColumnIndex("fld_hvactypeother"))));
	      		    request.addProperty("fld_energysource",db.decode(c1.getString(c1.getColumnIndex("fld_energysource"))));	      		    
	      		    request.addProperty("fld_energysourceother",db.decode(c1.getString(c1.getColumnIndex("fld_energysourceother"))));
	      		    if(db.decode(c1.getString(c1.getColumnIndex("fld_gasshutoff"))).equals("--Select--"))
	      		    {
	      		    	request.addProperty("fld_gasshutoff","");
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_gasshutoff",db.decode(c1.getString(c1.getColumnIndex("fld_gasshutoff"))));
	      		    }
	      		    request.addProperty("fld_gasshutoffother",db.decode(c1.getString(c1.getColumnIndex("fld_gasshutoffother"))));
	      		    
	      		    if(db.decode(c1.getString(c1.getColumnIndex("fld_fueltankloc"))).contains(","))
	      		    {
	      		    	String str = db.decode(c1.getString(c1.getColumnIndex("fld_fueltankloc"))).replace(",",", ");
	      		    	request.addProperty("fld_fueltankloc",str);
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_fueltankloc",db.decode(c1.getString(c1.getColumnIndex("fld_fueltankloc"))));	
	      		    }
	      		    
	      		    if(db.decode(c1.getString(c1.getColumnIndex("fld_fluetype"))).contains(","))
	      		    {
	      		    	String str = db.decode(c1.getString(c1.getColumnIndex("fld_fluetype"))).replace(",",", ");
	      		    	request.addProperty("fld_fluetype",str);
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_fluetype",db.decode(c1.getString(c1.getColumnIndex("fld_fluetype"))));	
	      		    }
	      		    
	      		    request.addProperty("fld_age",db.decode(c1.getString(c1.getColumnIndex("fld_age"))));
	      		    request.addProperty("fld_otherage",db.decode(c1.getString(c1.getColumnIndex("fld_otherage"))));
	      		    
	      		    request.addProperty("fld_heatsystembrand",db.decode(c1.getString(c1.getColumnIndex("fld_heatsystembrand"))));
	      		    request.addProperty("fld_furnacetype",db.decode(c1.getString(c1.getColumnIndex("fld_furnacetype"))));
	      		    request.addProperty("fld_heatdeliverytype",db.decode(c1.getString(c1.getColumnIndex("fld_heatdeliverytype"))));
	      		    request.addProperty("fld_blowermotor",db.decode(c1.getString(c1.getColumnIndex("fld_blowermotor"))));
	      		    
	      		    if(db.decode(c1.getString(c1.getColumnIndex("fld_testedfor"))).contains(","))
	      		    {
	      		    	String str = db.decode(c1.getString(c1.getColumnIndex("fld_testedfor"))).replace(",",", ");
	      		    	request.addProperty("fld_testedfor",str);
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_testedfor",db.decode(c1.getString(c1.getColumnIndex("fld_testedfor"))));	
	      		    }
	      		    
	      		    
	      		    if(db.decode(c1.getString(c1.getColumnIndex("fld_airhandlerfeatures"))).contains(","))
	      		    {
	      		    	String str = db.decode(c1.getString(c1.getColumnIndex("fld_airhandlerfeatures"))).replace(",",", ");
	      		    	request.addProperty("fld_airhandlerfeatures",str);
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_airhandlerfeatures",db.decode(c1.getString(c1.getColumnIndex("fld_airhandlerfeatures"))));	
	      		    }
	      		    
	      		    
	      		    request.addProperty("fld_tonnage",db.decode(c1.getString(c1.getColumnIndex("fld_tonnage"))));
	      		    request.addProperty("fld_tonnageother",db.decode(c1.getString(c1.getColumnIndex("fld_tonnageother"))));
	      		    request.addProperty("fld_btu",db.decode(c1.getString(c1.getColumnIndex("fld_btu"))));
	      		    request.addProperty("fld_btuother",db.decode(c1.getString(c1.getColumnIndex("fld_btuother"))));
	      		    

    		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("request=="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 if(getcurrentview.equals("start"))
						 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"Load_HVAC", envelope1);						 
						 }
						 else
						 {
							 androidHttpTransport1.call(wb.NAMESPACE+"Create_HVAC", envelope1);
							 
						 }
	         			
	         			
	         			 
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println(getcurrentview+"_HVAC "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    		if(getcurrentview.equals("start"))
	                    		{
	                    			System.out.println("jjj="+response.getProperty("h_id").toString());
	                    			System.out.println("TT1212="+c1.getString(c1.getColumnIndex("fld_hvactype")));
	                    			System.out.println("T12"+getselectedsrid);
	                    			
	                    	  System.out.println("UPDATE "+db.LoadHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' " +
	                    	  		"WHERE fld_inspectorid='"+db.UserId+"' and  fld_srid='"+getselectedsrid+"'  and fld_hvactype='"+c1.getString(c1.getColumnIndex("fld_hvactype"))+"'");
	          					db.hi_db.execSQL("UPDATE "+db.LoadHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and  fld_srid='"+getselectedsrid+"' and fld_hvactype='"+c1.getString(c1.getColumnIndex("fld_hvactype"))+"'");
	                    		}
	                    		else
	                    		{
	                    			db.hi_db.execSQL("UPDATE "+db.CreateHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+getmodulename+"' and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and fld_srid='"+getselectedsrid+"'");
	                    		}
	          				
	                    	    	/* try
		 	          				 {
	                    	    		db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_sendtoserver='"+response.getProperty("ssv_Id").toString()+"' " +
		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' " +
		 	          									"and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and " +
		 	          											"fld_primary='"+c1.getString(c1.getColumnIndex("fld_primary"))+"' and fld_srid='"+getph+"'");
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          					 System.out.println("sadsf catch="+e.getMessage());
		 	          				 }*/
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
   }
	public String getselected_chk(CheckBox[] CB) {
		// TODO Auto-generated method stub
		
		String s = "";
		for(int i=0;i<CB.length;i++)
		{
			if(CB[i].isChecked() && !CB[i].getText().toString().equals("Other"))
			{
				 s+=CB[i].getText().toString()+"&#94;";
			}
		} 
		if(s.length()>=2)
		{
			s=s.substring(0, s.length()-5);
		}
		return s;
	}
	public void setvaluechk1(String typechk_val, CheckBox[] typechk,
			EditText editText) {
		// TODO Auto-generated method stub
		if(!typechk_val.equals(""))
		{
			if(typechk_val.contains("&#40;"))
			{
				typechk_val=typechk_val.replace("&#40;", "&#94;");
			}
			else
			{
				typechk_val = typechk_val;
			}
			
			String k=typechk_val.replace("&#94;", "&#126;");
			String temp[]=k.split("&#126;");
			
			int j=0;
			do{
				for(int i=0;i<typechk.length;i++)
				{
					if(temp[j].trim().equals(typechk[i].getText().toString().trim()))
					{
						typechk[i].setChecked(true);
						j++;
						if(j==temp.length)
						{
							return;
						}
					}
					
				}
				if(j!=temp.length)
				{
					
					typechk[typechk.length-1].setChecked(true); // no matches found for the last value in the split text so we set the other to tru  
					editText.setVisibility(View.VISIBLE);
					editText.setText(temp[j]);
					j++;	
				}
			}while(j<temp.length);
		}
	}
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(context).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public void success_alert() {
		// TODO Auto-generated method stub
		String mess="You've successfully submitted";
       if(getcurrentview.equals("start"))
       {
    	   mess = "You've successfully submitted your Inspection";
       }
       else
       {
    	   mess = "You've successfully submitted your Template";
       }
       
		AlertDialog al = new AlertDialog.Builder(context).setMessage(mess).setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				
				  if(getcurrentview.equals("start"))
				  {
					  
					  try
	    				 {
						  System.out.println("UPDATE "+db.OrderInspection+ " SET fld_status='2' WHERE fld_orderedid='"+getselectedsrid+"'");
						  
	    					db.hi_db.execSQL("UPDATE "+db.OrderInspection+ " SET fld_status='2' WHERE fld_orderedid='"+getselectedsrid+"'");
	    				 }
	    				 catch (Exception e) {
	    					// TODO: handle exception
	    				 }
					  
					  Intent in = new Intent(context,CompletedInspection.class);
					  Bundle b5 = new Bundle();
					  b5.putString("inspectorid", db.UserId);			
					  in.putExtras(b5);
					  context.startActivity(in);
					((Activity) context).finish();  
				  }
				  else
				  {
					  ((Activity) context).startActivity(new Intent(context,HomeScreen.class));
				  }
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	public void DefaultValue_Insert(String gettempname,String getselectedho) {
		
	
	Cursor selcur1,selcur2;
			
			
			try
			{
				selcur1 = db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' ", null);
	            int cnt = selcur1.getCount();
	            System.out.println("hotwarecnt="+cnt);
	            if(cnt>0)
	           
	            	
					if (selcur1 != null) {
						selcur1.moveToFirst();
						db.hi_db.execSQL("Delete from "+db.LoadHotwatersystem+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
						do {
							System.out.println("hotwcount");		
							Cursor cur1 = db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
							System.out.println("hotwcouncurr11="+cur1.getCount());
	    		            if(cur1.getCount()==0)
	    		            {
    							try
    			            	{
    							System.out.println("tttt");
    								System.out.println("INSERT INTO "+db.LoadHotwatersystem+" (fld_templatename,fld_srid,fld_inspectorid,fld_characteristics,fld_radiators,fld_gaslog,fld_gasloglocation,fld_noofwoodstoves,fld_fluetype) VALUES" +
    										"('"+db.encode(gettempname)+"','"+getselectedho+"','"+db.UserId+"','"+selcur1.getString(4)+"','"+selcur1.getString(5)+"','"+selcur1.getString(6)+"','"+selcur1.getString(7)+"','"+selcur1.getString(8)+"','"+selcur1.getString(9)+"')");
    								
    								db.hi_db.execSQL("INSERT INTO "+db.LoadHotwatersystem+" (fld_templatename,fld_srid,fld_inspectorid,fld_characteristics,fld_radiators,fld_gaslog,fld_gasloglocation,fld_noofwoodstoves,fld_fluetype) VALUES" +
    										"('"+db.encode(gettempname)+"','"+getselectedho+"','"+db.UserId+"','"+selcur1.getString(4)+"','"+selcur1.getString(5)+"','"+selcur1.getString(6)+"','"+selcur1.getString(7)+"','"+selcur1.getString(8)+"','"+selcur1.getString(9)+"')");
    								System.out.println("hotwarer completed");
    			            	}
    			            	catch (Exception e) {
    								// TODO: handle exception
    			            		System.out.println("error catch22="+e.getMessage());
    							}
	    		            }
	    		           
						
						}while (selcur1.moveToNext());
					}
	            
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
			
			
			
		
		
	}
	
}
