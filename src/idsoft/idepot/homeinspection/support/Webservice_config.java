package idsoft.idepot.homeinspection.support;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Base64;

public class Webservice_config {
Context con;
//public static final String URL = "http://custominspection.paperless-inspectors.com:83/Homeinspection.asmx"; //UAT
public static final String URL="http://hi.paperlessinspectors.com/Homeinspection.asmx";  // Live ARR
//public static final String URL = "http://72.15.221.151:83/Homeinspection.asmx"; //UAT
public String NAMESPACE = "http://tempuri.org/";
public SoapSerializationEnvelope envelope;
private Bitmap bm;
DatabaseFunction db;
	public Webservice_config(Context con)
	{
		this.con=con;
		db= new DatabaseFunction(con);
	}	
	public SoapObject export_header(String string)
	{
		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		SoapObject add_property = new SoapObject(NAMESPACE,string);
		return add_property;
	}
	public String export_footer(SoapSerializationEnvelope envelope22,

			SoapObject add_property, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException {
		// TODO Auto-generated method stub
		MarshalBase64 marshal = new MarshalBase64();
		marshal.register(envelope22);
		envelope22.setOutputSoapObject(add_property);
		HttpTransportSE androidHttpTransport11 = new HttpTransportSE(URL);
		try {
			androidHttpTransport11.call(NAMESPACE+string, envelope22);
			String result = String.valueOf(envelope22.getResponse());
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "false";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "false";
		}
		
		
	}
	
}
