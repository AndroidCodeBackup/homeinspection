package idsoft.idepot.homeinspection.support;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseFunction {
	public String MY_DATABASE_NAME = "Home_Databases.db";
	public static final String Registration = "Registration";
	public static final String OrderInspection = "Order_Inspection";
	
	public static final String CreateTemplate = "Create_Template";
	public static final String AddOther = "Add_Other";
	public static final String AddOtherIP = "Add_Other_IP";
	public static final String AddOtherCont = "Add_Other_Contype";
	public static final String SaveAddOtherIP = "Save_Add_Other_IP";
	public static final String CreateDefault = "Create_Default";
	public static final String AddVC = "Add_VC";
	public static final String SetVC = "Set_VC";
	public static final String SaveSubModule = "Save_SubModule";
	public static final String SaveAddOther = "Save_AddOther";
	public static final String SaveVC = "Save_VC";
	public static final String CreateEditVC = "Create_EditVC";
	public static final String SaveEditVC = "Save_EditVC";
	public static final String SaveSetVC = "Save_SetVC";
	public static final String SaveVCImage = "Save_VCImage";
	public static final String CreateHeating = "Create_Heating";
	public static final String CreateBathroom = "Create_Bathroom";
	public static final String LoadBathroom = "Load_Bathroom";
	public static final String LoadHeating = "Load_Heating";
	public static final String LoadCooling = "Load_Cooling";
	public static final String LoadZones = "Load_Zones";
	public static final String CreateCooling = "Create_Cooling";
	public static final String CreateZones = "Create_Zones";	
	public static final String SaveSubOptionPools = "Save_SubPools";
	public static final String CreateSubOptionPools = "Create_SubPools";
	public static final String ADDNewCondition = "AddNew_VC";
	public static final String Downloaded_document="Downloaded_document";
	public static final String 	HomeWarranty="	Home_Warranty";
	public static final String FeedBackDocument="FeedBack_Document";
	public static final String FeedBackInfo="FeedBack_Info";
	public static final String CoverPhoto = "Cover_Photo";
	public static final String CSE = "CustomerEvaluationSurvey";
	public static final String PIA = "PreinspectionAgreement";
	
	public static final String CreateHVAC = "Create_HVAC";
	public static final String LoadHVAC = "Load_HVAC";
	
	public static final String CreateDuctwork = "Create_Ductwork";
	public static final String LoadDuctwork = "Load_Ductwork";
	
	public static final String CreateSystembrands = "Create_Systembrands";
	public static final String LoadSystembrands = "Load_Systembrands";
	
	public static final String CreateThermostat = "Create_Thermostat";
	public static final String LoadThermostat = "Load_Thermostat";
	
	public static final String CreateHotwatersystem = "Create_Hotwatersystem";
	public static final String LoadHotwatersystem = "Load_Hotwatersystem";
	
	public static final String CreateDuctSysThermo = "Create_DuctSysThermo";
	public static final String LoadDuctSysThermo = "Load_DuctSysThermo";
	
	public static final String Invoice = "tbl_Invoice";
	public static final String HVACNotApplicable = "tbl_HVACNotApplicable";
	public static final String CreateHVACNotApplicable = "tbl_CreateHVACNotApplicable";
	
	
	public static SQLiteDatabase hi_db;
    Context context;
    String inspectorid;
    public String UserId="",UserName="",Userfirstname="",Userlastname="",UserEmail="",UserPhone="",Companyname="";
    
	public DatabaseFunction(Context con) {
		// TODO Auto-generated constructor stub
		hi_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		context = con;
	}

	public void CreateTable(int i) {
		// TODO Auto-generated method stub
		switch(i)
		{
			case 1:
				/*Registration LOGIN*/
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Registration
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_userid varchar(100),fld_firstname varchar(100),fld_lastname varchar(100)," +
						"fld_middlename varchar(100),fld_address1 varchar(100),fld_address2 varchar(100),fld_zipcode varchar(100),fld_city varchar(100)," +
						"fld_state varchar(100),fld_county varchar(100),fld_contactphone varchar(100),fld_email varchar(100)," +
						"fld_username varchar(100),fld_password varchar(100),fld_licensenumber varchar(100),fld_licenseexpirydate varchar(100)," +
						"fld_licensetype varchar(50),fld_otherlicensetype varchar(50),fld_companyname varchar(100),fld_companylogo varchar(100)," +
						"fld_headshot VARCHAR(100),fld_signature VARCHAR(100),fld_initials VARCHAR(100)," +
						"fld_companyphone varchar(100),fld_companyemail varchar(50),fld_uploadlicense varchar(1000),fld_logout integer default '0',fld_compfax varchar2,fld_compwebsite varchar2);");
				} catch (Exception e){}
				
			break;
			
			case 2:
				/* CREATING NEW TEMPLATE */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateTemplate
							+ " (at_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_tempoption varchar(100),fld_modulename varchar(100),fld_templatename varchar(100),fld_date varchar(100),fld_time varchar(100));");
				} catch (Exception e) {
					System.out.println("Error in creating new template table"+e.getMessage());
				}
			break;
			
			case 3:
				/* CREATING OTHER TEXT FOR SUBSECTION OPTION */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ AddOther
							+ " (ao_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200),fld_submodule varchar(200),fld_other varchar(200),fld_templatename varchar(200));");
				} catch (Exception e) {
					System.out.println("Error in creating creating other text for subsection table"+e.getMessage());
				}
			break;
			
			case 4:
				/* CREATING TABLE FOR FOR SAVING SUBSECTION OPTION */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateDefault
							+ " (cd_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200),fld_submodule varchar(200),fld_templatename varchar(200),fld_NA integer,fld_description varchar(1000),fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in creating creating other text for subsection table"+e.getMessage());
				}
			break;
			
			case 5:
				/* CREATING TABLE FOR FOR ADDING VC */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ AddVC
							+ " (ad_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200),fld_submodule varchar(200),fld_primary varchar(1000),fld_secondary varchar(1000),fld_comments varchar(1000),fld_condition varchar(1000),fld_other varchar(1000),fld_checkbox varchar(1000),fld_inspectionpoint varchar(1000),fld_apprrepaircost varchar(250),fld_contractortype varchar(1000));");
				} catch (Exception e) {
					System.out.println("Error in adding VC table table"+e.getMessage());
				}
			break;
			
			case 6:
				/* CREATING TABLE FOR FOR SETTING VC */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SetVC
							+ " (sv_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200)," +
							"fld_vcname varchar(500),fld_submodule varchar(200),fld_templatename varchar(1000),fld_primary varchar(1000),fld_secondary varchar(1000),fld_comments varchar(1000),fld_condition varchar(1000),fld_other varchar(1000),fld_checkbox varchar(1000),fld_inspectionpoint varchar(1000),fld_apprrepaircost varchar(250),fld_contractortype varchar(1000),fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in setting VC table table"+e.getMessage());
				}
			break;
			
			case 7:
				/* CREATING TABLE FOR SAVING SUBMODULES OPTION  */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SaveSubModule
							+ " (ssm_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50) NOT NULL,fld_module varchar(200),fld_submodule varchar(200),fld_templatename varchar(200),fld_NA integer,fld_description varchar(1000),fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in creating creating other text for subsection table"+e.getMessage());
				}
			break;
			
			case 8:
				/* CREATING OTHER TEXT FOR SAVING SUBSECTION OPTION */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SaveAddOther
							+ " (sao_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50),fld_module varchar(200),fld_submodule varchar(200),fld_other varchar(200),fld_templatename varchar(200));");
				} catch (Exception e) {
					System.out.println("Error in creating creating other text for subsection table"+e.getMessage());
				}
			break;
			
			case 9:
				/* CREATING TABLE FOR FOR SAVING VC */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SaveVC
							+ " (svc_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50),fld_module varchar(200),fld_submodule varchar(200),fld_templatename varchar(1000),fld_primary varchar(1000),fld_secondary varchar(1000),fld_comments varchar(1000),fld_condition varchar(1000),fld_other varchar(1000),fld_checkbox varchar(1000),fld_inspectionpoint varchar(1000));");
				} catch (Exception e) {
					System.out.println("Error in Saving VC table "+e.getMessage());
				}
			break;
			
			case 10:
				/* SAVING & EDITING VC */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateEditVC
							+ " (ced_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200),fld_submodule varchar(200),fld_VCName varchar(200),fld_templatename varchar(200),fld_selected integer,fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in creating saving VC table"+e.getMessage());
				}
			break;			
			case 11:
				/* SAVING & EDITING VC FOR PH */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SaveEditVC
							+ " (sed_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50),fld_module varchar(200),fld_submodule varchar(200),fld_VCName varchar(200),fld_templatename varchar(200),fld_selected integer,fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in creating saving VC table"+e.getMessage());
				}
			break;			
			case 12:
				/* CREATING TABLE FOR FOR SETTING VC SAVE */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SaveSetVC
							+ " (ssv_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_ed_id varchar(50) NOT NULL Default '0',fld_inspectorid varchar(50) NOT NULL," +
							"fld_srid varchar,fld_module varchar(200),fld_submodule varchar(200),fld_templatename varchar(1000),fld_vcname varchar(1000) default '',fld_primary varchar(1000)," +
							"fld_secondary varchar(1000),fld_comments varchar(1000),fld_condition varchar(1000),fld_other varchar(1000)," +
							"fld_checkbox varchar(1000),fld_inspectionpoint varchar(1000),fld_apprrepaircost varchar(250)," +
							"fld_contractortype varchar(1000),fld_printphoto varchar(10) default '2',fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in setting VC table table"+e.getMessage());
				}
			break;			
			case 13:
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SaveVCImage
							+ " (si_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_image varchar(1250) NOT NULL,fld_caption varchar(1000),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar,fld_ed_id varchar(200),fld_module varchar(200),fld_submodule varchar(200),fld_vc varchar(10000),fld_templatename varchar(1000),fld_identity integer default '0',fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in adding VC image table"+e.getMessage());
				}
				break;					
			case 15:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateBathroom
							+ " (br_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_bathroomname varchar(200),fld_otherbathroomname varchar(200),fld_tubs varchar(1000),"+
							"fld_sinks varchar(1000),fld_showerenclosures varchar(1000),fld_safetyglass varchar(1000),fld_othersafetyglass varchar(1000),fld_toilets varchar(1000),fld_bathfixturematerials varchar(1000),"+
							"fld_supplydrainage varchar(1000),fld_supplydrainageother varchar(1000),fld_bathother varchar(1000),fld_otherfeatures varchar(1000),fld_bathcomments varchar(1000),fld_sendtoserver integer default '0');");
			    }
				catch (Exception e) {
					// TODO: handle exception
				}
				break;				
			case 16:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ LoadBathroom
							+ " (br_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50),fld_bathroomname varchar(200),fld_otherbathroomname varchar(200),fld_tubs varchar(1000),"+
							"fld_sinks varchar(1000),fld_showerenclosures varchar(1000),fld_safetyglass varchar(1000),fld_othersafetyglass varchar(1000),fld_toilets varchar(1000),fld_bathfixturematerials varchar(1000),"+
							"fld_supplydrainage varchar(1000),fld_supplydrainageother varchar(1000),fld_bathother varchar(1000),fld_otherfeatures varchar(1000),fld_bathcomments varchar(1000),fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("load bathroom table="+e.getMessage());
				}
				break;
			case 17:
				/* CREATING OTHER TEXT FOR INSPECTION POINT OPTION */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ AddOtherIP
							+ " (aoip_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200),fld_submodule varchar(200),fld_other varchar(200),fld_templatename varchar(200));");
				} catch (Exception e) {
					System.out.println("Error in creating creating other text for inspection point table"+e.getMessage());
				}
			break;
			
			
			
			case 18:
				/* CREATING SAVE OTHER TEXT FOR INSPECTION POINT OPTION */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SaveAddOtherIP
							+ " (saoip_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50),fld_module varchar(200),fld_submodule varchar(200),fld_other varchar(200),fld_templatename varchar(200));");
				} catch (Exception e) {
					System.out.println("Error in creating creating save other text for inspection point table"+e.getMessage());
				}
			break;
			
				
			case 20:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateHeating
							+ " (h_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_heatintype varchar(100),fld_heatingtypeother varchar(100),fld_energysource varchar(200),fld_energysourceother varchar(100),fld_heatingno varchar(100),fld_otherheatingno varchar(100),"+
							   "fld_age varchar(100),fld_otherage varchar(100),fld_heatsystembrand varchar(100),fld_furnacetype varchar(100),fld_heatdeliverytype varchar(100),"+
							   "fld_motorblower varchar(100),fld_noofstoves varchar(100),fld_othernoofstoves varchar(100),fld_furnacefeatuers varchar(100),fld_supplyregister varchar(100),"+
							   "fld_retunrregister varchar(100),fld_radiators varchar(100),fld_watersystemtype varchar(100),fld_fueltanklocation varchar(100),fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				break;
			
			case 21:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateCooling
							+ " (c_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_Coolingequipment varchar(50)," +
							"fld_coolingenergysource varchar(100),fld_coolingenergysourceother varchar(100),fld_centralairmanuf varchar(100),fld_noofcoolingzones varchar(200)," +
							"fld_noofcoolingzonesother varchar(100),fld_age varchar(100),fld_ageother varchar(100),"+
							   "fld_condensationdischarge varchar(100),fld_aircondtesting varchar(100),fld_thermostat varchar(100),fld_gaslog varchar(100),fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				break;
				
				
			case 22:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateZones
							+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_zonename varchar(200),fld_zonenameother varchar(50) NOT NULL," +
							"fld_unitype varchar(100),fla_zone5yearreplacement varchar(50),fld_zonetestedfor varchar(100),fld_zonetonnage varchar(100)," +
							"fld_zonetonnageother varchar(200),fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("Zone tabe="+e.getMessage() );
				}
				
				
			case 23:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ LoadHeating
							+ " (h_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50),fld_heatintype varchar(100),fld_heatingtypeother varchar(100),fld_energysource varchar(200),fld_energysourceother varchar(100),fld_heatingno varchar(100),fld_otherheatingno varchar(100),"+
							   "fld_age varchar(100),fld_otherage varchar(100),fld_heatsystembrand varchar(100),fld_furnacetype varchar(100),fld_heatdeliverytype varchar(100),"+
							   "fld_motorblower varchar(100),fld_noofstoves varchar(100),fld_othernoofstoves varchar(100),fld_furnacefeatuers varchar(100),fld_supplyregister varchar(100),"+
							   "fld_retunrregister varchar(100),fld_radiators varchar(100),fld_watersystemtype varchar(100),fld_fueltanklocation varchar(100) default '',fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				break;
				
			case 24:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ LoadCooling
							+ " (c_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50),fld_Coolingequipment varchar(50)," +
							"fld_coolingenergysource varchar(100),fld_coolingenergysourceother varchar(100),fld_centralairmanuf varchar(100),fld_noofcoolingzones varchar(200)," +
							"fld_noofcoolingzonesother varchar(100),fld_age varchar(100),fld_ageother varchar(100),"+
							   "fld_condensationdischarge varchar(100),fld_aircondtesting varchar(100),fld_thermostat varchar(100),fld_gaslog varchar(100),fld_sendtoserver integer default '0');");
			}
				catch (Exception e) {
					// TODO: handle exception
				}
				break;
				
			case 25:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ LoadZones
							+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50),fld_zonename varchar(200),fld_zonenameother varchar(50) NOT NULL," +
							"fld_unitype varchar(100),fla_zone5yearreplacement varchar(50),fld_zonetestedfor varchar(100),fld_zonetonnage varchar(100)," +
							"fld_zonetonnageother varchar(200),fld_sendtoserver integer default '0');");
			}
				catch (Exception e) {
					// TODO: handle exception
				}
				break;
				
			case 26:
				/* CREATING TABLE FOR SAVING SUBMODULES OPTION  */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SaveSubOptionPools
							+ " (s_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50) NOT NULL,fld_module varchar(200)," +
							"fld_submodule varchar(500),fld_pooloption varchar(300),fld_subpooloption varchar(200),fld_subpoolother varchar(250),fld_templatename varchar(200));");
				} catch (Exception e) {
					System.out.println("Error in creating creating other text for subsection table"+e.getMessage());
				}
			break;

			case 27:
				/* CREATING TABLE FOR SAVING SUBMODULES OPTION  */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateSubOptionPools
							+ " (s_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200)," +
							"fld_submodule varchar(500),fld_pooloption varchar(300),fld_subpooloption varchar(200),fld_subpoolother varchar(250),fld_templatename varchar(200),fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in creating creating other text for subsection table"+e.getMessage());
				}
			break;

			case 28:
				/*ORDER INSPECTION*/
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ OrderInspection
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectioncompany varchar(200),fld_policynumber varchar(200),fld_jobno varchar(50),fld_squarefootage varchar(200),fld_noofstories varchar(200),fld_inspectiondate varchar(200),fld_inspectiontime varchar(200),fld_yearofconstruction varchar(200),fld_otheryearofconstruction varchar(200),fld_inspectionfee varchar(200),"+
						"fld_firstname varchar(200),fld_lastname varchar(200),fld_email varchar(200),fld_phone varchar(200),fld_inspectionaddress1 varchar(200),fld_inspectionaddress2 varchar(200),"+
						"fld_inspectioncity varchar(200),fld_inspectionstate varchar(200),fld_inspectioncounty varchar(200),fld_zip varchar(200),fld_flag integer,"+
						"fld_mailingaddress1 varchar(200),fld_mailingaddress2 varchar(200),fld_mailingcity varchar(200),fld_mailingstate varchar(200),fld_mailingcounty varchar(200),"+
						"fld_mailingzip varchar(200),fld_inspectorid varchar(200),fld_orderedid integer,fld_status integer);");
				} catch (Exception e){System.out.println("error="+e.getMessage());}
				
			break;
			case 29:
				/* SAVING NEW PRIMARY AND SCEONDARY CONDITION */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ ADDNewCondition
							+ " (c_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200),fld_submodule varchar(200),fld_visiblecondition varchar(1000),fld_isviscondprimorsecond integer);");
				} catch (Exception e) {
					System.out.println("Error in creating saving VC table"+e.getMessage());
				}
			break;
			case 30:
				try
				{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Downloaded_document
						+ "(down_id INTEGER PRIMARY KEY Not null,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50) NOT NULL," +
						"down_loc_path varchar(150) Default(''),fld_down_server_path varchar(150) Default(''),down_status varchar(5) Default('true'),fld_down_created_date DATETIME DEFAULT(CURRENT_TIMESTAMP));");
				}catch (Exception e) {
					// TODO: handle exception
				}
				break;
				
			case 31:
				try
				{
				hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ HomeWarranty
						+ "(h_id INTEGER PRIMARY KEY Not null,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(50) NOT NULL," +
						"fld_homewarrantoption varchar(150),fld_sendtoserver integer default '0');");
				}catch (Exception e) {
					// TODO: handle exception
				}
				break;
			case 32:
				/** FEEDBACK DOCUMENT **/
				try {
					// db("DROP TABLE IF  EXISTS "
					// + FeedBackDocumentTable + " ");
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "+FeedBackDocument+" (FD_D_Id INTEGER PRIMARY KEY AUTOINCREMENT," +
							"FD_D_InspectorId varchar(50) NOT NULL,FD_D_SRID varchar(50) NOT NULL,FD_D_doctit varchar(15) NOT NULL," +
							"FD_D_doctit_other varchar(50), FD_D_path varchar(100) NOT NULL,FD_D_type varchar(50)," +
							"FD_D_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP),fld_sendtoserver integer default '0')");
				} catch (Exception e) {}
				break;
				
			case 33:
				/** Feedback info table **/
				try {				
					
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "+FeedBackInfo+" (FD_Id INTEGER PRIMARY KEY AUTOINCREMENT," +
							"FD_InspectorId varchar(50) NOT NULL,FD_SRID varchar(50) NOT NULL,FD_CSC varchar(15) NOT NULL," +
							"FD_whowas varchar(15) NOT NULL,FD_whowas_other varchar(50),FD_insupaperwork varchar(15) NOT NULL,FD_manuinfo varchar(15) NOT NULL," +
							"FD_Attendees varchar(100) NOT NULL,FD_comments varchar(500) NOT NULL,FD_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP),fld_sendtoserver integer default '0')");
					CreateTable(131);
				} 
				catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
			break;
			
			case 34:
				/** Cover Photo table **/
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CoverPhoto
							+ " (p_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_image varchar(1250) NOT NULL,fld_caption varchar(1000)," +
							"fld_inspectorid varchar(50) NOT NULL,fld_srid varchar," +
							"fld_sendtoserver integer default '0');");
				} catch (Exception e) {
					System.out.println("Error in adding Cover Photo table"+e.getMessage());
				}
				break;			

			case 35:
				/* CREATING OTHER TEXT FOR CONTRACTOR TYPE OPTION */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ AddOtherCont
							+ " (aoctype_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_module varchar(200),fld_submodule varchar(200),fld_other varchar(200),fld_templatename varchar(200));");
				} catch (Exception e) {
					System.out.println("Error in creating other text for inspection point table"+e.getMessage());
				}
			break;
			
			case 36:
				/* CREATING CSE */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CSE
							+ " (cse_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(200),fld_inspectorname varchar(200),fld_inspectiondate varchar(200),fld_reportno varchar(200),fld_present varchar(200),fld_otherpresent varchar(200),"+ 
							"fld_arrive varchar(200),fld_prior varchar(200),fld_badge varchar(200),fld_shoes varchar(200),fld_listen varchar(200),fld_preinspection varchar(200),fld_difference varchar(200),"+
							"fld_final varchar(200),fld_confident varchar(200),fld_rate varchar(200),fld_comments varchar(200),fld_signature varchar(200),fld_date varchar(200));");
				} catch (Exception e) {
					System.out.println("Error in creating cse table"+e.getMessage());
				}
			break;
			
			case 37:
				/* CREATING PIA */
				try 
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ PIA
							+ " (pia_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(200),fld_signature varchar(200));");
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("Error in creating pia table"+e.getMessage());
				}
				break;
			case 38:
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateHVAC
							+ " (h_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_hvactype varchar(100),fld_hvactypeother varchar(100),fld_energysource varchar(200),fld_energysourceother varchar(100)," +
							"fld_zoneno integer default '0',fld_otherzoneno varchar(10) default '',fld_zonename varchar(500) default '',fld_otherzonename varchar(500) default '',fld_gasshutoff varchar(100),fld_gasshutoffother varchar(100),fld_fueltankloc varchar(500) default '',fld_fluetype varchar(500),"+
							   "fld_age varchar(100),fld_otherage varchar(100),fld_heatsystembrand varchar(100),fld_furnacetype varchar(100),fld_heatdeliverytype varchar(100),"+
							   "fld_blowermotor varchar(100),fld_testedfor varchar(500),fld_airhandlerfeatures varchar(500),"+
							   "fld_tonnage varchar(100),fld_tonnageother varchar(100),fld_btu varchar(100),fld_btuother varchar(100)," +
							   "fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				break;
			case 42:
				try
				{
				
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ LoadHVAC
							+ " (h_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(100),fld_hvactype varchar(100),fld_hvactypeother varchar(100),fld_energysource varchar(200),fld_energysourceother varchar(100)," +
							"fld_zoneno integer default '0',fld_otherzoneno varchar(10),fld_zonename varchar(500),fld_otherzonename varchar(500),fld_gasshutoff varchar(100),fld_gasshutoffother varchar(100),fld_fueltankloc varchar(500) default '',fld_fluetype varchar(500),"+
							   "fld_age varchar(100),fld_otherage varchar(100),fld_heatsystembrand varchar(100),fld_furnacetype varchar(100),fld_heatdeliverytype varchar(100),"+
							   "fld_blowermotor varchar(100),fld_testedfor varchar(500),fld_airhandlerfeatures varchar(500),"+
							   "fld_tonnage varchar(100),fld_tonnageother varchar(100),fld_btu varchar(100),fld_btuother varchar(100)," +
							   "fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				break;
			case 39:
				
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateHotwatersystem
							+ " (hws_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_characteristics varchar(1000)," +
							"fld_radiators varchar(500),fld_gaslog varchar(500),fld_gasloglocation varchar(500),fld_noofwoodstoves varchar(500) default '',fld_fluetype varchar(500) default '', fld_hotwatersysNA integer default '0',fld_gaslogNA integer default '0',fld_woodstoveNA integer default '0',fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				
				break;
				case 43:
				
				try
				{
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ LoadHotwatersystem
							+ " (hws_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_srid varchar(100),fld_inspectorid varchar(50) NOT NULL,fld_characteristics varchar(1000) default ''," +
							"fld_radiators varchar(500) default '',fld_gaslog varchar(500) default '',fld_gasloglocation varchar(500) default '',fld_noofwoodstoves varchar(500) default '',fld_fluetype varchar(500) default '',fld_hotwatersysNA integer default '0',fld_gaslogNA integer default '0',fld_woodstoveNA integer default '0',fld_sendtoserver integer default '0');");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				
				break;
				case 45:
					try
					{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ CreateDuctwork
								+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_zoneno varchar(100),fld_supplyregister varchar(100) default '',fld_retunrregister varchar(100) default '',"+
								   "fld_ducworktype varchar(500) default '',fld_multizonesystem varchar(100) default '',fld_ductworkcomment varchar(250) default '',fld_sendtoserver integer default '0');");
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("CreateDuctwork table creation error="+e.getMessage());
					}
				break;
				case 46:
					try
					{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ LoadDuctwork
								+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(100),fld_zoneno varchar(100),fld_supplyregister varchar(100) default '',fld_retunrregister varchar(100) default '',"+
								   "fld_ducworktype varchar(500) default '',fld_multizonesystem varchar(100) default '',fld_ductworkcomment varchar(250) default '',fld_sendtoserver integer default '0');");
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("CreateDuctwork table creation error="+e.getMessage());
					}
				break;
				case 47:
					try
					{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ CreateSystembrands
								+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_zoneno varchar(100),fld_zonename varchar(100) default '',fld_zonenameother varchar(100) default '',fld_noofzones varchar(100) default '',"+
							   "fld_externalunit varchar(100) default '',fld_internalunit varchar(100) default '',fld_extunitreplprob varchar(100) default '',fld_extunitreplprobother varchar(100) default ''," +
							   "fld_intunitreplprob varchar(100) default '',fld_intunitreplprobother varchar(100) default '',fld_ampextunit varchar(100) default '',fld_ampintunit varchar(100) default '',fld_emerheatampdraw varchar(100) default '',fld_supplytemp varchar(100) default ''," +
							   "fld_returntemp varchar(100) default '',fld_systembrandcomments varchar(250) default '',fld_sendtoserver integer default '0');");
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("CreateSystembrands table creation error="+e.getMessage());
					}
				break;
				case 48:
					try
					{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ LoadSystembrands
								+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(100),fld_zoneno varchar(100),fld_zonename varchar(100) default '',fld_zonenameother varchar(100) default '',fld_noofzones varchar(100) default '',"+
							   "fld_externalunit varchar(100) default '',fld_internalunit varchar(100) default '',fld_extunitreplprob varchar(100) default '',fld_extunitreplprobother varchar(100) default ''," +
							   "fld_intunitreplprob varchar(100) default '',fld_intunitreplprobother varchar(100) default '',fld_ampextunit varchar(100) default '',fld_ampintunit varchar(100) default '',fld_emerheatampdraw varchar(100) default '',fld_supplytemp varchar(100) default ''," +
							   "fld_returntemp varchar(100) default '',fld_systembrandcomments varchar(250) default '',fld_sendtoserver integer default '0');");
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("CreateSystembrands table creation error="+e.getMessage());
					}
				break;
				case 49:
					try
					{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ CreateThermostat
								+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_zoneno varchar(100),"+
							   "fld_thermotype varchar(250) default '',fld_thermolocation varchar(250) default '',fld_thermostatcomments varchar(500) default '',fld_sendtoserver integer default '0');");
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("CreateSystembrands table creation error="+e.getMessage());
					}
				break;
				case 50:
					try
					{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ LoadThermostat
								+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(100),fld_zoneno varchar(100),"+
							   "fld_thermotype varchar(100) default '',fld_thermolocation varchar(100) default '',fld_thermostatcomments varchar(250) default '',fld_sendtoserver integer default '0');");
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("CreateSystembrands table creation error="+e.getMessage());
					}
				break;
				case 51:
					try
					{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ HVACNotApplicable
								+ " (hvac_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(100),"+
							   "fld_HVACNA integer default 0,fld_SysbrandsNA integer default '0',fld_thermoNA integer default '0',fld_ductworkNA integer default '0',fld_sendtoserver integer default '0');");
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("CreateSystembrands table creation error="+e.getMessage());
					}
				break;
				
				case 52:
					try
					{
						hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ CreateHVACNotApplicable
								+ " (hvac_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,"+
							   "fld_HVACNA integer default 0,fld_SysbrandsNA integer default '0',fld_thermoNA integer default '0',fld_ductworkNA integer default '0',fld_sendtoserver integer default '0');");
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("CreateSystembrands table creation error="+e.getMessage());
					}
				break;
				
			case 40:
				try
				{
					System.out.println("coreect create zones created");
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ CreateDuctSysThermo
							+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_zoneno varchar(100),fld_zonename varchar(100) default '',fld_zonenameother varchar(100) default '',fld_noofzones varchar(100) default '',fld_supplyregister varchar(100) default '',fld_retunrregister varchar(100) default '',"+
							   "fld_ducworktype varchar(500) default '',fld_multizonesystem varchar(100) default '',fld_thermotype varchar(100) default '',fld_thermolocation varchar(100) default '',fld_externalunit varchar(100) default '',fld_internalunit varchar(100) default '',fld_extunitreplprob varchar(100) default '',fld_extunitreplprobother varchar(100) default ''," +
							   "fld_intunitreplprob varchar(100) default '',fld_intunitreplprobother varchar(100) default '',fld_ampextunit varchar(100) default '',fld_ampintunit varchar(100) default '',fld_emerheatampdraw varchar(100) default '',fld_supplytemp varchar(100) default ''," +
							   "fld_returntemp varchar(100) default '',fld_conddischarge varchar(500) default '',fld_ductworkcomment varchar(250) default '',fld_systembrandcomments varchar(250) default '',fld_thermostatcomments varchar(250) default '',fld_sendtoserver integer default '0');");
					System.out.println("coreect create");
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("tttesss="+e.getMessage());
				}			
				
				break;
			case 44:
				try
				{
					
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ LoadDuctSysThermo
							+ " (z_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_templatename varchar(200),fld_inspectorid varchar(50) NOT NULL,fld_srid varchar(100),fld_zoneno varchar(100),fld_zonename varchar(100) default '',fld_zonenameother varchar(100) default '',fld_noofzones varchar(100) default '',fld_supplyregister varchar(100) default '',fld_retunrregister varchar(100) default '',"+
							   "fld_ducworktype varchar(500) default '',fld_multizonesystem varchar(100) default '',fld_thermotype varchar(100) default '',fld_thermolocation varchar(100) default '',fld_externalunit varchar(100) default '',fld_internalunit varchar(100) default '',fld_extunitreplprob varchar(100) default '',fld_extunitreplprobother varchar(100) default ''," +
							   "fld_intunitreplprob varchar(100) default '',fld_intunitreplprobother varchar(100) default '',fld_ampextunit varchar(100) default '',fld_ampintunit varchar(100) default '',fld_emerheatampdraw varchar(100) default '',fld_supplytemp varchar(100) default ''," +
							   "fld_returntemp varchar(100) default '',fld_conddischarge varchar(500) default '',fld_ductworkcomment varchar(250) default '',fld_systembrandcomments varchar(250) default '',fld_thermostatcomments varchar(250) default '',fld_sendtoserver integer default '0');");
					System.out.println("coreect create");
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("tttesss="+e.getMessage());
				}
				break;
				
			case 41:
				try
				{
				
					hi_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ Invoice
							+ " (invoice_id INTEGER PRIMARY KEY AUTOINCREMENT,fld_inspectorid varchar(50) NOT NULL," +
							"fld_srid varchar(100),fld_invoiceno varchar(100) default '',fld_invoicedate varchar(100) ,fld_insppropaddr varchar(100) default '',fld_inspcity varchar(100) default '',fld_inspstate varchar(100) default '',fld_inspzip varchar(10) default '',fld_occupied varchar(100) default '',"+
							   "fld_utilitieson varchar(500) default '',fld_additionalfees varchar(100) default '',fld_annualinspection varchar(100) default '',fld_shippingandhandling varchar(100) default '',fld_eocoverage varchar(100) default '',fld_homeguide varchar(100) default '',fld_payment varchar(100) default '',fld_cardtype varchar(100) default ''," +
							   "fld_specialnotes varchar(1000) default '',fld_standardhomeinsp varchar(100) default '',fld_crawlfees varchar(100) default '',fld_subtotal varchar(100) default '',fld_salestax varchar(100) default '',fld_totalfees varchar(100) default ''," +
							   "fld_amountreceived varchar(100) default '',fld_balancedue varchar(500) default '',fld_sendtoserver integer default '0');");
					System.out.println("coreect create");
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("Invoice tbl="+e.getMessage());
				}			
				
				break;	
				
				
			
		} 
	}

	public String encode(String oldstring) {
		// TODO Auto-generated method stub
		/*try {
			   byte[] bytes = oldstring.getBytes("ISO-8859-1");
			   if (!validUTF8(bytes))
			    return oldstring;   
			   return new String(bytes, "UTF-8");  
	    } catch (UnsupportedEncodingException e) {
			   // Impossible, throw unchecked
			   throw new IllegalStateException("No Latin1 or UTF-8: " + e.getMessage());
			  }*/
		if(oldstring==null)
		{
			oldstring="";
		}
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return oldstring;
	}
	private boolean validUTF8(byte[] input) {
		// TODO Auto-generated method stub
		int i = 0;
		  // Check for BOM
		  if (input.length >= 3 && (input[0] & 0xFF) == 0xEF
		    && (input[1] & 0xFF) == 0xBB & (input[2] & 0xFF) == 0xBF) {
		   i = 3;
		  }

		  int end;
		  for (int j = input.length; i < j; ++i) {
		   int octet = input[i];
		   if ((octet & 0x80) == 0) {
		    continue; // ASCII
		   }

		   // Check for UTF-8 leading byte
		   if ((octet & 0xE0) == 0xC0) {
		    end = i + 1;
		   } else if ((octet & 0xF0) == 0xE0) {
		    end = i + 2;
		   } else if ((octet & 0xF8) == 0xF0) {
		    end = i + 3;
		   } else {
		    // Java only supports BMP so 3 is max
		    return false;
		   }

		   while (i < end) {
		    i++;
		    octet = input[i];
		    if ((octet & 0xC0) != 0x80) {
		     // Not a valid trailing byte
		     return false;
		    }
		   }
		  }
		  return true;

	}

	public String decode(String newstring) {
		// TODO Auto-generated method stub
		/*try {
			   byte[] bytes = newstring.getBytes("ISO-8859-1");
			   if (!validUTF8(bytes))
			    return newstring;   
			   return new String(bytes, "UTF-8");  
			  } catch (UnsupportedEncodingException e) {
			   // Impossible, throw unchecked
			   throw new IllegalStateException("No Latin1 or UTF-8: " + e.getMessage());
			  }*/
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
			/*if(newstring.contains("&#44;"))
			{
                 newstring = newstring.replace("&#44;",",");	
                 newstring = newstring.substring(0,newstring.length()-1);
                 
			}*/
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newstring;

	}

	public void userid() {
		// TODO Auto-generated method stub
		try
		{
			CreateTable(1);
			Cursor cur = hi_db.rawQuery("select * from " + Registration + " where fld_logout=1",null);System.out.println("dfds"+cur.getCount());
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				UserId = decode(cur.getString(cur.getColumnIndex("fld_userid")));
				UserName = decode(cur.getString(cur.getColumnIndex("fld_username")));
				Userfirstname = decode(cur.getString(cur.getColumnIndex("fld_firstname")));
				
				Userlastname = decode(cur.getString(cur.getColumnIndex("fld_lastname")));
				UserEmail = decode(cur.getString(cur.getColumnIndex("fld_email")));
				UserPhone = decode(cur.getString(cur.getColumnIndex("fld_contactphone")));
				Companyname = decode(cur.getString(cur.getColumnIndex("fld_companyname")));
				/*UserEmail = decode(cur.getString(cur.getColumnIndex("email")));
				UserHeadshot = decode(cur.getString(cur.getColumnIndex("headshot")));
				UserLogo = decode(cur.getString(cur.getColumnIndex("logo")));
				*/
			}
			cur.close();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}


}
