 package idsoft.idepot.homeinspection;

import java.util.ArrayList;

import idsoft.idepot.homeinspection.NewHVAC1.CHListener;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class NewGasLog extends Activity {
	String selectvalgaslog="",selectvalgasloglocation="", getgaslog="",getgaslocation="",gasnavalue="0",gettempname="",getcurrentview="",str="",getmodulename="",gettempoption="",getselectedho="",watervalue="",seltdval="",finalselstrval="",gaslogvalue="",tempgaslog="",gasloglocvalue="",tempgaslogloc="";
	Spinner spinloadtemplate;
	int k=0,id=0,spintouch=0,chk=0,gaslogdialog=0,gasloglocationdialog=0;
	View v;
	TextView spin_gasloglocation,spin_gaslog;
	String arrtempname[];
	String tempname="";
	ArrayAdapter adapter;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	LinearLayout lin;
	TextView tvgaslog,tvgasloglocation;
	CheckBox chknotapplicable;
	String[] gaslog={"Direct Vent Gas Log","Unvented Gas Log","Gas Log Pilot Light Off - Not Operated/Check Prior to Closing","No Gas to test systems",
			         "Remote control gas Log(Not tested)"},	gasloglocation={"Living Room","Fireplace","Master Fireplace"};
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	CheckBox ch[];
	Button btntakephoto; 	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}		
		
		setContentView(R.layout.activity_newgaslog);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(20);
		db.CreateTable(21);
		db.CreateTable(22);
		db.CreateTable(23);
		db.CreateTable(24);
		db.CreateTable(25);
		db.CreateTable(38);
		db.CreateTable(39);
		db.CreateTable(40);
		db.CreateTable(42);
		db.CreateTable(43);
		db.CreateTable(44);
		db.userid();
		
		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/squadaone-regular.ttf"); 
		btnsubmit.setTypeface(font); 
		
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);
									
				}  
			}
		});

		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
	
			// TODO Auto-generated method stub
					    Intent intent = new Intent(NewGasLog.this, PhotoDialog.class);
						Bundle b1 = new Bundle();
						b1.putString("templatename", gettempname);			
						intent.putExtras(b1);
						Bundle b2 = new Bundle();
						b2.putString("modulename", getmodulename);			
						intent.putExtras(b2);
						Bundle b4 = new Bundle();
						b4.putString("currentview","takephoto");			
						intent.putExtras(b4);
						Bundle b5 = new Bundle();
						b5.putString("selectedid", getselectedho);			
						intent.putExtras(b5);
						startActivity(intent);
						finish();
		}
	});

		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}	
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC", null);
            System.out.println("cur="+cur.getCount()+"select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC");
			if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("tempname"+tempname);
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);				
				}  
			}
		});
		
		
		chknotapplicable = (CheckBox) findViewById(R.id.hvacnotappl);		 
		chknotapplicable.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
	                //is chkIos checked?
			if (((CheckBox) v).isChecked())
			{
				chk=1;
				System.out.println("its checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
				
			}
			else
			{
				chk=2;
				System.out.println("its not checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
				
				
			}
	 
		  }
		});
		
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		
		
		
		tvgaslog = (TextView)findViewById(R.id.gaslogsel);
		tvgasloglocation = (TextView)findViewById(R.id.gasloglocationsel);
		
		spin_gaslog = (TextView)findViewById(R.id.gaslog);
		spin_gaslog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				gaslogdialog=1;
				watervalue="Gas Log";k=1;
				multi_select(watervalue,k);		
			}
		});
		
		spin_gasloglocation = (TextView)findViewById(R.id.gasloglocation);
		spin_gasloglocation.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				gasloglocationdialog=1;
				watervalue="Gas Log Location";k=2;
				multi_select(watervalue,k);		
			}
		});
		
		
	
		
		((TextView)findViewById(R.id.modulename)).setVisibility(v.VISIBLE);
		((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		
		Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(NewGasLog.this,HomeScreen.class));
					finish();
					 
				}
			});
		
		Button	save = (Button)findViewById(R.id.btnsave);
		save.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					if(chknotapplicable.isChecked()==true)
					{
						chk=1;
					}
					else
					{
						chk=2;
					}
					if(getcurrentview.equals("start"))
					{
						try
						{
							Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"'", null);
							System.out.println("CCC="+cur.getCount());
							if(cur.getCount()==0)
							{
								db.hi_db.execSQL(" INSERT INTO "+db.LoadHotwatersystem+" (fld_templatename,fld_inspectorid,fld_srid,fld_gaslog,fld_gasloglocation,fld_gaslogNA) VALUES" +
																			    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+db.encode(tempgaslog)+"','"+db.encode(tempgaslogloc)+"','"+chk+"')");
														
							}
							else
							{
								System.out.println("UPDATE  "+db.LoadHotwatersystem+" SET fld_gaslog='"+db.encode(tempgaslog)+"',fld_gaslogNA='"+chk+"'"+
								           "fld_gasloglocation='"+db.encode(tempgaslogloc)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
								db.hi_db.execSQL("UPDATE  "+db.LoadHotwatersystem+" SET fld_gaslog='"+db.encode(tempgaslog)+"',fld_gaslogNA='"+chk+"',"+
								           "fld_gasloglocation='"+db.encode(tempgaslogloc)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
							
								
							}
						}catch (Exception e) {
							// TODO: handle exception
							System.out.println("sasas"+e.getMessage());
						}
					}
					else
					{
						try
						{
							Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
							System.out.println("CCC="+cur.getCount());
							if(cur.getCount()==0)
							{
								db.hi_db.execSQL(" INSERT INTO "+db.CreateHotwatersystem+" (fld_templatename,fld_inspectorid,fld_gaslog,fld_gasloglocation,fld_gaslogNA) VALUES" +
																			    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+db.encode(tempgaslog)+"','"+db.encode(tempgaslogloc)+"','"+chk+"')");
														
							}
							else
							{
								System.out.println("UPDATE  "+db.CreateHotwatersystem+" SET fld_gaslog='"+db.encode(tempgaslog)+"',fld_gaslogNA='"+chk+"'"+
								           "fld_gasloglocation='"+db.encode(tempgaslogloc)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
								db.hi_db.execSQL("UPDATE  "+db.CreateHotwatersystem+" SET fld_gaslog='"+db.encode(tempgaslog)+"',fld_gaslogNA='"+chk+"',"+
								           "fld_gasloglocation='"+db.encode(tempgaslogloc)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
							
								
							}
						}catch (Exception e) {
							// TODO: handle exception
							System.out.println("sasas"+e.getMessage());
						}
					}
					

					cf.DisplayToast("Hot Water system saved successfully");
					Intent intent = new Intent(NewGasLog.this, NewWoodStove.class);
			    	Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("tempoption", gettempoption);			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("selectedid", getselectedho);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
				}
			});
		displaygaslog();
		LinearLayout menu = (LinearLayout) findViewById(R.id.hvacmenu);
		menu.addView(new Header_Menu(this, 6, cf,gettempname,getcurrentview,getmodulename,gettempoption,getselectedho,db));		
		
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(NewGasLog.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						cf.DefaultValue_Insert(gettempname,getselectedho);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	
	private void displaygaslog() {
		// TODO Auto-generated method stub
		String radiators="",characteristics="";
		Cursor cur;
		if(getcurrentview.equals("start"))
		{
			 cur= db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);	
		}
		else
		{
			 cur= db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
		}
		
		 
		System.out.println("sdf"+cur.getCount());if(cur.getCount()>0)
		 {
			 cur.moveToFirst();
			    try
				{
			    	gaslogvalue=db.decode(cur.getString(cur.getColumnIndex("fld_gaslog")));System.out.println("SDSD"+gaslogvalue);
			    	gasloglocvalue=db.decode(cur.getString(cur.getColumnIndex("fld_gasloglocation")));
			    	gasnavalue = cur.getString(cur.getColumnIndex("fld_gaslogNA"));System.out.println("tes="+gasnavalue);
			    	if(!gaslogvalue.equals(""))
			    	{
			    		tvgaslog.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+gaslogvalue));
			    		tvgaslog.setVisibility(v.VISIBLE);
			    	}
			    	
			    	if(!gasloglocvalue.equals(""))
			    	{
			    		tvgasloglocation.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+gasloglocvalue));
			    		tvgasloglocation.setVisibility(v.VISIBLE);
			    	}
			    	if(gasnavalue.equals("1"))
					{
						chknotapplicable.setChecked(true);
						((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE); 		
					}
					else
					{
						chknotapplicable.setChecked(false);
						((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
					}
			    	
				}
			 	catch (Exception e) {
					// TODO: handle exception
				}
		}
	}

	private void multi_select(final String watervalue, int s) {
		// TODO Auto-generated method stub
		id=1;
		dbvalue(watervalue);
		final Dialog add_dialog = new Dialog(NewGasLog.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);

		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);
		if(s==1)
		{
			showsuboption(tempgaslog,tvgaslog);	
		}
		else if(s==2)
		{
			showsuboption(tempgaslogloc,tvgasloglocation);
		}
		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(watervalue);
				
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				// TODO Auto-generated method stub
				seltdval = "";
				for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						seltdval += ch[i].getText().toString() + "&#44;";
					}
				}
				
				finalselstrval = "";
				finalselstrval = seltdval;
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{	
					System.out.println("WW="+watervalue);
					if(watervalue.equals("Gas Log"))
					{
						System.out.println("FF="+finalselstrval);
						gaslogvalue=finalselstrval;
						tempgaslog = gaslogvalue;System.out.println("ddd"+tempgaslog);
						tempgaslog = cf.replacelastendswithcomma(tempgaslog);
						tvgaslog.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempgaslog));
						tvgaslog.setVisibility(v.VISIBLE);
					}
					else if(watervalue.equals("Gas Log Location"))
					{
						gasloglocvalue=finalselstrval;
						tempgaslogloc = gasloglocvalue;
						tempgaslogloc = cf.replacelastendswithcomma(tempgaslogloc);
						tvgasloglocation.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempgaslogloc));
						tvgasloglocation.setVisibility(v.VISIBLE);
					}
					add_dialog.cancel();
					id=0;	
				}	
				
				
			
					}
				
		});
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    id=0;
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);		
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		
				
			}
		});
		
		add_dialog.setCancelable(false);
		add_dialog.show();
		setvalue(watervalue);
	}
	protected void setvalue(String watervalue2) {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur;
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}

			System.out.println("count"+cur.getCount());
		    if (cur.getCount() > 0) 
		    {
				cur.moveToFirst();
				str="";
				if(watervalue2.equals("Gas Log"))
				{
						str = db.decode(cur.getString(cur.getColumnIndex("fld_gaslog")));	
				}
				else if(watervalue2.equals("Gas Log Location"))
				{
						str = db.decode(cur.getString(cur.getColumnIndex("fld_gasloglocation")));
				}
				if(str.contains(","))
			    {
			    	str = str.replace(",","&#44;");
			    }	
			}
		    else
		    {
			
		    	if(watervalue2.equals("Gas Log") && gaslogvalue!=null)
				{
					str = tempgaslog;							 
				}
				else if(watervalue2.equals("Gas Log Location") && gasloglocation!=null)
				{
					str = tempgaslogloc;		
				}
		    }
			    if(!str.trim().equals(""))
				{
					cf.setValuetoCheckbox(ch, str);
				}	
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void showsuboption(String selectedvalue,TextView tv) {
		// TODO Auto-generated method stub
		
		if(selectedvalue.trim().equals(""))
		{
			if(tv.getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(tv.getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	tv.getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=selectedvalue;
		}
		

		  ch = new CheckBox[str_arrlst.size()];
			
			for (int i = 0; i < str_arrlst.size(); i++) {
	        		ch[i] = new CheckBox(this);
					ch[i].setText(str_arrlst.get(i));
					ch[i].setTextColor(Color.BLACK);
					
					lin.addView(ch[i]);
					ch[i].setOnCheckedChangeListener(new CHListener(i));
					
			}
		
		

		if (str != "") {
			
			 cf.setValuetoCheckbox(ch, str);
			 String replaceendcommaionspoint= replacelastendswithcomma(str);
			 tv.setVisibility(v.VISIBLE);
			 tv.setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
		}
		else
		{
			tv.setVisibility(v.GONE);
		}
		
	}
	class CHListener implements OnCheckedChangeListener, android.widget.CompoundButton.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		
		//seltdval="";
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
			}
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	private String replacelastendswithcomma(String realword) {
		// TODO Auto-generated method stub
		
		if(realword.contains("&#44;"))
		{
			realword = realword.replace("&#44;",",");
		}
		if(realword.endsWith(","))
		{
			realword = realword.substring(0,realword.length()-1);
		}
		else
		{
			realword = realword;	
		}	
		return realword;
	}
	private void dbvalue(String watervalue2) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		if(watervalue2.equals("Gas Log"))
		{System.out.println("SSSS="+gaslog.length);
			for(int j=0;j<gaslog.length;j++)
			{
				str_arrlst.add(gaslog[j]);
			}	
		}
		if(watervalue2.equals("Gas Log Location"))
		{
			for(int j=0;j<gasloglocation.length;j++)
			{
				str_arrlst.add(gasloglocation[j]);
			}	
		}
		
		try
		{
			 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(watervalue2)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"'",null);
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =0;i<c.getCount();i++,c.moveToNext())
				 {
					 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
				 }
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	protected void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(NewGasLog.this, NewHotwater.class);
    	Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		Bundle b6 = new Bundle();
		b6.putString("selectedid", getselectedho);			
		intent.putExtras(b6);
		startActivity(intent);
		finish();
       
	}
	
	protected void onSaveInstanceState(Bundle ext)
	{		
		
		ext.putInt("chkvalue", chk);
		ext.putString("columnname", watervalue);
		
		ext.putInt("gaslog_dialog",gaslogdialog);	
		ext.putInt("index",k);
		if(gaslogdialog==1)
	    {
	        		selectvalgaslog="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalgaslog += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("gaslogoption_index", selectvalgaslog);System.out.println("selectvalgaslog"+selectvalgaslog);
        }
        else
        {
        	ext.putString("gaslogoption_index", tempgaslog);
        }
		 ext.putInt("gasloglocation_dialog",gasloglocationdialog);	
		 if(gasloglocationdialog==1)
	        {
			 selectvalgasloglocation="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalgasloglocation += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("gasloglocoption_index", selectvalgasloglocation);
	        }
	        else
	        {
	        	ext.putString("gasloglocoption_index", tempgaslogloc);
	        }
		
		    ext.putString("get_gaslong",tvgaslog.getText().toString());
		    ext.putString("get_gasloglocation", tvgasloglocation.getText().toString());
		super.onSaveInstanceState(ext);
	}
	protected void onRestoreInstanceState(Bundle ext)
	{	
		chk = ext.getInt("chkvalue");
		if(chk==1)
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
		}
		else
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
		}
		gaslogdialog = ext.getInt("gaslog_dialog");		
		tempgaslog= ext.getString("gaslogoption_index");
	    if(gaslogdialog==1)
	   {
		   watervalue = ext.getString("columnname");
		   multi_select(ext.getString("columnname"),ext.getInt("index"));
		   if(!tempgaslog.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, tempgaslog);
		   }
	   }
	   
	    getgaslog = ext.getString("get_gaslong");
		   if(!getgaslog.equals(""))
		   {
			   
			  tvgaslog.setVisibility(v.VISIBLE);
			  tvgaslog.setText(getgaslog);
		   }
		   
		   gasloglocationdialog = ext.getInt("gasloglocation_dialog");		
			tempgaslogloc= ext.getString("gasloglocoption_index");
		    if(gasloglocationdialog==1)
		   {
			   watervalue = ext.getString("columnname");
			   multi_select(ext.getString("columnname"),ext.getInt("index"));
			   if(!tempgaslogloc.equals(""))
			   {				
			    	  cf.setValuetoCheckbox(ch, tempgaslogloc);
			   }
		   }
		   
		    getgaslocation = ext.getString("get_gasloglocation");
			   if(!getgaslocation.equals(""))
			   {
				   
				   tvgasloglocation.setVisibility(v.VISIBLE);
				   tvgasloglocation.setText(getgaslocation);
			   }
	   
	    
		 super.onRestoreInstanceState(ext);
	}
}
