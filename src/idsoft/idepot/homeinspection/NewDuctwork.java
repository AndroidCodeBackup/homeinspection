package idsoft.idepot.homeinspection;

import java.util.ArrayList;

import idsoft.idepot.homeinspection.CreateHeating.myspinselectedlistener;
import idsoft.idepot.homeinspection.LoadBathroom.CHListener;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;

public class NewDuctwork extends Activity {
	String gettempname="",getcurrentview="",getmodulename="",gettempoption="",getselectedho="",fueltankvalue="",finalselstrval="",tempfueltankloc="",getnoofzones="",getheatbrand="",
			seltdval="",tempfluetype="",temptestedfor="",tempairhandlerfeat="",tempthermolocation="",tempthermotype="",tempductworktype="",tempsupplyreg="",tempreturnreg="",tempconddischarge="",getzoneno="",
					getfurnace="",getmotorblower="",getsupplyregister="",getreturnregister="",getmultizonesystem="",str="",fluetypevalue="",hvacvalue="",
							conddischangevalue="",thermolocationvalue="",thermostattypevalue="",ductworktypevalue="",supplregvalue="",returnregvalue="",airhandlerfeatvalue="",testedforvalue="";
	ScrollView scr;
	TableLayout heatingtblshow;
	Spinner spinloadtemplate;
	View v;
	String arrtempname[];
	LinearLayout lin;
	String tempname="",selectvalductwork="",selectvalsupplyreg="",selectvalreturnreg="",getductwork="",getsupplyreg="",getreturnreg="";
	ArrayAdapter adapter;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	CheckBox chknotapplicable;
	TextView supplyregnote,returnregnote,supplyregister,returnregister,ductworknote,sp_ductworktype,ductworktypetv;
	int hvac=0,hvacCurrent_select_id,editid=0,getisedit=0,spintouch=0,chk=0,dcutwork_dialog=0,supplyregister_dialog=0,returnreg_dialog=0;
	static int k=0;
	CheckBox ch[];
	Button btntakephoto;
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	Spinner sp_heatingtype,sp_energysource,sp_gasshutoff,spin_noofzones,spin_heatingage,spin_zoneno,sp_zonename,sp_heatbrand,sp_furnace,
	sp_motorblower,sp_heatdelivery,sp_zonestestedfor,spin_supplyregister,sp_returnregister,sp_multizonesystem,sp_zonetonnage,sp_btu,
	sp_thermotype,sp_thermolocation,sp_externalunitreplprob,sp_internalunitreplprob;
	String getheatdeliverytype="",getspinselectedname="",getenergysource="",strenergysource="",getheatingtype="",getheatingage="",strotherheatingage="",strotherheattype="",getgasshutoff="",strgasshutoff="",strfluetype="",
			getfluetype="",getzonename="",strzonename="",getzonetonnage="",strzonetonnage="",getbtu="",strbtu="",strexternalunit="",strinternalunit="",getinternalunit="",getexternalunit="";
	EditText otherheatingage,ed_otherzonename;
	
	static String[] arrfueltank={"Above Ground","Below Ground","Not Applicable","Above Ground Oil Tank","Below Ground Oil Tank","Original Oil Tank",
		"Oil Tank Fill Cap and Vent Not Located","Tank Fill and Vent Located","Portable Gas Tank","Gas Tank Below Ground","Underground Gas Tank Fill Valve Not Located",
		"Underground Gas Tank Not Located","Underground Gas Tank"},
					arrfluetype={"Unlined Masonry Chimney","Old Masonry Chimney Redundant","PVC Schedule 40","No Flue Cap","Metal Flue Cap","Flexible Metal Gas Flue","Masonry Chimney",
				"Clay Flue Liner","Metal Flue"},
		arrductworktype={"Flexible Ductwork","Metal Ductwork","Slab Ductwork","Insulated","Ductwork in Crawl SPace","Automatic Dampers(not tested)","Untidy Ductwork Installation Leakage Evident"},
		arrzonetestingfor={"Tested For Cooling","Tested For Heating","Tested For Emergency Heat Only","Reverse valve not tested", "Outside temperature prohibited test"},
				arrthermolocation={"Hall", "Master Bedroom","Living Room", "Upstairs","Bonus", "Other"},
				arrthermotype={"Manual Thermostat","Programmable Thermostat","Set Back Thermostat","Heat Pump Thermostat","Multiple Zone","Separate Heating/Cooling Thermostat","Aquastat"},
				arrcondendischarge={"Overflow Condensate Tray","Condensate Pipe To Sump","Condensate Pipe","Condensate Pipe To Exterior","Float Switch/Pump (not tested)"},
		arrairhandlerfeatures={"Air Filter","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner","Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light","Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Condensation Discharger"};
	
	String[] arrheatingnum={"--Select--","1","2","3","4","5","6","Other"},strheatsytem ={"--Select--","Adequate","Inadequate","Appears Functional","Recommend Replacement"},
			arrheattype={"--Select--","Forced Air","Heat Pump Forced Air","Central Air - Cool/Air Conditioning Only","Packaged Central Air - Heat pump","Packaged Central Air - Cool/Air Conditioning Only","Wall Unit(s)","Heat Strip","None",
			"Electric Base","Electric Heat","Radiant Floor","Space Heater","Steam Boiler","Circulating Boiler","Coal Furnace Converted to Gas","Coal Furnace converted to Oil",
			"Hydroninc System","Oil Furnace converted to Gas","Old Oil Furnace as Back up to heat pump","Radiant  Ceiling","Window Unit","Swamp Cooler","Water to Air system",
			"Cooling Only System","Uses Heat Ductwork","Refrigerant System","System Aged/Operable","Other"},
			arrgasshutoff={"--Select--","Front Side","Left Side","Right Side","Rear side","Adjacent Tank","Adjacent meter","Other"},
			
			
			arrzonename={"--Select--","Bonus Room","Main Area","Master Bedroom","Second Floor","Other"},
			arrcoolingage={"--Select--","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","Other"},
			arrheatexchanger={"--Select--","1 Burner","2 Burner","3 Burner",
			"4 Burner","5 Burner","6 Burner","Other"},arrdistribution={"--Select--","Hot Water","Steam","Radiator","Metal Duct","One Pipe","Two Pipe","Other"},
			arrcirculator={"--Select--","Pump","Gravity","Other"},arrdrain={"--Select--","Manual","Automatic","Other"},arrfuse={"--Select--","Single Wall","Double Wall","PVC"},
			arrdevious={"--Select--","Temp Guage","Expansion Tank","Pressure Gauge","Other"},arrhumidifier={"--Select--","Aprilaire","Honeywell","Other"},arrthermostats={"--Select--","Individual",
			"Multi Zone","Programmable","Other"},
			arrasbestos={"--Select--","Yes","No"},arrheatingnumber,arrheatingnumother,arrheatingsysteme ,arrmanufacture,arrmodelnumber,
			arrenergy={"--Select--","Oil","Gas","Electric","Water","Liquid Propane","Kerosene","Solar","Wood","Coal","Corn","Natural Gas","Diesel","Other"},
					arrheatbrand={"--Select--","Not Determined", "Not Visible","Aged","Amana","American Standard","Americanaire","Ao Smith","Apollo","Aquatherm","Arcoaire","Armstrong",
			"Bryant","Burnham","Carrier","Climate Master","Coleman","Comfort Maker","Crown","Empire","Envirotemp","Frigidaire","Goodman",
			"Hallmark","Heatwave","Heil","Int.Comfort","Intertherm","Janitrol","Kenmore","Lennox","Luxaire","Magic Chef","Monitor","Ninety Two",
			"None","Oneida","Patriot","Payne","Peerless","Presidential","Quattro","Rheem","Rinnai","Ruud","Sears","Singer","Tempstar","Thermopride",
			"Toyostove","Trane","Unknown","Weatherking","Weil Mclean","York","Zephyr"},
			arrheatdelivery={"--Select--","Forced Air","Refrigerant","Electric Resistance Heat","Hot Water","Steam Heat","Radiant","Boiler"},
			arrwatersystem={"--Select--","One Pipe System","Two Pipe System","Fin Coils","Boiler","Subfloor","Convectors","Radiators","Baseboard"},
			arrradiators={"--Select--","Cast Iron Radiators","Steel Radiators","Manual Fill","Water Level Sight Glass","Bleed Valves"},
			arrmotorblower={"--Select--","Blower Fan","Direct Drive","Belt Driven"},
			arrwoodstoves={"--Select--","1","2","3","4","5","None","Other"},
			arrsupplyregister={"Low","High","Low & High"},
					arrreturnregister={"Low","High"},
					arrexternalunitrepprob={"--Select--","Low","Medium","High","Other"},
					arrzonetonnage={"--Select--",	"1.5 Ton","2 Ton","2.5 Ton","3 Ton","3.5 Ton","4 Ton","4.5 Ton","5 Ton","Other"},
							arrbtu={"--Select--","Not Determined","12000 BTU's","18000 BTU's","24000 BTU's","30000 BTU's","36000 BTU's","42000 BTU's","48000 BTU's","54000 BTU's","Other"},
			
			arrretunrregister={"--Select--","Low","High"},
				arrnoofzones={"--Select--","1","2","3","4","5"},
			arrmultizonesystem={"--Select--","Yes", "No", "Auto Damper Not Tested"},
			
			arrfurnacefeatures={"--Select--","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner"," Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light"," Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Air Filter"},
			arrfurnace={"--Select--","Air To Air Heat Pump","Water To Air Heat Pump","Forced Air Furnace","Induced Air Furnace","Gravity Furnace","Evaporated Located Over Furnace","Natural Draft System","Condensation Furnace System","Inside panels sealed(no coil access)"};
	ArrayAdapter externalunitrepadap,internalunitrepadap,thermolocationadap,thermotypeadap,zonetonnadap,btuarrayadap,multizonearraydap,ductworktypearraydap,zonetestedforadap,zonenameadap,zonenoarrayadap,fluetypearrayadap,noofzonesarrayadap,gasshutoffarrayadap,heatnumarrayadap,heatsystemarrayadap,heattypearrayadap,heatexchangerarrayadap,distributionarrayadap,circulatorarrayadap,drainarrayadap,fusearrayadap,deviousarrayadap,
	watersysarrayadap,returnregarraydap,supplyregarraydap,furnfeatarraydap, humidifierarrayadap,thermostatsarrayadap,fueltankarrayadap,asbestosarrayadap,energysourcearrayadap,agearrayadap,brandarrayadap,furnacearrayadap,heatdeliveryarrayadap,radiatorsarrayadap,motorarrayadap,woodstoveadap;
	EditText et_otherheatingnum,et_manufacturer,et_modelnumber,et_serialnumber,et_capacity,et_otherheatexchanger,et_otherdistribution,et_othercirculator,
	         et_otherdrain,et_otherdevious,et_otherhumidifier,et_otherthermostats,et_otherfueltank;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}		
		
		setContentView(R.layout.activity_newductwork);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		
		db.CreateTable(45);
		db.CreateTable(46);
		
		
		db.userid();
		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(NewDuctwork.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							Bundle b4 = new Bundle();
							b4.putString("currentview","takephoto");			
							intent.putExtras(b4);
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getselectedho);			
							intent.putExtras(b5);
							startActivity(intent);
							finish();
			}
		});
		
		Button btnsave = (Button)findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) 
				{
					// TODO Auto-generated method stub
					savehvacnotappl(getcurrentview);
					Intent intent = new Intent(NewDuctwork.this, NewSystembrand.class);
			    	Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("tempoption", gettempoption);			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("selectedid", getselectedho);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
					
				}
			});
		chknotapplicable = (CheckBox) findViewById(R.id.hvacnotappl);		 
		chknotapplicable.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
	                //is chkIos checked?
			if (((CheckBox) v).isChecked())
			{
				chk=1;
				System.out.println("its checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
				
			}
			else
			{
				chk=2;
				System.out.println("its not checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
				
				
			}
	 
		  }
		});

		spin_zoneno = (Spinner)findViewById(R.id.spin_zoneno);
		zonenoarrayadap = new ArrayAdapter<String>(NewDuctwork.this,android.R.layout.simple_spinner_item,arrnoofzones);
		zonenoarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_zoneno.setAdapter(zonenoarrayadap);
		spin_zoneno.setOnItemSelectedListener(new myspinselectedlistener(1));
		
		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/squadaone-regular.ttf"); 
		btnsubmit.setTypeface(font); 
		
		btnsubmit.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);
									
				}  
			}
		});
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC", null);
           if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);				
				}  
			}
		});
		
		
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		
		
		supplyregister = (TextView)findViewById(R.id.supplyregister);
		supplyregister.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				supplyregister_dialog=1;
				hvacvalue="Supply Registers Located";k=6;
				multi_select(hvacvalue,k);				
			}
		});
		returnregister = (TextView)findViewById(R.id.returnregister);
		returnregister.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				returnreg_dialog=1;
				hvacvalue="Return Registers Located";k=7;
				multi_select(hvacvalue,k);				
			}
		});
		
		ductworktypetv = (TextView)findViewById(R.id.spin_ductworktype);
		ductworktypetv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dcutwork_dialog=1;
				hvacvalue="Ductwork Type";k=5;
				multi_select(hvacvalue,k);				
			}
		});
		
		
		sp_multizonesystem = (Spinner)findViewById(R.id.spin_multizonesystem);
		multizonearraydap = new ArrayAdapter<String>(NewDuctwork.this,android.R.layout.simple_spinner_item,arrmultizonesystem);
		multizonearraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_multizonesystem.setAdapter(multizonearraydap);
		sp_multizonesystem.setOnItemSelectedListener(new myspinselectedlistener(20));
		
		
		((TextView)findViewById(R.id.modulename)).setVisibility(v.VISIBLE);
		((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		heatingtblshow= (TableLayout)findViewById(R.id.ductworktable);
		
		Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(NewDuctwork.this,HomeScreen.class));
					finish();
					 
				}
			});

		Button	btncancel = (Button)findViewById(R.id.btn_cancel);
		btncancel.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					clearduct();
					 
				}
			});
		
		
	/*	TextView vc = ((TextView)findViewById(R.id.vc));
		vc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent = new Intent(NewDuctwork.this, NewSetVC.class);
					Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", "create");			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("submodulename", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("tempoption", gettempoption);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
			}
		});		*/
		
		Button save = (Button)findViewById(R.id.btn_save);
		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getzoneno.equals("--Select--"))
				{
					cf.DisplayToast("Please select the Zone No.");
				}
				else
				{
					
					 if(getsupplyregister.equals("--Select--")){getsupplyregister="";}
					 if(getreturnregister.equals("--Select--")){getreturnregister="";}
					 if(getmultizonesystem.equals("--Select--")){getmultizonesystem="";}
					
					 try
						{
							 if(getcurrentview.equals("start"))
							 {
								 if(((Button)findViewById(R.id.btn_save)).getText().toString().equals("Update"))
								 {
										db.hi_db.execSQL("UPDATE  "+db.LoadDuctwork+" SET  fld_supplyregister='"+db.encode(tempsupplyreg)+"',fld_retunrregister='"+db.encode(tempreturnreg)+"',fld_ducworktype='"+db.encode(tempductworktype)+"'," +
							   					"fld_multizonesystem='"+getmultizonesystem+"',fld_ductworkcomment='"+((EditText)findViewById(R.id.ed_ductworkcomments)).getText().toString()+"'" +" Where z_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
				
										ductcommon();
								 }
								 else
								 {
										Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " WHERE fld_inspectorid='"+db.UserId+"'  and fld_srid='"+getselectedho+"' and fld_zoneno='"+getzoneno+"'", null);
										if(cur.getCount()==0)
										{
											db.hi_db.execSQL(" INSERT INTO "+db.LoadDuctwork+" (fld_templatename,fld_inspectorid,fld_srid,fld_zoneno,fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_ductworkcomment) VALUES" +
																						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+getzoneno+"','"+db.encode(tempsupplyreg)+"','"+db.encode(tempreturnreg)+"','"+db.encode(tempductworktype)+"','"+getmultizonesystem+"','"+((EditText)findViewById(R.id.ed_ductworkcomments)).getText().toString()+"')");
											
											 ductcommon();savehvacnotappl(getcurrentview);
										}
										else
										{
										/*	db.hi_db.execSQL("UPDATE  "+db.LoadDuctwork+" SET  fld_supplyregister='"+db.encode(tempsupplyreg)+"',fld_retunrregister='"+db.encode(tempreturnreg)+"',fld_ducworktype='"+db.encode(tempductworktype)+"'," +
								   					"fld_multizonesystem='"+getmultizonesystem+"',fld_ductworkcomment='"+((EditText)findViewById(R.id.ed_ductworkcomments)).getText().toString()+"'" +" Where z_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
					
											ductcommon();*/
											 cf.DisplayToast("Already Exists!! Please select another Zone No.");
										}
								 }
							 }
							 else
							 {
								 if(((Button)findViewById(R.id.btn_save)).getText().toString().equals("Update"))
									{
											db.hi_db.execSQL("UPDATE  "+db.CreateDuctwork+" SET  fld_supplyregister='"+db.encode(tempsupplyreg)+"',fld_retunrregister='"+db.encode(tempreturnreg)+"',fld_ducworktype='"+db.encode(tempductworktype)+"'," +
													   					"fld_multizonesystem='"+getmultizonesystem+"',fld_ductworkcomment='"+((EditText)findViewById(R.id.ed_ductworkcomments)).getText().toString()+"'" +" Where z_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
										
											 ductcommon();
											
									}
									else
									{
										try
										{
												Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDuctwork + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'", null);
												if(cur.getCount()==0)
												{
													db.hi_db.execSQL(" INSERT INTO "+db.CreateDuctwork+" (fld_templatename,fld_inspectorid,fld_zoneno,fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_ductworkcomment) VALUES" +
																								    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getzoneno+"','"+db.encode(tempsupplyreg)+"','"+db.encode(tempreturnreg)+"','"+db.encode(tempductworktype)+"','"+getmultizonesystem+"','"+((EditText)findViewById(R.id.ed_ductworkcomments)).getText().toString()+"')");
													
													 ductcommon();savehvacnotappl(getcurrentview);
												}
												else
												{
													/*db.hi_db.execSQL("UPDATE  "+db.CreateDuctwork+" SET  fld_supplyregister='"+db.encode(getsupplyregister)+"',fld_retunrregister='"+db.encode(getreturnregister)+"',fld_ducworktype='"+db.encode(tempductworktype)+"'," +
										   					"fld_multizonesystem='"+getmultizonesystem+"',fld_ductworkcomment='"+((EditText)findViewById(R.id.ed_ductworkcomments)).getText().toString()+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
													 ductcommon();*/
													 cf.DisplayToast("Already Exists!! Please select another Zone No.");
												}
										}
										catch (Exception e) {
											// TODO: handle exception
											 System.out.println("insi teeeee"+e.getMessage());
										}
									}
							 }
						 
						}
						catch (Exception e) {
							// TODO: handle exception
							 System.out.println("insi sdasdaseeeea"+e.getMessage());
						}
				}			
			}
		});
		ductworknote = (TextView)findViewById(R.id.ductworktypetv);
		supplyregnote = (TextView)findViewById(R.id.supplyregistertv);
		returnregnote = (TextView)findViewById(R.id.returnregistertv);
		
		LinearLayout menu = (LinearLayout) findViewById(R.id.hvacmenu);
		menu.addView(new Header_Menu(this, 2, cf,gettempname,getcurrentview,getmodulename,gettempoption,getselectedho,db));
		displayduct();shownotpplicable();
	}
	private void shownotpplicable() {
		// TODO Auto-generated method stub
		Cursor cur1;
		if(getcurrentview.equals("start"))
		{
			  cur1= db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' ",null);
		}
		else
		{
			  cur1= db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable+ " where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+getselectedho+"' ",null);	
		}
		System.out.println("cur11="+cur1.getCount());
		if(cur1.getCount()>0)
		{
			cur1.moveToFirst();
			String navalue = cur1.getString(cur1.getColumnIndex("fld_ductworkNA"));System.out.println("tes="+navalue);
			if(navalue.equals("1"))
			{System.out.println("inside checked");
				chknotapplicable.setChecked(true);
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE); 		
			}
			else
			{System.out.println(" fdsfdsf");
				chknotapplicable.setChecked(false);
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
			}
		}
		
	}
	private void savehvacnotappl(String currentview) {
		// TODO Auto-generated method stub
		try
		{
			if(chknotapplicable.isChecked()==true)
			{
				chk=1;
			}
			else
			{
				chk=2;
			}
			
			Cursor cur2;
			if(currentview.equals("start"))
			{
				 cur2= db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);	
			}
			else
			{
				 cur2= db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
			
			
			if(cur2.getCount()==0)
			{
				if(currentview.equals("start"))
				{
					db.hi_db.execSQL(" INSERT INTO "+db.HVACNotApplicable+" (fld_templatename,fld_inspectorid,fld_srid,fld_ductworkNA) VALUES" +
															    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+chk+"')");
				}
				else
				{
					db.hi_db.execSQL(" INSERT INTO "+db.CreateHVACNotApplicable+" (fld_templatename,fld_inspectorid,fld_ductworkNA) VALUES" +
						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+chk+"')");
				}
						
			}
			else
			{
				if(currentview.equals("start"))
				{
					db.hi_db.execSQL("UPDATE  "+db.HVACNotApplicable+" SET fld_ductworkNA='"+chk+"' Where fld_inspectorid='"+db.UserId+"' and fld_Srid='"+getselectedho+"'");
				}
				else
				{
					db.hi_db.execSQL("UPDATE  "+db.CreateHVACNotApplicable+" SET fld_ductworkNA='"+chk+"' Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"'");
				}
				
			
			}
		}
		catch(Exception e1)
		{
			System.out.println("not appl="+e1.getMessage());
		}
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(NewDuctwork.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						DefaultValue_Insert(gettempname,getselectedho);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	
	private void DefaultValue_Insert(String gettempname,String getselectedho) {
		try
		{
			Cursor selcur2 = db.hi_db.rawQuery("select * from " + db.CreateDuctwork + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' ", null);
            int cnt = selcur2.getCount();
            if(cnt>0)
           
            	
				if (selcur2 != null) {
					selcur2.moveToFirst();
					db.hi_db.execSQL("Delete from "+db.LoadDuctwork+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
					do {
						
						Cursor cur2 = db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
						
    		            if(cur2.getCount()==0)
    		            {
							try
			            	{
								
								
								
								db.hi_db.execSQL("INSERT INTO "+db.LoadDuctwork+" (fld_templatename,fld_srid,fld_inspectorid,fld_zoneno,fld_zonename,fld_zonenameother,fld_noofzones,fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_ductworkcomment) VALUES" +
										"('"+db.encode(gettempname)+"','"+getselectedho+"','"+db.UserId+"','"+selcur2.getString(4)+"','"+selcur2.getString(5)+"','"+selcur2.getString(6)+"','"+selcur2.getString(7)+"','"+selcur2.getString(8)+"','"+selcur2.getString(9)+"')");
		                 	
								System.out.println("error catch completed");
			            	}
			            	catch (Exception e) {
								// TODO: handle exception
			            		System.out.println("error catch11="+e.getMessage());
							}
    		            }
    		           
					
					}while (selcur2.moveToNext());
				}
            
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	private void ductcommon()
	{
		cf.DisplayToast("Ductwork Type saved successfully");
		((Button)findViewById(R.id.btn_save)).setText("Save and Add more");
		clearduct();
		displayduct();
	}
	private void clearduct() {
		// TODO Auto-generated method stub
		System.out.println("came inside cleardunt"+tempsupplyreg+tempductworktype+tempreturnreg);
		tempsupplyreg="";tempductworktype="";tempreturnreg="";
		spin_zoneno.setSelection(0);spin_zoneno.setEnabled(true);
		sp_multizonesystem.setSelection(0);ductworktypevalue="";supplregvalue="";returnregvalue="";
	
		ductworknote.setVisibility(v.GONE);
		supplyregnote.setVisibility(v.GONE);
		returnregnote.setVisibility(v.GONE);
		((Button)findViewById(R.id.btn_save)).setText("Save and Add more");
		((EditText)findViewById(R.id.ed_ductworkcomments)).setText("");
	}

	private void displayduct() {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur;
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
				  System.out.println("select * from " + db.LoadDuctwork + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'");
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateDuctwork + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}			
		System.out.println("cc="+cur.getCount());
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 try
					{
						heatingtblshow.removeAllViews();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.systembrand_header, null); 
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.ductworkheader);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();System.out.println("SSSSSS");
			
				 heatingtblshow.setVisibility(View.VISIBLE); 
				 
				 heatingtblshow.addView(th,lp); 			 
				
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String supplyregister="",retunrregister="",zoneno="",ducworktype="",multizonesystem="",comments="";
					 
					 zoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));					 
					 supplyregister=db.decode(cur.getString(cur.getColumnIndex("fld_supplyregister")));
					 retunrregister=db.decode(cur.getString(cur.getColumnIndex("fld_retunrregister")));					 
					 ducworktype=db.decode(cur.getString(cur.getColumnIndex("fld_ducworktype")));
					 multizonesystem=db.decode(cur.getString(cur.getColumnIndex("fld_multizonesystem")));
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_ductworkcomment")));	
					  

					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.systembrand_listview, null);
					 TableRow t = (TableRow)l1.findViewById(R.id.ductworkvalue);System.out.println("TTT");
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     
				   TextView tvzoneno= (TextView) t.findViewWithTag("zoneno");System.out.println("zoneno");
					 if(zoneno.equals("--Select--"))
						{
						 zoneno="";
						}
					 tvzoneno.setText(zoneno);
						
										
						
					 TextView tvsupplyregister= (TextView) t.findViewWithTag("supplyregisters");
					 if(supplyregister.equals("--Select--"))
					 {
						 supplyregister="";
					 }
					 tvsupplyregister.setText(supplyregister);
					 
					 TextView tvmultizonesystem= (TextView) t.findViewWithTag("multizonesystem");
					 if(multizonesystem.equals("--Select--"))
					 {
						 multizonesystem="";
					 }
					 tvmultizonesystem.setText(multizonesystem);
					 
						
					 TextView tvretunrregister= (TextView) t.findViewWithTag("returnregisters");
					 if(retunrregister.equals("--Select--"))
					 {
						 retunrregister="";
					 }
					 tvretunrregister.setText(retunrregister);
					 
					 TextView tvductworktype= (TextView) t.findViewWithTag("ductworktype");
					 if(ducworktype.equals("--Select--"))
					 {
						 ducworktype="";
					 }
					 tvductworktype.setText(ducworktype);

					 
					 TextView tvductworkcomments= (TextView) t.findViewWithTag("comments");
					
					 tvductworkcomments.setText(comments);
					 
					 
					 
					 ImageView edit= (ImageView) t.findViewWithTag("edit");
					 	edit.setId(789456+i);
					 	edit.setTag(cur.getString(0));
		                edit.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
							try
							{
							   int i=Integer.parseInt(v.getTag().toString());
								((Button)findViewById(R.id.btn_save)).setText("Update");
							   //vrint=1;
							clearduct();
							   updatehvac(i);   
							}
							catch (Exception e) {
								// TODO: handle exception
							}
							}
		                });
	             
	                ImageView delete=(ImageView) t.findViewWithTag("del");  
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(NewDuctwork.this);
							builder.setMessage("Do you want to delete the selected ductowrk type details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadDuctwork+" Where z_id='"+i+"'");
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateDuctwork+" Where z_id='"+i+"'");	
													}
													
													
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
											
													clearduct();
													displayduct();((Button)findViewById(R.id.btn_save)).setText("Save and Add more");
													
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					
					
					
					
					l1.removeAllViews();
					
					heatingtblshow.addView(t,lp);
					cur.moveToNext();
					
				 }
			 }
			 else
			 {
				 heatingtblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	 	
	}
	private void scrollup() {
		// TODO Auto-generated method stub
	scr = (ScrollView)findViewById(R.id.scr);
	       scr.post(new Runnable() {
	          public void run() {
	              scr.scrollTo(0,0);
	          }
	      }); 
	}
	protected void updatehvac(int i) {
		// TODO Auto-generated method stub
		Cursor cur;
			editid=1;
			getisedit=1;
			hvacCurrent_select_id = i;scrollup();
			try
			{
				if(getcurrentview.equals("start"))
				{
					  cur= db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'",null);
				}
				else
				{
					 cur= db.hi_db.rawQuery("select * from " + db.CreateDuctwork + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'", null);
				}
				//	cur= db.hi_db.rawQuery("select * from " + db.CreateDuctwork + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'",null);
			System.out.println("sdsd"+cur.getCount()+"select * from " + db.CreateDuctwork + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'");
				String comments="";
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
						
					 getzoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));					 
					 tempsupplyreg=db.decode(cur.getString(cur.getColumnIndex("fld_supplyregister")));
					 tempreturnreg=db.decode(cur.getString(cur.getColumnIndex("fld_retunrregister")));					 
					 tempductworktype=db.decode(cur.getString(cur.getColumnIndex("fld_ducworktype")));
					 getmultizonesystem=db.decode(cur.getString(cur.getColumnIndex("fld_multizonesystem")));
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_ductworkcomment")));
					 
					spin_zoneno.setSelection(zonenoarrayadap.getPosition(getzoneno));		spin_zoneno.setEnabled(false);		

					if(getmultizonesystem.equals(""))
					{
						sp_multizonesystem.setSelection(0);
					}
					else
					{
						sp_multizonesystem.setSelection(multizonearraydap.getPosition(getmultizonesystem));
					}
					 if(!tempductworktype.equals(""))
					 {
						 ductworknote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempductworktype));
						 ductworknote.setVisibility(v.VISIBLE); 
					 }
					 else
					 {
						 ductworknote.setVisibility(v.GONE);	 
					 }
					 
					 if(!tempsupplyreg.equals(""))
					 {
						 supplyregnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempsupplyreg));
						 supplyregnote.setVisibility(v.VISIBLE); 
					 }
					 else
					 {
						 supplyregnote.setVisibility(v.GONE);	 
					 }
					 
					 
					 if(!tempreturnreg.equals(""))
					 {
						 returnregnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempreturnreg));
						 returnregnote.setVisibility(v.VISIBLE); 
					 }
					 else
					 {
						 returnregnote.setVisibility(v.GONE);	 
					 }
					 
					 
					((EditText)findViewById(R.id.ed_ductworkcomments)).setText(comments);
					((Button)findViewById(R.id.btn_save)).setText("Update");
				}
			}catch(Exception e){}
		
	}
	private void dbvalue(String bathvalue) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		
		if(bathvalue.equals("Ductwork Type"))
		{
			for(int j=0;j<arrductworktype.length;j++)
			{
				str_arrlst.add(arrductworktype[j]);
			}	
		}
		
		
		if(bathvalue.equals("Supply Registers Located"))
		{
			for(int j=0;j<arrsupplyregister.length;j++)
			{
				str_arrlst.add(arrsupplyregister[j]);
			}	
		}
		
		if(bathvalue.equals("Return Registers Located"))
		{
			for(int j=0;j<arrreturnregister.length;j++)
			{
				str_arrlst.add(arrreturnregister[j]);
			}	
		}
		
		
	}
	
	private void multi_select(final String hvacvalue, final int s) {
		// TODO Auto-generated method stub
		hvac=1;System.out.println("hvacvalue="+hvacvalue);
		dbvalue(hvacvalue);
		final Dialog add_dialog = new Dialog(NewDuctwork.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);
		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);	
		 		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(hvacvalue);
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);
		btnother.setVisibility(v.GONE);
		if(s==5)
		{
			showsuboption(tempductworktype,ductworknote);
		}
		else if(s==6)
		{
			showsuboption(tempsupplyreg,supplyregnote);
		}
		else if(s==7)
		{
			showsuboption(tempreturnreg,returnregnote);
		}
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval="";
								for (int i = 0; i < str_arrlst.size(); i++) {
									if (ch[i].isChecked()) {
										seltdval += ch[i].getText().toString() + "&#44;";
									}
								}							
								
								finalselstrval = "";
								finalselstrval = seltdval;
								if(finalselstrval.equals(""))
								{
									cf.DisplayToast("Please check at least any 1 option to save");
								}
								else
								{	
									if(hvacvalue.equals("Ductwork Type"))
									{
										ductworktypevalue=finalselstrval;
										tempductworktype = ductworktypevalue;
										tempductworktype = cf.replacelastendswithcomma(tempductworktype);
										ductworknote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempductworktype));
										ductworknote.setVisibility(v.VISIBLE);
									}									
									if(hvacvalue.equals("Supply Registers Located"))
									{
										supplregvalue=finalselstrval;
										tempsupplyreg = supplregvalue;
										tempsupplyreg = cf.replacelastendswithcomma(tempsupplyreg);
										supplyregnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempsupplyreg));
										supplyregnote.setVisibility(v.VISIBLE);
									}									
									if(hvacvalue.equals("Return Registers Located"))
									{
										returnregvalue=finalselstrval;
										tempreturnreg = returnregvalue;
										tempreturnreg = cf.replacelastendswithcomma(tempreturnreg);
										returnregnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempreturnreg));
										returnregnote.setVisibility(v.VISIBLE);
									}
									add_dialog.cancel();
									if(s==5){dcutwork_dialog=0;}
									else if(s==6){supplyregister_dialog=0;}
									else if(s==7){returnreg_dialog=0;}
									hvac=0;
								}								
					}
				
		});

		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(s==5){dcutwork_dialog=0;}
				else if(s==6){supplyregister_dialog=0;}
				else if(s==7){returnreg_dialog=0;}
				hvac=0;
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		
		setvalue(hvacvalue);
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	
	private void setvalue(String hvacvalue) {
		// TODO Auto-generated method stub
		Cursor cur =null;
		try
		{
			if(getcurrentview.equals("start"))
			{
				cur= db.hi_db.rawQuery("select * from " + db.LoadDuctwork + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"' and fld_zoneno='"+spin_zoneno.getSelectedItem().toString()+"'",null);
			}
			else
			{
				cur = db.hi_db.rawQuery("select * from " + db.CreateDuctwork+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+spin_zoneno.getSelectedItem().toString()+"'", null);
			}
			
			System.out.println("count"+cur.getCount());
		    if (cur.getCount() > 0) 
		    {
				cur.moveToFirst();
				if(hvacvalue.equals("Ductwork Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_ducworktype")));	
				}			
				else if(hvacvalue.equals("Supply Registers Located"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_supplyregister")));	
				}
				else if(hvacvalue.equals("Return Registers Located"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_retunrregister")));	
				}
				
				if(str.contains(","))
			    {
			    	str = str.replace(",","&#44;");
			    }	
			}
		    else
		    {
			
		    	if(hvacvalue.equals("Ductwork Type") && hvacvalue!=null)
				{
					str =  ductworktypevalue;
				}			
		    	else if(hvacvalue.equals("Supply Registers Located") && hvacvalue!=null)
				{
					str =  supplregvalue;
				}
		    	else if(hvacvalue.equals("Return Registers Located") && hvacvalue!=null)
				{
					str = returnregvalue;
				}
				
		    }
			    if(!str.trim().equals(""))
				{
					cf.setValuetoCheckbox(ch, str);
				}	
		}catch (Exception e) {
			// TODO: handle exception
		}
	    	
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	
	private void showsuboption(String selectedvalue,TextView tv) {
		// TODO Auto-generated method stub
		
		if(selectedvalue.trim().equals(""))
		{
			if(tv.getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(tv.getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	tv.getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=selectedvalue;
		}
		

		  ch = new CheckBox[str_arrlst.size()];
			
			for (int i = 0; i < str_arrlst.size(); i++) {
	        		ch[i] = new CheckBox(this);
					ch[i].setText(str_arrlst.get(i));
					ch[i].setTextColor(Color.BLACK);
					
					lin.addView(ch[i]);
					ch[i].setOnCheckedChangeListener(new CHListener(i));
					
			}
		
		

		if (str != "") {
			
			 cf.setValuetoCheckbox(ch, str);
			 String replaceendcommaionspoint= replacelastendswithcomma(str);
			 tv.setVisibility(v.VISIBLE);
			 tv.setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
		}
		else
		{
			tv.setVisibility(v.GONE);
		}
		
		
	}
	private String replacelastendswithcomma(String realword) {
		// TODO Auto-generated method stub
		
		if(realword.contains("&#44;"))
		{
			realword = realword.replace("&#44;",",");
		}
		if(realword.endsWith(","))
		{
			realword = realword.substring(0,realword.length()-1);
		}
		else
		{
			realword = realword;	
		}	
		return realword;
	}
	class CHListener implements OnCheckedChangeListener, android.widget.CompoundButton.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		
		//seltdval="";
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
			}
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	class myspinselectedlistener implements OnItemSelectedListener
	{
         int var=0;
        
		public myspinselectedlistener(int i) {
			// TODO Auto-generated constructor stub
			var=i;
			 getspinselectedname="";
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			getspinselectedname = arg0.getItemAtPosition(arg2).toString();
			switch(var)
			{
					case 1:
					 	getzoneno = getspinselectedname;				
					 break;
					  case 18:
				    	  getsupplyregister = getspinselectedname;
				    	  break;
				      case 19:
				    	  getreturnregister = getspinselectedname;
				    	  break;  
				      case 20:
				    	  getmultizonesystem = getspinselectedname;
				    	  break;	  
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	protected void back() {

		// TODO Auto-generated method stub
		Intent intent = new Intent(NewDuctwork.this, NewHVAC1.class);
    	Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		Bundle b6 = new Bundle();
		b6.putString("selectedid", getselectedho);			
		intent.putExtras(b6);
		startActivity(intent);
		finish();
		
       
	}
	protected void onSaveInstanceState(Bundle ext)
	{		
		
		ext.putInt("chkvalue", chk);
		ext.putString("columnname", hvacvalue);
		
		ext.putInt("ductwork_dialog",dcutwork_dialog);	
		ext.putInt("index",k);
		if(dcutwork_dialog==1)
	        {
	        selectvalductwork="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalductwork += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("ductworkoption_index", selectvalductwork);System.out.println("selectvalductwork"+selectvalductwork);
	        }
	        else
	        {
	        	ext.putString("ductworkoption_index", tempductworktype);
	        }
		 ext.putInt("supplyregdialog",supplyregister_dialog);	
		 if(supplyregister_dialog==1)
	        {
			 selectvalsupplyreg="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalsupplyreg += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("suppplyregoption_index", selectvalsupplyreg);
	        }
	        else
	        {
	        	ext.putString("suppplyregoption_index", tempsupplyreg);
	        }
		 ext.putInt("returnregdialog",returnreg_dialog);	
		 if(returnreg_dialog==1)
		 {
			 selectvalreturnreg="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalreturnreg += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("returnregoption_index", selectvalreturnreg);
		 }
		 else
		 {
				ext.putString("returnregoption_index", tempreturnreg);
		 }
		 

		    ext.putString("get_ductwork", ((TextView) findViewById(R.id.ductworktypetv)).getText().toString());
		    ext.putString("get_supplyreg", ((TextView) findViewById(R.id.supplyregistertv)).getText().toString());
		    ext.putString("get_returnreg", ((TextView) findViewById(R.id.returnregistertv)).getText().toString());
		super.onSaveInstanceState(ext);
	}
	protected void onRestoreInstanceState(Bundle ext)
	{	
		chk = ext.getInt("chkvalue");
		if(chk==1)
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
		}
		else
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
		}
		dcutwork_dialog = ext.getInt("ductwork_dialog");		
		tempductworktype = ext.getString("ductworkoption_index");System.out.println("tempductworktype==="+tempductworktype);
	    if(dcutwork_dialog==1)
	   {
		   hvacvalue = ext.getString("columnname");
		   multi_select(ext.getString("columnname"),ext.getInt("index"));
		   if(!tempductworktype.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, tempductworktype);
		   }
	   }
	
	    getductwork = ext.getString("get_ductwork");
	   if(!getductwork.equals(""))
	   {
		   
		   ((TextView) findViewById(R.id.ductworktypetv)).setVisibility(v.VISIBLE);
		   ((TextView) findViewById(R.id.ductworktypetv)).setText(getductwork);
	   }
	   
	   supplyregister_dialog = ext.getInt("supplyregdialog");		System.out.println("supplyreg_dialog"+supplyregister_dialog);
		tempsupplyreg = ext.getString("suppplyregoption_index");
	    if(supplyregister_dialog==1)
	   {
		   hvacvalue = ext.getString("columnname");
		   multi_select(ext.getString("columnname"),ext.getInt("index"));
		   if(!tempsupplyreg.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, tempsupplyreg);
		   }
	   }
		
		   getsupplyreg = ext.getString("get_supplyreg");
		   if(!getsupplyreg.equals(""))
		   {
			   
			   ((TextView) findViewById(R.id.supplyregistertv)).setVisibility(v.VISIBLE);
			   ((TextView) findViewById(R.id.supplyregistertv)).setText(getsupplyreg);
		   }
	    
	
		   returnreg_dialog = ext.getInt("returnregdialog");
		   
			tempreturnreg = ext.getString("returnregoption_index");
		    if(returnreg_dialog==1)
		   {
			   hvacvalue = ext.getString("columnname");
			   multi_select(ext.getString("columnname"),ext.getInt("index"));
			   if(!tempreturnreg.equals(""))
			   {				
			    	  cf.setValuetoCheckbox(ch, tempreturnreg);
			   }
		   }
			
		    getreturnreg = ext.getString("get_returnreg");
			   if(!getreturnreg.equals(""))
			   {
				   
				   ((TextView) findViewById(R.id.returnregistertv)).setVisibility(v.VISIBLE);
				   ((TextView) findViewById(R.id.returnregistertv)).setText(getreturnreg);
			   }
			   
			   
		
		
		 super.onRestoreInstanceState(ext);
	}
}

