package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.TextWatchLimit;
import idsoft.idepot.homeinspection.support.TouchFoucs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
public class Feedback extends Activity{
	
	DatabaseFunction db;
	CommonFunction cf;
	String getselectedho="",inspectorid="";
	String whowas="",who_other="";
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	String secondaryyarr[];
	String redcolor ="<font color=red> * </font>",csetext="",insupaperworktxt="",manufactinfotxt="";
	LinearLayout lin,sup_l,off_l;
	View v;
	Spinner sp[]=new Spinner[3];
	EditText ed_others[]= new EditText[3];
	RadioGroup rg[] = new RadioGroup[3];
	CheckBox[] ch;
	boolean csebool=false;
	int loadcomment_code=14;
	String[] array_who={"--Select--","Owner","Representative","Agent","No One Present","Other"},
			array_doc={"--Select--","Acknowledgement Form","CSE Form","OIR 1802 Form","Paper Signup Sheet","Other Information","Roof Permit","Sketch","Building Permit","Property Appraisal Information","Field Inspection Report","Field Notes"};
	EditText comments;
	public CheckBox cbwhowas[] = new CheckBox[4];
	public int[] cbwhowasid = {R.id.feedback_owner,R.id.feedback_rep,R.id.feedback_agent,R.id.feedback_Other};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b = this.getIntent().getExtras();
		if(b!=null)
		{
		  getselectedho = b.getString("srid");
		  inspectorid = b.getString("inspectorid");
		}
		setContentView(R.layout.feedback);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		db.CreateTable(1);
		db.CreateTable(2);
		db.CreateTable(31);
		db.CreateTable(32);
		db.CreateTable(33);
		db.CreateTable(36);
		db.userid();
		
		declaration();
		show_saved_data();
	}
 
	private void declaration() {
		// TODO Auto-generated method stub
		//sp[0]=(Spinner) findViewById(R.id.feedback_SPwhowas);
		sp[1]=(Spinner) findViewById(R.id.feedback_SPsupplement);
		sp[2]=(Spinner) findViewById(R.id.feedback_SPoffice);
		
		ed_others[0]=(EditText) findViewById(R.id.feedback_EDwhowas_other);
		ed_others[1]=(EditText) findViewById(R.id.feedback_EDsupplement_other);
		ed_others[2]=(EditText) findViewById(R.id.feedback_EDoffice_other);
		comments=(EditText) findViewById(R.id.feedback_EDcomment);
	
		ArrayAdapter ad =new ArrayAdapter(this,android.R.layout.simple_spinner_item,array_who);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ArrayAdapter ad1 =new ArrayAdapter(this,android.R.layout.simple_spinner_item,array_doc);
		ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//sp[0].setAdapter(ad);
		sp[1].setAdapter(ad1);
		sp[2].setAdapter(ad1);
		/*sp[0].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[0].getSelectedItem().toString().equals("Other"))
				{
					ed_others[0].setVisibility(View.VISIBLE);
					ed_others[0].setTag("required");
				}
				else
				{
					ed_others[0].setText("");
					ed_others[0].setVisibility(View.GONE);
					ed_others[0].setTag("");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});*/
		sp[1].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[1].getSelectedItem().toString().equals("Other Information"))
				{
					ed_others[1].setVisibility(View.VISIBLE);
				}
				else
				{
					ed_others[1].setText("");
					ed_others[1].setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		sp[2].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[2].getSelectedItem().toString().equals("Other Information"))
				{
					ed_others[2].setVisibility(View.VISIBLE);
				}
				else
				{
					ed_others[2].setText("");
					ed_others[2].setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		sup_l=(LinearLayout)findViewById(R.id.feedback_LLsupplement_dy_list);
		off_l=(LinearLayout)findViewById(R.id.feedback_LLoffice_dy_list);
		rg[0]=(RadioGroup) findViewById(R.id.feedback_RGcustomerservice);rg[0].setOnCheckedChangeListener(new checklistenetr(1));
		rg[1]=(RadioGroup) findViewById(R.id.feedback_Insowner);rg[1].setOnCheckedChangeListener(new checklistenetr(2));
		rg[2]=(RadioGroup) findViewById(R.id.feedback_Manowner);rg[2].setOnCheckedChangeListener(new checklistenetr(3));
		
		 for(int i=0;i<4;i++)
    	 {
    		try{
    			cbwhowas[i] = (CheckBox)findViewById(cbwhowasid[i]);
    		}
    		catch (Exception e) {
				// TODO: handle exception
			}
    	 }
		
		((RadioButton)rg[1].findViewWithTag("Not Applicable")).setChecked(true);
		((RadioButton)rg[2].findViewWithTag("Not Applicable")).setChecked(true);
		
		try
		{
			 Cursor cur = db.hi_db.rawQuery("select * from " + db.CSE + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+getselectedho+"'",null);
			 if(cur.getCount()>0)
			 {
				 csebool=true;
				 if(csebool)
				 {
					 try{
					 rg[0].clearCheck();
					 }
					 catch (Exception e) {
						// TODO: handle exception
					}
				 }((RadioButton)rg[0].findViewWithTag("Yes")).setChecked(true);
			 }
			 else
			 {
				 ((RadioButton)rg[0].findViewWithTag("No")).setChecked(true);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		comments.setOnTouchListener(new TouchFoucs(comments));
		comments.addTextChangedListener(new TextWatchLimit(comments, 500, ((TextView) findViewById(R.id.feedback_TVcommentslimit))));
		cf.setTouchListener(findViewById(R.id.content)); 
		
		Cursor c1 = db.hi_db.rawQuery("select * from " + db.FeedBackInfo+ " WHERE FD_SRID='"+getselectedho+"'", null);
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			String csc,who,who_other,insupaperwork,manuinfo,comments;
			csc=db.decode(c1.getString(c1.getColumnIndex("FD_CSC")));
			who=db.decode(c1.getString(c1.getColumnIndex("FD_whowas")));
			who_other=db.decode(c1.getString(c1.getColumnIndex("FD_whowas_other")));
			insupaperwork=db.decode(c1.getString(c1.getColumnIndex("FD_insupaperwork")));
			manuinfo=db.decode(c1.getString(c1.getColumnIndex("FD_manuinfo")));
			comments=db.decode(c1.getString(c1.getColumnIndex("FD_comments")));
			cf.setvaluerd(rg[0], csc);
			cf.setvaluerd(rg[1], insupaperwork);
			cf.setvaluerd(rg[2], manuinfo);
			//sp[0].setSelection(ad.getPosition(who));
			ed_others[0].setText(who_other);
			this.comments.setText(comments);
			((EditText) findViewById(R.id.attendees)).setText(db.decode(c1.getString(c1.getColumnIndex("FD_Attendees"))));
			
		}
		
		show_saved_data();
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		 this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						csebool=false;
						csetext= checkedRadioButton.getText().toString().trim();
					break;
						
					case 2:
						insupaperworktxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					case 3:
						manufactinfotxt= checkedRadioButton.getText().toString().trim();
					 break;
					 
					
					 
		          }
		        }
		}
	 }
	public void clicker(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.feedback_Other:
			if(cbwhowas[3].isChecked())
			{
				ed_others[0].setVisibility(v.VISIBLE);
			}
			else
			{
				ed_others[0].setText("");who_other="";
				ed_others[0].setVisibility(v.GONE);
			}
			break;
		
			case R.id.btn_back:
				Intent i = new Intent(Feedback.this,StartInspection.class);
				//i.putExtra("selectedid", getselectedho);
				i.putExtra("inspectorid", db.UserId);
				startActivity(i);
				finish();
			break;
		  	case R.id.home:
		  		Intent homepage = new Intent(Feedback.this,HomeScreen.class);
				startActivity(homepage);
				finish();
		    	break;
		  	case R.id.feedback_BToffice_clear:
		    	Clear_all_docu(1);
			break;
		 	case R.id.feedback_BTsupplement_clear:
		    	Clear_all_docu(0);
			break;
		  	case R.id.feedback_BTsupplement_browse:
				if(sp[1].getSelectedItemPosition()!=0)
				{
					if(!sp[1].getSelectedItem().toString().trim().equals("Other Information") || !ed_others[1].getText().toString().trim().equals(""))
					{
						Intent reptoit1 = new Intent(this,Select_phots.class);
						reptoit1.putExtra("Selectedvalue",""); /**Send the already selected image **/
						reptoit1.putExtra("office_use","false"); /**Send the is for office use **/
						reptoit1.putExtra("Db_count",0); /**SendDatabase count ofr the image order**/
						reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
						reptoit1.putExtra("Total_Maximumcount", 0); /***Total count of image we need to accept**/
						reptoit1.putExtra("unlimeted", "true"); /***Total count of image we need to accept**/
						reptoit1.putExtra("PDF", "true"); /***Total count of image we need to accept**/
						startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
					//	finish();
					}
					else
					{
						cf.DisplayToast("Please enter the Other information for the supplemental document");
					}
					
				}
				else
				{
					cf.DisplayToast("Please select supplemental document title");
				}
			break;
			case R.id.feedback_BToffice_browse:
				if(sp[2].getSelectedItemPosition()!=0)
				{
					if(!sp[2].getSelectedItem().toString().trim().equals("Other Information") || !ed_others[2].getText().toString().trim().equals(""))
					{
						Intent reptoit1 = new Intent(this,Select_phots.class);
						reptoit1.putExtra("Selectedvalue",""); /**Send the already selected image **/
						reptoit1.putExtra("office_use","false"); /**Send the is for office use **/
						reptoit1.putExtra("Db_count",0); /**SendDatabase count ofr the image order**/
						reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
						reptoit1.putExtra("Total_Maximumcount", 0); /***Total count of image we need to accept**/
						reptoit1.putExtra("unlimeted", "true"); /***Total count of image we need to accept**/
						reptoit1.putExtra("PDF", "true"); /***Total count of image we need to accept**/
						startActivityForResult(reptoit1,122); /** Call the Select image page in the idma application  image ***/
						//finish();
					}
					else
					{
						cf.DisplayToast("Please enter the Other information for the office document");
					}
					
				}
				else
				{
					cf.DisplayToast("Please select office document title");
				}
			break;
			case R.id.clear:
				//sp[0].setSelection(0);
				rg[0].clearCheck();
				rg[1].clearCheck();
				comments.setText("");
				break;
			case R.id.save:
				System.out.println("fdfdfd");
				save_values();
			break;
		}
	}
	private void Clear_all_docu(final int i) {
		// TODO Auto-generated method stub
		 String s="",offuse="0";
		if(i==0)
		{
		 s="Supplemental Document";
		 
		}
		else if(i==1)
		{
			 s="For Office Document";
			 offuse="1";
			 
		}
		final String of_u=offuse;
		AlertDialog.Builder b =new Builder(Feedback.this); 
		b.setIcon(R.drawable.alertmsg);
		b.setTitle("Confirmation");
		b.setMessage("Are you sure? Do you want to delete all the documents in "+s+"?");
		b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			  
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
			
				db.hi_db.execSQL("DELETE  FROM " + db.FeedBackDocument + " WHERE FD_D_SRID='" + getselectedho + "' and " +
						"FD_D_type='"+of_u+"'");
				show_saved_data();
				
			}
		});
		b.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		AlertDialog a=b.create();
		a.show();
	}
	
	private void save_values() {
		// TODO Auto-generated method stub
		whowas= cf.getselected_chk(cbwhowas);
		/*if(validate(rg[0], "Customer service evaluation form completed and signed by owner / representative"))
		{
			 if(validate_sp(sp[0], "Who was present at inspection?",array_who[0]))
			 {
				if(validate_other(ed_others[0], "Who was present at inspection?", "Other"))
				{
					if(validate(rg[1], "Did Homeowner have Insurance Paperwork available?"))
					{
						if(validate(rg[2], "Did Homeowner have Manufacture Information available?"))
						{*/
							Cursor c1 = db.hi_db.rawQuery("select * from " + db.FeedBackInfo+ " WHERE FD_SRID='"+getselectedho+"'", null);
							System.out.println("select * from " + db.FeedBackInfo+ " WHERE FD_SRID='"+getselectedho+"'"+c1.getCount());
							String who,who_other,csc,insupaperwork,manuinfo,comments;
						
							
							//who=db.encode(sp[0].getSelectedItem().toString().trim());
							who_other=db.encode(ed_others[0].getText().toString().trim());
							
							
							
							if(!csetext.equals(""))
							{
								csc =db.encode(((RadioButton) rg[0].findViewById((rg[0].getCheckedRadioButtonId()))).getText().toString().trim());
							}
							else
							{
								csc="";
							}
							
							if(!insupaperworktxt.equals(""))
							{
								insupaperwork =db.encode(((RadioButton) rg[1].findViewById((rg[1].getCheckedRadioButtonId()))).getText().toString().trim());
							}
							else
							{
								insupaperwork="";
							}
							
							if(!manufactinfotxt.equals(""))
							{
								manuinfo=db.encode(((RadioButton) rg[2].findViewById((rg[2].getCheckedRadioButtonId()))).getText().toString().trim());
							}
							else
							{
								manuinfo="";
							}

							
							comments=db.encode(this.comments.getText().toString().trim());
							if(c1.getCount()>0)
							{
								System.out.println("UPDATE "+db.FeedBackInfo+" SET FD_CSC='"+csc+"',FD_whowas='"+whowas+"',FD_whowas_other='"+who_other+"',FD_insupaperwork='"+insupaperwork+"',FD_manuinfo='"+manuinfo+"',FD_Attendees='"+db.encode(((EditText)findViewById(R.id.attendees)).getText().toString())+"',FD_comments='"+comments+"' WHERE FD_SRID='"+getselectedho+"'");
								db.hi_db.execSQL("UPDATE "+db.FeedBackInfo+" SET FD_CSC='"+csc+"',FD_whowas='"+whowas+"',FD_whowas_other='"+who_other+"',FD_insupaperwork='"+insupaperwork+"',FD_manuinfo='"+manuinfo+"',FD_Attendees='"+db.encode(((EditText)findViewById(R.id.attendees)).getText().toString())+"',FD_comments='"+comments+"' WHERE FD_SRID='"+getselectedho+"'");
							}
							else
							{
								System.out.println(" INSERT INTO "+db.FeedBackInfo+" (FD_InspectorId,FD_SRID,FD_CSC,FD_whowas,FD_whowas_other,FD_insupaperwork,FD_manuinfo,FD_Attendees,FD_comments) VALUES" +
										" ('"+db.UserId+"','"+getselectedho+"','"+csc+"','"+whowas+"','"+who_other+"','"+insupaperwork+"','"+manuinfo+"','"+db.encode(((EditText)findViewById(R.id.attendees)).getText().toString())+"','"+comments+"')");
								db.hi_db.execSQL(" INSERT INTO "+db.FeedBackInfo+" (FD_InspectorId,FD_SRID,FD_CSC,FD_whowas,FD_whowas_other,FD_insupaperwork,FD_manuinfo,FD_Attendees,FD_comments) VALUES" +
										" ('"+db.UserId+"','"+getselectedho+"','"+csc+"','"+whowas+"','"+who_other+"','"+insupaperwork+"','"+manuinfo+"','"+db.encode(((EditText)findViewById(R.id.attendees)).getText().toString())+"','"+comments+"')");
							}
							cf.DisplayToast("Feedback information saved successfully ");
							/*Intent in =new Intent(Feedback.this,OverallComments.class);System.out.println("cf.hom"+getselectedho);
							in.putExtra("homeid", cf.Homeid);
							in.putExtra("status", cf.status);
							startActivity(in);
							finish();*/
						/*}
					}	
				}
			}
		}*/
	}
	public boolean validate_other(EditText ed,String title,String option_other)
	{
		
		if(ed.getTag()!=null)
		{
			String validationstring=ed.getTag().toString().trim();
			if(validationstring.contains("required"))
			{
				if(ed.getText().toString().trim().equals(""))
				{
					cf.DisplayToast("Please enter the Other text for "+title);
					ed.requestFocus();
					return false;
				}
			
			}
		}
		return true;
	}

	public boolean validate_sp(Spinner sp,String title,String defaults) {
		// TODO Auto-generated method stub
		if(sp.getTag()!=null)
		{
			String validationstring=sp.getTag().toString().trim();
			System.out.println("validation string "+validationstring);
			if(validationstring.contains("required"))
			{
				if(sp.getSelectedItem().toString().equals(defaults))
				{
					 cf.DisplayToast("Please select value for "+title);
					return false;
				}
			}
		}
		
		return true;
	}
	public boolean validate(RadioGroup radioGroup, String title) {
		// TODO Auto-generated method stub
		System.out.println("radioGroup.getChildCount()"+radioGroup.getChildCount());
		for (int i=0;i<radioGroup.getChildCount();i++)
		{
			System.out.println("inside fror lppe");
			if(((RadioButton)radioGroup.getChildAt(i)).isChecked()){
				
				return true;
			}
		}
		 cf.DisplayToast("Please select value for "+title);
		 return false;
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK && requestCode!=loadcomment_code)
		{ int type=0; 
			String title="";
			String title_other="";
			if(requestCode==121)
			{
				type=0;
				System.out.println("sp11="+sp[1].getSelectedItem().toString());
				if(sp[1].getSelectedItem().toString().trim().equals("Other Information"))
				{
					title=db.encode(ed_others[1].getText().toString().trim());	
				}
				else
				{
					title=db.encode(sp[1].getSelectedItem().toString().trim());
				}
				sp[1].setSelection(0);
			}
			else if(requestCode==122)
			{
				type=1;
				System.out.println("sp22="+sp[2].getSelectedItem().toString());
				if(sp[2].getSelectedItem().toString().trim().equals("Other Information"))
				{
					title=db.encode(ed_others[2].getText().toString().trim());	
				}
				else
				{
					title=db.encode(sp[2].getSelectedItem().toString().trim());
				}
				sp[2].setSelection(0);
			}
			  String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
				for(int i=0;i<value.length;i++ )
				{
					System.out.println(" INSERT INTO "+db.FeedBackDocument+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
							"('"+db.UserId+"','"+getselectedho+"','"+title+"','','"+db.encode(value[i])+"','"+type+"') ");
					
					db.hi_db.execSQL(" INSERT INTO "+db.FeedBackDocument+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
							"('"+db.UserId+"','"+getselectedho+"','"+title+"','','"+db.encode(value[i])+"','"+type+"') ");
				}
				show_saved_data();
		}
			
		
	}
	private void show_saved_data() {
		// TODO Auto-generated method stub
		Cursor c = db.hi_db.rawQuery("select * from " + db.FeedBackDocument+ " WHERE FD_D_SRID='"+getselectedho+"'", null);
		sup_l.removeAllViews();
		off_l.removeAllViews();
		findViewById(R.id.feedback_LLsupplement_dy_main).setVisibility(View.GONE);
		findViewById(R.id.feedback_LLoffice_dy_main).setVisibility(View.GONE);
		if(c.getCount()>0)
		{
			
			c.moveToFirst();
			do{
				String path,doc_tit,doc_tit_other;
				int id,type;
				path=db.decode(c.getString(c.getColumnIndex("FD_D_path")));
				doc_tit=db.decode(c.getString(c.getColumnIndex("FD_D_doctit")));
				id=c.getInt(c.getColumnIndex("FD_D_Id"));
				type=c.getInt(c.getColumnIndex("FD_D_type"));
				LinearLayout li=new LinearLayout(this);
				ImageView im =new ImageView(this);
				im.setImageDrawable(getResources().getDrawable(R.drawable.allfilesicon));
				li.addView(im);
				TextView tv =new TextView(this);
				tv.setTag(id);
				tv.setText(doc_tit);
				li.addView(tv,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				li.setPadding(0, 5, 10, 5);
				li.setOnClickListener(new my_dyclicker(id,path,doc_tit));
				li.setGravity(Gravity.CENTER_VERTICAL);
				System.out.println("type="+type);
				if(type==0)
				{
					findViewById(R.id.feedback_LLsupplement_dy_main).setVisibility(View.VISIBLE);
						sup_l.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
						View v=new View(this);
						v.setBackgroundColor(getResources().getColor(R.color.main_menu));
						sup_l.addView(v,LayoutParams.FILL_PARENT,2);
						 findViewById(R.id.feedback_BTsupplement_clear).setVisibility(View.VISIBLE);  
					
				}
				else if(type==1)
				{
					findViewById(R.id.feedback_LLoffice_dy_main).setVisibility(View.VISIBLE);
					off_l.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
					View v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.main_menu));
					off_l.addView(v,LayoutParams.FILL_PARENT,2);
					findViewById(R.id.feedback_BToffice_clear).setVisibility(View.VISIBLE);
					
				}
				else
				{
					cf.DisplayToast("Comes in the wrong place");
					
				}
			}while (c.moveToNext());
		}
			
	}
	class my_dyclicker implements OnClickListener
	{
		int doc_id;
		String path;
		String title;
		 int currnet_rotated;
		 Bitmap rotated_b;
		 View v2;

		my_dyclicker(int id,String path,String title)
		{
			this.doc_id=id;
			this.path=path;
			this.title=title;
			
			
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			 final Dialog dialog = new Dialog(Feedback.this);
	            dialog.getWindow().setContentView(R.layout.maindialog);
	            dialog.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+title+"</font>"));//.pri
	            
	            dialog.setCancelable(true);
	            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
	            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
	            ed1.setVisibility(v2.GONE);img.setVisibility(v2.GONE);
	            
	            Button button_close = (Button) dialog.findViewById(R.id.Button01);
	    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
	    		
	    		
	            Button button_d = (Button) dialog.findViewById(R.id.Button03);
	    		button_d.setText("Delete");
	    		button_d.setVisibility(v2.VISIBLE);
	    		
	    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
	    		button_view.setText("View");
	    		button_view.setVisibility(v2.VISIBLE);
	    		
	    		
	    		final Button button_saveimage= (Button) dialog.findViewById(R.id.Button05);
	    		button_saveimage.setVisibility(v2.GONE);
	    		final LinearLayout linrotimage = (LinearLayout) dialog.findViewById(R.id.linrotation);
	    		linrotimage.setVisibility(v2.GONE);
	    		
	    		final Button rotateleft= (Button) dialog.findViewById(R.id.rotateleft);
	    		rotateleft.setVisibility(v2.GONE);
	    		final Button rotateright= (Button) dialog.findViewById(R.id.rotateright);
	    		rotateright.setVisibility(v2.GONE);
	     		
	    		button_d.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            
		            AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
		   			builder.setMessage("Are you sure? Do you want to delete the selected document?")
		   				.setTitle("Confirmation")
		   				.setIcon(R.drawable.alertmsg)
	   			       .setCancelable(false)
	   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	   			           public void onClick(DialogInterface dialog, int id) {
	   			        	 db.hi_db.execSQL("Delete From " +  db.FeedBackDocument + "  WHERE FD_D_Id ='" + doc_id + "'");
	   			        	cf.DisplayToast("Document has been deleted sucessfully");
	   					   show_saved_data();
	   			           }
	   			       })
	   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
	   			           public void onClick(DialogInterface dialog, int id) {
	   			                dialog.cancel();
	   			           }
	   			       });
	   			 builder.show();
	            
	   			 dialog.cancel(); 
            }
		           	
		  	 });
	    		
	    		
	    		
	    		button_view.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            currnet_rotated=0;
		            	if(path.endsWith(".pdf"))
					     {
		            		img.setVisibility(v2.GONE);
		            		String tempstr ;
		            		if(path.contains("file://"))
		    		    	{
		    		    		 tempstr = path.replace("file://","");		    		
		    		    	}
		            		else
		            		{
		            			tempstr = path;
		            		}
		            		 File file = new File(tempstr);
						 
			                 if (file.exists()) {
			                	Uri path = Uri.fromFile(file);
			                    Intent intent = new Intent(Intent.ACTION_VIEW);
			                    intent.setDataAndType(path, "application/pdf");
			                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			  
			                    try {
			                        startActivity(intent);
			                       // finish();
			                    } 
			                    catch (ActivityNotFoundException e) {
			                    	cf.DisplayToast("No application available to view PDF");
			                       
			                    }
			               
			                }
					     }
		            	else  
		            	{
		            		Bitmap bitmap2=cf.ShrinkBitmap(path,250,250);
		            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2); 
	    					img.setImageDrawable(bmd2);
	    					img.setVisibility(v2.VISIBLE);
	    					linrotimage.setVisibility(v2.VISIBLE);
	    					rotateleft.setVisibility(v2.VISIBLE);
	    					rotateright.setVisibility(v2.VISIBLE);
				            button_view.setVisibility(v2.GONE);
				            button_saveimage.setVisibility(v2.VISIBLE);
				           
		            		
		            	}
		            }
	    		});
	    		rotateleft.setOnClickListener(new OnClickListener() {  			
	    			
					public void onClick(View v) {

	    				// TODO Auto-generated method stub
	    			
	    				System.gc();
	    				currnet_rotated-=90;
	    				if(currnet_rotated<0)
	    				{
	    					currnet_rotated=270;
	    				}

	    				
	    				Bitmap myImg;
	    				try {
	    					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
	    					Matrix matrix =new Matrix();
	    					matrix.reset();
	    					//matrix.setRotate(currnet_rotated);
	    					
	    					matrix.postRotate(currnet_rotated);
	    					
	    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
	    					        matrix, true);
	    					 
	    					 img.setImageBitmap(rotated_b);

	    				} catch (FileNotFoundException e) {
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				}
	    				catch (Exception e) {
	    					
	    				}
	    				catch (OutOfMemoryError e) {
	    					
	    					System.gc();
	    					try {
	    						myImg=null;
	    						System.gc();
	    						Matrix matrix =new Matrix();
	    						matrix.reset();
	    						//matrix.setRotate(currnet_rotated);
	    						matrix.postRotate(currnet_rotated);
	    						myImg= cf.ShrinkBitmap(path, 800, 800);
	    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
	    						System.gc();
	    						img.setImageBitmap(rotated_b); 

	    					} catch (Exception e1) {
	    						// TODO Auto-generated catch block
	    						e1.printStackTrace();
	    					}
	    					 catch (OutOfMemoryError e1) {
	    							// TODO Auto-generated catch block
	    						 cf.DisplayToast("You cannot rotate this image. Image size is too large");
	    					}
	    				}

	    			
	    			}
	    		});
	    		rotateright.setOnClickListener(new OnClickListener() {
	    		    public void onClick(View v) {
	    		    	
	    		    	currnet_rotated+=90;
	    				if(currnet_rotated>=360)
	    				{
	    					currnet_rotated=0;
	    				}
	    				
	    				Bitmap myImg;
	    				try {
	    					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
	    					Matrix matrix =new Matrix();
	    					matrix.reset();
	    					//matrix.setRotate(currnet_rotated);
	    					
	    					matrix.postRotate(currnet_rotated);
	    					
	    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
	    					        matrix, true);
	    					 
	    					 img.setImageBitmap(rotated_b);  

	    				} catch (FileNotFoundException e) {
	    					//.println("FileNotFoundException "+e.getMessage()); 
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				}
	    				catch(Exception e){}
	    				catch (OutOfMemoryError e) {
	    					//.println("comes in to out ot mem exception");
	    					System.gc();
	    					try {
	    						myImg=null;
	    						System.gc();
	    						Matrix matrix =new Matrix();
	    						matrix.reset();
	    						//matrix.setRotate(currnet_rotated);
	    						matrix.postRotate(currnet_rotated);
	    						myImg= cf.ShrinkBitmap(path, 800, 800);
	    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
	    						System.gc();
	    						img.setImageBitmap(rotated_b); 

	    					} catch (Exception e1) {
	    						// TODO Auto-generated catch block
	    						e1.printStackTrace();
	    					}
	    					 catch (OutOfMemoryError e1) {
	    							// TODO Auto-generated catch block
	    						 cf.DisplayToast("You cannot rotate this image. Image size is too large");
	    					}
	    				}

	    		    }
	    		});
	    		button_saveimage.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						
						if(currnet_rotated>0)
						{ 

							try
							{
								/**Create the new image with the rotation **/
						
								String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
								 ContentValues values = new ContentValues();
								  values.put(MediaStore.Images.Media.ORIENTATION, 0);
								  Feedback.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
								
								if(current!=null)
								{
								String path_new=getPath(Uri.parse(current));
								File fout = new File(path);
								fout.delete();
								/** delete the selected image **/
								File fin = new File(path_new);
								/** move the newly created image in the slected image pathe ***/
								fin.renameTo(new File(path));
								cf.DisplayToast("Saved successfully");dialog.cancel();
								show_saved_data();
								
								
							}
							} catch(Exception e)
							{
								//.println("Error occure while rotate the image "+e.getMessage());
							}
							
						}
						else
						{
							cf.DisplayToast("Saved successfully");
							dialog.cancel();
							show_saved_data();
						}
						
					}
				});
	    		button_close.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            	dialog.cancel();
		            }
	    		});
	    		dialog.show();
			
		}
		
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = this.managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	@Override
    protected void onSaveInstanceState(Bundle outState) {	 
	  
        outState.putString("inspectorid", inspectorid);
        
        
        super.onSaveInstanceState(outState);
    }
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	inspectorid = savedInstanceState.getString("inspectorid");
    	 
        super.onRestoreInstanceState(savedInstanceState);
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(Feedback.this, StartInspection.class);
			System.out.println("inspectorid="+inspectorid);
			i.putExtra("inspectorid",inspectorid);
			startActivity(i);
			finish();
			
		}

		return super.onKeyDown(keyCode, event);
	}
}