package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.GraphicsView;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

public class Piaform extends Activity {
	DatabaseFunction db;
	CommonFunction cf;
	String inspectorid="",srid="",phfirstname="",phlastname="",phaddress="",phcity="",phfee="",
			phstate="",phcounty="",phzip="",phdate="";
	ScrollView parentscr;
	GraphicsView initialview;
	BitmapDrawable bmdregister;
	boolean initialbool = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 db = new DatabaseFunction(this);
		 cf = new CommonFunction(this);
		
		 
		 Bundle b = this.getIntent().getExtras();
		 if(b!=null)
		 {
		   inspectorid = b.getString("inspid");
		   srid = b.getString("srid");
		 }
		 setContentView(R.layout.activity_pia);
		 
		 Button btnback = (Button)findViewById(R.id.btn_back);
		 btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
				
			}
		});
		 db.userid();
		 db.CreateTable(28);
		 try
		 {
			 Cursor cur = db.hi_db.rawQuery("select * from " + db.OrderInspection + " where fld_orderedid='"+srid+"'",null);
			 cur.moveToFirst();
			 if (cur.getCount() >= 1) {
				phfirstname = db.decode(cur.getString(cur.getColumnIndex("fld_firstname")));
				phlastname = db.decode(cur.getString(cur.getColumnIndex("fld_lastname")));
				phaddress += db.decode(cur.getString(cur.getColumnIndex("fld_inspectionaddress1")));
				phaddress += ","+db.decode(cur.getString(cur.getColumnIndex("fld_inspectioncity")));
				phaddress += ","+db.decode(cur.getString(cur.getColumnIndex("fld_inspectionstate")));
				phaddress += ","+db.decode(cur.getString(cur.getColumnIndex("fld_inspectioncounty")));
				phaddress += ","+db.decode(cur.getString(cur.getColumnIndex("fld_zip")));
				phfee = cur.getString(cur.getColumnIndex("fld_inspectionfee"));
				phdate = cur.getString(cur.getColumnIndex("fld_inspectiondate"));
				
				if(phaddress.contains(",,")||phaddress.contains(",--Select--,"))
				{
					phaddress = phaddress.replace(",,", ",");
					phaddress = phaddress.replace(",--Select--,", "");
				}
				
			 }
		 }
		 catch (Exception e) {
			// TODO: handle exception
		}
		 TextView txtnote = (TextView)findViewById(R.id.note);
		 String source = "This agreement is made between  "+db.Companyname+" (herein referred to as the \"Company\") and <b><u>"+phfirstname+" " +phlastname+"</u></b> " +
		 		"(herein referred to as \"Customer\") regarding a building (the \"Building\") to be inspected located at <b><u>"+phaddress+""
				 +"</u></b>. The Company agrees to perform an inspection of the Building for the purpose of alerting the Customer "
				 +"to major deficiencies in its condition. A report (the \"Report\") containing the inspection's findings will be prepared by the Company "
				 +"and provided to the Customer for its sole, exclusive and confidential use. The Company will perform its inspection in accordance with "
				 +"the Standards of Practice of the American Society of Home Inspectors. Minor or cosmetic defects will not be reported."+"<br/><br/>The inspection "
				 +"fee is due and payable upon presentation of the Report and is based on a single visit to the Building. Additional fees may be charged "
				 +"for subsequent visits required by the Customer or, if the inspector conducting the inspection is called upon to prepare for litigation, "
				 +"give testimony as a result of his inspection, or the like, such additional services are beyond the scope of this Agreement. The inspection "
				 +"will be conducted only on visible and accessible areas and components of the Building, and is limited to the apparent condition of the Building "
				 +"on the date of the inspection. Not all conditions may be apparent on the inspection date due to weather conditions, inoperable systems, inaccessibilty, "
				 +"and the like. Conditions may exist which remain undiscovered. While the inspection reduuces the risk of purchasing property, it does not elimate such risk. "
				 +"<br/><br/> The Company is not responsible for the failure to discover latent defects or for problems which occur or become evident after the inspection time. "
				 +"No invasive or destructive testing will  be made. No equipment, systems or appliances will be dismantled. The moisture content of all walls, floors, ceilings, "
				 +"siding, and the like will not be tested. As to certain conditions, only random sampling will be conducted. The inspection report will not address the presence "
				 +"of radon gas, lead paint, asbestos, urea formaldehyde, carbon monoxide or any toxic or potentially harmful or flammable chemicals, the well system, septic "
				 +"tank or other buried drainage or storage systems, the secuirty system, the central vaccum systems, water softeners or treatment services, fire sprinkler systems, "
				 +"the presence of rodents, termites, wood-boring insects, ants, birds or other infestation. Neither this Agreement nor the report constitutes or should be construed to be:"
				 +"<br/><br/>(a) a compliance inspection with respect to any code, standard or regulation;"
				 +"<br/><br/>(b) a guarantee, warranty of policy of insurance;"
				 +"<br/><br/>(c) a survey, appraisal or flood plain certification;"
				 +"<br/><br/>(d) a wood-destroying organism report;"
				 +"<br/><br/>(e) an opinion regarding the condition of title, zoning or compliance with restrictive covenants;"
				 +"<br/><br/>(f) an environmental, mold, moisture or engineering analysis."
				 +"<br/><br/>The Customer may wish to seek other device or recommendations from appropriate professionals regarding the foregoing, conditions revealed in  the Report, and areas "
				 +"excluded from  the scope of the inspcetion. The Company assumes no liability for the cost of repair or replacement of unreported defects or deficiencies either current or "
				 +"arising in the future. The Company's liability for mistakes or omissions in the conduct of this inspection and its Report Is limited to the refund of the fee paid. This "
				 +"limitation of liability is binding upon the customer, its heris, successors and assigns, and all other parties claiming by or through the Customer."
				 +"<br/><br/>This is the entire agreement of the parties regarding these matters. Any modification or amendment to this Agreement must be in writing and signed by the affected "
				 +"party. In the event any portion of this Agreement is determined to be unenforceable, the remainder of it will continue in full force and effect. "
				 +"<br/><br/>This Agreement is binding upon and available to the heirs, successors and, to the extent permitted hereunder, the assigns of each of the parties."
				 +"<br/><br/>Any controversy of claim between the parties arising out of or relating to the interpretation of this Agreement, the services rendered hereunder or any "
				 +"other matter pertaining to this Agreement will be submitted in accordance with the applicable rules of the American Arbitration Association. The parties shall "
				 +"mutually appoint an arbitrator who is knowledgeable and familiar with the professional home inspection industry. Judgement on any award may be entered in any "
				 +"court having jurisdiction and the arbitration decision shall be binding on all parties. Secondary or consequential damages are specifically excluded. "
				 +"All claims must be presented within one year from the date of inspection. The Company is not liable for any claim presented more than one year after the date of "
				 +"inspection. In the event the Cusomer commences an arbitration and is unsuccessful in it, the customer will bear all of the company's expenses incurred in "
				 +"connection therewith including, but not limited to, attorney's fees and reasonable fee to the employees of the Company to investigate, prepare for, and attend "
				 +"any proceeding or examination. Customer may not present or pursue any claim against the Company until(1) written notice of the defect or omission is provided to "
				 +"the Company and (2) the Company is provided access to and the opportunity to cure the defect."
				 +"<br/><br/>I have been encouraged to participate in the inspection/survey and accept responsibility for incomplete information should I not participate in the "
				 +"inspection. My participation shall be at my own risk for falls, injuries, property damage, etc. I accept that this work is no substitute for a pre-settlement "
				 +"inspection for which I am responsible since damages, mechanical failures, and symptoms, cures, etc. may appear after this work and before my legal acceptance "
				 +"of the property. I waive all claims against the inspector/surveyor of company in the absence of diligently performing my pre-settlement inspection and for lack "
				 +"of more extensive investigation and follow through with a specialist on any problems noted including confirmation of any cost approximations.";
				 
		 txtnote.setText(Html.fromHtml(source));
		 
		 TextView txtnote1 = (TextView)findViewById(R.id.note1);
		 String source1 = "The customer requests the standard visual inspection of the readily accessible areas of the structure. The inspection is limited to visual observation "
				  +"existing at the time of inspection. The customer agrees and understands that the maximum liability incurred by The Inspector / The Company for errors and "
				  +"omissions in the Inspection shall be limited to the fee paid for the inspection.";
		 txtnote1.setText(Html.fromHtml(source1));
		 
		 TextView txtnote2 = (TextView)findViewById(R.id.note2);
		 String source2 ="The customer requests the technically exhaustive inspection without a limit of liability. The company agrees to retain other specialists as necessary "
				 +"and to prepare a technically exhaustive report within twenty days of the date of the receipt of this contract. An extension of time or purchase and sale "
				 +"contract should be agrees with sellers. This optional in-depth, intensive and technically exhaustive inspection and report fee is:";
		 txtnote2.setText(Html.fromHtml(source2));
		 
		 TextView txtfee = (TextView)findViewById(R.id.feenote);
		 
		 
		 if(phfee.equals(""))
		 {
			 phfee="";
		 }
		 else
		 {
			 phfee="$"+phfee;
		 }
		 
		 String source3 = "<b>Fee : </b>"+"<u><font color='000000'>"+phfee+"</font></u>";
		 txtfee.setText(Html.fromHtml(source3));
		 
		 TextView txtdate = (TextView)findViewById(R.id.datenote);
		 String source4 = "<b>Date of Inspection : </b>"+"<u><font color='000000'>"+phdate+"</font></u>";
		 txtdate.setText(Html.fromHtml(source4));
		 
		 Calendar c = Calendar.getInstance();
		 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
		 String strDate = sdf.format(c.getTime());
		 
		 TextView date = (TextView)findViewById(R.id.date);
		 String source5 = "<b>Date : </b>"+"<u><font color='#000000'>"+strDate+"</font></u>";
		 date.setText(Html.fromHtml(source5));
		 
		 TextView txtcustomer = (TextView)findViewById(R.id.customernote);
		 String source6 = "<b>Customer Signature : </b>"+"<br>By signing here customer declines technically exhasutive inspection"
		 +"<br><br><b>Customer Printed Name : </b>"+"<u><font color='000000'>"+phfirstname+" "+phlastname+"</font></u>"
		 +"<br><br><b>Inspected By : </b>"+"<u><font color='000000'>"+db.Userfirstname+" "+db.Userlastname+"</font>";
		 txtcustomer.setText(Html.fromHtml(source6));
		 
		 TextView txttechnical = (TextView)findViewById(R.id.technicalnote);
		 String source7 = "$5,000.00-10,000.00* Date : "+"<u><font color='000000'>"+phdate+"</font></u>"+" Customer : "+"<u><font color='000000'>"+phfirstname+" "+phlastname+"</font></u>"
		                   +"<br><br><b>Inspected By : </b>"+"<u><font color='000000'>"+db.Userfirstname+" "+db.Userlastname+"</font></u>"
		                   +"<br><br>* A 50% deposit is required to commence this Inspection.";
		 txttechnical.setText(Html.fromHtml(source7));
		 
		 parentscr = (ScrollView)findViewById(R.id.scr);			
			
		 initialview = (GraphicsView)findViewById(R.id.signature);
		 initialview.setDrawingCacheEnabled(true);
			
		 db.CreateTable(37);
		 try
			{
				 Cursor cur = db.hi_db.rawQuery("select * from " + db.PIA + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
				 if(cur.getCount()>0)
				 {
					 cur.moveToFirst();
					 
	                 String selectedImagePath = db.decode(cur.getString(cur.getColumnIndex("fld_signature")));
	                 System.out.println("selectedImagePath="+selectedImagePath);
	                 
	                 File outputFile = new File(selectedImagePath);
					 //File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/PIASignature/"+inspectorid+"/"+srid+"/"+selectedImagePath);
						System.out.println("outputFile.exists()"+outputFile.exists());
							if(outputFile.exists())
						{
							bmdregister=getbitmap(outputFile);
							if(bmdregister!=null)
							{
								initialview.setBackgroundDrawable(bmdregister);GraphicsView.s = 1;
							}		
						}
				 }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
	}
	
	private BitmapDrawable getbitmap(File OutputFile)
	{
		bmdregister=null;
		try {
			BitmapFactory.Options o1 = new BitmapFactory.Options();
			o1.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(OutputFile),null, o1);
		
		final int REQUIRED_SIZE = 200;
		int width_tmp1 = o1.outWidth, height_tmp1 = o1.outHeight;
		int scale1 = 1;
		while (true) {
			if (width_tmp1 / 2 < REQUIRED_SIZE
					|| height_tmp1 / 2 < REQUIRED_SIZE)
				break;
			width_tmp1 /= 2;
			height_tmp1 /= 2;
			scale1 *= 2;
		}
		BitmapFactory.Options o3 = new BitmapFactory.Options();
		o3.inSampleSize = scale1;
		Bitmap bitmap1 = BitmapFactory.decodeStream(new FileInputStream(
				OutputFile), null, o3);
		
		bmdregister = new BitmapDrawable(bitmap1);
		return bmdregister;
		} catch (Exception e) {
		System.out.println("catch="+e.getMessage());
			return bmdregister;
		}
	}
	protected void back() {
		// TODO Auto-generated method stub
		Intent i = new Intent(Piaform.this,StartInspection.class);
		i.putExtra("inspectorid", inspectorid);
		startActivity(i);
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			back();
			
		}

		return super.onKeyDown(keyCode, event);
	}
}
