package idsoft.idepot.homeinspection;

import java.util.ArrayList;

import idsoft.idepot.homeinspection.CreateHeating.myspinselectedlistener;
import idsoft.idepot.homeinspection.LoadBathroom.CHListener;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;

public class NewSystembrand extends Activity {
	String gettempname="",getcurrentview="",getmodulename="",gettempoption="",getselectedho="",fueltankvalue="",finalselstrval="",tempfueltankloc="",getnoofzones="",getheatbrand="",
			seltdval="",tempfluetype="",temptestedfor="",tempairhandlerfeat="",tempthermolocation="",tempthermotype="",tempductworktype="",tempconddischarge="",getzoneno="",
					getfurnace="",getmotorblower="",getsupplyregister="",getreturnregister="",getmultizonesystem="",str="",fluetypevalue="",hvacvalue="",
							conddischangevalue="",thermolocationvalue="",thermostattypevalue="",ductworktypevalue="",airhandlerfeatvalue="",testedforvalue="";
	ScrollView scr;
	TableLayout heatingtblshow;
	Spinner spinloadtemplate;
	View v;
	String arrtempname[];
	String tempname="";
	LinearLayout lin;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	ArrayAdapter adapter;
	CheckBox chknotapplicable;
	TextView fueltanklocnote,fluetypenote,testedfornote,thermostatlocationtv,airhandlerfeatnote,ductworknote,thermostattypetv,conddisctv;
	int hvac=0,hvacCurrent_select_id,editid=0,getisedit=0,spintouch=0,chk=0;
	static int k=0;
	Button btntakephoto;
	TextView fueltanktv,fluetypetv,testedfortv,airhandlertv,ductworktypetv,condensdischargetv;
	CheckBox ch[];
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	Spinner sp_heatingtype,sp_energysource,sp_gasshutoff,spin_noofzones,spin_heatingage,spin_zoneno,sp_zonename,sp_heatbrand,sp_furnace,
	sp_motorblower,sp_heatdelivery,sp_zonestestedfor,spin_supplyregister,sp_returnregister,sp_ductworktype,sp_multizonesystem,sp_zonetonnage,sp_btu,
	sp_thermotype,sp_thermolocation,sp_externalunitreplprob,sp_internalunitreplprob;
	String getheatdeliverytype="",getspinselectedname="",getenergysource="",strenergysource="",getheatingtype="",getheatingage="",strotherheatingage="",strotherheattype="",getgasshutoff="",strgasshutoff="",strfluetype="",
			getfluetype="",getzonename="",strzonename="",getzonetonnage="",strzonetonnage="",getbtu="",strbtu="",strexternalunit="",strinternalunit="",getinternalunit="",getexternalunit="";
	EditText ed_othergasshutoff,ed_otherfluetype,ed_otherheatingage,ed_otherzonename;
	
	static String[] arrfueltank={"Above Ground","Below Ground","Not Applicable","Above Ground Oil Tank","Below Ground Oil Tank","Original Oil Tank",
		"Oil Tank Fill Cap and Vent Not Located","Tank Fill and Vent Located","Portable Gas Tank","Gas Tank Below Ground","Underground Gas Tank Fill Valve Not Located",
		"Underground Gas Tank Not Located","Underground Gas Tank"},
					arrfluetype={"Unlined Masonry Chimney","Old Masonry Chimney Redundant","PVC Schedule 40","No Flue Cap","Metal Flue Cap","Flexible Metal Gas Flue","Masonry Chimney",
				"Clay Flue Liner","Metal Flue"},
		arrductworktype={"Flexible Ductwork","Metal Ductwork","Slab Ductwork","Insulated","Ductwork in Crawl SPace","Automatic Dampers(not tested)","Untidy Ductwork Installation Leakage Evident"},
		arrzonetestingfor={"Tested For Cooling","Tested For Heating","Tested For Emergency Heat Only","Reverse valve not tested", "Outside temperature prohibited test"},
				arrthermolocation={"Hall", "Master Bedroom","Living Room", "Upstairs","Bonus", "Other"},
				arrthermotype={"Manual Thermostat","Programmable Thermostat","Set Back Thermostat","Heat Pump Thermostat","Multiple Zone","Separate Heating/Cooling Thermostat","Aquastat"},
				arrcondendischarge={"Overflow Condensate Tray","Condensate Pipe To Sump","Condensate Pipe","Condensate Pipe To Exterior","Float Switch/Pump (not tested)"},
		arrairhandlerfeatures={"Air Filter","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner","Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light","Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Condensation Discharger"};
	
	String[] arrheatingnum={"--Select--","1","2","3","4","5","6","Other"},strheatsytem ={"--Select--","Adequate","Inadequate","Appears Functional","Recommend Replacement"},
			arrheattype={"--Select--","Forced Air","Heat Pump Forced Air","Central Air - Cool/Air Conditioning Only","Packaged Central Air - Heat pump","Packaged Central Air - Cool/Air Conditioning Only","Wall Unit(s)","Heat Strip","None",
			"Electric Base","Electric Heat","Radiant Floor","Space Heater","Steam Boiler","Circulating Boiler","Coal Furnace Converted to Gas","Coal Furnace converted to Oil",
			"Hydroninc System","Oil Furnace converted to Gas","Old Oil Furnace as Back up to heat pump","Radiant  Ceiling","Window Unit","Swamp Cooler","Water to Air system",
			"Cooling Only System","Uses Heat Ductwork","Refrigerant System","System Aged/Operable","Other"},
			arrgasshutoff={"--Select--","Front Side","Left Side","Right Side","Rear side","Adjacent Tank","Adjacent meter","Other"},
			
			
			arrzonename={"--Select--","Bonus Room","Main Area","Master Bedroom","Second Floor","Other"},
			arrcoolingage={"--Select--","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","Other"},
			arrheatexchanger={"--Select--","1 Burner","2 Burner","3 Burner",
			"4 Burner","5 Burner","6 Burner","Other"},arrdistribution={"--Select--","Hot Water","Steam","Radiator","Metal Duct","One Pipe","Two Pipe","Other"},
			arrcirculator={"--Select--","Pump","Gravity","Other"},arrdrain={"--Select--","Manual","Automatic","Other"},arrfuse={"--Select--","Single Wall","Double Wall","PVC"},
			arrdevious={"--Select--","Temp Guage","Expansion Tank","Pressure Gauge","Other"},arrhumidifier={"--Select--","Aprilaire","Honeywell","Other"},arrthermostats={"--Select--","Individual",
			"Multi Zone","Programmable","Other"},
			arrasbestos={"--Select--","Yes","No"},arrheatingnumber,arrheatingnumother,arrheatingsysteme ,arrmanufacture,arrmodelnumber,
			arrenergy={"--Select--","Oil","Gas","Electric","Water","Liquid Propane","Kerosene","Solar","Wood","Coal","Corn","Natural Gas","Diesel","Other"},
					arrheatbrand={"--Select--","Not Determined", "Not Visible","Aged","Amana","American Standard","Americanaire","Ao Smith","Apollo","Aquatherm","Arcoaire","Armstrong",
			"Bryant","Burnham","Carrier","Climate Master","Coleman","Comfort Maker","Crown","Empire","Envirotemp","Frigidaire","Goodman",
			"Hallmark","Heatwave","Heil","Int.Comfort","Intertherm","Janitrol","Kenmore","Lennox","Luxaire","Magic Chef","Monitor","Ninety Two",
			"None","Oneida","Patriot","Payne","Peerless","Presidential","Quattro","Rheem","Rinnai","Ruud","Sears","Singer","Tempstar","Thermopride",
			"Toyostove","Trane","Unknown","Weatherking","Weil Mclean","York","Zephyr"},
			arrheatdelivery={"--Select--","Forced Air","Refrigerant","Electric Resistance Heat","Hot Water","Steam Heat","Radiant","Boiler"},
			arrwatersystem={"--Select--","One Pipe System","Two Pipe System","Fin Coils","Boiler","Subfloor","Convectors","Radiators","Baseboard"},
			arrradiators={"--Select--","Cast Iron Radiators","Steel Radiators","Manual Fill","Water Level Sight Glass","Bleed Valves"},
			arrmotorblower={"--Select--","Blower Fan","Direct Drive","Belt Driven"},
			arrwoodstoves={"--Select--","1","2","3","4","5","None","Other"},
			arrsupplyregister={"--Select--","Low","High","Low & High"},
					arrreturnregister={"--Select--","Low","High"},
					arrexternalunitrepprob={"--Select--","Low","Medium","High","Other"},
					arrzonetonnage={"--Select--",	"1.5 Ton","2 Ton","2.5 Ton","3 Ton","3.5 Ton","4 Ton","4.5 Ton","5 Ton","Other"},
							arrbtu={"--Select--","Not Determined","12000 BTU's","18000 BTU's","24000 BTU's","30000 BTU's","36000 BTU's","42000 BTU's","48000 BTU's","54000 BTU's","Other"},
									arrnoofzones={"--Select--","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"},
			arrretunrregister={"--Select--","Low","High"},
				
			arrmultizonesystem={"--Select--","Yes", "No", "Auto Damper Not Tested"},
			
			arrfurnacefeatures={"--Select--","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner"," Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light"," Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Air Filter"},
			arrfurnace={"--Select--","Air To Air Heat Pump","Water To Air Heat Pump","Forced Air Furnace","Induced Air Furnace","Gravity Furnace","Evaporated Located Over Furnace","Natural Draft System","Condensation Furnace System","Inside panels sealed(no coil access)"};
	ArrayAdapter externalunitrepadap,internalunitrepadap,thermolocationadap,thermotypeadap,zonetonnadap,btuarrayadap,multizonearraydap,ductworktypearraydap,zonetestedforadap,zonenameadap,zonenoarrayadap,fluetypearrayadap,noofzonesarrayadap,gasshutoffarrayadap,heatnumarrayadap,heatsystemarrayadap,heattypearrayadap,heatexchangerarrayadap,distributionarrayadap,circulatorarrayadap,drainarrayadap,fusearrayadap,deviousarrayadap,
	watersysarrayadap,returnregarraydap,supplyregarraydap,furnfeatarraydap, humidifierarrayadap,thermostatsarrayadap,fueltankarrayadap,asbestosarrayadap,energysourcearrayadap,agearrayadap,brandarrayadap,furnacearrayadap,heatdeliveryarrayadap,radiatorsarrayadap,motorarrayadap,woodstoveadap;
	EditText et_otherheatingnum,et_manufacturer,et_modelnumber,et_serialnumber,et_capacity,et_otherheatexchanger,et_otherdistribution,et_othercirculator,
	         et_otherdrain,et_otherdevious,et_otherhumidifier,et_otherthermostats,et_otherfueltank;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}				
		setContentView(R.layout.activity_newsystembrand);		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(47);
		db.CreateTable(48);

		db.userid();
		
		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/squadaone-regular.ttf"); 
		btnsubmit.setTypeface(font); 
		
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		chknotapplicable = (CheckBox) findViewById(R.id.hvacnotappl);		 
		chknotapplicable.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
	                //is chkIos checked?
			if (((CheckBox) v).isChecked())
			{
				chk=1;
				System.out.println("its checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
				
			}
			else
			{
				chk=2;
				System.out.println("its not checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
				
				
			}
	 
		  }
		});
		Button btnsave = (Button)findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) 
				{
					// TODO Auto-generated method stub
					savehvacnotappl(getcurrentview);
					Intent intent = new Intent(NewSystembrand.this, NewThermostat.class);
			    	Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("tempoption", gettempoption);			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("selectedid", getselectedho);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
					
				}
			});
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);
									
				}  
			}
		});
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC", null);
            if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(NewSystembrand.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							Bundle b4 = new Bundle();
							b4.putString("currentview","takephoto");			
							intent.putExtras(b4);
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getselectedho);			
							intent.putExtras(b5);
							startActivity(intent);
							finish();
			}
		});
		spin_noofzones = (Spinner)findViewById(R.id.spin_noofzones);
		noofzonesarrayadap = new ArrayAdapter<String>(NewSystembrand.this,android.R.layout.simple_spinner_item,arrnoofzones);
		noofzonesarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_noofzones.setAdapter(noofzonesarrayadap);
		spin_noofzones.setOnItemSelectedListener(new myspinselectedlistener(15));
		
		spin_zoneno = (Spinner)findViewById(R.id.spin_zoneno);
		zonenoarrayadap = new ArrayAdapter<String>(NewSystembrand.this,android.R.layout.simple_spinner_item,arrnoofzones);
		zonenoarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_zoneno.setAdapter(zonenoarrayadap);
		spin_zoneno.setOnItemSelectedListener(new myspinselectedlistener(16));
		
		
		sp_zonename = (Spinner)findViewById(R.id.spin_zonename);
		zonenameadap = new ArrayAdapter<String>(NewSystembrand.this,android.R.layout.simple_spinner_item,arrzonename);
		zonenameadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonename.setAdapter(zonenameadap);
		sp_zonename.setOnItemSelectedListener(new myspinselectedlistener(25));
		ed_otherzonename= (EditText)findViewById(R.id.other_zonename);
		
	
		
		
		sp_externalunitreplprob = (Spinner)findViewById(R.id.spin_externalunitreplprob);
		externalunitrepadap = new ArrayAdapter<String>(NewSystembrand.this,android.R.layout.simple_spinner_item,arrexternalunitrepprob);
		externalunitrepadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_externalunitreplprob.setAdapter(externalunitrepadap);
		sp_externalunitreplprob.setOnItemSelectedListener(new myspinselectedlistener(31));
		
		sp_internalunitreplprob = (Spinner)findViewById(R.id.spin_internalunitreplprob);
		internalunitrepadap = new ArrayAdapter<String>(NewSystembrand.this,android.R.layout.simple_spinner_item,arrexternalunitrepprob);
		internalunitrepadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_internalunitreplprob.setAdapter(internalunitrepadap);
		sp_internalunitreplprob.setOnItemSelectedListener(new myspinselectedlistener(32));
		
		((TextView)findViewById(R.id.modulename)).setVisibility(v.VISIBLE);
		((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		heatingtblshow= (TableLayout)findViewById(R.id.heathvactable);
		
		Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(NewSystembrand.this,HomeScreen.class));
					finish();
					 
				}
			});
		
		Button	btncancel = (Button)findViewById(R.id.btncancel);
		btncancel.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
				clearheating();
				}
			});
		
		/*TextView vc = ((TextView)findViewById(R.id.vc));
		vc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent = new Intent(NewSystembrand.this, NewSetVC.class);
					Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", "create");			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("submodulename", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("tempoption", gettempoption);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
			}
		});		
		*/
		Button save = (Button)findViewById(R.id.btnsave);
		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getzoneno.equals("--Select--"))
				{
					cf.DisplayToast("Please select the Zone No.");
				}
				else
				{
					 strzonename = ed_otherzonename.getText().toString();
					 strexternalunit = ((EditText)findViewById(R.id.other_externalunit)).getText().toString();
					 strinternalunit = ((EditText)findViewById(R.id.other_internalunit)).getText().toString();

					 if(getnoofzones.equals("--Select--")){getnoofzones="";}
					 if(getzoneno.equals("--Select--")){getzoneno="";}
					 if(getzonename.equals("--Select--")){getzonename="";}
					 if(getexternalunit.equals("--Select--")){getexternalunit="";}
					 if(getinternalunit.equals("--Select--")){getinternalunit="";}
					 
					 try
						{
						 if(getcurrentview.equals("start"))
						 {
						 
							if(((Button)findViewById(R.id.btnsave)).getText().toString().equals("Update"))
							{
								db.hi_db.execSQL("UPDATE  "+db.LoadSystembrands+" SET fld_noofzones='"+getnoofzones+"',"+
									           "fld_zoneno='"+getzoneno+"',fld_zonename='"+getzonename+"',fld_zonenameother='"+db.encode(strzonename)+"',"+
											   "fld_externalunit='"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"'," +
											   									"fld_internalunit='"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"',fld_extunitreplprob='"+getexternalunit+"'," +
											   											"fld_extunitreplprobother='"+db.encode(strexternalunit)+"',fld_intunitreplprob='"+getinternalunit+"',fld_intunitreplprobother='"+db.encode(strinternalunit)+"'," +
											   													"fld_ampextunit='"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"'," +
											   															"fld_ampintunit='"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"'," +
											   																	"fld_emerheatampdraw='"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"'," +
											   																			"fld_supplytemp='"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"'," +
											   																					"fld_returntemp='"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"',fld_systembrandcomments='"+((EditText)findViewById(R.id.ed_sysbrandcomments)).getText().toString()+"'" +
											   																							" Where z_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and  fld_zoneno='"+getzoneno+"'");
								sysbrandscommon();
							}
							else
							{
								try
								{ System.out.println("insi try");
									
										Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'", null);
										System.out.println("CCC="+cur.getCount());
										if(cur.getCount()==0)
										{
										
											
											db.hi_db.execSQL(" INSERT INTO "+db.LoadSystembrands+" (fld_templatename,fld_inspectorid,fld_srid,fld_noofzones,fld_zoneno,fld_zonename," +
													"fld_zonenameother,fld_externalunit,fld_internalunit,fld_extunitreplprob,fld_extunitreplprobother,fld_intunitreplprob," +
													"fld_intunitreplprobother,fld_ampextunit,fld_ampintunit,fld_emerheatampdraw,fld_supplytemp,fld_returntemp,fld_systembrandcomments) VALUES" +
														 "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+getnoofzones+"','"+getzoneno+"','"+getzonename+"'," +
														 		"'"+db.encode(strzonename)+"','"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"'," +
														 				"'"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"',"+
													"'"+getexternalunit+"','"+db.encode(strexternalunit)+"','"+getinternalunit+"','"+db.encode(strinternalunit)+"'," +
															"'"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"'," +
																	"'"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"'," +
																			"'"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"'," +
																					"'"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"'," +
																							"'"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"'," +
																									"'"+((EditText)findViewById(R.id.ed_sysbrandcomments)).getText().toString()+"')");
									
											
											
											
											sysbrandscommon();savehvacnotappl(getcurrentview);
										}
										else											
										{
											db.hi_db.execSQL("UPDATE  "+db.LoadSystembrands+" SET fld_noofzones='"+getnoofzones+"',"+
											           "fld_zoneno='"+getzoneno+"',fld_zonename='"+getzonename+"',fld_zonenameother='"+db.encode(strzonename)+"',"+
													   "fld_externalunit='"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"'," +
													   									"fld_internalunit='"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"',fld_extunitreplprob='"+getexternalunit+"'," +
													   											"fld_extunitreplprobother='"+db.encode(strexternalunit)+"',fld_intunitreplprob='"+getinternalunit+"',fld_intunitreplprobother='"+db.encode(strinternalunit)+"'," +
													   													"fld_ampextunit='"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"'," +
													   															"fld_ampintunit='"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"'," +
													   																	"fld_emerheatampdraw='"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"'," +
													   																			"fld_supplytemp='"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"'," +
													   																					"fld_returntemp='"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"',fld_systembrandcomments='"+((EditText)findViewById(R.id.ed_sysbrandcomments)).getText().toString()+"'" +
													   																							" Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
											sysbrandscommon();
										}
									
								}
								catch (Exception e) {
									// TODO: handle exception
									System.out.println("catch="+e.getMessage());
								}
								
							}
					   	 }
						 else
						 {
							 if(((Button)findViewById(R.id.btnsave)).getText().toString().equals("Update"))
								{
									db.hi_db.execSQL("UPDATE  "+db.CreateSystembrands+" SET fld_noofzones='"+getnoofzones+"',"+
										           "fld_zoneno='"+getzoneno+"',fld_zonename='"+getzonename+"',fld_zonenameother='"+db.encode(strzonename)+"',"+
												   "fld_externalunit='"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"'," +
												   									"fld_internalunit='"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"',fld_extunitreplprob='"+getexternalunit+"'," +
												   											"fld_extunitreplprobother='"+db.encode(strexternalunit)+"',fld_intunitreplprob='"+getinternalunit+"',fld_intunitreplprobother='"+db.encode(strinternalunit)+"'," +
												   													"fld_ampextunit='"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"'," +
												   															"fld_ampintunit='"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"'," +
												   																	"fld_emerheatampdraw='"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"'," +
												   																			"fld_supplytemp='"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"'," +
												   																					"fld_returntemp='"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"',fld_systembrandcomments='"+((EditText)findViewById(R.id.ed_sysbrandcomments)).getText().toString()+"'" +
												   																							" Where z_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and  fld_zoneno='"+getzoneno+"'");
									sysbrandscommon();
								}
								else
								{
									try
									{ System.out.println("insi try");
										
											Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'", null);
											System.out.println("CCC="+cur.getCount());
											if(cur.getCount()==0)
											{
											
												
												db.hi_db.execSQL(" INSERT INTO "+db.CreateSystembrands+" (fld_templatename,fld_inspectorid,fld_noofzones,fld_zoneno,fld_zonename," +
														"fld_zonenameother,fld_externalunit,fld_internalunit,fld_extunitreplprob,fld_extunitreplprobother,fld_intunitreplprob," +
														"fld_intunitreplprobother,fld_ampextunit,fld_ampintunit,fld_emerheatampdraw,fld_supplytemp,fld_returntemp,fld_systembrandcomments) VALUES" +
															 "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getnoofzones+"','"+getzoneno+"','"+getzonename+"'," +
															 		"'"+db.encode(strzonename)+"','"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"'," +
															 				"'"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"',"+
														"'"+getexternalunit+"','"+db.encode(strexternalunit)+"','"+getinternalunit+"','"+db.encode(strinternalunit)+"'," +
																"'"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"'," +
																		"'"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"'," +
																				"'"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"'," +
																						"'"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"'," +
																								"'"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"'," +
																										"'"+((EditText)findViewById(R.id.ed_sysbrandcomments)).getText().toString()+"')");
										
												
												
												
												sysbrandscommon();savehvacnotappl(getcurrentview);
											}
											else											
											{
												db.hi_db.execSQL("UPDATE  "+db.CreateSystembrands+" SET fld_noofzones='"+getnoofzones+"',"+
												           "fld_zoneno='"+getzoneno+"',fld_zonename='"+getzonename+"',fld_zonenameother='"+db.encode(strzonename)+"',"+
														   "fld_externalunit='"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"'," +
														   									"fld_internalunit='"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"',fld_extunitreplprob='"+getexternalunit+"'," +
														   											"fld_extunitreplprobother='"+db.encode(strexternalunit)+"',fld_intunitreplprob='"+getinternalunit+"',fld_intunitreplprobother='"+db.encode(strinternalunit)+"'," +
														   													"fld_ampextunit='"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"'," +
														   															"fld_ampintunit='"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"'," +
														   																	"fld_emerheatampdraw='"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"'," +
														   																			"fld_supplytemp='"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"'," +
														   																					"fld_returntemp='"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"',fld_systembrandcomments='"+((EditText)findViewById(R.id.ed_sysbrandcomments)).getText().toString()+"'" +
														   																							" Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'");
												sysbrandscommon();
											}
										
									}
									catch (Exception e) {
										// TODO: handle exception
										System.out.println("catch="+e.getMessage());
									}
									
								}
						 }
					 
							
						}
						catch (Exception e) {
							// TODO: handle exception
							 System.out.println("insi sdasdaseeeea"+e.getMessage());
						}
					 
				
				}
			
			}
		});
		
		fueltanklocnote = (TextView)findViewById(R.id.fueltankloctv);
		fluetypenote = (TextView)findViewById(R.id.fluetypetv);
		testedfornote = (TextView)findViewById(R.id.testedfortv);
		airhandlerfeatnote = (TextView)findViewById(R.id.airhandlerfeattv);
		
		
		
		LinearLayout menu = (LinearLayout) findViewById(R.id.hvacmenu);
		menu.addView(new Header_Menu(this, 3, cf,gettempname,getcurrentview,getmodulename,gettempoption,getselectedho,db));
		displayheating();
		shownotpplicable();
	}
	private void savehvacnotappl(String currentview) {
		// TODO Auto-generated method stub
		try
		{
			if(chknotapplicable.isChecked()==true)
			{
				chk=1;
			}
			else
			{
				chk=2;
			}
			
			Cursor cur2;
			if(currentview.equals("start"))
			{
				 cur2= db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);	
			}
			else
			{
				 cur2= db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
			
			
			if(cur2.getCount()==0)
			{
				if(currentview.equals("start"))
				{
					db.hi_db.execSQL(" INSERT INTO "+db.HVACNotApplicable+" (fld_templatename,fld_inspectorid,fld_srid,fld_SysbrandsNA) VALUES" +
															    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+chk+"')");
				}
				else
				{
					db.hi_db.execSQL(" INSERT INTO "+db.CreateHVACNotApplicable+" (fld_templatename,fld_inspectorid,fld_SysbrandsNA) VALUES" +
						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+chk+"')");
				}
						
			}
			else
			{
				if(currentview.equals("start"))
				{
					db.hi_db.execSQL("UPDATE  "+db.HVACNotApplicable+" SET fld_SysbrandsNA='"+chk+"' Where fld_inspectorid='"+db.UserId+"' and fld_Srid='"+getselectedho+"'");
				}
				else
				{
					db.hi_db.execSQL("UPDATE  "+db.CreateHVACNotApplicable+" SET fld_SysbrandsNA='"+chk+"' Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"'");
				}
				
			
			}
		}
		catch(Exception e1)
		{
			System.out.println("not appl="+e1.getMessage());
		}
	}
	private void shownotpplicable() {
		// TODO Auto-generated method stub
		Cursor cur1;
		if(getcurrentview.equals("start"))
		{
			  cur1= db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' ",null);
		}
		else
		{
			  cur1= db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable+ " where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+getselectedho+"' ",null);	
		}
		System.out.println("cur11="+cur1.getCount());
		if(cur1.getCount()>0)
		{
			cur1.moveToFirst();
			String navalue = cur1.getString(cur1.getColumnIndex("fld_SysbrandsNA"));System.out.println("tes="+navalue);
			if(navalue.equals("1"))
			{System.out.println("inside checked");
				chknotapplicable.setChecked(true);
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE); 		
			}
			else
			{System.out.println(" fdsfdsf");
				chknotapplicable.setChecked(false);
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
			}
		}
		
	}
	private void sysbrandscommon()
	{
		cf.DisplayToast("System Brands/Testing updated successfully");
		((Button)findViewById(R.id.btnsave)).setText("Save and Add more");
		displayheating();
		clearheating();
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(NewSystembrand.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						//DefaultValue_Insert(gettempname);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	/*private void DefaultValue_Insert(String gettempname) {
		// TODO Auto-generated method stub
		cf.GetCurrentModuleName(getmodulename);
		for(int i=0;i<cf.arrsub.length;i++)
		{
			try
			{
				Cursor selcur = db.hi_db.rawQuery("select * from " + db.CreateSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	            int cnt = selcur.getCount();System.out.println("select * from " + db.CreateSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_module='"+db.encode(getmodulename)+"'"+"count="+cnt);
	            if(cnt==0)
	            {
	            	//Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
	            	Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getselectedho+"' and fld_module='"+db.encode(getmodulename)+"'", null);
		            if(cur.getCount()==0)
		            {
		            	try 
		            	{
		            		db.hi_db.execSQL("INSERT INTO "+db.LoadSystembrands+" (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_NA,fld_description) VALUES" +
									"('"+db.UserId+"','"+getselectedho+"','"+db.encode(getmodulename)+"','"+db.encode(cf.arrsub[i])+"','"+db.encode(gettempname)+"','1','')");
	                    }
		            	catch (Exception e) {
							// TODO: handle exception
		            		System.out.println("e="+e.getMessage());
						}
		            }
	            }
	            else
	            {
	            	
					if (selcur != null) {
						selcur.moveToFirst();System.out.println("cn="+cnt);
						do {
							String incval =selcur.getString(selcur.getColumnIndex("fld_NA"));
							String defvalue =db.decode(selcur.getString(selcur.getColumnIndex("fld_description")));
							
							//Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
							Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getselectedho+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	    		            if(cur.getCount()==0)
	    		            {
    							try
    			            	{
    			            		db.hi_db.execSQL("INSERT INTO "+db.LoadSystembrands+" (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES" +
    										"('"+db.UserId+"','"+getselectedho+"','"+db.encode(getmodulename)+"','"+db.encode(cf.arrsub[i])+"','"+db.encode(defvalue)+"','"+db.encode(gettempname)+"','"+incval+"')");
    		                 	}
    			            	catch (Exception e) {
    								// TODO: handle exception
    							}
	    		            }
	    		            else
	    		            {
	    		            	System.out.println("UPDATE "+db.LoadSystembrands+ " SET fld_NA='"+incval+"',fld_templatename='"+db.encode(gettempname)+"',fld_description='"+db.encode(defvalue)+"' WHERE fld_inspectorid='"+db.UserId+"' " +
	    		            			"and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getselectedho+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'");
	    		            	db.hi_db.execSQL("UPDATE "+db.LoadSystembrands+ " SET fld_templatename='"+db.encode(gettempname)+"',fld_description='"+db.encode(defvalue)+"' WHERE fld_inspectorid='"+db.UserId+"' " +
	    		            			"and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getselectedho+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'");
	    		            }
						
						}while (selcur.moveToNext());
					}
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		Display();
		
	}*/
	private void clearheating() {
		// TODO Auto-generated method stub
		spin_noofzones.setSelection(0);
		spin_zoneno.setSelection(0);
		sp_zonename.setSelection(0);
		
		
		sp_externalunitreplprob.setSelection(0);
		sp_internalunitreplprob.setSelection(0);
		ed_otherzonename.setVisibility(v.GONE);
		((EditText)findViewById(R.id.other_externalunit)).setVisibility(v.GONE);
		((EditText)findViewById(R.id.externalunit)).setText("");
		((EditText)findViewById(R.id.internalunit)).setText("");	
		((EditText)findViewById(R.id.other_externalunit)).setVisibility(v.GONE);
		((EditText)findViewById(R.id.ampdrawexternalunit)).setText("");
		((EditText)findViewById(R.id.ampdrawinternalunit)).setText("");
		((EditText)findViewById(R.id.emerheatampdraw)).setText("");
		((EditText)findViewById(R.id.suppplytemp)).setText("");
		((EditText)findViewById(R.id.returntemp)).setText("");
		((EditText)findViewById(R.id.ed_sysbrandcomments)).setText("");
	}

	private void displayheating() {
		// TODO Auto-generated method stub
		try
		{
			
			Cursor cur;
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}	
			
		System.out.println("cc="+cur.getCount());
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 try
					{
						heatingtblshow.removeAllViews();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.systembrand_header, null); 
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.systembrandheader);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();
			
				 heatingtblshow.setVisibility(View.VISIBLE); 
				 
				 heatingtblshow.addView(th,lp); 			 
				
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String hvactype="",hvactypeother="",energysource="",energysourceother="",gasshutoff="",gasshutoffother="",age="",fueltankloc="",fluetype="",
							 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",
							 furnacefeatuers="",supplyregister="",retunrregister="",fueltanklocation="",comments="",
									 noofzones="",zoneno="",zonename="",zonenameother="",blowermotor="",testedfor="",airhandlerfeatures="",
									 ducworktype="",multizonesystem="",tonnage="",tonnageother,btu,btuother,thermotype,thermolocation,
											 externalunit,internalunit,extunitreplprob,extunitreplprobother,intunitreplprob,intunitreplprobother,ampextunit,ampintunit,emerheatampdraw,supplytemp,returntemp,conddischarge;
					 
					System.out.println("mo of zones");
					 noofzones=cur.getString(cur.getColumnIndex("fld_noofzones"));System.out.println("DDD="+noofzones);
					 zoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));					 
					 zonename=cur.getString(cur.getColumnIndex("fld_zonename"));System.out.println("ddd"+zonename);
					 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_zonenameother")));
					 externalunit=db.decode(db.decode(cur.getString(cur.getColumnIndex("fld_externalunit"))));
					 internalunit=db.decode(cur.getString(cur.getColumnIndex("fld_internalunit")));					 
					 extunitreplprob=cur.getString(cur.getColumnIndex("fld_extunitreplprob"));
					 extunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_extunitreplprobother")));					 
					 intunitreplprob=cur.getString(cur.getColumnIndex("fld_intunitreplprob"));					 
					 intunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_intunitreplprobother")));
					 ampextunit=db.decode(cur.getString(cur.getColumnIndex("fld_ampextunit")));					 
					 ampintunit=db.decode(cur.getString(cur.getColumnIndex("fld_ampintunit")));
					 emerheatampdraw=db.decode(db.decode(cur.getString(cur.getColumnIndex("fld_emerheatampdraw"))));					 
					 supplytemp=db.decode(cur.getString(cur.getColumnIndex("fld_supplytemp")));
					 returntemp=db.decode(cur.getString(cur.getColumnIndex("fld_returntemp")));					 
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_systembrandcomments")));	
					 System.out.println("DDDDdddddddd");
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.systembrand_listview, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.systembrandvalue);	 System.out.println("dddd");
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     
				     System.out.println("sdsdsdkjld");
				     
				     TextView tvzonename= (TextView) t.findViewWithTag("zonename");
				     if(zonename.equals("--Select--"))
					 {
				    	 zonename="";
					 }
				     if(zonename.equals("Other"))
					 {
				    	 tvzonename.setText(zonenameother);	
					 }
					 else
					 {
						 tvzonename.setText(zonename);
					 }
				     TextView tvnoofzones= (TextView) t.findViewWithTag("noofzones");
						if(noofzones.equals("--Select--"))
						{
							noofzones="";
						}
						tvnoofzones.setText(noofzones);
						
					 TextView tvzoneno= (TextView) t.findViewWithTag("zoneno");
					 if(zoneno.equals("--Select--"))
						{
						 zoneno="";
						}
					 tvzoneno.setText(zoneno);						
										
					
					 TextView tvexternalunit= (TextView) t.findViewWithTag("externalunitsn");
					 tvexternalunit.setText(externalunit);
					 TextView tvinternalunit= (TextView) t.findViewWithTag("internalunitsn");
					 tvinternalunit.setText(internalunit);
					 
					 TextView tvexternalunitreppob= (TextView) t.findViewWithTag("externalunitrepprob");
					 if(extunitreplprob.equals("--Select--"))
					 {
						 extunitreplprob="";
					 }
					 tvexternalunitreppob.setText(extunitreplprob);
					 
					 TextView tvinternalunitreppob= (TextView) t.findViewWithTag("internalunitrepprob");
					 if(intunitreplprob.equals("--Select--"))
					 {
						 intunitreplprob="";
					 }
					 tvinternalunitreppob.setText(intunitreplprob);
					 
					
					 TextView tvampdrawextunit= (TextView) t.findViewWithTag("ampdrawextunit");
					 tvampdrawextunit.setText(ampextunit);
					 TextView tvampdrawintunit= (TextView) t.findViewWithTag("ampdrawintunit");
					 tvampdrawintunit.setText(ampintunit);
					 TextView tvemerheatampdraw= (TextView) t.findViewWithTag("emerheatampdraw");
					 tvemerheatampdraw.setText(emerheatampdraw);
					 TextView tvsupplytemp= (TextView) t.findViewWithTag("supplytemp");
					 tvsupplytemp.setText(supplytemp);
					 TextView tvreturntemp= (TextView) t.findViewWithTag("returntemp");
					 tvreturntemp.setText(returntemp);
					 
					 TextView tvductworkcomments= (TextView) t.findViewWithTag("comments");
						
					 tvductworkcomments.setText(comments);

					 
					 ImageView edit= (ImageView) t.findViewWithTag("edit");
					 	edit.setId(789456+i);
					 	edit.setTag(cur.getString(0));
		                edit.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
							try
							{
							   int i=Integer.parseInt(v.getTag().toString());
								((Button)findViewById(R.id.btnsave)).setText("Update");
							   //vrint=1;
							clearheating();
							   updatehvac(i);   
							}
							catch (Exception e) {
								// TODO: handle exception
							}
							}
		                });
	             
	                ImageView delete=(ImageView) t.findViewWithTag("del");  
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(NewSystembrand.this);
							builder.setMessage("Do you want to delete the selected Heating details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==hvacCurrent_select_id)
													{
														hvacCurrent_select_id=0;
													}
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadSystembrands+" Where z_id='"+i+"'");	
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateSystembrands+" Where z_id='"+i+"'");
													}
																										
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
												/*	Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadHeating + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname).toLowerCase()+"'",null);
													if(cur.getCount()==0)
													{
														flag1=1;
													}*/
													
													//clearheating();
													clearheating();
													displayheating();((Button)findViewById(R.id.btnsave)).setText("Save and Add more");
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					
					
					
					
					l1.removeAllViews();
					
					heatingtblshow.addView(t,lp);
					cur.moveToNext();
					
				 }
			 }
			 else
			 {
				 heatingtblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	 	
	}
	private void scrollup() {
		// TODO Auto-generated method stub
	scr = (ScrollView)findViewById(R.id.scr);
	       scr.post(new Runnable() {
	          public void run() {
	              scr.scrollTo(0,0);
	          }
	      }); 
	}
	protected void updatehvac(int i) {
		// TODO Auto-generated method stub
		Cursor cur;
			editid=1;
			getisedit=1;
			hvacCurrent_select_id = i;scrollup();
			try
			{
				if(getcurrentview.equals("start"))
				{
					  cur= db.hi_db.rawQuery("select * from " + db.LoadSystembrands + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'",null);
				}
				else
				{
					 cur= db.hi_db.rawQuery("select * from " + db.CreateSystembrands + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and z_id='"+i+"'", null);
				}
					
			System.out.println("SSDs"+cur.getCount());
				
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
						
					 String hvactype="",hvactypeother="",energysource="",energysourceother="",gasshutoff="",gasshutoffother="",age="",fueltankloc="",fluetype="",
							 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",
							 furnacefeatuers="",supplyregister="",retunrregister="",fueltanklocation="",comments="",
									 noofzones="",zoneno="",zonename="",zonenameother="",blowermotor="",testedfor="",airhandlerfeatures="",
									 ducworktype="",multizonesystem="",tonnage="",tonnageother,btu,btuother,thermotype,thermolocation,
											 externalunit,internalunit,extunitreplprob,extunitreplprobother,intunitreplprob,intunitreplprobother,ampextunit,ampintunit,emerheatampdraw,supplytemp,returntemp,conddischarge;
					 System.out.println("BBBB");
					 
					 noofzones=cur.getString(cur.getColumnIndex("fld_noofzones"));System.out.println("noofzones="+noofzones);
					 zoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));System.out.println("zoneno="+zoneno);					 
					 zonename=cur.getString(cur.getColumnIndex("fld_zonename"));
					 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_zonenameother")));
					 externalunit=db.decode(cur.getString(cur.getColumnIndex("fld_externalunit")));
					 internalunit=db.decode(cur.getString(cur.getColumnIndex("fld_internalunit")));					 
					 extunitreplprob=cur.getString(cur.getColumnIndex("fld_extunitreplprob"));
					 extunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_extunitreplprobother")));					 
					 intunitreplprob=cur.getString(cur.getColumnIndex("fld_intunitreplprob"));					 
					 intunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_intunitreplprobother")));
					 ampextunit=cur.getString(cur.getColumnIndex("fld_ampextunit"));					 
					 ampintunit=cur.getString(cur.getColumnIndex("fld_ampintunit"));
					 emerheatampdraw=db.decode(cur.getString(cur.getColumnIndex("fld_emerheatampdraw")));					 
					 supplytemp=cur.getString(cur.getColumnIndex("fld_supplytemp"));
					 returntemp=db.decode(cur.getString(cur.getColumnIndex("fld_returntemp")));	System.out.println("return temp");				 
					 comments=db.decode(cur.getString(cur.getColumnIndex("fld_systembrandcomments")));

					 if(noofzones.equals(""))
					{
						spin_noofzones.setSelection(0);
					}
					else
					{
						spin_noofzones.setSelection(noofzonesarrayadap.getPosition(noofzones));
					}
					
					
					spin_zoneno.setSelection(zonenoarrayadap.getPosition(zoneno));
					
					 if(zonename.equals(""))
					{
						 sp_zonename.setSelection(0);
					}
					else
					{
						sp_zonename.setSelection(zonenameadap.getPosition(zonename));
					}
					
					if(zonenameother.trim().equals(""))
					{
						ed_otherzonename.setVisibility(v.GONE);
					}
					else
					{
						ed_otherzonename.setVisibility(v.VISIBLE);
						ed_otherzonename.setText(zonenameother);
					}
					
					
					 ((EditText)findViewById(R.id.externalunit)).setText(externalunit);
					 ((EditText)findViewById(R.id.internalunit)).setText(internalunit);
					
					sp_externalunitreplprob.setSelection(externalunitrepadap.getPosition(extunitreplprob));
					if(extunitreplprobother.trim().equals(""))
					{
						((EditText)findViewById(R.id.other_externalunit)).setVisibility(v.GONE);
					}
					else
					{
						((EditText)findViewById(R.id.other_externalunit)).setVisibility(v.VISIBLE);
						((EditText)findViewById(R.id.other_externalunit)).setText(extunitreplprobother);
					}
					
					sp_internalunitreplprob.setSelection(internalunitrepadap.getPosition(intunitreplprob));
					if(intunitreplprobother.trim().equals(""))
					{
						((EditText)findViewById(R.id.other_internalunit)).setVisibility(v.GONE);
					}
					else
					{
						((EditText)findViewById(R.id.other_internalunit)).setVisibility(v.VISIBLE);
						((EditText)findViewById(R.id.other_internalunit)).setText(intunitreplprobother);
					}
					
					((EditText)findViewById(R.id.ampdrawexternalunit)).setText(ampextunit);
					((EditText)findViewById(R.id.ampdrawinternalunit)).setText(ampintunit);
					((EditText)findViewById(R.id.emerheatampdraw)).setText(emerheatampdraw);
					((EditText)findViewById(R.id.suppplytemp)).setText(supplytemp);
					((EditText)findViewById(R.id.returntemp)).setText(returntemp);
					System.out.println("comments ="+comments);
					((EditText)findViewById(R.id.ed_sysbrandcomments)).setText(comments);

					
					((Button)findViewById(R.id.btnsave)).setText("Update");
				}
			}catch(Exception e){}
		
	}
	private void dbvalue(String bathvalue) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		if(bathvalue.equals("Fuel Tank Location"))
		{
			for(int j=0;j<arrfueltank.length;j++)
			{
				str_arrlst.add(arrfueltank[j]);
			}	
		}
		if(bathvalue.equals("Flue Type"))
		{
			for(int j=0;j<arrfluetype.length;j++)
			{
				str_arrlst.add(arrfluetype[j]);
			}	
		}
		if(bathvalue.equals("Tested For"))
		{
			for(int j=0;j<arrzonetestingfor.length;j++)
			{
				str_arrlst.add(arrzonetestingfor[j]);
			}	
		}

		if(bathvalue.equals("Air Handler Features"))
		{
			for(int j=0;j<arrairhandlerfeatures.length;j++)
			{
				str_arrlst.add(arrairhandlerfeatures[j]);
			}	
		}
		
		if(bathvalue.equals("Ductwork Type"))
		{
			for(int j=0;j<arrductworktype.length;j++)
			{
				str_arrlst.add(arrductworktype[j]);
			}	
		}
		
		if(bathvalue.equals("Thermostat Type"))
		{
			for(int j=0;j<arrthermotype.length;j++)
			{
				str_arrlst.add(arrthermotype[j]);
			}	
		}
		
		if(bathvalue.equals("Thermostat Location"))
		{
			for(int j=0;j<arrthermolocation.length;j++)
			{
				str_arrlst.add(arrthermolocation[j]);
			}	
		}
		
		if(bathvalue.equals("Condensation Discharge"))
		{
			for(int j=0;j<arrcondendischarge.length;j++)
			{
				str_arrlst.add(arrcondendischarge[j]);
			}	
		}
		
	}
	
	private void multi_select(final String hvacvalue, int s) {
		// TODO Auto-generated method stub
		hvac=1;
		dbvalue(hvacvalue);
		System.out.println("hvacvalue"+hvacvalue+"tempfueltankloc="+tempfueltankloc);
		final Dialog add_dialog = new Dialog(NewSystembrand.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);

		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);
		
		 		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(hvacvalue);
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);
		btnother.setVisibility(v.GONE);
		showsuboption();
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval="";
								for (int i = 0; i < str_arrlst.size(); i++) {
									if (ch[i].isChecked()) {
										seltdval += ch[i].getText().toString() + "&#44;";
									}
								}
								
								
								finalselstrval = "";
								finalselstrval = seltdval;
								if(finalselstrval.equals(""))
								{
									cf.DisplayToast("Please check at least any 1 option to save");
								}
								else
								{	
									
									
									
									add_dialog.cancel();
									hvac=0;
								}
								
					}
				
		});

		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hvac=0;
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				
			}
		});
		setvalue(hvacvalue);System.out.println("tttt="+tempfueltankloc);
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	
	private void setvalue(String hvacvalue) {
		// TODO Auto-generated method stub
		System.out.println("bathvalue="+tempfueltankloc);
		try
		{
			
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateSystembrands+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			System.out.println("count"+cur.getCount());
		    if (cur.getCount() > 0) 
		    {
				cur.moveToFirst();
				if(hvacvalue.equals("Fuel Tank Location"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_fueltankloc")));						 
				}
				else if(hvacvalue.equals("Flue Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));	
				}
				else if(hvacvalue.equals("Tested For"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_testedfor")));	
				}
				else if(hvacvalue.equals("Air Handler Features"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_airhandlerfeatures")));	
				}			
				else if(hvacvalue.equals("Ductwork Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_ducworktype")));	
				}			
				else if(hvacvalue.equals("Thermostat Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_thermotype")));	
				}
				else if(hvacvalue.equals("Thermostat Location"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_thermolocation")));	
				}
				else if(hvacvalue.equals("Condensation Discharge"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_conddischarge")));	
				}
				
				if(str.contains(","))
			    {
			    	str = str.replace(",","&#44;");
			    }	
			}
		    else
		    {
			
		    	if(hvacvalue.equals("Fuel Tank Location") && hvacvalue!=null)
				{
					str = fueltankvalue;							 
				}
				else if(hvacvalue.equals("Flue Type") && hvacvalue!=null)
				{
					str = fluetypevalue;		
				}
				else if(hvacvalue.equals("Tested For") && hvacvalue!=null)
				{
					str = testedforvalue;
				}
				else if(hvacvalue.equals("Air Handler Features") && hvacvalue!=null)
				{
					str = airhandlerfeatvalue;
				}			
				else if(hvacvalue.equals("Ductwork Type") && hvacvalue!=null)
				{
					str =  ductworktypevalue;
				}			
				else if(hvacvalue.equals("Thermostat Type") && hvacvalue!=null)
				{
					str = thermostattypevalue;
				}
				else if(hvacvalue.equals("Thermostat Location") && hvacvalue!=null)
				{
					str  = thermolocationvalue;
				}
				else if(hvacvalue.equals("Condensation Discharge") && hvacvalue!=null)
				{
					str = conddischangevalue;
				}
		    }
			    if(!str.trim().equals(""))
				{
					cf.setValuetoCheckbox(ch, str);
				}	
		}catch (Exception e) {
			// TODO: handle exception
		}
	    	
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	private void showsuboption() {
		// TODO Auto-generated method stub
		  ch = new CheckBox[str_arrlst.size()];
			
			for (int i = 0; i < str_arrlst.size(); i++) {
	        		ch[i] = new CheckBox(this);
					ch[i].setText(str_arrlst.get(i));
					ch[i].setTextColor(Color.BLACK);
					
					lin.addView(ch[i]);
					ch[i].setOnCheckedChangeListener(new CHListener(i));
					
			}
	}
	
	class CHListener implements OnCheckedChangeListener, android.widget.CompoundButton.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		
		//seltdval="";
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
			}
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	class myspinselectedlistener implements OnItemSelectedListener
	{
         int var=0;
        
		public myspinselectedlistener(int i) {
			// TODO Auto-generated constructor stub
			var=i;
			 getspinselectedname="";
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			getspinselectedname = arg0.getItemAtPosition(arg2).toString();
			switch(var)
			{
			  case 25:
				  getzonename = getspinselectedname;
				  if(getzonename.equals("Other"))
		    	  {
		    		  ((EditText)findViewById(R.id.other_zonename)).setVisibility(arg1.VISIBLE);
		    		  ((EditText)findViewById(R.id.other_zonename)).requestFocus();
		    		 
		    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
		    	  }
		    	  else
		    	  {
		    		  ((EditText)findViewById(R.id.other_zonename)).setText("");
		    		  ((EditText)findViewById(R.id.other_zonename)).setVisibility(arg1.GONE);
		    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
		    	  }
		    	  break;
				
				  case 31:
			    	  getexternalunit= getspinselectedname;
			    	  if(getexternalunit.equals("Other"))
			    	  {
			    		  ((EditText)findViewById(R.id.other_externalunit)).setVisibility(arg1.VISIBLE);
			    		  ((EditText)findViewById(R.id.other_externalunit)).requestFocus();
			    		 
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
			    	  }
			    	  else
			    	  {
			    		  ((EditText)findViewById(R.id.other_externalunit)).setText("");
			    		  ((EditText)findViewById(R.id.other_externalunit)).setVisibility(arg1.GONE);
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
			    	  }
			    	  break;
				  case 32:
			    	  getinternalunit= getspinselectedname;
			    	  if(getinternalunit.equals("Other"))
			    	  {
			    		  ((EditText)findViewById(R.id.other_internalunit)).setVisibility(arg1.VISIBLE);
			    		  ((EditText)findViewById(R.id.other_internalunit)).requestFocus();
			    		 
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
			    	  }
			    	  else
			    	  {
			    		  ((EditText)findViewById(R.id.other_internalunit)).setText("");
			    		  ((EditText)findViewById(R.id.other_internalunit)).setVisibility(arg1.GONE);
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
			    	  }
			    	  break;
				  case 15:
					  getnoofzones = getspinselectedname;
			    	  break;
				  case 16:
					  getzoneno = getspinselectedname;
			    	  break;
				  case 6:
					  getheatbrand = getspinselectedname;
			    	  break;
				  case 7:
			    	  getfurnace = getspinselectedname;
			    	  break;
				  case 10:
			    	  getmotorblower = getspinselectedname;
			    	  break;
				  case 18:
			    	  getsupplyregister = getspinselectedname;
			    	  break;  
			    	  
			      case 19:
			    	  getreturnregister = getspinselectedname;
			    	  break;  
			      
			    	  
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	protected void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(NewSystembrand.this, NewDuctwork.class);
    	Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		Bundle b6 = new Bundle();
		b6.putString("selectedid", getselectedho);			
		intent.putExtras(b6);
		startActivity(intent);
		finish();
		
       
	}
	protected void onSaveInstanceState(Bundle ext)
	{		
		ext.putInt("chkvalue", chk);
		
		super.onSaveInstanceState(ext);
	}
	protected void onRestoreInstanceState(Bundle ext)
	{
		chk = ext.getInt("chkvalue");	
		
		if(chk==1)
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
		}
		else
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
		}
		
		 super.onRestoreInstanceState(ext);
	}
}
