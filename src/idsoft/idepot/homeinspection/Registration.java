package idsoft.idepot.homeinspection;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.GraphicsView;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;
import idsoft.idepot.homeinspection.support.phone_nowatcher;

import java.io.ByteArrayOutputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.TableRow;

public class Registration extends Activity {
	EditText etregister_firstname,etregister_lastname,etregister_middlename,
	         etregister_address1,etregister_address2,etregister_zipcode,
	         etregister_city,etregister_state,etregister_county,etregister_contactphone,etregister_fax,etregister_website,
	         etregister_email,etregister_licensenum,etregister_licenseexpiry,
	         etregister_licensetype,etcompany_name,etcompany_logo,etcompany_phone,etcompany_email,
	         et_headshot,etupload_license;
	Spinner spin_licensetype;
	String getlicensetype="",imageidentifier="",selectedImagePathcomplogo="",selectedImagePath="",extStorageDirectory="",strwebsite="",strinspectorid="",
		   headshotpicname="",complogopicname="",strfirstname="",strlastname="",strmiddlename="",licensepicname="",initialsfilepath="",strpassword="",
		   straddress1="",straddress2="",strzipcode="",strcity="",strcounty="",strstate="",strcontactphone="",signfilepath="",strfaxnumber="",
		   stremail="",strusername="",strlicensenum="",strlicenseexpirydate="",strotherlicensetype="",selectedImagePathlicense="",randomid="",
		   strcompanyname="",strcompanylogo="",strcompanyphone="",strcompanyemail="",strheadshot="",imgPath="",strupload_license="",userid="";
	int keyDel,ckeyDel,pi,datewin=0,pic=0,del_lic=0,del_comp=0,del_head=0;
	Button btn_expirydate,btn_companylogo,btn_headshotlogo,btn_signclear,btn_initialclear,btn_clear,btn_register,btn_uploadlicense;
	public Uri CapturedImageURI;
	BitmapDrawable bmdregister;
	boolean signbool=false,initialbool=false;
	byte[] signsignature,initialsignature;
	ArrayAdapter primlicentypeadap;
	TableRow trlicensetype;
	GraphicsView signview,initialview;
	Progress_staticvalues p_sv;
	CommonFunction cf;
	byte[] byteArray;
	Webservice_config wb;
	Bitmap bitmap,bm,bm1,bm2;
	DatabaseFunction db;
	private int handler_msg=0;
	String erro_msg="",filepath="",strsignature,strintitials;
	Random rand;
	View v1;
	ScrollView parentscr;
	String[] primarylictype={"--Select--","State Licensed Home Inspector","Licensed Roofing Contractor","Licensed Building Contractor","Licensed General Contractor",
			"Licensed Residential Contractor","Licensed Electrician","Licensed Plumber","Licensed Architect","Professional Engineer",
			"Certified ASHI Inspector","ASHI Candidate","Certified NACHI Inspector","CREIA","FABI RPI","HVAC License",
			"Building Code Official License","Licensed Loss Adjuster","Chartered Building Surveyor","Other"};
	final private int CAPTURE_IMAGE = 2;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		wb = new Webservice_config(this);
		Bundle b =getIntent().getExtras();
		if(b!=null)
		{
			userid=b.getString("userid");
		}
		
		rand = new Random();
		db.CreateTable(1);
		db.userid();		
		
		
		parentscr = (ScrollView)findViewById(R.id.parentscr);
		etregister_firstname = (EditText)findViewById(R.id.et_inspfirstname);
		etregister_lastname = (EditText)findViewById(R.id.et_insplastname);
		etregister_middlename = (EditText)findViewById(R.id.et_inspmiddlename);
		etregister_address1 = (EditText)findViewById(R.id.et_inspaddress1);
		etregister_address2 = (EditText)findViewById(R.id.et_inspaddress2);
		etregister_zipcode = (EditText)findViewById(R.id.et_inspzipcode);
		etregister_city = (EditText)findViewById(R.id.et_inspcity);
		etregister_state = (EditText)findViewById(R.id.et_inspstate);
		//etregister_county = (EditText)findViewById(R.id.et_inspcounty);
		etregister_contactphone = (EditText)findViewById(R.id.et_inspcontactphone);
		etregister_email = (EditText)findViewById(R.id.et_inspemail);
		etregister_licensenum = (EditText)findViewById(R.id.et_insplicensenumber);
		etregister_licenseexpiry = (EditText)findViewById(R.id.et_insplicenseexpirydate);
		btn_expirydate = (Button)findViewById(R.id.btn_licensedate);
		spin_licensetype = (Spinner)findViewById(R.id.spin_insplicensetype);
		primlicentypeadap = new ArrayAdapter<String>(Registration.this,android.R.layout.simple_spinner_item,primarylictype);
		primlicentypeadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_licensetype.setAdapter(primlicentypeadap);
		spin_licensetype.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		etregister_licensetype = (EditText)findViewById(R.id.et_licenseothertype);
		etcompany_name = (EditText)findViewById(R.id.et_companyname);
		etcompany_logo = (EditText)findViewById(R.id.et_companylogo);etcompany_logo.setVisibility(v1.VISIBLE);
		etcompany_phone = (EditText)findViewById(R.id.et_companycontactphone);
		etcompany_email = (EditText)findViewById(R.id.et_companyemail);
		btn_companylogo = (Button)findViewById(R.id.btn_companylogo);
		et_headshot = (EditText)findViewById(R.id.et_headshotlogo);et_headshot.setVisibility(v1.VISIBLE);
		btn_headshotlogo = (Button)findViewById(R.id.btn_headshot);btn_headshotlogo.setVisibility(v1.VISIBLE);
	
		
		etregister_fax = (EditText)findViewById(R.id.et_faxnumber);
		etregister_website = (EditText)findViewById(R.id.et_website);
		trlicensetype = (TableRow)findViewById(R.id.trlicense);
		

		btn_signclear = (Button)findViewById(R.id.btn_signatureclear);
		
	

		
		btn_initialclear = (Button)findViewById(R.id.btn_initialsclear);
		btn_clear = (Button)findViewById(R.id.clear);
		btn_register = (Button)findViewById(R.id.register);
		btn_uploadlicense = (Button)findViewById(R.id.btn_uploadlicense);
		etupload_license = (EditText)findViewById(R.id.et_uploadlicense);etupload_license.setVisibility(v1.VISIBLE);
 		
		etregister_firstname.addTextChangedListener(new CustomTextWatcher(etregister_firstname));
		etregister_lastname.addTextChangedListener(new CustomTextWatcher(etregister_lastname));
		etregister_middlename.addTextChangedListener(new CustomTextWatcher(etregister_middlename));
		etregister_address1.addTextChangedListener(new CustomTextWatcher(etregister_address1));
		etregister_address2.addTextChangedListener(new CustomTextWatcher(etregister_address2));
		etregister_zipcode.addTextChangedListener(new CustomTextWatcher(etregister_zipcode));
		etregister_city.addTextChangedListener(new CustomTextWatcher(etregister_city));
		etregister_state.addTextChangedListener(new CustomTextWatcher(etregister_state));
		//etregister_county.addTextChangedListener(new CustomTextWatcher(etregister_county));
		etregister_email.addTextChangedListener(new CustomTextWatcher(etregister_email));
		etregister_licensenum.addTextChangedListener(new CustomTextWatcher(etregister_licensenum));
		etregister_licensetype.addTextChangedListener(new CustomTextWatcher(etregister_licensetype));
		etcompany_name.addTextChangedListener(new CustomTextWatcher(etcompany_name));
		etcompany_email.addTextChangedListener(new CustomTextWatcher(etcompany_email));
		etregister_contactphone.addTextChangedListener(new phone_nowatcher(etregister_contactphone));		
		etcompany_phone.addTextChangedListener(new phone_nowatcher(etcompany_phone));
		etregister_fax.addTextChangedListener(new phone_nowatcher(etregister_fax));
		btn_companylogo.setVisibility(v1.VISIBLE);
		btn_uploadlicense.setVisibility(v1.VISIBLE);
		
		btn_expirydate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				datewin = 1;
				datewindow();

			}
		});
			
		btn_companylogo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pi = 0;
				pic=1;
				Gallery_Camera_Dialog();
			}
		});
		
		btn_headshotlogo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pi = 1;
				pic=1;
				Gallery_Camera_Dialog();
			}
		});
		
        btn_uploadlicense.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pi = 2;
				pic=1;
				Gallery_Camera_Dialog();
			}
		});
		
      
      
		btn_signclear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				signclear();

			}
		});
		
		signview = (GraphicsView)findViewById(R.id.signature);
		signview.setDrawingCacheEnabled(true);
		
        signview.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				parentscr.requestDisallowInterceptTouchEvent(true);
				signbool = true;
				return false;
			}
		});
        
        initialview=(GraphicsView) findViewById(R.id.initials);		
        initialview.setDrawingCacheEnabled(true);
		
        initialview.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				parentscr.requestDisallowInterceptTouchEvent(true);
				initialbool = true;
				return false;
			}
		});
		
        btn_initialclear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				initialsclear();

			}
		});
        
        btn_clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				etregister_firstname.setText("");
				etregister_lastname.setText("");
				etregister_middlename.setText("");
				etregister_address1.setText("");
				etregister_address2.setText("");
				etregister_zipcode.setText("");
				etregister_city.setText("");
				etregister_state.setText("");
				//etregister_county.setText("");
				etregister_contactphone.setText("");
				etregister_fax.setText("");
				etregister_website.setText("");
				etregister_email.setText("");
				etregister_licensenum.setText("");
				etregister_licenseexpiry.setText("");
				spin_licensetype.setSelection(0);
				etcompany_name.setText("");
				etcompany_logo.setText("");
				etcompany_phone.setText("");
				etcompany_email.setText("");
				etcompany_name.setText("");
				et_headshot.setText("");
				etupload_license.setText("");
				initialsclear(); 
				signclear();
				
			    ((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.GONE);
				((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.GONE);
				btn_uploadlicense.setVisibility(v1.VISIBLE);
				etupload_license.setVisibility(v1.VISIBLE);
				
				((ImageView) findViewById(R.id.complogo)).setVisibility(v1.GONE);
				((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.GONE);
				btn_companylogo.setVisibility(v1.VISIBLE);
				etcompany_logo.setVisibility(v1.VISIBLE);
				
				((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.GONE);
				((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.GONE);
				btn_headshotlogo.setVisibility(v1.VISIBLE);
				et_headshot.setVisibility(v1.VISIBLE);
			}
		});
        
        btn_register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				register_validation();
				//register();
			}
		});
        
       ((Button)findViewById(R.id.back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
       
    	 
       setvalue(); 
  	}
	
	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.deletecomplogo:
			del_comp=1;
			delete_comp();
			break;
		case R.id.deleteheadshot:
			del_head=1;
			delete_head();
			break;
			
		case R.id.deletelicphoto:
			del_lic=1;
			delete_lic();
			break;
		}
	}
	private void delete_head() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder1 = new AlertDialog.Builder(Registration.this);
		builder1.setMessage("Are you sure, Do you want to delete the Headshot?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int id) {
								String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
								String Path =extStorageDirectory+"/HI/HIHeadshot"+userid+".png";

								File outputFile1 = new File(Path);
								if(outputFile1.exists())
								{
									outputFile1.delete();
								}
								selectedImagePath="";
								try
								{
									db.hi_db.execSQL("UPDATE "+ db.Registration +"  SET fld_headshot='"+db.encode(selectedImagePath)+"' where fld_userid='"+userid+"'");
									
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								et_headshot.setVisibility(v1.VISIBLE);
								((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.GONE);
								((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.GONE);
								btn_headshotlogo.setVisibility(v1.VISIBLE);
								del_head=0;
							}
						})
				.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								del_head=0;
								dialog.cancel();
							}
						});
		AlertDialog a2=builder1.create();
		a2.setTitle("Confirmation");
		a2.setIcon(R.drawable.alertmsg);
		a2.setCancelable(false);
		a2.show();
	}

	private void delete_comp() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(Registration.this);
		builder.setMessage("Are you sure, Do you want to delete the Company Logo?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int id) {
								String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
								String Path =extStorageDirectory+"/HI/HILogo"+userid+".png";

								File outputFile1 = new File(Path);
								if(outputFile1.exists())
								{
									outputFile1.delete();
								}
								selectedImagePathcomplogo="";
								try
								{
									db.hi_db.execSQL("UPDATE "+ db.Registration +"  SET fld_companylogo='"+db.encode(selectedImagePathcomplogo)+"' where fld_userid='"+userid+"'");
									
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								etcompany_logo.setVisibility(v1.VISIBLE);
								((ImageView) findViewById(R.id.complogo)).setVisibility(v1.GONE);
								((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.GONE);
								btn_companylogo.setVisibility(v1.VISIBLE);
								del_comp=0;
							}
						})
				.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								del_comp=0;
								dialog.cancel();
							}
						});
		AlertDialog al=builder.create();
		al.setTitle("Confirmation");
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
	}

	private void delete_lic() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder2 = new AlertDialog.Builder(Registration.this);
		builder2.setMessage("Are you sure, Do you want to delete the License Photo?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
									int id) {
								String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
								String Path =extStorageDirectory+"/HI/HILicense"+userid+".png";

								File outputFile1 = new File(Path);
								if(outputFile1.exists())
								{
									outputFile1.delete();
								}
								selectedImagePathlicense="";
								try
								{
									db.hi_db.execSQL("UPDATE "+ db.Registration +"  SET fld_uploadlicense='"+db.encode(selectedImagePathlicense)+"' where fld_userid='"+userid+"'");
									
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								etupload_license.setVisibility(v1.VISIBLE);
								((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.GONE);
								((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.GONE);
								btn_uploadlicense.setVisibility(v1.VISIBLE);
								del_lic=0;
								
							}
						})
				.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								del_lic=0;
								dialog.cancel();
							}
						});
		AlertDialog a3=builder2.create();
		a3.setTitle("Confirmation");
		a3.setIcon(R.drawable.alertmsg);
		a3.setCancelable(false);
		a3.show();
	}

	private void setvalue() {
		// TODO Auto-generated method stub
		Cursor c = db.hi_db.rawQuery("select * from " + db.Registration + " WHERE fld_userid='"+userid+"'",null);System.out.println("count"+c.getCount());
		if(c.getCount()>0)
		{
			c.moveToFirst();
			String f_name=db.decode(c.getString(c.getColumnIndex("fld_firstname")));
			String l_name=db.decode(c.getString(c.getColumnIndex("fld_lastname")));
			String m_name=db.decode(c.getString(c.getColumnIndex("fld_middlename")));
			String addr1=db.decode(c.getString(c.getColumnIndex("fld_address1")));
			String addr2=db.decode(c.getString(c.getColumnIndex("fld_address2")));
			String zip=db.decode(c.getString(c.getColumnIndex("fld_zipcode")));
			String city=db.decode(c.getString(c.getColumnIndex("fld_city")));
			String state=db.decode(c.getString(c.getColumnIndex("fld_state")));
			String county=db.decode(c.getString(c.getColumnIndex("fld_county")));
			String phone=db.decode(c.getString(c.getColumnIndex("fld_contactphone")));
			String email=db.decode(c.getString(c.getColumnIndex("fld_email")));
			String licenseno=db.decode(c.getString(c.getColumnIndex("fld_licensenumber")));
			String company_name=db.decode(c.getString(c.getColumnIndex("fld_companyname")));
			String company_phone=db.decode(c.getString(c.getColumnIndex("fld_companyphone")));
			String fax=db.decode(c.getString(c.getColumnIndex("fld_compfax")));
			String website=db.decode(c.getString(c.getColumnIndex("fld_compwebsite")));
		    String company_email=db.decode(c.getString(c.getColumnIndex("fld_companyemail")));
			String licenseexpiry=db.decode(c.getString(c.getColumnIndex("fld_licenseexpirydate")));
			String licensetype=db.decode(c.getString(c.getColumnIndex("fld_licensetype")));
			String otherlicensetype=db.decode(c.getString(c.getColumnIndex("fld_otherlicensetype")));
		    selectedImagePathlicense=db.decode(c.getString(c.getColumnIndex("fld_uploadlicense")));
			selectedImagePathcomplogo=db.decode(c.getString(c.getColumnIndex("fld_companylogo")));
			selectedImagePath=db.decode(c.getString(c.getColumnIndex("fld_headshot")));
		    System.out.println("selectedImagePathlicense="+selectedImagePathlicense);
		    System.out.println("selectedImagePathcomplogo="+selectedImagePathcomplogo);
		    System.out.println("selectedImagePath="+selectedImagePath);
			
			etregister_firstname.setText(f_name);
			etregister_lastname.setText(l_name);
			etregister_middlename.setText(m_name);
			etregister_address1.setText(addr1);
			etregister_address2.setText(addr2);
			etregister_zipcode.setText(zip);
			etregister_city.setText(city);
			etregister_state.setText(state);
			//etregister_county.setText(county);
			etregister_contactphone.setText(phone);
			etregister_email.setText(email);
			etregister_licensenum.setText(licenseno);
			etregister_licenseexpiry.setText(licenseexpiry);
			etcompany_name.setText(company_name);
			etcompany_phone.setText(company_phone);
			etcompany_email.setText(company_email);
			etregister_fax.setText(fax);
			etregister_website.setText(website);
			((Button) findViewById(R.id.register)).setText("Update");

			
			ArrayAdapter ad =(ArrayAdapter) spin_licensetype.getAdapter();
			int pos=ad.getPosition(licensetype);
			if(pos!=-1)
			{
				spin_licensetype.setSelection(pos);
				if(licensetype.equals("Other"))
				{
					etregister_licensetype.setText(otherlicensetype);
				}
			}			
			String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
			String Path =extStorageDirectory+"/HI/HILicense"+userid+".png";

			File outputFile1 = new File(Path);
			if(outputFile1.exists())
			{
				bmdregister=getbitmap(outputFile1);
				if(bmdregister!=null)
				{
					((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.VISIBLE);
					 ((ImageView) findViewById(R.id.license_photo)).setImageDrawable(bmdregister);
					 etupload_license.setVisibility(v1.GONE);
					 btn_uploadlicense.setVisibility(v1.GONE);
					 ((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.VISIBLE);
				}
				else
				{
					((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.GONE);
					((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.GONE);
					btn_uploadlicense.setVisibility(v1.VISIBLE);
					etupload_license.setVisibility(v1.VISIBLE);
				}				
			}
			else
			{
				((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.GONE);
				((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.GONE);
				btn_uploadlicense.setVisibility(v1.VISIBLE);
				etupload_license.setVisibility(v1.VISIBLE);
			}
			
			String Path1 =extStorageDirectory+"/HI/HILogo"+userid+".png";
			File outputFile2 = new File(Path1);
			if(outputFile2.exists())
			{
				bmdregister=getbitmap(outputFile2);
				if(bmdregister!=null)
				{
					 ((ImageView) findViewById(R.id.complogo)).setVisibility(v1.VISIBLE);
					 ((ImageView) findViewById(R.id.complogo)).setImageDrawable(bmdregister);
					 etcompany_logo.setVisibility(v1.GONE);
					 btn_companylogo.setVisibility(v1.GONE);
					 ((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.VISIBLE);
				}
				else
				{
					((ImageView) findViewById(R.id.complogo)).setVisibility(v1.GONE);
					((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.GONE);
					btn_companylogo.setVisibility(v1.VISIBLE);
					etcompany_logo.setVisibility(v1.VISIBLE);
				}				
			}
			else
			{
				((ImageView) findViewById(R.id.complogo)).setVisibility(v1.GONE);
				((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.GONE);
				btn_companylogo.setVisibility(v1.VISIBLE);
				etcompany_logo.setVisibility(v1.VISIBLE);
			}
			
			String Path2 =extStorageDirectory+"/HI/HIInitials"+userid+".png";
			System.out.println("Path2="+Path2);
			
			File outputFile3 = new File(Path2);
			System.out.println("outputFile3="+outputFile3.exists());
			if(outputFile3.exists())
			{
				bmdregister=getbitmap(outputFile3);
				if(bmdregister!=null)
				{
					initialview.setBackgroundDrawable(bmdregister);
				}
			}
			
			String Path3 =extStorageDirectory+"/HI/HISignature"+userid+".png";
			File outputFile4 = new File(Path3);
			if(outputFile4.exists())
			{
				bmdregister=getbitmap(outputFile4);
				if(bmdregister!=null)
				{
					signview.setBackgroundDrawable(bmdregister);
				}		
			}
			
				
			String Path4 =extStorageDirectory+"/HI/HIHeadshot"+userid+".png";
			File outputFile5 = new File(Path4);
			if(outputFile5.exists())
			{
				bmdregister=getbitmap(outputFile5);
				if(bmdregister!=null)
				{
					((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.VISIBLE);
					((ImageView) findViewById(R.id.headshot_photo)).setImageDrawable(bmdregister);
					 et_headshot.setVisibility(v1.GONE);
					 btn_headshotlogo.setVisibility(v1.GONE);
					((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.VISIBLE);
				}
				else
				{
					((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.GONE);
					((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.GONE);
					btn_headshotlogo.setVisibility(v1.VISIBLE);
					et_headshot.setVisibility(v1.VISIBLE);
				}
			}
			else
			{
				((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.GONE);
				((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.GONE);
				btn_headshotlogo.setVisibility(v1.VISIBLE);
				et_headshot.setVisibility(v1.VISIBLE);
			}
			signbool=true;initialbool=true;
		}		
	}
	
	
	
	private BitmapDrawable getbitmap(File OutputFile)
	{
		bmdregister=null;
		try {
			BitmapFactory.Options o1 = new BitmapFactory.Options();
			o1.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(OutputFile),null, o1);
		
		final int REQUIRED_SIZE = 200;
		int width_tmp1 = o1.outWidth, height_tmp1 = o1.outHeight;
		int scale1 = 1;
		while (true) {
			if (width_tmp1 / 2 < REQUIRED_SIZE
					|| height_tmp1 / 2 < REQUIRED_SIZE)
				break;
			width_tmp1 /= 2;
			height_tmp1 /= 2;
			scale1 *= 2;
		}
		BitmapFactory.Options o3 = new BitmapFactory.Options();
		o3.inSampleSize = scale1;
		Bitmap bitmap1 = BitmapFactory.decodeStream(new FileInputStream(
				OutputFile), null, o3);
		
		bmdregister = new BitmapDrawable(bitmap1);
		return bmdregister;
		} catch (Exception e) {
		System.out.println("catch="+e.getMessage());
			return bmdregister;
		}
	}
	protected void datewindow() {
		// TODO Auto-generated method stub
		cf.getcalender();
		DatePickerDialog dialog = new DatePickerDialog(Registration.this,new mDateSetListener(etregister_licenseexpiry),cf.mYear, cf.mMonth, cf.mDay);
		dialog.setCancelable(false);
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                  new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                       datewin=0;
                       dialog.dismiss();

                      }
                  });
		dialog.show();
	}

	protected void register_validation() {
		// TODO Auto-generated method stub
		strfirstname = etregister_firstname.getText().toString();
		strlastname = etregister_lastname.getText().toString();
		strmiddlename = etregister_middlename.getText().toString();
		straddress1 = etregister_address1.getText().toString();
		straddress2 = etregister_address2.getText().toString();
		strzipcode = etregister_zipcode.getText().toString();
		strcity = etregister_city.getText().toString();
		strstate = etregister_state.getText().toString();
		//strcounty = etregister_county.getText().toString();
		strcontactphone = etregister_contactphone.getText().toString();
		stremail = etregister_email.getText().toString();
		strlicensenum = etregister_licensenum.getText().toString();
		strlicenseexpirydate = etregister_licenseexpiry.getText().toString();
		strotherlicensetype = etregister_licensetype.getText().toString();		
		
		if(strfirstname.trim().equals(""))
		{
			cf.DisplayToast("Please enter First Name");
		}
		else
		{
			if(strfirstname.length()<3)
			{
				cf.DisplayToast("Please enter First Name at least in 3 characters");
			}
			else
			{
				if(strlastname.trim().equals(""))
				{
					cf.DisplayToast("Please enter Last Name");
				}
				else
				{
						if(!strzipcode.equals(""))
						{
							if(strzipcode.length()<5)
							{
								cf.DisplayToast("Please enter the valid Zipcode");
							}
							else
							{
								Phonenumber_validation();
							}
						}
						else
						{
							Phonenumber_validation();
						}				
				}
			}
		}
	}
	private void Phonenumber_validation() {
		// TODO Auto-generated method stub
		System.out.println("strcontactphone"+strcontactphone);
			if(!strcontactphone.trim().equals(""))
			{
				boolean phnnumbool = cf.PhonenumValidation(strcontactphone);
				if(!phnnumbool)
				{
					cf.DisplayToast("Please enter valid Phone Number");
				}
				else
				{
					Email_validation();
				}
			}
			else
			{
				Email_validation();
			}
	}

	private void Email_validation() {
		// TODO Auto-generated method stub
		System.out.println("stremail"+stremail);
		if(!stremail.trim().equals(""))
		{
			boolean emailbool = cf.EmailValidation(stremail);System.out.println("emailbool"+emailbool);
			if(!emailbool)
			{
				cf.DisplayToast("Please enter valid Email");
			}
			else
			{
				companyvalidation();	
			}
		}
		else
		{
			cf.DisplayToast("Please enter Email");
		}
	}

	private void companyvalidation() {
		// TODO Auto-generated method stub
		strcompanyname = etcompany_name.getText().toString();
		strcompanylogo = etcompany_logo.getText().toString();
		strcompanyphone = etcompany_phone.getText().toString();
		strcompanyemail = etcompany_email.getText().toString();
		strfaxnumber = etregister_fax.getText().toString();
		strwebsite = etregister_website.getText().toString();
		strheadshot = et_headshot.getText().toString();
		System.out.println("strcompanyphone"+strcompanyphone);
		if(!strcompanyphone.trim().equals(""))
		{
			boolean phnnumbool = cf.PhonenumValidation(strcompanyphone);
			if(!phnnumbool)
			{
				cf.DisplayToast("Please enter valid Contact Phone Number");
			}
			else
			{
				companyemailvalidation();
			}
		}
		else
		{
			companyemailvalidation();
		}
	}
		
		
	private void companyemailvalidation() 
		// TODO Auto-generated method stub		
	{
		System.out.println("strcompanyemail"+strcompanyemail);
		if(!strcompanyemail.trim().equals(""))
		{
			boolean emailbool = cf.EmailValidation(strcompanyemail);System.out.println("emailbool"+emailbool);
			if(!emailbool)
			{
				cf.DisplayToast("Please enter valid Email");
			}
			else
			{
				companyfaxvalidation();
			}
		}
		else
		{
			companyfaxvalidation();
		}
	}

	private void companyfaxvalidation() {
		// TODO Auto-generated method stub
		System.out.println("strfaxnumber"+strfaxnumber);
		if(!strfaxnumber.trim().equals(""))
		{
			if(strfaxnumber.trim().length()<13)
			{
				cf.DisplayToast("Please enter valid Fax number in 10 digits");
			}
			else
			{
				companywebsitevalidation();
			}
		}
		else
		{
			companywebsitevalidation();
		}	
	}

	private void companywebsitevalidation() {
		// TODO Auto-generated method stub
		System.out.println("strwebsite"+strwebsite);
		if(!strwebsite.trim().equals(""))
		{
			/*System.out.println("URLUtil.isValidUrl(strwebsite)"+URLUtil.isValidUrl(strwebsite));
			if(URLUtil.isValidUrl(strwebsite))
			{*/
				register();
			/*}
			else
			{
				cf.DisplayToast("Please enter valid website");
			}*/
		}
		else
		{
			register();
		}
	}
	private void register() {
		// TODO Auto-generated method stub
		
		  if(cf.isInternetOn())
		  {
			   new Start_xporting().execute("");
		  }
		  else
		  {
			  cf.DisplayToast("Please enable your internet connection.");
			  
		  } 

		
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   String  inspe_custom_id,username,password,name,l_name,phone,email_id,address1,address2,city,state,county,zip,lic_no,lic_type,lic_cmt,
		   bus_ph_no,bus_email,bus_add1,bus_add2,bus_city,bus_state,bus_county,bus_zip,com_com_name,com_website,com_add1,
		   com_add2,com_city,com_state,com_county,com_zip,chk_C_top_val,chk_L_topval;

			@Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="Registering";
			    p_sv.progress_Msg="Please wait we are registering your information. It may take few minutes.";
				
				startActivityForResult(new Intent(Registration.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
					Send_record_to_server();
					
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("test catc=");
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		                return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;
		    	String title="Registration";
		    	
		    	System.out.println("onprogress update"+handler_msg);
		    	if(handler_msg==0)
		    	{
		    		String msg="Sorry, you have some problem while "+title+". Please try again later.";
		    		if(!erro_msg.equals(""))
		    		{
		    			msg=erro_msg;
		    			erro_msg="";
		    		}
		    		
		    		AlertDialog al = new AlertDialog.Builder(Registration.this).setMessage(msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							//finish();
						}
					}).setTitle(title+" Failure").create();
		    		al.show();
		    		
		    		
		    	}
		    	else
		    	{
		    		if(userid.equals(""))
		    		{
			    		AlertDialog al = new AlertDialog.Builder(Registration.this).setMessage("Successfully registered your account. An email has been sent to your registered email.").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								Intent intent=new Intent(Registration.this,LoginActivity.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
								intent.putExtra("status", "register");
								intent.putExtra("user", strusername);
								startActivity(intent);
								finish();
								
							}
						}).setTitle(title+" Success").create();
			    		al.show();
		    		}
		    		else
	    			{
		    			cf.DisplayToast("Inspector Profile updated successfully.");
		    			startActivity(new Intent(Registration.this,HomeScreen.class));
		    			finish();
	    			}
		    		
		    	}
		    	
		    }
	}
	
	private void Send_record_to_server() throws IOException, XmlPullParserException {
		// TODO Auto-generated method stub
        SoapObject request = new SoapObject(wb.NAMESPACE,"InspectorRegistration");                 
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
 		envelope.dotNet = true;
 		//strupload_license = etupload_license.getText().toString();
 		System.out.println("record"+selectedImagePathlicense+"selectedImagePathcomplogo"+selectedImagePathcomplogo);
 		strupload_license=selectedImagePathlicense;
 		strcompanylogo = selectedImagePathcomplogo;
 		strheadshot = selectedImagePath;
 		
 		try
 		{
 			System.out.println("userid="+userid);
			if(!userid.equals(""))
			{
	 			/*Cursor c =db.hi_db.rawQuery(" SELECT max(Id) as id from "+db.Registration, null);
	 	 		if(c.getCount()>0)
	 	    	{
	 	    		c.moveToFirst();
	 	    		request.addProperty("InspectorId",c.getInt(c.getColumnIndex("id")));
	 	    	}
	 	 		else
	 	 		{*/
	 	 			request.addProperty("InspectorId",userid);
	 	 		//}
			}
			else
			{
				request.addProperty("InspectorId",0);
			}
 		}
 	    catch (Exception e) {
			// TODO: handle exception
		}
 		
 		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		randomid="HI_inspectorid_"+sdf.format(new Date())+"_"+rand.nextInt(1000);
 		request.addProperty("fld_userid",randomid);
 		request.addProperty("fld_firstname",strfirstname);
 		request.addProperty("fld_lastname",strlastname);
 		request.addProperty("fld_middlename",strmiddlename);
 		request.addProperty("fld_address1",straddress1);
 		request.addProperty("fld_address2",straddress2);
 		request.addProperty("fld_zipcode",strzipcode);
 		request.addProperty("fld_city",strcity);
 		request.addProperty("fld_state",strstate);
 		request.addProperty("fld_county",strcounty);
 		request.addProperty("fld_contactphone",strcontactphone);
 		request.addProperty("fld_email",stremail);
 		request.addProperty("fld_username","dsf");
 		request.addProperty("fld_password","sdf");
 		request.addProperty("fld_licensenumber",strlicensenum);
 		request.addProperty("fld_licenseexpirydate",strlicenseexpirydate);
 		request.addProperty("fld_licensetype",getlicensetype);
 		request.addProperty("fld_otherlicensetype",strotherlicensetype);
 		request.addProperty("fld_companyname",strcompanyname);
 		if(!strcompanylogo.equals(""))
 		{
 			Bitmap bm = null;
 			String FILENAME = "HILogo"+db.UserId +".png";
			File outputFile2 = new File(Environment.getExternalStorageDirectory() + "/HI/"+FILENAME);
			if(outputFile2.exists())
			{
				bm = BitmapFactory.decodeFile(outputFile2.getAbsolutePath());
			}
			else
			{
				 bm = cf.ShrinkBitmap(strcompanylogo, 200, 200);
			}
 			//bm=ShrinkBitmap(strcompanylogo, 400, 400);
 			if(bm!=null)							
			{
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bm.compress(CompressFormat.PNG, 100, out);
				byte[] raw = out.toByteArray();
				request.addProperty("fld_companylogo", raw);
			}
 			else
 			{System.out.println("else");
 				request.addProperty("fld_companylogo","");
 			}
 		}
 		else
 		{System.out.println("ends");
			request.addProperty("fld_companylogo","");
		}
 		
 		if(!strheadshot.equals(""))
 		{
 			String FILENAME = "HIHeadshot"+db.UserId +".png";
			File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/"+FILENAME);
			System.out.println("outputFile"+outputFile.exists());
			if(outputFile.exists())
			{
				bm1 = BitmapFactory.decodeFile(outputFile.getAbsolutePath());
			}
			else
			{
				 bm1 = cf.ShrinkBitmap(strheadshot, 200, 200);
			}
			
 			//bm1=ShrinkBitmap(strheadshot, 400, 400);
 			if(bm1!=null)							
			{
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bm1.compress(CompressFormat.PNG, 100, out);
				byte[] raw = out.toByteArray();
				request.addProperty("fld_headshot", raw);
			}
 			else
 			{
 				request.addProperty("fld_headshot","");
 			}
 		}
 		else
 		{
			request.addProperty("fld_headshot","");
		}
 		System.out.println("signbool="+signbool+"\n"+initialbool);
 		
 		if(signbool)
		{
			signview.setDrawingCacheEnabled(true);
			Bitmap bmsignature=Bitmap.createBitmap(signview.getDrawingCache());
			
			ByteArrayOutputStream bos=new ByteArrayOutputStream();
			bmsignature.compress(Bitmap.CompressFormat.PNG, 0, bos);
			signsignature=bos.toByteArray();
			signview.setDrawingCacheEnabled(false);		
		}
 		if(initialbool)
		{
			initialview.setDrawingCacheEnabled(true);
			Bitmap insignature=Bitmap.createBitmap(initialview.getDrawingCache());
			
			ByteArrayOutputStream is=new ByteArrayOutputStream();
			insignature.compress(Bitmap.CompressFormat.PNG, 0, is);
			initialsignature=is.toByteArray();
			initialview.setDrawingCacheEnabled(false);
		}
 		
 		request.addProperty("fld_signature",signsignature);
 		request.addProperty("fld_initials",initialsignature);
 		request.addProperty("fld_companyphone",strcompanyphone);
 		request.addProperty("fld_companyemail",strcompanyemail);
 		System.out.println("strupload_license"+strupload_license);
 		if(!strupload_license.equals(""))
 		{
 			String FILENAME = "HILicense"+db.UserId +".png";
			File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/"+FILENAME);
			System.out.println("outputFile"+outputFile.exists());
			if(outputFile.exists())
			{
				bm2 = BitmapFactory.decodeFile(outputFile.getAbsolutePath());
			}
			else
			{
 			    bm2=ShrinkBitmap(strupload_license, 400, 400);
			}
 			if(bm2!=null)							
			{
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bm2.compress(CompressFormat.PNG, 100, out);
				byte[] raw = out.toByteArray();
				request.addProperty("fld_uploadlicense", raw);
			}
 			else
 			{
 				request.addProperty("fld_uploadlicense","");
 			}
 		}
 		else
 		{
			request.addProperty("fld_uploadlicense","");
		}
 		
 		request.addProperty("fld_logout",0);
 		request.addProperty("fld_compfax",strfaxnumber);
 		request.addProperty("fld_compwebsite",strwebsite);
 		request.addProperty("CreatedDate","");
 		
 		envelope.setOutputSoapObject(request);
 		MarshalBase64 marshal = new MarshalBase64();
		marshal.register(envelope);
 		System.out.println(" the request"+request);
       // envelope.addMapping(wb.NAMESPACE, "Category",new InspectorRegistration_HomeInspection_For_Android().getClass());
        AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
        SoapObject response = null;
        try
        {
             androidHttpTransport.call(wb.NAMESPACE+"InspectorRegistration", envelope);
             response = (SoapObject)envelope.getResponse();
            System.out.println(" registrationresult "+response);
            
            if(response.getProperty("StatusCode").toString().equals("0"))
    		{
            	
            	strinspectorid =  String.valueOf(response.getProperty("InspectorId")); 
            	strfirstname = (String.valueOf(response.getProperty("fld_firstname")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_firstname"));
            	strlastname = (String.valueOf(response.getProperty("fld_lastname")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_lastname"));
            	strmiddlename = (String.valueOf(response.getProperty("fld_middlename")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_middlename"));
            	straddress1 = (String.valueOf(response.getProperty("fld_address1")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_address1"));
            	straddress2 = (String.valueOf(response.getProperty("fld_address2")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_address2"));
            	strzipcode = (String.valueOf(response.getProperty("fld_zipcode")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_zipcode"));
            	strcity = (String.valueOf(response.getProperty("fld_city")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_city"));        	
            	strstate = (String.valueOf(response.getProperty("fld_state")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_state"));
            	strcounty = (String.valueOf(response.getProperty("fld_county")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_county"));
            	strcontactphone = (String.valueOf(response.getProperty("fld_contactphone")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_contactphone"));
            	stremail = (String.valueOf(response.getProperty("fld_email")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_email"));
            	strlicensenum = (String.valueOf(response.getProperty("fld_licensenumber")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_licensenumber"));
            	strlicenseexpirydate = (String.valueOf(response.getProperty("fld_licenseexpirydate")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_licenseexpirydate"));
            	getlicensetype = (String.valueOf(response.getProperty("fld_licensetype")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_licensetype"));
            	strotherlicensetype = (String.valueOf(response.getProperty("fld_otherlicensetype")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_otherlicensetype"));
            	strcompanyname = (String.valueOf(response.getProperty("fld_companyname")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_companyname"));
            	strcompanylogo = (String.valueOf(response.getProperty("fld_companylogo")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_companylogo"));
            	strheadshot = (String.valueOf(response.getProperty("fld_headshot")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_headshot"));
            	strsignature = (String.valueOf(response.getProperty("fld_signature")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_signature"));
            	strintitials = (String.valueOf(response.getProperty("fld_initials")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_initials"));
            	strcompanyemail = (String.valueOf(response.getProperty("fld_companyemail")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_companyemail"));
            	strcompanyphone = (String.valueOf(response.getProperty("fld_companyphone")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_companyphone"));
            	strupload_license = (String.valueOf(response.getProperty("fld_uploadlicense")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_uploadlicense"));
            	strfaxnumber = (String.valueOf(response.getProperty("fld_compfax")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_compfax"));
            	strwebsite = (String.valueOf(response.getProperty("fld_compwebsite")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_compwebsite"));
            	 	
            	strusername =  String.valueOf(response.getProperty("fld_username")); 
            	strpassword =  String.valueOf(response.getProperty("fld_password"));
            	
            	
            	File direct = new File(Environment.getExternalStorageDirectory() + "/HI");

    		    if(!direct.exists())
    		    {
    		         direct.mkdir() ;
    		    }
    		    
        		 try
    			 {
        			 String FILENAME="";
    				 if(!strheadshot.equals(""))
    				 {
    						URL ulrn = new URL(strheadshot);
    					    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
    					    InputStream is = con.getInputStream();
    					    Bitmap bmp = BitmapFactory.decodeStream(is);
    					   
    					    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
    					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
    					    byte[] b = baos.toByteArray();
    					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
    					    byte[] decode = Base64.decode(headshot_insp.toString(), 0);
    					    if(userid.equals(""))
    					    {
    					    	FILENAME = "HIHeadshot"+strinspectorid+".png";
    					    }
    					    else
    					    {
    					    	FILENAME = "HIHeadshot"+userid+".png";
    					    }
    					    
    			        	  File f = new File(direct,FILENAME);				        	  
    			        	  FileOutputStream fos = new FileOutputStream(f);
    			        	  fos.write(decode);
    				 }
    			}
    			catch (Exception e1){
    				System.out.println("the exception in headshot is "+e1.getMessage());
    		
    			}
        		 
        		 try
    			 {
        			 String FILENAME1="";
    					 if(!strcompanylogo.equals(""))
    					 {
    						URL ulrn = new URL(strcompanylogo);
    					    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
    					    InputStream is = con.getInputStream();
    					    Bitmap bmp = BitmapFactory.decodeStream(is);
    					   
    					    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
    					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
    					    byte[] b = baos.toByteArray();
    					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
    					    byte[] decode = Base64.decode(headshot_insp.toString(), 0);
    					    if(userid.equals(""))
    					    {
    					    	FILENAME1 = "HILogo"+strinspectorid+".png";
    					    }
    					    else
    					    {
    					    	FILENAME1 = "HILogo"+userid+".png";
    					    }
    					    File f = new File(direct,FILENAME1);					        	  
    			        	FileOutputStream fos = new FileOutputStream(f);
    						fos.write(decode);
    						fos.close();	
    					 }
    				    
    				}
    				catch (Exception e1){
    					
    				}
        		 
        		 try
    				{
        			 System.out.println("strsignature="+strsignature);
        			 String FILENAME2="";
    					 if(!strsignature.equals(""))
    					 {
    						URL ulrn = new URL(strsignature);
    					    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
    					    InputStream is = con.getInputStream();
    					    Bitmap bmp = BitmapFactory.decodeStream(is);
    					   
    					    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
    					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
    					    byte[] b = baos.toByteArray();
    					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
    					    byte[] decode = Base64.decode(headshot_insp.toString(), 0);
    					    if(userid.equals(""))
    					    {
    					    	FILENAME2 = "HISignature"+strinspectorid+".png";
    					    }
    					    else
    					    {
    					    	FILENAME2 = "HISignature"+userid+".png";
    					    }
    					    File f = new File(direct,FILENAME2);					        	  
    			        	FileOutputStream fos = new FileOutputStream(f);
    						fos.write(decode);
    						fos.close();	
    					 }
    				    
    				}
    				catch (Exception e1){
    					
    				}
        		 
        		 try
    			 {
        			 String FILENAME3="";
    					 if(!strintitials.equals(""))
    					 {
    						URL ulrn = new URL(strintitials);
    					    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
    					    InputStream is = con.getInputStream();
    					    Bitmap bmp = BitmapFactory.decodeStream(is);
    					   
    					    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
    					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
    					    byte[] b = baos.toByteArray();
    					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
    					    byte[] decode = Base64.decode(headshot_insp.toString(), 0);
    					    if(userid.equals(""))
    					    {
    					    	FILENAME3 = "HIInitials"+strinspectorid+".png";
    					    }
    					    else
    					    {
    					    	FILENAME3 = "HIInitials"+userid+".png";
    					    }
    					    File f = new File(direct,FILENAME3);					        	  
    			        	FileOutputStream fos = new FileOutputStream(f);
    						fos.write(decode);
    						fos.close();	
    					 }
    				    
    				}
    				catch (Exception e1){
    					
    				}
        		 
        		 try
    				{
        			 System.out.println("try="+strupload_license);
        			 String FILENAME4="";
    					 if(!strupload_license.equals(""))
    					 {
    						URL ulrn = new URL(strupload_license);
    					    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
    					    InputStream is = con.getInputStream();
    					    Bitmap bmp = BitmapFactory.decodeStream(is);
    					   
    					    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
    					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
    					    byte[] b = baos.toByteArray();
    					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
    					    byte[] decode = Base64.decode(headshot_insp.toString(), 0);
    					    if(userid.equals(""))
    					    {
    					    	FILENAME4 = "HILicense"+strinspectorid+".png";
    					    }
    					    else
    					    {
    					    	FILENAME4 = "HILicense"+userid+".png";
    					    }
    					    
    					    
    					    
    					    File f = new File(direct,FILENAME4);					        	  
    			        	FileOutputStream fos = new FileOutputStream(f);
    						fos.write(decode);
    						fos.close();	
    					 }
    				    
    				}
    				catch (Exception e1){
    					System.out.println("erroe="+e1.getMessage());
    				}
            	
            	Registration_insert();
    			handler_msg=1;
    		}
    		else
    		{
    			handler_msg=0;
    			erro_msg=response.getProperty("StatusMessage").toString();
    		}
        }
        catch(Exception e)
        {
        	handler_msg=0;
        	System.out.println("inside reg catch="+e.getMessage());
            e.printStackTrace();
        }
	}
	protected void signclear() {
		// TODO Auto-generated method stub
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		String Path3 =extStorageDirectory+"/HI/HISignature"+userid+".png";
		File outputFile4 = new File(Path3);
		if(outputFile4.exists())
		{
			outputFile4.delete();
		}
		strsignature="";
		signview.setDrawingCacheEnabled(true);
		signview.clear();
		signview.setDrawingCacheEnabled(false);
		signview.setBackgroundColor(Color.parseColor("#CBCBCB"));
		signbool=false;
	}
	public void Registration_insert() {
		// TODO Auto-generated method stub
		
		try
		{
			System.out.println("license="+db.encode(strupload_license));
			Cursor cur1 = db.hi_db.rawQuery("select * from " + db.Registration + " where fld_userid='"+userid+"'",null);
            if(cur1.getCount()>0)
            {
            	db.hi_db.execSQL("UPDATE "+ db.Registration +"  SET fld_first" +
            			"name='"+db.encode(strfirstname) + "',fld_lastname='"+db.encode(strlastname)+"',fld_middlename='"+db.encode(strmiddlename)+"'," +
            			"fld_address1='"+db.encode(straddress1) + "',fld_address2='"+ db.encode(straddress2) + "',fld_zipcode='"+ db.encode(strzipcode) + "',fld_city='"+db.encode(strcity) + "'," +
            			"fld_state='"+ db.encode(strstate) + "',fld_county='"+db.encode(strcounty)+ "',fld_contactphone='"+db.encode(strcontactphone) + "',fld_email='"+db.encode(stremail)+"'," +
            			"fld_username='"+db.encode(strusername)+"',fld_password='"+db.encode(strpassword)+"',fld_licensenumber='"+db.encode(strlicensenum)+"'," +
            			"fld_licenseexpirydate='"+db.encode(strlicenseexpirydate)+"',fld_licensetype='"+db.encode(getlicensetype)+"',fld_otherlicensetype='"+db.encode(strotherlicensetype)+"'," +
            			"fld_companyname='"+db.encode(strcompanyname)+"',fld_companylogo='"+db.encode(strcompanylogo)+"',fld_headshot='" +db.encode(strheadshot) + "',fld_signature='"+db.encode(strsignature)+"'," +
            			"fld_initials='"+db.encode(strintitials)+"',fld_companyphone='"+db.encode(strcompanyphone)+"',fld_companyemail='"+db.encode(strcompanyemail)+"',fld_uploadlicense='"+db.encode(strupload_license)+"'," +
            			"fld_logout='1',fld_compfax='"+db.encode(strfaxnumber)+"',fld_compwebsite='"+db.encode(strwebsite)+"' where fld_userid='"+userid+"'");
            }
            else
            {
            	  db.hi_db.execSQL("INSERT INTO "
  						+ db.Registration
  						+ " (fld_userid,fld_firstname,fld_lastname,fld_middlename,fld_address1,fld_address2,fld_zipcode,fld_city,fld_state," +
  						"fld_county,fld_contactphone,fld_email,fld_username,fld_password,fld_licensenumber,fld_licenseexpirydate,fld_licensetype,fld_otherlicensetype,fld_companyname,fld_companylogo," +
  						"fld_headshot,fld_signature,fld_initials,fld_companyphone,fld_companyemail,fld_uploadlicense,fld_compfax,fld_compwebsite)"
  						+ " VALUES ('" + strinspectorid + "','"+db.encode(strfirstname) + "','"+db.encode(strlastname)+"','"+db.encode(strmiddlename)+"',"+
  						"'"+db.encode(straddress1) + "','"+ db.encode(straddress2) + "','"+ db.encode(strzipcode) + "','"+db.encode(strcity) + "',"+
  						"'"+ db.encode(strstate) + "','"+db.encode(strcounty)+ "','"+db.encode(strcontactphone) + "','"+db.encode(stremail)+"','"+db.encode(strusername)+"','"+db.encode(strpassword)+"'," +
  						"'"+db.encode(strlicensenum)+"','"+db.encode(strlicenseexpirydate)+"','"+db.encode(getlicensetype)+"','"+db.encode(strotherlicensetype)+"','"+db.encode(strcompanyname)+"'," +
  						"'"+db.encode(strcompanylogo)+"','" +db.encode(strheadshot) + "','','',"+
  						"'"+db.encode(strcompanyphone)+"','"+db.encode(strcompanyemail)+"','"+db.encode(strupload_license)+"','"+db.encode(strfaxnumber)+"','"+db.encode(strwebsite)+"')");
  		    }
		}
		catch (Exception e) {
			// TODO: handle exception
		}		
	}

	protected void initialsclear() {
		// TODO Auto-generated method stub
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		String path = extStorageDirectory+"/HI/HIInitials"+userid+".png";
		File outfile = new File(path);
		if(outfile.exists())
		{
			outfile.delete();
		}
		
		strintitials="";
		initialview.setDrawingCacheEnabled(true);
		initialview.clear();
		initialview.setDrawingCacheEnabled(false);
		initialview.setBackgroundColor(Color.parseColor("#CBCBCB"));
		initialbool=false;
	}
	protected void Gallery_Camera_Dialog() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(Registration.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.galleryalert);
		dialog.setCancelable(false);
		dialog.getWindow().getAttributes().windowAnimations = R.style.up_animation;
		
		Button btnchoosefromgallery = (Button) dialog.findViewById(R.id.alert_choosefromgallery);
		Button btntakeapicturefromcamera = (Button) dialog.findViewById(R.id.alert_takeapicturefromcamera);
		ImageView ivclose = (ImageView) dialog.findViewById(R.id.alert_helpclose);
		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				pic =0;
			}
		});
		btnchoosefromgallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				imageidentifier = "uploadaphoto";
				Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
				i.setType("image/*");
				startActivityForResult(i, 0);

			}
		});
		btntakeapicturefromcamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				imageidentifier = "uploadaphoto";
				String fileName = "temp.jpg";
				
				 Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                 intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                 startActivityForResult(intent, CAPTURE_IMAGE);
        
			}
		});
		dialog.show();

	}
	protected Uri  setImageUri() {
		// TODO Auto-generated method stub
		  File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
          Uri imgUri = Uri.fromFile(file);
          this.imgPath = file.getAbsolutePath();
          
			System.out.println("imgPath="+imgPath);
		return imgUri;
	}
	public String getImagePath() {
        return imgPath;
    }
	 @Override
	    protected void onSaveInstanceState(Bundle outState) {
		 
		 outState.putInt("datewin",datewin);
		 outState.putInt("picwin", pic);
		 outState.putBoolean("bsign", signbool);
		 if(signbool)
		 {
			 if(signview.isDrawingCacheEnabled())
			 {			 System.out.println("signinside");
			    signview.setSaveEnabled(true);
				 Bitmap bmsignature=Bitmap.createBitmap(signview.getDrawingCache());
				 ByteArrayOutputStream bos=new ByteArrayOutputStream();
					bmsignature.compress(Bitmap.CompressFormat.PNG, 0, bos);
					signsignature=bos.toByteArray();
					
				 outState.putByteArray("bsignature", signsignature);
			 }
		 }
		 outState.putBoolean("binitials", initialbool);
		 if(initialbool)
		 {
			 if(initialview.isDrawingCacheEnabled())
			 {			 System.out.println("intialinside");
			   initialview.setSaveEnabled(true);
			 Bitmap bminitial=Bitmap.createBitmap(initialview.getDrawingCache());
			 ByteArrayOutputStream bos1=new ByteArrayOutputStream();
			 bminitial.compress(Bitmap.CompressFormat.PNG, 0, bos1);
				initialsignature=bos1.toByteArray();
				
			 outState.putByteArray("binitial", initialsignature);
			 }
		 }	 
            
	        outState.putString("photo_index", imgPath);
	        outState.putInt("pi",pi);
	        outState.putInt("dellic", del_lic);
	        outState.putInt("delcomp", del_comp);
	        outState.putInt("delhead", del_head);
	        outState.putString("licensepath", selectedImagePathlicense);
	        outState.putString("companylogopath", selectedImagePathcomplogo);
	        outState.putString("headshotpath", selectedImagePath);
	       
	       super.onSaveInstanceState(outState);
	    }
	 @Override
	    protected void onRestoreInstanceState(Bundle savedInstanceState) {
		      datewin = savedInstanceState.getInt("datewin");
		      if(datewin==1)
		      {
		    	  datewindow();
		      }
		      pic = savedInstanceState.getInt("picwin");
		      if(pic==1)
		      {
		    	  Gallery_Camera_Dialog();
		      }
		      imgPath = savedInstanceState.getString("photo_index");
		      pi=savedInstanceState.getInt("pi");
		      signbool = savedInstanceState.getBoolean("bsign");
		      if(signbool)
		      {
		    	  signsignature = savedInstanceState.getByteArray("bsignature");
		    	  if(signsignature!=null)
			      {
		    	  Bitmap bitmap=BitmapFactory.decodeByteArray(signsignature, 0, signsignature.length);
				   
			       Bitmap bmsignature=Bitmap.createBitmap(bitmap);
			       Bitmap mutableBitmap = bmsignature.copy(Bitmap.Config.ARGB_8888, true);
			       Canvas c = new Canvas(mutableBitmap);
			       
			       Paint textPaint = new Paint();
			     
			       c.drawBitmap(bmsignature, 0, 0, textPaint);

			       signview.buildDrawingCache();
			       signview.setDrawingCacheEnabled(true);
			       bmdregister = new BitmapDrawable(bitmap);
			     	if(bmdregister!=null)
					{
						signview.setBackgroundDrawable(bmdregister);
					}	
			      }
		      }
		      initialbool = savedInstanceState.getBoolean("binitials");
		      if(initialbool)
		      {
		    	  initialsignature = savedInstanceState.getByteArray("binitial");
		    	  if(initialsignature!=null)
			      {
		    	  
		    	  Bitmap bitmap=BitmapFactory.decodeByteArray(initialsignature, 0, initialsignature.length);
				   
			       Bitmap bminitial=Bitmap.createBitmap(bitmap);
			       Bitmap mutableBitmap1 = bminitial.copy(Bitmap.Config.ARGB_8888, true);
			       Canvas c = new Canvas(mutableBitmap1);
			       
			       Paint textPaint = new Paint();
			     
			       c.drawBitmap(bminitial, 0, 0, textPaint);

			       initialview.buildDrawingCache();
			       initialview.setDrawingCacheEnabled(true);
			       bmdregister = new BitmapDrawable(bitmap);
			     	if(bmdregister!=null)
					{
			     		initialview.setBackgroundDrawable(bmdregister);
					}	
			      }
				
		      }
		   
			 del_lic = savedInstanceState.getInt("dellic");
			 if(del_lic==1)
			 {
				 delete_lic();
			 }
			 
			 del_comp = savedInstanceState.getInt("delcomp");
			 if(del_comp==1)
			 {
				 delete_comp();
			 }
			 
			 del_head = savedInstanceState.getInt("delhead");
			 if(del_head==1)
			 {
				 delete_head();
			 }
			 
			 selectedImagePathlicense = savedInstanceState.getString("licensepath");System.out.println("onrestore="+selectedImagePathlicense);
			 if(selectedImagePathlicense.equals(""))
			 {
				 etupload_license.setVisibility(v1.VISIBLE);
				 btn_uploadlicense.setVisibility(v1.VISIBLE);
				 ((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.GONE);
				 ((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.GONE);
			 }
			 else
			 {
				    String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
					String Path =extStorageDirectory+"/HI/HILicense"+userid+".png";

					File outputFile1 = new File(Path);
					if(outputFile1.exists())
					{
						bmdregister=getbitmap(outputFile1);
						if(bmdregister!=null)
						{
							((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.VISIBLE);
							 ((ImageView) findViewById(R.id.license_photo)).setImageDrawable(bmdregister);
						}
					}
					else
					{
				      Bitmap bmd = BitmapFactory.decodeFile(selectedImagePathlicense);
					  ((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.VISIBLE);
					  ((ImageView) findViewById(R.id.license_photo)).setImageBitmap(bmd);
					}
					 etupload_license.setVisibility(v1.GONE);
					 btn_uploadlicense.setVisibility(v1.GONE);
					 ((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.VISIBLE);
			 }
			 
			 selectedImagePathcomplogo = savedInstanceState.getString("companylogopath");System.out.println("onrestoreselectedImagePathcomplogo"+selectedImagePathcomplogo);
			 if(selectedImagePathcomplogo.equals(""))
			 {
				 etcompany_logo.setVisibility(v1.VISIBLE);
				 btn_companylogo.setVisibility(v1.VISIBLE);
				 ((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.GONE);
				 ((ImageView) findViewById(R.id.complogo)).setVisibility(v1.GONE);
			 }
			 else
			 {
				    String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
					String Path =extStorageDirectory+"/HI/HILogo"+userid+".png";
					System.out.println("Path="+Path);

					File outputFile1 = new File(Path);System.out.println("outputFile1.exists()"+outputFile1.exists());
					if(outputFile1.exists())
					{
						bmdregister=getbitmap(outputFile1);
						if(bmdregister!=null)
						{
							((ImageView) findViewById(R.id.complogo)).setVisibility(v1.VISIBLE);
							 ((ImageView) findViewById(R.id.complogo)).setImageDrawable(bmdregister);
						}
					}
					else
					{
				       Bitmap bmd = BitmapFactory.decodeFile(selectedImagePathcomplogo);
					  ((ImageView) findViewById(R.id.complogo)).setVisibility(v1.VISIBLE);
					  ((ImageView) findViewById(R.id.complogo)).setImageBitmap(bmd);
					}
					 etcompany_logo.setVisibility(v1.GONE);
					 btn_companylogo.setVisibility(v1.GONE);
					 ((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.VISIBLE);
			 }
			 
			 selectedImagePath = savedInstanceState.getString("headshotpath");
			 if(selectedImagePath.equals(""))
			 {
				 et_headshot.setVisibility(v1.VISIBLE);
				 btn_headshotlogo.setVisibility(v1.VISIBLE);
				 ((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.GONE);
				 ((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.GONE);
			 }
			 else
			 {
				    String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
					String Path =extStorageDirectory+"/HI/HIHeadshot"+userid+".png";

					File outputFile1 = new File(Path);
					if(outputFile1.exists())
					{
						bmdregister=getbitmap(outputFile1);
						if(bmdregister!=null)
						{
							((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.VISIBLE);
							 ((ImageView) findViewById(R.id.headshot_photo)).setImageDrawable(bmdregister);
						}
					}
					else
					{
					     Bitmap bmd = BitmapFactory.decodeFile(selectedImagePath);
						 ((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.VISIBLE);
						 ((ImageView) findViewById(R.id.headshot_photo)).setImageBitmap(bmd);
					}
					 et_headshot.setVisibility(v1.GONE);
					 btn_headshotlogo.setVisibility(v1.GONE);
					 ((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.VISIBLE);
			 }
	        super.onRestoreInstanceState(savedInstanceState);
	    }

	 public Bitmap ShrinkBitmap(String string, int i, int j) {
			// TODO Auto-generated method stub
			  
				try {
					BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
					bmpFactoryOptions.inJustDecodeBounds = true;
				    bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);

					int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
							/ (float) j);
					int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
							/ (float) i);
	               
					if (heightRatio > 1 || widthRatio > 1) {
						if (heightRatio > widthRatio) {
							bmpFactoryOptions.inSampleSize = heightRatio;
						} else {
							bmpFactoryOptions.inSampleSize = widthRatio;
						}
					}

					bmpFactoryOptions.inJustDecodeBounds = false;
					bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);
					//System.out.println("bitmap="+bitmap);
					return bitmap;
				} catch (Exception e) {
				System.out.println("catch="+e.getMessage());
					return bitmap;
				}
				
				

		}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		pic=0;
		if (requestCode == 0 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String filePath = cursor.getString(columnIndex);
			
			
			File f = new File(filePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			
			if (mb >= 2) {
				cf.DisplayToast("File size exceeds!! Too large to attach");
			} else {
				Bitmap bm = decodeFile(filePath);
				
				System.out.println("The bitmap is "+bm);
				if(bm==null)
				{
					cf.DisplayToast("File corrupted!! Cant able to attach");
				}
				else
				{
					if(pi==1)
					{	
						selectedImagePath = filePath;
						String[] bits = selectedImagePath.split("/");
						headshotpicname = bits[bits.length - 1];
						//et_headshot.setText(selectedImagePath);
						
						Bitmap bmd = BitmapFactory.decodeFile(selectedImagePath);
						((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.VISIBLE);
						 ((ImageView) findViewById(R.id.headshot_photo)).setImageBitmap(bmd);
						 et_headshot.setVisibility(v1.GONE);
						 btn_headshotlogo.setVisibility(v1.GONE);
						 ((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.VISIBLE);
					}
					else if(pi==0)
					{
						selectedImagePathcomplogo = filePath;
						String[] bits1 = selectedImagePathcomplogo.split("/");
						complogopicname = bits1[bits1.length - 1];
						//etcompany_logo.setText(selectedImagePathcomplogo);
						Bitmap bmd = BitmapFactory.decodeFile(selectedImagePathcomplogo);
						((ImageView) findViewById(R.id.complogo)).setVisibility(v1.VISIBLE);
						 ((ImageView) findViewById(R.id.complogo)).setImageBitmap(bmd);
						 etcompany_logo.setVisibility(v1.GONE);
						 btn_companylogo.setVisibility(v1.GONE);
						 ((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.VISIBLE);
					}
					else if(pi==2)
					{
						selectedImagePathlicense = filePath;
						String[] bits2 = selectedImagePathlicense.split("/");
						licensepicname = bits2[bits2.length - 1];
						//etupload_license.setText(selectedImagePathlicense);
						Bitmap bmd = BitmapFactory.decodeFile(selectedImagePathlicense);
						((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.VISIBLE);
						 ((ImageView) findViewById(R.id.license_photo)).setImageBitmap(bmd);
						 etupload_license.setVisibility(v1.GONE);
						 btn_uploadlicense.setVisibility(v1.GONE);
						 ((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.VISIBLE);
					}
				
				}
			}
		}
		
		else if (requestCode == CAPTURE_IMAGE && resultCode == RESULT_OK) {
			 String capselectedImagePath = getImagePath();
			 System.out.println("imgPath="+imgPath);
			 System.out.println("selectedImagePath= c"+capselectedImagePath);
			 File f = new File(capselectedImagePath);
				double length = f.length();
				double kb = length / 1024;
				double mb = kb / 1024;
				
				if (mb >= 2) {
					cf.DisplayToast("File size exceeds!! Too large to attach");
				} else {
					try
					{
						Bitmap bitmapdb = decodeFile(capselectedImagePath);
						System.out.println("bitmapdb="+bitmapdb);
						if(bitmapdb==null)
						{
							cf.DisplayToast("File corrupted!! Cant able to attach");
						}
						else
						{
							System.out.println("pi="+pi);
							if(pi==1)
							{	System.out.println("pione");
								selectedImagePath = capselectedImagePath;
								String[] bits = selectedImagePath.split("/");
								headshotpicname = bits[bits.length - 1];
								System.out.println("headshotpicname"+headshotpicname);
								//et_headshot.setText(selectedImagePath);
								
								Bitmap bmd = BitmapFactory.decodeFile(selectedImagePath);
								((ImageView) findViewById(R.id.headshot_photo)).setVisibility(v1.VISIBLE);
								 ((ImageView) findViewById(R.id.headshot_photo)).setImageBitmap(bmd);
								 et_headshot.setVisibility(v1.GONE);
								 btn_headshotlogo.setVisibility(v1.GONE);
								 ((Button) findViewById(R.id.deleteheadshot)).setVisibility(v1.VISIBLE);
							}
							else if(pi==0)
							{   System.out.println("Pielse");
								selectedImagePathcomplogo = capselectedImagePath;
								String[] bits1 = selectedImagePathcomplogo.split("/");
								complogopicname = bits1[bits1.length - 1];
								System.out.println("complogopicname"+complogopicname);
								//etcompany_logo.setText(selectedImagePathcomplogo);
								
								Bitmap bmd = BitmapFactory.decodeFile(selectedImagePathcomplogo);
								((ImageView) findViewById(R.id.complogo)).setVisibility(v1.VISIBLE);
								 ((ImageView) findViewById(R.id.complogo)).setImageBitmap(bmd);
								 etcompany_logo.setVisibility(v1.GONE);
								 btn_companylogo.setVisibility(v1.GONE);
								 ((Button) findViewById(R.id.deletecomplogo)).setVisibility(v1.VISIBLE);
							}
							else if(pi==2)
							{
								selectedImagePathlicense = capselectedImagePath;
								String[] bits2 = selectedImagePathlicense.split("/");
								licensepicname = bits2[bits2.length - 1];
								//etupload_license.setText(selectedImagePathlicense);
								
								Bitmap bmd = BitmapFactory.decodeFile(selectedImagePathlicense);
								((ImageView) findViewById(R.id.license_photo)).setVisibility(v1.VISIBLE);
								 ((ImageView) findViewById(R.id.license_photo)).setImageBitmap(bmd);
								 etupload_license.setVisibility(v1.GONE);
								 btn_uploadlicense.setVisibility(v1.GONE);
								 ((Button) findViewById(R.id.deletelicphoto)).setVisibility(v1.VISIBLE);
							}
						}
					}
					catch (OutOfMemoryError e) {
						// TODO: handle exception
						System.out.println("error="+e.getMessage());
						cf.DisplayToast("File size exceeds!! Too large to attach");
					}
				}
			 
		
		}
	
	}
	private static Bitmap decodeFile(String file) {
	    try {
	    	
	    	File f=new File(file);	    	
	        // Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

	        // The new size we want to scale to
	        final int REQUIRED_SIZE = 150;

	        // Find the correct scale value. It should be the power of 2.
	        int scale = 1;
	        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
	                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
	            scale *= 2;

	        // Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize = scale;
	        o.inJustDecodeBounds = false;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {
	    }

	    return null;
	}


	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
	    	getlicensetype = parent.getItemAtPosition(pos).toString();
	    	if(getlicensetype.equals("Other"))
	    	{
	    		trlicensetype.setVisibility(view.VISIBLE);
	    		etregister_licensetype.setVisibility(view.VISIBLE);
	    		etregister_licensetype.requestFocus();
	    	}
	    	else
	    	{
	    		etregister_licensetype.setText("");
	    		etregister_licensetype.clearFocus();
	    		etregister_licensetype.setVisibility(view.GONE);
	    		trlicensetype.setVisibility(view.GONE);
	    	}
	    		
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	class mDateSetListener implements DatePickerDialog.OnDateSetListener {
		EditText v;

		mDateSetListener(EditText v) {
			this.v = v;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			// TODO Auto-generated method stub
			
			int mYear = year;
			int mMonth = monthOfYear;
			int mDay = dayOfMonth;
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			System.out.println(v.getText().toString());

			Date date1 = null, date2 = null;
			try {
				String currentdate = (cf.mMonth + 1) + "/" + cf.mDay + "/" + cf.mYear;
				
				System.out.println("Inside try");
				String formatString = "MM/dd/yyyy";
				SimpleDateFormat df = new SimpleDateFormat(formatString);
				date1 = df.parse(currentdate);
				date2 = df.parse(v.getText().toString());
				System.out.println("current date " + date1);
				System.out.println("selected date " + date2);
				if (date2.compareTo(date1) < 0) {
					System.out.println("inside date if");
					cf. DisplayToast("Please select Current and Future Date");
					v.setText("");
					datewin=0;
				} else {
					System.out.println("inside date else");
					datewin=0;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		startActivity(new Intent(Registration.this,LoginActivity.class));
		finish();
	}
}
