package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NewHVAC1 extends Activity {
	String gettempname="",getcurrentview="",getsysbrands="",getmodulename="",gettempoption="",getselectedho="",fueltankvalue="",finalselstrval="",tempfueltankloc="",getnoofzones="",getheatbrand="",
			seltdval="",tempfluetype="",temptestedfor="",tempairhandlerfeat="",tempthermolocation="",tempthermotype="",tempductworktype="",tempconddischarge="",getzoneno="",
					getfurnace="",getmotorblower="",getsupplyregister="",getreturnregister="",getmultizonesystem="",str="",fluetypevalue="",hvacvalue="",
							getairhandler="",getfueltankloc="",gettestedfor="",conddischangevalue="",thermolocationvalue="",thermostattypevalue="",ductworktypevalue="",airhandlerfeatvalue="",testedforvalue="";
	ScrollView scr;
	TableLayout heatingtblshow;
	Spinner spinloadtemplate;
	View v;
	LinearLayout lin;
	String arrtempname[];
	LinearLayout otheredtlayout;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	String word="",error_msg="",tempname="",selectvalfueltype="",selectvalueairhandler="",selectvalfueltypeloc="",selectvaltestedfor="",strzoneno="";
	TextView fueltanklocnote,fluetypenote,testedfornote,thermostatlocationtv,airhandlerfeatnote,ductworknote,thermostattypetv,conddisctv;
	int hvac=0,hvacCurrent_select_id,editid=0,getisedit=0,submitalert=0,handler_msg=2,spintouch=0,chk=0,fueltank_dialog=0,fluetype_dialog=0,testedfordialog=0,airhandler_dialog=0;
	static int k=0;
	TextView fueltanktv,fluetypetv,testedfortv,airhandlertv,ductworktypetv,thermolocationtv,thermotypetv,condensdischargetv;
	private static final Random rgenerator = new Random();
	CheckBox ch[];
	CheckBox chknotapplicable;
	Button btntakephoto;
	ArrayAdapter adapter;
	Progress_staticvalues p_sv;
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	Spinner sp_heatingtype,sp_energysource,sp_gasshutoff,spin_noofzones,spin_heatingage,spin_zoneno,sp_zonename,sp_heatbrand,sp_furnace,
	sp_motorblower,sp_heatdelivery,sp_zonestestedfor,spin_supplyregister,sp_returnregister,sp_ductworktype,sp_multizonesystem,sp_zonetonnage,sp_btu,
	sp_thermotype,sp_thermolocation,sp_externalunitreplprob,sp_internalunitreplprob;
	String getheatdeliverytype="",getspinselectedname="",getenergysource="",strenergysource="",getheatingtype="",getheatingage="",strotherheatingage="",strotherheattype="",getgasshutoff="",strgasshutoff="",strfluetype="",
			getfluetype="",getzonename="",strzonename="",getzonetonnage="",strzonetonnage="",getbtu="",strbtu="",strexternalunit="",strinternalunit="",getinternalunit="",getexternalunit="";
	EditText wrdedit,ed_otherzoneno,ed_otherbtu,ed_otherzonetonnage,ed_otherheattype,ed_otherenergysource,ed_othergasshutoff,ed_otherfluetype,ed_otherheatingage,ed_otherzonename;
	
	static String[] arrfueltank={"Above Ground","Below Ground","Not Applicable","Above Ground Oil Tank","Below Ground Oil Tank","Original Oil Tank",
		"Oil Tank Fill Cap and Vent Not Located","Tank Fill and Vent Located","Portable Gas Tank","Gas Tank Below Ground","Underground Gas Tank Fill Valve Not Located",
		"Underground Gas Tank Not Located","Underground Gas Tank"},
					arrfluetype={"Unlined Masonry Chimney","Old Masonry Chimney Redundant","PVC Schedule 40","No Flue Cap","Metal Flue Cap","Flexible Metal Gas Flue","Masonry Chimney",
				"Clay Flue Liner","Metal Flue"},
		arrductworktype={"Flexible Ductwork","Metal Ductwork","Slab Ductwork","Insulated","Ductwork in Crawl SPace","Automatic Dampers(not tested)","Untidy Ductwork Installation Leakage Evident"},
		arrzonetestingfor={"Tested For Cooling","Tested For Heating","Tested For Emergency Heat Only","Reverse valve not tested", "Outside temperature prohibited test"},
				arrthermolocation={"Hall", "Master Bedroom","Living Room", "Upstairs","Bonus", "Other"},
				arrthermotype={"Manual Thermostat","Programmable Thermostat","Set Back Thermostat","Heat Pump Thermostat","Multiple Zone","Separate Heating/Cooling Thermostat","Aquastat"},
				arrcondendischarge={"Overflow Condensate Tray","Condensate Pipe To Sump","Condensate Pipe","Condensate Pipe To Exterior","Float Switch/Pump (not tested)"},
		arrairhandlerfeatures={"Air Filter","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner","Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light","Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Condensation Discharger"};
	
	String[] arrheatingnum={"--Select--","1","2","3","4","5","6","Other"},strheatsytem ={"--Select--","Adequate","Inadequate","Appears Functional","Recommend Replacement"},
			arrheattype={"--Select--","Forced Air","Heat Pump Forced Air","Central Air - Cool/Air Conditioning Only","Packaged Central Air - Heat pump","Packaged Central Air - Cool/Air Conditioning Only","Wall Unit(s)","Heat Strip","None",
			"Electric Base","Electric Heat","Radiant Floor","Space Heater","Steam Boiler","Circulating Boiler","Coal Furnace Converted to Gas","Coal Furnace converted to Oil",
			"Hydroninc System","Oil Furnace converted to Gas","Old Oil Furnace as Back up to heat pump","Radiant  Ceiling","Window Unit","Swamp Cooler","Water to Air system",
			"Cooling Only System","Uses Heat Ductwork","Refrigerant System","System Aged/Operable","Other"},
			arrgasshutoff={"--Select--","Front Side","Left Side","Right Side","Rear side","Adjacent Tank","Adjacent meter","Other"},
			
			
			arrzonename={"--Select--","Bonus Room","Main Area","Master Bedroom","Second Floor","Other"},
			arrcoolingage={"--Select--","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years",
			"11 Year","12 Years","13 Years","14 Years","15 Years","16 Years","17 Years","18 Years","19 Years","20 Years",
			"21 Year","22 Years","23 Years","24 Years","25 Years","26 Years","27 Years","28 Years","29 Years","30 Years","Other"},
			arrheatexchanger={"--Select--","1 Burner","2 Burner","3 Burner",
			"4 Burner","5 Burner","6 Burner","Other"},arrdistribution={"--Select--","Hot Water","Steam","Radiator","Metal Duct","One Pipe","Two Pipe","Other"},
			arrcirculator={"--Select--","Pump","Gravity","Other"},arrdrain={"--Select--","Manual","Automatic","Other"},arrfuse={"--Select--","Single Wall","Double Wall","PVC"},
			arrdevious={"--Select--","Temp Guage","Expansion Tank","Pressure Gauge","Other"},arrhumidifier={"--Select--","Aprilaire","Honeywell","Other"},arrthermostats={"--Select--","Individual",
			"Multi Zone","Programmable","Other"},
			arrasbestos={"--Select--","Yes","No"},arrheatingnumber,arrheatingnumother,arrheatingsysteme ,arrmanufacture,arrmodelnumber,
			arrenergy={"--Select--","Oil","Gas","Electric","Water","Liquid Propane","Kerosene","Solar","Wood","Coal","Corn","Natural Gas","Diesel","Other"},
					arrheatbrand={"--Select--","Not Determined", "Not Visible","Aged","Amana","American Standard","Americanaire","A O Smith","Apollo","Aquatherm","Arcoaire","Armstrong",
			"Bryant","Burnham","Carrier","Climate Master","Coleman","Comfort Maker","Crown","Empire","Envirotemp","Frigidaire","Goodman",
			"Hallmark","Heatwave","Heil","Int.Comfort","Intertherm","Janitrol","Kenmore","Lennox","Luxaire","Magic Chef","Monitor","Ninety Two",
			"None","Oneida","Patriot","Payne","Peerless","Presidential","Quattro","Rheem","Rinnai","Ruud","Sears","Singer","Tempstar","Thermopride",
			"Toyostove","Trane","Unknown","Weatherking","Weil Mclean","York","Zephyr"},
			arrheatdelivery={"--Select--","Forced Air","Refrigerant","Electric Resistance Heat","Hot Water","Steam Heat","Radiant","Boiler"},
			arrwatersystem={"--Select--","One Pipe System","Two Pipe System","Fin Coils","Boiler","Subfloor","Convectors","Radiators","Baseboard"},
			arrradiators={"--Select--","Cast Iron Radiators","Steel Radiators","Manual Fill","Water Level Sight Glass","Bleed Valves"},
			arrmotorblower={"--Select--","Blower Fan","Direct Drive","Belt Driven"},
			arrwoodstoves={"--Select--","1","2","3","4","5","None","Other"},
			arrsupplyregister={"--Select--","Low","High","Low & High"},
					arrreturnregister={"--Select--","Low","High"},
					arrexternalunitrepprob={"--Select--","Low","Medium","High","Other"},
					arrzonetonnage={"--Select--",	"1.5 Ton","2 Ton","2.5 Ton","3 Ton","3.5 Ton","4 Ton","4.5 Ton","5 Ton","Other"},
							arrbtu={"--Select--","Not Determined","12000 BTU's","18000 BTU's","24000 BTU's","30000 BTU's","36000 BTU's","42000 BTU's","48000 BTU's","54000 BTU's","Other"},
			
			arrretunrregister={"--Select--","Low","High"},
				arrnoofzones={"--Select--","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"},
			arrmultizonesystem={"--Select--","Yes", "No", "Auto Damper Not Tested"},
			
			arrfurnacefeatures={"--Select--","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner"," Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light"," Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Air Filter"},
			arrfurnace={"--Select--","Air To Air Heat Pump","Water To Air Heat Pump","Forced Air Furnace","Induced Air Furnace","Gravity Furnace","Evaporated Located Over Furnace","Natural Draft System","Condensation Furnace System","Inside panels sealed(no coil access)"};
	ArrayAdapter externalunitrepadap,internalunitrepadap,thermolocationadap,thermotypeadap,zonetonnadap,btuarrayadap,multizonearraydap,ductworktypearraydap,zonetestedforadap,zoneadap,zonenoarrayadap,fluetypearrayadap,noofzonesarrayadap,gasshutoffarrayadap,heatnumarrayadap,heatsystemarrayadap,heattypearrayadap,heatexchangerarrayadap,distributionarrayadap,circulatorarrayadap,drainarrayadap,fusearrayadap,deviousarrayadap,
	watersysarrayadap,returnregarraydap,supplyregarraydap,furnfeatarraydap, humidifierarrayadap,thermostatsarrayadap,fueltankarrayadap,asbestosarrayadap,energysourcearrayadap,agearrayadap,brandarrayadap,furnacearrayadap,heatdeliveryarrayadap,radiatorsarrayadap,motorarrayadap,woodstoveadap;
	EditText et_otherheatingnum,et_manufacturer,et_modelnumber,et_serialnumber,et_capacity,et_otherheatexchanger,et_otherdistribution,et_othercirculator,
	         et_otherdrain,et_otherdevious,et_otherhumidifier,et_otherthermostats,et_otherfueltank;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}		
		
		setContentView(R.layout.activity_newhvac1);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(20);
		db.CreateTable(21);
		db.CreateTable(22);
		db.CreateTable(23);
		db.CreateTable(24);
		db.CreateTable(25);
		db.CreateTable(38);
		db.CreateTable(39);
		db.CreateTable(40);
		db.CreateTable(42);
		db.CreateTable(43);
		db.CreateTable(44);
		db.CreateTable(51);
		db.CreateTable(52);
		db.userid();		
        cf.getDeviceDimensions();
        /*LinearLayout lnrow = (LinearLayout)findViewById(R.id.hvaclin);
		lnrow.setMinimumHeight(cf.ht);*/
        
        
		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/squadaone-regular.ttf"); 
		btnsubmit.setTypeface(font); 
		
		
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		
		otheredtlayout = (LinearLayout)findViewById(R.id.otheredtlayout);
		
		
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
		btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(NewHVAC1.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							Bundle b3 = new Bundle();
							b3.putString("photooption","takephoto");			
							intent.putExtras(b3);
							Bundle b4 = new Bundle();
							b4.putString("currentview",getcurrentview);			
							intent.putExtras(b4);							
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getselectedho);			
							intent.putExtras(b5);
							startActivity(intent);
							finish();
			}
		});
		
		sp_heatingtype = (Spinner)findViewById(R.id.spin_heattype);
		heattypearrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrheattype);
		heattypearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_heatingtype.setAdapter(heattypearrayadap);
		sp_heatingtype.setOnItemSelectedListener(new myspinselectedlistener(3));
		ed_otherheattype= (EditText)findViewById(R.id.other_heatingtype);
		
		sp_energysource = (Spinner)findViewById(R.id.spin_energysource);
		energysourcearrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrenergy);
		energysourcearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_energysource.setAdapter(energysourcearrayadap);
		sp_energysource.setOnItemSelectedListener(new myspinselectedlistener(2));
		ed_otherenergysource= (EditText)findViewById(R.id.other_energysource);
		
		fueltanktv = (TextView)findViewById(R.id.spin_fueltanklocation);
		fueltanktv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				fueltank_dialog=1;
				hvacvalue="Fuel Tank Location";k=1;
				multi_select(hvacvalue,k);				
			}
		});		
		sp_gasshutoff = (Spinner)findViewById(R.id.spin_gasshutoff);
		gasshutoffarrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrgasshutoff);
		gasshutoffarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_gasshutoff.setAdapter(gasshutoffarrayadap);
		sp_gasshutoff.setOnItemSelectedListener(new myspinselectedlistener(13));
		ed_othergasshutoff= (EditText)findViewById(R.id.other_gasshutoff);

		spin_zoneno = (Spinner)findViewById(R.id.spin_zoneno);
		zonenoarrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrnoofzones);
		zonenoarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_zoneno.setAdapter(zonenoarrayadap);
		spin_zoneno.setOnItemSelectedListener(new myspinselectedlistener(16));
		//ed_otherzoneno= (EditText)findViewById(R.id.other_zoneno);System.out.println("sixth");
		
		sp_zonename = (Spinner)findViewById(R.id.spin_zonename);System.out.println("frist");
		zoneadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrzonename);System.out.println("second");
		zoneadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);System.out.println("third");
		sp_zonename.setAdapter(zoneadap);System.out.println("fourth");
		sp_zonename.setOnItemSelectedListener(new myspinselectedlistener(25));System.out.println("fifth");
		ed_otherzonename= (EditText)findViewById(R.id.other_zonename);System.out.println("sixth");
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC", null);
            System.out.println("cur="+cur.getCount()+"select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC");
			if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);				
				}  
			}
		});
		
		
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		
		fluetypetv = (TextView)findViewById(R.id.spin_fluetype);
		fluetypetv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fluetype_dialog=1;
				hvacvalue="Flue Type";k=2;
				multi_select(hvacvalue,k);				
			}
		});
		spin_heatingage = (Spinner)findViewById(R.id.spin_heatingage);
		agearrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrcoolingage);
		agearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_heatingage.setAdapter(agearrayadap);
		spin_heatingage.setOnItemSelectedListener(new myspinselectedlistener(5));
		ed_otherheatingage= (EditText)findViewById(R.id.other_heatingage);
		
		sp_heatbrand = (Spinner)findViewById(R.id.spin_heatbrand);
		brandarrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrheatbrand);
		brandarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		sp_heatbrand.setAdapter(brandarrayadap);
		sp_heatbrand.setOnItemSelectedListener(new myspinselectedlistener(6));
		
		sp_furnace = (Spinner)findViewById(R.id.spin_furnace);
		furnacearrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrfurnace);
		furnacearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		sp_furnace.setAdapter(furnacearrayadap);
		sp_furnace.setOnItemSelectedListener(new myspinselectedlistener(7));
		
		sp_heatdelivery = (Spinner)findViewById(R.id.spin_heatdelivery);
		heatdeliveryarrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrheatdelivery);
		heatdeliveryarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_heatdelivery.setAdapter(heatdeliveryarrayadap);
		sp_heatdelivery.setOnItemSelectedListener(new myspinselectedlistener(1));
		
		
		sp_motorblower = (Spinner)findViewById(R.id.spin_motorblowers);
		motorarrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrmotorblower);
		motorarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_motorblower.setAdapter(motorarrayadap);
		sp_motorblower.setOnItemSelectedListener(new myspinselectedlistener(10));
		
		
		testedfortv = (TextView)findViewById(R.id.spin_zonestestedfor);
		testedfortv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				testedfordialog=1;
				hvacvalue="Tested For";k=3;
				multi_select(hvacvalue,k);				
			}
		});

		
		airhandlertv = (TextView)findViewById(R.id.spin_airhandlerfeatures);
		airhandlertv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				airhandler_dialog=1;
				hvacvalue="Air Handler Features";k=4;
				multi_select(hvacvalue,k);				
			}
		});
		sp_zonetonnage = (Spinner)findViewById(R.id.spin_zonestonnage);
		zonetonnadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrzonetonnage);
		zonetonnadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonetonnage.setAdapter(zonetonnadap);
		sp_zonetonnage.setOnItemSelectedListener(new myspinselectedlistener(29));
		ed_otherzonetonnage= (EditText)findViewById(R.id.other_zontonnage);
		
		
		sp_btu = (Spinner)findViewById(R.id.spin_btu);
		btuarrayadap = new ArrayAdapter<String>(NewHVAC1.this,android.R.layout.simple_spinner_item,arrbtu);
		btuarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_btu.setOnItemSelectedListener(new myspinselectedlistener(30));
		sp_btu.setAdapter(btuarrayadap);
		ed_otherbtu= (EditText)findViewById(R.id.other_btu);
		
	
		((TextView)findViewById(R.id.modulename)).setVisibility(v.VISIBLE);
		((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		heatingtblshow= (TableLayout)findViewById(R.id.heathvactable);
		
		
		Button btnsave = (Button)findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) 
				{
					// TODO Auto-generated method stub
					savehvacnotappl(getcurrentview);
					
					Intent intent = new Intent(NewHVAC1.this, NewDuctwork.class);
			    	Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("tempoption", gettempoption);			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("selectedid", getselectedho);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
					
				}
			});
		
		
		Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(NewHVAC1.this,HomeScreen.class));
					finish();
					 
				}
			});
		
		Button	btncancel = (Button)findViewById(R.id.btn_cancelheating);
		btncancel.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					clearheating();
					 
				}
			});
		
		Button save = (Button)findViewById(R.id.btn_addheaating);
		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getzoneno.equals("--Select--"))
				{
					cf.DisplayToast("Please select the Zone No.");
				}
				else if (getheatingtype.equals("--Select--"))
				{
					cf.DisplayToast("Please select the HVAC Type.");
				}
				else
				{
					 strotherheattype = ed_otherheattype.getText().toString();
					 strgasshutoff = ed_othergasshutoff.getText().toString();
					 strotherheatingage = ed_otherheatingage.getText().toString();
					 strzonetonnage = ed_otherzonetonnage.getText().toString();
					 strbtu = ed_otherbtu.getText().toString();
					 strenergysource = ed_otherenergysource.getText().toString();
					// strzoneno = ed_otherzoneno.getText().toString();
					 strzonename = ed_otherzonename.getText().toString();
					 
					 if(getheatingtype.equals("--Select--")){getheatingtype="";}
					 if(getenergysource.equals("--Select--")){getenergysource="";}
					 if(getheatingage.equals("--Select--")){getheatingage="";}
					 if(getheatbrand.equals("--Select--")){getheatbrand="";}
					 if(getfurnace.equals("--Select--")){getfurnace="";}
					 if(getheatdeliverytype.equals("--Select--")){getheatdeliverytype="";}
					 if(getmotorblower.equals("--Select--")){getmotorblower="";}
					 if(getzonetonnage.equals("--Select--")){getzonetonnage="";}
					 if(getbtu.equals("--Select--")){getbtu="";}
					
					 try
						{
						 
						 if(getcurrentview.equals("start"))
						 {
							 if(((Button)findViewById(R.id.btn_addheaating)).getText().toString().equals("Update"))
								{
								 db.hi_db.execSQL("UPDATE  "+db.LoadHVAC+" SET fld_zoneno='"+getzoneno+"',fld_otherzoneno='"+db.encode(strzoneno)+"',fld_zonename='"+getzonename+"',fld_otherzonename='"+strzonename+"', fld_energysource='"+db.encode(getenergysource)+"',fld_energysourceother='"+db.encode(strenergysource)+"',"+
										           "fld_gasshutoff='"+db.encode(getgasshutoff)+"',fld_gasshutoffother='"+db.encode(strgasshutoff)+"',fld_fueltankloc='"+db.encode(tempfueltankloc)+"',"+
												   "fld_fluetype='"+db.encode(tempfluetype)+"',"+
												   "fld_age='"+getheatingage+"',fld_otherage='"+db.encode(strotherheatingage)+"',fld_heatsystembrand='"+db.encode(getheatbrand)+"',fld_furnacetype='"+db.encode(getfurnace)+"',fld_heatdeliverytype='"+db.encode(getheatdeliverytype)+"'," +
												   			"fld_blowermotor='"+db.encode(getmotorblower)+"',fld_testedfor='"+db.encode(temptestedfor)+"',fld_airhandlerfeatures='"+db.encode(tempairhandlerfeat)+"'," +
												   					"fld_tonnage='"+db.encode(getzonetonnage)+"',fld_tonnageother='"+db.encode(strzonetonnage)+"',fld_btu='"+db.encode(getbtu)+"',fld_btuother='"+db.encode(strbtu)+"'" +
												   				" Where h_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
										cf.DisplayToast("HVAC system updated successfully");
										((Button)findViewById(R.id.btn_addheaating)).setText("Save and Add more");
										displayheating();
										clearheating();
								}
								else
								{
									try
									{
											Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_zoneno='"+getzoneno+"'", null);
											
											if(cur.getCount()==0)
											{
											db.hi_db.execSQL(" INSERT INTO "+db.LoadHVAC+" (fld_templatename,fld_inspectorid,fld_srid,fld_zoneno,fld_otherzoneno,fld_zonename,fld_otherzonename,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype," +
														"fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures," +
														"fld_tonnage,fld_tonnageother,fld_btu,fld_btuother) VALUES" +
																							    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+getzoneno+"','"+strzoneno+"','"+db.encode(getzonename)+"','"+db.encode(strzonename)+"','"+db.encode(getheatingtype)+"','"+db.encode(strotherheattype)+"','"+db.encode(getenergysource)+"','"+db.encode(strenergysource)+"','"+db.encode(getgasshutoff)+"','"+db.encode(strgasshutoff)+"','"+db.encode(tempfueltankloc)+"','"+db.encode(tempfluetype)+"',"+
														 "'"+db.encode(getheatingage)+"','"+db.encode(strotherheatingage)+"','"+db.encode(getheatbrand)+"','"+db.encode(getfurnace)+"','"+db.encode(getheatdeliverytype)+"','"+db.encode(getmotorblower)+"','"+db.encode(temptestedfor)+"','"+db.encode(tempairhandlerfeat)+"',"+
													    "'"+db.encode(getzonetonnage)+"','"+db.encode(strzonetonnage)+"','"+db.encode(getbtu)+"','"+db.encode(strbtu)+"')");
											savehvacnotappl(getcurrentview);											
											}
											else
											{
											   cf.DisplayToast("Already Exists!! Please select another Zone No.");
											}	
											
											
											
											
												cf.DisplayToast("HVAC system saved successfully");
												displayheating();
												clearheating();
										
											
											
											
											
									}
									catch (Exception e) {
										// TODO: handle exception
										System.out.println("test ="+e.getMessage());
									}								
								}
						 }
						 else
						 {
							 if(((Button)findViewById(R.id.btn_addheaating)).getText().toString().equals("Update"))
								{
								 	db.hi_db.execSQL("UPDATE  "+db.CreateHVAC+" SET fld_zoneno='"+getzoneno+"',fld_otherzoneno='"+db.encode(strzoneno)+"',fld_zonename='"+getzonename+"',fld_otherzonename='"+strzonename+"',fld_energysource='"+getenergysource+"',fld_energysourceother='"+db.encode(strenergysource)+"',"+
										           "fld_gasshutoff='"+getgasshutoff+"',fld_gasshutoffother='"+db.encode(strgasshutoff)+"',fld_fueltankloc='"+db.encode(tempfueltankloc)+"',"+
												   "fld_fluetype='"+db.encode(tempfluetype)+"',"+
												   "fld_age='"+getheatingage+"',fld_otherage='"+db.encode(strotherheatingage)+"',fld_heatsystembrand='"+getheatbrand+"',fld_furnacetype='"+getfurnace+"',fld_heatdeliverytype='"+getheatdeliverytype+"'," +
												   			"fld_blowermotor='"+getmotorblower+"',fld_testedfor='"+temptestedfor+"',fld_airhandlerfeatures='"+tempairhandlerfeat+"'," +
												   					"fld_tonnage='"+getzonetonnage+"',fld_tonnageother='"+strzonetonnage+"',fld_btu='"+db.encode(getbtu)+"',fld_btuother='"+db.encode(strbtu)+"'" +
												   				" Where h_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
										cf.DisplayToast("HVAC system updated successfully");
										((Button)findViewById(R.id.btn_addheaating)).setText("Save and Add more");
										displayheating();
										clearheating();
								}
								else
								{
									try
									{
										
											Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_zoneno='"+getzoneno+"'", null);
											if(cur.getCount()==0)
											{
												db.hi_db.execSQL(" INSERT INTO "+db.CreateHVAC+" (fld_templatename,fld_inspectorid,fld_zoneno,fld_otherzoneno,fld_zonename,fld_otherzonename,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype," +
														"fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures," +
														"fld_tonnage,fld_tonnageother,fld_btu,fld_btuother) VALUES" +
																							    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getzoneno+"','"+strzoneno+"','"+db.encode(getzonename)+"','"+db.encode(strzonename)+"','"+getheatingtype+"','"+db.encode(strotherheattype)+"','"+getenergysource+"','"+db.encode(strenergysource)+"','"+getgasshutoff+"','"+db.encode(strgasshutoff)+"','"+db.encode(tempfueltankloc)+"','"+db.encode(tempfluetype)+"',"+
														 "'"+getheatingage+"','"+db.encode(strotherheatingage)+"','"+getheatbrand+"','"+getfurnace+"','"+getheatdeliverytype+"','"+getmotorblower+"','"+temptestedfor+"','"+tempairhandlerfeat+"',"+
													    "'"+getzonetonnage+"','"+strzonetonnage+"','"+db.encode(getbtu)+"','"+db.encode(strbtu)+"')");
											savehvacnotappl(getcurrentview);
												cf.DisplayToast("HVAC system saved successfully");
												displayheating();
												clearheating();
											}
											else
											{
											   cf.DisplayToast("Already Exists!! Please select another Zone No.");
											}
										
									}
									catch (Exception e) {
										// TODO: handle exception
										System.out.println("test ="+e.getMessage());
									}								
								}
						 }
														
						}
						catch (Exception e) {
							// TODO: handle exception
							 System.out.println("insi sdasdaseeeea"+e.getMessage());
						}
					 
				}
			
			}
		});		
		chknotapplicable = (CheckBox) findViewById(R.id.hvacnotappl);		 
		chknotapplicable.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
	                //is chkIos checked?
			if (((CheckBox) v).isChecked())
			{
				chk=1;
				
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
				
			}
			else
			{
				chk=2;
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
				
				
			}
	 
		  }
		});
		fueltanklocnote = (TextView)findViewById(R.id.fueltankloctv);
		fluetypenote = (TextView)findViewById(R.id.fluetypetv);
		testedfornote = (TextView)findViewById(R.id.testedfortv);
		airhandlerfeatnote = (TextView)findViewById(R.id.airhandlerfeattv);
		LinearLayout menu = (LinearLayout) findViewById(R.id.hvacmenu);
		menu.addView(new Header_Menu(this, 1, cf,gettempname,getcurrentview,getmodulename,gettempoption,getselectedho,db));
		displayheating();
		shownotpplicable();
	}
	private void shownotpplicable() {
		// TODO Auto-generated method stub
		Cursor cur1;
		if(getcurrentview.equals("start"))
		{
			  cur1= db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' ",null);
		}
		else
		{
			  cur1= db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable+ " where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+getselectedho+"' ",null);	
		}
		System.out.println("cur="+cur1.getCount());
		if(cur1.getCount()>0)
		{
			cur1.moveToFirst();
			String navalue = cur1.getString(cur1.getColumnIndex("fld_HVACNA"));System.out.println("NAVALUE="+navalue);
			if(navalue.equals("1"))
			{
				chknotapplicable.setChecked(true);
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE); 		
			}
			else
			{
				chknotapplicable.setChecked(false);
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
			}
		}
		
	}
	private void savehvacnotappl(String currentview) {
		// TODO Auto-generated method stub
		try
		{
			if(chknotapplicable.isChecked()==true)
			{
				chk=1;
			}
			else
			{
				chk=2;
			}
			
			Cursor cur2;
			if(currentview.equals("start"))
			{
				 cur2= db.hi_db.rawQuery("select * from " + db.HVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);	
			}
			else
			{
				 cur2= db.hi_db.rawQuery("select * from " + db.CreateHVACNotApplicable + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
			
			
			if(cur2.getCount()==0)
			{
				if(currentview.equals("start"))
				{
					db.hi_db.execSQL(" INSERT INTO "+db.HVACNotApplicable+" (fld_templatename,fld_inspectorid,fld_srid,fld_HVACNA) VALUES" +
															    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+chk+"')");
				}
				else
				{
					db.hi_db.execSQL(" INSERT INTO "+db.CreateHVACNotApplicable+" (fld_templatename,fld_inspectorid,fld_HVACNA) VALUES" +
						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+chk+"')");
				}
						
			}
			else
			{
				if(currentview.equals("start"))
				{
					db.hi_db.execSQL("UPDATE  "+db.HVACNotApplicable+" SET fld_HVACNA='"+chk+"' Where fld_inspectorid='"+db.UserId+"' and fld_Srid='"+getselectedho+"'");
				}
				else
				{
					db.hi_db.execSQL("UPDATE  "+db.CreateHVACNotApplicable+" SET fld_HVACNA='"+chk+"' Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"'");
				}
				
			
			}
		}
		catch(Exception e1)
		{
			System.out.println("not appl="+e1.getMessage());
		}
	}
	private void submitalert()
	{
		/*final Dialog dialog = new Dialog(NewHVAC1.this,android.R.style.Theme_Translucent_NoTitleBar);
		//dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.setContentView(R.layout.input_submit_inspection);		
				
		 final String myString[] = getResources().getStringArray(R.array.myArray);
		 word = myString[rgenerator.nextInt(myString.length)];
		 wrdedit = ((EditText) dialog.findViewById(R.id.wrdedt));
		 
		 wrdedit.setText(word);
		 if (word.contains(" ")) { word = word.replace(" ", ""); }
			
		 
		db.userid();
		  ((TextView) dialog.findViewById(R.id.tv1)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"			
							 + db.Userfirstname +" "+db.Userlastname
							+ "</font></b><font color=black> have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
							+ "</font>"));
		 
			((TextView) dialog.findViewById(R.id.tv2)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ db.Userfirstname +" "+db.Userlastname
							+ "</font></b><font color=black> also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
							+ "</font>"));
			
			
			((Button) dialog.findViewById(R.id.submit)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (((RadioButton) dialog.findViewById(R.id.accept)).isChecked() == true) {
						
						if (((EditText) dialog.findViewById(R.id.wrdedt1)).getText().toString().equals("")) 
						{
							cf.DisplayToast("Please enter Word Verification.");
							((EditText) dialog.findViewById(R.id.wrdedt1)).requestFocus();
						} 
						else
						{
							if (((EditText) dialog.findViewById(R.id.wrdedt1)).getText().toString().equals(word.trim().toString())) 
							{
								System.out.println("111");
								dialog.cancel();
								if(cf.isInternetOn())
								  {
									   new Start_xporting().execute("");
								  }
								  else
								  {
									  cf.DisplayToast("Please enable your internet connection.");
									  
								  } 
							} 
							else
							{
								cf.DisplayToast("Please enter valid Word Verification(case sensitive).");
								((EditText) dialog.findViewById(R.id.wrdedt1)).setText("");
								((EditText) dialog.findViewById(R.id.wrdedt1)).requestFocus();
							}
						}
					} 
					else 
					{
						cf.DisplayToast("Please select Accept Radio Button.");
					}

				}
			});			
			
			((Button) dialog.findViewById(R.id.cancel)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});			
			
			((Button) dialog.findViewById(R.id.S_refresh)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					word = myString[rgenerator.nextInt(myString.length)];			
					 ((EditText) dialog.findViewById(R.id.wrdedt)).setText(word);
					if (word.contains(" ")) {
						word = word.replace(" ", "");
					}	
					
				}
			});			
		dialog.show();*/
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(NewHVAC1.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						DefaultValue_Insert(gettempname,getselectedho);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}	
	private void DefaultValue_Insert(String gettempname,String getselectedho) {
		// TODO Auto-generated method stub
		Cursor selcur;
			try
			{
				selcur = db.hi_db.rawQuery("select * from " + db.CreateHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' ", null);
				int cnt = selcur.getCount();
	            if(cnt>0)
	           
	            	
					if (selcur != null) {
						selcur.moveToFirst();
						db.hi_db.execSQL("Delete from "+db.LoadHVAC+" WHERE  fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
						do {
							
							Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
							
	    		            if(cur.getCount()==0)
	    		            {
    							try
    			            	{
    								db.hi_db.execSQL("INSERT INTO "+db.LoadHVAC+" (fld_templatename,fld_inspectorid,fld_srid,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures,fld_tonnage,fld_tonnageother,fld_btu,fld_btuother) VALUES" +
    										"('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+selcur.getString(3)+"','"+selcur.getString(4)+"','"+selcur.getString(5)+"','"+selcur.getString(6)+"','"+selcur.getString(7)+"','"+selcur.getString(8)+"','"+selcur.getString(9)+"','"+selcur.getString(10)+"','"+selcur.getString(11)+"','"+selcur.getString(12)+"','"+selcur.getString(13)+"','"+selcur.getString(14)+"','"+selcur.getString(15)+"','"+selcur.getString(16)+"','"+selcur.getString(17)+"','"+selcur.getString(18)+"','"+selcur.getString(19)+"','"+selcur.getString(20)+"','"+selcur.getString(21)+"','"+selcur.getString(22)+"')");
    		                 	
    		                 	displayheating();
    		                 	clearheating();
    			            	}
    			            	catch (Exception e) {
    								// TODO: handle exception
    			            		System.out.println("error catch="+e.getMessage());
    			            		
    							}
	    		            }
	    		           
						
						}while (selcur.moveToNext());
					}
	            
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
			
	}
class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        if(getcurrentview.equals("start"))
		        {
		        	p_sv.progress_title="Inspection Submission";
		        	p_sv.progress_Msg="Please wait..Your Inspection has been submitting.";
		        }
		        else
		        {
		        	p_sv.progress_Msg="Please wait..Your template has been submitting.";
		        }
		        
			    
		       
				startActivityForResult(new Intent(NewHVAC1.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		
			 			cf.Submit_HVAC();
			 		    //Submit_Hotwatersystem();
			 		      
			 		
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    /*	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
         	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}*/
		    	
		    }
	}
	/*private void Submit_HVAC() throws IOException, XmlPullParserException {    	
		// TODO Auto-generated method stub
		Cursor c1=null;SoapObject request=null;
		System.out.println("test");
		if(getcurrentview.equals("start"))
		{
			c1 = db.hi_db.rawQuery("select * from " + db.LoadHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
		}
		else
		{
			c1 = db.hi_db.rawQuery("select * from " + db.CreateHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
		}
		System.out.println("c1="+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 if(getcurrentview.equals("start"))
					 {
						  request = new SoapObject(wb.NAMESPACE,"Load_Heating"); 
					 }
					 else
					 {
						  request = new SoapObject(wb.NAMESPACE,"Create_Heating");
					 }
   	        		
	                SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    System.out.println("ddsfdsf");
	      		    request.addProperty("h_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_hvactype",db.decode(c1.getString(c1.getColumnIndex("fld_hvactype"))));
	      		    request.addProperty("fld_hvactypeother",db.decode(c1.getString(c1.getColumnIndex("fld_hvactypeother"))));
	      		    request.addProperty("fld_energysource",db.decode(c1.getString(c1.getColumnIndex("fld_energysource"))));	      		    
	      		    request.addProperty("fld_energysourceother",db.decode(c1.getString(c1.getColumnIndex("fld_energysourceother"))));
	      		    request.addProperty("fld_gasshutoff",db.decode(c1.getString(c1.getColumnIndex("fld_gasshutoff"))));
	      		    request.addProperty("fld_gasshutoffother",db.decode(c1.getString(c1.getColumnIndex("fld_gasshutoffother"))));	      		    
	      		    request.addProperty("fld_fueltankloc",db.decode(c1.getString(c1.getColumnIndex("fld_fueltankloc"))));
	      		    request.addProperty("fld_fluetype",db.decode(c1.getString(c1.getColumnIndex("fld_fluetype"))));
	      		    request.addProperty("fld_noofzones",db.decode(c1.getString(c1.getColumnIndex("fld_noofzones"))));	      		   
	      		    request.addProperty("fld_zoneno",db.decode(c1.getString(c1.getColumnIndex("fld_zoneno"))));
	      		    request.addProperty("fld_zonename",db.decode(c1.getString(c1.getColumnIndex("fld_zonename"))));	      		    
	      		    request.addProperty("fld_zonenameother",db.decode(c1.getString(c1.getColumnIndex("fld_zonenameother"))));
	      		    request.addProperty("fld_age",db.decode(c1.getString(c1.getColumnIndex("fld_age"))));
	      		    request.addProperty("fld_otherage",db.decode(c1.getString(c1.getColumnIndex("fld_otherage"))));	      		    
	      		    request.addProperty("fld_heatsystembrand",db.decode(c1.getString(c1.getColumnIndex("fld_heatsystembrand"))));
	      		    request.addProperty("fld_furnacetype",db.decode(c1.getString(c1.getColumnIndex("fld_furnacetype"))));	      		    
	      		    request.addProperty("fld_heatdeliverytype",db.decode(c1.getString(c1.getColumnIndex("fld_heatdeliverytype"))));
	      		    request.addProperty("fld_blowermotor",db.decode(c1.getString(c1.getColumnIndex("fld_blowermotor"))));
	      		    request.addProperty("fld_testedfor",db.decode(c1.getString(c1.getColumnIndex("fld_testedfor"))));	      		    
	      		    request.addProperty("fld_watersystemtype",db.decode(c1.getString(c1.getColumnIndex("fld_watersystemtype"))));
	      		    request.addProperty("fld_fueltanklocation",db.decode(c1.getString(c1.getColumnIndex("fld_fueltanklocation"))));	      		    
	      		    request.addProperty("fld_airhandlerfeatures",db.decode(c1.getString(c1.getColumnIndex("fld_airhandlerfeatures"))));
	      		    request.addProperty("fld_supplyregister",db.decode(c1.getString(c1.getColumnIndex("fld_supplyregister"))));	      		    
	      		    request.addProperty("fld_retunrregister",db.decode(c1.getString(c1.getColumnIndex("fld_retunrregister"))));
	      		    request.addProperty("fld_ducworktype",db.decode(c1.getString(c1.getColumnIndex("fld_ducworktype"))));
	      		    request.addProperty("fld_multizonesystem",db.decode(c1.getString(c1.getColumnIndex("fld_multizonesystem"))));	      		    
	      		    request.addProperty("fld_watersystemtype",db.decode(c1.getString(c1.getColumnIndex("fld_watersystemtype"))));
	      		    request.addProperty("fld_fueltanklocation",db.decode(c1.getString(c1.getColumnIndex("fld_fueltanklocation"))));
	      		    request.addProperty("fld_tonnage",db.decode(c1.getString(c1.getColumnIndex("fld_tonnage"))));
	      		    request.addProperty("fld_tonnageother",db.decode(c1.getString(c1.getColumnIndex("fld_tonnageother"))));	      		    
	      		    request.addProperty("fld_retunrregister",db.decode(c1.getString(c1.getColumnIndex("fld_retunrregister"))));
	      		    request.addProperty("fld_btu",db.decode(c1.getString(c1.getColumnIndex("fld_btu"))));
	      		    request.addProperty("fld_btuother",db.decode(c1.getString(c1.getColumnIndex("fld_btuother"))));	      		    
	      		    request.addProperty("fld_thermotype",db.decode(c1.getString(c1.getColumnIndex("fld_thermotype"))));
	      		    request.addProperty("fld_thermolocation",db.decode(c1.getString(c1.getColumnIndex("fld_thermolocation"))));
	      		    
	      		    
	      		    request.addProperty("fld_externalunit",db.decode(c1.getString(c1.getColumnIndex("fld_externalunit"))));
	      		    request.addProperty("fld_internalunit",db.decode(c1.getString(c1.getColumnIndex("fld_internalunit"))));
	      		    request.addProperty("fld_extunitreplprob",db.decode(c1.getString(c1.getColumnIndex("fld_extunitreplprob"))));
	      		    request.addProperty("fld_extunitreplprobother",db.decode(c1.getString(c1.getColumnIndex("fld_extunitreplprobother"))));	      		    
	      		    request.addProperty("fld_intunitreplprob",db.decode(c1.getString(c1.getColumnIndex("fld_intunitreplprob"))));
	      		    request.addProperty("fld_intunitreplprobother",db.decode(c1.getString(c1.getColumnIndex("fld_intunitreplprobother"))));
	      		    request.addProperty("fld_ampextunit",db.decode(c1.getString(c1.getColumnIndex("fld_ampextunit"))));	      		    
	      		    request.addProperty("fld_ampintunit",db.decode(c1.getString(c1.getColumnIndex("fld_ampintunit"))));
	      		    request.addProperty("fld_emerheatampdraw",db.decode(c1.getString(c1.getColumnIndex("fld_emerheatampdraw"))));
	      		    
	      		    
	      		  request.addProperty("fld_supplytemp",db.decode(c1.getString(c1.getColumnIndex("fld_supplytemp"))));
	      		    request.addProperty("fld_returntemp",db.decode(c1.getString(c1.getColumnIndex("fld_returntemp"))));	      		    
	      		    request.addProperty("fld_conddischarge",db.decode(c1.getString(c1.getColumnIndex("fld_conddischarge"))));
	      		    
	      		    
	      		    if(getcurrentview.equals("start"))
					 {
	      		    	request.addProperty("fld_srid",db.decode(c1.getString(c1.getColumnIndex("fld_srid"))));
					 }
	      		    
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Heating request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			if(getcurrentview.equals("start"))
						 {
	         				androidHttpTransport1.call(wb.NAMESPACE+"Load_Heating", envelope1); 
						 }
						 else
						 {
							 androidHttpTransport1.call(wb.NAMESPACE+"Create_Heating", envelope1);
						 }
	         			  
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Heating "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {	                    	 
	                    		 try
	 	          				 {	  
	                    			 if(getcurrentview.equals("start"))
	            					 {
	                    				 
	                    				 db.hi_db.execSQL("UPDATE "+db.LoadHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"' and fld_srid='"+getselectedho+"'");
	                    				 System.out.println("UPDATE "+db.LoadHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"' and fld_srid='"+getselectedho+"'");	 
	            					 }
	                    			 else
	                    			 {
	                    				 System.out.println("UPDATE "+db.CreateHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"'");
	                    				 db.hi_db.execSQL("UPDATE "+db.CreateHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"'");
	 	 	          					
	                    			 }
	 	          					
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;
	                      }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		 else
    		  {
    			  handler_msg=0;
    		  }
	}*/
private void Submit_Hotwatersystem() throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
	Cursor c1=null;SoapObject request=null;
	
	if(getcurrentview.equals("start"))
	{
		c1 = db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
	}
	else
	{
		c1 = db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
	}
	System.out.println("c1="+c1.getCount());
		  if(c1.getCount()>0)
	          {
	        	c1.moveToFirst();   	        
			 do
			 {				
				 if(getcurrentview.equals("start"))
				 {
					  request = new SoapObject(wb.NAMESPACE,"Load_Heating"); 
				 }
				 else
				 {
					  request = new SoapObject(wb.NAMESPACE,"Create_Heating");
				 }
	        		
                SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
      		    envelope1.dotNet = true;
      		    
      		    request.addProperty("h_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
      		    request.addProperty("fld_inspectorid",db.UserId);
      		    request.addProperty("fld_radiators",db.decode(c1.getString(c1.getColumnIndex("fld_radiators"))));
      		    request.addProperty("fld_characteristics",db.decode(c1.getString(c1.getColumnIndex("fld_characteristics"))));
      		    request.addProperty("fld_gaslog",db.decode(c1.getString(c1.getColumnIndex("fld_gaslog"))));	      		    
      		    request.addProperty("fld_gasloglocation",db.decode(c1.getString(c1.getColumnIndex("fld_gasloglocation"))));
      		   request.addProperty("fld_fluetype",db.decode(c1.getString(c1.getColumnIndex("fld_fluetype"))));
      		    request.addProperty("fld_noofwoodstoves",db.decode(c1.getString(c1.getColumnIndex("fld_noofwoodstoves"))));	      		   
      		    request.addProperty("fld_zoneno",db.decode(c1.getString(c1.getColumnIndex("fld_zoneno"))));
      		   
      		     if(getcurrentview.equals("start"))
				 {
      		    	request.addProperty("fld_srid",db.decode(c1.getString(c1.getColumnIndex("fld_srid"))));
				 }
      		    
      		    
      		    envelope1.setOutputSoapObject(request);
         		System.out.println("Heating request="+request);
         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
         		SoapObject response =null;
         		try
                {
         			if(getcurrentview.equals("start"))
					 {
         				androidHttpTransport1.call(wb.NAMESPACE+"Load_Heating", envelope1); 
					 }
					 else
					 {
						 androidHttpTransport1.call(wb.NAMESPACE+"Create_Heating", envelope1);
					 }
         			  
                      response = (SoapObject)envelope1.getResponse();
                      System.out.println("Heating "+response);
                      handler_msg = 0;	  
                      if(response.getProperty("StatusCode").toString().equals("0"))
              		  {	                    	 
                    		 try
 	          				 {	  
                    			 if(getcurrentview.equals("start"))
            					 {
                    				 
                    				 db.hi_db.execSQL("UPDATE "+db.LoadHotwatersystem+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"' and fld_srid='"+getselectedho+"'");
                    				 System.out.println("UPDATE "+db.LoadHotwatersystem+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"' and fld_srid='"+getselectedho+"'");	 
            					 }
                    			 else
                    			 {
                    				 System.out.println("UPDATE "+db.CreateHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"'");
                    				 db.hi_db.execSQL("UPDATE "+db.CreateHVAC+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+c1.getString(c1.getColumnIndex("fld_heatintype"))+"'");
 	 	          					
                    			 }
 	          					
 	          				 }
 	          				 catch (Exception e) {
 	          					// TODO: handle exception
 	          				 }
 	                    	handler_msg = 0;
                      }
              		  else
              		  {
              			 error_msg = response.getProperty("StatusMessage").toString();
              			 handler_msg=1;
              			
              		  }
                }
                
                catch(Exception e)
                {
                	System.out.println("sadsf"+e.getMessage());
                     e.printStackTrace();
                }
        	} while (c1.moveToNext());
	          }
 		 else
		  {
			  handler_msg=0;
		  }
	}
	private void clearheating() {
		// TODO Auto-generated method stub
		tempfueltankloc="";tempfluetype="";temptestedfor="";tempairhandlerfeat="";
	   getgasshutoff="";strgasshutoff="";tempairhandlerfeat="";
		((Button)findViewById(R.id.btn_addheaating)).setText("Save and Add more");
		sp_heatingtype.setSelection(0);//sp_heatingtype.setEnabled(true);
		spin_zoneno.setSelection(0);spin_zoneno.setEnabled(true);
		((EditText)findViewById(R.id.other_heatingtype)).setVisibility(v.GONE);
		((EditText)findViewById(R.id.other_heatingtype)).setText("");
		ed_otherzonename.setVisibility(v.GONE);
		ed_otherzonename.setText("");
		sp_energysource.setSelection(0);sp_zonename.setSelection(0);
		sp_gasshutoff.setSelection(0);
		spin_heatingage.setSelection(0);
		sp_heatbrand.setSelection(0);
		sp_furnace.setSelection(0);
		sp_heatdelivery.setSelection(0);
		sp_motorblower.setSelection(0);
		sp_zonetonnage.setSelection(0);
		sp_btu.setSelection(0);
		fueltanklocnote.setVisibility(v.GONE);
		fluetypenote.setVisibility(v.GONE);
		testedfornote.setVisibility(v.GONE);
		airhandlerfeatnote.setVisibility(v.GONE);
		ed_otherenergysource.setVisibility(v.GONE);
		ed_othergasshutoff.setVisibility(v.GONE);
		ed_otherheattype.setVisibility(v.GONE);
		ed_otherheatingage.setVisibility(v.INVISIBLE);
		ed_otherzonetonnage.setVisibility(v.GONE);
		ed_otherbtu.setVisibility(v.GONE);
	}

	private void displayheating() {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur=null;
			System.out.println("getc="+getcurrentview);
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadHVAC + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' ",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
		System.out.println("count="+cur.getCount());
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 try
					{
						heatingtblshow.removeAllViews();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_headernew, null); 
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.heatinglist);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();			
				 heatingtblshow.setVisibility(View.VISIBLE); 				 
				 heatingtblshow.addView(th,lp); 			 
			
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String hvactype="",hvactypeother="",energysource="",energysourceother="",gasshutoff="",gasshutoffother="",age="",fueltankloc="",fluetype="",
							 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",
							 furnacefeatuers="",supplyregister="",retunrregister="",fueltanklocation="",
									 noofzones="",zoneno="",zonename="",zonenameother="",blowermotor="",testedfor="",airhandlerfeatures="",
									 ducworktype="",multizonesystem="",tonnage="",tonnageother,btu,btuother,thermotype,thermolocation,
											 externalunit,internalunit,extunitreplprob,extunitreplprobother,intunitreplprob,intunitreplprobother,ampextunit,ampintunit,emerheatampdraw,supplytemp,returntemp,conddischarge;
					 
					 zoneno=db.decode(cur.getString(cur.getColumnIndex("fld_zoneno")));System.out.println("zonen="+zoneno);
					 
					 zonename=db.decode(cur.getString(cur.getColumnIndex("fld_zonename")));
					 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_otherzonename")));
					 hvactype=db.decode(cur.getString(cur.getColumnIndex("fld_hvactype")));
					 hvactypeother=db.decode(cur.getString(cur.getColumnIndex("fld_hvactypeother")));
					 energysource=db.decode(cur.getString(cur.getColumnIndex("fld_energysource")));
					 energysourceother=db.decode(cur.getString(cur.getColumnIndex("fld_energysourceother")));
					 gasshutoff=db.decode(cur.getString(cur.getColumnIndex("fld_gasshutoff")));
					 gasshutoffother=db.decode(cur.getString(cur.getColumnIndex("fld_gasshutoffother")));
					 fueltankloc=db.decode(cur.getString(cur.getColumnIndex("fld_fueltankloc")));
					 fluetype=db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));
					 age=db.decode(cur.getString(cur.getColumnIndex("fld_age")));
					 otherage=db.decode(cur.getString(cur.getColumnIndex("fld_otherage")));					 
					 heatsystembrand=db.decode(cur.getString(cur.getColumnIndex("fld_heatsystembrand")));
					 furnacetype=db.decode(cur.getString(cur.getColumnIndex("fld_furnacetype")));
					 heatdeliverytype=db.decode(cur.getString(cur.getColumnIndex("fld_heatdeliverytype")));				 
					 blowermotor=db.decode(cur.getString(cur.getColumnIndex("fld_blowermotor")));
					 testedfor=db.decode(cur.getString(cur.getColumnIndex("fld_testedfor")));					 
					 airhandlerfeatures=db.decode(cur.getString(cur.getColumnIndex("fld_airhandlerfeatures")));					 
					 tonnage=db.decode(cur.getString(cur.getColumnIndex("fld_tonnage")));
					 tonnageother=db.decode(cur.getString(cur.getColumnIndex("fld_tonnageother")));
					 btu=db.decode(cur.getString(cur.getColumnIndex("fld_btu")));					 
					 btuother=db.decode(cur.getString(cur.getColumnIndex("fld_btuother")));

					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvaclistviewnew, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.heatingvalue);
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     
				     TextView tvzoenno= (TextView) t.findViewWithTag("zoneno");
				     tvzoenno.setText(zoneno);
				     
				     TextView tvzonename= (TextView) t.findViewWithTag("zonename");
				     if(zonename.equals("--Select--"))
					 {
				    	 zonename="";
					 }
				     
				     if(zonename.equals("Other"))
					 {
				    	 tvzonename.setText(zonenameother);	
					 }
					 else
					 {
						 tvzonename.setText(zonename);
					 }
				     
				     TextView tvheattype= (TextView) t.findViewWithTag("hvactype");
				     if(hvactype.equals("Other"))
					 {
				    	 tvheattype.setText(hvactypeother);	
					 }
					 else
					 {
						 tvheattype.setText(hvactype);
					 }
				     
				     TextView tvenergsource= (TextView) t.findViewWithTag("energysource");
				     if(energysource.equals("--Select--"))
					 {
				    	 energysource="";
					 }
				     if(energysource.equals("Other"))
					 {
				    	 tvenergsource.setText(energysourceother);	
					 }
					 else
					 {
						 tvenergsource.setText(energysource);
					 }
				     
				     TextView tvgasshutoff= (TextView) t.findViewWithTag("gasshutoff");
				     if(gasshutoff.equals("--Select--"))
					 {
				    	 gasshutoff="";
					 }
				     if(gasshutoff.equals("Other"))
					 {
				    	 tvgasshutoff.setText(gasshutoffother);	
					 }
					 else
					 {
						 tvgasshutoff.setText(gasshutoff);
					 }
				     
				     
				     TextView tvfueltank= (TextView) t.findViewWithTag("fueltanklocation");
				     if(fueltankloc.equals("--Select--"))
					 {
				    	 fueltankloc="";
					 }
				     if(fueltankloc.equals("Other"))
					 {
				    	 tvfueltank.setText(gasshutoffother);	
					 }
					 else
					 {
						 tvfueltank.setText(fueltankloc);
					 }
				     
				     
				     TextView tvage= (TextView) t.findViewWithTag("age");
				     if(age.equals("--Select--"))
					 {
				    	 age="";
					 }
				     if(age.equals("Other"))
					 {
				    	 tvage.setText(otherage);	
					 }
					 else
					 {
						 tvage.setText(age);
					 }
										
					TextView tvbrand= (TextView) t.findViewWithTag("systembrand");
					if(heatsystembrand.equals("--Select--"))
					{
						heatsystembrand="";
					}
					tvbrand.setText(heatsystembrand);
					
					TextView tvfurnace= (TextView) t.findViewWithTag("furnacetype");
					if(furnacetype.equals("--Select--"))
					{
						furnacetype="";
					}
					tvfurnace.setText(furnacetype);
					
					TextView tvtestedfor= (TextView) t.findViewWithTag("testedfor");
					if(testedfor.equals("--Select--"))
					{
						testedfor="";
					}
					tvtestedfor.setText(testedfor);
					
					TextView tvheatdeliverytype= (TextView) t.findViewWithTag("heatdeliverytype");
					if(heatdeliverytype.equals("--Select--"))
					{
						heatdeliverytype="";
					}
					tvheatdeliverytype.setText(heatdeliverytype);
					
					TextView tvmotorblower= (TextView) t.findViewWithTag("blowermotor");
					if(blowermotor.equals("--Select--"))
					{
						blowermotor="";
					}
					tvmotorblower.setText(blowermotor);
					
					  TextView tvbtu= (TextView) t.findViewWithTag("btu");
					     if(btu.equals("--Select--"))
						 {
					    	 btu="";
						 }
					     if(btu.equals("Other"))
						 {
					    	 tvbtu.setText(btuother);	
						 }
						 else
						 {
							 tvbtu.setText(btu);
						 }
					     
					     TextView tvtonnage= (TextView) t.findViewWithTag("tonnage");
					     if(tonnage.equals("--Select--"))
						 {
					    	 tonnage="";
						 }
					     if(tonnage.equals("Other"))
						 {
					    	 tvtonnage.setText(tonnageother);	
						 }
						 else
						 {
							 tvtonnage.setText(tonnage);
						 }
					 
					 
					 TextView tvfluetype= (TextView) t.findViewWithTag("fluetype");
					 if(fluetype.equals("--Select--"))
					 {
						 fluetype="";
					 }
					 tvfluetype.setText(fluetype);
					 
					 TextView tvairhanlderfeat= (TextView) t.findViewWithTag("airhanlderfeat");
					 if(airhandlerfeatures.equals("--Select--"))
					 {
						 airhandlerfeatures="";
					 }
					 tvairhanlderfeat.setText(airhandlerfeatures);
					 
					 ImageView edit= (ImageView) t.findViewWithTag("edit");
					 	edit.setId(789456+i);
					 	edit.setTag(cur.getString(0));
		                edit.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
							try
							{
							   int i=Integer.parseInt(v.getTag().toString());
								((Button)findViewById(R.id.btn_addheaating)).setText("Update");
							   //vrint=1;
							
							   updatehvac(i);   System.out.println("Came completed");
							}
							catch (Exception e) {
								// TODO: handle exception
							}
							}
		                });System.out.println("delete");
	             
	                ImageView delete=(ImageView) t.findViewWithTag("del");  
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(NewHVAC1.this);
							builder.setMessage("Do you want to delete the selected Heating details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==hvacCurrent_select_id)
													{
														hvacCurrent_select_id=0;
													}
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadHVAC+" Where h_id='"+i+"'");	
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateHVAC+" Where h_id='"+i+"'");
													}
																										
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
												/*	Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadHeating + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname).toLowerCase()+"'",null);
													if(cur.getCount()==0)
													{
														flag1=1;
													}*/
													
													//clearheating();
													clearheating();
													displayheating();((Button)findViewById(R.id.btn_addheaating)).setText("Save and Add more");
													
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					
					
					
					
					l1.removeAllViews();
					
					heatingtblshow.addView(t,lp);
					cur.moveToNext();
					
				 }
			 }
			 else
			 {
				 heatingtblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	 	
	}
	private void scrollup() {
		// TODO Auto-generated method stub
	scr = (ScrollView)findViewById(R.id.scr);
	       scr.post(new Runnable() {
	          public void run() {
	              scr.scrollTo(0,0);
	          }
	      }); 
	}
	protected void updatehvac(int i) {
		// TODO Auto-generated method stub
		Cursor cur;
			editid=1;
			getisedit=1;
			hvacCurrent_select_id = i;scrollup();
			try
			{
				if(getcurrentview.equals("start"))
				{
					cur= db.hi_db.rawQuery("select * from " + db.LoadHVAC + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"' and h_id='"+i+"'",null);
				}
				else
				{
					cur= db.hi_db.rawQuery("select * from " + db.CreateHVAC + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and h_id='"+i+"'",null);
				}
				
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
						
					 String hvactype="",hvactypeother="",energysource="",energysourceother="",gasshutoff="",gasshutoffother="",age="",fueltankloc="",fluetype="",
							 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",
							 furnacefeatuers="",supplyregister="",retunrregister="",fueltanklocation="",
									 noofzones="",zoneno="",zonename="",zonenameother="",blowermotor="",testedfor="",airhandlerfeatures="",
									 ducworktype="",multizonesystem="",tonnage="",tonnageother,btu,btuother,thermotype,thermolocation,
											 externalunit,internalunit,extunitreplprob,extunitreplprobother,intunitreplprob,intunitreplprobother,ampextunit,ampintunit,emerheatampdraw,supplytemp,returntemp,conddischarge;
					 
					 zoneno=db.decode(cur.getString(cur.getColumnIndex("fld_zoneno")));
					 zonename=db.decode(cur.getString(cur.getColumnIndex("fld_zonename")));
					 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_otherzonename")));
					 hvactype=db.decode(cur.getString(cur.getColumnIndex("fld_hvactype")));
					 hvactypeother=db.decode(cur.getString(cur.getColumnIndex("fld_hvactypeother")));
					 energysource=db.decode(cur.getString(cur.getColumnIndex("fld_energysource")));
					 energysourceother=db.decode(cur.getString(cur.getColumnIndex("fld_energysourceother")));
					 gasshutoff=db.decode(cur.getString(cur.getColumnIndex("fld_gasshutoff")));
					 gasshutoffother=db.decode(cur.getString(cur.getColumnIndex("fld_gasshutoffother")));
					 tempfueltankloc=db.decode(cur.getString(cur.getColumnIndex("fld_fueltankloc")));
					 tempfluetype=db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));
					 age=db.decode(cur.getString(cur.getColumnIndex("fld_age")));
					 otherage=db.decode(cur.getString(cur.getColumnIndex("fld_otherage")));
					 heatsystembrand=db.decode(cur.getString(cur.getColumnIndex("fld_heatsystembrand")));
					 furnacetype=db.decode(cur.getString(cur.getColumnIndex("fld_furnacetype")));
					 heatdeliverytype=db.decode(cur.getString(cur.getColumnIndex("fld_heatdeliverytype")));					 
					 blowermotor=db.decode(cur.getString(cur.getColumnIndex("fld_blowermotor")));
					 temptestedfor=db.decode(cur.getString(cur.getColumnIndex("fld_testedfor")));					 
					 tempairhandlerfeat=db.decode(cur.getString(cur.getColumnIndex("fld_airhandlerfeatures")));
					 tonnage=db.decode(cur.getString(cur.getColumnIndex("fld_tonnage")));
					 tonnageother=db.decode(cur.getString(cur.getColumnIndex("fld_tonnageother")));
					 btu=db.decode(cur.getString(cur.getColumnIndex("fld_btu")));					 
					 btuother=db.decode(cur.getString(cur.getColumnIndex("fld_btuother")));
					 
					 spin_zoneno.setSelection(zonenoarrayadap.getPosition(zoneno));
					spin_zoneno.setEnabled(false);
					
					if(!zonename.equals(""))
					{
						System.out.println("zone"+zonename);
						sp_zonename.setSelection(zoneadap.getPosition(zonename));
						if(zonename.equals("Other"))
						{
							((EditText)findViewById(R.id.other_zonename)).setVisibility(v.VISIBLE);
							((EditText)findViewById(R.id.other_zonename)).setText(zonenameother);
						}
					}
					
					
					
					sp_heatingtype.setSelection(heattypearrayadap.getPosition(hvactype));//sp_heatingtype.setEnabled(false);
					if(hvactypeother.trim().equals(""))
					{
						ed_otherheattype.setVisibility(v.GONE);
					}
					else
					{
						ed_otherheattype.setVisibility(v.VISIBLE);
						ed_otherheattype.setText(hvactypeother);
					}
					
					sp_energysource.setSelection(energysourcearrayadap.getPosition(energysource));
					if(energysourceother.trim().equals(""))
					{
						ed_otherenergysource.setVisibility(v.GONE);
					}
					else
					{
						ed_otherenergysource.setVisibility(v.VISIBLE);
						ed_otherenergysource.setText(energysourceother);
					}System.out.println("energyy");
					if(energysource.equals("Gas") || energysource.equals("Kerosene") || energysource.equals("Liquid Propane"))
					{ 
						sp_gasshutoff.setSelection(gasshutoffarrayadap.getPosition(gasshutoff));
						if(gasshutoffother.trim().equals(""))
						{
							ed_othergasshutoff.setVisibility(v.GONE);
						}
						else
						{
							ed_othergasshutoff.setVisibility(v.VISIBLE);
							ed_othergasshutoff.setText(gasshutoffother);
						}
						
						 if(!tempfueltankloc.equals(""))
						 {
							 fueltanklocnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempfueltankloc));
							 fueltanklocnote.setVisibility(v.VISIBLE); 
						 }
						 else
						 {
							 fueltanklocnote.setVisibility(v.GONE);	 
						 }
						 
						 
						 if(!tempfluetype.equals(""))
						 {
							 fluetypenote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempfluetype));
							 fluetypenote.setVisibility(v.VISIBLE); 
						 }
						 else
						 {
							 fluetypenote.setVisibility(v.GONE);	 
						 }
						
					}
					else
					{
						
					}
					
					//spin_noofzones.setSelection(noofzonesarrayadap.getPosition(noofzones));
					System.out.println("aheating age="+age);
					spin_heatingage.setSelection(agearrayadap.getPosition(age));
					if(otherage.trim().equals(""))
					{
						ed_otherheatingage.setVisibility(v.INVISIBLE);
					}
					else
					{
						ed_otherheatingage.setVisibility(v.VISIBLE);
						ed_otherheatingage.setText(otherage);
					}
					sp_furnace.setSelection(furnacearrayadap.getPosition(furnacetype));System.out.println("blowermotor"+blowermotor);
					sp_motorblower.setSelection(motorarrayadap.getPosition(blowermotor));
					sp_heatdelivery.setSelection(heatdeliveryarrayadap.getPosition(heatdeliverytype));
					
					sp_zonetonnage.setSelection(zonetonnadap.getPosition(tonnage));
					if(tonnageother.trim().equals(""))
					{
						ed_otherzonetonnage.setVisibility(v.GONE);
					}
					else
					{
						ed_otherzonetonnage.setVisibility(v.VISIBLE);
						ed_otherzonetonnage.setText(tonnageother);
					}
					System.out.println("btu"+btu);
					sp_btu.setSelection(btuarrayadap.getPosition(btu));
					if(btuother.trim().equals(""))
					{
						ed_otherbtu.setVisibility(v.GONE);
					}
					else
					{
						ed_otherbtu.setVisibility(v.VISIBLE);
						ed_otherbtu.setText(btuother);
					}		
					System.out.println("tempairhandlerfeat"+tempairhandlerfeat);
					
					 if(!tempairhandlerfeat.equals(""))
					 {
						 airhandlerfeatnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempairhandlerfeat));
						 airhandlerfeatnote.setVisibility(v.VISIBLE); 
					 }
					 else
					 {
						 airhandlerfeatnote.setVisibility(v.GONE);	 
					 }
					 System.out.println("temptestedfor"+temptestedfor);
					 if(!temptestedfor.equals(""))
					 {
						 testedfornote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+temptestedfor));
						 testedfornote.setVisibility(v.VISIBLE); 
					 }
					 else
					 {
						 testedfornote.setVisibility(v.GONE);	 
					 }System.out.println("heatsystembrand"+heatsystembrand);
					 
					 
					 sp_heatbrand.setSelection(brandarrayadap.getPosition(heatsystembrand));System.out.println("updae="+heatsystembrand);
					((Button)findViewById(R.id.btn_addheaating)).setText("Update");System.out.println("aggehheoej");
				}
			}catch(Exception e){}
		
	}
	private void dbvalue(String bathvalue) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		if(bathvalue.equals("Fuel Tank Location"))
		{
			for(int j=0;j<arrfueltank.length;j++)
			{
				str_arrlst.add(arrfueltank[j]);
			}	
		}
		if(bathvalue.equals("Flue Type"))
		{
			for(int j=0;j<arrfluetype.length;j++)
			{
				str_arrlst.add(arrfluetype[j]);
			}	
		}
		if(bathvalue.equals("Tested For"))
		{
			for(int j=0;j<arrzonetestingfor.length;j++)
			{
				str_arrlst.add(arrzonetestingfor[j]);
			}	
		}

		if(bathvalue.equals("Air Handler Features"))
		{
			for(int j=0;j<arrairhandlerfeatures.length;j++)
			{
				str_arrlst.add(arrairhandlerfeatures[j]);
			}	
		}
		
		
	}
	
	private void multi_select(final String hvacvalue, final int s) {
		// TODO Auto-generated method stub
		hvac=1;
		dbvalue(hvacvalue);
		
		final Dialog add_dialog = new Dialog(NewHVAC1.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);

		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);
		
		 		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(hvacvalue);
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);
		btnother.setVisibility(v.GONE);
		if(s==1)
		{
			showsuboption(tempfueltankloc,fueltanklocnote);
		}
		else if(s==2)
		{
			showsuboption(tempfluetype,fluetypenote);
		}
		else if(s==3)
		{
			showsuboption(temptestedfor,testedfornote);
		}
		else if(s==4)
		{
			showsuboption(tempairhandlerfeat,airhandlerfeatnote);
		}
		
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval="";
								for (int i = 0; i < str_arrlst.size(); i++) {
									if (ch[i].isChecked()) {
										seltdval += ch[i].getText().toString() + "&#44;";
									}
								}
								
								
								finalselstrval = "";
								finalselstrval = seltdval;
								if(finalselstrval.equals(""))
								{
									cf.DisplayToast("Please check at least any 1 option to save");
								}
								else
								{	
									if(hvacvalue.equals("Fuel Tank Location"))
									{
										
										fueltankvalue=finalselstrval;
										tempfueltankloc = fueltankvalue;
										tempfueltankloc = cf.replacelastendswithcomma(tempfueltankloc);
										fueltanklocnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempfueltankloc));
										fueltanklocnote.setVisibility(v.VISIBLE);
									}
									


									else if(hvacvalue.equals("Flue Type"))
									{
										fluetypevalue= finalselstrval;
										tempfluetype = fluetypevalue;
										tempfluetype = cf.replacelastendswithcomma(tempfluetype);
										fluetypenote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempfluetype));
										fluetypenote.setVisibility(v.VISIBLE);
									}
									
									else if(hvacvalue.equals("Tested For"))
									{
										testedforvalue=finalselstrval;
										temptestedfor = testedforvalue;
										temptestedfor = cf.replacelastendswithcomma(temptestedfor);
										testedfornote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+temptestedfor));
										testedfornote.setVisibility(v.VISIBLE);
									}
									
									else if(hvacvalue.equals("Air Handler Features"))
									{
										airhandlerfeatvalue=finalselstrval;
										tempairhandlerfeat = airhandlerfeatvalue;
										tempairhandlerfeat = cf.replacelastendswithcomma(tempairhandlerfeat);
										airhandlerfeatnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempairhandlerfeat));
										airhandlerfeatnote.setVisibility(v.VISIBLE);
									}
									
							
									
									add_dialog.cancel();
									if(s==1){fueltank_dialog=0;}
									else if(s==2){fluetype_dialog=0;}
									else if(s==3){testedfordialog=0;}
									else if(s==4){airhandler_dialog=0;}
									hvac=0;
								}
								
					}
				
		});

		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hvac=0;
				if(s==1){fueltank_dialog=0;}
				else if(s==2){fluetype_dialog=0;}
				else if(s==3){testedfordialog=0;}
				else if(s==4){airhandler_dialog=0;}
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				
			}
		});
		setvalue(hvacvalue);
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	
	private void setvalue(String hvacvalue) {
		// TODO Auto-generated method stub
		
		try
		{
			
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHVAC+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			
		    if (cur.getCount() > 0) 
		    {
				cur.moveToFirst();
				if(hvacvalue.equals("Fuel Tank Location"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_fueltankloc")));						 
				}
				else if(hvacvalue.equals("Flue Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));	
				}
				else if(hvacvalue.equals("Tested For"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_testedfor")));	
				}
				else if(hvacvalue.equals("Air Handler Features"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_airhandlerfeatures")));	
				}			
			
				
				if(str.contains(","))
			    {
			    	str = str.replace(",","&#44;");
			    }	
			}
		    else
		    {
			
		    	if(hvacvalue.equals("Fuel Tank Location") && hvacvalue!=null)
				{
					str = fueltankvalue;							 
				}
				else if(hvacvalue.equals("Flue Type") && hvacvalue!=null)
				{
					str = fluetypevalue;		
				}
				else if(hvacvalue.equals("Tested For") && hvacvalue!=null)
				{
					str = testedforvalue;
				}
				else if(hvacvalue.equals("Air Handler Features") && hvacvalue!=null)
				{
					str = airhandlerfeatvalue;
				}			
			
		    }
			    if(!str.trim().equals(""))
				{
					cf.setValuetoCheckbox(ch, str);
				}	
		}catch (Exception e) {
			// TODO: handle exception
		}
	    	
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	private void showsuboption(String selectedvalue,TextView tv) {
		// TODO Auto-generated method stub
		
		if(selectedvalue.trim().equals(""))
		{
			if(tv.getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(tv.getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	tv.getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=selectedvalue;
		}
		

		  ch = new CheckBox[str_arrlst.size()];
			
			for (int i = 0; i < str_arrlst.size(); i++) {
	        		ch[i] = new CheckBox(this);
					ch[i].setText(str_arrlst.get(i));
					ch[i].setTextColor(Color.BLACK);
					
					lin.addView(ch[i]);
					ch[i].setOnCheckedChangeListener(new CHListener(i));
					
			}
		
		

		if (str != "") {
			
			 cf.setValuetoCheckbox(ch, str);
			 String replaceendcommaionspoint= replacelastendswithcomma(str);
			 tv.setVisibility(v.VISIBLE);
			 tv.setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
		}
		else
		{
			tv.setVisibility(v.GONE);
		}
		
		
	}
	private String replacelastendswithcomma(String realword) {
		// TODO Auto-generated method stub
		
		if(realword.contains("&#44;"))
		{
			realword = realword.replace("&#44;",",");
		}
		if(realword.endsWith(","))
		{
			realword = realword.substring(0,realword.length()-1);
		}
		else
		{
			realword = realword;	
		}	
		return realword;
	}
	class CHListener implements OnCheckedChangeListener, android.widget.CompoundButton.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		
		//seltdval="";
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
			}
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	class myspinselectedlistener implements OnItemSelectedListener
	{
         int var=0;        
		public myspinselectedlistener(int i) {
			// TODO Auto-generated constructor stub
			var=i;
			 getspinselectedname="";
		}
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			getspinselectedname = arg0.getItemAtPosition(arg2).toString();
			switch(var)
			{
				case 1:
					 getheatdeliverytype = getspinselectedname;		 
				break;
				case 2:
					 getenergysource = getspinselectedname;
					 if(!getenergysource.equals("--Select--"))
		    		 {			
						if(getenergysource.equals("Gas") || getenergysource.equals("Liquid Propane") || getenergysource.equals("Kerosene"))
						 {
							 ((LinearLayout)findViewById(R.id.energysource1)).setVisibility(View.VISIBLE);
							 ((LinearLayout)findViewById(R.id.energysource2)).setVisibility(View.VISIBLE);	 
							 ((LinearLayout)findViewById(R.id.energysource3)).setVisibility(View.VISIBLE);							 
						 }
						 else
						 {
							 ((LinearLayout)findViewById(R.id.energysource1)).setVisibility(View.GONE);
							 ((LinearLayout)findViewById(R.id.energysource2)).setVisibility(View.GONE);
							 ((LinearLayout)findViewById(R.id.energysource3)).setVisibility(View.GONE);
							 ed_othergasshutoff.setVisibility(arg1.GONE);
						 }
						 
						 
						 if(getenergysource.equals("Other"))
				    	  {
							 ed_otherenergysource.setVisibility(arg1.VISIBLE);
				    		 ed_otherenergysource.requestFocus();
				    		
				    	  }
				    	  else
				    	  {
				    		  ed_otherenergysource.setText("");
				    		  ed_otherenergysource.setVisibility(arg1.GONE);
				    	  }
						 
		    		 }
					 else
					 {
						 ((LinearLayout)findViewById(R.id.energysource1)).setVisibility(View.GONE);
						 ((LinearLayout)findViewById(R.id.energysource2)).setVisibility(View.GONE);
						 ((LinearLayout)findViewById(R.id.energysource3)).setVisibility(View.GONE);
					 }
				break;
				case 3:
					 getheatingtype = getspinselectedname;
			    	  Cursor cur;
			    	  try
						{
			    		  if(getheatingtype.equals("Other"))
			    		  {
			    			  ed_otherheattype.setVisibility(arg1.VISIBLE);
			    			  ed_otherheattype.requestFocus();
			    			  cur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatingtypeother='"+strotherheattype+"'", null);
			    		  }
			    		  else
			    		  {
			    			  ed_otherheattype.setVisibility(arg1.GONE);
			    			  cur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+getheatingtype+"'", null);
			    		  }
					   }
			    	  catch (Exception e) {
						// TODO: handle exception
					  }  
			    	  break;
				 case 5:
			    	  getheatingage = getspinselectedname;
			    	  if(getheatingage.equals("Other"))
			    	  {
			    		  ed_otherheatingage.setVisibility(arg1.VISIBLE);
			    		  ed_otherheatingage.requestFocus();			    		
			    	  }
			    	  else
			    	  {
			    		  ed_otherheatingage.setText("");
			    		  ed_otherheatingage.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
					case 6:
						 getheatbrand = getspinselectedname;	 
					break;
					case 7:
						getfurnace = getspinselectedname;		 
					 break;
					case 10:
						getmotorblower = getspinselectedname;		 
						break;
					case 13:
			    	  getgasshutoff = getspinselectedname;
			    	  if(getgasshutoff.equals("Other"))
			    	  {
			    		  ed_othergasshutoff.setVisibility(arg1.VISIBLE);
			    		  ed_othergasshutoff.requestFocus();			    		 
			    	  }
			    	  else
			    	  {
			    		  ed_othergasshutoff.setText("");
			    		  ed_othergasshutoff.setVisibility(arg1.GONE);
			    	  }
			    	  break;
				  case 14:
			    	  getfluetype = getspinselectedname;
			    	  if(getfluetype.equals("Other"))
			    	  {
			    		  ed_otherfluetype.setVisibility(arg1.VISIBLE);
			    		  ed_otherfluetype.requestFocus();
			    		 
			    	  }
			    	  else
			    	  {

			    		  ed_otherfluetype.setText("");
			    		  ed_otherfluetype.setVisibility(arg1.GONE);
			    	  }
			    	  break;
				  case 16:
			    	  getzoneno = getspinselectedname;
			    	  /*if(getzoneno.equals("Other"))
			    	  {
			    		  ((EditText)findViewById(R.id.other_zoneno)).setVisibility(arg1.VISIBLE);
			    		  ((EditText)findViewById(R.id.other_zoneno)).requestFocus();
			    		 
			    	  }
			    	  else
			    	  {

			    		  ((EditText)findViewById(R.id.other_zoneno)).setText("");
			    		  ((EditText)findViewById(R.id.other_zoneno)).setVisibility(arg1.GONE);
			    	  }*/
			    	  break;
				  case 25:
			    	  getzonename = getspinselectedname;
			    	  if(getzonename.equals("Other"))
			    	  {
			    		  ((EditText)findViewById(R.id.other_zonename)).setVisibility(arg1.VISIBLE);
			    		  ((EditText)findViewById(R.id.other_zonename)).requestFocus();			    		 
			    	  }
			    	  else
			    	  {

			    		  ((EditText)findViewById(R.id.other_zonename)).setText("");
			    		  ((EditText)findViewById(R.id.other_zonename)).setVisibility(arg1.GONE);
			    	  }
			    	  break;
				
				  case 29:
					  getzonetonnage= getspinselectedname;
			    	  if(getzonetonnage.equals("Other"))
			    	  {
			    		  ed_otherzonetonnage.setVisibility(arg1.VISIBLE);
			    		  ed_otherzonetonnage.requestFocus();
			    	  }
			    	  else
			    	  {
			    		  ed_otherzonetonnage.setText("");
			    		  ed_otherzonetonnage.setVisibility(arg1.GONE);
			    	  }
			    	  break;
				  case 30:
			    	  getbtu= getspinselectedname;
			    	  if(getbtu.equals("Other"))
			    	  {
			    		  ed_otherbtu.setVisibility(arg1.VISIBLE);
			    		  ed_otherbtu.requestFocus();
			    	  }
			    	  else
			    	  {
			    		  ed_otherbtu.setText("");
			    		  ed_otherbtu.setVisibility(arg1.GONE);
			    	  }
			    	  break;	
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	protected void back() {
		// TODO Auto-generated method stub
		Intent intent;
		if(getcurrentview.equals("start"))
		{
			db.userid();
			intent = new Intent(NewHVAC1.this, LoadModules.class);
			Bundle b1 = new Bundle();
			b1.putString("selectedid", getselectedho);			
			intent.putExtras(b1);
			
			Bundle b2 = new Bundle();
			b2.putString("inspectorid", db.UserId);			
			intent.putExtras(b2);
			startActivity(intent);
			finish();
		}
		else
		{	
			 if(gettempoption.equals("Report Section/Module"))
		        {
		       	  intent = new Intent(NewHVAC1.this, CreateTemplate.class);
		        }
		        else
		        {
		       	 intent = new Intent(NewHVAC1.this, CreateModule.class);
		        }
		        
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("tempoption", gettempoption);			
				intent.putExtras(b5);
				startActivity(intent);
				finish();
		}}

	protected void onSaveInstanceState(Bundle ext)
	{		
		ext.putInt("chkvalue", chk);
		ext.putString("hvactype",sp_heatingtype.getSelectedItem().toString());
		ext.putString("heatypeother",ed_otherheattype.getText().toString());
		
		ext.putString("energysource",sp_energysource.getSelectedItem().toString());
		ext.putString("energysourceother",ed_otherenergysource.getText().toString());
		ext.putString("columnname", hvacvalue);
		
		ext.putInt("fueltankdialog",fueltank_dialog);	
		ext.putInt("index",k);
		 if(fueltank_dialog==1)
	        {
	        	selectvalfueltypeloc="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalfueltypeloc += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("fueltanklocoption_index", selectvalfueltypeloc);
	        }
	        else
	        {
	        	ext.putString("fueltanklocoption_index", tempfueltankloc);
	        }
		 ext.putInt("fluetypedialog",fluetype_dialog);	
		 if(fluetype_dialog==1)
	        {
			 selectvalfueltype="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalfueltype += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("fluetypeoption_index", selectvalfueltype);
	        }
	        else
	        {
	        	ext.putString("fluetypeoption_index", tempfluetype);
	        }
		 ext.putInt("testedfordialog",testedfordialog);	
		 if(testedfordialog==1)
		 {
			 selectvaltestedfor="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvaltestedfor += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("testerforoption_index", selectvaltestedfor);
		 }
		 else
		 {
				ext.putString("testerforoption_index", temptestedfor);
		 }
		 
		 
		 	ext.putInt("airhandler_index", airhandler_dialog);
		    if(airhandler_dialog==1)
	        {
		    	selectvalueairhandler="";
	        	for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						selectvalueairhandler += ch[i].getText().toString() + "&#44;";
					}
				}
	        	ext.putString("sp_airhanlderfeat", selectvalueairhandler);
	        }
	        else
	        {
	        	ext.putString("sp_airhanlderfeat", tempairhandlerfeat);
	        }

		    ext.putString("get_airhandlertext", ((TextView) findViewById(R.id.airhandlerfeattv)).getText().toString());
		    ext.putString("get_fueltankloctext", ((TextView) findViewById(R.id.fueltankloctv)).getText().toString());
		    ext.putString("get_fluetypetext", ((TextView) findViewById(R.id.fluetypetv)).getText().toString());
		    ext.putString("get_testedforloctext", ((TextView) findViewById(R.id.testedfortv)).getText().toString());
		super.onSaveInstanceState(ext);
	}
	protected void onRestoreInstanceState(Bundle ext)
	{
		chk = ext.getInt("chkvalue");
		
		
		airhandler_dialog = ext.getInt("airhandler_index");		
		tempairhandlerfeat = ext.getString("sp_airhanlderfeat");
	    if(airhandler_dialog==1)
	   {
		   hvacvalue = ext.getString("columnname");
		   multi_select(ext.getString("columnname"),ext.getInt("index"));
		   if(!tempairhandlerfeat.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, tempairhandlerfeat);
		   }
	   }
	
	   getairhandler = ext.getString("get_airhandlertext");
	   if(!getairhandler.equals(""))
	   {
		   
		   ((TextView) findViewById(R.id.airhandlerfeattv)).setVisibility(v.VISIBLE);
		   ((TextView) findViewById(R.id.airhandlerfeattv)).setText(getairhandler);
	   }
	   
	   fueltank_dialog = ext.getInt("fluetypedialog");		
		tempfueltankloc = ext.getString("fueltanklocoption_index");
	    if(fueltank_dialog==1)
	   {
		   hvacvalue = ext.getString("columnname");
		   multi_select(ext.getString("columnname"),ext.getInt("index"));
		   if(!tempfueltankloc.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, tempfueltankloc);
		   }
	   }
		
		   getfueltankloc = ext.getString("get_fueltankloctext");
		   if(!getfueltankloc.equals(""))
		   {
			   
			   ((TextView) findViewById(R.id.fueltankloctv)).setVisibility(v.VISIBLE);
			   ((TextView) findViewById(R.id.fueltankloctv)).setText(getfueltankloc);
		   }
	    
	
		   fluetype_dialog = ext.getInt("fueltankdialog");
		   
			tempfluetype = ext.getString("fluetypeoption_index");
		    if(fluetype_dialog==1)
		   {
			   hvacvalue = ext.getString("columnname");
			   multi_select(ext.getString("columnname"),ext.getInt("index"));
			   if(!tempfluetype.equals(""))
			   {				
			    	  cf.setValuetoCheckbox(ch, tempfluetype);
			   }
		   }
			
			   getfluetype = ext.getString("get_fluetypetext");
			   if(!getfluetype.equals(""))
			   {
				   
				   ((TextView) findViewById(R.id.fluetypetv)).setVisibility(v.VISIBLE);
				   ((TextView) findViewById(R.id.fluetypetv)).setText(getfluetype);
			   }
			   
			   testedfordialog = ext.getInt("fueltankdialog");		
			   temptestedfor = ext.getString("testerforoption_index");
			    if(testedfordialog==1)
			   {
				   hvacvalue = ext.getString("columnname");
				   multi_select(ext.getString("columnname"),ext.getInt("index"));
				   if(!temptestedfor.equals(""))
				   {				
				    	  cf.setValuetoCheckbox(ch, temptestedfor);
				   }
			   }
				
				   gettestedfor = ext.getString("get_testedforloctext");
				   if(!gettestedfor.equals(""))
				   {
					   
					   ((TextView) findViewById(R.id.testedfortv)).setVisibility(v.VISIBLE);
					   ((TextView) findViewById(R.id.testedfortv)).setText(gettestedfor);
				   }
			   
		
		if(chk==1)
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
		}
		else
		{
			((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
		}
		if(ext.getString("hvactype").equals("Other"))
		{
			ed_otherheattype.setVisibility(v.VISIBLE);	
			ed_otherheattype.setText(ext.getString("heatypeother"));
		}
		else
		{
			ed_otherheattype.setVisibility(v.GONE);
		}
		if(ext.getString("energysource").equals("Other"))
		{
			ed_otherenergysource.setVisibility(v.VISIBLE);	
			ed_otherenergysource.setText(ext.getString("energysourceother"));
		}
		else
		{
			ed_otherenergysource.setVisibility(v.GONE);
		}
		 super.onRestoreInstanceState(ext);
	}
}
