package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.LoadShowDialog.CHListener;
import idsoft.idepot.homeinspection.support.ArrayModuleList;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.io.Externalizable;
import java.util.ArrayList;




import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class LoadPoolShowDialog extends Activity{
	String strwaterchemistry="",strwater="Water Chemistry Test",strnoofpumpspresent="",strpumptypes="",strvalue1="",strvalue2="",strvalue3="",strvalue4="",strvalue5="",strgasheater="",strsolarheater="",strthermostat="",arrstrname="",gettempname="",getph="",getmodulename="",str="",strmaterial="",strskimmer="",strmaindrain="",strvalves="",strautomated="",strswitchlocation="",strlocation="",seltdval="",strladder="",strsubppoloption="",strsubppoloptionother="",strdivingborad="",strslide="",stladderother="",stdivingother="",stslideother="",
			seltdvalsub="",gettempoption="",strchk="",strother="",apprdval="",strsuboption="",strvalue="",strsubvalue="",strpoolfeatmain="",strsuboption1="",strsuboption2="",strsuboption3="",strpoolother="";
	DatabaseFunction db;
	CommonFunction cf;
	ArrayModuleList ar;
	Cursor c,c1;
	String[] s; int i=0,iden=0;
	String getcurrentview="",selectedval="";
	CheckBox[] ch,childch,ch1,childch1,childch2,childch3,childch4,childch5;
	EditText[] wateret;String finalselstrval = "",finalselstrval1="",finalselstrval2="",finalselstrval3="",finalselstrval4="",finalselstrval5="";
	RadioButton[] rd;
	int dialog_other=0,getid=0;//pp=0;
	RadioGroup rg;
	View v;
	RadioButton[] rds1,rds2,rds3,rds4,rds5;
	LinearLayout poollin,lin,rdlin,firstlin,secondlin,seclin,thirdlin,thrlin,fourthlin,fourlin,fifthlin,fivelin;
	TextView firsttxt,secondtxt,thirdtxt,fourthtxt,fifthtxt;
	EditText etother;
	EditText ettxt,childettxt,secchildettxt,thrchildettxt,fourchildettxt,fivechildettxt;
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	//static ArrayList<String> str_arrlst1 = new ArrayList<String>();
	String[] rdname,poolfeatures={"Ladders","Diving Board","Slide","Railing"},laddertype={"Stainless Steel","Plastic","Other"},
			 divingboardtype={"Fiberglass","Stainless Frame","Other"},
			 poollighting={"High Voltage","Low Voltage","Fibreoptics",
			"Switch Location","Operable"},poollocation={"Right Side","Rear Side","Left Side","Front Side","Other"},
			switchlocation={"Inside","Outside","Timer Controlled","Remote","Other"},
			poolequipment={"Equipment Supported On Concrete Pad","Location","Protect From Roof Run Off"},
			plumbingsystem={"Material","Skimmer","Main Drain","Valves","Automated Fill System"},material={"PVC","Copper",
			"Brass","Galvanized","PEX"},skimmer={"1 Skimmer","2 Skimmer","Water Line Above Skimmer","Cover - Lid Present","Basket Present",
			"Floating Barrel Present","Other"},maindrain={"Located Deepest End","Safety Drain � Anti-Vortex Cover Present",
			"Safety Drain Cover Not Present","Hydrostatic Valve Not Confirmed","Other"},valves={"Three Port Valve","Motorized Three Port Valve",
			"Reverse Flow Water Heater Valve(s)","Multiport Valve","Check Valve(s)","Gate Valve(s)","Ball Valve","Other"},automated={"Water Level Control Device Present",
			"Programmable","Manual"},poolpumping={"No Of Pumps Present","Pump Types"},pumppresent={"1","2","3","4","Other"},pumptypes={"Booster Pump","Spa Pump",
			"Jazuzzi Pump","Strainer Pot/Basket","Fountain Pump","Other"},
        	poolcleaning={"Booster Pump System","Suction Side System","Boosterless Water Pressure System","Electric Robot","Self Cleaning Jets"},
        	boosterpumpsystem={"Vacuum Head","Sweep Head"},watertreatment={"InLine Chlorinator","Pump Chlorinator","Ozone Unit","Floating Chlorine Device",
			"Manual Delivery"},pumpchlor={"Peristaltic","Standard","Other"},waterheatingsystem={"Gas Heater","Solar Heater","Heat Pump","Electric Heater","Oil Heater","Thermostat",
			"On � Off Switch"},gasheater={"Natural Gas","Propane","Electronic Ignition","Pilot Light On","Pilot Light Off","Gas Valve Present",
			"Fusible Link","Pressure Switch","High Limit Switch","Not Tested"},solarheater={"Open Loop","Closed Loop"},thermostat={"Located Inside","Located Outside","Located At Equipment"},
			timers={"Electromechanical Timer","Electronic Timers","Twist Timer","Location","Remote Control(s)","Programmable Controls"},timerlocation={"Inside","At Equipment",
			"Remote","Other"},remotecontrols={"Pneumatic Air Switch","Electronic Switch"},progcontrols={"Inside","Outside"},
			electricalservice={"External Sub Panel","External Receptacle","Wiring Type","Conduit � Wire Protection","Equipment Bonding","Cover Present"},
			extsubpanel={"30 AMP","40 AMP","50 AMP","100 AMP","Circuit Breaker(s)","Shut Off","Other"},wiringtype={"Romex","Other"},conduit={"Plastic","Water Resistant",
			"Other"},poolcovers={"Spa Only","Pool and Spa","Specialty Spa Cover","Bubble Cover","Rollers","Vinyl","Electronic","Fiberglass",
           "Mesh","Wood","Safety Clips/Features Present"},safetyequipment={"Signage","Floatation Device/Toss Ring","Life Hook","Safety Barrier"},
           waterchemistry={"Chlorine residual","Total Alkalinity","pH","Hardness","Total Dissolved Solids","Cyanuric Acid","Temperature"},generalcomments={"Filter",
			"Pumps","Filter Pressure","Heater"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		
		    Bundle b = this.getIntent().getExtras();
			arrstrname = b.getString("arrstr");
			Bundle b1 = this.getIntent().getExtras();
			gettempname = b1.getString("templatename");
			Bundle b2 = this.getIntent().getExtras();
			getmodulename = b2.getString("modulename");
			Bundle b3 = this.getIntent().getExtras();
			getph = b3.getString("selectedid");
			Bundle b4 = this.getIntent().getExtras();
			getid = b4.getInt("id");
			Bundle b5 = this.getIntent().getExtras();
			getcurrentview = b5.getString("currentview");
			Bundle b6 = this.getIntent().getExtras();
			gettempoption = b6.getString("tempoption");
			setContentView(R.layout.poolcustomizedialog);
			
			Declaration();
			
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		 System.out.println("what is temp name = "+gettempname);
		 
		 
		 firstlin = (LinearLayout)findViewById(R.id.first);
			firsttxt = (TextView)findViewById(R.id.firsttext);
			secondlin = (LinearLayout)findViewById(R.id.second);
			secondtxt = (TextView)findViewById(R.id.secondtext);
			thirdlin = (LinearLayout)findViewById(R.id.third);
			thirdtxt = (TextView)findViewById(R.id.thirdtext);
			fourthlin = (LinearLayout)findViewById(R.id.fourth);
			fourthtxt = (TextView)findViewById(R.id.fourthtext);
			fifthlin = (LinearLayout)findViewById(R.id.fifth);
			fifthtxt = (TextView)findViewById(R.id.fifthtext);
			
			rdlin = (LinearLayout)findViewById(R.id.rdg);
			lin = (LinearLayout)findViewById(R.id.chk);
			seclin = (LinearLayout)findViewById(R.id.secondchk);
			thrlin = (LinearLayout)findViewById(R.id.thirdchk);
			fourlin = (LinearLayout)findViewById(R.id.fourthchk);
			fivelin = (LinearLayout)findViewById(R.id.fifthchk);
			ettxt = (EditText)findViewById(R.id.othertxt);
			childettxt = (EditText)findViewById(R.id.childothertxt);
			secchildettxt = (EditText)findViewById(R.id.secondchildothertxt);
			thrchildettxt = (EditText)findViewById(R.id.thirdchildothertxt);
			fourchildettxt = (EditText)findViewById(R.id.fourthchildothertxt);
			fivechildettxt = (EditText)findViewById(R.id.fifthchildothertxt);
		 
		ar = new ArrayModuleList(this,getmodulename);
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		
		db.CreateTable(1);
		db.CreateTable(3);
		db.CreateTable(8);
		db.CreateTable(7);
		db.CreateTable(26);
		db.CreateTable(27);
		db.userid();
		
		System.out.println("getccc="+getcurrentview);
		if(getcurrentview.equals("start"))
		{
	    	checkexistingtemplate();	
		}
		dbvalue();
    	show_alert();
		setvalue();
	}
	
	
	
	private void checkexistingtemplate() {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur= db.hi_db.rawQuery("select * from " + db.SaveSubOptionPools + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			  System.out.println("cur.getcount="+cur.getCount()+"select * from " + db.SaveSubOptionPools + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and  fld_templatename='"+db.encode(gettempname)+"'");
			 if(cur.getCount()==0)
			 {
				defaultinsert();
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void defaultinsert() {
		// TODO Auto-generated method stub
		System.out.println("defaultinsert");
		try
		{
			 Cursor cur= db.hi_db.rawQuery("select * from " + db.CreateSubOptionPools + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 for(int i=0;i<cur.getCount();i++)
				 {
					 db.hi_db.execSQL("INSERT INTO "+db.SaveSubOptionPools+" (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_pooloption,fld_subpooloption,fld_subpoolother,fld_templatename) VALUES" +
								"('"+db.UserId+"','"+getph+"','"+cur.getString(2)+"','"+cur.getString(3)+"','"+cur.getString(4)+"','"+cur.getString(5)+"','"+cur.getString(6)+"','"+cur.getString(7)+"')");
						
					 cur.moveToNext();
				 }
				 
			 }
			 
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("catch=="+e.getMessage());
		}
		
		
	}
	private void setvalue() {
		// TODO Auto-generated method stub		
		String poolsubvalue="";
		if(getcurrentview.equals("start"))
		{
			c = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);	
		}
		else
		{
			c = db.hi_db.rawQuery("select * from " + db.CreateDefault+ " WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
		}
		if(c.getCount()>0)
		 {
			 c.moveToFirst();
			 strvalue = db.decode(c.getString(c.getColumnIndex("fld_description")));
			System.out.println("strvalue"+strvalue);
			 if(!strvalue.trim().equals(""))
				{
					/*if(arrstrname.equals("General Comments/Observations"))
					{
						if(strvalue.contains("Other"))
						{
							String str[] = strvalue.split("&#94;");
							cf.setValuetoRadio(rd,str[0]);
							ettxt.setText(str[1]);
						}
						else if(arrstrname.equals("Water Chemistry Test"))
						{
							cf.setValuetoCheckbox(ch, strvalue);
						}
						else
						{
							cf.setValuetoRadio(rd,strvalue);
						}
					}	*/
					 if(arrstrname.equals("Water Chemistry Test"))
						{
							cf.setValuetoCheckbox(ch, strvalue);
						}
					else
					{
						cf.setvaluechkwithother(strvalue,ch,ettxt);
					}
				}			
				
				for(int i=0;i<str_arrlst.size();i++)
				{					
					/*if( arrstrname.equals("General Comments/Observations"))
					{
						if(rd[i].isChecked())
						{
							poolsubvalue = rd[i].getTag().toString();
						}
					}
					else*/ if(arrstrname.equals("Water Chemistry Test"))
					{
							poolsubvalue="Water Chemistry Test";
					}
					else
					{
						if(ch[i].isChecked())
						{
							poolsubvalue = ch[i].getText().toString();
						}
					}
				
					if(getcurrentview.equals("start"))
					{
						 c1 = db.hi_db.rawQuery("select * from " + db.SaveSubOptionPools+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_pooloption='"+db.encode(poolsubvalue)+"'", null);
					}
					else
					{
						 c1 = db.hi_db.rawQuery("select * from " + db.CreateSubOptionPools+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_pooloption='"+db.encode(poolsubvalue)+"'", null);
					}
					
					
					if(c1.getCount()>0)
						{
							c1.moveToFirst();
			            	strsuboption = db.decode(c1.getString(c1.getColumnIndex("fld_subpooloption")));System.out.println("strsub="+strsuboption);
			            	strpoolother = db.decode(c1.getString(c1.getColumnIndex("fld_subpoolother")));

			            		if(poolsubvalue.equals("Ladders") || poolsubvalue.equals("Booster Pump System") || poolsubvalue.equals("Switch Location") || poolsubvalue.equals("External Sub Panel") || poolsubvalue.equals("Gas Heater") || poolsubvalue.equals("Material") )
			            		{
			            			System.out.println("cem inside 111");
			            			
			            			cf.setValuetoCheckbox(childch1, strsuboption);
			            			SetOther(strsuboption,childettxt,strpoolother);
			            		}
			            		if(poolsubvalue.equals("Diving Board") || poolsubvalue.equals("Skimmer") || poolsubvalue.equals("Wiring Type") || poolsubvalue.equals("Solar Heater") || poolsubvalue.equals("Pump Types"))
			            		{
			            			cf.setValuetoCheckbox(childch2, strsuboption);
			            			SetOther(strsuboption,secchildettxt,strpoolother);
			            		}
			            		if(poolsubvalue.equals("Slide") || poolsubvalue.equals("Conduit � Wire Protection") || poolsubvalue.equals("Thermostat") || poolsubvalue.equals("Main Drain"))
			            		{
			            			cf.setValuetoCheckbox(childch3, strsuboption);
			            			SetOther(strsuboption,thrchildettxt,strpoolother);
			            		}
			            		if(poolsubvalue.equals("No Of Pumps Present") ||  poolsubvalue.equals("Location"))
			            		{
			            			cf.setValuetoRadio(rds1, strsuboption);
			            			SetOther(strsuboption,childettxt,strpoolother);
			            		}
			            		if(poolsubvalue.equals("Pump Chlorinator") ||  poolsubvalue.equals("Remote Control(s)"))
			            		{
			            			cf.setValuetoRadio(rds2, strsuboption);
			            			SetOther(strsuboption,secchildettxt,strpoolother);
			            		}
			            		if(poolsubvalue.equals("Programmable Controls"))
			            		{
			            			cf.setValuetoRadio(rds3, strsuboption);
			            			SetOther(strsuboption,thrchildettxt,strpoolother);
			            		}
			            		if(poolsubvalue.equals("Valves"))
			            		{
			            			cf.setValuetoCheckbox(childch4, strsuboption);
			            			SetOther(strsuboption,fourchildettxt,strpoolother);
			            		}
			            		if(poolsubvalue.equals("Automated Fill System"))
			            		{
			            			cf.setValuetoCheckbox(childch5, strsuboption);
			            			SetOther(strsuboption,fivechildettxt,strpoolother);
			            		}
			            		if(poolsubvalue.equals("Water Chemistry Test"))
			            		{
			            			System.out.println("setinside set water"+strsuboption);
			            			if(strsuboption.contains("&#90;"))
			            			{
			            				String str[] = strsuboption.split("&#90;");System.out.println("len="+str.length);
			            				
			            					for(int j=0;j<str.length;j++)
					            			{
				            					
				            					if(ch[i].isChecked())
					            				{System.out
														.println("check");
				            						if(str[j].contains("&#44;"))
							            			{
				            						   String strsub[] = str[j].split("&#44;");
					            					   //System.out.println("its checked"+strsub[0]+ch[i].getText().toString());
				            						  /* System.out
															.println("strsub"+strsub.length);
				            						   System.out
															.println("strsuvb0"+strsub[0]);
				            						   System.out
															.println("strsub1"+strsub[1]);*/
					            					   if(strsub[0].equals(ch[i].getText().toString()))
					            					   { 
					            						   System.out.println("inside");
					            						   if(strsub.length>1)
					            						   {
						            						   if(!strsub[1].trim().equals(""))
						            						   {
								            						System.out.println("comesinsidechkbxtrue"+strsub[1]);
								            						wateret[i].setText(strsub[1]);
								            					}else
									            				{
									            					wateret[i].setText("");
									            				}
					            						   }
					            						   else
					            						   {
					            							   
					            						   }
					            					   }
					            					 
					            					}
					            					
					            				}
					            			}
			            			}
			            			else
			            			{
			            				System.out.println("comes else");
			            			}
			            		
			            			//setwater(ch, strsuboption);
			            			
			            		}
						}
				}
		 }		
	}
	private void setwater(CheckBox[] checkbox, String strsuboption) {
		// TODO Auto-generated method stub
		for(int i=0;i<checkbox.length;i++)
		{
		
		System.out.println("stropio="+strsuboption);
		String str[] = strsuboption.split("&#90;");
			for(int j=0;j<str.length;j++)
			{
				
				String strsub[] = str[j].split("&#44;;");
				System.out.println("strsub="+strsub[0]);
				if(checkbox[i].isChecked())
				{
					System.out.println("its checked");
					if(strsub[0].equals(checkbox[i].getText().toString()))
					{
					
						System.out.println("comesinsidechkbxtrue"+strsub[1]);
						wateret[j].setText(strsub[1]);
					}
				}
			}
		}
	
	}
	private void SetOther(String suboption, EditText ed,String strothervalue) {
		// TODO Auto-generated method stub
		if(suboption.contains("Other"))
		{
			ed.setVisibility(v.VISIBLE);
			ed.setText(strothervalue);
		}
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	//	back();
	}

	private void dbvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		 if(arrstrname.equals("Pool Features"))
		 {
			 for(int j=0;j<poolfeatures.length;j++)
			 {
					str_arrlst.add(poolfeatures[j]);
			 }	
		 }
		
		if(arrstrname.equals("Pool Lighting"))
		 {
			 for(int j=0;j<poollighting.length;j++)
			 {
					str_arrlst.add(poollighting[j]);
			 }				
		 }
		 else if(arrstrname.equals("Pool Equipment"))
		 {
			 for(int j=0;j<poolequipment.length;j++)
			 {
					str_arrlst.add(poolequipment[j]);
			 }	
		 }
		 else if(arrstrname.equals("Plumbing Systems"))
		 {
			 for(int j=0;j<plumbingsystem.length;j++)
			 {
				str_arrlst.add(plumbingsystem[j]);
			 }
		 }
		 else if(arrstrname.equals("Pool Pumping System"))
		 {

			 for(int j=0;j<poolpumping.length;j++)
			 {
				str_arrlst.add(poolpumping[j]);
			 }			 
		 }
		  else if(arrstrname.equals("Pool Cleaning Equipment"))
		 { 
			 for(int j=0;j<poolcleaning.length;j++)
			 {
				str_arrlst.add(poolcleaning[j]);
			 }	
		 }
		 else if(arrstrname.equals("Water Treatment"))
		 {
			 for(int j=0;j<watertreatment.length;j++)
			 {
				str_arrlst.add(watertreatment[j]);
			 }	
		 }
		 else if(arrstrname.equals("Water Heating Systems"))
		 {
			 for(int j=0;j<waterheatingsystem.length;j++)
			 {
				str_arrlst.add(waterheatingsystem[j]);
			 }	
		 }
		 else if(arrstrname.equals("Timers/Clocks/Controls"))
		 {
			 for(int j=0;j<timers.length;j++)
			 {
				str_arrlst.add(timers[j]);
			 }	
		 }
		 else if(arrstrname.equals("Electrical Service"))
		 {
			 for(int j=0;j<electricalservice.length;j++)
			 {
				str_arrlst.add(electricalservice[j]);
			 }	
		 }
		 else if(arrstrname.equals("Pool Cover(s)"))
		 {
			 for(int j=0;j<poolcovers.length;j++)
			 {
				str_arrlst.add(poolcovers[j]);
			 }	
		 }
		 else if(arrstrname.equals("Safety Equipment"))
		 {
			 for(int j=0;j<safetyequipment.length;j++)
			 {
				str_arrlst.add(safetyequipment[j]);
			 }	
		 }
		 else if(arrstrname.equals("Water Chemistry Test"))
		 {
			 for(int j=0;j<waterchemistry.length;j++)
			 {
				str_arrlst.add(waterchemistry[j]);
			 }	
		 }
		 else if(arrstrname.equals("General Comments/Observations"))
		 {
			 for(int j=0;j<generalcomments.length;j++)
			 {
				str_arrlst.add(generalcomments[j]);
			 }	
		 }	
		
		
		try
		{
			 Cursor c1=db.hi_db.rawQuery("select * from " + db.AddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			 if(c1.getCount()>0)
			 {
				 if (c1 != null) {
					 c1.moveToFirst();System.out.println("cn="+c1.getCount());
						do {
						    String defvalue =db.decode(c1.getString(c1.getColumnIndex("fld_other")));
						    str_arrlst.add(defvalue);
							Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_templatename='"+gettempname+"'", null);
	    		            int ccnt = cur.getCount();System.out.println("ccnt="+ccnt);
	    		            if(ccnt==0)
	    		            {System.out.println("insert");
	    		            	db.hi_db.execSQL(" INSERT INTO "
										+ db.SaveAddOther
										+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
										+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+arrstrname+"','"
										+ db.encode(defvalue) + "','"+gettempname+"')");
	    		            	
	    		            }
	    		         //   cur.close();
						
						}while (c1.moveToNext());
					}
			 }
			 else
			 {System.out.println("else");
				 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'",null);
				System.out.println("else"+"select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+arrstrname+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'");
				 if(c.getCount()>0)
				 {
					 c.moveToFirst();
					 for(int i =0;i<c.getCount();i++,c.moveToNext())
					 {
						 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
					 }//c.close();
				 }
			 }
		
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

     public void show_alert() {		
	
		lin = (LinearLayout)findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)findViewById(R.id.header);
		hdr.setText(arrstrname);
		
		show();
	
		//show_suboption();
		System.out.println("afetr shhh");
		Button btncancel = (Button) findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		Button btnsave = (Button)findViewById(R.id.ok);
		btnsave.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("inisdeeede savvveee="+arrstrname);
				 seltdval = "";
				 if(arrstrname.equals("Pool Features")  ||  arrstrname.equals("Plumbing Systems")  || arrstrname.equals("Pool Cleaning Equipment") 
						 || arrstrname.equals("Pool Lighting")  ||  arrstrname.equals("Electrical Service") ||  arrstrname.equals("Water Heating Systems"))
					{
						if(firstlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < childch1.length; i++) {
								if (childch1[i].isChecked()) {
								strvalue1 += childch1[i].getText().toString() + "&#44;";
								}
							}
						}						
						if(secondlin.getVisibility()==View.VISIBLE)
						{System.out.println(" childch2.length="+ childch2.length);
							for (int i = 0; i < childch2.length; i++) {
								if (childch2[i].isChecked()) {
								strvalue2 += childch2[i].getText().toString() + "&#44;";
								}
							}
						}						
						if(thirdlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < childch3.length; i++) {
								if (childch3[i].isChecked()) {
								strvalue3 += childch3[i].getText().toString() + "&#44;";
								}
							}
						}
						if(fourthlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < childch4.length; i++) {
								if (childch4[i].isChecked()) {
								strvalue4 += childch4[i].getText().toString() + "&#44;";
								}
							}
						}
						
						if(fifthlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < childch5.length; i++) {
								if (childch5[i].isChecked()) {
								strvalue5 += childch5[i].getText().toString() + "&#44;";
								}
							}
						}
						System.out.println("WAht is s="+strvalue1);
					}
				 	else if(arrstrname.equals("Pool Pumping System"))
					{
						if(firstlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < rds1.length; i++) {
								if (rds1[i].isChecked()) {
								strvalue1 = rds1[i].getText().toString();
								}
							}
						}						
						if(secondlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < childch2.length; i++) {
								if (childch2[i].isChecked()) {
									strvalue2 += childch2[i].getText().toString() + "&#44;";
								}
							}
						}
					}
					else
					{
						if(firstlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < rds1.length; i++) {
								if (rds1[i].isChecked()) {
								strvalue1 = rds1[i].getText().toString();
								}
							}
						}
						if(secondlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < rds2.length; i++) {
								if (rds2[i].isChecked()) {
								strvalue2 = rds2[i].getText().toString();
								}
							}
						}
						
						if(thirdlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < rds3.length; i++) {
								if (rds3[i].isChecked()) {
								strvalue3 = rds3[i].getText().toString();
								}
							}
						}
						
						
						if(fourthlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < rds4.length; i++) {
								if (rds4[i].isChecked()) {
								strvalue4 = rds4[i].getText().toString();
								}
							}
						}
						
						if(fifthlin.getVisibility()==View.VISIBLE)
						{
							for (int i = 0; i < rds5.length; i++) {
								if (rds5[i].isChecked()) {
								strvalue5 = rds5[i].getText().toString();
								}
							}
						}
					}

				 finalselstrval1="";
					finalselstrval1 =strvalue1;
					
					finalselstrval2="";
					finalselstrval2 =strvalue2;
					
					finalselstrval3="";
					finalselstrval3 =strvalue3;
					
					finalselstrval4="";
					finalselstrval4 =strvalue4;
					
					finalselstrval5="";
					finalselstrval5 =strvalue5;
					
					System.out.println("strvalue1="+finalselstrval1);
					System.out.println("strvalue2="+finalselstrval2);
					System.out.println("strvalue3="+finalselstrval3);
					System.out.println("arrstrname="+arrstrname);
				if(arrstrname.equals("Main Panel Meter Located Inside")||arrstrname.equals("Main Panel Meter Located Outside") 
						|| arrstrname.equals("Fire Separation Present")|| arrstrname.equals("Moisture Stains Present")
						|| arrstrname.equals("Typical Cracks") || arrstrname.equals("Fire Door")
						|| arrstrname.equals("Auto Closure") || arrstrname.equals("Water Meter Flow Detector Stable")
						|| arrstrname.equals("5 Year Wiring Replacement Probability")
						 || arrstrname.equals("Supply Registers Located") 
						|| arrstrname.equals("Return Registers Located")|| arrstrname.equals("Safety Glass Satmp at Shower Enclosure")
						|| arrstrname.equals("Spa - Whirlpool Style/Type")|| arrstrname.equals("Water Clarity")|| arrstrname.equals("Pool Shape")
						|| arrstrname.equals("Pool Shell Material"))
				{
					for (int i = 0; i < str_arrlst.size(); i++) {
						if (rd[i].isChecked()) {
							seltdval = rd[i].getText().toString();
						}
					}
					
						finalselstrval="";
						finalselstrval =seltdval;
						System.out.println("try"+seltdval);
						if(finalselstrval.equals(""))
						{
							cf.DisplayToast("Please check at least any 1 option to save");
						}
						else
						{
							if(finalselstrval.contains("Other") && ettxt.getText().toString().equals(""))
							{
								cf.DisplayToast("Please enter the other value for "+arrstrname);
							}
							else
							{
								
								if(finalselstrval.contains("Other"))
								{
									finalselstrval = finalselstrval+ "&#94;"+ettxt.getText().toString();
								}
								
								/*if(arrstrname.equals("Pool Cleaning Equipment"))
								{			
									if(validate_emptystring(finalselstrval1,"Booster Pump System",finalselstrval))
									{
										insertoptionquery(finalselstrval);
										insertsuboptionquery("radio");nextscreen();
									}
								}
								else*/ /*if(arrstrname.equals("Water Treatment"))
									
								{
									if(validate_emptystring(finalselstrval1,"Pump Chlorinator",finalselstrval))
									{
										
											insertoptionquery(finalselstrval);
											insertsuboptionquery("radio");nextscreen();
										
									}
								}*/
								else
								{
									insertoptionquery(finalselstrval);nextscreen();								}
								
								
							}
						}
				}
				
				else
				{
				System.out.println("inisd eelse ");
					for (int i = 0; i < str_arrlst.size(); i++) {
							if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
					System.out.println("ssss="+seltdval);
					
					finalselstrval="";
					finalselstrval =seltdval;System.out.println("finalselstrval"+finalselstrval);
					if(finalselstrval.equals(""))
					{
						cf.DisplayToast("Please check at least any 1 option to save");
					}
					else
					{
						System.out.println("final="+finalselstrval+"Other="+ ettxt.getText().toString());
						if(finalselstrval.contains("Other") && ettxt.getText().toString().equals(""))
						{
							cf.DisplayToast("Please enter the other value");
						}
						else
						{
							if(finalselstrval.length()>=2)
							{
								finalselstrval=finalselstrval.substring(0, finalselstrval.length()-5);
							}
							
							if(finalselstrval.contains("Other"))
							{
								String optionothrvalue =(ch[ch.length-1].isChecked())? "&#94;"+ettxt.getText().toString():"";
								finalselstrval+=optionothrvalue;
							}
								
								if(arrstrname.equals("Pool Features"))
								{			
									if(validate_emptystring(finalselstrval1,"Ladders",finalselstrval))
									{
										if(validate_other(finalselstrval1,childettxt.getText().toString(),"Ladders",finalselstrval))
										{
											if(validate_emptystring(finalselstrval2,"Diving Board",finalselstrval))
											{
												if(validate_other(finalselstrval2,secchildettxt.getText().toString(),"Diving Board",finalselstrval))
												{
													if(validate_emptystring(finalselstrval3,"Slide",finalselstrval))
													{
														if(validate_other(finalselstrval3,thrchildettxt.getText().toString(),"Slide",finalselstrval))
														{
															insertoptionquery(finalselstrval);
															insertsuboptionquery("check");
															 nextscreen();
														}
													}
												}
											}
										}
									}
								}
								else if(arrstrname.equals("Pool Lighting"))
								{
									if(validate_emptystring(finalselstrval1,"Switch Location",finalselstrval))
									{
										if(validate_other(finalselstrval1,childettxt.getText().toString(),"Switch Location",finalselstrval))
										{
											insertoptionquery(finalselstrval);
											insertsuboptionquery("check");
											nextscreen();
										}
									} 
								}
								else if(arrstrname.equals("Pool Equipment"))
								{
									if(validate_emptystring(finalselstrval1,"Location",finalselstrval))
									{
										if(validate_other(finalselstrval1,childettxt.getText().toString(),"Location",finalselstrval))
										{
											insertoptionquery(finalselstrval);
											insertsuboptionquery("check");
											nextscreen();
										}
									} 
								}
								else if(arrstrname.equals("Plumbing Systems"))
								{
									System.out.println("strvvv="+strvalue1);
									if(validate_emptystring(finalselstrval1,"Material",finalselstrval))
									{
										if(validate_emptystring(finalselstrval2,"Skimmer",finalselstrval))
										{
											if(validate_other(finalselstrval2,secchildettxt.getText().toString(),"Skimmer",finalselstrval))
											{
												if(validate_emptystring(finalselstrval3,"Main Drain",finalselstrval))
												{
													if(validate_other(finalselstrval3,thrchildettxt.getText().toString(),"Main Drain",finalselstrval))
													{
														if(validate_emptystring(finalselstrval4,"Valves",finalselstrval))
														{
															if(validate_other(finalselstrval4,fourchildettxt.getText().toString(),"Valves",finalselstrval))
															{
																if(validate_emptystring(finalselstrval5,"Automated Fill System",finalselstrval))
																{
																	if(validate_other(finalselstrval5,fivechildettxt.getText().toString(),"Automated Fill System",finalselstrval))
																	{
																		insertoptionquery(finalselstrval);
																		insertsuboptionquery("check");
																		nextscreen();
																	}													
																}	
															}													
														}		
													}													
												}	
											}													
										}												
									}
								}
								else if(arrstrname.equals("Pool Pumping System"))
								{
									if(validate_emptystring(strvalue1,"No Of Pumps Present",finalselstrval))
									{
										if(validate_other(strvalue1,childettxt.getText().toString(),"No Of Pumps Present",finalselstrval))
										{
											if(validate_emptystring(strvalue2,"Pump Types",finalselstrval))
											{
												if(validate_other(strvalue2,secchildettxt.getText().toString(),"Pump Types",finalselstrval))
												{
													insertoptionquery(finalselstrval);
													insertsuboptionquery("check");
													nextscreen();
												}
											}
										}
									}
								}
								else if(arrstrname.equals("Water Treatment"))
								{
									if(validate_emptystring(finalselstrval2,"Pump Chlorinator",finalselstrval))
									{
										
											insertoptionquery(finalselstrval);
											insertsuboptionquery("check");nextscreen();
										
									}
								}
								else if(arrstrname.equals("Timers/Clocks/Controls"))
								{
									if(validate_emptystring(strvalue1,"Location",finalselstrval))
									{
										if(validate_other(strvalue1,childettxt.getText().toString(),"Location",finalselstrval))
										{
											if(validate_emptystring(strvalue2,"Remote Control(s)",finalselstrval))
											{
												if(validate_emptystring(strvalue3,"Programmable Controls",finalselstrval))
												{
													insertoptionquery(finalselstrval);
													insertsuboptionquery("check");
													nextscreen();
												}
											}
										}
									}
								}
								else if(arrstrname.equals("Electrical Service"))
								{
									if(validate_emptystring(strvalue1,"External Sub Panel",finalselstrval))
									{
										if(validate_other(strvalue1,childettxt.getText().toString(),"External Sub Panel",finalselstrval))
										{
											if(validate_emptystring(strvalue2,"Wiring Type",finalselstrval))
											{
												if(validate_other(strvalue2,secchildettxt.getText().toString(),"Wiring Type",finalselstrval))
												{
													if(validate_emptystring(strvalue3,"Conduit � Wire Protection",finalselstrval))
													{
														if(validate_other(strvalue3,thrchildettxt.getText().toString(),"Conduit � Wire Protection",finalselstrval))
														{
															insertoptionquery(finalselstrval);
															insertsuboptionquery("check");
															nextscreen();
														}
													}
												}												
											}
										}
									}
								}
								else if(arrstrname.equals("Water Heating Systems"))
								{
									if(validate_emptystring(strvalue1,"Gas Heater",finalselstrval))
									{
										if(validate_emptystring(strvalue2,"Solar Heater",finalselstrval))
										{
											if(validate_emptystring(strvalue3,"Thermostat",finalselstrval))
											{
												insertoptionquery(finalselstrval);
												insertsuboptionquery("check");
												nextscreen();
											}
										}
									}
								}
								else if(arrstrname.equals("Pool Cleaning Equipment"))
								{
										if(validate_emptystring(strvalue1,"Booster Pump System",finalselstrval))
										{
											insertoptionquery(finalselstrval);
											insertsuboptionquery("check");
											nextscreen();
										}
								
								}
								else if(arrstrname.equals("Water Chemistry Test"))
								{
									/*for (int i = 0; i < str_arrlst.size(); i++) 
									{
										if (ch[i].isChecked()) 
										{
											if(wateret[i].getText().toString().equals(""))
											{
												cf.DisplayToast("Please enter the value for "+ch[i].getText().toString());	
												return;
											}
											else
											{
												strwaterchemistry += ch[i].getText().toString() +"&#44;;"+wateret[i].getText().toString()+"&#90;";	
												System.out.println("waterchem="+strwaterchemistry);
											}
										}
									}*/
									for (int i = 0; i < str_arrlst.size(); i++) 
									{
										if (ch[i].isChecked()) 
										{
											if(wateret[i].getText().toString().trim().equals(""))
											{
												strwaterchemistry += ch[i].getText().toString() +"&#90;";
											}
											else
											{
												strwaterchemistry += ch[i].getText().toString() +"&#44;"+wateret[i].getText().toString()+"&#90;";
											}
										}
									}
									
										
									System.out.println("waterchem="+strwaterchemistry);
									insertoptionquery(finalselstrval);
									insertsuboptionquery("edittext");
									nextscreen();
								}
								else
								{
									insertoptionquery(finalselstrval);
									nextscreen();
								}
						}						
					}	
				}				
			}
		});
		
		Button btnvc = (Button) findViewById(R.id.setvc);
		btnvc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("loadshowdialog="+getph+gettempname);
				Intent intent = new Intent(LoadPoolShowDialog.this, LoadNewSetVC.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("submodulename", arrstrname);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("selectedid", getph);			
				intent.putExtras(b6);
				Bundle b7 = new Bundle();
				b7.putString("tempoption", gettempoption);			
				intent.putExtras(b7);
				startActivity(intent);
				finish();
				
			}
		});
		
		Button btnother = (Button)findViewById(R.id.addnew);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=1;
				dialogshow();
				
				
			}
		});
	}
	private boolean validate_emptystring(String selectedsuboption, String message,String selectedoption)
     {
			if(selectedoption.contains(message) && selectedsuboption.equals(""))
			{
				cf.DisplayToast("Please check at least 1 option for "+message);
				return false;
			}	
			return true;
     }
     
     
     private boolean validate_other(String suboption, String othervalue, String message,String selectedoption) 
     {
    	if(selectedoption.contains(message) && suboption.contains("Other") && othervalue.equals(""))
		{
			cf.DisplayToast("Please enter the other value for "+message);
			return false;
		}
		return true;
	}
	private void insertoptionquery(String seltdval) {
	// TODO Auto-generated method stub
    	 try
			
    	 {
    	 if(getcurrentview.equals("create"))
    	 {
    		 Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
 			 if (cur.getCount() > 0)
				{
					db.hi_db.execSQL("UPDATE "
							+ db.CreateDefault
							+ " SET fld_description='"
							+ db.encode(seltdval)
							+ "'  WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'");
				}
				else
				{
					db.hi_db.execSQL(" INSERT INTO "
							+ db.CreateDefault
							+ " (fld_inspectorid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES"
							+ "('"+db.UserId+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(seltdval)+"','"+db.encode(gettempname)+"','0')");
				}
    	 }
    	 else
    	 {
    		 Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
				if (cur.getCount() > 0)
				{
					System.out.println("UPDATE "
							+ db.SaveSubModule
							+ " SET fld_description='"
							+ db.encode(seltdval)
							+ "'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'");
					db.hi_db.execSQL("UPDATE "
							+ db.SaveSubModule
							+ " SET fld_description='"
							+ db.encode(seltdval)
							+ "'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'");
				}
				else
				{
					System.out.println(" INSERT INTO "
							+ db.SaveSubModule
							+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES"
							+ "('"+db.UserId+"','"+getph+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(seltdval)+"','"+db.encode(gettempname)+"','0')");
					db.hi_db.execSQL(" INSERT INTO "
							+ db.SaveSubModule
							+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_description,fld_templatename,fld_NA) VALUES"
							+ "('"+db.UserId+"','"+getph+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(seltdval)+"','"+db.encode(gettempname)+"','0')");
				}
    	 }
				
			}
			catch (Exception e) {
				// TODO: handle exception
			}	
    }
	 
	 private void insertsuboptionquery(String control)
	 {
		 String optionvalue="";
		 
			for (int i = 0; i < str_arrlst.size(); i++) 
			{
				if(control.equals("check"))
				{
					if (ch[i].isChecked()) 
					{
						optionvalue =ch[i].getText().toString();
						
						if(ch[i].getText().toString().equals("Ladders") || ch[i].getText().toString().equals("Booster Pump System") || ch[i].getText().toString().equals("Switch Location") ||  ch[i].getText().toString().equals("Material")  ||  ch[i].getText().toString().equals("No Of Pumps Present") || ch[i].getText().toString().equals("Gas Heater") || ch[i].getText().toString().equals("Location") ||  ch[i].getText().toString().equals("External Sub Panel") ||  ch[i].getText().toString().equals("Gas Heater"))
						{
							strsubppoloption = strvalue1;
							strsubppoloptionother = childettxt.getText().toString().trim();
						}
						else if(ch[i].getText().toString().equals("Diving Board") ||  ch[i].getText().toString().equals("Pump Chlorinator") ||  ch[i].getText().toString().equals("Skimmer")  ||  ch[i].getText().toString().equals("Pump Types") || ch[i].getText().toString().equals("Solar Heater") || ch[i].getText().toString().equals("Remote Control(s)") || ch[i].getText().toString().equals("Wiring Type") ||   ch[i].getText().toString().equals("Solar Heater"))
						{
							strsubppoloption = strvalue2;
							strsubppoloptionother = secchildettxt.getText().toString();
						}
						else if(ch[i].getText().toString().equals("Slide") ||  ch[i].getText().toString().equals("Main Drain") || ch[i].getText().toString().equals("Thermostat") || ch[i].getText().toString().equals("Programmable Controls") || ch[i].getText().toString().equals("Conduit � Wire Protection") ||  ch[i].getText().toString().equals("Thermostat"))
						{
							strsubppoloption = strvalue3;
							strsubppoloptionother = thrchildettxt.getText().toString();
						}
						else if(ch[i].getText().toString().equals("Valves"))
						{
							strsubppoloption = strvalue4;
							strsubppoloptionother = fourchildettxt.getText().toString();
						}
						else if(ch[i].getText().toString().equals("Automated Fill System"))
						{
							strsubppoloption = strvalue5;
							strsubppoloptionother = fivechildettxt.getText().toString();
						}						
					}
					insertquery(optionvalue,strsubppoloption,strsubppoloptionother);
				}		
				else if(control.equals("edittext"))
				{
					optionvalue="Water Chemistry Test";
					strsubppoloption = strwaterchemistry;		
					insertquery(optionvalue,strsubppoloption,strsubppoloptionother);
				}
				else
				{
					if (rd[i].isChecked())
					{
						optionvalue =rd[i].getTag().toString();
					/*	if(rd[i].getTag().toString().equals("Booster Pump System") )
						{
							strsubppoloption = strvalue1;
							strsubppoloptionother = childettxt.getText().toString().trim();
						}
						insertquery(optionvalue,strsubppoloption,strsubppoloptionother);*/
					}
				}				
				
					
			}
			
			
		}
	private void insertquery(String optionvalue,String strsubppoloption,String strsubppoloptionother) {
		// TODO Auto-generated method stub
		
		if(optionvalue.equals("Ladders") || optionvalue.equals("Switch Location") || optionvalue.equals("Material") ||  optionvalue.equals("Booster Pump System") ||
				optionvalue.equals("No Of Pumps Present") || optionvalue.equals("Gas Heater") || optionvalue.equals("Location") ||  optionvalue.equals("Location") || 
				optionvalue.equals("External Sub Panel") || optionvalue.equals("Diving Board") || 
				optionvalue.equals("Skimmer") || optionvalue.equals("Pump Chlorinator")  ||  optionvalue.equals("Pump Types") || optionvalue.equals("Solar Heater") || 
				optionvalue.equals("Remote Control(s)") || optionvalue.equals("Wiring Type") || optionvalue.equals("Slide") ||  
				optionvalue.equals("Main Drain") || optionvalue.equals("Thermostat") || optionvalue.equals("Programmable Controls") || 
				optionvalue.equals("Conduit � Wire Protection") || optionvalue.equals("Valves") || optionvalue.equals("Automated Fill System") ||				
						optionvalue.equals("Booster Pump System") || optionvalue.equals("Water Chemistry Test"))
		{
			 if(getcurrentview.equals("create"))
			 {
				 Cursor c1 = db.hi_db.rawQuery("select * from " + db.CreateSubOptionPools+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_pooloption='"+db.encode(optionvalue)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
				 if(c1.getCount()==0)
				 {
						db.hi_db.execSQL(" INSERT INTO "
							+ db.CreateSubOptionPools
							+ " (fld_inspectorid,fld_module,fld_submodule,fld_pooloption,fld_subpooloption,fld_subpoolother,fld_templatename) VALUES"
							+ "('"+db.UserId+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(optionvalue)+"','"+db.encode(strsubppoloption)+"','"+db.encode(strsubppoloptionother)+"','"+db.encode(gettempname)+"')");
				 }
				 else
				 {	
						db.hi_db.execSQL("UPDATE "
								+ db.CreateSubOptionPools
								+ " SET fld_subpoolother='"+db.encode(strsubppoloptionother)+"',fld_subpooloption='"+db.encode(strsubppoloption)+"'  WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_pooloption='"+ db.encode(optionvalue)+ "'");
				 }
			 }
			 else
			 {
				 Cursor c1 = db.hi_db.rawQuery("select * from " + db.SaveSubOptionPools+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_pooloption='"+db.encode(optionvalue)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
					if(c1.getCount()==0)
					{
						System.out.println(" INSERT INTO "
							+ db.SaveSubOptionPools
							+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_pooloption,fld_subpooloption,fld_subpoolother,fld_templatename) VALUES"
							+ "('"+db.UserId+"','"+getph+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(optionvalue)+"','"+db.encode(strsubppoloption)+"','"+db.encode(strsubppoloptionother)+"','"+db.encode(gettempname)+"')");
						db.hi_db.execSQL(" INSERT INTO "
							+ db.SaveSubOptionPools
							+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_pooloption,fld_subpooloption,fld_subpoolother,fld_templatename) VALUES"
							+ "('"+db.UserId+"','"+getph+"','" + db.encode(getmodulename)+ "','"+db.encode(arrstrname)+"','"+db.encode(optionvalue)+"','"+db.encode(strsubppoloption)+"','"+db.encode(strsubppoloptionother)+"','"+db.encode(gettempname)+"')");
					}
					else
					{	
						System.out.println("UPDATE "
								+ db.SaveSubOptionPools
								+ " SET fld_subpoolother='"+db.encode(strsubppoloptionother)+"',fld_subpooloption='"+db.encode(strsubppoloption)+"'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_pooloption='"+ db.encode(optionvalue)+ "'");
						db.hi_db.execSQL("UPDATE "
								+ db.SaveSubOptionPools
								+ " SET fld_subpoolother='"+db.encode(strsubppoloptionother)+"',fld_subpooloption='"+db.encode(strsubppoloption)+"'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_pooloption='"+ db.encode(optionvalue)+ "'");
					}
			 }
			 

		}
	}
	private void nextscreen()
	{
		cf.DisplayToast("Added successfully");
		back();
	}
	protected void dialogshow() {
		// TODO Auto-generated method stub
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.addnewcustom);
		
		etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
		cf.hidekeyboard(etother);
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=0;
				add_dialog.dismiss();
			}
		});
		
		Button btnsave = (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etother.getText().toString().trim().equals("")) {
					cf.DisplayToast("Please enter the other text");
					
				} else {
					if(str_arrlst.contains(etother.getText().toString().trim().toLowerCase()))
					{
						cf.DisplayToast("Already exists!! Please enter the new text");
						etother.setText("");
						etother.requestFocus();
					}
					else
					{
						try
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.SaveAddOther
									+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
									+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+arrstrname+"','"
									+ db.encode(etother.getText().toString()) + "','"+gettempname+"')");
							
							String addedval = db.encode(etother.getText().toString());
							System.out.println("addedval="+addedval);
							str_arrlst.add(addedval);
							dialog_other=0;
							cf.DisplayToast("Added successfully");
							add_dialog.cancel();
							
							rdlin.removeAllViews();
							dbvalue();
							show();
						
							
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
		
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	private void show() {
		// TODO Auto-generated method stub
		System.out.println("insid eshow");
		try
		{
				if(getcurrentview.equals("start"))
				{
					c = db.hi_db.rawQuery("select * from " + db.SaveSubModule+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
				}
				else
				{
					c = db.hi_db.rawQuery("select * from " + db.CreateDefault+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+db.encode(getmodulename)+"' and fld_submodule='"+db.encode(arrstrname)+"' and fld_templatename='"+gettempname+"'", null);
				}
				
				if(c.getCount()>0)
				 {
					 c.moveToFirst();
					 str = db.decode(c.getString(c.getColumnIndex("fld_description")));			 
				 }
		}catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("arrstrname="+arrstrname);
		if(arrstrname.equals("Main Panel Meter Located Inside")||arrstrname.equals("Main Panel Meter Located Outside") 
				//||  arrstrname.equals("Water Treatment")
				//|| arrstrname.equals("General Comments/Observations")
				|| arrstrname.equals("Fire Separation Present")|| arrstrname.equals("Moisture Stains Present")
				|| arrstrname.equals("Typical Cracks") || arrstrname.equals("Fire Door")
				|| arrstrname.equals("Auto Closure") || arrstrname.equals("Water Meter Flow Detector Stable")
				|| arrstrname.equals("5 Year Wiring Replacement Probability")
				||  arrstrname.equals("Supply Registers Located")
				|| arrstrname.equals("Return Registers Located")|| arrstrname.equals("Safety Glass Satmp at Shower Enclosure")
				|| arrstrname.equals("Spa - Whirlpool Style/Type")|| arrstrname.equals("Water Clarity")|| arrstrname.equals("Pool Shape")
				|| arrstrname.equals("Pool Shell Material"))
			{
			System.out.println("innn="+str_arrlst.size());
				rd = new RadioButton[str_arrlst.size()];
				RadioGroup rg = new RadioGroup(this);
				rg.setOrientation(RadioGroup.VERTICAL);
				for (int i = 0; i < str_arrlst.size(); i++) 
					 {
						 rd[i] = new RadioButton(this);
						 rd[i].setText(str_arrlst.get(i));
						 rd[i].setTag(str_arrlst.get(i));
						 rd[i].setTextColor(Color.parseColor("#000000"));
						 rg.addView(rd[i]);
						 
						 rg.setOnCheckedChangeListener(new CHListener(i));
					 }rdlin.addView(rg);
					
			}
		 else if(arrstrname.equals("Water Chemistry Test"))
		 {
			 
			 poollin = new LinearLayout(this);
			 poollin.setOrientation(LinearLayout.VERTICAL);
			 poollin.removeAllViews();
			 ch = new CheckBox[str_arrlst.size()];
			 wateret = new EditText[str_arrlst.size()];
			 rdlin.removeAllViews();
			 poollin.removeAllViews();			
			 for(int i=0;i<str_arrlst.size();i++)
			 {		
				 ch[i] = new CheckBox(this);
				 ch[i].setText(str_arrlst.get(i));
				 ch[i].setMaxWidth(200);
				 ch[i].setTextColor(Color.parseColor("#000000"));
				 poollin.addView(ch[i]);
				 
				 wateret[i] = new EditText(this);
				 wateret[i].setMaxWidth(200);
				 wateret[i].setTextColor(Color.parseColor("#000000"));
				 wateret[i].setVisibility(v.GONE);
				 poollin.addView(wateret[i]);
				 				
				 
				 ch[i].setOnCheckedChangeListener(new onpoolchecklistner(i));
			 }
			 rdlin.addView(poollin);
		 }
			else
			{		
               
				
			
				
					
				ch = new CheckBox[str_arrlst.size()];System.out.println("str="+str_arrlst.size());
				 for(int i=0;i<str_arrlst.size();i++)
				 {
					 ch[i] = new CheckBox(this);
					 ch[i].setText(str_arrlst.get(i));
					 ch[i].setMaxWidth(300);
					 ch[i].setTextColor(Color.parseColor("#000000"));
					
					 rdlin.addView(ch[i]);
					 
					// if(pp!=1){
					 ch[i].setOnCheckedChangeListener(new CHListener(i));
					 //}
				 }
				 
				 if (str != "") {
						System.out.println("strinside="+str);
							cf.setValuetoCheckbox(ch, str);
					}
			}
	}
	class onpoolchecklistner implements OnCheckedChangeListener{

		public int id;
		public onpoolchecklistner(int i) {
			// TODO Auto-generated constructor stub
			id=i;
		}

		@Override
		public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
			// TODO Auto-generated method stub
			if(arg1)
			{
				wateret[id].setVisibility(v.VISIBLE);
			}
			else
			{
				wateret[id].setVisibility(v.GONE);
			}
		}
		
	}
	
	class CHListener implements OnCheckedChangeListener, android.widget.RadioGroup.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}
		int len=0;String[] name = null;
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			apprdval = buttonView.getText().toString().trim();
			if(isChecked)
			{		
				seltdval="";System.out.println("seltdvaonCheckedChangedl"+seltdval+arrstrname);
				if(ch[clkpod].isChecked())
				{
					seltdval += ch[clkpod].getText().toString() + "&#44;";
				}System.out.println("after"+seltdval);
				System.out.println("apprdval="+apprdval);
				
				 if(apprdval.equals("Other"))
				 {
					 ettxt.setText("");
					 ettxt.setVisibility(v.VISIBLE);
				 }
				 else
				 {
					if(apprdval.equals("Ladders"))
					 {
						System.out.println("insid eladder");
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 name = laddertype; 
						 view(name,lin,childettxt,1);
					 }
					
					 else if(apprdval.equals("Diving Board"))
					 {System.out.println("insid Diving");
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 name = divingboardtype; 
						 view(name,seclin,secchildettxt,2);
					 }
					 else if(apprdval.equals("Slide"))
					 {System.out.println("insid Slide");
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 name = divingboardtype; 
						 view(name,thrlin,thrchildettxt,3);
					 }
					 else if(apprdval.equals("Switch Location"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 name = switchlocation; 
						 view(name,lin,childettxt,1);						 
					 }
					 else if(apprdval.equals("Location") && arrstrname.equals("Pool Equipment"))						 
					 {
						 System.out.println("inside location");
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);System.out.println("test11");
						 lin.setVisibility(v.VISIBLE);
						 name = poollocation;				System.out.println("test22");		 
						 rdview(name,lin,childettxt,1);System.out.println("test33");
					 }
					 else if(apprdval.equals("Material"))
					 {
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 name = material; 
						 view(name,lin,childettxt,1);
					 }
					 else if(apprdval.equals("Skimmer"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 name = skimmer; 
						 view(name,seclin,secchildettxt,2);						 
					 }
					 else if(apprdval.equals("Pump Chlorinator"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 seclin.setVisibility(v.VISIBLE);
						 name = pumpchlor;						 
						 rdview(name,seclin,secchildettxt,2);
					 }
					 else if(apprdval.equals("Main Drain"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 name = maindrain; 
						 view(name,thrlin,thrchildettxt,3);
					 }
					 else if(apprdval.equals("Valves"))
					 {
						 fourthlin.setVisibility(v.VISIBLE);fourthtxt.setVisibility(v.VISIBLE); fourthtxt.setText(apprdval);
						 name = valves; 
						 view(name,fourlin,fourchildettxt,4);
					 }
					 else if(apprdval.equals("Automated Fill System"))
					 {
						 fifthlin.setVisibility(v.VISIBLE); fifthtxt.setVisibility(v.VISIBLE); fifthtxt.setText(apprdval);
						 name = automated; 
						 view(name,fivelin,fivechildettxt,5);
					 }
					 else if(apprdval.equals("No Of Pumps Present"))
					 {
						 System.out.println("inside no of pump");
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);//pp=1;
						 name = pumppresent;						 
						 rdview(name,lin,childettxt,1);
					 }
					 else if(apprdval.equals("Pump Types"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 name = pumptypes; 
						 view(name,seclin,secchildettxt,2);
						 
					 }
					
					 else if(apprdval.equals("Gas Heater"))
					 {						
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 name = gasheater; 
						 view(name,lin,childettxt,1);
					 }
					 else if(apprdval.equals("Solar Heater"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 name = solarheater; 
						 view(name,seclin,secchildettxt,2);
					 }
					 else if(apprdval.equals("Thermostat"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 name = thermostat; 
						 view(name,thrlin,thrchildettxt,3);
					 }
					 else if(apprdval.equals("Location"))
					 {
						 System.out.println("insis second location");
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 lin.setVisibility(v.VISIBLE);
						 name = timerlocation;						 
						 rdview(name,lin,childettxt,1);
					 }
					 else if(apprdval.equals("Remote Control(s)"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 seclin.setVisibility(v.VISIBLE);
						 name = remotecontrols;						 
						 rdview(name,seclin,secchildettxt,2);
					 }
					 else if(apprdval.equals("Programmable Controls"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 thrlin.setVisibility(v.VISIBLE);
						 name = progcontrols;						 
						 rdview(name,thrlin,thrchildettxt,3);
					 }
					 else if(apprdval.equals("Booster Pump System"))
					 {						
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 name = extsubpanel; 
						 view(name,lin,childettxt,1);
					 }
					 else if(apprdval.equals("External Sub Panel"))
					 {						
						 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(apprdval);
						 name = extsubpanel; 
						 view(name,lin,childettxt,1);
					 }
					 else if(apprdval.equals("Wiring Type"))
					 {
						 secondlin.setVisibility(v.VISIBLE);secondtxt.setText(apprdval);
						 name = wiringtype; 
						 view(name,seclin,secchildettxt,2);
					 }
					 else if(apprdval.equals("Conduit � Wire Protection"))
					 {
						 thirdlin.setVisibility(v.VISIBLE);thirdtxt.setText(apprdval);
						 name = conduit; 
						 view(name,thrlin,thrchildettxt,3);
					 }
					
				 }				
			}
			 else  
			 {
				
				 System.out.println("insideesle ="+apprdval);
				 
				 if(apprdval.equals("Other"))
				 {
				     ettxt.setVisibility(v.GONE);
				 }
				 else
				 {
				    if(apprdval.equals("Ladders")|| apprdval.equals("Switch Location") || apprdval.equals("Location")|| apprdval.equals("Material") || apprdval.equals("Booster Pump System")
				    		|| apprdval.equals("No Of Pumps Present")|| apprdval.equals("Gas Heater") || (apprdval.equals("Location") && arrstrname.equals("Timers/Clocks/Controls"))
				    		|| apprdval.equals("External Sub Panel"))
				     {
				    	firstlin.setVisibility(v.GONE); childettxt.setVisibility(v.GONE);
				     }
				     else if(apprdval.equals("Diving Board")|| apprdval.equals("Skimmer") || apprdval.equals("Pump Chlorinator") || apprdval.equals("Pump Types")|| apprdval.equals("Solar Heater")
				    		 || apprdval.equals("Remote Control(s)")||apprdval.equals("Wiring Type"))
				     {
					    secondlin.setVisibility(v.GONE);secchildettxt.setVisibility(v.GONE);
				     }
				     else if(apprdval.equals("Slide") || apprdval.equals("Main Drain")||apprdval.equals("Thermostat") || apprdval.equals("Programmable Controls")
				    		 || apprdval.equals("Conduit � Wire Protection"))
				     {
					    thirdlin.setVisibility(v.GONE);thrchildettxt.setVisibility(v.GONE);
				     }
				     else if(apprdval.equals("Valves"))
				     {
				    	 fourthlin.setVisibility(v.GONE);fourthtxt.setVisibility(v.GONE);
				     }
				     else if(apprdval.equals("Automated Fill System"))
				     {
				    	  fifthlin.setVisibility(v.GONE);fifthtxt.setVisibility(v.GONE);
				     }
				 }
			 }
			
		}
		 public void onCheckedChanged(RadioGroup group, int checkedId) {
			 String getselected = (String) findViewById(checkedId).getTag();
			 System.out.println("getsle"+getselected);
			 if(getselected.equals("Other"))
				{
					if(apprdval.equals("Skimmer") || apprdval.equals("Pump Chlorinator") || apprdval.equals("Pump Types"))
					{
						secchildettxt.setVisibility(v.VISIBLE);
					}
					else if(apprdval.equals("Main Drain"))
					{
						thrchildettxt.setVisibility(v.VISIBLE);
					}
					else if(apprdval.equals("Valves"))
					{
						fourchildettxt.setVisibility(v.VISIBLE);
					}
					else if(apprdval.equals("Automated Fill System"))
					{
						fivechildettxt.setVisibility(v.VISIBLE);
					}
					else if(arrstrname.equals("Pool Cleaning Equipment") || arrstrname.equals("Water Treatment")
							 || arrstrname.equals("General Comments/Observations"))
						
						
					{
						ettxt.setVisibility(v.VISIBLE);
						firstlin.setVisibility(v.GONE);childettxt.setVisibility(v.VISIBLE);
					}
					else if(apprdval.equals("Wiring Type"))
					{
						secchildettxt.setVisibility(v.VISIBLE);
						
					}
					else if(apprdval.equals("Conduit � Wire Protection"))
					{
						thrchildettxt.setVisibility(v.VISIBLE);
						
					}
					else
					{
					    childettxt.setVisibility(v.VISIBLE);
					}
				}
				else
				{System.out.println("arrstrname="+arrstrname);
					if(apprdval.equals("Skimmer") || apprdval.equals("Pump Types"))
					{
						secchildettxt.setText("");
						secchildettxt.setVisibility(v.GONE);
					}
					else if(apprdval.equals("Main Drain"))
					{
						thrchildettxt.setText("");
						thrchildettxt.setVisibility(v.GONE);
					}
					else if(apprdval.equals("Valves"))
					{
						fourchildettxt.setText("");
						fourchildettxt.setVisibility(v.GONE);
					}
					else if(apprdval.equals("Automated Fill System"))
					{
						fivechildettxt.setText("");
						fivechildettxt.setVisibility(v.GONE);
					}
					else if( arrstrname.equals("Water Treatment")
							 || arrstrname.equals("General Comments/Observations"))
					{
						ettxt.setVisibility(v.GONE);ettxt.setText("");
						firstlin.setVisibility(v.GONE);childettxt.setVisibility(v.GONE);
					}
					/*else if(arrstrname.equals("Pool Cleaning Equipment") || arrstrname.equals("Water Treatment")
							 || arrstrname.equals("General Comments/Observations"))
					{
						System.out.println("arrstat="+getselected);
						if(getselected.equals("Booster Pump System"))
						{
							 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(getselected);
							 name = boosterpumpsystem;
							 view(name,lin,childettxt,1);
						}
						else if(getselected.equals("Pump Chlorinator"))
						{
							 System.out.println("Chlorinator");
							 firstlin.setVisibility(v.VISIBLE);firsttxt.setText(getselected);
							 name = pumpchlor;
							 view(name,lin,childettxt,1);
						}
						else
						{
							 firstlin.setVisibility(v.GONE);childettxt.setVisibility(v.GONE);
						}
						ettxt.setText("");
						ettxt.setVisibility(v.GONE);
					}*/
					else if(arrstrname.equals("Timers/Clocks/Controls"))
					{
						if(apprdval.equals("Location"))
						{
							childettxt.setText("");
							childettxt.setVisibility(v.GONE);
						}
					}
					else if(arrstrname.equals("Pool Cleaning Equipment"))
					{
						if(apprdval.equals("Booster Pump System"))
						{
							childettxt.setText("");
							childettxt.setVisibility(v.GONE);
						}
						
					}
					else if(arrstrname.equals("Electrical Service"))
					{
						if(apprdval.equals("External Sub Panel"))
						{
							childettxt.setText("");
							childettxt.setVisibility(v.GONE);
						}
						else if(apprdval.equals("Wiring Type"))
						{
							secchildettxt.setText("");
							secchildettxt.setVisibility(v.GONE);
						}
						else 
						{
							thirdtxt.setText("");
							thirdtxt.setVisibility(v.GONE);
						}
					}
					else if(arrstrname.equals("Water Heating Systems"))
					{
						if(apprdval.equals("Gas Heater"))
						{
							childettxt.setText("");
							childettxt.setVisibility(v.GONE);
						}
						else if(apprdval.equals("Solar Heater"))
						{
							secchildettxt.setText("");
							secchildettxt.setVisibility(v.GONE);
						}
						else if(apprdval.equals("Thermostat"))
						{
							thirdtxt.setText("");
							thirdtxt.setVisibility(v.GONE);
						}
					}
					else
					{
						childettxt.setText("");
					    childettxt.setVisibility(v.GONE);
					}
					
					
				}
		 }
	}
	protected void view(String[] name, LinearLayout lin2, final EditText secchildettxt2,int m) {
		// TODO Auto-generated method stub
		String[] id = name;
		int k=0;
		String subvalue="",str1,str2,str3;
		iden=1;System.out.println("inside view"+id.length);
		if(m==1)
		{
			childch1= new CheckBox[id.length];
		}
		else if(m==2)
		{
			childch2= new CheckBox[id.length];
		}
		else if(m==3)
		{
			childch3= new CheckBox[id.length];
		}
		else if(m==4)
		{
			childch4= new CheckBox[id.length];
		}
		else if(m==5)
		{
			childch5= new CheckBox[id.length];
		}
		System.out.println("lin2="+lin2.getVisibility());
		lin2.removeAllViews();
		
		 for(i=0;i<id.length;i++)
		 {
			 if(m==1)
			 {
				 childch1[i] = new CheckBox(LoadPoolShowDialog.this);
				 childch1[i].setText(id[i]);
				 childch1[i].setTag(id[i]);
				 childch1[i].setTextColor(Color.parseColor("#000000"));
				 
				 lin2.addView(childch1[i]);			 
				
				 childch1[i].setOnClickListener(new Subpoolclick(i,m,secchildettxt2));
			 }
			 else   if(m==2)
			 {
				 childch2[i] = new CheckBox(LoadPoolShowDialog.this);
				 childch2[i].setText(id[i]);
				 childch2[i].setTag(id[i]);
				 childch2[i].setTextColor(Color.parseColor("#000000"));
				 
				 lin2.addView(childch2[i]);			 
				
				 childch2[i].setOnClickListener(new Subpoolclick(i,m,secchildettxt2));
			 }
			 else if(m==3)
			 {
				 childch3[i] = new CheckBox(LoadPoolShowDialog.this);
				 childch3[i].setText(id[i]);
				 childch3[i].setTag(id[i]);
				 childch3[i].setTextColor(Color.parseColor("#000000"));
				 
				 lin2.addView(childch3[i]);			 
				
				 childch3[i].setOnClickListener(new Subpoolclick(i,m,secchildettxt2));
			 }
			 else if(m==4)
			 {
				 childch4[i] = new CheckBox(LoadPoolShowDialog.this);
				 childch4[i].setText(id[i]);
				 childch4[i].setTag(id[i]);
				 childch4[i].setTextColor(Color.parseColor("#000000"));
				 
				 lin2.addView(childch4[i]);			 
				
				 childch4[i].setOnClickListener(new Subpoolclick(i,m,secchildettxt2));
			 }
			 else if(m==5)
			 {
				 childch5[i] = new CheckBox(LoadPoolShowDialog.this);
				 childch5[i].setText(id[i]);
				 childch5[i].setTag(id[i]);
				 childch5[i].setTextColor(Color.parseColor("#000000"));
				 
				 lin2.addView(childch5[i]);			 
				
				 childch5[i].setOnClickListener(new Subpoolclick(i,m,secchildettxt2));
			 }
		 }
	}
	
	class Subpoolclick implements OnClickListener
	{
		int subid;EditText edsub;  
		Subpoolclick(int i,int l,EditText ed)
		{
			subid=l;System.out.println("subid="+subid);
			edsub=ed;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final String id=v.getTag().toString();
			if(((CheckBox) v).isChecked())
			{
				 
				    if(id.equals("Other"))
					{
				    	edsub.setVisibility(v.VISIBLE);
					}
			}
			else
			{
				
					if(id.equals("Other"))
					{
						edsub.setText("");
						edsub.setVisibility(v.GONE);
					}
				
			}
			
		}		
	}
	protected void rdview(String[] name, LinearLayout lin2, EditText childettxt2,int m) {
		// TODO Auto-generated method stub
		iden=2;
		RadioGroup rdgswtch = new RadioGroup(LoadPoolShowDialog.this);
		if(m==1)
		{
			 rds1 = new RadioButton[name.length];
		}
		else if(m==2)
		{
			 rds2 = new RadioButton[name.length];
		}
		else if(m==3)
		{
			 rds3 = new RadioButton[name.length];
		}
		else if(m==4)
		{
			 rds4 = new RadioButton[name.length];
		}
		else if(m==5)
		{
			 rds5 = new RadioButton[name.length];
		}
		//System.out.println("rds1"+rds1.length);
		 lin2.removeAllViews();
		 for(int i=0;i<name.length;i++)
		 {
			 
			 if(m==1)
			 {
				 rds1[i] = new RadioButton(LoadPoolShowDialog.this);
				 rds1[i].setText(name[i]);
				 rds1[i].setTag(name[i]);
				 rds1[i].setTextColor(Color.parseColor("#000000"));
				 
				 rdgswtch.addView(rds1[i]);System.out.println("dfdfd");
				 rds1[i].setOnClickListener(new RDclick(i,childettxt2,m));System.out.println("childettxt2");
			 }
			 else   if(m==2)
			 {
				 rds2[i] = new RadioButton(LoadPoolShowDialog.this);
				 rds2[i].setText(name[i]);
				 rds2[i].setTag(name[i]);
				 rds2[i].setTextColor(Color.parseColor("#000000"));
				 
				 rdgswtch.addView(rds2[i]);
				 rds2[i].setOnClickListener(new RDclick(i,childettxt2,m));
			 }
			 else   if(m==3)
			 {
				 System.out.println("sdhdjhfjdfhjk");
				 rds3[i] = new RadioButton(LoadPoolShowDialog.this);
				 rds3[i].setText(name[i]);
				 rds3[i].setTag(name[i]);
				 rds3[i].setTextColor(Color.parseColor("#000000")); System.out.println("ggg");
				 
				 rdgswtch.addView(rds3[i]);
				 rds3[i].setOnClickListener(new RDclick(i,childettxt2,m)); System.out.println("sdfdsf");
			 }
			 
			 else   if(m==4)
			 {
				 rds4[i] = new RadioButton(LoadPoolShowDialog.this);
				 rds4[i].setText(name[i]);
				 rds4[i].setTag(name[i]);
				 rds4[i].setTextColor(Color.parseColor("#000000"));
				 
				 rdgswtch.addView(rds4[i]);
				 rds4[i].setOnClickListener(new RDclick(i,childettxt2,m));
			 }
			 
			 else   if(m==5)
			 {
				 rds5[i] = new RadioButton(LoadPoolShowDialog.this);
				 rds5[i].setText(name[i]);
				 rds5[i].setTag(name[i]);
				 rds5[i].setTextColor(Color.parseColor("#000000"));
				 
				 rdgswtch.addView(rds5[i]);
				 rds5[i].setOnClickListener(new RDclick(i,childettxt2,m));
			 }
		 }System.out.println("lin2");
		 lin2.addView(rdgswtch);System.out.println("rdgswtch");
	}
	class RDclick implements OnClickListener
	{
		int subid;EditText edsub;  
		RDclick(int i,EditText ed,int s)
		{
			subid=s;
			edsub=ed;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final String id=v.getTag().toString();
			if(((RadioButton) v).isChecked())
			{
				if(id.equals("Other"))
				{
			    	edsub.setVisibility(v.VISIBLE);
				}
			    else
			    {
			    	edsub.setVisibility(v.GONE);
			    }
			}
			else
			{
					if(id.equals("Other"))
					{
						edsub.setText("");
						edsub.setVisibility(v.GONE);
					}
			}
			
		}		
	}
	public void onSaveInstanceState(Bundle outState) {
		 
        outState.putInt("other_index", dialog_other);
        if(dialog_other==1)
        {
          outState.putString("othertext_index", etother.getText().toString());
        }
        outState.putString("checkbox_index", seltdval);
        
        selectedval="";
       	for (int i = 0; i < str_arrlst.size(); i++) {
   			if (ch[i].isChecked()) {
   				selectedval += ch[i].getText().toString() + "&#44;";
   			}
   		}
       	outState.putString("alertcheck_value", selectedval); 
       	outState.putInt("identity", iden); 
       	
        if(firstlin.getVisibility()==View.VISIBLE)
	 	{	
        	if(iden==1)
        	{
        		for (int i = 0; i < childch1.length; i++) {
    	   			if (childch1[i].isChecked()) {
    	   				strvalue1 += childch1[i].getText().toString() + "&#44;";
    	   			}
    	   		}
        	}
        	else if(iden==2)
        	{
        		for (int i = 0; i < rds1.length; i++) {
    	   			if (rds1[i].isChecked()) {
    	   				strvalue1 = rds1[i].getText().toString();
    	   			}
    	   		}
        	}
	 	}
        
        if(secondlin.getVisibility()==View.VISIBLE)
	 	{	
        	if(iden==1)
        	{
        		for (int i = 0; i < childch2.length; i++) {
    	   			if (childch2[i].isChecked()) {
    	   				strvalue2 += childch2[i].getText().toString() + "&#44;";
    	   			}
    	   		}
        	}
        	else if(iden==2)
        	{
        		for (int i = 0; i < rds2.length; i++) {
    	   			if (rds2[i].isChecked()) {
    	   				strvalue2 = rds2[i].getText().toString();
    	   			}
    	   		}
        	}
	 	}
        
        
        if(thirdlin.getVisibility()==View.VISIBLE)
	 	{	
        	if(iden==1)
        	{
        		for (int i = 0; i < childch3.length; i++) {
    	   			if (childch3[i].isChecked()) {
    	   				strvalue3 += childch3[i].getText().toString() + "&#44;";
    	   			}
    	   		}
        	}
        	else if(iden==2)
        	{
        		for (int i = 0; i < rds3.length; i++) {
    	   			if (rds3[i].isChecked()) {
    	   				strvalue3 = rds3[i].getText().toString();
    	   			}
    	   		}
        	}
	 	}
        
        
        if(fourthlin.getVisibility()==View.VISIBLE)
	 	{	
        	if(iden==1)
        	{
        		for (int i = 0; i < childch4.length; i++) {
    	   			if (childch4[i].isChecked()) {
    	   				strvalue4 += childch4[i].getText().toString() + "&#44;";
    	   			}
    	   		}
        	}
        	else if(iden==2)
        	{
        		for (int i = 0; i < rds4.length; i++) {
    	   			if (rds4[i].isChecked()) {
    	   				strvalue4 = rds4[i].getText().toString();
    	   			}
    	   		}
        	}
	 	}
        
        if(fifthlin.getVisibility()==View.VISIBLE)
	 	{	
        	if(iden==1)
        	{
        		for (int i = 0; i < childch5.length; i++) {
    	   			if (childch5[i].isChecked()) {
    	   				strvalue5 += childch5[i].getText().toString() + "&#44;";
    	   			}
    	   		}
        	}
        	else if(iden==2)
        	{
        		for (int i = 0; i < rds5.length; i++) {
    	   			if (rds5[i].isChecked()) {
    	   				strvalue5 = rds5[i].getText().toString();
    	   			}
    	   		}
        	}
	 	}
        outState.putString("firstvalue", strvalue1);
        outState.putString("secondvalue", strvalue2);
        outState.putString("thirdvalue", strvalue3);
        outState.putString("fourthvalue", strvalue4);
        outState.putString("fifthvalue", strvalue5);
        
        outState.putString("firstothervalue", ettxt.getText().toString());
        outState.putString("secondothervalue", secchildettxt.getText().toString());
        outState.putString("thirdothervalue", thrchildettxt.getText().toString());
        outState.putString("fourthothervalue", fourchildettxt.getText().toString());
        outState.putString("fifthothrevalue", fivechildettxt.getText().toString());
       /* outState.putInt("ppval", pp);
        outState.putString("app", apprdval);System.out.println("save="+apprdval);*/
        
        super.onSaveInstanceState(outState);
    }
   protected void onRestoreInstanceState(Bundle savedInstanceState) {
	   dialog_other = savedInstanceState.getInt("other_index");
	   if(dialog_other==1)
	   {
		   dialogshow();
		   strother = savedInstanceState.getString("othertext_index");
		   etother.setText(strother);
		 
	   }
	   seltdval = savedInstanceState.getString("alertcheck_value");
	   if(!seltdval.equals(""))
       {				
    	  cf.setValuetoCheckbox(ch, seltdval);
       }
	   
	   
	   if(firstlin.getVisibility()==View.VISIBLE)
	   {
		   strvalue1 = savedInstanceState.getString("firstvalue");
		   strpoolother= savedInstanceState.getString("firstothervalue");
		 
		   if(!strvalue1.equals(""))
		   {			
			   if(savedInstanceState.getInt("identity")==1)
			   {
				   cf.setValuetoCheckbox(childch1, strvalue1);				   
			   }
			   else if(savedInstanceState.getInt("identity")==2)
			   {
				   cf.setValuetoRadio(rds1, strvalue1);
       			 
			   }
			   SetOther(strvalue1,childettxt,strpoolother);
		   }
		   
		}
	   
	   if(secondlin.getVisibility()==View.VISIBLE)
	   {System.out.println("onrestoresecond"+savedInstanceState.getInt("identity"));
		   strvalue2 = savedInstanceState.getString("secondvalue");
		   strpoolother= savedInstanceState.getString("secondothervalue");
		 
		   if(!strvalue2.equals(""))
		   {			
			   if(savedInstanceState.getInt("identity")==1)
			   {
				   cf.setValuetoCheckbox(childch2, strvalue2);
			   }
			   else if(savedInstanceState.getInt("identity")==2)
			   {
				   cf.setValuetoRadio(rds2, strvalue2);
			   }
			   SetOther(strvalue2,secchildettxt,strpoolother);
		   }
		   
		}
	   
	   
	   if(thirdlin.getVisibility()==View.VISIBLE)
	   {
		   strvalue3 = savedInstanceState.getString("thirdvalue");
		   strpoolother= savedInstanceState.getString("thirdothervalue");
		 
		   if(!strvalue3.equals(""))
		   {			
			   if(savedInstanceState.getInt("identity")==1)
			   {
				   cf.setValuetoCheckbox(childch3, strvalue3);
				   
			   }
			   else if(savedInstanceState.getInt("identity")==2)
			   {
				   cf.setValuetoRadio(rds3, strvalue3);
			   }
			   SetOther(strvalue3,thrchildettxt,strpoolother);
		   }
		   
		}
	   
	   if(fourthlin.getVisibility()==View.VISIBLE)
	   {
		   strvalue4 = savedInstanceState.getString("fourthvalue");
		   strpoolother= savedInstanceState.getString("fourthothervalue");
		 
		   if(!strvalue4.equals(""))
		   {			
			   if(savedInstanceState.getInt("identity")==1)
			   {
				   cf.setValuetoCheckbox(childch4, strvalue4);				  
			   }
			   else if(savedInstanceState.getInt("identity")==2)
			   {
				   cf.setValuetoRadio(rds4, strvalue4);
			   }
			   SetOther(strvalue4,fourchildettxt,strpoolother);
		   }
		   
		}
	   
	   if(fifthlin.getVisibility()==View.VISIBLE)
	   {
		   strvalue5 = savedInstanceState.getString("fifthvalue");
		   strpoolother= savedInstanceState.getString("fifthothrevalue");
		 
		   if(!strvalue5.equals(""))
		   {			
			   if(savedInstanceState.getInt("identity")==1)
			   {
				   cf.setValuetoCheckbox(childch5, strvalue5);				   
			   }
			   else if(savedInstanceState.getInt("identity")==2)
			   {
				   cf.setValuetoRadio(rds5, strvalue5);
			   }
			   SetOther(strvalue5,fivechildettxt,strpoolother);
		   }
		   
		}
			
	 //  apprdval = savedInstanceState.getString("app");
      //  System.out.println("restpre="+apprdval);								
	   super.onRestoreInstanceState(savedInstanceState);
    }
	private void back() {
		// TODO Auto-generated method stub
		Intent i = new Intent(LoadPoolShowDialog.this,LoadSubModules.class);
		System.out.println("gettempname="+gettempname+"getmodulename="+getmodulename+"getph="+getph+"getid="+getid+"getcurrentview"+getcurrentview);
		i.putExtra("currentview", getcurrentview);
		i.putExtra("templatename", gettempname);
		i.putExtra("modulename", getmodulename);
		i.putExtra("selectedid", getph);
		i.putExtra("tempoption", gettempoption);
		i.putExtra("id",getid);
		//startActivity(i);
		setResult(101,i);
		finish();	
	}

}