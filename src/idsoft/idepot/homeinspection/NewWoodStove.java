package idsoft.idepot.homeinspection;

import java.lang.reflect.Array;
import java.util.ArrayList;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class NewWoodStove extends Activity {
	String gettempname="",getcurrentview="",getmodulename="",gettempoption="",getselectedho="",watervalue="",getspinselectedname="",getnoofwoodstoves="",
			finalselstrval="",fluetypevalue="",tempfluetype="",noofwoodstovesval="",seltdval="",str="";
	Spinner spinloadtemplate,sp_noofwoodstoves;
	LinearLayout lin;
	CheckBox ch[];
	View v;
	int id=0,k=0,spintouch=0,chk=0;
	String arrtempname[];
	String tempname="",woodstovenavalue="0";
	ArrayAdapter adapter;
	DatabaseFunction db;
	Webservice_config wb;
	EditText ed_othernoowoodstoves;
	CommonFunction cf;
	Button btntakephoto;
	CheckBox chknotapplicable;
	ArrayAdapter woodstovearrayadap;
	TextView tvfluetype,tvfluetypesel;
	String arrnoofwoodstoves[] = {"--Select--","1","2","3","4","5","None","Other"};
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	String fueltyprearr[] = {"Unlined Masonry Chimney","Old Masonry Chimney Redundant","PVC Schedule 40","No Flue Cap","Metal Flue Cap","Flexible Metal Gas Flue","Masonry Chimney","Clay Flue Liner","Metal Flue"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}		
		
		setContentView(R.layout.activity_newwoodstove);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(20);
		db.CreateTable(21);
		db.CreateTable(22);
		db.CreateTable(23);
		db.CreateTable(24);
		db.CreateTable(25);
		db.CreateTable(38);
		db.CreateTable(39);
		db.CreateTable(40);
		db.CreateTable(42);
		db.CreateTable(43);
		db.CreateTable(44);
		db.userid();
		btntakephoto = (Button)findViewById(R.id.btn_takephoto);
btntakephoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						    Intent intent = new Intent(NewWoodStove.this, PhotoDialog.class);
							Bundle b1 = new Bundle();
							b1.putString("templatename", gettempname);			
							intent.putExtras(b1);
							Bundle b2 = new Bundle();
							b2.putString("modulename", getmodulename);			
							intent.putExtras(b2);
							Bundle b4 = new Bundle();
							b4.putString("currentview","takephoto");			
							intent.putExtras(b4);
							Bundle b5 = new Bundle();
							b5.putString("selectedid", getselectedho);			
							intent.putExtras(b5);
							startActivity(intent);
							finish();
			}
		});
		
		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/squadaone-regular.ttf"); 
		btnsubmit.setTypeface(font); 
		
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);
									
				}  
			}
		});
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		
		chknotapplicable = (CheckBox) findViewById(R.id.hvacnotappl);		 
		chknotapplicable.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
	                //is chkIos checked?
			if (((CheckBox) v).isChecked())
			{
				chk=1;
				System.out.println("its checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE);
				
			}
			else
			{
				chk=2;
				System.out.println("its not checked");
				((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
				
				
			}
	 
		  }
		});
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC", null);
            System.out.println("cur="+cur.getCount()+"select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' and (fld_modulename='"+db.encode(getmodulename)+"' or fld_tempoption='Entire Report')  order by fld_date,fld_time DESC");
			if(cur.getCount()>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}           	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("tempname"+tempname);
		btnsubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(getcurrentview.equals("start"))
				{
					cf.showsubmitalert(getselectedho,getcurrentview,getmodulename,gettempname);
				}
				else
				{
					cf.HVACexport(getselectedho,getcurrentview,getmodulename,gettempname);				
				}  
			}
		});
		
		
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		
		spinloadtemplate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				spintouch=1;
				return false;
			}
		});
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		
		tvfluetypesel = (TextView)findViewById(R.id.fluetypesel);
		
		TextView tvfluetype = (TextView)findViewById(R.id.fluetype);
		tvfluetype.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				watervalue="Flue Type";k=1;
				multi_select(watervalue,k);		
			}
		});
		
		
		((TextView)findViewById(R.id.modulename)).setVisibility(v.VISIBLE);
		((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		
		Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(NewWoodStove.this,HomeScreen.class));
					finish();
					 
				}
			});
		
		/*TextView vc = ((TextView)findViewById(R.id.vc));
		vc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent = new Intent(NewWoodStove.this, NewSetVC.class);
					Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", getcurrentview);			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("submodulename", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("tempoption", gettempoption);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
			}
		});*/
		
		
		Button	save = (Button)findViewById(R.id.btnsave);
		save.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					try
					{
						if(chknotapplicable.isChecked()==true)
						{
							chk=1;
						}
						else
						{
							chk=2;
						}
						if(getcurrentview.equals("start"))
						 {
									Cursor cur = db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"'  and fld_srid='"+getselectedho+"'", null);
									if(cur.getCount()==0)
									{
										db.hi_db.execSQL(" INSERT INTO "+db.LoadHotwatersystem+" (fld_templatename,fld_inspectorid,fld_srid,fld_noofwoodstoves,fld_fluetype,fld_woodstoveNA) VALUES" +
																					    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getselectedho+"','"+db.encode(getnoofwoodstoves)+"','"+db.encode(tempfluetype)+"','"+chk+"')");
										
									}
									else
									{
										System.out.println("UPDATE  "+db.LoadHotwatersystem+" SET  fld_noofwoodstoves='"+db.encode(getnoofwoodstoves)+"',fld_woodstoveNA='"+chk+"'," +
							   					"fld_fluetype='"+db.encode(tempfluetype)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
										db.hi_db.execSQL("UPDATE  "+db.LoadHotwatersystem+" SET  fld_noofwoodstoves='"+db.encode(getnoofwoodstoves)+"',fld_woodstoveNA='"+chk+"'," +
							   					"fld_fluetype='"+db.encode(tempfluetype)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
				
									}	
									cf.DisplayToast("Wood Stove saved successfully");
						 }
						 else
						 {
							
									try
									{
											Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
											if(cur.getCount()==0)
											{
												System.out
														.println(" INSERT INTO "+db.CreateHotwatersystem+" (fld_templatename,fld_inspectorid,fld_noofwoodstoves,fld_fluetype,fld_woodstoveNA) VALUES" +
													    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+db.encode(getnoofwoodstoves)+"','"+db.encode(tempfluetype)+"')");
												db.hi_db.execSQL(" INSERT INTO "+db.CreateHotwatersystem+" (fld_templatename,fld_inspectorid,fld_noofwoodstoves,fld_fluetype,fld_woodstoveNA) VALUES" +
													    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+db.encode(getnoofwoodstoves)+"','"+db.encode(tempfluetype)+"','"+chk+"')");
												
											}
											else
											{
												db.hi_db.execSQL("UPDATE  "+db.CreateHotwatersystem+" SET  fld_noofwoodstoves='"+db.encode(getnoofwoodstoves)+"',fld_woodstoveNA='"+chk+"'," +
									   					"fld_fluetype='"+db.encode(tempfluetype)+"'" +" Where fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
						
											}
											cf.DisplayToast("Wood Stove saved successfully");
									}
									catch (Exception e) {
										// TODO: handle exception
										 System.out.println("insi teeeee"+e.getMessage());
									}
								
						 }
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("sasas"+e.getMessage());
					}

					cf.DisplayToast("Wood Stove saved successfully");
				}
			});

		
		sp_noofwoodstoves = (Spinner)findViewById(R.id.spin_noofwoodstoves);
		woodstovearrayadap = new ArrayAdapter<String>(NewWoodStove.this,android.R.layout.simple_spinner_item,arrnoofwoodstoves);
		woodstovearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_noofwoodstoves.setAdapter(woodstovearrayadap);
		sp_noofwoodstoves.setOnItemSelectedListener(new myspinselectedlistener(1));
		ed_othernoowoodstoves= (EditText)findViewById(R.id.other_woodstove);
		
		tvfluetype = (TextView)findViewById(R.id.fluetype);
		tvfluetype = (TextView)findViewById(R.id.fluetypesel);
		displaywoodstove();
		
		LinearLayout menu = (LinearLayout) findViewById(R.id.hvacmenu);
		menu.addView(new Header_Menu(this, 7, cf,gettempname,getcurrentview,getmodulename,gettempoption,getselectedho,db));
		
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(final AdapterView<?> parent,
			        View view, final int pos, long id) {	
	    	
	    	if(spintouch==1)
	    	{
		    	AlertDialog.Builder b =new AlertDialog.Builder(NewWoodStove.this);
				b.setTitle("Confirmation");
				b.setMessage("Selected Template Data Points will Overwrite. Are you sure, Do you want to Overwrite? ");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						gettempname = parent.getItemAtPosition(pos).toString();
						
						cf.DefaultValue_Insert(gettempname,getselectedho);
						
						spintouch=0;
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if(gettempname.equals("N/A"))
						{
							spinloadtemplate.setSelection(0);
						}
						else
						{
							int spinnerPosition = adapter.getPosition(gettempname);
			        		spinloadtemplate.setSelection(spinnerPosition);
						}
						spintouch=0;
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	class myspinselectedlistener implements OnItemSelectedListener
	{
         int var=0;
        
		public myspinselectedlistener(int i) {
			// TODO Auto-generated constructor stub
			var=i;
			 getspinselectedname="";
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			getspinselectedname = arg0.getItemAtPosition(arg2).toString();
			switch(var)
			{
				case 1:
					 getnoofwoodstoves = getspinselectedname;
					
				break;
				
			
			}
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
			
			
		}
	
	private void displaywoodstove() {
		// TODO Auto-generated method stub
		String radiators="",characteristics="";
		Cursor cur;
		if(getcurrentview.equals("start"))
		{
			 cur= db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);	
		}
		else
		{
			 cur= db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
		}
		
		System.out.println("sdf"+cur.getCount());
		if(cur.getCount()>0)
		 {
			 cur.moveToFirst();
			    try
				{
			    	noofwoodstovesval=db.decode(cur.getString(cur.getColumnIndex("fld_noofwoodstoves")));
			    	fluetypevalue=db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));
			    	woodstovenavalue = cur.getString(cur.getColumnIndex("fld_woodstoveNA"));System.out.println("tes="+woodstovenavalue);
			    	sp_noofwoodstoves.setSelection(woodstovearrayadap.getPosition(noofwoodstovesval));
			    	
			    	
			    	if(!fluetypevalue.equals(""))
			    	{
			    		tvfluetypesel.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+fluetypevalue));
			    		tvfluetypesel.setVisibility(v.VISIBLE);
			    	}
			    
			    	
			    	if(woodstovenavalue.equals("1"))
					{
						chknotapplicable.setChecked(true);
						((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.INVISIBLE); 		
					}
					else
					{
						chknotapplicable.setChecked(false);
						((LinearLayout)findViewById(R.id.hvaclin)).setVisibility(v.VISIBLE);
					}
				}
			 	catch (Exception e) {
					// TODO: handle exception
				}
		}
	}

	private void multi_select(final String watervalue, int s) {
		// TODO Auto-generated method stub
		id=1;
		dbvalue(watervalue);
		final Dialog add_dialog = new Dialog(NewWoodStove.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);

		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);
		
		showoptions();
		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(watervalue);
				
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				// TODO Auto-generated method stub
				seltdval = "";
				for (int i = 0; i < str_arrlst.size(); i++) {
					if (ch[i].isChecked()) {
						seltdval += ch[i].getText().toString() + "&#44;";
					}
				}
				
				finalselstrval = "";
				finalselstrval = seltdval;
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{	
					System.out.println("WW="+watervalue+"fluetypevalue"+fluetypevalue);
					if(watervalue.equals("Flue Type"))
					{
						fluetypevalue=finalselstrval;
						tempfluetype = fluetypevalue;System.out.println("tempfluetype"+tempfluetype);
						tempfluetype = cf.replacelastendswithcomma(tempfluetype);
						tvfluetypesel.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempfluetype));
						tvfluetypesel.setVisibility(v.VISIBLE);
					}
					add_dialog.cancel();
					id=0;	
				}	
			}
		});
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    id=0;
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);		
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		
				
			}
		});
		
		add_dialog.setCancelable(false);
		add_dialog.show();
		setvalue(watervalue);
	}
	protected void setvalue(String watervalue2) {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur;
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadHotwatersystem + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{
				 cur= db.hi_db.rawQuery("select * from " + db.CreateHotwatersystem + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			}
		
			System.out.println("count"+cur.getCount());
		    if (cur.getCount() > 0) 
		    {
				cur.moveToFirst();
				str = db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));
				if(str.contains(","))
			    {
			    	str = str.replace(",","&#44;");
			    }	
			}
		    else
		    {
			if(watervalue2.equals("Flue Type") && fluetypevalue!=null)
				{
					str = tempfluetype;		
				}
		    }
		    System.out.println("str"+str);
		    
			    if(!str.trim().equals(""))
				{
					cf.setValuetoCheckbox(ch, str);
				}	
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void showoptions() {
		// TODO Auto-generated method stub
		ch = new CheckBox[str_arrlst.size()];		
		for (int i = 0; i < str_arrlst.size(); i++) {
        		ch[i] = new CheckBox(this);
				ch[i].setText(str_arrlst.get(i));
				ch[i].setTextColor(Color.BLACK);
				lin.addView(ch[i]);
				
		}
	}

	private void dbvalue(String watervalue2) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		
			for(int j=0;j<fueltyprearr.length;j++)
			{
				str_arrlst.add(fueltyprearr[j]);
			}	
		
		try
		{
			 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOther + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(watervalue2)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_srid='"+getselectedho+"'",null);
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =0;i<c.getCount();i++,c.moveToNext())
				 {
					 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
				 }
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	protected void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(NewWoodStove.this, NewGasLog.class);
    	Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		Bundle b6 = new Bundle();
		b6.putString("selectedid", getselectedho);			
		intent.putExtras(b6);
		startActivity(intent);
		finish();
	}
}

