package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.Cseform.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.GraphicsView;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;



public class StartInspection extends Activity {
	static Context con;
	static DatabaseFunction db;
	static Webservice_config wb;
	static CommonFunction cf;
	static int k=0,pia=0;
	static int signalert=0;
	static boolean initialbool = false;
	static Progress_staticvalues p_sv;
	ListView startlst_module;
	static String inspectorid="",filepath = "empty",extStorageDirectory,selectedImagePath = "empty",picname="",
			phfirstname="",phlastname="",phaddress="",phcity="",phfee="",title="",error_msg="",phsrid="",
			phstate="",phcounty="",phzip="",phdate="";
	String str_firstname="", str_lastname="", str_inspectionaddress1="",str_inspectioncity="",str_inspectionstate="", str_inspectionzip="",
	       str_policynumber="",str_inspectiondate="",str_inspectiontime="",str_seachtext="",str_srid="",start_module="";
	static String getwarrantyoption="Yes",fullpath="";
	static String selectedho="";
	static String[] arrfname=null, arrlname=null, arraddress=null, arrcity=null, arrstate=null, arrzip=null, arrpolicynumber=null, arrdate=null,
			arrtime=null, arrsrid=null;
	EditText et_searchtext;
	static GraphicsView gv;
	TextView tvnote,tvcountid, tvheader;
	static BitmapDrawable bmdregister;
	static RadioGroup rdg;
	static byte[] byteArray;
	static private int handler_msg=2;
	static Dialog add_dialog = null;
	View v;	
	static int stat=0;
	static OutputStream outStream;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 db = new DatabaseFunction(this);
		 cf = new CommonFunction(this);
		 wb = new Webservice_config(this);
		 
		 con = StartInspection.this;
		 
		 Bundle b = this.getIntent().getExtras();
		 if(b!=null)
		 {
		   inspectorid = b.getString("inspectorid");
		 }
			System.out.println("inspec="+inspectorid);
		setContentView(R.layout.activity_start);
		
		db.CreateTable(12);
		db.CreateTable(28);
		db.CreateTable(31);
		db.userid();
		declaration();
	}
	private void declaration() {
		// TODO Auto-generated method stub
		startlst_module = (ListView) findViewById(R.id.startlist);
		tvnote = (TextView)findViewById(R.id.noteid);
		tvcountid = (TextView)findViewById(R.id.countid);
		tvheader = (TextView)findViewById(R.id.header);
		System.out.println("sat="+cf.chkstat);
		if(cf.chkstat==1)
		{
			tvheader.setText(" START INSPECTION ");
		}
		else if(cf.chkstat==2)
		{
			tvheader.setText(" SUBMITTED INSPECTION ");
		}
		listfromdb();		
	
		((Button)findViewById(R.id.btn_back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(StartInspection.this, Dashboard.class));
				finish();
			}
		});
		
            
       et_searchtext = (EditText)findViewById(R.id.searchtext);
       
       
       ((ImageView)findViewById(R.id.clear)).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			et_searchtext.setText("");str_seachtext="";cf.hidekeyboard(et_searchtext);
			listfromdb();
		}
	   });
       
       ((ImageView)findViewById(R.id.search)).setOnClickListener(new OnClickListener() {
   		
   		@Override
   		public void onClick(View v) {
   			// TODO Auto-generated method stub
   			str_seachtext = et_searchtext.getText().toString();cf.hidekeyboard(et_searchtext);
   			listfromdb();
   		}
   	   });
       
       
       et_searchtext.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        boolean handled = false;
		        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
		        	str_seachtext = et_searchtext.getText().toString();
		        	cf.hidekeyboard(et_searchtext);							
		   			listfromdb();
		            handled = true;
		        }
		        return handled;
		    }
		});
	}
	private void listfromdb() {
		// TODO Auto-generated method stub
		Cursor cur1 = null;
		str_firstname="";str_lastname="";str_inspectionaddress1="";str_inspectioncity="";str_inspectionstate="";
		str_inspectionzip="";str_policynumber="";str_inspectiondate="";str_inspectiontime="";
		System.out.println("cf.chkstat="+cf.chkstat);
		try
		{
			if(str_seachtext.trim().equals(""))
			{
			   cur1 = db.hi_db.rawQuery("select * from " + db.OrderInspection + " where fld_inspectorid='"+inspectorid+"' and fld_status='"+cf.chkstat+"'",null);
			   
			}
			else
			{
				cur1 = db.hi_db.rawQuery("select * from " + db.OrderInspection + " where fld_inspectorid='"+inspectorid+"' and fld_status='"+cf.chkstat+"' and (fld_firstname like '%"+db.encode(str_seachtext) + "%' or fld_lastname like '%"+db.encode(str_seachtext) + "%' or fld_policynumber like '%"+db.encode(str_seachtext) + "%')",null);
			}
			if(cur1!=null)
			{
				tvcountid.setText("Total no. of records : "+cur1.getCount());
				if(cur1.getCount()==0)
				{
					tvnote.setText("No records found for your search criteria.");
					startlst_module.setVisibility(v.GONE);
				}
				else
				{
					tvnote.setText("FIRST NAME | LAST NAME | INSPECTION ADDRESS1 | INSPECTION CITY | INSPECTION STATE | INSPECTION ZIP | POLICY NUMBER | INSPECTION DATE | INSPECTION TIME");
					startlst_module.setVisibility(v.VISIBLE);
					cur1.moveToFirst();
					
					arrfname=new String[cur1.getCount()];
					arrlname=new String[cur1.getCount()];
					arraddress=new String[cur1.getCount()];
					arrcity=new String[cur1.getCount()];
					arrstate=new String[cur1.getCount()];
					arrzip=new String[cur1.getCount()];
					arrpolicynumber=new String[cur1.getCount()];
					arrdate=new String[cur1.getCount()];
					arrtime=new String[cur1.getCount()];
					arrsrid=new String[cur1.getCount()];
					
					for(int i=0;i<cur1.getCount();i++,cur1.moveToNext())
					{
						arrfname[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_firstname")));
						arrlname[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_lastname")));
						arraddress[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectionaddress1")));
						arrcity[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectioncity")));
						arrstate[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectionstate")));
						arrzip[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_zip")));
						arrpolicynumber[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_policynumber")));
						arrdate[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectiondate")));
						arrtime[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_inspectiontime")));
						arrsrid[i] = db.decode(cur1.getString(cur1.getColumnIndex("fld_orderedid")));						
						
	        		}
					startlst_module.setAdapter(new EfficientAdapter(StartInspection.this));
				}
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("error="+e.getMessage());
		}
	}
	public static class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
		
		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
			
		}

		public int getCount() {
			int arrlen = 0;
			arrlen = arrfname.length;


			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.startlist, null);
				holder.text = (TextView) convertView.findViewById(R.id.TextView01);
				holder.text2 = (Button) convertView.findViewById(R.id.start);
				
				if(cf.chkstat==1)
				{
					holder.text2.setText("Start Inspection");	
				}
				else if(cf.chkstat==2)
				{
					holder.text2.setText("Edit Inspection");	
				}
				
				holder.rel = (RelativeLayout) convertView.findViewById(R.id.rel2);
				holder.rel.setVisibility(convertView.VISIBLE);
				holder.text2.setVisibility(convertView.VISIBLE);
				
				
				holder.invoice = (Button)convertView.findViewById(R.id.invoicebtn);
				holder.invoice.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectedho = arrsrid[position];System.out.println("selectedho"+selectedho);
						 
						Intent i = new Intent(con,InVoice.class);
						i.putExtra("inspectorid", inspectorid);
						i.putExtra("srid", selectedho);
						con.startActivity(i);
						((Activity) con).finish();	
						
						
					}

				});
				
				holder.coverphoto = (Button)convertView.findViewById(R.id.coverphotobtn);
				holder.coverphoto.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectedho = arrsrid[position];System.out.println("selectedho"+selectedho);
						 
						Intent i = new Intent(con,CoverPhoto.class);
						i.putExtra("inspectorid", inspectorid);
						i.putExtra("srid", selectedho);
						con.startActivity(i);
						((Activity) con).finish();	
					}

				});
				
				holder.homewarranty = (Button)convertView.findViewById(R.id.homewarrantybtn);
				holder.homewarranty.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectedho = arrsrid[position];System.out.println("selectedho"+selectedho);
						 
						show_alert();
					}

				});
				
				holder.cse = (Button)convertView.findViewById(R.id.csebtn);
				holder.cse.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectedho = arrsrid[position];System.out.println("cseselectedho"+selectedho);
						 
						Intent i = new Intent(con,Cseform.class);
						i.putExtra("inspectorid", inspectorid);
						i.putExtra("srid", selectedho);
						con.startActivity(i);
						((Activity) con).finish();						
					}

				});
				
				holder.pia = (Button)convertView.findViewById(R.id.piabtn);
				holder.pia.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						db.CreateTable(37);
						pia=1;
						selectedho = arrsrid[position];System.out.println("piaselectedho"+selectedho);
						 signaturealert(selectedho);
						/*Intent i = new Intent(con,Piaform.class);
						i.putExtra("inspectorid", inspectorid);
						i.putExtra("srid", selectedho);
						con.startActivity(i);
						((Activity) con).finish();	*/					
					}

				});
				
				holder.summary = (Button)convertView.findViewById(R.id.summarybtn);
				holder.summary.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectedho = arrsrid[position];System.out.println("summaryselectedho"+selectedho);
						 
						Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+selectedho+"'", null);
						if(cur.getCount()>0)
				        {
							Intent in = new Intent(con, Summary.class);
							in.putExtra("inspectorid",inspectorid);
							in.putExtra("srid",selectedho);					
							con.startActivity(in);
							((Activity) con).finish();									
				        }
						else
						{
							cf.DisplayToast("No Summary information available.");
						}					
					}

				});
				
				holder.feedback = (Button)convertView.findViewById(R.id.feedbackbtn);
				holder.feedback.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectedho = arrsrid[position];System.out.println("cseselectedho"+selectedho);
						 
						Intent i = new Intent(con, Feedback.class);	
						i.putExtra("srid",selectedho);
						i.putExtra("inspectorid",db.UserId);
						con.startActivity(i);
						((Activity) con).finish();					
					}

				});
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
		
			holder.text.setText(Html.fromHtml(arrfname[position]+" | "+arrlname[position])+" | "+arraddress[position]+" | "+ 
				(arrcity[position].equals("")? "N/A":arrcity[position]) +" | "+(arrstate[position].equals("--Select--")? "N/A":arrstate[position]) +" | "+
				(arrzip[position].equals("")? "N/A":arrzip[position]) +" | "+(arrpolicynumber[position].equals("")? "N/A":arrpolicynumber[position]) +" | "+
				(arrdate[position].equals("")? "N/A":arrdate[position]) +" | "+(arrtime[position].equals("")? "N/A":arrtime[position]));
			
				
				holder.text2.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						 selectedho = arrsrid[position];System.out.println("selectedho"+selectedho);
						
						 Intent intent = new Intent(con,LoadModules.class);
						 intent.putExtra("selectedid", selectedho);
						 intent.putExtra("inspectorid", inspectorid);
				         con.startActivity(intent);
						 ((Activity) con).finish();
					}
				});
			
			return convertView;
		}

		

		



		static class ViewHolder {
			TextView text;
			Button text2, homewarranty, cse, summary, feedback, pia,coverphoto,invoice;
			RelativeLayout rel;
		
		}

	}
	private static void signaturealert(final String srid)
	{
		signalert=1;
		final Dialog dialog = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.getWindow().setContentView(R.layout.signature);
		
		Button btn_save = (Button) dialog.findViewById(R.id.save);
		Button btn_cancel = (Button) dialog.findViewById(R.id.cancel);
		Button btn_close = (Button) dialog.findViewById(R.id.close);
		Button btn_preview = (Button) dialog.findViewById(R.id.preview);
		gv = (GraphicsView) dialog.findViewById(R.id.sigview);
		gv.setDrawingCacheEnabled(true);
		//gv.buildDrawingCache();
        
		System.out.println("inside");
		try
		{
			 Cursor cur = db.hi_db.rawQuery("select * from " + db.PIA + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 
                 selectedImagePath = db.decode(cur.getString(cur.getColumnIndex("fld_signature")));
                 System.out.println("selectedImagePath="+selectedImagePath);
				 
				 //File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/PIASignature/"+inspectorid+"/"+srid+"/"+selectedImagePath);
				 File outputFile = new File(selectedImagePath);
					System.out.println("outputFile.exists()"+outputFile.exists());
						if(outputFile.exists())
					{
						bmdregister=getbitmap(outputFile);
						if(bmdregister!=null)
						{
							gv.setBackgroundDrawable(bmdregister);GraphicsView.s = 1;
						}		
					}
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		gv.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				GraphicsView.s = 1;
				initialbool = true;
				return false;
			}
		});
	 
		
		btn_close.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				pia=0;GraphicsView.s =0;
				signalert=0;
				dialog.cancel();
			}
		});
		
		btn_preview.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) {
				pia=0;
				Intent i = new Intent(con,Piaform.class);
				i.putExtra("inspid", inspectorid);
				i.putExtra("srid",selectedho);
				con.startActivity(i);
				((Activity) con).finish();
			}
		});
		
		btn_save.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			
				System.out.println("GraphicsView.s "+GraphicsView.s +"c");
				//Bitmap bm = gv.getDrawingCache();
				if (GraphicsView.s == 1) 
				{
					/*gv.setBackgroundColor(Color.WHITE);
					Bitmap bm = gv.getDrawingCache();
					System.out.println("bm="+bm);*/
					/*if(bm!=null)
					{*/
						/*cf.DisplayToast("Please add Customer Signature.");
					}
					else
					{*/
					/*ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
					byteArray = stream.toByteArray();System.out.println("what us bytearr"+byteArray);*/
					
					signalert=0;
					
					 db.userid();
					 db.CreateTable(28);
					 
					 
					 try
					 {
						 Cursor cur = db.hi_db.rawQuery("select * from " + db.OrderInspection + " where fld_orderedid='"+srid+"'",null);
						 cur.moveToFirst();
						 if (cur.getCount() >= 1) {
							 	phsrid = db.decode(cur.getString(cur.getColumnIndex("fld_orderedid")));
							 	phfirstname = db.decode(cur.getString(cur.getColumnIndex("fld_firstname")));
								phlastname = db.decode(cur.getString(cur.getColumnIndex("fld_lastname")));
								phaddress = db.decode(cur.getString(cur.getColumnIndex("fld_inspectionaddress1")));
								phcity = db.decode(cur.getString(cur.getColumnIndex("fld_inspectioncity")));
								phstate = db.decode(cur.getString(cur.getColumnIndex("fld_inspectionstate")));
								phcounty =  db.decode(cur.getString(cur.getColumnIndex("fld_inspectioncounty")));
								phzip = db.decode(cur.getString(cur.getColumnIndex("fld_zip")));
								phfee = cur.getString(cur.getColumnIndex("fld_inspectionfee"));
								phdate = cur.getString(cur.getColumnIndex("fld_inspectiondate"));
							
								if(phstate.equals("--Select--"))
								{
									phstate = phstate.replace("--Select--", "");
								}
								
						 }
					 }
					 catch (Exception e) {
						// TODO: handle exception
					}
					
					 if(phfee.equals(""))
					 {
						 phfee="0.0";
					 }
					 
					 if (GraphicsView.s == 1) 
						{
							//gv.setBackgroundColor(Color.TRANSPARENT);
							signature();
							 try
								{
									 Cursor cur = db.hi_db.rawQuery("select * from " + db.PIA + " where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'",null);
									 System.out.println("cut="+cur.getCount());
									 if(cur.getCount()>0)
									 {
										 System.out.println("UPDATE "+ db.PIA +"  SET fld_signature='"+db.encode(fullpath) + "' where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'");
										 db.hi_db.execSQL("UPDATE "+ db.PIA +"  SET fld_signature='"+db.encode(fullpath) + "' where fld_inspectorid='"+inspectorid+"' and fld_srid='"+srid+"'");
									 }
									 else
									 {
										 System.out.println("INSERT INTO "
							  						+ db.PIA
							  						+ " (fld_inspectorid,fld_srid,fld_signature)"
						  						+ " VALUES ('" + inspectorid + "','"+srid+ "','" +db.encode(fullpath)+"')");
										 db.hi_db.execSQL("INSERT INTO "
							  						+ db.PIA
							  						+ " (fld_inspectorid,fld_srid,fld_signature)"
						  						+ " VALUES ('" + inspectorid + "','"+srid+ "','" +db.encode(fullpath)+"')");
									 }
								}
							    catch (Exception e) {
									// TODO: handle exception
								}
							   cf.DisplayToast("Pre Inspection Agreement has been submitted successfully");
							   dialog.cancel();
						}
					//}
				} else {
					cf.DisplayToast("Please add PIA Signature.");

				}
			}					
		});
		btn_cancel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/*pia=0;
				signalert=0;
				GraphicsView.s=0;
				gv.clear();*/
			   signclear();
				
			}					
		});
		dialog.setCancelable(true);
		dialog.show();
	}
	
	private static BitmapDrawable getbitmap(File OutputFile)
	{
		bmdregister=null;
		try {
			BitmapFactory.Options o1 = new BitmapFactory.Options();
			o1.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(OutputFile),null, o1);
		
		final int REQUIRED_SIZE = 200;
		int width_tmp1 = o1.outWidth, height_tmp1 = o1.outHeight;
		int scale1 = 1;
		while (true) {
			if (width_tmp1 / 2 < REQUIRED_SIZE
					|| height_tmp1 / 2 < REQUIRED_SIZE)
				break;
			width_tmp1 /= 2;
			height_tmp1 /= 2;
			scale1 *= 2;
		}
		BitmapFactory.Options o3 = new BitmapFactory.Options();
		o3.inSampleSize = scale1;
		Bitmap bitmap1 = BitmapFactory.decodeStream(new FileInputStream(
				OutputFile), null, o3);
		
		bmdregister = new BitmapDrawable(bitmap1);
		return bmdregister;
		} catch (Exception e) {
		System.out.println("catch="+e.getMessage());
			return bmdregister;
		}
	}
	protected static void signclear() {
		// TODO Auto-generated method stub
		 File outputFile = new File(Environment.getExternalStorageDirectory() + "/HI/PIASignature/"+inspectorid+"/"+selectedho+"/"+selectedImagePath);
		 System.out.println("outputFile.exists()"+outputFile.exists());
				if(outputFile.exists())
			{
					outputFile.delete();
			}
		selectedImagePath="";
	//	gv.setDrawingCacheEnabled(true);
		gv.clear();
		//gv.setDrawingCacheEnabled(false);
		gv.setBackgroundColor(Color.parseColor("#ffffff"));
		initialbool=false;
		GraphicsView.s = 0;
		
		
	}
	protected static void signature() {
		// TODO Auto-generated method stub
		Bitmap bm = gv.getDrawingCache();								
		if(bm!=null)
		{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
			byteArray = stream.toByteArray();					
			
			 String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
			 fullpath = extStorageDirectory+"/HI/PIASignature/"+inspectorid+"/"+selectedho+"/"+ "PIASignature"+"_"+selectedho+".jpg";
			 String st = "/HI/PIASignature/"+inspectorid+"/"+selectedho+"/";
			 File folder = new File(extStorageDirectory,st);
			 if(!folder.exists())
			 {
			    folder.mkdirs();
			 }			 
			
			String _fileName = "PIASignature"+"_"+selectedho;
			filepath = _fileName.toString() + ".jpg";		
			
			File file = new File(folder, String.valueOf(filepath));
			
	  	System.out.println("_file="+file.exists()+"file="+filepath);
		
		try {
			 
			outStream = new FileOutputStream(file);
			
			outStream.write(byteArray);
			outStream.flush();
			outStream.close();
			selectedImagePath = filepath.toString();
			String[] bits = selectedImagePath.split("/");
			picname = bits[bits.length - 1];
			
			
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("IOException"+e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("ee"+e.getMessage());
			e.printStackTrace();
		}
		
			
			
		}
		
	}
	static void call_webservice() {
		// TODO Auto-generated method stub
		if(cf.isInternetOn())
		  {
			   new Start_xporting().execute("");
		  }
		  else
		  {
			  
			  cf.DisplayToast("Please enable your internet connection.");
			  
		  }
		
	}
	static class Start_xporting extends AsyncTask <String, String, String> {		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="PIA Submission";
			    p_sv.progress_Msg="Please wait..Your PIA has been submitting.";      
				((Activity) con).startActivityForResult(new Intent(con,Progress_dialogbox.class), Static_variables.progress_code);	       
		    }		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		
			 		    Submit_PIA();					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		        return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		success_alert();
      	  
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		failure_alert();
		    	
		    	}
		    	
		    }
	}
	private static void success_alert() {
		// TODO Auto-generated method stub
       
		AlertDialog al = new AlertDialog.Builder(con).setMessage("You've successfully submitted your PIA Form.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				  
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	private static void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(con).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}
	private static void Submit_PIA() throws IOException, XmlPullParserException 
	{
		  try
 		  {
	        	    SoapObject request = new SoapObject(wb.NAMESPACE,"GeneratePIA_PDF");
	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope.dotNet = true;
	      		    
	      		    request.addProperty("OwnerName",phfirstname);
	      		    request.addProperty("Address",phaddress);
	      		    request.addProperty("City",phcity);
	      		    request.addProperty("state",phstate);
	      		    request.addProperty("zipcode",phzip);
	      		    request.addProperty("InspectorName",db.Userfirstname+" "+db.Userlastname);
	      		    request.addProperty("Policynumber",phsrid);
	      		    request.addProperty("fee",phfee);
	      		    request.addProperty("InspectionDate",phdate);
	      		    request.addProperty("CustomerSignature",byteArray);
		      		      		  
		      		  
		         		envelope.setOutputSoapObject(request);
		         		MarshalBase64 marshal = new MarshalBase64();
		        		marshal.register(envelope);
		         		System.out.println("request="+request);
		         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
		         		SoapObject response = null;
	         		
		         		try
		         		{
	                      androidHttpTransport.call(wb.NAMESPACE+"GeneratePIA_PDF", envelope);
	                      response = (SoapObject)envelope.getResponse();
	                      System.out.println("GeneratePIA_PDF "+response);
	                      
	                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	                      {
	                    	  	String path = response.getProperty("PDFURL").toString();System.out.println("path "+path);
	                    		String[] filenamesplit = path.split("/");
								System.out.println("the path is "+path);
								String viewfilename = filenamesplit[filenamesplit.length - 1];
								System.out.println("The File Name is " + viewfilename);
								String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
								File folder = new File(extStorageDirectory,"HI/PIAForm");
								folder.mkdir();
								File file = new File(folder, viewfilename);
								try {
									file.createNewFile();
									Downloader.DownloadFile(path, file);
									 View_Pdf_File(viewfilename,path);
								} catch (IOException e1) {
									e1.printStackTrace();
									System.out.println("e1="+e1.getMessage());
								}
	                    	                		  
	 	                    	handler_msg = 0;                   	  
	                    	 	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                catch(Exception e)
	                {
	                     e.printStackTrace();
	                }
	        	
		}      
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("error in cse form webservice"+e.getMessage());
		}
   }
	private static void View_Pdf_File(String filename,String filepath) {
		// TODO Auto-generated method stub
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/HI/PIAForm/" + filename);
		Uri path = Uri.fromFile(file);
		System.out.println("pathdd="+path);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		try 
		{
			(con).startActivity(intent);

		} catch (ActivityNotFoundException e) {
			System.out.println("e="+e.getMessage());
			Intent intentview = new Intent(con, ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			(con).startActivity(intentview);
			
		}
	}
	public static void show_alert(){
			// TODO Auto-generated method stub
			k=1;
			add_dialog = new Dialog(con);
			add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			add_dialog.setCancelable(false);
			add_dialog.getWindow().setContentView(R.layout.warrantyinfo);
			
			rdg = (RadioGroup)add_dialog.findViewById(R.id.homerdg);
			Cursor cur = db.hi_db.rawQuery("select * from " + db.HomeWarranty + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+selectedho+"'", null);
			if(cur.getCount()>0)
	        {
				cur.moveToFirst();	
				((RadioButton)rdg.findViewWithTag(cur.getString(cur.getColumnIndex("fld_homewarrantoption")))).setChecked(true);
	        }
			else
			{
				((RadioButton)rdg.findViewWithTag("Yes")).setChecked(true);
			}
			
			rdg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					RadioButton rd = ((RadioButton)group.findViewById(checkedId));
					getwarrantyoption = rd.getText().toString();
					
				}
			});
			
			TextView txt = (TextView) add_dialog.findViewById(R.id.txt);
			String str = "<b>This home is eligible for a one year Home Warranty Offer!. Items included in the one year home warranty if purchased are:\n</b>" +
					"\t Furnaces, Boilers, Heat Pumps, Central Air Conditioners, Electrical Systems, Thermostats, Water Heater System, Plumbing, Polybutylene Lines, " +
					"Sump Pumps, Whirlpools, Dishwasher, Food Waste Disposer, Cooking Range/Oven, Microwave, Kitchen Refrigerator, Trash Compactor, " +
					"Plumbing Fixtures and Faucets, Domestic Water Softner, Well Water System, Fireplace Gas Burner, Attic and Exhausts Fans, Humidifiers, Dehumidifiers, " +
					"Lighting Fixtures, Septic Lines, Ejector Pump";
			txt.setText(Html.fromHtml(str));
			
			Button btnclose = (Button)add_dialog.findViewById(R.id.close);
			btnclose.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					k=0;
					add_dialog.dismiss();
				}
			});
			
			Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
			btncancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					k=0;
					add_dialog.dismiss();
				}
			});
			
			Button btnsave= (Button) add_dialog.findViewById(R.id.submit);
			btnsave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{						
						int warrantyrdiogrpid= rdg.getCheckedRadioButtonId();
						getwarrantyoption = (warrantyrdiogrpid==-1)? "":((RadioButton) add_dialog.findViewById(warrantyrdiogrpid)).getText().toString();						
						Cursor cur = db.hi_db.rawQuery("select * from " + db.HomeWarranty + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+selectedho+"'", null);
						System.out.println("select * from " + db.HomeWarranty + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+selectedho+"'"+cur.getCount());
						if(cur.getCount()>0)
			            {
			            	db.hi_db.execSQL("UPDATE "
									+ db.HomeWarranty
									+ " SET fld_homewarrantoption='"+getwarrantyoption
									+"'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+selectedho+"'");
			            }
			            else
			            {
			            	System.out.println(" INSERT INTO "
									+ db.HomeWarranty
									+ " (fld_inspectorid,fld_srid,fld_homewarrantoption) VALUES"
									+ "('"+db.UserId+"','"+selectedho+"','" + getwarrantyoption+ "')");
			            	db.hi_db.execSQL(" INSERT INTO "
									+ db.HomeWarranty
									+ " (fld_inspectorid,fld_srid,fld_homewarrantoption) VALUES"
									+ "('"+db.UserId+"','"+selectedho+"','" + getwarrantyoption+ "')");
			            }
						cf.DisplayToast("Home Warranty information saved successfully");
						add_dialog.dismiss();
						k=0;
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("hoe warranty="+e.getMessage());
					}
				}
			});
			
			add_dialog.show();
	}		
	@Override
    protected void onSaveInstanceState(Bundle outState) {	 
	  
        outState.putString("searchtext", str_seachtext);
        outState.putInt("iden", k);        
        outState.putString("warrantyoption", getwarrantyoption);
        outState.putInt("sign_alert", signalert);
        outState.putInt("pia",pia);
        outState.putString("selecho", selectedho);
        if(pia==1)
        {
        	outState.putBoolean("graphic", initialbool);
    		
    		if(initialbool)
    		{
    		    signature();
    			outState.putString("imagepath", selectedImagePath);
    		}
           /*outState.putInt("img_drawn", GraphicsView.s );
           if(gv.isDrawingCacheEnabled())
  		   {			 System.out.println("signinside");
  		 	gv.setSaveEnabled(true);
  		 	gv.setBackgroundColor(Color.TRANSPARENT);
  		    Bitmap asd = gv.getDrawingCache();
  		   
  		    ByteArrayOutputStream baos=new  ByteArrayOutputStream();
  		    asd.compress(Bitmap.CompressFormat.PNG,100, baos);
             byte [] arr=baos.toByteArray();
             System.out.println("savearr="+arr);
             outState.putByteArray("img_array",arr);*/
  		}
       
        /*if(GraphicsView.s==1)
        {
        	gv.setBackgroundColor(Color.TRANSPARENT);
			Bitmap bm = gv.getDrawingCache();		
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
			byteArray = stream.toByteArray();
			 outState.putByteArray("img_array",byteArray);System.out.println("what us bytearr"+byteArray);
        }
       */
       
        System.out.println("save="+getwarrantyoption);
        super.onSaveInstanceState(outState);
    }
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	str_seachtext = savedInstanceState.getString("searchtext");
    	k = savedInstanceState.getInt("iden");
    	signalert = savedInstanceState.getInt("sign_alert");
    	pia = savedInstanceState.getInt("pia");
    	selectedho = savedInstanceState.getString("selecho");
    	if(pia==1)
    	{
    		signaturealert(selectedho);
    	}
    	System.out.println("signalert"+signalert);
    	/*if(signalert==1)
    	{*/
    		/*GraphicsView.s = savedInstanceState.getInt("img_drawn");System.out.println("GraphicsView.s"+GraphicsView.s);
    		
    		if(GraphicsView.s==1)
    		{
    			System.out.println("nssd");
    			
  		      byteArray = savedInstanceState.getByteArray("img_array");
  		    gv.setDrawingCacheEnabled(true);
  			gv.buildDrawingCache();
*/		      /*System.out.println("onres="+byteArray);
		      
		       Bitmap bitmap=BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
		   
		       Bitmap bmsignature=Bitmap.createBitmap(bitmap);
		       Bitmap mutableBitmap = bmsignature.copy(Bitmap.Config.ARGB_8888, true);
		       Canvas c = new Canvas(mutableBitmap);
		       
		       Paint textPaint = new Paint();
		       textPaint.setColor(Color.TRANSPARENT);
		       c.drawBitmap(bmsignature, 0, 0, textPaint);

		       gv.buildDrawingCache();
		       gv.setDrawingCacheEnabled(true);*/
		      

  		   /*
  		    Bitmap bmp=BitmapFactory.decodeByteArray(byteArray,0,byteArray.length); 
  		    if (bmp!=null) 
  		    { 
  		    	gv.setBackgroundColor(Color.TRANSPARENT);
				//bmp = gv.getDrawingCache();	
  		    	System.out.println("jjjjj="+bmp);
  		    	gv.setImageBitmap(bmp); 
  		    }*/ 
  		 
  			//signaturealert();
  			/*byteArray = savedInstanceState.getByteArray("img_array");System.out.println("byteArray==="+byteArray);
  			Bitmap bmp=BitmapFactory.decodeByteArray(byteArray,0,byteArray.length); 
  		    if (bmp!=null) 
  		    { 
  		    	System.out.println("dddd"+bmp);
  		    	//gv.setDrawingCacheEnabled(true);
  		    	gv.setDrawingCacheEnabled(true);
  		    	//gv.setBackgroundColor(Color.TRANSPARENT);
				//bmp = gv.getDrawingCache();	
  		    	//System.out.println("jjjjj="+bmp);
  		    	gv.setImageBitmap(bmp); 
  		    }*/
  		     
    		//}
    	//}
    	if(k==1)
    	{
    		getwarrantyoption = savedInstanceState.getString("warrantyoption");
    		show_alert();
    		System.out.println("getwarrantyoption="+getwarrantyoption);
    		((RadioButton)rdg.findViewWithTag(getwarrantyoption)).setChecked(true);
    		
    	}
    	listfromdb();
    	
         
        super.onRestoreInstanceState(savedInstanceState);
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(StartInspection.this, Dashboard.class));
			finish();
			
		}

		return super.onKeyDown(keyCode, event);
	}
}
