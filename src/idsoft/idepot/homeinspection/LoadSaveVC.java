package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.ReportPreview.btn_click_listener;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DataBaseHelperVC;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.io.IOException;
import java.util.ArrayList;



import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class LoadSaveVC extends Activity{
	String gettempname="",getcurrentview="",getmodulename="",strrd_cond="",getinspectionpoint="",str_ip="",get_scon="",str_pp="2",
			getprimaryvc="",getsecondaryvc="",getsubmodulename="",strother="",strcomments="",getph="",str="",getvctemp="",geteditid="",
			getvc="",gettempoption="",strchkval="",seltdval="",selectvalseccond="",selectvalinsppoint="";
	TextView tv_seltemplate,tv_modulename,tvcomment,sp_inspectionpoint,tv_header,tv_photo,sp_secondaryvc,sp_conttype,tv_submodulename;
	String getconttype="",selectvalcont="",getsectext="",getinsptext="",getconttext="",editvcid="0";
	Button btnback,btnhome,btn_clear,btn_save;
	Spinner sp_primaryvc;
	ArrayAdapter primaryadap,secondaryadap;
	ImageView loadcomments;
	LinearLayout modlinear,lin,li_pp;
	String[] arr_pvc,secondaryyarr;
	CommonFunction cf;
	int insp_dialog=0,sec_dialog=0,dialog_other=0,cont_dialog=0;
	Cursor scur;
	DatabaseFunction db;
	Button cur_sel_b ;
	boolean load_comment=true;
	int current_sub_pos;
	EditText etcomments,otheret,etother;
	RadioGroup rgcnd,rdpp;
	boolean rgcond=false;
	CheckBox[] ch;
	View v;
	DataBaseHelperVC dbh;
	LinearLayout li_ho;
	public int[] chkbxid = {R.id.chk1,R.id.chk2,R.id.chk3,R.id.chk4};
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	public CheckBox[] cb = new CheckBox[chkbxid.length];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getmodulename = b1.getString("modulename");
		Bundle b2 = this.getIntent().getExtras();
		getsubmodulename = b2.getString("submodulename");
		Bundle b3 = this.getIntent().getExtras();
		getcurrentview = b3.getString("currentview");
		Bundle b4 = this.getIntent().getExtras();
		getph = b4.getString("selectedid");
		
		Bundle b6 = this.getIntent().getExtras();
		getvc = b6.getString("selectedvc");
		Bundle b7 = this.getIntent().getExtras();
		if(b7.getString("tempoption")!=null){gettempoption = b7.getString("tempoption");}
		Bundle b8 = this.getIntent().getExtras();
		if(b8.getString("vceditid")!=null){geteditid = b8.getString("vceditid");}
		System.out.println("geteditid"+geteditid);
		
		setContentView(R.layout.activity_createvc);
		System.out.println("tetsLoadSaveVC");
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		
		db.CreateTable(1);
		db.CreateTable(5);
		db.CreateTable(6);
		db.CreateTable(12);
		db.CreateTable(18);
		db.userid();
		
		final  ScrollView scr = (ScrollView)findViewById(R.id.submodulelist);
	       scr.post(new Runnable() {
	          public void run() {
	              scr.scrollTo(0,0);
	          }
	      }); 
	       
		tv_seltemplate = (TextView)findViewById(R.id.seltemplate);
		tv_seltemplate.setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
		tv_modulename = (TextView)findViewById(R.id.modulename);
		tv_modulename.setText("Module Name : "+getmodulename);
		tv_submodulename = (TextView)findViewById(R.id.submodulename);
		tv_submodulename.setText(getsubmodulename);
		/*tv_header = (TextView)findViewById(R.id.header);
		tv_header.setText(" Start Inspection - Visible Conditions ");*/
		tv_photo = (TextView)findViewById(R.id.btn_photo);
		tv_photo.setVisibility(v.VISIBLE);
			
		li_pp=(LinearLayout) findViewById(R.id.pp_table);
		li_pp.setVisibility(v.VISIBLE);
		rdpp = (RadioGroup)findViewById(R.id.printphotosrdgp);
		rdpp.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					    RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
				        boolean isChecked = checkedRadioButton.isChecked();
				        if (isChecked)
				        {
				        	str_pp = checkedRadioButton.getTag().toString();
				         
						
					   }
				}
			});
		
		
		li_ho=(LinearLayout) findViewById(R.id.sub_section_names);
		tv_photo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			 	// TODO Auto-generated method stub
				    Intent intent = new Intent(LoadSaveVC.this, PhotoDialog.class);
					Bundle b1 = new Bundle();
					b1.putString("templatename", gettempname);			
					intent.putExtras(b1);
					Bundle b2 = new Bundle();
					b2.putString("modulename", getmodulename);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("submodulename", getsubmodulename);			
					intent.putExtras(b3);
					Bundle b8 = new Bundle();
					b8.putString("tempoption", gettempoption);			
					intent.putExtras(b8);
					Bundle b4 = new Bundle();
					b4.putString("photooption",getcurrentview);			
					intent.putExtras(b4);
					
					Bundle b6 = new Bundle();
					b6.putString("currentview", getcurrentview);			
					intent.putExtras(b6);
					
					Bundle b5 = new Bundle();
					b5.putString("selectedid", getph);			
					intent.putExtras(b5);
					Bundle b7 = new Bundle();
					b7.putString("selectedvc", getvc);			
					intent.putExtras(b7);
					startActivity(intent);
					finish();
             
			}
		});		
		btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		loadcomments = (ImageView)findViewById(R.id.loadcomments);
 		loadcomments.setOnClickListener(new OnClickListener() { 			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 		
 					
 						if(load_comment)
 						{
 							load_comment=false;
 							int loc1[] = new int[2];
 							v.getLocationOnScreen(loc1);
 							
 							Intent in =new Intent(LoadSaveVC.this,LoadComments.class);
 						 	in.putExtra("mod_name", getmodulename);
 							in.putExtra("submod_name", getsubmodulename);
 							in.putExtra("vcname", getvc);
 							in.putExtra("length", 12);
 							in.putExtra("max_length", 1000);
 							in.putExtra("xfrom", loc1[0]+10);
 							in.putExtra("yfrom", loc1[1]+10);
 							startActivityForResult(in, 34);
 						}
 			
 			}
 		});
		
		btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoadSaveVC.this,HomeScreen.class));
				finish();
			}
		});
		
		/*arr_pvc = new String[1];
		arr_pvc[0]=getvc;*/		 
		//getprimaryvc = arr_pvc[0];
		sp_primaryvc = (Spinner)findViewById(R.id.spin_primaryvc);
		
		try {
			dbh = new DataBaseHelperVC(LoadSaveVC.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			getvctemp = getvc;
			if(getvc.contains("'"))
			{
				getvctemp = getvctemp.replace("'","''");				
			}

			
			Cursor cur = newDB.rawQuery("select * from VCRANGE where RANGETABLE='"+getmodulename+"' and VCTEXT='"+getvctemp+"'",null);
			if(cur.getCount()>0)
			{
				cur.moveToFirst();
				String vcid = db.decode(cur.getString(cur.getColumnIndex("VCTYPE")));
				Cursor cur1 = newDB.rawQuery("select DISTINCT(VCTEXT) from TBL_VCCOMMENTS where RANGETABLE='"+getmodulename+"' and VCTYPE='"+vcid+"'",null);
				if(cur1.getCount()>0)
				{
					cur1.moveToFirst();
					arr_pvc =new String[cur1.getCount()+1]; 
					arr_pvc[0]="--Select--";
					for(int k =0;k<cur1.getCount();k++,cur1.moveToNext())
					{
						arr_pvc[k+1] = db.decode(cur1.getString(cur1.getColumnIndex("VCTEXT")));
					}
				}
				else
				{
					arr_pvc = new String[2];
					arr_pvc[0]="--Select--";
					arr_pvc[1]=getvc;
				}
			}
			else
			{
				arr_pvc = new String[2];
				arr_pvc[0]="--Select--";
				arr_pvc[1]=getvc;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}

		
		
		primaryadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arr_pvc);
		primaryadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_primaryvc.setAdapter(primaryadap);
		sp_primaryvc.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(1));
		
		sp_conttype = (TextView)findViewById(R.id.spin_contractortype);
		sp_conttype.setText("--Select--");
		sp_conttype.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cont_dialog=1;
				showcontdialog();
			}
		});     
		
		cf.GetCurrentModuleName(getmodulename);
		 cf.GetVC(getmodulename,getsubmodulename);
		secondaryyarr = cf.submodvcid;System.out.println("secarr="+secondaryyarr.length);
		/*sp_secondaryvc = (Spinner)findViewById(R.id.spin_secondaryvc);
	 	secondaryadap = new ArrayAdapter<String>(LoadSaveVC.this,android.R.layout.simple_spinner_item,secondaryyarr);
	 	secondaryadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	sp_secondaryvc.setAdapter(secondaryadap);
	 	sp_secondaryvc.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(2));*/
	 	
	 	etcomments = (EditText)findViewById(R.id.comments);
        tvcomment = (TextView)findViewById(R.id.tvcomment);
        etcomments.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
    			if(s.toString().startsWith(" "))
    			{
    				etcomments.setText("");
    			}
    			cf.ShowingLimit(s.toString(),tvcomment,1000); 
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				

			}
		});
        
        rgcnd = (RadioGroup)findViewById(R.id.rgcond);
		otheret = (EditText)findViewById(R.id.othertxt);
		otheret.addTextChangedListener(new CustomTextWatcher(otheret));
		rgcnd.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				    RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
			        boolean isChecked = checkedRadioButton.isChecked();
			        if (isChecked)
			        {
			         	strrd_cond= checkedRadioButton.getText().toString().trim();
			         	if(strrd_cond.equals("Other"))
			         	{
			         		otheret.setVisibility(v.VISIBLE);
			         		otheret.requestFocus();
			         	}
			         	else
			         	{
			         		otheret.setVisibility(v.GONE);
			         	}
			         	rgcond=false;
					
				   }
			}
		});
		
		for(int i=0;i<chkbxid.length;i++)
		{
			cb[i] = (CheckBox)findViewById(chkbxid[i]);
		}
		
		sp_inspectionpoint = (TextView)findViewById(R.id.spin_inspectionpoint);
		sp_inspectionpoint.setText("--Select--");
		sp_inspectionpoint.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				insp_dialog=1;
				dbvalue();
				showalert();
			}
		});
		
		sp_secondaryvc = (TextView)findViewById(R.id.spin_secondaryvc);
		sp_secondaryvc.setText("--Select--");
		sp_secondaryvc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sec_dialog=1;
				
				dbsecvalue();
				showsec_alert();
			}
		});
		
		
		btn_save = (Button)findViewById(R.id.ok);
        btn_save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 //strother = otheret.getText().toString();
				 strcomments = etcomments.getText().toString();
				 
				/*  if(getsecondaryvc.trim().equals(""))
				  {
							 cf.DisplayToast("Please select Secondary Condition");
				  }
				  else
				  {*/
					  /* if(strcomments.equals(""))
					 {
						 cf.DisplayToast("Please enter Comments");
					 }
					 else
					 {
						 if(strrd_cond.equals(""))
						 {
							 cf.DisplayToast("Please select Condition");
						 }
						 else
						 {
							 if(strrd_cond.equals("Other"))
							 {
								 if(strother.equals(""))
								 {
									 cf.DisplayToast("Please enter the Other text");
								 }
								 else
								 {*/
									 checkboxvalidation();
							/*	 }
							 }
							 else
							 {
								 checkboxvalidation();
							 }
						 }
					 }*/
				// }
		    }
		
		});
        
        btn_clear = (Button)findViewById(R.id.clear);
        btn_clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clearall();
				 
			}
		});
		defaultinsert();
		Show_subsection_info();
	}
	private void clearall() {
		// TODO Auto-generated method stub
		System.out.println("dsfosddjs");
		 getinspectionpoint="";getconttype="";getsecondaryvc="";
		 ((EditText) findViewById(R.id.approximaterepaircost)).setText("");	
		 etcomments.setText("");
         ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		 	((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.GONE);
		    ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);
		    sp_primaryvc.setSelection(0);		    
	}
	private void Show_subsection_info() {
		// TODO Auto-generated method stub
		for(int i=0;i<cf.strarr_module.length;i++)
		{
				Button bt=new Button(this,null,R.attr.n_header_text);
				bt.setTextColor(Color.parseColor("#206E96"));
				bt.setGravity(Gravity.CENTER_VERTICAL);
				bt.setText(cf.strarr_module[i]);
				bt.setId(i);
				cf.set_header_fonts(bt);
				li_ho.addView(bt,LayoutParams.WRAP_CONTENT,40);
				LayoutParams lp =(LayoutParams) bt.getLayoutParams();
				lp.setMargins(5, 0, 5, 0);
				bt.setLayoutParams(lp);
				if(i==0)
				{
					//((TextView)findViewById(R.id.pre_tv_header)).setText(cf.strarr_module[i]);
					bt.setTextColor(getResources().getColor(R.color.n_button_input_page_select_color));
					cur_sel_b=bt;
					current_sub_pos=i;
				}
				if(i+1!=cf.strarr_module.length)
				{
					View v =new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.n_linesperator));
					li_ho.addView(v,1,LayoutParams.FILL_PARENT);
				}
				bt.setOnClickListener(new btn_click_listener(i));
				
		}
	}
	class btn_click_listener implements OnClickListener
	{
		int pos;
		public btn_click_listener(int i)
		{
			pos=i;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			cur_sel_b.setTextColor(getResources().getColor(R.color.n_header_text_color));
			cur_sel_b=(Button) v;
			cur_sel_b.setTextColor(getResources().getColor(R.color.n_button_input_page_select_color));
			current_sub_pos=pos;
			
			if(cur_sel_b.getText().toString().equals("Bathroom"))
			{
				Intent intent = new Intent(LoadSaveVC.this, LoadBathroom.class);
			    Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename",cur_sel_b.getText().toString());	
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("tempoption", gettempoption);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("selectedid", getph);			
				intent.putExtras(b6);
				startActivity(intent);
				finish();
			}
			else if(cur_sel_b.getText().toString().equals("HVAC"))
			{
				Intent intent = new Intent(LoadSaveVC.this, CreateHeating.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename",cur_sel_b.getText().toString());			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("tempoption", gettempoption);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("selectedid", getph);			
				intent.putExtras(b6);
				startActivity(intent);
				finish();
			}
			else
			{

				Intent intent = new Intent(LoadSaveVC.this, LoadSubModules.class);
		    	Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b1 = new Bundle();
				b1.putString("modulename", cur_sel_b.getText().toString());			
				intent.putExtras(b1);
				Bundle b3 = new Bundle();
				b3.putString("selectedid", getph);			
				intent.putExtras(b3);
				startActivity(intent);
				finish();
			}			
		}	
	}
	protected void showcontdialog() {
		// TODO Auto-generated method stub
		str="";
		final Dialog dialog = new Dialog(LoadSaveVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		LinearLayout lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Contractor Type");
		
		if(getconttype.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.conttypedisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.conttypedisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.conttypedisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getconttype;
		}
		
		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);		
		((Button) dialog.findViewById(R.id.addnew)).setVisibility(v.GONE);
		 ch = new CheckBox[cf.contractortype_arr.length];
		for (int i = 0; i < cf.contractortype_arr.length; i++) {
			
			ch[i] = new CheckBox(LoadSaveVC.this);
		    ch[i].setText(cf.contractortype_arr[i]);
			ch[i].setTextColor(Color.BLACK);
			ch[i].setChecked(true);//Need to remove this(9/2/2015)
			lin.addView(ch[i]);
		
		}
		
		if (str != "") {
			cf.setValuetoCheckbox(ch, str);
			String replaceendcommaconttype= replacelastendswithcomma(str);
			((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaconttype));
		}
		
		for (int i = 0; i < cf.contractortype_arr.length; i++) {
			if (ch[i].isChecked()) {
				seltdval += ch[i].getText().toString() + "&#44;";
			}
		}

		getconttype = seltdval;

		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				seltdval = "";
				for (int i = 0; i < cf.contractortype_arr.length; i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
			
				getconttype = seltdval;
				cont_dialog=0;
				((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
				String replaceendcommaconttype = replacelastendswithcomma(getconttype);
				((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaconttype));
				dialog.dismiss();
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cont_dialog=0;dialog.dismiss();	
			}
		});
		
		
		dialog.show();
	}
	private void defaultinsert() {
		// TODO Auto-generated method stub
		try
		{
			 Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+getsubmodulename+"' and fld_srid='"+getph+"' " +
			 		"and fld_module='"+getmodulename+"' and fld_templatename='"+gettempname+"'", null);
			 System.out.println("ddf"+cur.getCount());
			 if(cur.getCount()>0)
	         {
	        	 SetValue();show();secshow();
	         }
	         else
	         {
	        	 
	         Cursor c1=db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
	         System.out.println("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"'"+c1.getCount());
			 if(c1.getCount()>0)
			 {
				 if (c1 != null) {
					 c1.moveToFirst();
						do {
						    String primvalue =db.decode(c1.getString(c1.getColumnIndex("fld_primary")));
						    String secvalue =db.decode(c1.getString(c1.getColumnIndex("fld_secondary")));
						    String commvalue =db.decode(c1.getString(c1.getColumnIndex("fld_comments")));
						    String inspvalue =db.decode(c1.getString(c1.getColumnIndex("fld_inspectionpoint")));
						    String apprcostvalue =db.decode(c1.getString(c1.getColumnIndex("fld_apprrepaircost")));
						    String conttypevalue =db.decode(c1.getString(c1.getColumnIndex("fld_contractortype")));
						    String vcname =db.decode(c1.getString(c1.getColumnIndex("fld_vcname")));
						    
						    if(cur.getCount()==0)
						    {
						    	System.out.println(" INSERT INTO "
										+ db.SaveSetVC
										+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_vcname,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype,fld_printphoto) VALUES"
										+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+getsubmodulename+"','"+gettempname+"','"+db.encode(vcname)+"','"+db.encode(primvalue)+"','"
										+db.encode(secvalue)+"','"+db.encode(commvalue)+"','','','','"+db.encode(inspvalue)+"','"+db.encode(((EditText)findViewById(R.id.approximaterepaircost)).getText().toString())+"','"+db.encode(getconttype)+"')");
						    	db.hi_db.execSQL(" INSERT INTO "
										+ db.SaveSetVC
										+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_templatename,fld_vcname,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype) VALUES"
										+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+getsubmodulename+"','"+gettempname+"','"+db.encode(vcname)+"','"+db.encode(primvalue)+"','"
										+db.encode(secvalue)+"','"+db.encode(commvalue)+"','','','','"+db.encode(inspvalue)+"','"+db.encode(apprcostvalue)+"','"+db.encode(conttypevalue)+"')");
						    }
						    else
						    {
						    	System.out.println("UPDATE "+db.SaveSetVC+ " SET fld_secondary='"+db.encode(secvalue)+"',"
				            			+"fld_comments='"+db.encode(commvalue)+"',"
				            			+"fld_inspectionpoint='"+db.encode(inspvalue)+"'," +
				            					"fld_apprrepaircost='"+db.encode(apprcostvalue)+"',fld_contractortype='"+db.encode(conttypevalue)+"' " +
				            							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_primary='"+db.encode(getprimaryvc)+"'");
						    	db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_secondary='"+db.encode(secvalue)+"',"
				            			+"fld_comments='"+db.encode(commvalue)+"',"
				            			+"fld_inspectionpoint='"+db.encode(inspvalue)+"'," +
				            					"fld_apprrepaircost='"+db.encode(apprcostvalue)+"',fld_contractortype='"+db.encode(conttypevalue)+"' " +
				            							"WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_primary='"+db.encode(getprimaryvc)+"'");
						    }
						    	
						}while(c1.moveToNext());
						SetValue();show();secshow();conttypeshow();
				 }
			 }
	        }
		}catch (Exception e) {
			// TODO: handle exception
		}
			
	}
	protected void dbsecvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		
		for(int j=0;j<secondaryyarr.length;j++)
		{
			 str_arrlst.add(secondaryyarr[j]);
	
		}		
	}

	protected void showsec_alert() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(LoadSaveVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Secondary Condition");
		
		secshow();
		
		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);
		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				String seltdval = "";
				for (int i = 0; i < str_arrlst.size(); i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
				
				String finalselstrval = "";
				finalselstrval = seltdval;
				
				if(finalselstrval.equals(""))
				{
					cf.DisplayToast("Please check at least any 1 option to save");
				}
				else
				{			
					getsecondaryvc = seltdval;
					secshow();
					dialog.dismiss();sec_dialog=0;
				}
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sec_dialog=0;dialog.dismiss();	
			}
		});
		
		Button btnother = (Button) dialog.findViewById(R.id.addnew);
		btnother.setVisibility(v.GONE);
		
		
		dialog.show();
	}

	private void secshow() {
		// TODO Auto-generated method stub
		if(getsecondaryvc.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.secconddisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.secconddisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.secconddisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getsecondaryvc;
		}
		
		ch = new CheckBox[str_arrlst.size()];
		for (int i = 0; i < str_arrlst.size(); i++) {

			ch[i] = new CheckBox(LoadSaveVC.this);
			ch[i].setText(str_arrlst.get(i));
			ch[i].setTextColor(Color.BLACK);
			ch[i].setChecked(true);//Need to remove this(9/2/2015)
			lin.addView(ch[i]);
		}	
		if (str != "") 
		{			
			cf.setValuetoCheckbox(ch, str);
			((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);
			String replaceendcommaseccond= replacelastendswithcomma(str);
			((TextView) findViewById(R.id.secconddisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaseccond));
		}
		else
		{
			((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);
		}
	}
	
	private String replacelastendswithcomma(String realword) {
		// TODO Auto-generated method stub
		
		if(realword.contains("&#44;"))
		{
			realword = realword.replace("&#44;",",");
		}
		if(realword.endsWith(","))
		{
			realword = realword.substring(0,realword.length()-1);
		}
		else
		{
			realword = realword;	
		}	
		return realword;
	}
	private void conttypeshow() {
		// TODO Auto-generated method stub
		
		if(getconttype.trim().equals(""))
		{
			str=str;
		}
		else
		{
			str=getconttype;
		}
		
		ch = new CheckBox[str_arrlst.size()];
		for (int i = 0; i < str_arrlst.size(); i++) {

			ch[i] = new CheckBox(LoadSaveVC.this);
			ch[i].setText(str_arrlst.get(i));
			ch[i].setTextColor(Color.BLACK);
			ch[i].setChecked(true);//Need to remove this(9/2/2015)
			lin.addView(ch[i]);
		}		
		

		if (str != "") {
			cf.setValuetoCheckbox(ch, str);
			String replaceendcommaconttype = replacelastendswithcomma(str);
			((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaconttype));
		}
	}

	@Override
    protected void onSaveInstanceState(Bundle outState) {
	
        outState.putString("pc_index", getprimaryvc);
        outState.putString("sc_index", getsecondaryvc);
        outState.putString("comments_index", etcomments.getText().toString());
     
        outState.putInt("insp_index", insp_dialog);
        if(insp_dialog==1)
        {
        	selectvalinsppoint="";
        	for (int i = 0; i < str_arrlst.size(); i++) {
				if (ch[i].isChecked()) {
					selectvalinsppoint += ch[i].getText().toString() + "&#44;";
				}
			}
        	outState.putString("spinspoption_index", selectvalinsppoint);
        }
        else
        {
        	outState.putString("spinspoption_index", getinspectionpoint);
        }


        outState.putInt("sec_index", sec_dialog);
        if(sec_dialog==1)
        {
        	selectvalseccond="";
        	for (int i = 0; i < str_arrlst.size(); i++) {
				if (ch[i].isChecked()) {
					selectvalseccond += ch[i].getText().toString() + "&#44;";
				}
			}
        	outState.putString("spoption_index", selectvalseccond);
        }
        else
        {
        	outState.putString("spoption_index", getsecondaryvc);
        }
        
        outState.putInt("cont_index", cont_dialog);
        if(cont_dialog==1)
        {
        	selectvalcont="";
        	for (int i = 0; i < str_arrlst.size(); i++) {
				if (ch[i].isChecked()) {
					selectvalcont += ch[i].getText().toString() + "&#44;";
				}
			}
        	outState.putString("contoption_index", selectvalcont);
        }
        else
        {
        	outState.putString("contoption_index", getconttype);
        }
        
        outState.putInt("other_index", dialog_other);
        if(dialog_other==1)
        {
           outState.putString("othertext_index", etother.getText().toString());
        }
        
        outState.putString("get_sectext", ((TextView) findViewById(R.id.secconddisplay)).getText().toString());
        outState.putString("get_insptext", ((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString());
        outState.putString("get_conttext", ((TextView) findViewById(R.id.conttypedisplay)).getText().toString());
        
        super.onSaveInstanceState(outState);
 }
  @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
	   getprimaryvc = savedInstanceState.getString("pc_index");
	   getsecondaryvc = savedInstanceState.getString("sc_index");
	   strcomments = savedInstanceState.getString("comments_index");
      
	   insp_dialog = savedInstanceState.getInt("insp_index");
	   getinspectionpoint = savedInstanceState.getString("spinspoption_index");
	   if(insp_dialog==1)
	   {
		   
		   showalert();
		  
		   if(!getinspectionpoint.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, getinspectionpoint);
		   }
	   }
	   
	   sec_dialog = savedInstanceState.getInt("sec_index");
	   get_scon = savedInstanceState.getString("spoption_index");
	   if(sec_dialog==1)
	   {
		   showsec_alert();
		   
		   if(!get_scon.equals(""))
		   {				
		    	  cf.setValuetoCheckbox(ch, get_scon);
		   }
	   }
	   
	   
	   cont_dialog = savedInstanceState.getInt("cont_index");
	   getconttype= savedInstanceState.getString("contoption_index");
	   if(cont_dialog==1)
	   {			  
		   showcontdialog();
	   }
	   
	   
	   
	   getsectext =  savedInstanceState.getString("get_sectext");
	   if(!getsectext.equals(""))
	   {
		   
		   ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);
		   ((TextView) findViewById(R.id.secconddisplay)).setText(getsectext);
	   }
	   
	   getinsptext =  savedInstanceState.getString("get_insptext");
	   if(!getinsptext.equals(""))
	   {
		
		   ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
		   ((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(getinsptext);
	   }
	   
	   getconttext =  savedInstanceState.getString("get_conttext");
	   if(!getconttext.equals(""))
	   {
		   ((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
		   ((TextView) findViewById(R.id.conttypedisplay)).setText(getconttext);
	   }
	   
	   
	   dialog_other = savedInstanceState.getInt("other_index");
	   if(dialog_other==1)
	   {
		   dialogshow();
		   strother = savedInstanceState.getString("othertext_index");
		   etother.setText(strother);
		 
	   }
	   super.onRestoreInstanceState(savedInstanceState);
    }
	protected void checkboxvalidation() {
		// TODO Auto-generated method stub
		/*String strchkval = cf.getvaluefromchk(cb);
		 
		 if(strchkval.equals(""))
		 {
			 cf.DisplayToast("Please select at least any one checkbox option");
		 }
		 else
		 {*/
			 if(getinspectionpoint.equals(""))
			 {
				 cf.DisplayToast("Please select Inspection Point");
			 }
			 else
			 {
				 strcomments = etcomments.getText().toString();
				
				 try
					{
						
		            	Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+gettempname+"' and fld_primary='"+db.encode(getprimaryvc)+"'", null);
			            int cnt = cur.getCount();
			            if(cnt==0)
			            {
			            	Cursor cur1 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_VCName='"+db.encode(getvc)+"'", null);
							System.out.println("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+db.encode(getsubmodulename)+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_VCName='"+db.encode(getvc)+"'"+cur1.getCount());
			            	if(cur1.getCount()>0)
			            	{
			            		cur1.moveToFirst();System.out.println("TTT");
			            		editvcid = cur1.getString(cur1.getColumnIndex("fld_sendtoserver"));System.out.println("edi="+editvcid);
			            	}   
			            	db.hi_db.execSQL("INSERT INTO "
									+ db.SaveSetVC
									+ " (fld_inspectorid,fld_ed_id,fld_srid,fld_module,fld_submodule,fld_templatename,fld_vcname,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint,fld_apprrepaircost,fld_contractortype,fld_printphoto) VALUES"
									+ "('"+db.UserId+"','"+editvcid+"','"+getph+"','"+getmodulename+"','"+getsubmodulename+"','"+gettempname+"','"+db.encode(getvc)+"','"+db.encode(getprimaryvc)+"','"
									+db.encode(getsecondaryvc)+"','"+db.encode(strcomments)+"','','','','"+db.encode(getinspectionpoint)+"','"+db.encode(((EditText)findViewById(R.id.approximaterepaircost)).getText().toString())+"','"+db.encode(getconttype)+"','"+str_pp+"')");
			            }			            
			            else
			            {
			            	   	db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_secondary='"+db.encode(getsecondaryvc)+"',"
			            			+"fld_comments='"+db.encode(etcomments.getText().toString())+"',fld_printphoto='"+str_pp+"',"
			            			+"fld_inspectionpoint='"+db.encode(getinspectionpoint)+"',fld_apprrepaircost='"+db.encode(((EditText)findViewById(R.id.approximaterepaircost)).getText().toString())+"',fld_contractortype='"+db.encode(getconttype)+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+gettempname+"' and fld_primary='"+db.encode(getprimaryvc)+"'");
					   }
					    cf.DisplayToast("Visible Condition saved successfully");
					    
					    
						Intent intent = new Intent(LoadSaveVC.this, LoadNewSetVC.class);
						Bundle b2 = new Bundle();
						b2.putString("templatename", gettempname);			
						intent.putExtras(b2);
						Bundle b3 = new Bundle();
						b3.putString("currentview", "start");			
						intent.putExtras(b3);
						Bundle b4 = new Bundle();
						b4.putString("modulename", getmodulename);			
						intent.putExtras(b4);
						Bundle b5 = new Bundle();
						b5.putString("submodulename", getsubmodulename);			
						intent.putExtras(b5);
						Bundle b6 = new Bundle();
						b6.putString("selectedid", getph);			
						intent.putExtras(b6);
						startActivity(intent);
						finish();
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("erroe="+e.getMessage());
					}
			 }
		// }
	}
	private void clear() {
		// TODO Auto-generated method stub
		 getinspectionpoint="";getconttype="";getsecondaryvc="";
		 ((EditText) findViewById(R.id.approximaterepaircost)).setText("");	
		 etcomments.setText("");str="";
         ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.GONE);
		 ((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.GONE);	
    }
	private void SetValue() {
		// TODO Auto-generated method stub
		try
		{
			//scur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_primary='"+db.encode(getvc)+"' and fld_templatename='"+gettempname+"'", null);
			/*scur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_ed_id='"+geteditid+"' and fld_templatename='"+gettempname+"'", null);
			System.out.println("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_ed_id='"+geteditid+"' and fld_templatename='"+gettempname+"'"+scur.getCount());*/
			scur = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_vcname='"+db.encode(getvc)+"' and fld_templatename='"+gettempname+"'", null);
			int cnt=scur.getCount();
			System.out.println("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_vcname='"+db.encode(getvc)+"' and fld_templatename='"+gettempname+"'"+scur.getCount());
			if(cnt==0)
			{
				Cursor c = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"'and fld_primary='"+db.encode(getvc)+"' and fld_templatename='"+gettempname+"'", null);
				System.out.println("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"'and fld_primary='"+db.encode(getvc)+"' and fld_templatename='"+gettempname+"'"+c.getCount());
				int scnt = scur.getCount();
				if(scnt==0)
				{
					Cursor cur = db.hi_db.rawQuery("select * from " + db.AddVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"'  and fld_primary='"+db.encode(getvc)+"'", null);
					System.out.println("select * from " + db.AddVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"'  and fld_primary='"+db.encode(getvc)+"'"+cur.getCount());
					int cnt1 = cur.getCount();
					if(cnt1>0)
					{
						GetLoad(cur);
						
					}
				
				}
				else
				{
					GetLoad(c);
				}
				
			}
			else
			{
				GetLoad(scur);
			}
		
	  }
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void GetLoad(Cursor cur) {
		// TODO Auto-generated method stub		
		cur.moveToFirst();
    	String get_pcon = db.decode(cur.getString(cur.getColumnIndex("fld_primary")));
    	int pc = primaryadap.getPosition(get_pcon);
    	sp_primaryvc.setSelection(pc);    	
    	getsecondaryvc = db.decode(cur.getString(cur.getColumnIndex("fld_secondary")));
    	if(!getsecondaryvc.equals(""))
    	{
    		String replaceendcommaseccond= replacelastendswithcomma(getsecondaryvc);
    		((TextView) findViewById(R.id.secconddisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.secconddisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaseccond));
    	}
    	
    	String getcomments = db.decode(cur.getString(cur.getColumnIndex("fld_comments")));
    	etcomments.setText(getcomments);
    	
    	getinspectionpoint = db.decode(cur.getString(cur.getColumnIndex("fld_inspectionpoint")));
    	if(!getinspectionpoint.equals(""))
    	{
    		String replaceendcommaionspoint= replacelastendswithcomma(getinspectionpoint);
    		((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
    	}
    	
    	
    	getconttype = db.decode(cur.getString(cur.getColumnIndex("fld_contractortype")));
    	if(!getconttype.equals(""))
    	{
    		String replaceendcommaconttype= replacelastendswithcomma(getconttype);
	    	((TextView) findViewById(R.id.conttypedisplay)).setVisibility(v.VISIBLE);
			((TextView) findViewById(R.id.conttypedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaconttype));
    	}    	
    	String pp = cur.getString(cur.getColumnIndex("fld_printphoto"));
    	 ((RadioButton) rdpp.findViewWithTag(pp)).setChecked(true);
    	
    	String getapprcost = db.decode(cur.getString(cur.getColumnIndex("fld_apprrepaircost")));
    	((EditText)findViewById(R.id.approximaterepaircost)).setText(getapprcost);
	}
	protected void showalert() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(LoadSaveVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Inspection Point");
		
		show();
		
		((Button)dialog.findViewById(R.id.setvc)).setVisibility(v.GONE);
		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval = "";
				for (int i = 0; i < str_arrlst.size(); i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
			
				getinspectionpoint = seltdval;
				show();
				dialog.dismiss();insp_dialog=0;
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				insp_dialog=0;dialog.dismiss();	
			}
		});
		
		Button btnother = (Button) dialog.findViewById(R.id.addnew);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_other=1;
				dialogshow();
			}
		});
		
		dialog.show();
	}
	protected void dialogshow() {
		// TODO Auto-generated method stub
		cf.hideskeyboard();
		final Dialog add_dialog = new Dialog(LoadSaveVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.addnewcustom);
		
		etother = (EditText) add_dialog.findViewById(R.id.et_addnew);
		cf.hidekeyboard(etother);
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		strother = etother.getText().toString();
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//CreateSubModule.sp=0;
				dialog_other=0;
				add_dialog.dismiss();
			}
		});
		
		Button btnsave = (Button) add_dialog.findViewById(R.id.save);
		btnsave.setOnClickListener(new OnClickListener() {
            String seltdval="";
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strother = etother.getText().toString();
				if (etother.getText().toString().trim().equals("")) {
					cf.DisplayToast("Please enter the other text");
					
				} else {
					
					if(str_arrlst.contains(strother))
					{
						cf.DisplayToast("Already exists!! Please enter the new text");
						etother.setText("");
						etother.requestFocus();
					}
					else
					{
						try
						{
							db.hi_db.execSQL(" INSERT INTO "
									+ db.SaveAddOtherIP
									+ " (fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_other,fld_templatename) VALUES"
									+ "('"+db.UserId+"','"+getph+"','"+getmodulename+"','"+getsubmodulename+"','"
									+ db.encode(etother.getText().toString()) + "','"+gettempname+"')");
							
							String addedval = db.encode(etother.getText().toString());
							str_arrlst.add(addedval);
							dialog_other=0;
							cf.DisplayToast("Added successfully");
							add_dialog.cancel();
							
							lin.removeAllViews();
							dbvalue();
							show();
							
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		});
		
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	private void dbvalue() {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		cf.GetCurrentModuleName(getmodulename);		
		
		for(int j=0;j<cf.insp_arr.length;j++)
		{
			 str_arrlst.add(cf.insp_arr[j]);
	
		}		
		
		try
		{
			 Cursor c=db.hi_db.rawQuery("select * from " + db.SaveAddOtherIP + " WHERE fld_inspectorid='"+db.UserId+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+gettempname+"' and fld_srid='"+getph+"'",null);
			 if(c.getCount()>0)
			 {
				 c.moveToFirst();
				 for(int i =0;i<c.getCount();i++,c.moveToNext())
				 {
					 str_arrlst.add(db.decode(c.getString(c.getColumnIndex("fld_other"))));
				 }
			 }
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void show() {
		// TODO Auto-generated method stub
		if(getinspectionpoint.trim().equals(""))
		{
			if(((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString().equals("Selected : "))
			{
				str="";	
			}
			else
			{
				if(((TextView) findViewById(R.id.inspectionvaluedisplay)).getVisibility()==View.GONE)
				{
					str="";
				}
				else
				{
					str = 	((TextView) findViewById(R.id.inspectionvaluedisplay)).getText().toString().replace("Selected : ","");
					str = str.replace(",","&#44;");
				}
			}
		}
		else
		{
			str=getinspectionpoint;
		}
		
	
		ch = new CheckBox[str_arrlst.size()];
		for (int i = 0; i < str_arrlst.size(); i++) {

			ch[i] = new CheckBox(LoadSaveVC.this);
			ch[i].setText(str_arrlst.get(i));
			ch[i].setTextColor(Color.BLACK);
			ch[i].setChecked(true);//Need to remove this(9/2/2015)
			lin.addView(ch[i]);
		}		
		
		

		if (str != "") {
			System.out.println("camer her");
			 cf.setValuetoCheckbox(ch, str);System.out.println("endss");
			 String replaceendcommaionspoint= replacelastendswithcomma(str);System.out.println("DDDDD");
 			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.VISIBLE);
			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setText(Html.fromHtml("<b>Selected : </b>"+replaceendcommaionspoint));
		}
		else
		{
			 ((TextView) findViewById(R.id.inspectionvaluedisplay)).setVisibility(v.GONE);
		}
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		int j;
		public MyOnItemSelectedListenerdata(int i) {
			// TODO Auto-generated constructor stub
			j=i;
		}

		public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
		  
			if(j==1)
			{
				getprimaryvc = parent.getItemAtPosition(pos).toString();
			}
			else
			{
				getsecondaryvc = parent.getItemAtPosition(pos).toString();
			}
	   }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
 		try
 		{
 			if(requestCode==34)
 			{
 				load_comment=true;
 				if(resultCode==RESULT_OK)
 				{
 					String comm = ((EditText)findViewById(R.id.comments)).getText().toString();
 					((EditText)findViewById(R.id.comments)).setText(comm + data.getExtras().getString("Comments"));
	 			}
 			}
 		}
 		catch (Exception e) {
 			// TODO: handle exception
 			System.out.println("the erro was "+e.getMessage());
 		}
 	}/**on activity result for the load comments ends**/
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	private void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(LoadSaveVC.this, LoadNewSetVC.class);
		Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		Bundle b7 = new Bundle();
		b7.putString("selectedid", getph);			
		intent.putExtras(b7);
		Bundle b6 = new Bundle();
		b6.putString("submodulename", getsubmodulename);			
		intent.putExtras(b6);
		startActivity(intent);
		finish();
		
		
	}
}
