package idsoft.idepot.homeinspection;

import java.util.ArrayList;

import idsoft.idepot.homeinspection.CreateHeating.myspinselectedlistener;
import idsoft.idepot.homeinspection.LoadBathroom.CHListener;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;

public class NewHVAC extends Activity {
	String gettempname="",getcurrentview="",getmodulename="",gettempoption="",getselectedho="",fueltankvalue="",finalselstrval="",tempfueltankloc="",getnoofzones="",getheatbrand="",
			seltdval="",tempfluetype="",temptestedfor="",tempairhandlerfeat="",tempthermolocation="",tempthermotype="",tempductworktype="",tempconddischarge="",getzoneno="",
					getfurnace="",getmotorblower="",getsupplyregister="",getreturnregister="",getmultizonesystem="",str="",fluetypevalue="",hvacvalue="",
							conddischangevalue="",thermolocationvalue="",thermostattypevalue="",ductworktypevalue="",airhandlerfeatvalue="",testedforvalue="";
	ScrollView scr;
	TableLayout heatingtblshow;
	Spinner spinloadtemplate;
	View v;
	LinearLayout lin;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	TextView fueltanklocnote,fluetypenote,testedfornote,thermostatlocationtv,airhandlerfeatnote,ductworknote,thermostattypetv,conddisctv;
	int hvac=0,hvacCurrent_select_id,editid=0,getisedit=0;
	static int k=0;
	TextView fueltanktv,fluetypetv,testedfortv,airhandlertv,ductworktypetv,thermolocationtv,thermotypetv,condensdischargetv;
	CheckBox ch[];
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	Spinner sp_heatingtype,sp_energysource,sp_gasshutoff,spin_noofzones,spin_heatingage,spin_zoneno,sp_zonename,sp_heatbrand,sp_furnace,
	sp_motorblower,sp_heatdelivery,sp_zonestestedfor,spin_supplyregister,sp_returnregister,sp_ductworktype,sp_multizonesystem,sp_zonetonnage,sp_btu,
	sp_thermotype,sp_thermolocation,sp_externalunitreplprob,sp_internalunitreplprob;
	String getheatdeliverytype="",getspinselectedname="",getenergysource="",strenergysource="",getheatingtype="",getheatingage="",strotherheatingage="",strotherheattype="",getgasshutoff="",strgasshutoff="",strfluetype="",
			getfluetype="",getzonename="",strzonename="",getzonetonnage="",strzonetonnage="",getbtu="",strbtu="",strexternalunit="",strinternalunit="",getinternalunit="",getexternalunit="";
	EditText ed_otherbtu,ed_otherzonetonnage,ed_otherheattype,ed_otherenergysource,ed_othergasshutoff,ed_otherfluetype,ed_otherheatingage,ed_otherzonename;
	
	static String[] arrfueltank={"Above Ground","Below Ground","Not Applicable","Above Ground Oil Tank","Below Ground Oil Tank","Original Oil Tank",
		"Oil Tank Fill Cap and Vent Not Located","Tank Fill and Vent Located","Portable Gas Tank","Gas Tank Below Ground","Underground Gas Tank Fill Valve Not Located",
		"Underground Gas Tank Not Located","Underground Gas Tank"},
					arrfluetype={"Unlined Masonry Chimney","Old Masonry Chimney Redundant","PVC Schedule 40","No Flue Cap","Metal Flue Cap","Flexible Metal Gas Flue","Masonry Chimney",
				"Clay Flue Liner","Metal Flue"},
		arrductworktype={"Flexible Ductwork","Metal Ductwork","Slab Ductwork","Insulated","Ductwork in Crawl SPace","Automatic Dampers(not tested)","Untidy Ductwork Installation Leakage Evident"},
		arrzonetestingfor={"Tested For Cooling","Tested For Heating","Tested For Emergency Heat Only","Reverse valve not tested", "Outside temperature prohibited test"},
				arrthermolocation={"Hall", "Master Bedroom","Living Room", "Upstairs","Bonus", "Other"},
				arrthermotype={"Manual Thermostat","Programmable Thermostat","Set Back Thermostat","Heat Pump Thermostat","Multiple Zone","Separate Heating/Cooling Thermostat","Aquastat"},
				arrcondendischarge={"Overflow Condensate Tray","Condensate Pipe To Sump","Condensate Pipe","Condensate Pipe To Exterior","Float Switch/Pump (not tested)"},
		arrairhandlerfeatures={"Air Filter","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner","Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light","Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Condensation Discharger"};
	
	String[] arrheatingnum={"--Select--","1","2","3","4","5","6","Other"},strheatsytem ={"--Select--","Adequate","Inadequate","Appears Functional","Recommend Replacement"},
			arrheattype={"--Select--","Forced Air","Heat Pump Forced Air","Central Air - Cool/Air Conditioning Only","Packaged Central Air - Heat pump","Packaged Central Air - Cool/Air Conditioning Only","Wall Unit(s)","Heat Strip","None",
			"Electric Base","Electric Heat","Radiant Floor","Space Heater","Steam Boiler","Circulating Boiler","Coal Furnace Converted to Gas","Coal Furnace converted to Oil",
			"Hydroninc System","Oil Furnace converted to Gas","Old Oil Furnace as Back up to heat pump","Radiant  Ceiling","Window Unit","Swamp Cooler","Water to Air system",
			"Cooling Only System","Uses Heat Ductwork","Refrigerant System","System Aged/Operable","Other"},
			arrgasshutoff={"--Select--","Front Side","Left Side","Right Side","Rear side","Adjacent Tank","Adjacent meter","Other"},
			
			
			arrzonename={"--Select--","Bonus Room","Main Area","Master Bedroom","Second Floor","Other"},
			arrcoolingage={"--Select--","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","Other"},
			arrheatexchanger={"--Select--","1 Burner","2 Burner","3 Burner",
			"4 Burner","5 Burner","6 Burner","Other"},arrdistribution={"--Select--","Hot Water","Steam","Radiator","Metal Duct","One Pipe","Two Pipe","Other"},
			arrcirculator={"--Select--","Pump","Gravity","Other"},arrdrain={"--Select--","Manual","Automatic","Other"},arrfuse={"--Select--","Single Wall","Double Wall","PVC"},
			arrdevious={"--Select--","Temp Guage","Expansion Tank","Pressure Gauge","Other"},arrhumidifier={"--Select--","Aprilaire","Honeywell","Other"},arrthermostats={"--Select--","Individual",
			"Multi Zone","Programmable","Other"},
			arrasbestos={"--Select--","Yes","No"},arrheatingnumber,arrheatingnumother,arrheatingsysteme ,arrmanufacture,arrmodelnumber,
			arrenergy={"--Select--","Oil","Gas","Electric","Water","Liquid Propane","Kerosene","Solar","Wood","Coal","Corn","Natural Gas","Diesel","Other"},
					arrheatbrand={"--Select--","Not Determined", "Not Visible","Aged","Amana","American Standard","Americanaire","Ao Smith","Apollo","Aquatherm","Arcoaire","Armstrong",
			"Bryant","Burnham","Carrier","Climate Master","Coleman","Comfort Maker","Crown","Empire","Envirotemp","Frigidaire","Goodman",
			"Hallmark","Heatwave","Heil","Int.Comfort","Intertherm","Janitrol","Kenmore","Lennox","Luxaire","Magic Chef","Monitor","Ninety Two",
			"None","Oneida","Patriot","Payne","Peerless","Presidential","Quattro","Rheem","Rinnai","Ruud","Sears","Singer","Tempstar","Thermopride",
			"Toyostove","Trane","Unknown","Weatherking","Weil Mclean","York","Zephyr"},
			arrheatdelivery={"--Select--","Forced Air","Refrigerant","Electric Resistance Heat","Hot Water","Steam Heat","Radiant","Boiler"},
			arrwatersystem={"--Select--","One Pipe System","Two Pipe System","Fin Coils","Boiler","Subfloor","Convectors","Radiators","Baseboard"},
			arrradiators={"--Select--","Cast Iron Radiators","Steel Radiators","Manual Fill","Water Level Sight Glass","Bleed Valves"},
			arrmotorblower={"--Select--","Blower Fan","Direct Drive","Belt Driven"},
			arrwoodstoves={"--Select--","1","2","3","4","5","None","Other"},
			arrsupplyregister={"--Select--","Low","High","Low & High"},
					arrreturnregister={"--Select--","Low","High"},
					arrexternalunitrepprob={"--Select--","Low","Medium","High","Other"},
					arrzonetonnage={"--Select--",	"1.5 Ton","2 Ton","2.5 Ton","3 Ton","3.5 Ton","4 Ton","4.5 Ton","5 Ton","Other"},
							arrbtu={"--Select--","Not Determined","12000 BTU's","18000 BTU's","24000 BTU's","30000 BTU's","36000 BTU's","42000 BTU's","48000 BTU's","54000 BTU's","Other"},
			
			arrretunrregister={"--Select--","Low","High"},
				arrnoofzones={"--Select--","1","2","3","4","5"},
			arrmultizonesystem={"--Select--","Yes", "No", "Auto Damper Not Tested"},
			
			arrfurnacefeatures={"--Select--","Inspection Door","Steel Heat Exchanger","Heat Exchanger","Cast Iron Heat Exchanger","Metal Heat Exchanger","Oil Burner","Single Pipe Oil Burner"," Mono Port","Dual Pipe Oil Burner","Fuel Oil Filter","Manual Pilot Light"," Electronic Pilot Light","Barometric Damper","High Limit Switch","Gas Valve","Drip Leg On Pipe","Thermocouple","Humidifier","Electronic Air Cleaner","Air Filter"},
			arrfurnace={"--Select--","Air To Air Heat Pump","Water To Air Heat Pump","Forced Air Furnace","Induced Air Furnace","Gravity Furnace","Evaporated Located Over Furnace","Natural Draft System","Condensation Furnace System","Inside panels sealed(no coil access)"};
	ArrayAdapter externalunitrepadap,internalunitrepadap,thermolocationadap,thermotypeadap,zonetonnadap,btuarrayadap,multizonearraydap,ductworktypearraydap,zonetestedforadap,zonenameadap,zonenoarrayadap,fluetypearrayadap,noofzonesarrayadap,gasshutoffarrayadap,heatnumarrayadap,heatsystemarrayadap,heattypearrayadap,heatexchangerarrayadap,distributionarrayadap,circulatorarrayadap,drainarrayadap,fusearrayadap,deviousarrayadap,
	watersysarrayadap,returnregarraydap,supplyregarraydap,furnfeatarraydap, humidifierarrayadap,thermostatsarrayadap,fueltankarrayadap,asbestosarrayadap,energysourcearrayadap,agearrayadap,brandarrayadap,furnacearrayadap,heatdeliveryarrayadap,radiatorsarrayadap,motorarrayadap,woodstoveadap;
	EditText et_otherheatingnum,et_manufacturer,et_modelnumber,et_serialnumber,et_capacity,et_otherheatexchanger,et_otherdistribution,et_othercirculator,
	         et_otherdrain,et_otherdevious,et_otherhumidifier,et_otherthermostats,et_otherfueltank;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		Bundle b4 = this.getIntent().getExtras();
		if(b4!=null)
		{
			getselectedho = b4.getString("selectedid");
		}
		
		if(gettempname.equals("--Select--"))
		{
			gettempname="N/A";
		}		
		
		setContentView(R.layout.activity_newhvac);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(20);
		db.CreateTable(21);
		db.CreateTable(22);
		db.CreateTable(23);
		db.CreateTable(24);
		db.CreateTable(25);
		db.CreateTable(38);
		db.userid();
		
		Button btnsubmit = (Button)findViewById(R.id.btn_submit);
		spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		if(getcurrentview.equals("create"))
		{
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.GONE);	
			spinloadtemplate.setVisibility(v.GONE);
			((TextView)findViewById(R.id.seltemplate)).setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
			btnsubmit.setText("Submit Template");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  Please tap on the Submit Template above in order to not to lose your data. An internet connection is required.");
		}
		else if(getcurrentview.equals("start"))
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		else 
		{
			spinloadtemplate.setVisibility(v.VISIBLE);
			((Button)findViewById(R.id.btn_takephoto)).setVisibility(v.VISIBLE);
			btnsubmit.setText("Submit Section");
			((TextView)findViewById(R.id.notetxt)).setText("Note :  All data temporarily stored on this device. Please submit your inspection as soon as possible to prevent data loss. An internet connection is required. Data can be submitted partially by using submit button above or in full using final submit feature. Once entire report submitted, the inspection report will be created and available to you.");
		}
		
		
		sp_heatingtype = (Spinner)findViewById(R.id.spin_heattype);
		heattypearrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrheattype);
		heattypearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_heatingtype.setAdapter(heattypearrayadap);
		sp_heatingtype.setOnItemSelectedListener(new myspinselectedlistener(3));
		ed_otherheattype= (EditText)findViewById(R.id.other_heatingtype);
		
		sp_energysource = (Spinner)findViewById(R.id.spin_energysource);
		energysourcearrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrenergy);
		energysourcearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_energysource.setAdapter(energysourcearrayadap);
		sp_energysource.setOnItemSelectedListener(new myspinselectedlistener(2));
		ed_otherenergysource= (EditText)findViewById(R.id.other_energysource);
		
		fueltanktv = (TextView)findViewById(R.id.spin_fueltanklocation);
		fueltanktv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("FUEL TANK"+fueltankvalue);
				hvacvalue="Fuel Tank Location";k=1;
				multi_select(hvacvalue,k);				
			}
		});		
		sp_gasshutoff = (Spinner)findViewById(R.id.spin_gasshutoff);
		gasshutoffarrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrgasshutoff);
		gasshutoffarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_gasshutoff.setAdapter(gasshutoffarrayadap);
		sp_gasshutoff.setOnItemSelectedListener(new myspinselectedlistener(13));
		ed_othergasshutoff= (EditText)findViewById(R.id.other_gasshutoff);
		
		fluetypetv = (TextView)findViewById(R.id.spin_fluetype);
		fluetypetv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				hvacvalue="Flue Type";k=2;
				multi_select(hvacvalue,k);				
			}
		});
		/*fluetypearrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrfluetype);
		fluetypearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_fluetype.setAdapter(fluetypearrayadap);
		spin_fluetype.setOnItemSelectedListener(new myspinselectedlistener(14));*/
		
		
		spin_noofzones = (Spinner)findViewById(R.id.spin_noofzones);
		noofzonesarrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrnoofzones);
		noofzonesarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_noofzones.setAdapter(noofzonesarrayadap);
		spin_noofzones.setOnItemSelectedListener(new myspinselectedlistener(15));
		
		spin_zoneno = (Spinner)findViewById(R.id.spin_zoneno);
		zonenoarrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrnoofzones);
		zonenoarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_zoneno.setAdapter(zonenoarrayadap);
		spin_zoneno.setOnItemSelectedListener(new myspinselectedlistener(16));
		
		spin_heatingage = (Spinner)findViewById(R.id.spin_heatingage);
		agearrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrcoolingage);
		agearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_heatingage.setAdapter(agearrayadap);
		spin_heatingage.setOnItemSelectedListener(new myspinselectedlistener(5));
		ed_otherheatingage= (EditText)findViewById(R.id.other_heatingage);
		
		sp_zonename = (Spinner)findViewById(R.id.spin_zonename);
		zonenameadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrzonename);
		zonenameadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonename.setAdapter(zonenameadap);
		sp_zonename.setOnItemSelectedListener(new myspinselectedlistener(25));
		ed_otherzonename= (EditText)findViewById(R.id.other_zonename);
		
		sp_heatbrand = (Spinner)findViewById(R.id.spin_heatbrand);
		brandarrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrheatbrand);
		brandarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		sp_heatbrand.setAdapter(brandarrayadap);
		sp_heatbrand.setOnItemSelectedListener(new myspinselectedlistener(6));
		
		sp_furnace = (Spinner)findViewById(R.id.spin_furnace);
		furnacearrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrfurnace);
		furnacearrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		sp_furnace.setAdapter(furnacearrayadap);
		sp_furnace.setOnItemSelectedListener(new myspinselectedlistener(7));
		
		sp_heatdelivery = (Spinner)findViewById(R.id.spin_heatdelivery);
		heatdeliveryarrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrheatdelivery);
		heatdeliveryarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_heatdelivery.setAdapter(heatdeliveryarrayadap);
		sp_heatdelivery.setOnItemSelectedListener(new myspinselectedlistener(1));
		
		
		sp_motorblower = (Spinner)findViewById(R.id.spin_motorblowers);
		motorarrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrmotorblower);
		motorarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_motorblower.setAdapter(motorarrayadap);
		sp_motorblower.setOnItemSelectedListener(new myspinselectedlistener(10));
		
		condensdischargetv = (TextView)findViewById(R.id.spin_condensdischarge);
		condensdischargetv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				hvacvalue="Condensation Discharge";k=8;
				multi_select(hvacvalue,k);				
			}
		});
		
		
		testedfortv = (TextView)findViewById(R.id.spin_zonestestedfor);
		testedfortv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				hvacvalue="Tested For";k=3;
				multi_select(hvacvalue,k);				
			}
		});
	/*	zonetestedforadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrzonetestingfor);
		zonetestedforadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonestestedfor.setAdapter(zonetestedforadap);
		sp_zonestestedfor.setOnItemSelectedListener(new myspinselectedlistener(28));
	*/	
		
		airhandlertv = (TextView)findViewById(R.id.spin_airhandlerfeatures);
		airhandlertv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				hvacvalue="Air Handler Features";k=4;
				multi_select(hvacvalue,k);				
			}
		});
		/*furnfeatarraydap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrairhandlerfeatures);
		furnfeatarraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_airhandlerfeatures.setAdapter(furnfeatarraydap);*/
		//spin_airhandlerfeatures.setOnItemSelectedListener(new myspinselectedlistener(13));
		
		
		spin_supplyregister = (Spinner)findViewById(R.id.spin_supplyregister);
		supplyregarraydap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrsupplyregister);
		supplyregarraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_supplyregister.setAdapter(supplyregarraydap);
		spin_supplyregister.setOnItemSelectedListener(new myspinselectedlistener(18));
		
		
		sp_returnregister = (Spinner)findViewById(R.id.spin_returnregister);
		returnregarraydap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrretunrregister);
		returnregarraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_returnregister.setAdapter(returnregarraydap);
		sp_returnregister.setOnItemSelectedListener(new myspinselectedlistener(19));
		
		
		ductworktypetv = (TextView)findViewById(R.id.spin_ductworktype);
		ductworktypetv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				hvacvalue="Ductwork Type";k=5;
				multi_select(hvacvalue,k);				
			}
		});
		/*ductworktypearraydap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrductworktype);
		ductworktypearraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_ductworktype.setAdapter(ductworktypearraydap);*/
		
		
		sp_multizonesystem = (Spinner)findViewById(R.id.spin_multizonesystem);
		multizonearraydap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrmultizonesystem);
		multizonearraydap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_multizonesystem.setAdapter(multizonearraydap);
		sp_multizonesystem.setOnItemSelectedListener(new myspinselectedlistener(20));
		
		sp_zonetonnage = (Spinner)findViewById(R.id.spin_zonestonnage);
		zonetonnadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrzonetonnage);
		zonetonnadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_zonetonnage.setAdapter(zonetonnadap);
		sp_zonetonnage.setOnItemSelectedListener(new myspinselectedlistener(29));
		ed_otherzonetonnage= (EditText)findViewById(R.id.other_zontonnage);
		
		
		sp_btu = (Spinner)findViewById(R.id.spin_btu);
		btuarrayadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrbtu);
		btuarrayadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_btu.setOnItemSelectedListener(new myspinselectedlistener(30));
		sp_btu.setAdapter(btuarrayadap);
		ed_otherbtu= (EditText)findViewById(R.id.other_btu);
		
		thermotypetv = (TextView)findViewById(R.id.spin_thermotype);
		thermotypetv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				hvacvalue="Thermostat Type";k=6;
				multi_select(hvacvalue,k);				
			}
		});
		
		/*sp_thermotype = (Spinner)findViewById(R.id.spin_thermotype);
		thermotypeadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrthermotype);
		thermotypeadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_thermotype.setAdapter(thermotypeadap);*/
		
		/*sp_thermolocation = (Spinner)findViewById(R.id.spin_thermolocation);
		thermolocationadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrthermolocation);
		thermolocationadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_thermolocation.setAdapter(thermolocationadap);*/
		
		thermolocationtv = (TextView)findViewById(R.id.spin_thermolocation);
		thermolocationtv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				hvacvalue="Thermostat Location";k=7;
				multi_select(hvacvalue,k);				
			}
		});
		
		
		sp_externalunitreplprob = (Spinner)findViewById(R.id.spin_externalunitreplprob);
		externalunitrepadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrexternalunitrepprob);
		externalunitrepadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_externalunitreplprob.setAdapter(externalunitrepadap);
		sp_externalunitreplprob.setOnItemSelectedListener(new myspinselectedlistener(31));
		
		sp_internalunitreplprob = (Spinner)findViewById(R.id.spin_internalunitreplprob);
		internalunitrepadap = new ArrayAdapter<String>(NewHVAC.this,android.R.layout.simple_spinner_item,arrexternalunitrepprob);
		internalunitrepadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_internalunitreplprob.setAdapter(internalunitrepadap);
		sp_internalunitreplprob.setOnItemSelectedListener(new myspinselectedlistener(32));
		
		((TextView)findViewById(R.id.modulename)).setVisibility(v.VISIBLE);
		((TextView)findViewById(R.id.modulename)).setText(Html.fromHtml("<b>Module Name : </b>"+getmodulename));
		heatingtblshow= (TableLayout)findViewById(R.id.heathvactable);
		
		Button btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
			
		Button	btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					startActivity(new Intent(NewHVAC.this,HomeScreen.class));
					finish();
					 
				}
			});
		
		TextView vc = ((TextView)findViewById(R.id.vc));
		vc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent = new Intent(NewHVAC.this, NewSetVC.class);
					Bundle b2 = new Bundle();
					b2.putString("templatename", gettempname);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", "create");			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", getmodulename);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("submodulename", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("tempoption", gettempoption);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
			}
		});		
		
		Button save = (Button)findViewById(R.id.btn_addheaating);
		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getheatingtype.equals("--Select--"))
				{
					cf.DisplayToast("Please select the HVAC Type.");
				}
				else
				{
					 strotherheattype = ed_otherheattype.getText().toString();
					 strgasshutoff = ed_othergasshutoff.getText().toString();
					 strotherheatingage = ed_otherheatingage.getText().toString();
					 strzonename = ed_otherzonename.getText().toString();
					 strzonetonnage = ed_otherzonetonnage.getText().toString();
					 strbtu = ed_otherbtu.getText().toString();
					 strexternalunit = ((EditText)findViewById(R.id.other_externalunit)).getText().toString();
					 strinternalunit = ((EditText)findViewById(R.id.other_internalunit)).getText().toString();
					 strenergysource = ed_otherenergysource.getText().toString();
					 
					 if(getheatingtype.equals("--Select--")){getheatingtype="";}
					 if(getenergysource.equals("--Select--")){getenergysource="";}
					 if(getnoofzones.equals("--Select--")){getnoofzones="";}
					 if(getheatingage.equals("--Select--")){getheatingage="";}
					 if(getzoneno.equals("--Select--")){getzoneno="";}
					 if(getzonename.equals("--Select--")){getzonename="";}
					 if(getheatbrand.equals("--Select--")){getheatbrand="";}
					 if(getfurnace.equals("--Select--")){getfurnace="";}
					 if(getheatdeliverytype.equals("--Select--")){getheatdeliverytype="";}
					 if(getmotorblower.equals("--Select--")){getmotorblower="";}
					 if(getsupplyregister.equals("--Select--")){getsupplyregister="";}
					 if(getreturnregister.equals("--Select--")){getreturnregister="";}
					 if(getmultizonesystem.equals("--Select--")){getmultizonesystem="";}
					 if(getzonetonnage.equals("--Select--")){getzonetonnage="";}
					 if(getbtu.equals("--Select--")){getbtu="";}
					 if(getexternalunit.equals("--Select--")){getexternalunit="";}
					 if(getinternalunit.equals("--Select--")){getinternalunit="";}
					 
					 System.out.println("insi save");
					 try
						{
						 System.out.println("insi sdasdasa");
							if(((Button)findViewById(R.id.btn_addheaating)).getText().toString().equals("Update"))
							{
								System.out.println("UPDATE  "+db.CreateHVAC+" SET fld_energysource='"+getenergysource+"',fld_energysourceother='"+db.encode(strenergysource)+"',"+
								           "fld_gasshutoff='"+getgasshutoff+"',fld_gasshutoffother='"+db.encode(strgasshutoff)+"',fld_fueltankloc='"+db.encode(tempfueltankloc)+"',"+
										   "fld_fluetype='"+db.encode(tempfluetype)+"',fld_noofzones='"+getnoofzones+"',"+
								           "fld_zoneno='"+getzoneno+"',fld_zonename='"+getzonename+"',fld_zonenameother='"+db.encode(strzonename)+"',"+
										   "fld_age='"+getheatingage+"',fld_otherage='"+db.encode(strotherheatingage)+"',fld_heatsystembrand='"+getheatbrand+"',fld_furnacetype='"+getfurnace+"',fld_heatdeliverytype='"+getheatdeliverytype+"'," +
										   			"fld_blowermotor='"+getmotorblower+"',fld_testedfor='"+temptestedfor+"',fld_airhandlerfeatures='"+getsupplyregister+"',fld_supplyregister='"+getmotorblower+"'," +
										   					"fld_multizonesystem='"+getmultizonesystem+"',fld_tonnage='"+getzonetonnage+"',fld_tonnageother='"+strzonetonnage+"',fld_btu='"+db.encode(getbtu)+"',fld_btuother='"+db.encode(strbtu)+"',fld_thermotype='"+tempthermotype+"',fld_thermolocation='"+tempthermolocation+"',fld_externalunit='"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"',fld_internalunit='"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"',fld_extunitreplprob='"+getexternalunit+"',fld_extunitreplprobother='"+db.encode(strexternalunit)+"',fld_intunitreplprob='"+getinternalunit+"',fld_intunitreplprobother='"+db.encode(strinternalunit)+"',fld_ampextunit='"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"',fld_ampintunit='"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"',fld_emerheatampdraw='"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"',fld_supplytemp='"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"',fld_returntemp='"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"',fld_conddischarge='"+db.encode(tempconddischarge)+"'" +
										   				" Where h_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
									db.hi_db.execSQL("UPDATE  "+db.CreateHVAC+" SET fld_energysource='"+getenergysource+"',fld_energysourceother='"+db.encode(strenergysource)+"',"+
									           "fld_gasshutoff='"+getgasshutoff+"',fld_gasshutoffother='"+db.encode(strgasshutoff)+"',fld_fueltankloc='"+db.encode(tempfueltankloc)+"',"+
											   "fld_fluetype='"+db.encode(tempfluetype)+"',fld_noofzones='"+getnoofzones+"',"+
									           "fld_zoneno='"+getzoneno+"',fld_zonename='"+getzonename+"',fld_zonenameother='"+db.encode(strzonename)+"',"+
											   "fld_age='"+getheatingage+"',fld_otherage='"+db.encode(strotherheatingage)+"',fld_heatsystembrand='"+getheatbrand+"',fld_furnacetype='"+getfurnace+"',fld_heatdeliverytype='"+getheatdeliverytype+"'," +
											   			"fld_blowermotor='"+getmotorblower+"',fld_testedfor='"+temptestedfor+"',fld_airhandlerfeatures='"+tempairhandlerfeat+"',fld_supplyregister='"+getsupplyregister+"',fld_retunrregister='"+getreturnregister+"',fld_ducworktype='"+db.encode(tempductworktype)+"'," +
											   					"fld_multizonesystem='"+getmultizonesystem+"',fld_tonnage='"+getzonetonnage+"',fld_tonnageother='"+strzonetonnage+"',fld_btu='"+db.encode(getbtu)+"',fld_btuother='"+db.encode(strbtu)+"',fld_thermotype='"+tempthermotype+"'," +
											   							"fld_thermolocation='"+tempthermolocation+"',fld_externalunit='"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"'," +
											   									"fld_internalunit='"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"',fld_extunitreplprob='"+getexternalunit+"'," +
											   											"fld_extunitreplprobother='"+db.encode(strexternalunit)+"',fld_intunitreplprob='"+getinternalunit+"',fld_intunitreplprobother='"+db.encode(strinternalunit)+"'," +
											   													"fld_ampextunit='"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"'," +
											   															"fld_ampintunit='"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"'," +
											   																	"fld_emerheatampdraw='"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"'," +
											   																			"fld_supplytemp='"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"'," +
											   																					"fld_returntemp='"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"'," +
											   																							"fld_conddischarge='"+db.encode(tempconddischarge)+"'" +
											   				" Where h_id='"+hvacCurrent_select_id+"' and fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'");
									cf.DisplayToast("HVAC system updated successfully");
									((Button)findViewById(R.id.btn_addheaating)).setText("Save and Add more");
									displayheating();
									clearheating();
							}
							else
							{
								try
								{ System.out.println("insi try");
									
										Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_hvactype='"+getheatingtype+"'", null);
										System.out.println("CCC="+cur.getCount());
										if(cur.getCount()==0)
										{
											System.out.println(" INSERT INTO "+db.CreateHVAC+" (fld_templatename,fld_inspectorid,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype," +
													"fld_noofzones,fld_zoneno,fld_zonename,fld_zonenameother,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures," +
													"fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_tonnage,fld_tonnageother,fld_btu,fld_btuother,fld_thermotype,fld_thermolocation,fld_externalunit,fld_internalunit," +
													"fld_extunitreplprob,fld_extunitreplprobother,fld_intunitreplprob,fld_intunitreplprobother,fld_ampextunit,fld_ampintunit,fld_emerheatampdraw,fld_supplytemp,fld_returntemp,fld_conddischarge) VALUES" +
																						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getheatingtype+"','"+db.encode(strotherheattype)+"','"+getenergysource+"','"+db.encode(strenergysource)+"','"+getgasshutoff+"','"+db.encode(strgasshutoff)+"','"+db.encode(tempfueltankloc)+"','"+db.encode(tempfluetype)+"',"+
													 "'"+getnoofzones+"','"+getzoneno+"','"+getzonename+"','"+db.encode(strzonename)+"','"+getheatingage+"','"+db.encode(strotherheatingage)+"','"+getheatbrand+"','"+getfurnace+"','"+getheatdeliverytype+"','"+getmotorblower+"','"+temptestedfor+"','"+tempairhandlerfeat+"',"+
												    "'"+getsupplyregister+"','"+getreturnregister+"','"+db.encode(tempductworktype)+"','"+getmultizonesystem+"','"+getzonetonnage+"','"+strzonetonnage+"','"+db.encode(getbtu)+"','"+db.encode(strbtu)+"','"+tempthermotype+"','"+tempthermolocation+"','"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"'"+
													"'"+getexternalunit+"','"+db.encode(strexternalunit)+"','"+getinternalunit+"','"+db.encode(strinternalunit)+"','"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"','"+db.encode(tempconddischarge)+"')");
											db.hi_db.execSQL(" INSERT INTO "+db.CreateHVAC+" (fld_templatename,fld_inspectorid,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype," +
													"fld_noofzones,fld_zoneno,fld_zonename,fld_zonenameother,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures," +
													"fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_tonnage,fld_tonnageother,fld_btu,fld_btuother,fld_thermotype,fld_thermolocation,fld_externalunit,fld_internalunit," +
													"fld_extunitreplprob,fld_extunitreplprobother,fld_intunitreplprob,fld_intunitreplprobother,fld_ampextunit,fld_ampintunit,fld_emerheatampdraw,fld_supplytemp,fld_returntemp,fld_conddischarge) VALUES" +
																						    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getheatingtype+"','"+db.encode(strotherheattype)+"','"+getenergysource+"','"+db.encode(strenergysource)+"','"+getgasshutoff+"','"+db.encode(strgasshutoff)+"','"+db.encode(tempfueltankloc)+"','"+db.encode(tempfluetype)+"',"+
													 "'"+getnoofzones+"','"+getzoneno+"','"+getzonename+"','"+db.encode(strzonename)+"','"+getheatingage+"','"+db.encode(strotherheatingage)+"','"+getheatbrand+"','"+getfurnace+"','"+getheatdeliverytype+"','"+getmotorblower+"','"+temptestedfor+"','"+tempairhandlerfeat+"',"+
												    "'"+getsupplyregister+"','"+getreturnregister+"','"+db.encode(tempductworktype)+"','"+getmultizonesystem+"','"+getzonetonnage+"','"+strzonetonnage+"','"+db.encode(getbtu)+"','"+db.encode(strbtu)+"','"+tempthermotype+"','"+tempthermolocation+"','"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"'"+
													"'"+getexternalunit+"','"+db.encode(strexternalunit)+"','"+getinternalunit+"','"+db.encode(strinternalunit)+"','"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"','"+db.encode(tempconddischarge)+"')");
											
											
											cf.DisplayToast("HVAC system saved successfully");
											displayheating();
											clearheating();
										}
										else
										{
										   cf.DisplayToast("Already Exists!! Please select another HVAC Type.");
										}
									
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								
							}
							
						}
						catch (Exception e) {
							// TODO: handle exception
							 System.out.println("insi sdasdaseeeea"+e.getMessage());
						}
					 
					/*try
					{
					System.out.println(" INSERT INTO "+db.CreateHVAC+" (fld_templatename,fld_inspectorid,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype," +
							"fld_noofzones,fld_zoneno,fld_zonename,fld_zonenameother,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures," +
							"fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_tonnage,fld_tonnageother,fld_btu,fld_btuother,fld_thermotype,fld_thermolocation,fld_externalunit,fld_internalunit," +
							"fld_extunitreplprob,fld_extunitreplprobother,fld_intunitreplprob,fld_intunitreplprobother,fld_ampextunit,fld_ampintunit,fld_emerheatampdraw,fld_supplytemp,fld_returntemp,fld_conddischarge) VALUES" +
																    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getheatingtype+"','"+db.encode(strotherheattype)+"','"+getenergysource+"','"+db.encode(strenergysource)+"','"+getgasshutoff+"','"+db.encode(strgasshutoff)+"','"+db.encode(tempfueltankloc)+"','"+db.encode(tempfluetype)+"',"+
							 "'"+getnoofzones+"','"+getzoneno+"','"+getzonename+"','"+db.encode(strzonename)+"','"+getheatingage+"','"+db.encode(strotherheatingage)+"','"+getheatbrand+"','"+getfurnace+"','"+getheatdeliverytype+"','"+getmotorblower+"','"+temptestedfor+"','"+tempairhandlerfeat+"',"+
						    "'"+getsupplyregister+"','"+getreturnregister+"','"+db.encode(tempductworktype)+"','"+getmultizonesystem+"','"+getzonetonnage+"','"+strzonetonnage+"','"+db.encode(getbtu)+"','"+db.encode(strbtu)+"','"+tempthermotype+"','"+tempthermolocation+"','"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"'"+
							"'"+getexternalunit+"','"+db.encode(strexternalunit)+"','"+getinternalunit+"','"+db.encode(strinternalunit)+"','"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"','"+db.encode(tempconddischarge)+"')");
					
					db.hi_db.execSQL(" INSERT INTO "+db.CreateHVAC+" (fld_templatename,fld_inspectorid,fld_hvactype,fld_hvactypeother,fld_energysource,fld_energysourceother,fld_gasshutoff,fld_gasshutoffother,fld_fueltankloc,fld_fluetype," +
							"fld_noofzones,fld_zoneno,fld_zonename,fld_zonenameother,fld_age,fld_otherage,fld_heatsystembrand,fld_furnacetype,fld_heatdeliverytype,fld_blowermotor,fld_testedfor,fld_airhandlerfeatures," +
							"fld_supplyregister,fld_retunrregister,fld_ducworktype,fld_multizonesystem,fld_tonnage,fld_tonnageother,fld_btu,fld_btuother,fld_thermotype,fld_thermolocation,fld_externalunit,fld_internalunit," +
							"fld_extunitreplprob,fld_extunitreplprobother,fld_intunitreplprob,fld_intunitreplprobother,fld_ampextunit,fld_ampintunit,fld_emerheatampdraw,fld_supplytemp,fld_returntemp,fld_conddischarge) VALUES" +
																    "('"+db.encode(gettempname)+"','"+db.UserId+"','"+getheatingtype+"','"+db.encode(strotherheattype)+"','"+getenergysource+"','"+db.encode(strenergysource)+"','"+getgasshutoff+"','"+db.encode(strgasshutoff)+"','"+db.encode(tempfueltankloc)+"','"+db.encode(tempfluetype)+"',"+
							 "'"+getnoofzones+"','"+getzoneno+"','"+getzonename+"','"+db.encode(strzonename)+"','"+getheatingage+"','"+db.encode(strotherheatingage)+"','"+getheatbrand+"','"+getfurnace+"','"+getheatdeliverytype+"','"+getmotorblower+"','"+temptestedfor+"','"+tempairhandlerfeat+"',"+
						    "'"+getsupplyregister+"','"+getreturnregister+"','"+db.encode(tempductworktype)+"','"+getmultizonesystem+"','"+getzonetonnage+"','"+strzonetonnage+"','"+db.encode(getbtu)+"','"+db.encode(strbtu)+"','"+tempthermotype+"','"+tempthermolocation+"','"+db.encode(((EditText)findViewById(R.id.externalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.internalunit)).getText().toString())+"'"+
							"'"+getexternalunit+"','"+db.encode(strexternalunit)+"','"+getinternalunit+"','"+db.encode(strinternalunit)+"','"+db.encode(((EditText)findViewById(R.id.ampdrawexternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.ampdrawinternalunit)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.emerheatampdraw)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.suppplytemp)).getText().toString())+"','"+db.encode(((EditText)findViewById(R.id.returntemp)).getText().toString())+"','"+db.encode(tempconddischarge)+"')");
					
					
					cf.DisplayToast("HVAC system saved successfully");
					displayheating();
					}
					catch(Exception e)
					{
						System.out.println("EEEE="+e.getMessage());
					}*/
				}
			
			}
		});
		
		fueltanklocnote = (TextView)findViewById(R.id.fueltankloctv);
		fluetypenote = (TextView)findViewById(R.id.fluetypetv);
		testedfornote = (TextView)findViewById(R.id.testedfortv);
		airhandlerfeatnote = (TextView)findViewById(R.id.airhandlerfeattv);
		ductworknote = (TextView)findViewById(R.id.ductworktypetv);
		thermostattypetv = (TextView)findViewById(R.id.thermotypetv);
		thermostatlocationtv = (TextView)findViewById(R.id.thermoloctypetv);
		conddisctv = (TextView)findViewById(R.id.condischargetv);
		
		LinearLayout menu = (LinearLayout) findViewById(R.id.hvacmenu);
		menu.addView(new Header_Menu(this, 1, cf,gettempname,getcurrentview,getmodulename,gettempoption,getselectedho,db));
		displayheating();
	}
	
	private void clearheating() {
		// TODO Auto-generated method stub
		sp_heatingtype.setSelection(0);sp_heatingtype.setEnabled(true);
		sp_energysource.setSelection(0);
		sp_gasshutoff.setSelection(0);
		spin_noofzones.setSelection(0);
		spin_heatingage.setSelection(0);
		spin_zoneno.setSelection(0);
		sp_zonename.setSelection(0);
		sp_heatbrand.setSelection(0);
		sp_furnace.setSelection(0);
		sp_heatdelivery.setSelection(0);
		sp_motorblower.setSelection(0);
		sp_returnregister.setSelection(0);
		spin_supplyregister.setSelection(0);
		sp_multizonesystem.setSelection(0);
		sp_zonetonnage.setSelection(0);
		sp_btu.setSelection(0);
		
		fueltanklocnote.setVisibility(v.GONE);
		fluetypenote.setVisibility(v.GONE);
		testedfornote.setVisibility(v.GONE);
		airhandlerfeatnote.setVisibility(v.GONE);
		ductworknote.setVisibility(v.GONE);
		thermostattypetv.setVisibility(v.GONE);
		thermostatlocationtv.setVisibility(v.GONE);
		conddisctv.setVisibility(v.GONE);
		
		
		sp_externalunitreplprob.setSelection(0);
		sp_internalunitreplprob.setSelection(0);
		ed_othergasshutoff.setVisibility(v.GONE);
		ed_otherheattype.setVisibility(v.GONE);
		ed_otherheatingage.setVisibility(v.GONE);
		ed_otherzonename.setVisibility(v.GONE);
		ed_otherzonetonnage.setVisibility(v.GONE);
		ed_otherbtu.setVisibility(v.GONE);
		((EditText)findViewById(R.id.other_externalunit)).setVisibility(v.GONE);
		((EditText)findViewById(R.id.externalunit)).setText("");
		((EditText)findViewById(R.id.internalunit)).setText("");	
		((EditText)findViewById(R.id.other_externalunit)).setVisibility(v.GONE);
		((EditText)findViewById(R.id.ampdrawexternalunit)).setText("");
		((EditText)findViewById(R.id.ampdrawinternalunit)).setText("");
		((EditText)findViewById(R.id.emerheatampdraw)).setText("");
		((EditText)findViewById(R.id.suppplytemp)).setText("");
		((EditText)findViewById(R.id.returntemp)).setText("");
	}

	private void displayheating() {
		// TODO Auto-generated method stub
		try
		{
			/*
			if(getcurrentview.equals("start"))
			{
				  cur= db.hi_db.rawQuery("select * from " + db.LoadHeating + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname)+"'",null);
			}
			else
			{*/
				Cursor cur= db.hi_db.rawQuery("select * from " + db.CreateHVAC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			//}
		System.out.println("cc="+cur.getCount());
			 if(cur.getCount()>0)
			 {
				 cur.moveToFirst();
				 try
					{
						heatingtblshow.removeAllViews();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				 LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvaclistview_header, null); 
				 LinearLayout th= (LinearLayout)h1.findViewById(R.id.heatinglist);th.setVisibility(v.VISIBLE);
				 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 lp.setMargins(2, 0, 2, 2);
				 h1.removeAllViews();
			
				 heatingtblshow.setVisibility(View.VISIBLE); 
				 
				 heatingtblshow.addView(th,lp); 			 
				
				 for(int i=0;i<cur.getCount();i++)
				 {
					 String hvactype="",hvactypeother="",energysource="",energysourceother="",gasshutoff="",gasshutoffother="",age="",fueltankloc="",fluetype="",
							 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",
							 furnacefeatuers="",supplyregister="",retunrregister="",fueltanklocation="",
									 noofzones="",zoneno="",zonename="",zonenameother="",blowermotor="",testedfor="",airhandlerfeatures="",
									 ducworktype="",multizonesystem="",tonnage="",tonnageother,btu,btuother,thermotype,thermolocation,
											 externalunit,internalunit,extunitreplprob,extunitreplprobother,intunitreplprob,intunitreplprobother,ampextunit,ampintunit,emerheatampdraw,supplytemp,returntemp,conddischarge;
					 
					 hvactype=cur.getString(cur.getColumnIndex("fld_hvactype"));
					 hvactypeother=db.decode(cur.getString(cur.getColumnIndex("fld_hvactypeother")));
					 energysource=db.decode(cur.getString(cur.getColumnIndex("fld_energysource")));
					 energysourceother=db.decode(cur.getString(cur.getColumnIndex("fld_energysourceother")));
					 gasshutoff=db.decode(cur.getString(cur.getColumnIndex("fld_gasshutoff")));
					 gasshutoffother=db.decode(cur.getString(cur.getColumnIndex("fld_gasshutoffother")));
					 fueltankloc=db.decode(cur.getString(cur.getColumnIndex("fld_fueltankloc")));
					 fluetype=db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));
					 noofzones=cur.getString(cur.getColumnIndex("fld_noofzones"));
					 zoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));					 
					 zonename=cur.getString(cur.getColumnIndex("fld_zonename"));
					 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_zonenameother")));
					 age=cur.getString(cur.getColumnIndex("fld_age"));
					 otherage=db.decode(cur.getString(cur.getColumnIndex("fld_otherage")));
					 heatsystembrand=cur.getString(cur.getColumnIndex("fld_heatsystembrand"));
					 furnacetype=db.decode(cur.getString(cur.getColumnIndex("fld_furnacetype")));
					 heatdeliverytype=cur.getString(cur.getColumnIndex("fld_heatdeliverytype"));	System.out.println("heatdeliverytype"+heatdeliverytype);				 
					 blowermotor=cur.getString(cur.getColumnIndex("fld_blowermotor"));
					 testedfor=db.decode(cur.getString(cur.getColumnIndex("fld_testedfor")));					 
					 airhandlerfeatures=cur.getString(cur.getColumnIndex("fld_airhandlerfeatures"));
					 supplyregister=db.decode(cur.getString(cur.getColumnIndex("fld_supplyregister")));
					 retunrregister=cur.getString(cur.getColumnIndex("fld_retunrregister"));					 
					 ducworktype=db.decode(cur.getString(cur.getColumnIndex("fld_ducworktype")));
					 multizonesystem=db.decode(cur.getString(cur.getColumnIndex("fld_multizonesystem")));					 
					 tonnage=cur.getString(cur.getColumnIndex("fld_tonnage"));
					 tonnageother=db.decode(cur.getString(cur.getColumnIndex("fld_tonnageother")));
					 btu=db.decode(cur.getString(cur.getColumnIndex("fld_btu")));					 
					 btuother=db.decode(cur.getString(cur.getColumnIndex("fld_btuother")));
					 thermotype=db.decode(cur.getString(cur.getColumnIndex("fld_thermotype")));					 
					 thermolocation=cur.getString(cur.getColumnIndex("fld_thermolocation"));
					 externalunit=db.decode(db.decode(cur.getString(cur.getColumnIndex("fld_externalunit"))));
					 internalunit=db.decode(cur.getString(cur.getColumnIndex("fld_internalunit")));					 
					 extunitreplprob=cur.getString(cur.getColumnIndex("fld_extunitreplprob"));
					 extunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_extunitreplprobother")));					 
					 intunitreplprob=cur.getString(cur.getColumnIndex("fld_intunitreplprob"));					 
					 intunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_intunitreplprobother")));
					 ampextunit=db.decode(cur.getString(cur.getColumnIndex("fld_ampextunit")));					 
					 ampintunit=db.decode(cur.getString(cur.getColumnIndex("fld_ampintunit")));
					 emerheatampdraw=db.decode(db.decode(cur.getString(cur.getColumnIndex("fld_emerheatampdraw"))));					 
					 supplytemp=db.decode(cur.getString(cur.getColumnIndex("fld_supplytemp")));
					 
					 returntemp=db.decode(cur.getString(cur.getColumnIndex("fld_returntemp")));					 
					 conddischarge=db.decode(cur.getString(cur.getColumnIndex("fld_conddischarge")));
					 
					 
					 LinearLayout l1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvaclistview, null);
					 LinearLayout t = (LinearLayout)l1.findViewById(R.id.heatingvalue);
					 t.setVisibility(v.VISIBLE);
				     t.setId(100+i);
				     
				     
				     TextView tvheattype= (TextView) t.findViewWithTag("hvactype");
				     if(hvactype.equals("Other"))
					 {
				    	 tvheattype.setText(hvactypeother);	
					 }
					 else
					 {
						 tvheattype.setText(hvactype);
					 }
				     
				     TextView tvenergsource= (TextView) t.findViewWithTag("energysource");
				     if(energysource.equals("--Select--"))
					 {
				    	 energysource="";
					 }
				     if(energysource.equals("Other"))
					 {
				    	 tvenergsource.setText(energysourceother);	
					 }
					 else
					 {
						 tvenergsource.setText(energysource);
					 }
				     
				     TextView tvgasshutoff= (TextView) t.findViewWithTag("gasshutoff");
				     if(gasshutoff.equals("--Select--"))
					 {
				    	 gasshutoff="";
					 }
				     if(gasshutoff.equals("Other"))
					 {
				    	 tvgasshutoff.setText(gasshutoffother);	
					 }
					 else
					 {
						 tvgasshutoff.setText(gasshutoff);
					 }
				     
				     
				     TextView tvfueltank= (TextView) t.findViewWithTag("fueltanklocation");
				     if(fueltankloc.equals("--Select--"))
					 {
				    	 fueltankloc="";
					 }
				     if(fueltankloc.equals("Other"))
					 {
				    	 tvfueltank.setText(gasshutoffother);	
					 }
					 else
					 {
						 tvfueltank.setText(fueltankloc);
					 }
				     
				     
				     TextView tvzonename= (TextView) t.findViewWithTag("zonename");
				     if(zonename.equals("--Select--"))
					 {
				    	 zonename="";
					 }
				     if(fueltankloc.equals("Other"))
					 {
				    	 tvzonename.setText(zonenameother);	
					 }
					 else
					 {
						 tvzonename.setText(zonename);
					 }
				     
				     
				     TextView tvage= (TextView) t.findViewWithTag("age");
				     if(age.equals("--Select--"))
					 {
				    	 age="";
					 }
				     if(age.equals("Other"))
					 {
				    	 tvage.setText(otherage);	
					 }
					 else
					 {
						 tvage.setText(age);
					 }
				     TextView tvnoofzones= (TextView) t.findViewWithTag("noofzones");
						if(noofzones.equals("--Select--"))
						{
							noofzones="";
						}
						tvnoofzones.setText(noofzones);
						
					 TextView tvzoneno= (TextView) t.findViewWithTag("zoneno");
					 if(zoneno.equals("--Select--"))
						{
						 zoneno="";
						}
					 tvzoneno.setText(zoneno);
						
										
					TextView tvbrand= (TextView) t.findViewWithTag("systembrand");
					if(heatsystembrand.equals("--Select--"))
					{
						heatsystembrand="";
					}
					tvbrand.setText(heatsystembrand);
					
					TextView tvfurnace= (TextView) t.findViewWithTag("furnacetype");
					if(furnacetype.equals("--Select--"))
					{
						furnacetype="";
					}
					tvfurnace.setText(furnacetype);
					
					TextView tvtestedfor= (TextView) t.findViewWithTag("testedfor");
					if(testedfor.equals("--Select--"))
					{
						testedfor="";
					}
					tvtestedfor.setText(testedfor);
					
					TextView tvheatdeliverytype= (TextView) t.findViewWithTag("heatdeliverytype");
					if(heatdeliverytype.equals("--Select--"))
					{
						heatdeliverytype="";
					}
					tvheatdeliverytype.setText(heatdeliverytype);
					
					TextView tvmotorblower= (TextView) t.findViewWithTag("blowermotor");System.out.println("motorblower"+motorblower);
					if(blowermotor.equals("--Select--"))
					{
						blowermotor="";
					}
					tvmotorblower.setText(blowermotor);
					
						
					 TextView tvsupplyregister= (TextView) t.findViewWithTag("supplyregisters");
					 if(supplyregister.equals("--Select--"))
					 {
						 supplyregister="";
					 }
					 tvsupplyregister.setText(supplyregister);
					 
					 TextView tvmultizonesystem= (TextView) t.findViewWithTag("multizonesystem");
					 if(multizonesystem.equals("--Select--"))
					 {
						 multizonesystem="";
					 }
					 tvmultizonesystem.setText(multizonesystem);
					 
					
						
						
					 TextView tvretunrregister= (TextView) t.findViewWithTag("returnregisters");
					 if(retunrregister.equals("--Select--"))
					 {
						 retunrregister="";
					 }
					 tvretunrregister.setText(retunrregister);

					  TextView tvbtu= (TextView) t.findViewWithTag("btu");
					     if(btu.equals("--Select--"))
						 {
					    	 btu="";
						 }
					     if(btu.equals("Other"))
						 {
					    	 tvbtu.setText(btuother);	
						 }
						 else
						 {
							 tvbtu.setText(btu);
						 }
					     
					     TextView tvtonnage= (TextView) t.findViewWithTag("tonnage");
					     if(tonnage.equals("--Select--"))
						 {
					    	 tonnage="";
						 }
					     if(tonnage.equals("Other"))
						 {
					    	 tvtonnage.setText(tonnageother);	
						 }
						 else
						 {
							 tvtonnage.setText(tonnage);
						 }
					 
					 
					 TextView tvfluetype= (TextView) t.findViewWithTag("fluetype");
					 if(fluetype.equals("--Select--"))
					 {
						 fluetype="";
					 }
					 tvfluetype.setText(fluetype);
					 
					 TextView tvthermotype= (TextView) t.findViewWithTag("thermostattype");
					 if(thermotype.equals("--Select--"))
					 {
						 thermotype="";
					 }
					 tvthermotype.setText(thermotype);
					 
					 TextView tvthermoloc= (TextView) t.findViewWithTag("thermostatlocation");
					 if(thermolocation.equals("--Select--"))
					 {
						 thermolocation="";
					 }
					 tvthermoloc.setText(thermolocation);
					 
					 TextView tvcondensationdisc= (TextView) t.findViewWithTag("conddischarge");
					 if(conddischarge.equals("--Select--"))
					 {
						 conddischarge="";
					 }
					 tvcondensationdisc.setText(conddischarge);
					
					
					 TextView tvexternalunit= (TextView) t.findViewWithTag("externalunitsn");
					 tvexternalunit.setText(externalunit);
					 TextView tvinternalunit= (TextView) t.findViewWithTag("internalunitsn");
					 tvinternalunit.setText(internalunit);
					 
					 TextView tvexternalunitreppob= (TextView) t.findViewWithTag("externalunitreppob");
					 if(extunitreplprob.equals("--Select--"))
					 {
						 extunitreplprob="";
					 }
					 tvexternalunitreppob.setText(extunitreplprob);
					 
					 TextView tvinternalunitreppob= (TextView) t.findViewWithTag("internalunitreppob");
					 if(intunitreplprob.equals("--Select--"))
					 {
						 intunitreplprob="";
					 }
					 tvinternalunitreppob.setText(intunitreplprob);
					 
					 TextView tvductworktype= (TextView) t.findViewWithTag("ductworktype");
					 if(ductworktypevalue.equals("--Select--"))
					 {
						 ductworktypevalue="";
					 }
					 tvductworktype.setText(ductworktypevalue);
					 
					 TextView tvairhanlderfeat= (TextView) t.findViewWithTag("airhanlderfeat");
					 if(airhandlerfeatures.equals("--Select--"))
					 {
						 airhandlerfeatures="";
					 }
					 tvairhanlderfeat.setText(airhandlerfeatures);
					 
					 
					 TextView tvampdrawextunit= (TextView) t.findViewWithTag("ampdrawextunit");
					 tvampdrawextunit.setText(ampextunit);
					 TextView tvampdrawintunit= (TextView) t.findViewWithTag("ampdrawintunit");
					 tvampdrawintunit.setText(ampintunit);
					 TextView tvemerheatampdraw= (TextView) t.findViewWithTag("emerheatampdraw");
					 tvemerheatampdraw.setText(emerheatampdraw);
					 TextView tvsupplytemp= (TextView) t.findViewWithTag("supplytemp");
					 tvsupplytemp.setText(supplytemp);
					 TextView tvreturntemp= (TextView) t.findViewWithTag("returntemp");
					 tvreturntemp.setText(returntemp);
					 
					 ImageView edit= (ImageView) t.findViewWithTag("edit");
					 	edit.setId(789456+i);
					 	edit.setTag(cur.getString(0));
		                edit.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
							try
							{
							   int i=Integer.parseInt(v.getTag().toString());
								((Button)findViewById(R.id.btn_addheaating)).setText("Update");
							   //vrint=1;
							
							   updatehvac(i);   
							}
							catch (Exception e) {
								// TODO: handle exception
							}
							}
		                });
	             
	                ImageView delete=(ImageView) t.findViewWithTag("del");  
				 	delete.setTag(cur.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(NewHVAC.this);
							builder.setMessage("Do you want to delete the selected Heating details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==hvacCurrent_select_id)
													{
														hvacCurrent_select_id=0;
													}
													if(getcurrentview.equals("start"))
													{
														db.hi_db.execSQL("Delete from "+db.LoadHeating+" Where h_id='"+i+"'");	
													}
													else
													{
														db.hi_db.execSQL("Delete from "+db.CreateHVAC+" Where h_id='"+i+"'");
													}
																										
													Toast.makeText(getApplicationContext(), "Deleted successfully.", 0);
												/*	Cursor cur= db.hi_db.rawQuery("select * from " + db.LoadHeating + " where fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_templatename='"+db.encode(gettempname).toLowerCase()+"'",null);
													if(cur.getCount()==0)
													{
														flag1=1;
													}*/
													
													//clearheating();
													clearheating();
													displayheating();((Button)findViewById(R.id.btn_addheaating)).setText("Save and Add more");
													
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
					
					
					
					
					l1.removeAllViews();
					
					heatingtblshow.addView(t,lp);
					cur.moveToNext();
					
				 }
			 }
			 else
			 {
				 heatingtblshow.setVisibility(View.GONE);
			 }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	 	
	}
	private void scrollup() {
		// TODO Auto-generated method stub
	scr = (ScrollView)findViewById(R.id.scr);
	       scr.post(new Runnable() {
	          public void run() {
	              scr.scrollTo(0,0);
	          }
	      }); 
	}
	protected void updatehvac(int i) {
		// TODO Auto-generated method stub
		Cursor cur;
			editid=1;
			getisedit=1;
			hvacCurrent_select_id = i;scrollup();
			try
			{
					cur= db.hi_db.rawQuery("select * from " + db.CreateHVAC + " where fld_inspectorid='"+db.UserId+"' and  fld_templatename='"+db.encode(gettempname)+"' and h_id='"+i+"'",null);
			
				
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
						
					 String hvactype="",hvactypeother="",energysource="",energysourceother="",gasshutoff="",gasshutoffother="",age="",fueltankloc="",fluetype="",
							 otherage="",heatsystembrand="",furnacetype="",heatdeliverytype="",motorblower="",
							 furnacefeatuers="",supplyregister="",retunrregister="",fueltanklocation="",
									 noofzones="",zoneno="",zonename="",zonenameother="",blowermotor="",testedfor="",airhandlerfeatures="",
									 ducworktype="",multizonesystem="",tonnage="",tonnageother,btu,btuother,thermotype,thermolocation,
											 externalunit,internalunit,extunitreplprob,extunitreplprobother,intunitreplprob,intunitreplprobother,ampextunit,ampintunit,emerheatampdraw,supplytemp,returntemp,conddischarge;
					 
					 hvactype=cur.getString(cur.getColumnIndex("fld_hvactype"));
					 hvactypeother=db.decode(cur.getString(cur.getColumnIndex("fld_hvactypeother")));
					 energysource=db.decode(cur.getString(cur.getColumnIndex("fld_energysource")));
					 energysourceother=db.decode(cur.getString(cur.getColumnIndex("fld_energysourceother")));
					 gasshutoff=db.decode(cur.getString(cur.getColumnIndex("fld_gasshutoff")));
					 gasshutoffother=db.decode(cur.getString(cur.getColumnIndex("fld_gasshutoffother")));
					 fueltankloc=db.decode(cur.getString(cur.getColumnIndex("fld_fueltankloc")));
					 fluetype=db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));
					 noofzones=cur.getString(cur.getColumnIndex("fld_noofzones"));
					 zoneno=cur.getString(cur.getColumnIndex("fld_zoneno"));					 
					 zonename=cur.getString(cur.getColumnIndex("fld_zonename"));
					 zonenameother=db.decode(cur.getString(cur.getColumnIndex("fld_zonenameother")));
					 age=cur.getString(cur.getColumnIndex("fld_age"));
					 otherage=db.decode(cur.getString(cur.getColumnIndex("fld_otherage")));
					 heatsystembrand=cur.getString(cur.getColumnIndex("fld_heatsystembrand"));
					 furnacetype=db.decode(cur.getString(cur.getColumnIndex("fld_furnacetype")));
					 heatdeliverytype=cur.getString(cur.getColumnIndex("fld_heatdeliverytype"));					 
					 blowermotor=cur.getString(cur.getColumnIndex("fld_blowermotor"));
					 testedfor=db.decode(cur.getString(cur.getColumnIndex("fld_testedfor")));					 
					 airhandlerfeatures=cur.getString(cur.getColumnIndex("fld_airhandlerfeatures"));
					 supplyregister=db.decode(cur.getString(cur.getColumnIndex("fld_supplyregister")));
					 retunrregister=cur.getString(cur.getColumnIndex("fld_retunrregister"));					 
					 ducworktype=cur.getString(cur.getColumnIndex("fld_ducworktype"));
					 multizonesystem=db.decode(cur.getString(cur.getColumnIndex("fld_multizonesystem")));					 
					 tonnage=cur.getString(cur.getColumnIndex("fld_tonnage"));
					 tonnageother=db.decode(cur.getString(cur.getColumnIndex("fld_tonnageother")));
					 btu=cur.getString(cur.getColumnIndex("fld_btu"));					 
					 btuother=cur.getString(cur.getColumnIndex("fld_btuother"));
					 thermotype=db.decode(cur.getString(cur.getColumnIndex("fld_thermotype")));					 
					 thermolocation=cur.getString(cur.getColumnIndex("fld_thermolocation"));
					 externalunit=db.decode(cur.getString(cur.getColumnIndex("fld_externalunit")));
					 internalunit=cur.getString(cur.getColumnIndex("fld_internalunit"));					 
					 extunitreplprob=cur.getString(cur.getColumnIndex("fld_extunitreplprob"));
					 extunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_extunitreplprobother")));					 
					 intunitreplprob=cur.getString(cur.getColumnIndex("fld_intunitreplprob"));					 
					 intunitreplprobother=db.decode(cur.getString(cur.getColumnIndex("fld_intunitreplprobother")));
					 ampextunit=cur.getString(cur.getColumnIndex("fld_ampextunit"));					 
					 ampintunit=cur.getString(cur.getColumnIndex("fld_ampintunit"));
					 emerheatampdraw=db.decode(cur.getString(cur.getColumnIndex("fld_emerheatampdraw")));					 
					 supplytemp=cur.getString(cur.getColumnIndex("fld_supplytemp"));
					 
					 returntemp=db.decode(cur.getString(cur.getColumnIndex("fld_returntemp")));					 
					 conddischarge=cur.getString(cur.getColumnIndex("fld_conddischarge"));
					
					sp_heatingtype.setSelection(heattypearrayadap.getPosition(hvactype));sp_heatingtype.setEnabled(false);
					if(hvactypeother.trim().equals(""))
					{
						ed_otherheattype.setVisibility(v.GONE);
					}
					else
					{
						ed_otherheattype.setVisibility(v.VISIBLE);
						ed_otherheattype.setText(hvactypeother);
					}
					
					sp_energysource.setSelection(energysourcearrayadap.getPosition(energysource));
					if(energysourceother.trim().equals(""))
					{
						ed_otherenergysource.setVisibility(v.GONE);
					}
					else
					{
						ed_otherenergysource.setVisibility(v.VISIBLE);
						ed_otherenergysource.setText(energysourceother);
					}
					if(energysource.equals("Gas"))
					{
						sp_gasshutoff.setSelection(gasshutoffarrayadap.getPosition(gasshutoff));
						if(gasshutoffother.trim().equals(""))
						{
							ed_othergasshutoff.setVisibility(v.GONE);
						}
						else
						{
							ed_othergasshutoff.setVisibility(v.VISIBLE);
							ed_othergasshutoff.setText(gasshutoffother);
						}
					}
					else
					{
						
					}
					
					spin_noofzones.setSelection(noofzonesarrayadap.getPosition(noofzones));
					
					spin_heatingage.setSelection(agearrayadap.getPosition(age));
					if(otherage.trim().equals(""))
					{
						ed_otherheatingage.setVisibility(v.GONE);
					}
					else
					{
						ed_otherheatingage.setVisibility(v.VISIBLE);
						ed_otherheatingage.setText(otherage);
					}
					
					spin_zoneno.setSelection(zonenoarrayadap.getPosition(zoneno));
					
					sp_zonename.setSelection(zonenameadap.getPosition(zonename));
					if(zonenameother.trim().equals(""))
					{
						ed_otherzonename.setVisibility(v.GONE);
					}
					else
					{
						ed_otherzonename.setVisibility(v.VISIBLE);
						ed_otherzonename.setText(zonenameother);
					}
					
					sp_heatbrand.setSelection(heatsystemarrayadap.getPosition(heatsystembrand));
					sp_furnace.setSelection(furnacearrayadap.getPosition(furnacetype));
					
					sp_heatdelivery.setSelection(heatdeliveryarrayadap.getPosition(heatdeliverytype));
					sp_motorblower.setSelection(motorarrayadap.getPosition(motorblower));
					spin_supplyregister.setSelection(supplyregarraydap.getPosition(motorblower));
					sp_returnregister.setSelection(returnregarraydap.getPosition(retunrregister));
					sp_multizonesystem.setSelection(multizonearraydap.getPosition(multizonesystem));
					sp_zonetonnage.setSelection(zonetonnadap.getPosition(tonnage));
					if(tonnageother.trim().equals(""))
					{
						ed_otherzonetonnage.setVisibility(v.GONE);
					}
					else
					{
						ed_otherzonetonnage.setVisibility(v.VISIBLE);
						ed_otherzonetonnage.setText(tonnageother);
					}
					
					sp_btu.setSelection(btuarrayadap.getPosition(btu));
					if(btuother.trim().equals(""))
					{
						ed_otherbtu.setVisibility(v.GONE);
					}
					else
					{
						ed_otherbtu.setVisibility(v.VISIBLE);
						ed_otherbtu.setText(btuother);
					}
					
					 ((EditText)findViewById(R.id.externalunit)).setText(externalunit);
					 ((EditText)findViewById(R.id.internalunit)).setText(internalunit);
					
					sp_externalunitreplprob.setSelection(externalunitrepadap.getPosition(extunitreplprob));
					if(extunitreplprobother.trim().equals(""))
					{
						((EditText)findViewById(R.id.other_externalunit)).setVisibility(v.GONE);
					}
					else
					{
						((EditText)findViewById(R.id.other_externalunit)).setVisibility(v.VISIBLE);
						((EditText)findViewById(R.id.other_externalunit)).setText(extunitreplprobother);
					}
					
					sp_internalunitreplprob.setSelection(internalunitrepadap.getPosition(intunitreplprob));
					if(intunitreplprobother.trim().equals(""))
					{
						((EditText)findViewById(R.id.other_internalunit)).setVisibility(v.GONE);
					}
					else
					{
						((EditText)findViewById(R.id.other_internalunit)).setVisibility(v.VISIBLE);
						((EditText)findViewById(R.id.other_internalunit)).setText(intunitreplprobother);
					}
					
					((EditText)findViewById(R.id.ampdrawexternalunit)).setText(ampextunit);
					((EditText)findViewById(R.id.ampdrawinternalunit)).setText(ampintunit);
					((EditText)findViewById(R.id.emerheatampdraw)).setText(emerheatampdraw);
					((EditText)findViewById(R.id.suppplytemp)).setText(supplytemp);
					((EditText)findViewById(R.id.returntemp)).setText(returntemp);
					
					
					((Button)findViewById(R.id.btn_addheaating)).setText("Update");
				}
			}catch(Exception e){}
		
	}
	private void dbvalue(String bathvalue) {
		// TODO Auto-generated method stub
		str_arrlst.clear();
		if(bathvalue.equals("Fuel Tank Location"))
		{
			for(int j=0;j<arrfueltank.length;j++)
			{
				str_arrlst.add(arrfueltank[j]);
			}	
		}
		if(bathvalue.equals("Flue Type"))
		{
			for(int j=0;j<arrfluetype.length;j++)
			{
				str_arrlst.add(arrfluetype[j]);
			}	
		}
		if(bathvalue.equals("Tested For"))
		{
			for(int j=0;j<arrzonetestingfor.length;j++)
			{
				str_arrlst.add(arrzonetestingfor[j]);
			}	
		}

		if(bathvalue.equals("Air Handler Features"))
		{
			for(int j=0;j<arrairhandlerfeatures.length;j++)
			{
				str_arrlst.add(arrairhandlerfeatures[j]);
			}	
		}
		
		if(bathvalue.equals("Ductwork Type"))
		{
			for(int j=0;j<arrductworktype.length;j++)
			{
				str_arrlst.add(arrductworktype[j]);
			}	
		}
		
		if(bathvalue.equals("Thermostat Type"))
		{
			for(int j=0;j<arrthermotype.length;j++)
			{
				str_arrlst.add(arrthermotype[j]);
			}	
		}
		
		if(bathvalue.equals("Thermostat Location"))
		{
			for(int j=0;j<arrthermolocation.length;j++)
			{
				str_arrlst.add(arrthermolocation[j]);
			}	
		}
		
		if(bathvalue.equals("Condensation Discharge"))
		{
			for(int j=0;j<arrcondendischarge.length;j++)
			{
				str_arrlst.add(arrcondendischarge[j]);
			}	
		}
		
	}
	
	private void multi_select(final String hvacvalue, int s) {
		// TODO Auto-generated method stub
		hvac=1;
		dbvalue(hvacvalue);
		System.out.println("hvacvalue"+hvacvalue+"tempfueltankloc="+tempfueltankloc);
		final Dialog add_dialog = new Dialog(NewHVAC.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.customizedialog);

		lin = (LinearLayout)add_dialog.findViewById(R.id.chk);
		
		 		
		TextView hdr = (TextView)add_dialog.findViewById(R.id.header);
		hdr.setText(hvacvalue);
		Button btnother = (Button)add_dialog.findViewById(R.id.addnew);
		btnother.setVisibility(v.GONE);
		showsuboption();
		Button btnok = (Button) add_dialog.findViewById(R.id.ok);
		btnok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval="";
								for (int i = 0; i < str_arrlst.size(); i++) {
									if (ch[i].isChecked()) {
										seltdval += ch[i].getText().toString() + "&#44;";
									}
								}
								
								
								finalselstrval = "";
								finalselstrval = seltdval;
								if(finalselstrval.equals(""))
								{
									cf.DisplayToast("Please check at least any 1 option to save");
								}
								else
								{	
									if(hvacvalue.equals("Fuel Tank Location"))
									{
										System.out.println("seltdval"+finalselstrval);
										fueltankvalue=finalselstrval;System.out.println("fuel="+fueltankvalue);
										tempfueltankloc = fueltankvalue;System.out.println("tempfueltankloc="+tempfueltankloc);
										tempfueltankloc = cf.replacelastendswithcomma(tempfueltankloc);System.out.println("fueltankvalue"+tempfueltankloc);
										fueltanklocnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempfueltankloc));
										fueltanklocnote.setVisibility(v.VISIBLE);
									}
									


									else if(hvacvalue.equals("Flue Type"))
									{
										fluetypevalue= finalselstrval;
										tempfluetype = fluetypevalue;
										tempfluetype = cf.replacelastendswithcomma(tempfluetype);
										fluetypenote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempfluetype));
										fluetypenote.setVisibility(v.VISIBLE);
									}
									
									else if(hvacvalue.equals("Tested For"))
									{
										testedforvalue=finalselstrval;
										temptestedfor = testedforvalue;
										temptestedfor = cf.replacelastendswithcomma(temptestedfor);
										testedfornote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+temptestedfor));
										testedfornote.setVisibility(v.VISIBLE);
									}
									
									else if(hvacvalue.equals("Air Handler Features"))
									{
										airhandlerfeatvalue=finalselstrval;
										tempairhandlerfeat = airhandlerfeatvalue;
										tempairhandlerfeat = cf.replacelastendswithcomma(tempairhandlerfeat);
										airhandlerfeatnote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempairhandlerfeat));
										airhandlerfeatnote.setVisibility(v.VISIBLE);
									}
									
									else if(hvacvalue.equals("Ductwork Type"))
									{
										ductworktypevalue=finalselstrval;
										tempductworktype = ductworktypevalue;
										tempductworktype = cf.replacelastendswithcomma(tempductworktype);
										ductworknote.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempductworktype));
										ductworknote.setVisibility(v.VISIBLE);
									}
									
									else if(hvacvalue.equals("Thermostat Type"))
									{
										thermostattypevalue=finalselstrval;
										tempthermotype = thermostattypevalue;
										tempthermotype = cf.replacelastendswithcomma(tempthermotype);
										thermostattypetv.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempthermotype));
										thermostattypetv.setVisibility(v.VISIBLE);
									}
									
									
									else if(hvacvalue.equals("Thermostat Location"))
									{
										thermolocationvalue=finalselstrval;
										tempthermolocation = thermolocationvalue;
										tempthermolocation = cf.replacelastendswithcomma(tempthermolocation);
										thermostatlocationtv.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempthermolocation));
										thermostatlocationtv.setVisibility(v.VISIBLE);
									}
									else if(hvacvalue.equals("Condensation Discharge"))
									{
										conddischangevalue=finalselstrval;
										tempconddischarge = conddischangevalue;
										tempconddischarge = cf.replacelastendswithcomma(tempconddischarge);
										conddisctv.setText(Html.fromHtml("<font><b><big>Selected: </big></b></font>"+tempconddischarge));
										conddisctv.setVisibility(v.VISIBLE);
									}
									
									
									add_dialog.cancel();
									hvac=0;
								}
								
					}
				
		});

		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hvac=0;
				add_dialog.cancel();
				
			}
		});
		
		Button btnvc = (Button)add_dialog.findViewById(R.id.setvc);
		btnvc.setVisibility(v.GONE);
		btnother.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				
			}
		});
		setvalue(hvacvalue);System.out.println("tttt="+tempfueltankloc);
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	
	private void setvalue(String hvacvalue) {
		// TODO Auto-generated method stub
		System.out.println("bathvalue="+tempfueltankloc);
		try
		{
			
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateHVAC+ " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
			System.out.println("count"+cur.getCount());
		    if (cur.getCount() > 0) 
		    {
				cur.moveToFirst();
				if(hvacvalue.equals("Fuel Tank Location"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_fueltankloc")));						 
				}
				else if(hvacvalue.equals("Flue Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_fluetype")));	
				}
				else if(hvacvalue.equals("Tested For"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_testedfor")));	
				}
				else if(hvacvalue.equals("Air Handler Features"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_airhandlerfeatures")));	
				}			
				else if(hvacvalue.equals("Ductwork Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_ducworktype")));	
				}			
				else if(hvacvalue.equals("Thermostat Type"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_thermotype")));	
				}
				else if(hvacvalue.equals("Thermostat Location"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_thermolocation")));	
				}
				else if(hvacvalue.equals("Condensation Discharge"))
				{
					str = db.decode(cur.getString(cur.getColumnIndex("fld_conddischarge")));	
				}
				
				if(str.contains(","))
			    {
			    	str = str.replace(",","&#44;");
			    }	
			}
		    else
		    {
			
		    	if(hvacvalue.equals("Fuel Tank Location") && hvacvalue!=null)
				{
					str = fueltankvalue;							 
				}
				else if(hvacvalue.equals("Flue Type") && hvacvalue!=null)
				{
					str = fluetypevalue;		
				}
				else if(hvacvalue.equals("Tested For") && hvacvalue!=null)
				{
					str = testedforvalue;
				}
				else if(hvacvalue.equals("Air Handler Features") && hvacvalue!=null)
				{
					str = airhandlerfeatvalue;
				}			
				else if(hvacvalue.equals("Ductwork Type") && hvacvalue!=null)
				{
					str =  ductworktypevalue;
				}			
				else if(hvacvalue.equals("Thermostat Type") && hvacvalue!=null)
				{
					str = thermostattypevalue;
				}
				else if(hvacvalue.equals("Thermostat Location") && hvacvalue!=null)
				{
					str  = thermolocationvalue;
				}
				else if(hvacvalue.equals("Condensation Discharge") && hvacvalue!=null)
				{
					str = conddischangevalue;
				}
		    }
			    if(!str.trim().equals(""))
				{
					cf.setValuetoCheckbox(ch, str);
				}	
		}catch (Exception e) {
			// TODO: handle exception
		}
	    	
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	
	
	private void showsuboption() {
		// TODO Auto-generated method stub
		  ch = new CheckBox[str_arrlst.size()];
			
			for (int i = 0; i < str_arrlst.size(); i++) {
	        		ch[i] = new CheckBox(this);
					ch[i].setText(str_arrlst.get(i));
					ch[i].setTextColor(Color.BLACK);
					
					lin.addView(ch[i]);
					ch[i].setOnCheckedChangeListener(new CHListener(i));
					
			}
	}
	
	class CHListener implements OnCheckedChangeListener, android.widget.CompoundButton.OnCheckedChangeListener
	{
    int clkpod = 0;
		public CHListener(int i) {
			// TODO Auto-generated constructor stub
			clkpod = i;
		}

		
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
		
		//seltdval="";
			if(ch[clkpod].isChecked())
			{
				seltdval += ch[clkpod].getText().toString() + "&#44;";
			}
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	class myspinselectedlistener implements OnItemSelectedListener
	{
         int var=0;
        
		public myspinselectedlistener(int i) {
			// TODO Auto-generated constructor stub
			var=i;
			 getspinselectedname="";
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			getspinselectedname = arg0.getItemAtPosition(arg2).toString();
			switch(var)
			{
				case 1:
					System.out.println("GGGGGGG="+getspinselectedname);
					 getheatdeliverytype = getspinselectedname;System.out.println("SSD"+getheatdeliverytype);
					/* if(getheatdeliverytype.equals("Hot Water Heat") || getheatdeliverytype.equals("Boiler") ||  getheatdeliverytype.equals("Radiant"))
		    		 {		
						 ((LinearLayout)findViewById(R.id.lin01)).setVisibility(View.VISIBLE);
						 if(getheatdeliverytype.equals("Radiant"))
						 {
							 ((TextView)findViewById(R.id.watersystemtit)).setVisibility(View.GONE);
							 ((TextView)findViewById(R.id.watersyscolon)).setVisibility(View.GONE);
							 sp_watersystype.setVisibility(View.GONE);
						 }
						 else
						 {
							 ((TextView)findViewById(R.id.watersystemtit)).setVisibility(View.VISIBLE);
							 ((TextView)findViewById(R.id.watersyscolon)).setVisibility(View.VISIBLE);
							 sp_watersystype.setVisibility(View.VISIBLE);
						 }
		    		 }
					 else
					 {
						 ((LinearLayout)findViewById(R.id.lin01)).setVisibility(View.GONE);
					 }
		    			 */ 
				break;
				case 2:
					 getenergysource = getspinselectedname;
					 if(!getenergysource.equals("--Select--"))
		    		 {			
						 if(getenergysource.equals("Other"))
				    	  {
							 ed_otherenergysource.setVisibility(arg1.VISIBLE);
				    		 ed_otherenergysource.requestFocus();
				    		
				    	  }
				    	  else
				    	  {
				    		  ed_otherenergysource.setText("");
				    		  ed_otherenergysource.setVisibility(arg1.GONE);
				    	  }
						 
						 if(getenergysource.equals("Gas") || getenergysource.equals("Liquid Propane") || getenergysource.equals("Kerosene"))
						 {
							 ((LinearLayout)findViewById(R.id.energysource1)).setVisibility(View.VISIBLE);
							 ((LinearLayout)findViewById(R.id.energysource2)).setVisibility(View.VISIBLE);	 
							 ((LinearLayout)findViewById(R.id.energysource3)).setVisibility(View.VISIBLE);
							 
						 }
						 else
						 {
							 ((LinearLayout)findViewById(R.id.energysource1)).setVisibility(View.GONE);
							 ((LinearLayout)findViewById(R.id.energysource2)).setVisibility(View.GONE);
							 ((LinearLayout)findViewById(R.id.energysource3)).setVisibility(View.GONE);
							 ed_othergasshutoff.setVisibility(arg1.GONE);
						 }
		    		 }
					 else
					 {
						 ((LinearLayout)findViewById(R.id.energysource1)).setVisibility(View.GONE);
						 ((LinearLayout)findViewById(R.id.energysource2)).setVisibility(View.GONE);
						 ((LinearLayout)findViewById(R.id.energysource3)).setVisibility(View.GONE);
					 }
		    			  
				break;
				case 3:
					System.out.println("getheatingtype"+getheatingtype);
					 getheatingtype = getspinselectedname;
			    	  Cursor cur;
			    	  try
						{
			    		  if(getheatingtype.equals("Other"))
			    		  {
			    			  ed_otherheattype.setVisibility(arg1.VISIBLE);
			    			  ed_otherheattype.requestFocus();
			    			 
			    			  System.out.println("tttt="+strotherheattype);
			    			  cur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatingtypeother='"+strotherheattype+"'", null);
			    		  }
			    		  else
			    		  {
			    			  ed_otherheattype.setVisibility(arg1.GONE);
			    			  cur = db.hi_db.rawQuery("select * from " + db.CreateHeating + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_heatintype='"+getheatingtype+"'", null);
			    		  }
					   }
			    	  catch (Exception e) {
						// TODO: handle exception
					  }
				break;
				  case 13:
			    	  getgasshutoff = getspinselectedname;
			    	  if(getgasshutoff.equals("Other"))
			    	  {
			    		  ed_othergasshutoff.setVisibility(arg1.VISIBLE);
			    		  ed_othergasshutoff.requestFocus();
			    		 
			    	  }
			    	  else
			    	  {

			    		  ed_othergasshutoff.setText("");
			    		  ed_othergasshutoff.setVisibility(arg1.GONE);
			    	  }
			    	  break;
				  case 14:
			    	  getfluetype = getspinselectedname;
			    	  if(getfluetype.equals("Other"))
			    	  {
			    		  ed_otherfluetype.setVisibility(arg1.VISIBLE);
			    		  ed_otherfluetype.requestFocus();
			    		 
			    	  }
			    	  else
			    	  {

			    		  ed_otherfluetype.setText("");
			    		  ed_otherfluetype.setVisibility(arg1.GONE);
			    	  }
			    	  break;
				  case 5:
			    	  getheatingage = getspinselectedname;
			    	  if(getheatingage.equals("Other"))
			    	  {
			    		  ed_otherheatingage.setVisibility(arg1.VISIBLE);
			    		  ed_otherheatingage.requestFocus();
			    		
			    	  }
			    	  else
			    	  {

			    		  ed_otherheatingage.setText("");
			    		  ed_otherheatingage.setVisibility(arg1.GONE);
			    	  }
			    	  break;
				  case 25:
			    	  getzonename= getspinselectedname;
			    	  if(getzonename.equals("Other"))
			    	  {
			    		  ed_otherzonename.setVisibility(arg1.VISIBLE);
			    		  ed_otherzonename.requestFocus();
			    		
			    	  }
			    	  else
			    	  {
			    		  ed_otherzonename.setText("");
			    		  ed_otherzonename.setVisibility(arg1.GONE);
			    	  }
			    	  break;
				  case 29:
			    	  getzonetonnage= getspinselectedname;
			    	  if(getzonetonnage.equals("Other"))
			    	  {
			    		  ed_otherzonetonnage.setVisibility(arg1.VISIBLE);
			    		  ed_otherzonetonnage.requestFocus();
			    		
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
			    	  }
			    	  else
			    	  {
			    		  ed_otherzonetonnage.setText("");
			    		  ed_otherzonetonnage.setVisibility(arg1.GONE);
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
			    	  }
			    	  break;
				  case 30:
			    	  getbtu= getspinselectedname;
			    	  if(getbtu.equals("Other"))
			    	  {
			    		  ed_otherbtu.setVisibility(arg1.VISIBLE);
			    		  ed_otherbtu.requestFocus();
			    		  
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
			    	  }
			    	  else
			    	  {
			    		  ed_otherbtu.setText("");
			    		  ed_otherbtu.setVisibility(arg1.GONE);
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
			    	  }
			    	  break;
				  case 31:
			    	  getexternalunit= getspinselectedname;
			    	  if(getexternalunit.equals("Other"))
			    	  {
			    		  ((EditText)findViewById(R.id.other_externalunit)).setVisibility(arg1.VISIBLE);
			    		  ((EditText)findViewById(R.id.other_externalunit)).requestFocus();
			    		 
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
			    	  }
			    	  else
			    	  {
			    		  ((EditText)findViewById(R.id.other_externalunit)).setText("");
			    		  ((EditText)findViewById(R.id.other_externalunit)).setVisibility(arg1.GONE);
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
			    	  }
			    	  break;
				  case 32:
			    	  getinternalunit= getspinselectedname;
			    	  if(getinternalunit.equals("Other"))
			    	  {
			    		  ((EditText)findViewById(R.id.other_internalunit)).setVisibility(arg1.VISIBLE);
			    		  ((EditText)findViewById(R.id.other_internalunit)).requestFocus();
			    		 
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.VISIBLE);
			    	  }
			    	  else
			    	  {
			    		  ((EditText)findViewById(R.id.other_internalunit)).setText("");
			    		  ((EditText)findViewById(R.id.other_internalunit)).setVisibility(arg1.GONE);
			    		 // ((TextView)findViewById(R.id.tontxt)).setVisibility(View.GONE);
			    	  }
			    	  break;
				  case 15:
					  getnoofzones = getspinselectedname;
			    	  break;
				  case 16:
					  getzoneno = getspinselectedname;
			    	  break;
				  case 6:
					  getheatbrand = getspinselectedname;
			    	  break;
				  case 7:
			    	  getfurnace = getspinselectedname;
			    	  break;
				  case 10:
			    	  getmotorblower = getspinselectedname;
			    	  break;
				  case 18:
			    	  getsupplyregister = getspinselectedname;
			    	  break;  
			    	  
			      case 19:
			    	  getreturnregister = getspinselectedname;
			    	  break;  
			      case 20:
			    	  getmultizonesystem = getspinselectedname;
			    	  break;  
			    	  
				  /*case 4:
			    	  getheatingno = getspinselectedname;
			    	  if(getheatingno.equals("Other"))
			    	  {
			    		  ed_otherheatignno.setVisibility(arg1.VISIBLE);
			    		  ed_otherheatignno.requestFocus();
			    		  strotherheatingnum = ed_otherheatignno.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_otherheatignno.setText("");
			    		  ed_otherheatignno.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;	
				 
				  
			   
			        
			      case 8:			    	  
			    	  getwatersystemtype = getspinselectedname;
			    	  break;
			      case 9:
			    	  getradiators = getspinselectedname;
			    	  break;
			    	  
			      
			      case 11:
			    	  getwoodstoves = getspinselectedname;
			    	  if(getwoodstoves.equals("Other"))
			    	  {
			    		  ed_otherwoodstoves.setVisibility(arg1.VISIBLE);
			    		  ed_otherwoodstoves.requestFocus();
			    		  strotherwoodstoves = ed_otherwoodstoves.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_otherwoodstoves.setText("");
			    		  ed_otherwoodstoves.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
			      case 12:
			    	  getfueltank = getspinselectedname;
			    	  break;  
			      case 13:
			    	  getfurnfeatures = getspinselectedname;
			    	  break;  
			    	  
			        
			      case 16:
			    	  getcoolingequipment = getspinselectedname;
			    	  break;
			      case 17:
			    	  getcoolingenrgysource= getspinselectedname;
			    	  if(getcoolingenrgysource.equals("Other"))
			    	  {
			    		  ed_othercoolingeergysource.setVisibility(arg1.VISIBLE);
			    		  ed_othercoolingeergysource.requestFocus();
			    		  strcoolingenergysource = ed_othercoolingeergysource.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_othercoolingeergysource.setText("");
			    		  ed_othercoolingeergysource.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  
			    	  break;
			      case 18:
			    	  getcenterairmanuf= getspinselectedname;
			    	  break;
			      case 19:
			    	  getcoolinoofzones= getspinselectedname;
			    	  if(getcoolinoofzones.equals("Other"))
			    	  {
			    		  ed_coolingnoofzones.setVisibility(arg1.VISIBLE);
			    		  ed_coolingnoofzones.requestFocus();
			    		  strcoolingnoofzones = ed_coolingnoofzones.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_coolingnoofzones.setText("");
			    		  ed_coolingnoofzones.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
			      case 20:
			    	  getcoolingage= getspinselectedname;
			    	  if(getcoolingage.equals("Other"))
			    	  {
			    		  ed_coolingage.setVisibility(arg1.VISIBLE);
			    		  ed_coolingage.requestFocus();
			    		  strcoolingage = ed_coolingage.getText().toString();
			    	  }
			    	  else
			    	  {
			    		  ed_coolingage.setText("");
			    		  ed_coolingage.setVisibility(arg1.INVISIBLE);
			    	  }
			    	  break;
			      case 21:
			    	  getconddischarge= getspinselectedname;
			    	  break;
			      case 22:
			    	  getacsystem= getspinselectedname;
			    	  break;
			      case 23:
			    	  getthermostats= getspinselectedname;
			    	  break;
			      case 24:
			    	  getgaslog= getspinselectedname;
			    	  break;
			      
			      case 26:
			    	  getzoneunittype= getspinselectedname;
			    	  break;
			      case 27:
			    	  getzone5yearrep= getspinselectedname;
			    	  break;
			      case 28:
			    	  gezonetestedfor= getspinselectedname;
			    	  break;
			     
			    	  break;*/
			    	  
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	protected void back() {
		// TODO Auto-generated method stub
		Intent intent;
		
		if(getcurrentview.equals("start"))
		{
			db.userid();
			intent = new Intent(NewHVAC.this, LoadModules.class);
			Bundle b1 = new Bundle();
			b1.putString("selectedid", getselectedho);			
			intent.putExtras(b1);
			
			Bundle b2 = new Bundle();
			b2.putString("inspectorid", db.UserId);			
			intent.putExtras(b2);
			startActivity(intent);System.out.println("finished");
			finish();
		}
		else
		{	
			 if(gettempoption.equals("Report Section/Module"))
		        {
		       	  intent = new Intent(NewHVAC.this, CreateTemplate.class);
		        }
		        else
		        {
		       	 intent = new Intent(NewHVAC.this, CreateModule.class);
		        }
		        System.out.println("gettemp="+gettempoption);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("tempoption", gettempoption);			
				intent.putExtras(b5);
				startActivity(intent);
				finish();
		}
		
       
	}
}
