package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.Registration.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.backup.RestoreObserver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;



public class LoginActivity extends Activity {
	Button loginbtn_submit,loginbtn_cancel;
	EditText etlogin_username,etlogin_password,forgot_et_username,forgot_et_email;
	String strlogin_username="",strlogin_password="",reguser="",chkstatus="",erro_msg="",resetuser="",resetpass="",forgotusername="",forgotemail="";
	ImageView tvlogin_register;
	CommonFunction cf;
	DatabaseFunction db;
	Webservice_config wb;
	Progress_staticvalues p_sv;
	private int handler_msg=3;
	Dialog forgotdialog;
	int forgot=0,k=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initialize();
	}
	
	private void initialize() {
		// TODO Auto-generated method stub
		Bundle bundle=getIntent().getExtras();
		if(bundle!=null)
		{
			if(bundle.getString("status")!=null)
			{
			  chkstatus=bundle.getString("status");
			  System.out.println("chkstatus="+chkstatus);
			 
			  if(chkstatus.equals("register"))
			  {
				  reguser =bundle.getString("user");
			  }
			  else if(chkstatus.equals("resetpassword"))
			  {
				  resetuser =bundle.getString("username");
				  resetpass =bundle.getString("password");
				  System.out.println("resetpass="+resetpass);
			  }
			  else if(chkstatus.equals("forgotpassword"))
			  {
				  resetuser =bundle.getString("username");
				  resetpass =bundle.getString("password");
				  System.out.println("forgotpass="+resetpass);
			  }
			}
		}
		
		declaration();
	}

	private void declaration() {
		// TODO Auto-generated method stub
		
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.Registration + " where fld_logout='1'", null);
			System.out.println("select * from " + db.Registration + " where fld_logout='1'");
			if(cur.getCount() >= 1)
			{
				startActivity(new Intent(this, HomeScreen.class));
				finish();
			}
			
			cur.close();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		setContentView(R.layout.activity_login);
		etlogin_username = (EditText)findViewById(R.id.et_username);
		etlogin_password = (EditText)findViewById(R.id.et_password);
		loginbtn_submit = (Button)findViewById(R.id.login_submit);
		loginbtn_cancel = (Button)findViewById(R.id.login_cancel);
		tvlogin_register = (ImageView)findViewById(R.id.register);
		
		etlogin_username.addTextChangedListener(new CustomTextWatcher(etlogin_username));
		
		etlogin_password.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (etlogin_password.getText().toString().startsWith(" "))
		        {
					etlogin_password.setText("");
		        }
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		etlogin_password.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        boolean handled = false;
		        if (actionId == EditorInfo.IME_ACTION_GO) {
		            login();
		            handled = true;
		        }
		        return handled;
		    }
		});
		
		tvlogin_register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,Registration.class));
				finish();
			}
		});
		
        ((TextView)findViewById(R.id.forgot)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,ForgotPassword.class));
				
			}
		});
		
		loginbtn_cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				etlogin_username.setText("");
				etlogin_password.setText("");
				finish();
			}
		});
		
		loginbtn_submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				login();
			}
		});
		
		
		 if(chkstatus.equals("register"))
		 {
			try
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.Registration + " where fld_username='"+db.encode(reguser)+"' and fld_logout='0'", null);
				if(cur.getCount() >= 1)
				{
					cur.moveToFirst();
					System.out.println("username="+cur.getString(cur.getColumnIndex("fld_username")));
					etlogin_username.setText(db.decode(cur.getString(cur.getColumnIndex("fld_username"))));
					etlogin_password.setText(db.decode(cur.getString(cur.getColumnIndex("fld_password"))));
				}
				cur.close();
			}catch (Exception e) {
				// TODO: handle exception
			}
		 }
		 else if(chkstatus.equals("resetpassword") || chkstatus.equals("forgotpassword"))
		 {
			    etlogin_username.setText(resetuser);
				etlogin_password.setText(resetpass);
		 }
	}
	


	protected void login() {
		// TODO Auto-generated method stub
		strlogin_username = etlogin_username.getText().toString();
		strlogin_password = etlogin_password.getText().toString();
		
		if(!"".equals(strlogin_username.trim())&& !"".equals(strlogin_password.trim()))
		{
			  if(cf.isInternetOn())
			  {
				   new Start_xporting().execute("");
			  }
			  else
			  {
				  cf.DisplayToast("Please enable your internet connection.");
				  
			  } 
		}
		else
		{
			if(strlogin_username.trim().equals(""))
			{
			    cf.DisplayToast("Please enter Username");
				etlogin_username.requestFocus();
			}
			else if(strlogin_password.trim().equals(""))
			{
				cf.DisplayToast("Please enter Password");
				etlogin_password.requestFocus();
			}
		}
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        if(k==0)
		        {
		          p_sv.progress_title="Checking user authentication";
			      p_sv.progress_Msg="Please be patient as we process your initial log in authentication. This is a one time password.";
		        }
		        else if(k==1)
		        {
		        	p_sv.progress_title="Retrieving password";
				    p_sv.progress_Msg="Please wait we are retrieving your password. It may take few minutes.";
		        }
				
				startActivityForResult(new Intent(LoginActivity.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		 if(k==0)
				     {
					    Send_record_to_server();
				     }
			 		 else if(k==1)
			 		 {
			 			 Send_forgotpassword();
			 		 }
					
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		                return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		if(k==0)
		    		{
		    		  failure_alert();
		    		}
		    		/*else if(k==1)
		    		{
		    			forgot_failure_alert();
		    		}*/
		    		
		    	}
		    	else if(handler_msg==1)
		    	{
		    		if(k==0)
		    		{
		    		  success_alert();
		    		}
		    		/*else if(k==1)
		    		{
		    			forgot_success_alert();
		    		}*/
		    	}
		    	
		    }
	}
	private void Send_record_to_server() throws IOException, XmlPullParserException {
		
		    String userid="";
		    
		    SoapObject request = new SoapObject(wb.NAMESPACE,"USERAUTHENTICATION");
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	 		envelope.dotNet = true;
	 		
	 		request.addProperty("UserName",strlogin_username);
	 		request.addProperty("Password",strlogin_password);
	 		
	 		envelope.setOutputSoapObject(request);
	 		System.out.println("loginrequest="+request);
	 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
	        SoapObject response = null;
	        try
	        {System.out.println("try");
	              androidHttpTransport.call(wb.NAMESPACE+"USERAUTHENTICATION", envelope);
	              response = (SoapObject)envelope.getResponse();
	              System.out.println("USERAUTHENTICATIONresult "+response);
	              
	              if(response.getProperty("StatusCode").toString().equals("0"))
	  			{
	  	        	 
	  	        	try
	  				{
	  	        		//db.hi_db.execSQL("delete from " + db.Registration + " where fld_logout='0'");
	  	        		String inspectorid="",firstname="",lastname="",middlename="",address1="",address2="",zip="",city="",state="",county="",phone="",email="",
	  	        			   licensenumber="",licenseexpirydate="",licensetype="",otherlicensetype="",companyname="",companylogo="",headshot="",signature="",
	  	        			   initials="",companyphone="",companyemail="",license="",logout="",fax="",website="",date="";
	  	        		
	  	        		inspectorid = (String.valueOf(response.getProperty("InspectorId")).equals("anyType{}"))?"":String.valueOf(response.getProperty("InspectorId"));
	  	        		firstname = (String.valueOf(response.getProperty("fld_firstname")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_firstname"));
	  	        		lastname = (String.valueOf(response.getProperty("fld_lastname")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_lastname"));
	  	        		middlename = (String.valueOf(response.getProperty("fld_middlename")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_middlename"));
	  	        		address1 = (String.valueOf(response.getProperty("fld_address1")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_address1"));
	  	        		address2 = (String.valueOf(response.getProperty("fld_address2")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_address2"));
	  	        		zip = (String.valueOf(response.getProperty("fld_zipcode")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_zipcode"));
	  	        		city = (String.valueOf(response.getProperty("fld_city")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_city"));
	  	        		state = (String.valueOf(response.getProperty("fld_state")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_state"));
	  	        		county = (String.valueOf(response.getProperty("fld_county")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_county"));
	  	        		phone = (String.valueOf(response.getProperty("fld_contactphone")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_contactphone"));
	  	        		email = (String.valueOf(response.getProperty("fld_email")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_email"));
	  	        		strlogin_username = (String.valueOf(response.getProperty("fld_username")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_username"));
	  	        		strlogin_password = (String.valueOf(response.getProperty("fld_password")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_password"));
	  	        		licensenumber = (String.valueOf(response.getProperty("fld_licensenumber")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_licensenumber"));
	  	        		licenseexpirydate = (String.valueOf(response.getProperty("fld_licenseexpirydate")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_licenseexpirydate"));
	  	        		licensetype = (String.valueOf(response.getProperty("fld_licensetype")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_licensetype"));
	  	        		otherlicensetype = (String.valueOf(response.getProperty("fld_otherlicensetype")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_otherlicensetype"));
	  	        		companyname = (String.valueOf(response.getProperty("fld_companyname")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_companyname"));
	  	        		companylogo = (String.valueOf(response.getProperty("fld_companylogo")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_companylogo"));
	  	        		headshot = (String.valueOf(response.getProperty("fld_headshot")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_headshot"));
	  	        		signature = (String.valueOf(response.getProperty("fld_signature")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_signature"));
	  	        		initials = (String.valueOf(response.getProperty("fld_initials")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_initials"));
	  	        		companyphone = (String.valueOf(response.getProperty("fld_companyphone")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_companyphone"));
	  	        		companyemail = (String.valueOf(response.getProperty("fld_companyemail")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_companyemail"));
	  	        		license = (String.valueOf(response.getProperty("fld_uploadlicense")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_uploadlicense"));
	  	        		logout = (String.valueOf(response.getProperty("fld_logout")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_logout"));
	  	        		fax = (String.valueOf(response.getProperty("fld_compfax")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_compfax"));
	  	        		website = (String.valueOf(response.getProperty("fld_compwebsite")).equals("anyType{}"))?"":String.valueOf(response.getProperty("fld_compwebsite"));
	  	        		date = (String.valueOf(response.getProperty("CreatedDate")).equals("anyType{}"))?"":String.valueOf(response.getProperty("CreatedDate"));
	  	        		
	  	        		File direct = new File(Environment.getExternalStorageDirectory() + "/HI");

	  				    if(!direct.exists())
	  				    {
	  				         direct.mkdir() ;
	  				    }
	  				    
	  	        		 try
	  						{
	  							 if(!headshot.equals(""))
	  							 {
	  									URL ulrn = new URL(headshot);
	  								    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
	  								    InputStream is = con.getInputStream();
	  								    Bitmap bmp = BitmapFactory.decodeStream(is);
	  								   
	  								    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	  								    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
	  								    byte[] b = baos.toByteArray();
	  								    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
	  								    byte[] decode = Base64.decode(headshot_insp.toString(), 0);
	  								    
	  								      String FILENAME = "HIHeadshot"+inspectorid +".png";
	  						        	  File f = new File(direct,FILENAME);
	  						        	  
	  						        	  FileOutputStream fos = new FileOutputStream(f);
	  						        	  fos.write(decode);
	  									  fos.close();
	  								 
	  										
	  							 }
	  						    
	  						}
	  						catch (Exception e1){
	  							System.out.println("the exception in headshot is "+e1.getMessage());
	  					
	  						}
	  	        		 
	  	        		 try
	  						{
	  							 if(!companylogo.equals(""))
	  							 {
	  								URL ulrn = new URL(companylogo);
	  							    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
	  							    InputStream is = con.getInputStream();
	  							    Bitmap bmp = BitmapFactory.decodeStream(is);
	  							   
	  							    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	  							    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
	  							    byte[] b = baos.toByteArray();
	  							    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
	  							    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

	  							    String FILENAME = "HILogo"+inspectorid+".png";System.out.println("FILENAME "+FILENAME);
	  							    File f = new File(direct,FILENAME);					        	  
	  					        	FileOutputStream fos = new FileOutputStream(f);
	  								fos.write(decode);
	  								fos.close();	
	  							 }
	  						    
	  						}
	  						catch (Exception e1){
	  							
	  						}
	  	        		 
	  	        		 try
	  						{
	  							 if(!signature.equals(""))
	  							 {
	  								URL ulrn = new URL(signature);
	  							    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
	  							    InputStream is = con.getInputStream();
	  							    Bitmap bmp = BitmapFactory.decodeStream(is);
	  							   
	  							    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	  							    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
	  							    byte[] b = baos.toByteArray();
	  							    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
	  							    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

	  							    String FILENAME = "HISignature"+inspectorid+".png";System.out.println("FILENAME "+FILENAME);
	  							    File f = new File(direct,FILENAME);					        	  
	  					        	FileOutputStream fos = new FileOutputStream(f);
	  								fos.write(decode);
	  								fos.close();	
	  							 }
	  						    
	  						}
	  						catch (Exception e1){
	  							
	  						}
	  	        		 
	  	        		 try
	  						{
	  							 if(!initials.equals(""))
	  							 {
	  								URL ulrn = new URL(initials);
	  							    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
	  							    InputStream is = con.getInputStream();
	  							    Bitmap bmp = BitmapFactory.decodeStream(is);
	  							   
	  							    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	  							    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
	  							    byte[] b = baos.toByteArray();
	  							    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
	  							    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

	  							    String FILENAME = "HIInitials"+inspectorid+".png";System.out.println("FILENAME "+FILENAME);
	  							    File f = new File(direct,FILENAME);					        	  
	  					        	FileOutputStream fos = new FileOutputStream(f);
	  								fos.write(decode);
	  								fos.close();	
	  							 }
	  						    
	  						}
	  						catch (Exception e1){
	  							
	  						}
	  	        		 
	  	        		 try
	  						{
	  							 if(!license.equals(""))
	  							 {
	  								URL ulrn = new URL(license);
	  							    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
	  							    InputStream is = con.getInputStream();
	  							    Bitmap bmp = BitmapFactory.decodeStream(is);
	  							   
	  							    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	  							    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
	  							    byte[] b = baos.toByteArray();
	  							    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
	  							    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

	  							    String FILENAME = "HILicense"+inspectorid+".png";System.out.println("FILENAME "+FILENAME);
	  							    File f = new File(direct,FILENAME);					        	  
	  					        	FileOutputStream fos = new FileOutputStream(f);
	  								fos.write(decode);
	  								fos.close();	
	  							 }
	  						    
	  						}
	  						catch (Exception e1){
	  							
	  						}
	  	        		Cursor cur1 = db.hi_db.rawQuery("select * from " + db.Registration + " where fld_userid='"+inspectorid+"'",null);
	  	    			System.out.println("select * from " + db.Registration + " where fld_userid='"+inspectorid+"'");
	  	    			System.out.println("count="+cur1.getCount());
	                      if(cur1.getCount()>0)
	                      {
	                      	db.hi_db.execSQL("UPDATE "+ db.Registration +"  SET fld_firstname='"+db.encode(firstname) + "',fld_lastname='"+db.encode(lastname)+"',fld_middlename='"+db.encode(middlename)+"'," +
	                      			"fld_address1='"+db.encode(address1) + "',fld_address2='"+ db.encode(address2) + "',fld_zipcode='"+ db.encode(zip) + "',fld_city='"+db.encode(city) + "'," +
	                      			"fld_state='"+ db.encode(state) + "',fld_county='"+db.encode(county)+ "',fld_contactphone='"+db.encode(phone) + "',fld_email='"+db.encode(email)+"'," +
	                      			"fld_username='"+db.encode(strlogin_username)+"',fld_password='"+db.encode(strlogin_password)+"',fld_licensenumber='"+db.encode(licensenumber)+"'," +
	                      			"fld_licenseexpirydate='"+db.encode(licenseexpirydate)+"',fld_licensetype='"+db.encode(licensetype)+"',fld_otherlicensetype='"+db.encode(otherlicensetype)+"'," +
	                      			"fld_companyname='"+db.encode(companyname)+"',fld_companylogo='"+db.encode(companylogo)+"',fld_headshot='" +db.encode(headshot) + "',fld_signature='"+db.encode(signature)+"'," +
	                      			"fld_initials='"+db.encode(initials)+"',fld_companyphone='"+db.encode(companyphone)+"',fld_companyemail='"+db.encode(companyemail)+"',fld_uploadlicense='"+db.encode(license)+"'," +
	                      			"fld_logout='1',fld_compfax='"+db.encode(fax)+"',fld_compwebsite='"+db.encode(website)+"' where fld_userid='"+inspectorid+"'");
	                      }
	                      else
	                      {
	  	        		   db.hi_db.execSQL("INSERT INTO "
	  	 						+ db.Registration
	  	 						+ " (fld_userid,fld_firstname,fld_lastname,fld_middlename,fld_address1,fld_address2,fld_zipcode,fld_city,fld_state," +
	  	 						"fld_county,fld_contactphone,fld_email,fld_username,fld_password,fld_licensenumber,fld_licenseexpirydate,fld_licensetype,fld_otherlicensetype,fld_companyname,fld_companylogo," +
	  	 						"fld_headshot,fld_signature,fld_initials,fld_companyphone,fld_companyemail,fld_uploadlicense,fld_logout,fld_compfax,fld_compwebsite)"
	  	 						+ " VALUES ('" + inspectorid + "','"+db.encode(firstname) + "','"+db.encode(lastname)+"','"+db.encode(middlename)+"',"+
	  	 						"'"+db.encode(address1) + "','"+ db.encode(address2) + "','"+ db.encode(zip) + "','"+db.encode(city) + "',"+
	  	 						"'"+ db.encode(state) + "','"+db.encode(county)+ "','"+db.encode(phone) + "','"+db.encode(email)+"','"+db.encode(strlogin_username)+"','"+db.encode(strlogin_password)+"'," +
	  	 						"'"+db.encode(licensenumber)+"','"+db.encode(licenseexpirydate)+"','"+db.encode(licensetype)+"','"+db.encode(otherlicensetype)+"','"+db.encode(companyname)+"'," +
	  	 						"'"+db.encode(companylogo)+"','" +db.encode(headshot) + "','"+db.encode(signature)+"','"+db.encode(initials)+"',"+
	  	 						"'"+db.encode(companyphone)+"','"+db.encode(companyemail)+"','"+db.encode(license)+"','1','"+db.encode(fax)+"','"+db.encode(website)+"')");
	                      }
	                      cur1.close();
	  				}
	  				catch (Exception e) {
	  					// TODO: handle exception
	  				}
	  				handler_msg=1;
	  			}
	  			else
	  			{
	  				erro_msg=response.getProperty("StatusMessage").toString();
	  				handler_msg=0;
	  				
	  			}
	              
	        }
	        catch(Exception e)
	        {
	        	handler_msg=0;
	             e.printStackTrace();
	        }
	        
	       
	}
	
	public void forgot_failure_alert() {
		// TODO Auto-generated method stub
		String title="Forgot Password";
			
		String msg="Sorry, you have some problem in "+title+". Please try again later.";
		if(!erro_msg.equals(""))
		{
			msg=erro_msg;
			erro_msg="";
		}
		
		AlertDialog al = new AlertDialog.Builder(LoginActivity.this).setMessage(msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.show();
	}

	public void forgot_success_alert() {
		// TODO Auto-generated method stub
        String title="Retrieved password";
		
		AlertDialog al = new AlertDialog.Builder(LoginActivity.this).setMessage("Password has been successfully sent to your registered email.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				System.out.println("sucess="+forgotusername);
				etlogin_username.setText(forgotusername);
				try
				{
					Cursor cur = db.hi_db.rawQuery("select * from " + db.Registration + " where fld_username='"+db.encode(forgotusername)+"'", null);
					if(cur.getCount() >= 1)
					{
						cur.moveToFirst();
						etlogin_password.setText(db.decode(cur.getString(cur.getColumnIndex("fld_password"))));
					}
					cur.close();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				k=0;
			}
		}).setTitle(title+" success").create();
		al.show();
	}

	private void Send_forgotpassword() throws IOException, XmlPullParserException {
		
		forgotusername = forgot_et_username.getText().toString();
		forgotemail = forgot_et_email.getText().toString();
		
	    SoapObject request = new SoapObject(wb.NAMESPACE,"FORGOTPASSWORD");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
 		envelope.dotNet = true;
 		
 		request.addProperty("Username",forgotusername);
 		request.addProperty("Emailid",forgotemail);
 		
 		envelope.setOutputSoapObject(request);
 		System.out.println("forgotrequest="+request);
 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
        SoapObject response = null;
        try
        {
              androidHttpTransport.call(wb.NAMESPACE+"FORGOTPASSWORD", envelope);
              response = (SoapObject)envelope.getResponse();
              System.out.println("FORGOTPASSWORDresult "+response);
              
              
        }
        catch(Exception e)
        {
             e.printStackTrace();
        }
        if(response.getProperty("StatusCode").toString().equals("0"))
		  {
      	      db.CreateTable(1);
            	db.hi_db.execSQL("UPDATE "+ db.Registration +"  SET fld_logout='2' where fld_username='"+db.encode(forgotusername)+"'");
        	System.out.println("UPDATE "+ db.Registration +"  SET fld_logout='2' where fld_username='"+db.encode(forgotusername)+"'");
        	forgot=0;
			handler_msg=1;
		 }
		 else
		 {
			erro_msg=response.getProperty("StatusMessage").toString();System.out.println("erro_msgweb="+erro_msg);
			//forgot=0;
			handler_msg=0;
		 }
       
}
	
	public void success_alert() {
		// TODO Auto-generated method stub
		String title="Login";
		
		Intent intent=new Intent(LoginActivity.this,HomeScreen.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				finish();
				
	
	}

	public void failure_alert() {
		// TODO Auto-generated method stub
		String title="Login";
		
		
		String msg="Sorry, you have some problem in "+title+". Please try again later.";
		if(!erro_msg.equals(""))
		{
			msg=erro_msg;
			erro_msg="";
		}
		
		AlertDialog al = new AlertDialog.Builder(LoginActivity.this).setMessage(msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.show();
	}

	@Override
    protected void onSaveInstanceState(Bundle outState) {	 
	 
		outState.putInt("kval", k);
        outState.putInt("handlermsg", handler_msg);
       /* outState.putInt("forgotval",forgot);
        outState.putString("forgotusername", forgot_et_username.getText().toString());
        outState.putString("forgotemail", forgot_et_email.getText().toString());
        outState.putString("Error",erro_msg);System.out.println("erro_msgsave="+erro_msg);*/
        super.onSaveInstanceState(outState);
    }
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	
    	handler_msg = savedInstanceState.getInt("handlermsg");
    	k = savedInstanceState.getInt("kval");
    	/*forgot = savedInstanceState.getInt("forgotval");
    	
    	if(forgot==1)
    	{
    		forgotpassword();
    		forgotusername = savedInstanceState.getString("forgotusername");
    		forgotemail = savedInstanceState.getString("forgotemail");
    		forgot_et_username.setText(forgotusername);
    		forgot_et_email.setText(forgotemail);
    		erro_msg = savedInstanceState.getString("Error");System.out.println("erro_msgre"+erro_msg);
    	}*/
    	
		
		
		
    	
    	
    	if(handler_msg==0)
    	{
    		if(k==0)
    		{
    		  failure_alert();
    		}
    		/*else if(k==1)
    		{
    			forgot_failure_alert();
    		}*/
    	}
    	else if(handler_msg==1)
    	{
    		if(k==0)
    		{
    		  success_alert();
    		}
    		/*else if(k==1)
    		{
    			forgot_success_alert();
    		}*/
    	}
	      
        super.onRestoreInstanceState(savedInstanceState);
    }
	protected void goback() {
		// TODO Auto-generated method stub
		Intent startMain = new Intent(Intent.ACTION_MAIN);
		startMain.addCategory(Intent.CATEGORY_HOME);
		startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(startMain);
	}
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goback();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	

}
