package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.ArrayModuleList;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.ExpandableListAdapter;
import idsoft.idepot.homeinspection.support.MyExpandableAdapter;

import java.util.ArrayList;



import android.app.Activity;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;

public class PoolNewShowDialog extends ExpandableListActivity  {
	String arrstrname="",gettempname="",getmodulename="",str="",getcurretview="",gettempoption="";
	private ArrayList<String> parentItems = new ArrayList<String>();
    private ArrayList<Object> childItems = new ArrayList<Object>();
	DatabaseFunction db;
	CommonFunction cf;
	ArrayModuleList ar;
	ExpandableListView expListView;
	String[] rdname,poolfeatures={"Ladders","Diving Board","Slide","Railing","Other"},laddertype={"Stainless Steel","Plastic","Other"},
			 divingboardtype={"Fiberglass","Stainless Frame","Other"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		
		    Bundle b = this.getIntent().getExtras();
			arrstrname = b.getString("arrstr");
			Bundle b1 = this.getIntent().getExtras();
			gettempname = b1.getString("tempname");
			Bundle b2 = this.getIntent().getExtras();
			getmodulename = b2.getString("modulename");
			Bundle b3 = this.getIntent().getExtras();
			getcurretview = b3.getString("currentview");
			Bundle b4 = this.getIntent().getExtras();
			gettempoption = b4.getString("tempoption");
			
            setContentView(R.layout.poolcustomizedialog);
		    
		    ar = new ArrayModuleList(this,getmodulename);
			cf = new CommonFunction(this);
			db = new DatabaseFunction(this);
			
			TextView hdr = (TextView)findViewById(R.id.header);
			hdr.setText(arrstrname);
			
			expListView = (ExpandableListView) findViewById(android.R.id.list);
			ExpandableListView expListView = getExpandableListView();
			expListView.setDividerHeight(2);
			expListView.setGroupIndicator(null);
			expListView.setClickable(true);
			
			setGroupParents();
	        // Set The Child Data
	        setChildData();
	        
			MyExpandableAdapter adapter = new MyExpandableAdapter(parentItems, childItems);
            adapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE), this);
            
            expListView.setAdapter(adapter);
            expListView.setOnChildClickListener(this);
	        
	}

	private void setChildData() {
		// TODO Auto-generated method stub
		    ArrayList<String> child = new ArrayList<String>();
		    for(int i=0;i<laddertype.length;i++)
			{
		    	child.add(laddertype[i]);
		    
			}
	       
	        childItems.add(child);
	        
	        child = new ArrayList<String>();
	        for(int i=0;i<divingboardtype.length;i++)
			{
		    	
		        child.add(divingboardtype[i]);
		       
			}
	       
	        childItems.add(child);

	        child = new ArrayList<String>();
	        for(int i=0;i<divingboardtype.length;i++)
			{
		    	
		        child.add(divingboardtype[i]);
		       
			}
	       
	        childItems.add(child);

	       
		    	        
	}

	private void setGroupParents() {
		// TODO Auto-generated method stub
		for(int i=0;i<poolfeatures.length;i++)
		{
		   parentItems.add(poolfeatures[i]);
		}
		
		 
	}
}
