
package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;



public class Dashboard extends Activity{
	CommonFunction cf;
	DatabaseFunction db;
	int scheduled=0,inspected=0,reportsready=0,total=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		
        setContentView(R.layout.activity_dashboard);
     
		declaration();
	}
	
	

	private void declaration() {
		// TODO Auto-generated method stub
		db.CreateTable(1);
		db.CreateTable(28);
		db.userid();
		
		try
		{
			Cursor cur1 = db.hi_db.rawQuery("select * from " + db.Registration + " where fld_userid='"+db.UserId+"'",null);
			System.out.println("select * from " + db.Registration + " where fld_userid='"+db.UserId+"'");
			if(cur1!=null)
			{
				cur1.moveToFirst();
				String stragentfirstname = db.decode(cur1.getString(cur1.getColumnIndex("fld_firstname")));
				stragentfirstname += " "+db.decode(cur1.getString(cur1.getColumnIndex("fld_lastname")));
				
				((TextView)findViewById(R.id.dashboard_tvname)).setText(" "+stragentfirstname);
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		try
		{
			Cursor cur1 = db.hi_db.rawQuery("select * from " + db.OrderInspection + " where fld_inspectorid='"+db.UserId+"'",null);
			System.out.println("select * from " + db.OrderInspection + " where fld_inspectorid='"+db.UserId+"'");
			if(cur1!=null)
			{
				cur1.moveToFirst();
				for(int i=0;i<cur1.getCount();i++,cur1.moveToNext())
				{
					int str_status = cur1.getInt(cur1.getColumnIndex("fld_status"));
					
					if(str_status==1)
					{
						scheduled++;
					}
					else if(str_status==2)
					{
						inspected++;
					}
					else if(str_status==3)
					{
						reportsready++;
					}
				}
				
				
				total = scheduled+inspected+reportsready;
				
				((TextView)findViewById(R.id.dash_scheduled)).setText(scheduled+"");
				((TextView)findViewById(R.id.dash_inspected)).setText(inspected+"");
				((TextView)findViewById(R.id.dash_reports)).setText(reportsready+"");
				((TextView)findViewById(R.id.dash_total)).setText(total+"");
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		((TextView)findViewById(R.id.dash_scheduled)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(scheduled!=0)
				{
					cf.chkstat=1;System.out.println("sch="+cf.chkstat);
					Intent in = new Intent(Dashboard.this,StartInspection.class);
					in.putExtra("inspectorid", db.UserId);
					startActivity(in);
					finish();
				}
			}
		});
		
       ((TextView)findViewById(R.id.dash_inspected)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(inspected!=0)
				{
					cf.chkstat=2;
					Intent in = new Intent(Dashboard.this,StartInspection.class);
					in.putExtra("inspectorid", db.UserId);
					startActivity(in);
					finish();
				}
			}
		});
       
       ((TextView)findViewById(R.id.dash_reports)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(reportsready!=0)
				{
					
				}
			}
		});

		((Button)findViewById(R.id.btn_back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Dashboard.this,HomeScreen.class));
				//overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
			}
		});
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(Dashboard.this,HomeScreen.class));
			//overridePendingTransition(R.anim.left_in, R.anim.right_out);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
