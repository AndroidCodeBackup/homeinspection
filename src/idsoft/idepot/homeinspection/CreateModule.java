package idsoft.idepot.homeinspection;
import idsoft.idepot.homeinspection.LoadSubModules.EfficientAdapter;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CreateModule extends Activity{
	String gettempname="",getcurrentview="",getmodulename="",gettempoption="",str_module="",str_cbval="";
	TextView tv_seltemplate,tv_modulename;
	Button btnback,btnhome;
	int pos;
	ListView modulelist;
	String[] arrstrmodule,cbval,value;
	View v;
	CommonFunction cf;
	EfficientAdapter effadap;
	DatabaseFunction db;			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getcurrentview = b1.getString("currentview");
		Bundle b2 = this.getIntent().getExtras();
		getmodulename = b2.getString("modulename");
		Bundle b3 = this.getIntent().getExtras();
		gettempoption = b3.getString("tempoption");
		
		cf = new CommonFunction(this);
		db = new DatabaseFunction(this);
		db.CreateTable(1);
		db.CreateTable(4);
		db.userid();
			
		setContentView(R.layout.activity_createmodule);
		
		tv_seltemplate = (TextView)findViewById(R.id.seltemplate);
		tv_seltemplate.setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
		tv_modulename = (TextView)findViewById(R.id.modulename);
		tv_modulename.setText("Module Name : "+getmodulename);
		
		if(!getmodulename.equals(""))
		{
			cf.GetCurrentModuleName(getmodulename);	
			DefaultValue_Insert();
		}
		
		btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(CreateModule.this,CreateTemplate.class));
				finish();
			}
		});
		
		btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(CreateModule.this,HomeScreen.class));
				finish();
			}
		});
		
		modulelist = (ListView)findViewById(R.id.modulelist);
		
		if(gettempoption.equals("Report Section/Module"))
		{
			tv_modulename.setVisibility(v.VISIBLE);
			Display();
			
		}
		else
		{
			tv_modulename.setVisibility(v.GONE);
			for(int i=0;i<cf.strarr_module.length;i++)
			{
				  str_module += cf.strarr_module[i]+"~";
				  arrstrmodule = str_module.split("~");
				  
				  modulelist.setAdapter(new EfficientAdapter(arrstrmodule,this));
			}
			
			modulelist.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					Intent intent = null;
					 final String sel_modulename= arrstrmodule[arg2];
					
					if(sel_modulename.equals("HVAC"))
					{
						intent = new Intent(CreateModule.this, NewHVAC1.class);
						Bundle b2 = new Bundle();
						b2.putString("templatename", gettempname);			
						intent.putExtras(b2);
						Bundle b3 = new Bundle();
						b3.putString("currentview", "create");			
						intent.putExtras(b3);
						Bundle b4 = new Bundle();
						b4.putString("modulename", sel_modulename);			
						intent.putExtras(b4);
						Bundle b5 = new Bundle();
						b5.putString("tempoption", gettempoption);			
						intent.putExtras(b5);
						Bundle b6 = new Bundle();
						b6.putString("selectedid", "");			
						intent.putExtras(b6);
						startActivity(intent);
						finish();
					}
					else if(sel_modulename.equals("Bathroom"))
					{
						System.out.println("inside create bath");
						intent = new Intent(CreateModule.this, LoadBathroom.class);
					    Bundle b2 = new Bundle();
						b2.putString("templatename", gettempname);			
						intent.putExtras(b2);
						Bundle b3 = new Bundle();
						b3.putString("currentview", "create");			
						intent.putExtras(b3);
						Bundle b4 = new Bundle();
						b4.putString("modulename", sel_modulename);			
						intent.putExtras(b4);
						Bundle b5 = new Bundle();System.out.println("gettempppp="+gettempoption);
						b5.putString("tempoption", gettempoption);			
						intent.putExtras(b5);
						Bundle b6 = new Bundle();
						b6.putString("selectedid", "");			
						intent.putExtras(b6);
						startActivity(intent);
						finish();
					}
					else
					{
					    intent = new Intent(CreateModule.this, CreateSubModule.class);
					    Bundle b2 = new Bundle();
						b2.putString("templatename", gettempname);			
						intent.putExtras(b2);
						Bundle b3 = new Bundle();
						b3.putString("currentview", "create");			
						intent.putExtras(b3);
						Bundle b4 = new Bundle();
						b4.putString("modulename", sel_modulename);			
						intent.putExtras(b4);
						Bundle b5 = new Bundle();
						b5.putString("tempoption", gettempoption);			
						intent.putExtras(b5);
						startActivity(intent);
						finish();
						System.out.println("ends");
					}
					
				}
			});
		}
		
	}
	private void Display() {
		// TODO Auto-generated method stub
		/*for(int i=0;i<cf.arrsub.length;i++)
		{
		*/	try
			{
				Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"'  and fld_module='"+db.encode(getmodulename)+"'", null);
	            System.out.println("trydisplay="+"select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"'  and fld_module='"+getmodulename+"'");
				int cnt = cur.getCount();
	            if(cnt>0)
	            {
	            	cur.moveToFirst();
					if (cur != null) {
						cbval=  new String[cur.getCount()];
						arrstrmodule = new String[cur.getCount()];
						int j=0;
						do {
							String incval =cur.getString(cur.getColumnIndex("fld_NA"));
							String suboption =db.decode(cur.getString(cur.getColumnIndex("fld_submodule")));
							
							/*str_module += suboption+"~";
							arrstrmodule = str_module.split("~");*/
							
							arrstrmodule[j] = suboption;
							
							if(incval.equals("1"))
				            {
								//str_cbval += "checked"+"~";
								cbval[j] = "checked";
								
					        }
				            else
				            {
				            	cbval[j] = "unchecked";
				            	//str_cbval += "unchecked"+"~";
								
						    }
							j++;
							//cbval = str_cbval.split("~");
							//modulelist.setAdapter(new EfficientAdapter(cbval,this));
							
					
						} while (cur.moveToNext());
						effadap = new EfficientAdapter(cbval, this);
						modulelist.setAdapter(effadap);
					}
	            }
	            else
	            {
	            	for(int j=0;j<cf.strarr_module.length;j++)
	    			{
	    				if(getmodulename.equals(cf.strarr_module[j]))
	    				{
	    					for(int k=0;k<cf.arrsub.length;k++)
	    					{
	    						str_module += cf.arrsub[k]+"~";
	    						arrstrmodule = str_module.split("~");
	    						str_cbval += "checked"+"~";
	    						cbval = str_cbval.split("~");
	    						modulelist.setAdapter(new EfficientAdapter(cbval,this));
	    					}
	    				}
	    				
	    			}
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		//}
	}
	private void DefaultValue_Insert() {
		// TODO Auto-generated method stub
		
		cf.GetCurrentModuleName(getmodulename);
		for(int i=0;i<cf.arrsub.length;i++)
		{
			try
			{
				Cursor selcur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_module='"+db.encode(getmodulename)+"'", null);
	            int cnt = selcur.getCount();
	            if(cnt==0)
	            {
	            	try
	            	{
	            		db.hi_db.execSQL(" INSERT INTO "+db.CreateDefault+" (fld_inspectorid,fld_module,fld_submodule,fld_templatename,fld_NA,fld_description) VALUES" +
								"('"+db.UserId+"','"+db.encode(getmodulename)+"','"+db.encode(cf.arrsub[i])+"','"+db.encode(gettempname)+"','1','')");
                    	
	            	}
	            	catch (Exception e) {
						// TODO: handle exception
					}
	            }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		
	}
	class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
		
		public EfficientAdapter(String[] cbval, Context context) {
			mInflater = LayoutInflater.from(context);
			value=cbval;
			

		}

		public int getCount() {
			int arrlen = 0;
			
			 arrlen = arrstrmodule.length;
			
			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			

				if(gettempoption.equals("Module"))
				{
					    holder = new ViewHolder();
						convertView = mInflater.inflate(R.layout.custommodulelist, null);						
						holder.moduletext = (TextView) convertView.findViewById(R.id.submodule);
						holder.cbsubmodule = (CheckBox)convertView.findViewById(R.id.cb_submodule);
						holder.spinsubmodule = (TextView)convertView.findViewById(R.id.spin_submodule);
						holder.spinsubmodule.setText("--Select--");
						holder.vcsubmodule = (TextView)convertView.findViewById(R.id.vc_submodule);
						
						 convertView.setTag(holder);				 
						 
						 holder.cbsubmodule.setOnCheckedChangeListener(new oncheckedlistener(position,holder.cbsubmodule,holder.spinsubmodule,holder.vcsubmodule));
						 holder.spinsubmodule.setOnClickListener(new onclicklistener(position,holder.cbsubmodule));
						 holder.vcsubmodule.setOnClickListener(new vcclicklistener(position,holder.cbsubmodule,holder.spinsubmodule,holder.vcsubmodule));
						 
						 if(value[position].equals("checked"))
						 {
							 holder.cbsubmodule.setChecked(true);
						 }
						 else
						 {
							 holder.cbsubmodule.setChecked(false);
						 }
					
						if (arrstrmodule[position].contains("null")) {
							arrstrmodule[position] = arrstrmodule[position].replace("null", "");
							holder.moduletext.setText(Html.fromHtml(arrstrmodule[position]));
						
						}
						else
						{
							holder.moduletext.setText(Html.fromHtml(arrstrmodule[position]));
							
						}
						
				}
				else
				{
					    holder = new ViewHolder();
						convertView = mInflater.inflate(R.layout.customallmodulelist, null);
						holder.moduletext = (TextView) convertView.findViewById(R.id.moduletext);
						holder.moduleimg = (ImageView) convertView.findViewById(R.id.moduleimg);
									
					    convertView.setTag(holder);
				
						if (arrstrmodule[position].contains("null")) {
							arrstrmodule[position] = arrstrmodule[position].replace("null", "");
							holder.moduletext.setText(Html.fromHtml(arrstrmodule[position]));
						}
						else
						{
							holder.moduletext.setText(Html.fromHtml(arrstrmodule[position]));
						}
				}			
			
			return convertView;
		}

		 class ViewHolder {
			TextView moduletext,spinsubmodule,vcsubmodule;
			ImageView moduleimg;
			CheckBox cbsubmodule;
			
		   
		}

	}
	class vcclicklistener implements OnClickListener
	{
		public int id =0;
		public TextView spmod,vcmod;
		public CheckBox cbmod;
		
		public vcclicklistener(int position, CheckBox cbsubmodule, TextView spinsubmodule, TextView vcsubmodule) {
			// TODO Auto-generated constructor stub
			id=position;
			cbmod=cbsubmodule;
			spmod=spinsubmodule;
			vcmod=vcsubmodule;
			
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(!cbmod.isChecked())
			{
				Intent intent = new Intent(CreateModule.this, NewSetVC.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", "create");			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b5 = new Bundle();
				b5.putString("submodulename", arrstrmodule[id]);			
				intent.putExtras(b5);
				Bundle b6 = new Bundle();
				b6.putString("tempoption", gettempoption);			
				intent.putExtras(b6);
				startActivity(intent);
				System.out.println("gettempoptionmod="+gettempoption);
				finish();
				
			}
		}
		
	}
	class onclicklistener implements OnClickListener{

		public int id =0;
		public CheckBox cbmod;
		
		public onclicklistener(int position, CheckBox cbsubmodule) {
			// TODO Auto-generated constructor stub
			id = position;
			cbmod=cbsubmodule;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(!cbmod.isChecked())
			{
				//new Show_Dialog(arrstrmodule[id],gettempname,getmodulename,CreateModule.this);
				Intent i = new Intent(CreateModule.this,ShowDialog.class);						
				i.putExtra("arrstr", arrstrmodule[id]);
				i.putExtra("tempname", gettempname);
				i.putExtra("modulename", getmodulename);
				i.putExtra("currentview", getcurrentview);
				i.putExtra("tempoption", gettempoption);
				startActivity(i);
			
			}
		}
		
	}
	 class oncheckedlistener implements OnCheckedChangeListener {
			public int id =0;
			public TextView spmod,vcmod;
			public CheckBox cbmod;
			
			public oncheckedlistener(int position, CheckBox cbsubmodule, TextView spinsubmodule, TextView vcsubmodule) {
				// TODO Auto-generated constructor stub
				id=position;
				cbmod=cbsubmodule;
				spmod=spinsubmodule;
				vcmod=vcsubmodule;
				
			}

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
								
				if(isChecked)
				{
					try
					{
						Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateDefault + " WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"'", null);
			            int cnt = cur.getCount();
			            if(cnt>0)
			            {
			            	cur.moveToFirst();
							String defaultval =db.decode(cur.getString(cur.getColumnIndex("fld_description")));
							System.out.println("defaultval="+defaultval);
							if(defaultval.equals(""))
							{
								spmod.setBackgroundResource(R.drawable.btn_dropdown_disabled);
								vcmod.setTextColor(Color.parseColor("#C0C0C0"));
								vcmod.setText(Html.fromHtml("Add/Edit Condition"));
								value[id]="checked";
								try
								{
									db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_NA='1' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+db.encode(gettempname)+"' and fld_submodule='"+db.encode(cf.arrsub[id])+"'");
								}
								catch (Exception e) {
									// TODO: handle exception
								}
							}
			               else
			               {
			            	
			            	AlertDialog.Builder popupDialog = new AlertDialog.Builder(CreateModule.this);
			            	popupDialog.setTitle("Override...");
			            	popupDialog.setMessage("Are you sure you want to overwrite the saved data?");
			            	popupDialog.setIcon(R.drawable.ic_launcher);
			            	popupDialog.setPositiveButton("Yes",
							        new DialogInterface.OnClickListener() {
							            public void onClick(DialogInterface dialog, int which) {
							               
							            	spmod.setBackgroundResource(R.drawable.btn_dropdown_disabled);
							            	vcmod.setTextColor(Color.parseColor("#C0C0C0"));
											vcmod.setText(Html.fromHtml("Add/Edit Condition"));
											value[id]="checked";
											try
											{
												db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_NA='1',fld_description='' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_submodule='"+cf.arrsub[id]+"'");
											}
											catch (Exception e) {
												// TODO: handle exception
											}
							            	 dialog.cancel();
							            }
							        });
							
			            	popupDialog.setNegativeButton("No",
							        new DialogInterface.OnClickListener() {
							            public void onClick(DialogInterface dialog, int which) {
							                // Write your code here to execute after dialog
							            	cbmod.setChecked(false);
							                dialog.cancel();
							            }
							        });
							 
			            	popupDialog.show();
			            }
					 }
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					/*spmod.setBackgroundResource(R.drawable.btn_dropdown_disabled);
					vcmod.setTextColor(Color.parseColor("#C0C0C0"));
					vcmod.setText(Html.fromHtml("Set Visible Condition"));
					value[id]="checked";*/
				
				}
				else
				{
					try
					{
						db.hi_db.execSQL("UPDATE "+db.CreateDefault+ " SET fld_NA='0' WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_submodule='"+cf.arrsub[id]+"'");
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					
					spmod.setBackgroundResource(R.drawable.btn_dropdown_normal);
					vcmod.setTextColor(Color.parseColor("#3F00FF"));
					vcmod.setText(Html.fromHtml("<u>Add/Edit Condition</u>"));
					value[id]="unchecked";
					
					/*spmod.setBackgroundResource(R.drawable.btn_dropdown_normal);
					vcmod.setTextColor(Color.parseColor("#3F00FF"));
					vcmod.setText(Html.fromHtml("<u>Set Visible Condition</u>"));
					value[id]="unchecked";*/
				}
				
			}

		
	 }

			
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		startActivity(new Intent(CreateModule.this,CreateTemplate.class));
		finish();
	}
	
}
