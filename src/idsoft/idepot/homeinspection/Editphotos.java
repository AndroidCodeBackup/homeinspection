package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;




import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Editphotos extends Activity {

	private int currnet_rotated;
	Button rl,rr;
	ImageView ph_im,close_im;
	Bundle b;
	Bitmap bitmap;
	String path,caption,saved_val[];
	String gettempname="",getcurrentview="",getmodulename="",getph="",getinspectionpoint="",getvc="",getsubmodulename="",gettempoption="";
	private Bitmap rotated_b;
	CommonFunction cf;
	String delete;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_selection_edit_photos);
		cf=new CommonFunction(this);
		b=getIntent().getExtras();
		if(b!=null)
		{
			path=b.getString("Path");
			saved_val=b.getStringArray("saved_val");
			caption=b.getString("Caption");
			delete=b.getString("delete");
			 Bundle b = this.getIntent().getExtras();
				gettempname = b.getString("templatename");
				Bundle b1 = this.getIntent().getExtras();
				getmodulename = b1.getString("modulename");
				Bundle b2 = this.getIntent().getExtras();
				getsubmodulename = b2.getString("submodulename");
				Bundle b3 = this.getIntent().getExtras();
				getcurrentview = b3.getString("currentview");
				Bundle b4 = this.getIntent().getExtras();
				getph = b4.getString("selectedid");
				Bundle b6 = this.getIntent().getExtras();
				getvc = b6.getString("selectedvc");
				Bundle b7 = this.getIntent().getExtras();
				gettempoption = b7.getString("tempoption");
			if(caption==null)
			{
			
				caption="";
			}
		}
		
		declaration();
	}
	
	private void declaration() {
		// TODO Auto-generated method stub
	
		rl=(Button) findViewById(R.id.rotateleft);
		rr=(Button) findViewById(R.id.rotateright);
	
		ph_im=(ImageView) findViewById(R.id.ph_img);
		if(delete!=null)
		{
			if(delete.equals("false"))
			{
				((Button) findViewById(R.id.delete)).setVisibility(View.GONE);
			}
		}
		set_image();
		
	}

	private void set_image() {
		// TODO Auto-generated method stub
		System.out.println("sssss"+path);
		rotated_b= ShrinkBitmap(path, 400, 400);
		
		if(rotated_b ==null)
		{
			ph_im.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
		}
		ph_im.setImageBitmap(rotated_b);
		
	}
	public Bitmap ShrinkBitmap(String string, int i, int j) {
		// TODO Auto-generated method stub
		  
			try {
				BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
				bmpFactoryOptions.inJustDecodeBounds = true;
			     bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);

				int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
						/ (float) j);
				int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
						/ (float) i);
               
				if (heightRatio > 1 || widthRatio > 1) {
					if (heightRatio > widthRatio) {
						bmpFactoryOptions.inSampleSize = heightRatio;
					} else {
						bmpFactoryOptions.inSampleSize = widthRatio;
					}
				}

				bmpFactoryOptions.inJustDecodeBounds = false;
				bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);
				//System.out.println("bitmap="+bitmap);
				return bitmap;
			} catch (Exception e) {
			
				return bitmap;
			}

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	private void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Editphotos.this, PhotoDialog.class);
		Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b7 = new Bundle();
		b7.putString("selectedid", getph);			
		intent.putExtras(b7);
		Bundle b6 = new Bundle();
		b6.putString("submodulename", getsubmodulename);			
		intent.putExtras(b6);
		Bundle b8 = new Bundle();
		b8.putString("tempoption", gettempoption);			
		intent.putExtras(b8);
		Bundle b = new Bundle();
		b.putString("selectedvc", getvc);			
		intent.putExtras(b);
		startActivity(intent);
		finish();
	}

	public void clicker(View v)
	{
		switch (v.getId()) {
		
		case R.id.rotateleft:
			if(rotated_b!=null)
			{
			System.gc();
			currnet_rotated-=90;
			if(currnet_rotated<0)
			{
				currnet_rotated=270;
			}

			
			Bitmap myImg;
			try {
				
					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
				Matrix matrix =new Matrix();
				matrix.reset();
				//matrix.setRotate(currnet_rotated);
				
				matrix.postRotate(currnet_rotated);
				
				 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
				        matrix, true);
				 
				 ph_im.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) { }
			catch (Exception e) { }
			catch (OutOfMemoryError e) {}
			}
		break;

		case R.id.rotateright:
			if(rotated_b!=null)
			{
			currnet_rotated+=90;
			if(currnet_rotated>=360)
			{
				currnet_rotated=0;
			}
			
			Bitmap myImg1;
			try {
				
					myImg1 = BitmapFactory.decodeStream(new FileInputStream(path));
				
				
				Matrix matrix =new Matrix();
				matrix.reset();
				//matrix.setRotate(currnet_rotated);
				matrix.postRotate(currnet_rotated);
				
				 rotated_b  = Bitmap.createBitmap(myImg1, 0, 0,  myImg1.getWidth(),myImg1.getHeight(),
				        matrix, true);
				 System.gc();
				 ph_im.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) { }
			catch (Exception e) { }
			catch (OutOfMemoryError e) {}
			}
			break;
		
		case R.id.close:
			Intent in = getIntent();
			in.putExtras(b);
			setResult(RESULT_CANCELED, in);
			finish();
		break;
		case R.id.delete:
			final AlertDialog.Builder b =new AlertDialog.Builder(Editphotos.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to delete the selected image?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						
						Intent in2 = getIntent();
						Bundle b =in2.getExtras();
						b.putBoolean("Delete_data", true);
						in2.putExtras(b);
						setResult(RESULT_OK, in2);
						finish();	
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					cf.DisplayToast("The selected image has been deleted successfully.");
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show();
			
		break;
		case R.id.update:
			
				if(currnet_rotated>0)
				{ 

					try
					{
						/**Create the new image with the rotation **/
						String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
						  ContentValues values = new ContentValues();
						  values.put(MediaStore.Images.Media.ORIENTATION, 0);
						  Editphotos.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
						
						
						if(current!=null)
						{
						String path=getPath(Uri.parse(current));
						
							
							File fout = new File(this.path);
							fout.delete();
							/** delete the selected image **/
							
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(this.path));
						}
					} catch(Exception e)
					{
						System.out.println("Error occure while rotate the image "+e.getMessage());
					}
					
			
			
			}
				Intent in2 = getIntent();
				Bundle b1 =in2.getExtras();
				b1.putBoolean("Delete_data", false);
				//b1.putString("Caption", ed.getText().toString().trim());
				b1.putString("Path", path);
				in2.putExtras(b1);
				setResult(RESULT_OK, in2);
				finish();	
			
			
		break;
		case R.id.ph_img:
			Intent reptoit = new Intent(Editphotos.this,Select_phots.class);
				reptoit.putExtra("Selectedvalue", saved_val);
				reptoit.putExtra("Maximumcount", 0);
				reptoit.putExtra("Total_Maximumcount", 1); 
				startActivityForResult(reptoit,122);
		break;
		default:
		break;
		}
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = ((Activity) this).managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK)
		{
			if(requestCode==122)
			{
				String[] value=	data.getExtras().getStringArray("Selected_array");
				path=value[0];System.out.println("pttha="+path);
				set_image();
			}
		}
	}
	
}
