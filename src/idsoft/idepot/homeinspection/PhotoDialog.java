package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;

import java.io.ByteArrayOutputStream;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Currency;
import java.util.Date;



import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;


public class PhotoDialog extends Activity{
	String gettempname="",getcurrentview="",getmodulename="",getph="",getinspectionpoint="",getvc="",getsubmodulename="",gettempoption="",getphotooption="",imagepath="",imagecaption="",captionval="",
			selectedImagePath="",imgPath="";
	ImageView closeimg,galleryimg,cameraimg;
	DatabaseFunction db;
	CommonFunction cf;
	ArrayAdapter adapter;
	Bitmap bitmap =null;
	LinearLayout subseclayout;
	String[] photoarr,imgpath,captionarr,imgcaption,imgcaptionval,value,captionvalarr,imgcaptionvar,subsectionarr;
	int[] idarr;
	TextView txtnoimage;
	ListView photolist;
	EditText edtcaption;
	public Uri CapturedImageURI;
	View v;
	Spinner spinsubsec;
	int maximg=4,t=0,cnt=0,idval=0,iden=0;
	final private int CAPTURE_IMAGE = 2;	
	  @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);        
	     
	        setContentView(R.layout.image_selection_imagepicker);
	        
	        Bundle b = this.getIntent().getExtras();
			gettempname = b.getString("templatename");
			Bundle b1 = this.getIntent().getExtras();
			getmodulename = b1.getString("modulename");
			Bundle b2 = this.getIntent().getExtras();
			getsubmodulename = b2.getString("submodulename");
			Bundle b3 = this.getIntent().getExtras();
			getcurrentview = b3.getString("currentview");
			Bundle b4 = this.getIntent().getExtras();
			getph = b4.getString("selectedid");
			Bundle b6 = this.getIntent().getExtras();
			getvc = b6.getString("selectedvc");
			Bundle b7 = this.getIntent().getExtras();
			gettempoption = b7.getString("tempoption");	
			Bundle b8 = this.getIntent().getExtras();
			getphotooption = b8.getString("photooption");System.out.println("getphotooption"+getphotooption);
			
			db = new DatabaseFunction(this);
			cf = new CommonFunction(this);
			db.CreateTable(1);
			db.CreateTable(13);
			db.userid();
			subseclayout = (LinearLayout)findViewById(R.id.subsection);
			if(getphotooption.equals("takephoto"))
			{
				if(getmodulename.equals("HVAC") || getmodulename.equals("Bathroom"))
				{
					 subseclayout.setVisibility(View.GONE);			
				}
				else
				{
					 subseclayout.setVisibility(View.VISIBLE);				
					 spinsubsec = (Spinner)findViewById(R.id.spin_subsection);
					 cf.GetCurrentModuleName(getmodulename);
					 
					 subsectionarr = new String[cf.arrsub.length+1];				 
					 subsectionarr[0]="--Select--";
					 for(int i=0;i<cf.arrsub.length;i++)
					 {
						 subsectionarr[i+1]=cf.arrsub[i];
					 }
					 adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, subsectionarr);
				     adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					 spinsubsec.setAdapter(adapter);
					 spinsubsec.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
				}
			}
			
			
			
	        closeimg = (ImageView)findViewById(R.id.close);
	        closeimg.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					back();
				}
			});
	        
	        txtnoimage = (TextView)findViewById(R.id.option_notext);
	        photolist = (ListView)findViewById(R.id.option_list);
	        
	        galleryimg = (ImageView)findViewById(R.id.gallery);
	        galleryimg.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					 // TODO Auto-generated method stub
					
					if(getphotooption.equals("takephoto"))
					{
						if(getmodulename.equals("HVAC") || getmodulename.equals("Bathroom"))
						{
							uploadphoto();
						}
						else
						{
							if(spinsubsec.getSelectedItem().toString().equals("--Select--"))
							{
								cf.DisplayToast("Please select Subsection.");
							}
							else
							{
								uploadphoto();
							}
						}
					}	
					else
					{
						uploadphoto();
					}
				}
			});	        
	        cameraimg = (ImageView)findViewById(R.id.camera);
	        cameraimg.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(getmodulename.equals("HVAC") || getmodulename.equals("Bathroom"))
					{
						getvc=getmodulename;
					}
					Cursor cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_vc='"+db.encode(getvc)+"'", null);
		            int cnt = cur.getCount();
		            if(cnt<maximg)
		            {
		            	 Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		            	 intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
		                 startActivityForResult(intent, CAPTURE_IMAGE);
		            }
		            else
		            {
		            	cf.DisplayToast("You have reached the maximum count to upload");
		            }
					
				}
			});
	        
	        display();
	  }
	  private void uploadphoto()
	  {
		  Cursor cur;
		  if(getphotooption.equals("takephoto"))
		  {
			  if(getmodulename.equals("HVAC") || getmodulename.equals("Bathroom"))
			  {
				  cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_identity='1'", null);  
			  }
			  else
			  {
				  cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+spinsubsec.getSelectedItem().toString()+"' and fld_identity='1'", null);
			  }
			  
		  }
		  else
		  {
			   cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_vc='"+db.encode(getvc)+"' and fld_identity='0'", null);  
		  }
		  
		  
          int cnt = cur.getCount();
          if(cnt<maximg)
          {
          	   	cur.moveToFirst();
          	   	String[] selected_paht = new String[cur.getCount()];
   				
   				for(int i=0;i<cur.getCount();i++)
   				{
   					selected_paht[i]=db.decode(cur.getString(cur.getColumnIndex("fld_image"))); 
   					
   					cur.moveToNext();
   				}
		    		
				    Intent i = new Intent(PhotoDialog.this,Select_phots.class);						
					i.putExtra("Selectedvalue", selected_paht);
					i.putExtra("Total_Maximumcount", 4);
					i.putExtra("Maximumcount", cnt);
					i.putExtra("submodulename", getsubmodulename);
					startActivityForResult(i, 121);
          }
          else
          {
          	cf.DisplayToast("You have reached the maximum count to upload");
          }
	  }
	  private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
			
		   public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
			   getsubmodulename = parent.getItemAtPosition(pos).toString();
	    		if(!getsubmodulename.equals("--Select--"))
	    		{
	    			display();
	    		}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	  protected Uri  setImageUri() {
			// TODO Auto-generated method stub
			  File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
	          Uri imgUri = Uri.fromFile(file);
	          this.imgPath = file.getAbsolutePath();
	          
			  
			return imgUri;
		}
		public String getImagePath() {
	        return imgPath;
	    }
		 @Override
		    protected void onSaveInstanceState(Bundle outState) {
			   
		        outState.putString("photo_index", imgPath);
		        super.onSaveInstanceState(outState);
		    }
		 @Override
		    protected void onRestoreInstanceState(Bundle savedInstanceState) {
			      imgPath = savedInstanceState.getString("photo_index");
			    
			   
			      
		        super.onRestoreInstanceState(savedInstanceState);
		    }
	  public void clicker(View v)
	  {
		  switch(v.getId())
		  {
			  case R.id.option_cancel:
				  back();
				  break;
			  case R.id.option_close:
				  back();
				  break;
			  case R.id.option_ok:
                  try
                  {
                	  Cursor cur;
                	  if(getphotooption.equals("takephoto"))
                	  {
                		  if(getmodulename.equals("HVAC") || getmodulename.equals("Bathroom"))
                		  {
                			  cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_identity='1'", null);	  
                		  }
                		  else
                		  {
                			  cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+spinsubsec.getSelectedItem().toString()+"' and fld_identity='1'", null);
                	      }
                	  }
                	  else
                	  {
                		  cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_vc='"+db.encode(getvc)+"' and fld_identity='0'", null);  
                	  }
                	    
	  		            int cnt = cur.getCount();System.out.println("COUNT="+cur.getCount());
	  		            if(cnt>0)
	  		            {
	  		            	for(int i=0;i<captionarr.length;i++)
	  					   {
	  						  if(!imgcaptionval[i].equals("textfill"))
	  						  {
	  							  t=1;
	  							  cf.DisplayToast("Please enter the caption");
	  							  return;
	  						  }
	  						  else
	  						  {
	  							  t=0;
	  						  }
	  					  }
	  		            	 if(t==0)
	  						  {
	  		            		  for(int i=0;i<captionarr.length;i++)
	  							  {
	  		            			  if(getphotooption.equals("takephoto"))
	  		                  	      {
	  		            				db.hi_db.execSQL("UPDATE "+db.SaveVCImage+ " SET fld_caption='"+db.encode(captionarr[i])+"' WHERE fld_inspectorid='"+db.UserId+"' " +
	  		            						"and fld_image='"+db.encode(photoarr[i])+"' and fld_srid='"+getph+"' and fld_module='"+getmodulename+"' " +
	  		            								"and fld_submodule='"+getsubmodulename+"'  and fld_templatename='"+gettempname+"' and fld_identity='1'");
	  		                  	      }
	  		            			  else
	  		            			  {
	  		            				  db.hi_db.execSQL("UPDATE "+db.SaveVCImage+ " SET fld_caption='"+db.encode(captionarr[i])+"'," +
	  		            				  		"fld_vc='"+db.encode(getvc)+"' WHERE fld_inspectorid='"+db.UserId+"' and " +
	  		            				  				"fld_image='"+db.encode(photoarr[i])+"' and fld_srid='"+getph+"' " +
	  		            				  						"and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"'  " +
	  		            				  								"and fld_templatename='"+gettempname+"' and fld_identity='0'");
	  		            			  }
	  							  }
	  							  cf.DisplayToast("Saved successfully");
	  						  }
	  		            }
	  		            else
	  		            {
	  		            	cf.DisplayToast("Please upload at least 1 image");
	  		            }
                  }
  		            catch (Exception e) {
						// TODO: handle exception
					}
				  			 
				  display();
				
				  break;
		  }
	  }
	 
	  @Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			// TODO Auto-generated method stub
			super.onActivityResult(requestCode, resultCode, data);
			try
			{
				
			if(resultCode == RESULT_OK)
			{
				if(requestCode == 121)
				{
					Cursor cur;
					 int getlast=0;
					 value=	data.getExtras().getStringArray("Selected_array");
				 	  for(int i=0;i<value.length;i++)
					  {
						  if(getphotooption.equals("takephoto"))
						  {
							  iden=1;
							  if(getmodulename.equals("HVAC") || getmodulename.equals("Bathroom"))
							  {
								  getvc = getmodulename;
								  cur =db.hi_db.rawQuery("select COUNT(si_Id) as _id from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_identity='1'", null);
							  }
							  else
							  {
								  getvc = spinsubsec.getSelectedItem().toString();
								  cur =db.hi_db.rawQuery("select COUNT(si_Id) as _id from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+spinsubsec.getSelectedItem().toString()+"' and fld_identity='1'", null);
							  }
						  }
						  else
						  {
							  iden=0;
							  cur =db.hi_db.rawQuery("select COUNT(si_Id) as _id from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_vc='"+db.encode(getvc)+"' and fld_identity='0'", null);
						  }
						  
						  if(cur.getCount()>0)
						  {
							  cur.moveToFirst();
					          getlast = cur.getInt(cur.getColumnIndex("_id"));
					          getlast = getlast+1;
						  }
						 
						  
						  
						 db.hi_db.execSQL("INSERT INTO "+db.SaveVCImage+" (fld_image,fld_caption,fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_vc,fld_templatename,fld_identity) VALUES" +
									"('"+db.encode(value[i])+"','"+(db.encode(getvc)+" "+getlast)+"','"+db.UserId+"','"+getph+"','"+getmodulename+"','"+getsubmodulename+"','"+db.encode(getvc)+"','"+gettempname+"','"+iden+"')");
					  }
					  display();
				
				}
				else if(requestCode == CAPTURE_IMAGE)
				{
					Cursor cur = null;
					 selectedImagePath = getImagePath();
					 
					 File f = new File(selectedImagePath);
						double length = f.length();
						double kb = length / 1024;
						double mb = kb / 1024;
						
						if (mb >= 2) {
							cf.DisplayToast("File size exceeds!! Too large to attach");
						} else {
						
							try {
								Bitmap bitmapdb = decodeFile(selectedImagePath);
								if(bitmapdb==null)
								{
									cf.DisplayToast("File corrupted!! Cant able to attach");
								}
								else
								{
									selectedImagePath = selectedImagePath;
									 int getlast=0;
									 if(getphotooption.equals("takephoto"))
									 {
										 iden=1;
										 if(getmodulename.equals("HVAC") || getmodulename.equals("Bathroom"))
										 {
											 getvc = getmodulename;
											 cur =db.hi_db.rawQuery("select COUNT(si_Id) as _id from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_identity='1'", null);
										 }
										 else
									     {
											 getvc = spinsubsec.getSelectedItem().toString();
											 cur =db.hi_db.rawQuery("select COUNT(si_Id) as _id from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+spinsubsec.getSelectedItem().toString()+"' and fld_identity='1'", null);
									     }
									 }
									 else
									 {
										 iden=0;
										 cur =db.hi_db.rawQuery("select COUNT(si_Id) as _id from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_vc='"+db.encode(getvc)+"' and fld_identity='0'", null);	 
									 }
									 
									  if(cur.getCount()>0)
									  {
										  cur.moveToFirst();
								          getlast = cur.getInt(cur.getColumnIndex("_id"));
								          getlast = getlast+1;
									  }
									 
									db.hi_db.execSQL(" INSERT INTO "+db.SaveVCImage+" (fld_image,fld_caption,fld_inspectorid,fld_srid,fld_module,fld_submodule,fld_vc,fld_templatename,fld_identity) VALUES" +
												"('"+db.encode(selectedImagePath)+"','"+(db.encode(getvc)+" "+getlast)+"','"+db.UserId+"','"+getph+"','"+getmodulename+"','"+getsubmodulename+"','"+db.encode(getvc)+"','"+gettempname+"','"+iden+"')");
								
								  
								   display();
								
								}
							} catch (Exception e) {
								System.out.println("my exception " + e);
							}
						}
	

				
				}
				else if(requestCode == 101)
				{
					  String value=	data.getExtras().getString("Path"); 
					  String  id=	data.getExtras().getString("id"); 
					  boolean dele=data.getExtras().getBoolean("Delete_data");
					if(dele)
					{
						db.hi_db.execSQL("DELETE FROM "+db.SaveVCImage+" WHERE fld_image='"+db.encode(id)+"'");
					}
					display();

				}
				else if(requestCode == 122)
				{
					 String value=	data.getExtras().getString("Path"); 
					 int  id=	data.getExtras().getInt("id"); 
					 boolean dele=data.getExtras().getBoolean("Delete_data");
					if(!dele)
					  {
						db.hi_db.execSQL(" UPDATE  "
	 							+ db.SaveVCImage
	 							+ " SET fld_image='"+db.encode(value)+"' "
	 							+ "  WHERE si_Id='"+id+"'");
							cf.DisplayToast("Image saved successfully");					
							
					  }
					  else
					  {
						 
							
							if(dele)
							{
								db.hi_db.execSQL("DELETE FROM "+db.SaveVCImage+" WHERE si_Id='"+id+"'");
							//	cf.DisplayToast("Image deleted successfully");	
							}
					  }
					display();

				}
			}}catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	  private static Bitmap decodeFile(String file) {
		    try {
		    	
		    	File f=new File(file);	    	
		        // Decode image size
		        BitmapFactory.Options o = new BitmapFactory.Options();
		        o.inJustDecodeBounds = true;
		        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

		        // The new size we want to scale to
		        final int REQUIRED_SIZE = 150;

		        // Find the correct scale value. It should be the power of 2.
		        int scale = 1;
		        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
		                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
		            scale *= 2;

		        // Decode with inSampleSize
		        BitmapFactory.Options o2 = new BitmapFactory.Options();
		        o2.inSampleSize = scale;
		        o.inJustDecodeBounds = false;
		        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		    } catch (FileNotFoundException e) {
		    }

		    return null;
		}
		private void display() {
		// TODO Auto-generated method stub
			 try
	         {
				 	Cursor cur;
				    imagepath="";imagecaption="";captionval="";
				    if(getphotooption.equals("takephoto"))
				    {
				    	if(getmodulename.equals("HVAC") || getmodulename.equals("Bathroom"))
				    	{
				    		cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module ='"+getmodulename+"' and fld_identity='1'", null);		
				    	}
				    	else
				    	{
				    		cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module ='"+getmodulename+"' and fld_submodule='"+spinsubsec.getSelectedItem().toString()+"' and fld_identity='1'", null);
				    	}
				    	
				    }
				    else
				    {
				    	cur =db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_srid='"+getph+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_vc='"+db.encode(getvc)+"' and fld_module ='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_identity='0'", null);
				    }
	                
		            cnt = cur.getCount();
		            if(cnt==0)
		            {
		            	txtnoimage.setVisibility(v.VISIBLE);
		            	photolist.setVisibility(v.GONE);
		            }
		            else
		            {
		            	
		            	photolist.setVisibility(v.VISIBLE);
		            	txtnoimage.setVisibility(v.GONE);
		            	cur.moveToFirst();
		            	photoarr=new String[cnt];
		            	captionarr=new String[cnt];
		            	captionvalarr=new String[cnt];
		            	idarr=new int[cnt];
		            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
		            	{
		            		photoarr[i] = db.decode(cur.getString(cur.getColumnIndex("fld_image")));
		            		captionarr[i] = db.decode(cur.getString(cur.getColumnIndex("fld_caption")));
		            		idarr[i] = cur.getInt(cur.getColumnIndex("si_Id"));
		            		captionvalarr[i] = "textfill";
		            	}
		            	photolist.setAdapter(null);
						 photolist.setAdapter(new EfficientAdapter(photoarr,captionarr,captionvalarr,idarr,this));
		            }
	         }catch (Exception e) {
				// TODO: handle exception
			}
	}
		class EfficientAdapter extends BaseAdapter {
			private static final int IMAGE_MAX_SIZE = 0;
			private LayoutInflater mInflater;
			ViewHolder holder;
			View v2;
			
			public EfficientAdapter(String[] cbval, String[] captionarr, String[] captionvalarr,int[] idarr1, Context context) {
				mInflater = LayoutInflater.from(context);
				imgpath=cbval;			
				imgcaption=captionarr;
				imgcaptionval=captionvalarr;
				idarr=idarr1;

			}

			public int getCount() {
				int arrlen = 0;				
				 arrlen = photoarr.length;				
				return arrlen;
			}

			public Object getItem(int position) {
				return position;
			}

			public long getItemId(int position) {
				return position;
			}

			public View getView(int position, View convertView, ViewGroup parent) {
				
						if(cnt!=0)
						{
						    holder = new ViewHolder();
							convertView = mInflater.inflate(R.layout.activity_photos, null);						
							holder.vcphoto = (ImageView) convertView.findViewById(R.id.image);
							holder.vccaption = (EditText)convertView.findViewById(R.id.caption);							
							
							 convertView.setTag(holder);				 
							
							if (photoarr[position].contains("null") || captionarr[position].contains("null")) {
								photoarr[position] = photoarr[position].replace("null", "");
								captionarr[position] = captionarr[position].replace("null", "");
							}
							
								bitmap = ShrinkBitmap(photoarr[position],150,150);
								if(bitmap!=null)
								{
									holder.vcphoto.setImageBitmap(bitmap); 
								}
								else
								{
									holder.vcphoto.setBackgroundDrawable(getResources().getDrawable(R.drawable.photonotavail));

								}
								
								holder.vccaption.setText(captionarr[position]);
							
							    holder.vccaption.addTextChangedListener(new ontextchangelistner(position,holder.vccaption));
							    holder.vcphoto.setOnClickListener(new imagelistner(position,holder.vcphoto,idarr[position]));
						}
				return convertView;
			}

			 class ViewHolder {
			    ImageView vcphoto;
				EditText vccaption;
			   
			}

		}
		class imagelistner implements OnClickListener
		{

			int id=0,autoid=0;
			ImageView myphoto;
			public imagelistner(int position, ImageView vcphoto,int autoincid) {
				// TODO Auto-generated constructor stub
				id=position;
				myphoto=vcphoto;
				autoid=autoincid;
			}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new  Intent(PhotoDialog.this,Editphotos.class);
				in.putExtra("Path",photoarr[id]);
				in.putExtra("id", autoid);
				in.putExtra("delete","true");
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				in.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				in.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				in.putExtras(b4);
				Bundle b7 = new Bundle();
				b7.putString("selectedid", getph);			
				in.putExtras(b7);
				Bundle b6 = new Bundle();
				b6.putString("submodulename", getsubmodulename);			
				in.putExtras(b6);
				Bundle b8 = new Bundle();
				b8.putString("tempoption", gettempoption);			
				in.putExtras(b8);
				Bundle b = new Bundle();
				b.putString("selectedvc", getvc);			
				in.putExtras(b);
				startActivityForResult(in, 122);
				//startActivityForResult(in, 101);

			}
			
		}
		class ontextchangelistner implements TextWatcher
		{
			
			int id;

			public ontextchangelistner(int position, EditText vccaption) {
				// TODO Auto-generated constructor stub
				id=position;
				edtcaption = vccaption;
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				captionarr[id]=s.toString();
				if(s.toString().equals(""))
				{
					imgcaptionval[id]="textnotfill";
				}
				else
				{
					 imgcaptionval[id]="textfill";
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				    if (s.toString().startsWith(" "))
			        {
				    	captionarr[id]="";
					    //edtcaption.setText("");
					    imgcaptionval[id]="textnotfill";
			        }
				    else
				    {
				        imgcaptionval[id]="textfill";
				    }
			}
			
		}
		private void back() {
			// TODO Auto-generated method stub
			if(getphotooption.equals("takephoto"))
			{
				if(getmodulename.equals("HVAC"))
				{
					System.out.println("getcurrentviewHVAC="+getcurrentview);
					Intent intent = new Intent(PhotoDialog.this, NewHVAC1.class);
					Bundle b1 = new Bundle();
					b1.putString("templatename", gettempname);			
					intent.putExtras(b1);				
					Bundle b2 = new Bundle();
					b2.putString("currentview", getcurrentview);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("modulename", getmodulename);			
					intent.putExtras(b3);				
					Bundle b4 = new Bundle();
					b4.putString("tempoption", gettempname);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("selectedid", getph);			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("photooption","takephoto");			
					intent.putExtras(b6);
					startActivity(intent);
					finish();	
				}
				else if(getmodulename.equals("Bathroom"))
				{
					Intent intent = new Intent(PhotoDialog.this, LoadBathroom.class);
					Bundle b1 = new Bundle();
					b1.putString("templatename", gettempname);			
					intent.putExtras(b1);				
					Bundle b2 = new Bundle();
					b2.putString("currentview", getcurrentview);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("modulename", getmodulename);			
					intent.putExtras(b3);				
					Bundle b4 = new Bundle();
					b4.putString("tempoption", gettempname);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("selectedid", getph);			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("photooption","takephoto");			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
					
					
				}
				else
				{
					Intent intent = new Intent(PhotoDialog.this, LoadSubModules.class);
					Bundle b1 = new Bundle();
					b1.putString("templatename", gettempname);			
					intent.putExtras(b1);				
					Bundle b2 = new Bundle();
					b2.putString("modulename", getmodulename);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("selectedid", getph);			
					intent.putExtras(b3);
					Bundle b6 = new Bundle();
					b6.putString("photooption","takephoto");			
					intent.putExtras(b6);
					startActivity(intent);
					finish();	
				}					
			}
			else
			{
				Intent intent = new Intent(PhotoDialog.this, LoadSaveVC.class);
				Bundle b2 = new Bundle();
				b2.putString("templatename", gettempname);			
				intent.putExtras(b2);
				Bundle b3 = new Bundle();
				b3.putString("currentview", getcurrentview);			
				intent.putExtras(b3);
				Bundle b4 = new Bundle();
				b4.putString("modulename", getmodulename);			
				intent.putExtras(b4);
				Bundle b7 = new Bundle();
				b7.putString("selectedid", getph);			
				intent.putExtras(b7);
				Bundle b6 = new Bundle();
				b6.putString("submodulename", getsubmodulename);			
				intent.putExtras(b6);
				Bundle b8 = new Bundle();
				b8.putString("tempoption", gettempoption);			
				intent.putExtras(b8);
				Bundle b = new Bundle();
				b.putString("selectedvc", getvc);			
				intent.putExtras(b);
				Bundle b1 = new Bundle();
				b1.putString("photooption","takephoto");			
				intent.putExtras(b1);
				
				startActivity(intent);
				finish();
			}
			
			
		}
		public Bitmap ShrinkBitmap(String string, int i, int j) {
			// TODO Auto-generated method stub
			  
				try {
					BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
					bmpFactoryOptions.inJustDecodeBounds = true;
				    bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);

					int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
							/ (float) j);
					int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
							/ (float) i);
	               
					if (heightRatio > 1 || widthRatio > 1) {
						if (heightRatio > widthRatio) {
							bmpFactoryOptions.inSampleSize = heightRatio;
						} else {
							bmpFactoryOptions.inSampleSize = widthRatio;
						}
					}

					bmpFactoryOptions.inJustDecodeBounds = false;
					bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);
					//System.out.println("bitmap="+bitmap);
					return bitmap;
				} catch (Exception e) {
				System.out.println("catch="+e.getMessage());
					return bitmap;
				}

		}
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			// super.onBackPressed();
			back();
		}
}
