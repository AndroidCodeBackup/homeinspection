package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.LoginActivity.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ForgotPassword extends Activity {
	CommonFunction cf;
	Webservice_config wb;
	EditText forgot_et_username,forgot_et_email;
	int handler_msg=3,k=0;
	Progress_staticvalues p_sv;
	String erro_msg="",forgotusername="",forgotpassword="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize();
		
	}
	private void initialize() {
		// TODO Auto-generated method stub
		setContentView(R.layout.forgotpassword);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
				
		forgot_et_username = (EditText)findViewById(R.id.forgot_username);
		forgot_et_email = (EditText)findViewById(R.id.forgot_email);
		
		forgot_et_username.addTextChangedListener(new CustomTextWatcher(forgot_et_username));
		forgot_et_email.addTextChangedListener(new CustomTextWatcher(forgot_et_email));
		
		((Button)findViewById(R.id.submit)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(forgot_et_username.getText().toString().trim().equals("") && forgot_et_email.getText().toString().trim().equals(""))
				{
					cf.DisplayToast("Please enter at least any one option");
				}
				else
				{
					
					if(forgot_et_username.getText().toString().trim().equals(""))
					{
						if(!forgot_et_email.getText().toString().trim().equals(""))
						{
							boolean chkmail = cf.EmailValidation(forgot_et_email.getText().toString());
							if(chkmail)
							{
								submitpassword();
							}
							else
							{
								cf.DisplayToast("Please enter valid Email");
								forgot_et_email.requestFocus();
							}
						}
				
					}
					else
					{
						if(forgot_et_email.getText().toString().trim().equals(""))
						{
							submitpassword();
						}
						else
						{
							boolean chkmail = cf.EmailValidation(forgot_et_email.getText().toString());
							if(chkmail)
							{
								submitpassword();
							}
							else
							{
								cf.DisplayToast("Please enter valid Email");
								forgot_et_email.requestFocus();
							}
						}
						
					}
					
				}
			}

			
		});
		
		((Button)findViewById(R.id.back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(ForgotPassword.this,LoginActivity.class));
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();	
			}
		});
		
        ((Button)findViewById(R.id.clear)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				forgot_et_username.setText("");
				forgot_et_email.setText("");
				forgot_et_username.requestFocus();
			}
        });
	}
	
    private void Send_forgotpassword() throws IOException, XmlPullParserException {
			
	    SoapObject request = new SoapObject(wb.NAMESPACE,"FORGOTPASSWORD");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
 		envelope.dotNet = true;
 		
 		request.addProperty("Username",forgot_et_username.getText().toString());
 		request.addProperty("Emailid",forgot_et_email.getText().toString());
 		
 		envelope.setOutputSoapObject(request);
 		System.out.println("forgotrequest="+request);
 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
        SoapObject response = null;
        try
        {
              androidHttpTransport.call(wb.NAMESPACE+"FORGOTPASSWORD", envelope);
              response = (SoapObject)envelope.getResponse();
              System.out.println("FORGOTPASSWORDresult "+response);
              if(response.getProperty("StatusCode").toString().equals("0"))
      		{
              	
            	    forgotusername =  response.getProperty("fld_username").toString();  
            	    forgotpassword =  response.getProperty("fld_password").toString();
              	handler_msg=1;
      		}
      		else
      		{
      			erro_msg=response.getProperty("StatusMessage").toString();System.out.println("erro_msgweb="+erro_msg);
      			handler_msg=0;
      		}
              
        }
        catch(Exception e)
        {
        	handler_msg=0;
             e.printStackTrace();
        }
     
       
}
	
	protected void submitpassword() {
		// TODO Auto-generated method stub
		
		  if(cf.isInternetOn())
		  {
			   new Start_xporting().execute("");
		  }
		  else
		  {
			  cf.DisplayToast("Please enable your internet connection.");
			  
		  } 
	}

	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="Retrieving password";
			    p_sv.progress_Msg="Please wait we are retrieving your password. It may take few minutes.";
		       
				startActivityForResult(new Intent(ForgotPassword.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
			 		
			 			 Send_forgotpassword();
			 		
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		                return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	
		    	
		    	if(handler_msg==0)
		    	{
		    		k=2;
		    		failure_alert();
		    	
		    	}
		    	else if(handler_msg==1)
		    	{
		    		k=1;
		    		success_alert();
		    	
		    	}
		    	
		    }
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(ForgotPassword.this,LoginActivity.class));
			overridePendingTransition(R.anim.left_in, R.anim.right_out);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void success_alert() {
		// TODO Auto-generated method stub
		AlertDialog al = new AlertDialog.Builder(ForgotPassword.this).setMessage("Password has been successfully sent to your registered email.").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				k=0;
				Intent intent=new Intent(ForgotPassword.this,LoginActivity.class);
				intent.putExtra("status", "forgotpassword");
				intent.putExtra("username", forgotusername);
				intent.putExtra("password", forgotpassword);
				startActivity(intent);
				finish();System.out.println("forgotusername="+forgotusername);
				
			}
		}).setTitle("Retrieve Password Success").create();
		al.setCancelable(false);
		al.show();
	}
	public void failure_alert() {
		// TODO Auto-generated method stub
		AlertDialog al = new AlertDialog.Builder(ForgotPassword.this).setMessage(erro_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				k=0;
			}
		}).setTitle("Retrieve Password Failure").create();
		al.setCancelable(false);
		al.show();
	}
	
	 @Override
	    protected void onSaveInstanceState(Bundle outState) {
		 System.out.println("saveinstan");
		 outState.putInt("kval",k);
		 outState.putString("error",erro_msg);
		 outState.putString("user", forgotusername);
		 outState.putString("pass", forgotpassword);
		 super.onSaveInstanceState(outState);
	 }
     
	 @Override
	    protected void onRestoreInstanceState(Bundle savedInstanceState) {
		 System.out.println("restore");
		      k = savedInstanceState.getInt("kval");
		      erro_msg = savedInstanceState.getString("error");
		      forgotusername = savedInstanceState.getString("user");
		      forgotpassword = savedInstanceState.getString("pass");
		      
		      if(k==2)
		      {
		    	  failure_alert();
		      }
		      else if(k==1)
		      {
		    	  success_alert();
		      }
		      super.onRestoreInstanceState(savedInstanceState);
	 }
}
