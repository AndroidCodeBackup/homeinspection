package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DataBaseHelper;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;
import idsoft.idepot.homeinspection.support.phone_nowatcher;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.TimePicker;




public class PlaceOrder extends Activity {
	Progress_staticvalues p_sv;
	DatabaseFunction db;
	DataBaseHelper dbh;
	Webservice_config wb;
	CommonFunction cf;
	Random rand;
	EditText et_inspectioncompany,et_policynumber,et_jobno,et_squarefootage,et_numberofstories,et_inspectiondate,et_inspectiontime,et_otheryear,
	         et_firstname,et_lastname,et_email,et_phone,et_inspectionaddress1,et_inspectionaddress2,et_inspectioncity,et_inspectionzip,
	         et_mailinginspectionaddress1,et_mailinginspectionaddress2,et_mailinginspectioncity,et_mailinginspectionzip,et_inspectionfee;
	Spinner sp_yearconstruction,sp_inspectionstate,sp_inspectioncounty,sp_mailinginspectionstate,sp_mailinginspectioncounty;
	String str_inspectioncompany="",str_yearofconstruction="",statevalue="false",str_inspectionstate="",str_inspectionstateid="",strorderid="",
		   str_mailingstate="",str_mailingstateid="",str_mailingcounty="",str_mailingcountyid="",str_inspectionaddress1="",
		   str_inspectionaddress2="",str_inspectioncity="",str_inspectionzip="",str_inspectioncounty="",str_inspectioncountyid="",
		   str_policynumber="",str_squarefootage="",str_numberofstories="",str_inspectiondate="",str_inspectiontime="",
		   str_otheryearofconstruction="",str_firstname="",str_lastname="",str_email="",str_phone="",str_mailingaddress1="",
		   str_mailingaddress2="",str_mailingcity="",str_mailingzip="",str_inspectionfee="",error_msg="",title="Order Inspection";
	DatePickerDialog datePicker;
	TimePickerDialog timepicker;
	LinearLayout yearlinear, maillinear;
	CheckBox cb_mailing, cb_mail;
	
	boolean call_county2=true;
	String[] arraystateid, arraystatename, arraycountyname, arraycountyid, arraycountyname2, arraycountyid2,arraystateid2,arraystatename2;
	ArrayAdapter inspectionstateadapter, countyadapter, countyadapter2;
	String randomid="",str_jobno="";
	String yearbuilt[] = { "--Select--", "Not Applicable","Unknown","Other", "2014","2013", "2012", "2011","2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003",
			"2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995","1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987",
			"1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979","1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971",
			"1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963","1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955",
			"1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947","1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939",
			"1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931","1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923",
			"1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915","1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907",
			"1906", "1905", "1904", "1903", "1902", "1901", "1900" };
	private int handler_msg=2;
	int mailingchk=0,succhk=0;	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		
		 db.CreateTable(1);
		 db.CreateTable(28);
		
         setContentView(R.layout.placeorder);
         rand = new Random();
         et_inspectioncompany = (EditText)findViewById(R.id.orderinspection_preferredimc);
         et_policynumber = (EditText)findViewById(R.id.orderinspection_policynumber);
         et_jobno = (EditText)findViewById(R.id.orderinsp_etjobno);
         et_squarefootage = (EditText)findViewById(R.id.orderinsp_etsquarefootage);
         et_numberofstories = (EditText)findViewById(R.id.orderinspection_etnoofstories);
         et_inspectiondate = (EditText)findViewById(R.id.orderinsp_etinspdate);
         et_inspectiontime = (EditText)findViewById(R.id.orderinsp_etinsptime);
         sp_yearconstruction = (Spinner)findViewById(R.id.orderinspection_spinneryear);
         yearlinear = (LinearLayout)findViewById(R.id.yearlin);
         et_otheryear = (EditText)findViewById(R.id.orderinspection_etotheryear);
         et_inspectionfee = (EditText)findViewById(R.id.orderinsp_etinspfee);
         et_firstname = (EditText)findViewById(R.id.orderinspection_etfirstanme);
         et_lastname = (EditText)findViewById(R.id.orderinspection_etlastname);
         et_email = (EditText)findViewById(R.id.orderinspection_etmail);
         et_phone = (EditText)findViewById(R.id.orderinspection_etphone);
         et_inspectionaddress1 = (EditText)findViewById(R.id.orderinspection_etinspectionaddress1);
         et_inspectionaddress2 = (EditText)findViewById(R.id.orderinspection_etinspectionaddress2);
         et_inspectioncity = (EditText)findViewById(R.id.orderinspection_etcity);
         sp_inspectionstate = (Spinner)findViewById(R.id.orderinspection_spinnerstate);
         /*sp_inspectioncounty = (Spinner)findViewById(R.id.orderinspection_spinnercounty);
         sp_inspectioncounty.setEnabled(false);*/
         et_inspectionzip = (EditText)findViewById(R.id.orderinspection_etzip);
         
         cb_mail = (CheckBox)findViewById(R.id.orderinspection_mailing);
         maillinear = (LinearLayout)findViewById(R.id.maillin);
         
         cb_mail.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				 if(isChecked)
				 {
					 maillinear.setVisibility(buttonView.GONE);
				 }
				 else
				 {
					 maillinear.setVisibility(buttonView.VISIBLE);
					 cb_mailing.setChecked(false);
					 et_mailinginspectionaddress1.setText("");
					 et_mailinginspectionaddress2.setText("");
					 et_mailinginspectioncity.setText("");
					 sp_mailinginspectionstate.setSelection(0);
					 et_mailinginspectionzip.setText("");
				 }
			}
		});
         
         //mailing information
         et_mailinginspectionaddress1 = (EditText)findViewById(R.id.orderinspection_etmailingaddress1);
         et_mailinginspectionaddress2 = (EditText)findViewById(R.id.orderinspection_etmailingaddress2);
         et_mailinginspectioncity = (EditText)findViewById(R.id.orderinspection_etcity2);
         sp_mailinginspectionstate = (Spinner)findViewById(R.id.orderinspection_spinnerstate2);
         /*sp_mailinginspectioncounty = (Spinner)findViewById(R.id.orderinspection_spinnercounty2);
         sp_mailinginspectioncounty.setEnabled(false);*/
         et_mailinginspectionzip = (EditText)findViewById(R.id.orderinspection_etzip2);
        
         et_policynumber.addTextChangedListener(new CustomTextWatcher(et_policynumber));
         et_squarefootage.addTextChangedListener(new CustomTextWatcher(et_squarefootage));
         et_numberofstories.addTextChangedListener(new CustomTextWatcher(et_numberofstories));
         et_firstname.addTextChangedListener(new CustomTextWatcher(et_firstname));
         et_lastname.addTextChangedListener(new CustomTextWatcher(et_lastname));
         et_email.addTextChangedListener(new CustomTextWatcher(et_email));
         et_phone.addTextChangedListener(new phone_nowatcher(et_phone));
         et_inspectionaddress1.addTextChangedListener(new CustomTextWatcher(et_inspectionaddress1));
         et_inspectionaddress2.addTextChangedListener(new CustomTextWatcher(et_inspectionaddress2));
         et_inspectioncity.addTextChangedListener(new CustomTextWatcher(et_inspectioncity));
         et_inspectionzip.addTextChangedListener(new CustomTextWatcher(et_inspectionzip));
         
         //mailing information
         et_mailinginspectionaddress1.addTextChangedListener(new CustomTextWatcher(et_mailinginspectionaddress1));
         et_mailinginspectionaddress2.addTextChangedListener(new CustomTextWatcher(et_mailinginspectionaddress2));
         et_mailinginspectioncity.addTextChangedListener(new CustomTextWatcher(et_mailinginspectioncity));
         et_mailinginspectionzip.addTextChangedListener(new CustomTextWatcher(et_mailinginspectionzip));
         
         cb_mailing = (CheckBox)findViewById(R.id.orderinspection_addresscheck);
         
         cb_mailing.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
				{
					mailingchk =1;
					
					str_inspectionaddress1 = et_inspectionaddress1.getText().toString();
					str_inspectionaddress2 = et_inspectionaddress2.getText().toString();
					str_inspectioncity = et_inspectioncity.getText().toString();
					str_inspectionzip = et_inspectionzip.getText().toString();
					
					call_county2=false;
					
					et_mailinginspectionaddress1.setText(str_inspectionaddress1);
					et_mailinginspectionaddress2.setText(str_inspectionaddress2);
					et_mailinginspectioncity.setText(str_inspectioncity);
					et_mailinginspectionzip.setText(str_inspectionzip);
					str_mailingstate=str_inspectionstate;
					str_mailingstateid=str_inspectionstateid;
					str_mailingcounty=str_inspectioncounty;
					str_mailingcountyid=str_inspectioncountyid;
					
					arraystateid2=arraystateid;
					arraystatename2=arraystatename;
					arraycountyid2=arraycountyid;
					arraycountyname2=arraycountyname;
					
					sp_mailinginspectionstate.setAdapter(inspectionstateadapter);
					//sp_mailinginspectioncounty.setAdapter(countyadapter);
					sp_mailinginspectionstate.setSelection(sp_inspectionstate.getSelectedItemPosition());
					/*if(!sp_mailinginspectionstate.equals("--Select--"))
					{
					   sp_mailinginspectioncounty.setEnabled(true);
					   sp_mailinginspectioncounty.setSelection(sp_inspectioncounty.getSelectedItemPosition());
					}
					else
					{
						sp_mailinginspectioncounty.setEnabled(false);
					}*/
				}
				else
				{
					mailingchk =0;
					
					arraystateid2=null;arraystatename2=null;arraycountyid2=null;arraycountyname2=null;
					et_mailinginspectionaddress1.setText("");
					et_mailinginspectionaddress2.setText("");
					et_mailinginspectioncity.setText("");
					et_mailinginspectionzip.setText("");
					sp_mailinginspectionstate.setSelection(0);
					//sp_mailinginspectioncounty.setEnabled(false);
					call_county2=true;
				}
			}
		});
         
         ArrayAdapter<String> yearadapter = new ArrayAdapter<String>(PlaceOrder.this, android.R.layout.simple_spinner_item,yearbuilt);
 		 yearadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 		 sp_yearconstruction.setAdapter(yearadapter);
 		 sp_yearconstruction.setSelection(1);
 		 sp_yearconstruction.setOnItemSelectedListener(new OnItemSelectedListener() {

  			@Override
  			public void onItemSelected(AdapterView<?> arg0, View arg1,
  					int arg2, long arg3) {
  				// TODO Auto-generated method stub
  				str_yearofconstruction = sp_yearconstruction.getSelectedItem().toString();
  				if (str_yearofconstruction.equals("Other"))
  				{
  				    yearlinear.setVisibility(View.VISIBLE);
  				} else {
  					yearlinear.setVisibility(View.GONE);
  				}
  			}

  			@Override
  			public void onNothingSelected(AdapterView<?> arg0) {
  				// TODO Auto-generated method stub

  			}
  		});
 		 
 		et_otheryear.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,int count)
			{
				final Calendar c = Calendar.getInstance();
				final int year1 = c.get(Calendar.YEAR);
				
				if (et_otheryear.getText().toString().trim().matches("^0")) {
					// Not allowed
					et_otheryear.setText("");
				}
				if (!et_otheryear.getText().toString().trim().equals("")) {
					if (Integer.parseInt((et_otheryear.getText().toString().trim())) > year1) {
						et_otheryear.setText("");
						cf.DisplayToast("Please enter a valid year of construction");
						
					}
				}
				if (Arrays.asList(yearbuilt).contains(et_otheryear.getText().toString().trim())) {
					et_otheryear.setText("");
					cf.DisplayToast("Please enter a year that is not in the year of construction list");
				
				}

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void afterTextChanged(Editable s) {
			}
		});
          
 	    statevalue = LoadState();
 	    if(statevalue.equals("true"))
		{
			inspectionstateadapter = new ArrayAdapter<String>(PlaceOrder.this,android.R.layout.simple_spinner_item,arraystatename);
			inspectionstateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			sp_inspectionstate.setAdapter(inspectionstateadapter);
			
			//mailing information
			inspectionstateadapter = new ArrayAdapter<String>(PlaceOrder.this,android.R.layout.simple_spinner_item,arraystatename);
			inspectionstateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			sp_mailinginspectionstate.setAdapter(inspectionstateadapter);
		
		}
 	    
 	   sp_inspectionstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				str_inspectionstate = sp_inspectionstate.getSelectedItem().toString();
				int stateid = sp_inspectionstate.getSelectedItemPosition();
				str_inspectionstateid = arraystateid[stateid];
				/*if (!str_inspectionstate.equals("--Select--")) {
					LoadCounty(str_inspectionstateid);
					sp_inspectioncounty.setEnabled(true);

				} else {
					
					
					sp_inspectioncounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(PlaceOrder.this,android.R.layout.simple_spinner_item,arraycountyname);
					countyadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					sp_inspectioncounty.setAdapter(countyadapter);

				}*/

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
 	    
 	/*  sp_inspectioncounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				str_inspectioncounty = sp_inspectioncounty.getSelectedItem().toString();
				int id = sp_inspectioncounty.getSelectedItemPosition();
				str_inspectioncountyid = arraycountyid[id];

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});*/

 	  sp_mailinginspectionstate.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				call_county2=true;
				return false;
			}
		});

 	 sp_mailinginspectionstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				str_mailingstate = sp_mailinginspectionstate.getSelectedItem().toString();
				int stateid = sp_mailinginspectionstate.getSelectedItemPosition();
				str_mailingstateid = arraystateid[stateid];
				
				/*if(call_county2==true)
				{
					if (!str_mailingstate.equals("--Select--")) {
						LoadCounty2(str_mailingstateid);
						sp_mailinginspectioncounty.setEnabled(true);

					} else {
						
						sp_mailinginspectioncounty.setEnabled(false);
						arraycountyname2 = new String[0];
						countyadapter2 = new ArrayAdapter<String>(PlaceOrder.this,android.R.layout.simple_spinner_item,arraycountyname2);
						countyadapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						sp_mailinginspectioncounty.setAdapter(countyadapter2);
					}
				}
				else
				{
					sp_mailinginspectioncounty.setEnabled(false);
					sp_mailinginspectioncounty.setSelection(sp_mailinginspectioncounty.getSelectedItemPosition());
				}*/
				

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

 	 /* sp_mailinginspectioncounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				str_mailingcounty = sp_mailinginspectioncounty.getSelectedItem().toString();
				int id = sp_mailinginspectioncounty.getSelectedItemPosition();
				str_mailingcountyid = arraycountyid2[id];

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});*/
 	 
        db.userid();
         System.out.println("db.UserId"+db.UserId);
         try
         {
        	 Cursor c = db.hi_db.rawQuery("select * from "+db.Registration+" where fld_userid='"+db.UserId+"'", null);
        	 System.out.println("select * from "+db.Registration+" where fld_userid='"+db.UserId+"'"+"c.getc="+c.getCount());
        	 if(c.getCount()>0)
        	 {
        		 c.moveToFirst();
        		 str_inspectioncompany = db.decode(c.getString(c.getColumnIndex("fld_companyname")));
        		 et_inspectioncompany.setText(str_inspectioncompany);
        	 }
         }
         catch (Exception e) {
			// TODO: handle exception
		}
         
         ((Button)findViewById(R.id.btn_back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(PlaceOrder.this,HomeScreen.class));
				finish();
			}
		});
         
         ((Button)findViewById(R.id.orderinsp_getdate)).setOnClickListener(new OnClickListener() {
 			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 				showDialog(0);
 			}
 		});
         
         ((Button)findViewById(R.id.orderinsp_gettime)).setOnClickListener(new OnClickListener() {
  			
  			@Override
  			public void onClick(View v) {
  				// TODO Auto-generated method stub
  				showDialog(1);
  			}
  		});
         
        ((Button)findViewById(R.id.orderinsp_clear)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				   mailingchk =0;
				   
			       et_inspectioncompany.setText("");	
			       et_policynumber.setText("");
			       et_squarefootage.setText("");
			       et_numberofstories.setText("");
			       et_inspectiondate.setText("");
			       et_inspectiontime.setText("");
			       sp_yearconstruction.setSelection(0);
			       et_otheryear.setText("");
			       yearlinear.setVisibility(v.GONE);
			       et_firstname.setText("");
			       et_lastname.setText("");
			       et_email.setText("");
			       et_phone.setText("");
			       et_inspectionaddress1.setText("");
			       et_inspectionaddress2.setText("");
			       et_inspectioncity.setText("");
			       sp_inspectionstate.setSelection(0);
			       //sp_inspectioncounty.setEnabled(false);
			       et_inspectionzip.setText("");
			       cb_mailing.setChecked(false);
			       et_mailinginspectionaddress1.setText("");
			       et_mailinginspectionaddress2.setText("");
			       et_mailinginspectioncity.setText("");
			       sp_mailinginspectionstate.setSelection(0);
			       //sp_mailinginspectioncounty.setEnabled(false);
			       et_mailinginspectionzip.setText("");
			}
		});
        
        ((Button)findViewById(R.id.orderinsp_submit)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			       orderinspection();	
			}
		});
        
        et_mailinginspectionzip.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        boolean handled = false;
		        if (actionId == EditorInfo.IME_ACTION_GO) {
		        	orderinspection();
		            handled = true;
		        }
		        return handled;
		    }
		});
        
        et_inspectiondate.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				showDialog(0);
				return false;
			}
		});
        
        et_inspectiontime.setOnTouchListener(new OnTouchListener() {			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				showDialog(1);
				return false;
			}
		});
	}
	protected void orderinspection() {
		// TODO Auto-generated method stub
		str_inspectioncompany = et_inspectioncompany.getText().toString();
		str_policynumber = et_policynumber.getText().toString();
		str_squarefootage = et_squarefootage.getText().toString();
		str_numberofstories = et_numberofstories.getText().toString();
		str_jobno= et_jobno.getText().toString();
		str_inspectiondate = et_inspectiondate.getText().toString();
		str_inspectiontime = et_inspectiontime.getText().toString();
		if(str_yearofconstruction.equals("Other"))
		{
			str_otheryearofconstruction = et_otheryear.getText().toString();
		}
		str_inspectionfee = et_inspectionfee.getText().toString();
		
		str_firstname = et_firstname.getText().toString();
		str_lastname = et_lastname.getText().toString();
		str_email = et_email.getText().toString();
		str_phone = et_phone.getText().toString();
		
		str_inspectionaddress1 = et_inspectionaddress1.getText().toString();
		str_inspectionaddress2 = et_inspectionaddress2.getText().toString();
		str_inspectioncity = et_inspectioncity.getText().toString();
		str_inspectionzip = et_inspectionzip.getText().toString();
		
		str_mailingaddress1 = et_mailinginspectionaddress1.getText().toString();
		str_mailingaddress2 = et_mailinginspectionaddress2.getText().toString();
		str_mailingcity = et_mailinginspectioncity.getText().toString();
		str_mailingzip = et_mailinginspectionzip.getText().toString();
		
		
		if(str_squarefootage.trim().equals(""))
		{
			cf.DisplayToast("Please enter square footage");
		}
		else
		{
			if(str_numberofstories.trim().equals(""))
			{
				cf.DisplayToast("Please enter no. of stories");
			}
			else
			{
				if(str_inspectiondate.trim().equals(""))
				{
					cf.DisplayToast("Please enter inspection date");
				}
				else
				{
					/*if (checkfortodaysdate(str_inspectiondate).equals("equal")) 
					{
						cf.DisplayToast("Inspection Date should not be Today's Date");
					}
					else if (checkfortodaysdate(str_inspectiondate).equals("greater")) 
					{
						cf.DisplayToast("Inspection Date should not be greater than or equal to Today's Date");
					}
					else
					{*/
						if(str_inspectiontime.trim().equals(""))
						{
							cf.DisplayToast("Please enter inspection time");
						}
						else
						{
							if(str_yearofconstruction.equals("--Select--"))
							{
								cf.DisplayToast("Please select year of construction");
							}
							else
							{
								if(str_yearofconstruction.equals("Other"))
								{
									if(str_otheryearofconstruction.trim().equals(""))
									{
										cf.DisplayToast("Please enter other text for year of construction");
									}
									else
									{
										clientvalidation();
									}
								}
								else
								{
									clientvalidation();
								}
							}
						}
						
					//}	
				}
			}
		}
	}
	private String checkfortodaysdate(String insurancedate) {
		String chkdate = "false";
		int i1 = insurancedate.indexOf("/");
		String result = insurancedate.substring(0, i1);
		int i2 = insurancedate.lastIndexOf("/");
		String result1 = insurancedate.substring(i1 + 1, i2);
		String result2 = insurancedate.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;
		if (j < thsyr || (j1 < curmnth && j <= thsyr)
				|| (j2 < curdate && j1 <= curmnth && j <= thsyr)) {
			chkdate = "lesser";
		}
		else if (j < thsyr || (j1 < curmnth && j <= thsyr)
				|| (j2 <= curdate && j1 <= curmnth && j <= thsyr)) {
			chkdate = "equal";
		}
		else {
			chkdate = "greater";
		}
		System.out.println("chkdate="+chkdate);
		return chkdate;

	}
	private void clientvalidation() {
		// TODO Auto-generated method stub
		if(str_firstname.trim().equals(""))
		{
			cf.DisplayToast("Please enter first name");
		}
		else
		{
			
			if(str_lastname.trim().equals(""))
			{
				cf.DisplayToast("Please enter last name");
			}
			else
			{
			      if(!str_email.trim().equals(""))
			      {
			    	    boolean emailbool = cf.EmailValidation(str_email);
						if(!emailbool)
						{
							cf.DisplayToast("Please enter valid email");
						}
						else
						{
							phonenumber_validation();
						}
			      }
			      else
			      {
			    	  phonenumber_validation();
			      }
			
			}
		}
	}
	private void phonenumber_validation() {
		// TODO Auto-generated method stub
		if(!str_phone.trim().equals(""))
		{
			boolean phnnumbool = cf.PhonenumValidation(str_phone);
			if(!phnnumbool)
			{
				cf.DisplayToast("Please enter valid phone number");
			}
			else
			{
				inspectionaddress_validation();
			}
		}
		else
		{
			inspectionaddress_validation();
		}
	}
	private void inspectionaddress_validation() {
		// TODO Auto-generated method stub
		if(str_inspectionaddress1.trim().equals(""))
		{
			cf.DisplayToast("Please enter inspection address #1");
		}
		else
		{			
			if(!str_inspectionzip.trim().equals("") || !str_mailingzip.trim().equals(""))
			{
				if(str_inspectionzip.length()<5 || str_mailingzip.length()<5)
				{
					if(str_inspectionzip.length()<5 && !str_inspectionzip.trim().equals("") )
					{
						cf.DisplayToast("Please enter valid inspection zip");
					}
					else if(str_mailingzip.length()<5 && !str_mailingzip.trim().equals("") )
					{
						cf.DisplayToast("Please enter valid mailing inspection zip");
					}
					else
					{
						//Placeorder_CallWebservice();
						Insert_Orderinspection();
					}	
				}
				else
				{
					//Placeorder_CallWebservice();
					Insert_Orderinspection();
				}				
			}
			else
			{
				//Placeorder_CallWebservice();
				Insert_Orderinspection();
			}
		}
			/*if(str_inspectioncity.trim().equals(""))
			{
				cf.DisplayToast("Please enter inspection city");
			}
			else
			{
				if(str_inspectionstate.equals("--Select--"))
				{
					cf.DisplayToast("Please select inspection state");
				}
				else
				{
					if(str_inspectioncounty.equals("--Select--"))
					{
						cf.DisplayToast("Please select inspection county");
					}
					else
					{
						if(str_inspectionzip.trim().equals(""))
						{
							cf.DisplayToast("Please enter inspection zip");
						}
						else
						{
							if(str_inspectionzip.length()<5)
							{
								cf.DisplayToast("Please enter valid inspection zip");
							}
							else
							{
								if(str_mailingaddress1.trim().equals(""))
								{
									cf.DisplayToast("Please enter mailing address #1");
								}
								else
								{
									if(str_mailingcity.trim().equals(""))
									{
										cf.DisplayToast("Please enter mailing city");
									}
									else
									{
										if(str_mailingstate.equals("--Select--"))
										{
											cf.DisplayToast("Please select mailing state");
										}
										else
										{
											if(str_mailingcounty.equals("--Select--"))
											{
												cf.DisplayToast("Please select mailing county");
											}
											else
											{
												if(str_mailingzip.trim().equals(""))
												{
													cf.DisplayToast("Please enter mailing zip");
												}
												else
												{
													if(str_mailingzip.length()<5)
													{
														cf.DisplayToast("Please enter valid mailing zip");
													}
													else
													{
														if(cf.isInternetOn())
														{
															  new Start_xporting().execute("");
														}
														else
														{
															  cf.DisplayToast("Please enable your internet connection.");
														}
													}
												}
											}
										}
									}
								}
							}
							}
						}
					}
				}
			}*/
		
	}
	private void Insert_Orderinspection() {
		// TODO Auto-generated method stub
		try
 		 {			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			randomid="HI_"+sdf.format(new Date())+"_"+rand.nextInt(1000);
			
 			  db.hi_db.execSQL("INSERT INTO "
 						+ db.OrderInspection
 						+ " (fld_inspectioncompany,fld_policynumber,fld_jobno,fld_squarefootage,fld_noofstories,fld_inspectiondate,fld_inspectiontime,fld_yearofconstruction,"+
 						"fld_otheryearofconstruction,fld_inspectionfee,fld_firstname,fld_lastname,fld_email,fld_phone,"+
 						"fld_inspectionaddress1,fld_inspectionaddress2,fld_inspectioncity,fld_inspectionstate,fld_inspectioncounty,fld_zip,fld_flag,"+
				        "fld_mailingaddress1,fld_mailingaddress2,fld_mailingcity,fld_mailingstate,fld_mailingcounty,fld_mailingzip,"+
 						"fld_inspectorid,fld_orderedid,fld_status)"
 						+ " VALUES ('" + db.encode(str_inspectioncompany) + "','"+db.encode(str_policynumber)+"','"+db.encode(str_jobno)+"',"+
 						"'"+str_squarefootage+"','"+str_numberofstories+"',"+
 						"'"+str_inspectiondate+"','"+str_inspectiontime+"',"+
 						"'"+str_yearofconstruction+"','"+db.encode(str_otheryearofconstruction)+"',"+
 						"'"+db.encode(str_inspectionfee)+"','"+db.encode(str_firstname)+"',"+
 						"'"+db.encode(str_lastname)+"','"+db.encode(str_email)+"',"+
 						"'"+db.encode(str_phone)+"','"+db.encode(str_inspectionaddress1)+"',"+
 						"'"+db.encode(str_inspectionaddress2)+"','"+db.encode(str_inspectioncity)+"',"+
 						"'"+db.encode(str_inspectionstate)+"','"+db.encode(str_inspectioncounty)+"',"+
 						"'"+str_inspectionzip+"','1',"+
 						"'"+db.encode(str_mailingaddress1)+"','"+db.encode(str_mailingaddress2)+"',"+
 						"'"+db.encode(str_mailingcity)+"','"+db.encode(str_mailingstate)+"',"+
 						"'"+db.encode(str_mailingcounty)+"','"+str_mailingzip+"',"+
 						"'"+db.UserId+"','"+randomid+"','1')");
 			 success_alert();
 		}
 		catch (Exception e) {
 			// TODO: handle exception
 			System.out.println("testcatch="+e.getMessage());
 		}
	}
	private void Placeorder_CallWebservice()
	{
		if(cf.isInternetOn())
		{
			  new Start_xporting().execute("");
		}
		else
		{
			  cf.DisplayToast("Please enable your internet connection.");
		}		
	}
	class Start_xporting extends AsyncTask <String, String, String> {
		
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		        p_sv.progress_title="Order Inspection";
			    p_sv.progress_Msg="Please wait..Your ordered inspection request has been submitting.";
				
				startActivityForResult(new Intent(PlaceOrder.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl) {		
			 	try {
					
			 		Send_record_to_server();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					handler_msg=0;
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler_msg=0;
				}
		                return null;
		    }
		
		   

			protected void onProgressUpdate(String... progress) {
		
		     
		    }
		
		    @Override
		    protected void onPostExecute(String unused) {
		    	p_sv.progress_live=false;		    	
		    	System.out.println("hand"+handler_msg);
		    	if(handler_msg==0)
		    	{
		    		success_alert();		    		
		    		
		    	}
		    	else if(handler_msg==1)
		    	{
		    		
		    		failure_alert();
		    		
		    	}
		    	
		    }
	 }
	public String LoadState() {
		try {
			dbh = new DataBaseHelper(PlaceOrder.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = db.decode(cur.getString(cur.getColumnIndex("stateid")));
					String Category = db.decode(cur.getString(cur.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}
	
    private void Send_record_to_server() throws IOException, XmlPullParserException {
		
		db.userid();
		 
	    SoapObject request = new SoapObject(wb.NAMESPACE,"OrderInspection");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
 		envelope.dotNet = true;
 		
 		request.addProperty("OrderId",0);
 		request.addProperty("fld_inspectioncompany",str_inspectioncompany);
 		request.addProperty("fld_policynumber",str_policynumber);
 		request.addProperty("fld_squarefootage",str_squarefootage);
 		request.addProperty("fld_noofstories",str_numberofstories);
 		request.addProperty("fld_inspectiondate",str_inspectiondate);
 		request.addProperty("fld_inspectiontime",str_inspectiontime);
 		request.addProperty("fld_yearofconstruction",str_yearofconstruction);
 		request.addProperty("fld_otheryearofconstruction",str_otheryearofconstruction);
 		request.addProperty("fld_inspectionfee",str_inspectionfee);
 		request.addProperty("fld_firstname",str_firstname);
 		request.addProperty("fld_lastname",str_lastname);
 		request.addProperty("fld_email",str_email);
 		request.addProperty("fld_phone",str_phone);
 		request.addProperty("fld_inspectionaddress1",str_inspectionaddress1);
 		request.addProperty("fld_inspectionaddress2",str_inspectionaddress2);
 		request.addProperty("fld_inspectioncity",str_inspectioncity);
 		request.addProperty("fld_inspectionstate",str_inspectionstate);
 		request.addProperty("fld_inspectioncounty",str_inspectioncounty);
 		request.addProperty("fld_zip",str_inspectionzip);
 		request.addProperty("fld_flag",mailingchk);
 		request.addProperty("fld_mailingaddress1",str_mailingaddress1);
 		request.addProperty("fld_mailingaddress2",str_mailingaddress2);
 		request.addProperty("fld_mailingcity",str_mailingcity);
 		request.addProperty("fld_mailingstate",str_mailingstate);
 		request.addProperty("fld_mailingcounty",str_mailingcounty);
 		request.addProperty("fld_mailingzip",str_mailingzip);
 		request.addProperty("fld_inspectorid",db.UserId);
 		request.addProperty("fld_status",1); //For place order 1 = new record placing
 		
 		envelope.setOutputSoapObject(request);
 		System.out.println("request="+request);
 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
 		SoapObject response = null;
        try
        {
              androidHttpTransport.call(wb.NAMESPACE+"OrderInspection", envelope);
              response = (SoapObject)envelope.getResponse();
              System.out.println("OrderInspection "+response);
              
              //statuscode ==0 -> sucess && statuscode !=0 -> failure
              if(response.getProperty("StatusCode").toString().equals("0"))
      		  {
          		 str_policynumber = response.getProperty("fld_policynumber").toString().equals("anyType{}")?"N/A":response.getProperty("fld_policynumber").toString();
          		 str_otheryearofconstruction = response.getProperty("fld_otheryearofconstruction").toString().equals("anyType{}")?"":response.getProperty("fld_otheryearofconstruction").toString();
          		 str_inspectionfee = response.getProperty("fld_inspectionfee").toString().equals("anyType{}")?"":response.getProperty("fld_inspectionfee").toString();
          		 str_email = response.getProperty("fld_email").toString().equals("anyType{}")?"":response.getProperty("fld_email").toString();
          		 str_phone = response.getProperty("fld_phone").toString().equals("anyType{}")?"":response.getProperty("fld_phone").toString();
          		 str_inspectionaddress2 = response.getProperty("fld_inspectionaddress2").toString().equals("anyType{}")?"":response.getProperty("fld_inspectionaddress2").toString();
          		 str_mailingaddress2 = response.getProperty("fld_mailingaddress2").toString().equals("anyType{}")?"":response.getProperty("fld_mailingaddress2").toString();
          		 strorderid = response.getProperty("OrderId").toString();
            	 try
          		 {
          			  db.hi_db.execSQL("INSERT INTO "
          						+ db.OrderInspection
          						+ " (fld_inspectioncompany,fld_policynumber,fld_squarefootage,fld_noofstories,fld_inspectiondate,fld_inspectiontime,fld_yearofconstruction,"+
          						"fld_otheryearofconstruction,fld_inspectionfee,fld_firstname,fld_lastname,fld_email,fld_phone,"+
          						"fld_inspectionaddress1,fld_inspectionaddress2,fld_inspectioncity,fld_inspectionstate,fld_inspectioncounty,fld_zip,fld_flag,"+
						        "fld_mailingaddress1,fld_mailingaddress2,fld_mailingcity,fld_mailingstate,fld_mailingcounty,fld_mailingzip,"+
          						"fld_inspectorid,fld_orderedid,fld_status)"
          						+ " VALUES ('" + db.encode(response.getProperty("fld_inspectioncompany").toString()) + "','"+db.encode(str_policynumber)+"',"+
          						"'"+response.getProperty("fld_squarefootage").toString()+"','"+response.getProperty("fld_noofstories").toString()+"',"+
          						"'"+response.getProperty("fld_inspectiondate").toString()+"','"+response.getProperty("fld_inspectiontime").toString()+"',"+
          						"'"+response.getProperty("fld_yearofconstruction").toString()+"','"+db.encode(str_otheryearofconstruction)+"',"+
          						"'"+db.encode(str_inspectionfee)+"','"+db.encode(response.getProperty("fld_firstname").toString())+"',"+
          						"'"+db.encode(response.getProperty("fld_lastname").toString())+"','"+db.encode(str_email)+"',"+
          						"'"+db.encode(str_phone)+"','"+db.encode(response.getProperty("fld_inspectionaddress1").toString())+"',"+
          						"'"+db.encode(str_inspectionaddress2)+"','"+db.encode(response.getProperty("fld_inspectioncity").toString())+"',"+
          						"'"+db.encode(response.getProperty("fld_inspectionstate").toString())+"','"+db.encode(response.getProperty("fld_inspectioncounty").toString())+"',"+
          						"'"+response.getProperty("fld_zip").toString()+"','"+response.getProperty("fld_flag").toString()+"',"+
          						"'"+db.encode(response.getProperty("fld_mailingaddress1").toString())+"','"+db.encode(str_mailingaddress2)+"',"+
          						"'"+db.encode(response.getProperty("fld_mailingcity").toString())+"','"+db.encode(response.getProperty("fld_mailingstate").toString())+"',"+
          						"'"+db.encode(response.getProperty("fld_mailingcounty").toString())+"','"+response.getProperty("fld_mailingzip").toString()+"',"+
          						"'"+response.getProperty("fld_inspectorid").toString()+"','"+response.getProperty("OrderId").toString()+"','"+response.getProperty("fld_status").toString()+"')");
          			
          		}
          		catch (Exception e) {
          			// TODO: handle exception
          		}
              	 handler_msg=0;
      		  }
      		  else
      		  {
      			 error_msg = response.getProperty("StatusMessage").toString();
      			 handler_msg=1;
      			
      		  }
        }
        catch(Exception e)
        {
             e.printStackTrace();
        }
        
       
   }
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(PlaceOrder.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
			}
		}).setTitle(title+" Failure").create();
		al.setCancelable(false);
		al.show();
	}

	public void success_alert() {
		// TODO Auto-generated method stub
		succhk=1;
		AlertDialog al = new AlertDialog.Builder(PlaceOrder.this).setMessage("Your request of home inspection has been successfully ordered.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				handler_msg = 2 ;
				succhk=0;
				cf.chkstat=1;
				Intent intent = new Intent(PlaceOrder.this,LoadModules.class);
				 intent.putExtra("selectedid", randomid);
				 intent.putExtra("inspectorid", db.UserId);
		         startActivity(intent);
				 finish();
				/*startActivity(new Intent(PlaceOrder.this,Dashboard.class));
				finish();*/
				
			}
		}).setTitle(title+" Success").create();
		al.setCancelable(false);
		al.show();
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {	 
	   System.out.println("onsave"+handler_msg);
        outState.putInt("handler", handler_msg);
        outState.putString("error", error_msg);
        outState.putString("mailstate", str_mailingstate);
        outState.putInt("successalert", succhk);

      //  outState.putString("mailcounty", sp_mailinginspectionstate.getSelectedItemPosition());
        super.onSaveInstanceState(outState);
    }
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	
    	
    	handler_msg = savedInstanceState.getInt("handler");
    	error_msg = savedInstanceState.getString("error");
    	str_mailingstate = savedInstanceState.getString("mailstate");
    	int id = savedInstanceState.getInt("mailcounty");
    	System.out.println("onrestore="+str_mailingstate+str_mailingcounty);
    	/*if(!str_mailingstate.equals("--Select--"))
    	{
    		sp_mailinginspectioncounty.setEnabled(true);
    		LoadCounty2();
    		//sp_mailinginspectioncounty.setSelection(countyadapter2.getPosition(str_mailingcounty));
    	}*/
    	/*if(handler_msg==0)
    	{
    		success_alert();
    	}
    	else if(handler_msg==1)
    	{
    		failure_alert();
    	}*/
    	if(savedInstanceState.getInt("successalert")==1)
    	{
    		success_alert();
    	}
        super.onRestoreInstanceState(savedInstanceState);
    }
	/*public void LoadCounty(final String stateid) {
		try {
			dbh = new DataBaseHelper(PlaceOrder.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table where stateid='" + db.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = db.decode(cur.getString(cur.getColumnIndex("countyid")));
					String Name = db.decode(cur.getString(cur.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			countyadapter = new ArrayAdapter<String>(PlaceOrder.this,android.R.layout.simple_spinner_item, arraycountyname);
			countyadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			sp_inspectioncounty.setAdapter(countyadapter);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}*/

	/*public void LoadCounty2(final String stateid) {
		try {
			dbh = new DataBaseHelper(PlaceOrder.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table where stateid='" + db.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid2 = new String[rows + 1];
			arraycountyname2 = new String[rows + 1];
			arraycountyid2[0] = "--Select--";
			arraycountyname2[0] = "--Select--";
			System.out.println("LoadCounty2 count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = db.decode(cur.getString(cur.getColumnIndex("countyid")));
					String Name = db.decode(cur.getString(cur.getColumnIndex("countyname")));
					arraycountyid2[i] = id;
					arraycountyname2[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			
			countyadapter2 = new ArrayAdapter<String>(PlaceOrder.this,android.R.layout.simple_spinner_item, arraycountyname2);
			countyadapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			sp_mailinginspectioncounty.setAdapter(countyadapter2);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}
*/
	
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			// set date picker as current date
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			datePicker = new DatePickerDialog (this, datePickerListener, year, month, day); 
			   
			return datePicker;

		case 1:
			// set time picker as current time
			final Calendar c1 = Calendar.getInstance();
			int hour = c1.get(Calendar.HOUR_OF_DAY);
			int minute = c1.get(Calendar.MINUTE);
			timepicker= new TimePickerDialog(this, timePickerListener, hour, minute,false);
			return timepicker;
		}
		return null;
	}
	
	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			int year = selectedYear;
			int month = selectedMonth;
			int day = selectedDay;
			
			et_inspectiondate.setText(new StringBuilder().append(month + 1).append("/").append(day).append("/").append(year));
			
			
			/*if (checkfortodaysdate(et_inspectiondate.getText().toString()).equals("equal")) 
			{
				cf.DisplayToast("Inspection Date should not be Today's Date");
				et_inspectiondate.setText("");
			}
			else if (checkfortodaysdate(et_inspectiondate.getText().toString()).equals("greater")) 
			{
				et_inspectiondate.setText("");
				cf.DisplayToast("Inspection Date should not be greater than Today's Date");
			}
			else
			{*/
				et_inspectiondate.setText(new StringBuilder().append(month + 1).append("/").append(day).append("/").append(year));
			//}
			
			//updating date picker to current date
			final Calendar c = Calendar.getInstance();
			datePicker.updateDate(c.get(Calendar.YEAR), 
					 c.get(Calendar.MONTH), c.get(Calendar.DATE));

		}
	};

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {
			int hour = selectedHour;
			int minute = selectedMinute;
			StringBuilder sb = new StringBuilder();
			if (hour >= 12) {
				sb.append(hour - 12).append(":").append(minute).append(" PM");
			} else {
				sb.append(hour).append(":").append(minute).append(" AM");
			}
			et_inspectiontime.setText(sb);
			
			//updating time picker to current time
			final Calendar c = Calendar.getInstance();
			timepicker.updateTime(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));
		}
	};
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(PlaceOrder.this,HomeScreen.class));
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
