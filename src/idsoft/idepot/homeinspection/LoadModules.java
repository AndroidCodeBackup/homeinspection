package idsoft.idepot.homeinspection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.idepot.homeinspection.LoadModules.Start_xporting;
import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_export;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;



public class LoadModules extends Activity {
	Spinner spinloadtemplate;
	ProgressBar mProgressBar;
	ArrayAdapter adapter;
	  Dialog add_dialog;
	Progress_staticvalues p_sv;
	int j=0,handler_msg=2;
	static int exportalert=0,rws=0,rws2=0,notstarted=0;
	int[] modulearr = {R.id.ex_chk_structure,R.id.ex_chk_exterior,R.id.ex_chk_roofing,R.id.ex_chk_attic,R.id.ex_chk_garagereport,
R.id.ex_chk_plumbingsystem,R.id.ex_chk_electricalsystem,
			 R.id.ex_chk_hvac,R.id.ex_chk_kitchen,R.id.ex_chk_appliances,R.id.ex_chk_bathroom,R.id.ex_chk_interior,R.id.ex_chk_porchesscreensenclos,
			 R.id.ex_chk_swimmingpools,R.id.ex_chk_waterwells,R.id.ex_chk_cabanappolhouse};

	
	int[] imagetickarr = {R.id.ex_sta_structure,R.id.ex_sta_exterior,R.id.ex_sta_roofing,R.id.ex_sta_attic,R.id.ex_sta_garagereport,
R.id.ex_sta_plumbingsystem,R.id.ex_sta_electricalsystem,
			 R.id.ex_sta_hvac,R.id.ex_sta_kitchen,R.id.ex_sta_appliances,R.id.ex_sta_bathroom,R.id.ex_sta_interior,R.id.ex_sta_porchesscreensenclos,
			 R.id.ex_sta_swimmingpools,R.id.ex_sta_waterwells,R.id.ex_sta_cabanappolhouse};
	int showprogdialog=0,showexportelert=0;
	CheckBox ins_chk[]=new CheckBox[16];
	ImageView ins_sta_im[]=new ImageView[16];
	String insp_boo[] = { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",""};
	String insp_sta[] = { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",""};
	
	String main_boo[] = { "", "", "", "", ""};
	String main_sta[] = { "", "", "", "", ""};
	
	public boolean[] EX_Result = { false, false, false, false, false, false,false, false, false, false, false,false,false,false,false,false };
	RadioGroup rdg;
	LinearLayout par_log,chaild_log;
	double PerIns_inc = 0;
	Dialog exportdialog=null;
	double sub_total = 0;
	int delay = 1000; // Milliseconds of delay in the update loop
	TextView tv_head, tv_head_per;
	PowerManager.WakeLock wl = null;
	DatabaseFunction db;
	CommonFunction cf;
	Webservice_config wb;
	String strBase64,tempname,str_module,str_rows,na,str_rows1,gettemplatename="",getselectedho="",inspectorid="",getwarrantyoption="",error_msg="",title="",Exportbartext = "",sub_insp="";
	static String[] arrtempname,arrstrmodule,arrstrprogress,rows,rows1,arrna;
	ListView lst_module;
	int k=0,show_handler;
	static int sendtoserver[];
	public String Error_tracker = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b = this.getIntent().getExtras();
		if(b!=null)
		{
		  getselectedho = b.getString("selectedid");
		  inspectorid = b.getString("inspectorid");
		  
		}
		setContentView(R.layout.loadtemplate);
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		wb = new Webservice_config(this);
		db.CreateTable(1);
		db.CreateTable(2);
		db.CreateTable(7);
		db.CreateTable(31);
		db.userid();
		
		declaration();
	}
 
	private void declaration() {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur = db.hi_db.rawQuery("select * from " + db.CreateTemplate + " WHERE fld_inspectorid='"+db.UserId+"' order by fld_date,fld_time DESC", null);
            int cnt = cur.getCount();
            if(cnt>0)
            {
            	cur.moveToFirst();
            	tempname="--Select--"+"~";
            	for(int i=0;i<cur.getCount();i++,cur.moveToNext())
            	{
            		tempname += db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))) + "~";
            		if(tempname.equals("null")||tempname.equals(null))
                	{
            			tempname=tempname.replace("null", "");
                	}
    				arrtempname = tempname.split("~");
            	}
            	
            	
            }
            else
            {
            	tempname="--Select--"+"~";
            	arrtempname = tempname.split("~");
            }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		/*spinloadtemplate = (Spinner)findViewById(R.id.spinloadtemplate);
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrtempname);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinloadtemplate.setAdapter(adapter);
		spinloadtemplate.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
		*/
		((Button)findViewById(R.id.btn_back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(LoadModules.this, StartInspection.class);
				i.putExtra("inspectorid",inspectorid);
				startActivity(i);
				finish();
			}
		});
		
       ((Button)findViewById(R.id.btn_home)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(LoadModules.this, HomeScreen.class);
				startActivity(i);
				finish();
			}
		});
		
		
		lst_module = (ListView) findViewById(R.id.modulelist);
		sendtoserver = new int[cf.strarr_module.length];
		for(int i=0;i<cf.strarr_module.length;i++)
		{
			str_module += cf.strarr_module[i]+"~";
			arrstrmodule = str_module.split("~");
			if (arrstrmodule[i].contains("null")) {
				arrstrmodule[i] = arrstrmodule[i].replace("null", "");
			}
			
			try
			{
				Cursor c1 = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_module='"+db.encode(arrstrmodule[i])+"'", null);
				System.out.println("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_module='"+db.encode(arrstrmodule[i])+"'"+c1.getCount());
				str_rows += c1.getCount()+"~";
				rows = str_rows.split("~");
				if (rows[i].contains("null")) {
					rows[i] = rows[i].replace("null", "");
				}
				
				System.out.println("rows="+rows[i]+"sendserver="+sendtoserver[i]);
				if(c1.getCount()>0)
				{
					System.out.println("enter");
					c1.moveToFirst();
					sendtoserver[i] = c1.getInt(c1.getColumnIndex("fld_sendtoserver"));System.out.println("enterends");
					//na = new int[c1.getCount()];
					/*for(int j=0;j<c1.getCount();j++,c1.moveToNext())
					{
						try
						{
							    Cursor c2 = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and " +
									"fld_srid='"+getselectedho+"' and fld_module='"+db.encode(arrstrmodule[i])+"'", null);
							    System.out.println("c2count="+c2.getCount());
							    if(c2.getCount()>0)
							    {System.out.println("inside");
							    	c2.moveToFirst();
							    	na += c2.getInt(c2.getColumnIndex("fld_NA"))+"~";
							    	arrna = na.split("~");
							    	
							    	if (arrna[j].contains("null")) {
							    		arrna[j] = arrna[j].replace("null", "");
									}
							    	System.out.println("na[j]"+arrna[j]);
							    }
								str_rows1 += c2.getCount()+"~";
								rows1 = str_rows1.split("~");
								if (rows1[j].contains("null")) {
									rows1[j] = rows1[j].replace("null", "");
								}
							Cursor c2 = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and " +
								"fld_srid='"+getselectedho+"' and fld_module='"+arrstrmodule[i]+"' and (fld_NA='1' and fld_sendtoserver='0')", null);
							str_rows1 += c2.getCount()+"~";
							rows1 = str_rows1.split("~");
							if (rows1[j].contains("null")) {
								rows1[j] = rows1[j].replace("null", "");
							}
						}
						catch (Exception e) {
							// TODO: handle exception
						}
					}*/
				}
				else
				{
					System.out.println("elseenter");
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		
			lst_module.setAdapter(new EfficientAdapter(this));

		}
		
		lst_module.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = null;
			    String clkmod = arrstrmodule[arg2];
			    try
				{
					Cursor c1 = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' " +
							"and fld_module='"+db.encode(clkmod)+"'", null);
		            if(c1.getCount()>0)
		            {
		            	c1.moveToFirst();
		            	gettemplatename = db.decode(c1.getString(c1.getColumnIndex("fld_templatename")));
		            }
				}
				catch (Exception e) {
					// TODO: handle exception
				}  
			    
			    if(gettemplatename.equals("--Select--") || gettemplatename.equals(""))
			    {
			    	gettemplatename="N/A";
			    }
			    if(clkmod.equals("HVAC"))
				{
			    	intent = new Intent(LoadModules.this, NewHVAC1.class);
		    	    Bundle b2 = new Bundle();
					b2.putString("templatename", gettemplatename);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", "start");			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", clkmod);			
					intent.putExtras(b4);
					Bundle b5 = new Bundle();
					b5.putString("tempoption", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("selectedid", getselectedho);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
				}
			    else if(clkmod.equals("Bathroom"))
				{
			    	
			    	intent = new Intent(LoadModules.this, LoadBathroom.class);
			    	Bundle b2 = new Bundle();
					b2.putString("templatename", gettemplatename);			
					intent.putExtras(b2);
					Bundle b3 = new Bundle();
					b3.putString("currentview", "start");			
					intent.putExtras(b3);
					Bundle b4 = new Bundle();
					b4.putString("modulename", clkmod);			
					intent.putExtras(b4);					
					Bundle b5 = new Bundle();
					b5.putString("tempoption", "");			
					intent.putExtras(b5);
					Bundle b6 = new Bundle();
					b6.putString("selectedid", getselectedho);			
					intent.putExtras(b6);
					startActivity(intent);
					finish();
				}
			    else{
			    	
			    	intent = new Intent(LoadModules.this, LoadSubModules.class);
			    	Bundle b2 = new Bundle();
					b2.putString("templatename", gettemplatename);			
					intent.putExtras(b2);
					Bundle b1 = new Bundle();
					b1.putString("modulename", clkmod);			
					intent.putExtras(b1);
					Bundle b3 = new Bundle();
					b3.putString("selectedid", getselectedho);			
					intent.putExtras(b3);
					startActivity(intent);
					finish();
			    }
				
			
			}

		});	
		Button btn_preview = (Button)findViewById(R.id.preview);
		btn_preview.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(LoadModules.this, ReportPreview.class);
				i.putExtra("srid",getselectedho);
				i.putExtra("inspectorid",db.UserId);
				startActivity(i);
				finish();
			}
		});

		Button btn_submitall = (Button)findViewById(R.id.submitallsections);
		btn_submitall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//showecportalert();
				Intent n = new Intent(LoadModules.this, ExportAlert.class);
				n.putExtra("Srid", getselectedho);
				n.putExtra("insp", insp_boo);
				n.putExtra("errorinsp", insp_sta);
				n.putExtra("result", "true");
				n.putExtra("error", Error_tracker);
				startActivityForResult(n, 825);
				
			}
		});
		
	}
	private void showecportalert() {
		// TODO Auto-generated method stub		
		showexportelert=1;exportdialog = new Dialog(LoadModules.this);
		exportdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		exportdialog.setCancelable(false);
		exportdialog.getWindow().setContentView(R.layout.export_alert);
		
		for(int i=0;i<ins_sta_im.length;i++)
		{
			ins_sta_im[i]=(ImageView) exportdialog.findViewById(imagetickarr[i]);		
		}
		 
		for(int i=0;i<ins_chk.length;i++)
		{
			ins_chk[i]=(CheckBox) exportdialog.findViewById(modulearr[i]);System.out.println("ins_chk modue clo");
			ins_chk[i].setOnClickListener(new module_click());
		}
		
		
		par_log =(LinearLayout) exportdialog.findViewById(R.id.ex_lay_error_log);
		chaild_log =(LinearLayout) exportdialog.findViewById(R.id.list_error);
		
		
		
		((Button) exportdialog.findViewById(R.id.ex_chk_selectall)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(((CheckBox)v).isChecked())
				{
					for(int m=0;m<ins_chk.length;m++)
					{
						ins_chk[m].setChecked(true);
					}	
				}
				else
				{
					for(int m=0;m<ins_chk.length;m++)
					{
						ins_chk[m].setChecked(false);
					}	
				}
			}
		});
		
		
		((Button) exportdialog.findViewById(R.id.clear)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				for(int i=0;i<ins_chk.length;i++)
				{
					ins_chk[i].setChecked(false);
				}
				
				ins_chk[0].setChecked(false);
			
			}
		});
		
		((Button) exportdialog.findViewById(R.id.export)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showexportelert=2;
				showprogdialog=1;
				exportdialog.cancel();
				for(int i =0;i<ins_chk.length;i++)
				{
					
					if(ins_chk[i].isChecked())
					{
						for(int m=0;m<ins_chk.length;m++)
						{
									if(ins_chk[m].isChecked())
									{
										insp_boo[m]="true";
									}
									else
									{
										insp_boo[m]="";
									}
						};
					}
				}
				cf.DisplayToast("Please select altleast one module to export");
			//fn_export();
				  if(cf.isInternetOn())
				  {
					   new Start_xporting().execute("");
				  }
				  else
				  {
					  cf.DisplayToast("Please enable your internet connection.");
					  
				  } 
			}
		});
			
		
		exportdialog.show();
	}
	class  module_click implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			System.out.println("inside modue clo");
			for(int i=0;i<ins_chk.length;i++)
			{
				if(ins_chk[i].isChecked())
				{
					ins_chk[i].setChecked(true);
				}
			}
		}
	}
	
	
	
	
	class Start_xporting extends AsyncTask <String, String, String> {		
		   @Override
		    public void onPreExecute() {
			   
		        super.onPreExecute();		       
		        p_sv.progress_live=true;		        
		        p_sv.progress_title="Inspection Submission";
			    p_sv.progress_Msg="Please be patient as we export your file.<br> Please don't lock your screen it may affect your export.<br> "
						+ Exportbartext;
			    
			    add_dialog = new Dialog(LoadModules.this,android.R.style.Theme_Translucent);
				add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				add_dialog.setCancelable(false);
				add_dialog.getWindow().setContentView(R.layout.progressbar);add_dialog.show();
			    
			    ((TextView)add_dialog.findViewById(R.id.txthead)).setText(p_sv.progress_title);
				tv_head = ((TextView)add_dialog.findViewById(R.id.header_txt));
				mProgressBar= ((ProgressBar)add_dialog.findViewById(R.id.progressBar));
				tv_head.setText(Html
						.fromHtml(p_sv.progress_Msg));				
				
				tv_head_per = (TextView) add_dialog.findViewById(R.id.head_percent);				
				tv_head_per.setText("0%");
					       
		    }		
		@Override
		    protected String doInBackground(String... aurl) {		
			try {
				exportalert=1;
				
				for (int i = 0; i < insp_boo.length; i++) 
				{
					if (insp_boo[i].equals("true")) {
						j++;
					}
				}
				
				for (int i = 0; i < main_boo.length; i++) 
				{
					if (main_boo[i].equals("true")) {
						j++;
					}
				}	
				sub_total=5;

				Start_xporting.this.publishProgress();
				
				for (int i = 0; i < insp_boo.length; i++) 
				{
					if (insp_boo[i].equals("true")) 
					{
						
						int cc  = 95/j;
						if(cf.strarr_module[i].equals("HVAC") || cf.strarr_module[i].equals("Bathroom"))
						{
							cf.HVACexport(getselectedho,"start",cf.strarr_module[i],gettemplatename);
						}
						else
						{
							Exportbartext = "Exporting "+cf.strarr_module[i]+" Information";
							Submit_Datapoints(cf.strarr_module[i],i);	
						}						
						Save_Editvc(cf.strarr_module[i]);
						Submit_VisibleConditions(cf.strarr_module[i]);
				 		Submit_Photos(cf.strarr_module[i]);
						
				 		sub_total += cc;
				 						 		
						Start_xporting.this.publishProgress();						
					}
					else
					{
						insp_sta[i] = "true";
					}
					
				}Start_xporting.this.publishProgress();	
				
				if (main_boo[0].equals("true")) {
					Start_xporting.this.publishProgress();	
					Submit_Cse();
					
				} else {
					main_sta[0] = "true";
				}
				
				if (main_boo[1].equals("true")) {
					Start_xporting.this.publishProgress();	
					Exportbartext = "Exporting Pre Inspection Agreement";
					Submit_PIA();
					
				} else {
					main_sta[1] = "true";
				}
				
				
				if (main_boo[2].equals("true")) {
					
					Save_Invoice();
					Start_xporting.this.publishProgress();	
				} else {
					main_sta[2] = "true";
				}
				
				if (main_boo[3].equals("true")) {
					Exportbartext = "Exporting Photos";
					Send_CoverPhoto();
					Start_xporting.this.publishProgress();	
				} else {
					main_sta[3] = "true";
				}
				
				
				if (main_boo[4].equals("true")) {
					
					Send_FeedbackDocument();					
					Start_xporting.this.publishProgress();
					
				} else {
					main_sta[4] = "true";
				}
				Submit_Policyholderinfo();
				Submit_Homewarranty();
				sub_total=100;	
				
				show_handler=1;
				
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			show_handler=2;
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			show_handler=2;
		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			show_handler=2;
		}
      return null;
		}
		 @Override
			protected void onProgressUpdate(String... progress) 
		 	{
			 tv_head.setText(Html
						.fromHtml("Please be patient as we export your file.<br> Please don't lock your screen it may affect your export.<br> "
								+ Exportbartext));
			 mProgressBar.setProgress((int) sub_total);
			 tv_head_per.setText((int) sub_total + "%");
		    }
		   
		
		    protected void onPostExecute(String unused) {
		    	
		    	/*if (p_sv != null) {
					  p_sv.progress_live=false;
				 }*/
		    	add_dialog.cancel();
		    	exportalert=2;
		    	
		    	
		    	if(show_handler==1)
		    	{
		    		success_alert();            	  
		    	}
		    	else if(show_handler==2)
		    	{
		    		failure_alert();		    	
		    	}		    	
		    }		    
	}
private void Save_Editvc(String modulename) throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
	
		db.CreateTable(11);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_module='"+modulename+"' and fld_srid='"+getselectedho+"' and fld_selected='1'", null);
		
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Save_Editvc");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("sed_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
	      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		    request.addProperty("fld_module",db.decode(c1.getString(c1.getColumnIndex("fld_module"))));	      		    
	      		    request.addProperty("fld_submodule",db.decode(c1.getString(c1.getColumnIndex("fld_submodule"))));
	      		    request.addProperty("fld_VCName",db.decode(c1.getString(c1.getColumnIndex("fld_VCName"))));
	      		    request.addProperty("fld_selected",db.decode(c1.getString(c1.getColumnIndex("fld_selected"))));
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Save_Editvc request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Save_Editvc", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Save_Editvc "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	     try
	 	          				 {	     
	                    	    	 System.out.println("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"'" +
	                    	    	 		" WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+modulename+"' and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"'" +
	                    	    	 				" and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"' and fld_srid='"+getselectedho+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+modulename+"' and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"' and fld_srid='"+getselectedho+"'");
	 	          					System.out.println("REPSID="+response.getProperty("sed_Id").toString());
	 	          					
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
     	  
	}
	private void Submit_Policyholderinfo() throws IOException, XmlPullParserException {
	
	// TODO Auto-generated method stub
	Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.OrderInspection + " WHERE fld_inspectorid='"+db.UserId+"' and fld_orderedid='"+getselectedho+"'", null);
	System.out.println("DDD="+c1.getCount());
		  if(c1.getCount()>0)
	          {
			  c1.moveToFirst();
			  
			  
			  SoapObject request = new SoapObject(wb.NAMESPACE,"OrderInspection");
		        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
		 		envelope.dotNet = true;
		 		
		 		request.addProperty("OrderId",c1.getString(c1.getColumnIndex("fld_orderedid")));
		 		request.addProperty("fld_inspectioncompany",db.decode(c1.getString(c1.getColumnIndex("fld_inspectioncompany"))));
		 		request.addProperty("fld_policynumber",db.decode(c1.getString(c1.getColumnIndex("fld_policynumber"))));
		 		request.addProperty("fld_squarefootage",c1.getString(c1.getColumnIndex("fld_squarefootage")));
		 		request.addProperty("fld_noofstories",c1.getString(c1.getColumnIndex("fld_noofstories")));
		 		request.addProperty("fld_inspectiondate",db.decode(c1.getString(c1.getColumnIndex("fld_inspectiondate"))));
		 		request.addProperty("fld_inspectiontime",db.decode(c1.getString(c1.getColumnIndex("fld_inspectiontime"))));
		 		request.addProperty("fld_yearofconstruction",db.decode(c1.getString(c1.getColumnIndex("fld_yearofconstruction"))));
		 		request.addProperty("fld_otheryearofconstruction",db.decode(c1.getString(c1.getColumnIndex("fld_otheryearofconstruction"))));
		 		request.addProperty("fld_inspectionfee",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionfee"))));
		 		request.addProperty("fld_firstname",db.decode(c1.getString(c1.getColumnIndex("fld_firstname"))));
		 		request.addProperty("fld_lastname",db.decode(c1.getString(c1.getColumnIndex("fld_lastname"))));
		 		request.addProperty("fld_email",db.decode(c1.getString(c1.getColumnIndex("fld_email"))));
		 		request.addProperty("fld_phone",db.decode(c1.getString(c1.getColumnIndex("fld_phone"))));
		 		request.addProperty("fld_inspectionaddress1",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionaddress1"))));
		 		request.addProperty("fld_inspectionaddress2",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionaddress2"))));
		 		request.addProperty("fld_inspectioncity",db.decode(c1.getString(c1.getColumnIndex("fld_inspectioncity"))));
		 		request.addProperty("fld_inspectionstate",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionstate"))));
		 		request.addProperty("fld_inspectioncounty",db.decode(c1.getString(c1.getColumnIndex("fld_inspectioncounty"))));
		 		request.addProperty("fld_zip",db.decode(c1.getString(c1.getColumnIndex("fld_zip"))));
		 		request.addProperty("fld_flag",db.decode(c1.getString(c1.getColumnIndex("fld_flag"))));
		 		request.addProperty("fld_mailingaddress1",db.decode(c1.getString(c1.getColumnIndex("fld_mailingaddress1"))));
		 		request.addProperty("fld_mailingaddress2",db.decode(c1.getString(c1.getColumnIndex("fld_mailingaddress2"))));
		 		request.addProperty("fld_mailingcity",db.decode(c1.getString(c1.getColumnIndex("fld_mailingcity"))));
		 		request.addProperty("fld_mailingstate",db.decode(c1.getString(c1.getColumnIndex("fld_mailingstate"))));
		 		request.addProperty("fld_mailingcounty",db.decode(c1.getString(c1.getColumnIndex("fld_mailingcounty"))));
		 		request.addProperty("fld_mailingzip",db.decode(c1.getString(c1.getColumnIndex("fld_mailingzip"))));
		 		request.addProperty("fld_inspectorid",db.UserId);
		 		request.addProperty("fld_status",1); //For place order 1 = new record placing
		 		
		 		envelope.setOutputSoapObject(request);
		 		System.out.println("request="+request);
		 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
		 		SoapObject response = null;
		        try
		        {
		              androidHttpTransport.call(wb.NAMESPACE+"OrderInspection", envelope);
		              response = (SoapObject)envelope.getResponse();
		              System.out.println("OrderInspection "+response);
		              
		              //statuscode ==0 -> sucess && statuscode !=0 -> failure
		              if(response.getProperty("StatusCode").toString().equals("0"))
		      		  {
		      		  
		          		
		              	 handler_msg=0;
		      		  }
		      		  else
		      		  {
		      			 error_msg = response.getProperty("StatusMessage").toString();
		      			 handler_msg=1;
		      			
		      		  }
		        }
		        catch(Exception e)
		        {
		             e.printStackTrace();
		        }
		        
			  
	          }
		  else
		  {
			  handler_msg=0;
		  }
 	  
}
	private void Submit_Homewarranty() throws IOException, XmlPullParserException 
	{
		db.CreateTable(31);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.HomeWarranty + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
			  if(c1.getCount()>0)
		          {
				  c1.moveToFirst();
				  SoapObject request = new SoapObject(wb.NAMESPACE,"Home_Warranty");
			        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
			 		envelope.dotNet = true;
			 		
			 		request.addProperty("h_id",c1.getString(c1.getColumnIndex("fld_sendtoserver")));
			 		request.addProperty("fld_inspectorid",db.UserId);
			 		request.addProperty("fld_srid",getselectedho);
			 		request.addProperty("fld_homewarrantoption",c1.getString(c1.getColumnIndex("fld_homewarrantoption")));
			 		
			 		envelope.setOutputSoapObject(request);
			 		System.out.println("request="+request);
			 		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
			 		SoapObject response = null;
			        try
			        {
			              androidHttpTransport.call(wb.NAMESPACE+"Home_Warranty", envelope);
			              response = (SoapObject)envelope.getResponse();
			              System.out.println("Home_Warranty "+response);
			              if(response.getProperty("StatusCode").toString().equals("0"))
			      		  {
			            	  try
	 	          				 {	     
	                    	    	 System.out.println("UPDATE "+db.HomeWarranty+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.HomeWarranty+ " SET fld_sendtoserver='"+response.getProperty("h_id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;
			      		  }
			      		  else
			      		  {
			      			 error_msg = response.getProperty("StatusMessage").toString();
			      			 handler_msg=1;
			      			
			      		  }
			        }
			        catch(Exception e)
			        {
			             e.printStackTrace();
			        }
			        
				  
		          }
			  else
			  {
				  handler_msg=0;
			  }
	 	  
	}
	public void Send_CoverPhoto() {
		// TODO Auto-generated method stub
		double cc = 100 - sub_total;
		db.CreateTable(34);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.CoverPhoto + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getselectedho+"'", null);
		System.out.println("Send_CoverPhoto"+"Cout="+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Save_ImageforHomeInspection");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("El_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("Elevation_ID",1);
	      		    
	      		    request.addProperty("InspectorID",db.UserId);
	      		  	request.addProperty("SRID",getselectedho);      		    
	      		  
	      		    request.addProperty("Image_Description",db.decode(c1.getString(c1.getColumnIndex("fld_caption"))));
	      			System.out.println("testtt="+request);
	      		    String imagename = db.decode(c1.getString(c1.getColumnIndex("fld_image")));
	      		    System.out.println("imagename="+imagename);
	      		    if(!imagename.equals(""))
	      	 		{
	      	 			Bitmap bm=cf.ShrinkBitmap(imagename, 400, 400);
	      	 			if(bm!=null)							
	      				{
	      					ByteArrayOutputStream out = new ByteArrayOutputStream();
	      					bm.compress(CompressFormat.PNG, 100, out);
	      					byte[] raw = out.toByteArray();
	      					request.addProperty("Image_Name", raw);
	      				}
	      	 			else
	      	 			{
	      	 				request.addProperty("Image_Name","");
	      	 			}
	      	 		}
	      	 		else
	      	 		{
	      				request.addProperty("Image_Name","");
	      			}
	      		    
	      		    envelope1.setOutputSoapObject(request);
	      		    MarshalBase64 marshal = new MarshalBase64();
	      		    marshal.register(envelope1);
	         		System.out.println("Save_ImageforHomeInspection request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Save_ImageforHomeInspection", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Save_ImageforHomeInspection "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	     try
	 	          				 {	     
	                    	    	 System.out.println("UPDATE "+db.CoverPhoto+ " SET fld_sendtoserver='"+response.getProperty("El_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.CoverPhoto+ " SET fld_sendtoserver='"+response.getProperty("El_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sads ocver photof"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
			  double sub  = Math.round(cc/5);System.out.println("CC="+sub);
			  sub_total=sub+sub_total;
	}
	private void Submit_PIA() throws IOException, XmlPullParserException 
	{
		
		System.out.println("subtotal="+sub_total);
		double cc = 100 - sub_total;System.out.println("ccpia"+cc);
		db.CreateTable(37);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.PIA + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getselectedho+"'", null);
		System.out.println("Submit_PIA"+"Cout="+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					 SoapObject request = new SoapObject(wb.NAMESPACE,"PreinspectionAgreement");
		                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
		      		    envelope.dotNet = true;
		      		    
		      		    request.addProperty("pia_Id",0);
		      		    request.addProperty("fld_inspectorid",db.UserId);
		      		    request.addProperty("fld_srid",getselectedho);
		      		   
		      		    String imagename = db.decode(c1.getString(c1.getColumnIndex("fld_signature")));
		      		    if(!imagename.equals(""))
		      		    {
		      		    	System.out.println("imagename="+imagename);
		      	 			Bitmap bm=cf.ShrinkBitmap(imagename, 400, 400);System.out.println("bm="+bm);
		      	 			if(bm!=null)							
		      				{
		      					ByteArrayOutputStream out = new ByteArrayOutputStream();
		      					bm.compress(CompressFormat.PNG, 100, out);
		      					byte[] raw = out.toByteArray();System.out.println("RAW="+raw);
		      					
		      					request.addProperty("fld_signature", raw);
		      				}
		      	 			else
		      	 			{
		      	 				request.addProperty("fld_signature","");
		      	 			}
		      	 		}
		      	 		else
		      	 		{
		      				request.addProperty("fld_signature","");
		      			}
	      		    System.out.println("REQUE="+request);
	      		    
			      		    envelope.setOutputSoapObject(request);
			      		    MarshalBase64 marshal = new MarshalBase64();
			      		    marshal.register(envelope);
			         		System.out.println("PreinspectionAgreement request="+request);
			         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
			         		SoapObject response =null;
			         		try
			                {
			         			 androidHttpTransport1.call(wb.NAMESPACE+"PreinspectionAgreement", envelope);
								
			                      response = (SoapObject)envelope.getResponse();
			                      System.out.println("PreinspectionAgreement "+response);
			                      handler_msg = 0;	  
			                      if(response.getProperty("StatusCode").toString().equals("0"))
			              		  {
			                    	     handler_msg = 0;	  
			              		  }
			              		  else
			              		  {
			              			 error_msg = response.getProperty("StatusMessage").toString();
			              			 handler_msg=1;
			              			
			              		  }
			                }
			                
			                catch(Exception e)
			                {
			                	System.out.println("sads ocver photof"+e.getMessage());
			                     e.printStackTrace();
			                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
			  double sub  = Math.round(cc/5);System.out.println("CCpia="+sub);
			  sub_total=sub+sub_total;System.out.println("subpia="+sub_total);
   }
	
	private void Submit_Cse() throws IOException, XmlPullParserException 
	{
		Exportbartext = "Exporting Customer Service Evaluation";
	double cc = 100 - sub_total;System.out.println("CCsubmitcse"+cc);
	
		db.CreateTable(36);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.CSE + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getselectedho+"'", null);
		System.out.println("select * from " + db.CSE + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getselectedho+"'"+c1.getCount());
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"CustomerEvaluationSurvey");
					
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("cse_Id",0);
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
	      		    request.addProperty("fld_inspectorname",db.decode(c1.getString(c1.getColumnIndex("fld_inspectorname"))));
	      		    request.addProperty("fld_inspectiondate",db.decode(c1.getString(c1.getColumnIndex("fld_inspectiondate"))));	      		    
	      		    request.addProperty("fld_reportno",db.decode(c1.getString(c1.getColumnIndex("fld_reportno"))));
	      		    System.out.println("PRSERNT="+db.decode(c1.getString(c1.getColumnIndex("fld_present"))));
	      		    if(db.decode(c1.getString(c1.getColumnIndex("fld_present"))).contains("&#94;"))
	      		    {
	      		    	request.addProperty("fld_present",db.decode(c1.getString(c1.getColumnIndex("fld_present"))).replace("&#94;", "^"));
	      		    	request.addProperty("fld_present",db.decode(c1.getString(c1.getColumnIndex("fld_present"))).replace("&#40;", "~"));
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_present",db.decode(c1.getString(c1.getColumnIndex("fld_present"))));	      		    	
	      		    	request.addProperty("fld_present",db.decode(c1.getString(c1.getColumnIndex("fld_present"))).replace("&#40;", "~"));
	      		    }
	      		    
	      		    request.addProperty("fld_otherpresent",db.decode(c1.getString(c1.getColumnIndex("fld_otherpresent"))));
	      		    request.addProperty("fld_arrive",db.decode(c1.getString(c1.getColumnIndex("fld_arrive"))));
	      		    request.addProperty("fld_prior",db.decode(c1.getString(c1.getColumnIndex("fld_prior"))));
	      		    request.addProperty("fld_badge",db.decode(c1.getString(c1.getColumnIndex("fld_badge"))));
	      		    request.addProperty("fld_shoes",db.decode(c1.getString(c1.getColumnIndex("fld_shoes"))));
	      		    request.addProperty("fld_listen",db.decode(c1.getString(c1.getColumnIndex("fld_listen"))));
	      		    request.addProperty("fld_preinspection",db.decode(c1.getString(c1.getColumnIndex("fld_preinspection"))));
	      		    request.addProperty("fld_difference",db.decode(c1.getString(c1.getColumnIndex("fld_difference"))));
	      		    request.addProperty("fld_final",db.decode(c1.getString(c1.getColumnIndex("fld_final"))));
	      		    request.addProperty("fld_confident",db.decode(c1.getString(c1.getColumnIndex("fld_confident"))));
	      		    request.addProperty("fld_rate",db.decode(c1.getString(c1.getColumnIndex("fld_rate"))));
	      		    request.addProperty("fld_comments",db.decode(c1.getString(c1.getColumnIndex("fld_comments"))));
	      		    
	      		  String imagename = db.decode(c1.getString(c1.getColumnIndex("fld_signature")));
	      		    if(!imagename.equals(""))
	      		    {
	      	 			Bitmap bm=cf.ShrinkBitmap(imagename, 400, 400);
	      	 			if(bm!=null)							
	      				{
	      					ByteArrayOutputStream out = new ByteArrayOutputStream();
	      					bm.compress(CompressFormat.PNG, 100, out);
	      					byte[] raw = out.toByteArray();
	      					request.addProperty("fld_signature", raw);
	      				}
	      	 			else
	      	 			{
	      	 				request.addProperty("fld_signature","");
	      	 			}
	      	 		}
	      	 		else
	      	 		{
	      				request.addProperty("fld_signature","");
	      			}
    		    
		      		    envelope1.setOutputSoapObject(request);
		      		    MarshalBase64 marshal = new MarshalBase64();
		      		    marshal.register(envelope1);
	      		    
	      		    request.addProperty("fld_date",db.decode(c1.getString(c1.getColumnIndex("fld_date"))));
	      		    
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("CustomerEvaluationSurvey request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"CustomerEvaluationSurvey", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("CustomerEvaluationSurvey "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	   
	                    	    	/* try
		 	          				 {
	                    	    		db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_sendtoserver='"+response.getProperty("ssv_Id").toString()+"' " +
		 	          							"WHERE fld_inspectorid='"+db.UserId+"' and fld_templatename='"+gettempname+"' and fld_module='"+getmodulename+"' " +
		 	          									"and fld_submodule='"+c1.getString(c1.getColumnIndex("fld_submodule"))+"' and " +
		 	          											"fld_primary='"+c1.getString(c1.getColumnIndex("fld_primary"))+"' and fld_srid='"+getph+"'");
		 	          				 }
		 	          				 catch (Exception e) {
		 	          					// TODO: handle exception
		 	          					 System.out.println("sadsf catch="+e.getMessage());
		 	          				 }*/
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }System.out.println("handlercse="+handler_msg);
			  double sub  = Math.round(cc/5);System.out.println("CCcse="+sub);
			  sub_total=sub+sub_total;
			  
			  System.out.println("subcse="+sub_total);
   }
	
	private void Submit_Template() {
		
		db.userid();
		int j=0;
		try {
			
			
			for (int i = 0; i < insp_boo.length; i++) 
			{
				
				if (insp_boo[i].equals("true")) 
				{
					int cc = 100 / j;
					Submit_Datapoints(cf.strarr_module[i],i);
					Submit_VisibleConditions(cf.strarr_module[i]);
			 		Submit_Photos(cf.strarr_module[i]);
			 		sub_total += cc;
				}

			}
			show_handler=1;
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			////call_erro_service("Socket Exception :" + e.getMessage().toString());

			show_handler = 2;
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			////call_erro_service("NetworkErrorException :"+ e.getMessage().toString());
			show_handler = 2;
			e.printStackTrace();
		} 

		handler.sendEmptyMessage(0);
       
   }
	private void Submit_VisibleConditions(String modulename) throws IOException, XmlPullParserException {
		
		db.userid();
		cf.GetCurrentModuleName(modulename);
		for(int j=0;j<cf.arrsub.length;j++)
		{
		  try
 		  {
 			Cursor c1 = db.hi_db.rawQuery("select * from " + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+modulename+"' and fld_submodule='"+cf.arrsub[j]+"' and fld_srid='"+getselectedho+"'", null);
	        System.out.println("testselect" + db.SaveSetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+modulename+"' and fld_submodule='"+cf.arrsub[j]+"' and fld_srid='"+getselectedho+"'"+c1.getCount());
 			
	        if(c1.getCount()>0)
	        {
	        	 c1.moveToFirst();	        	
	        	 for(int i=0;i<c1.getCount();i++,c1.moveToNext())
	        	 {
	        	 
	        		SoapObject  request = new SoapObject(wb.NAMESPACE,"Save_Setvc");
	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope.dotNet = true;
	      		    
	      		  request.addProperty("ssv_Id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));    		    
	      		  
	      		    request.addProperty("fld_templatename",db.decode(c1.getString(c1.getColumnIndex("fld_templatename"))));
	      		    request.addProperty("fld_module",db.decode(c1.getString(c1.getColumnIndex("fld_module"))));	      		    
	      		    request.addProperty("fld_submodule",db.decode(c1.getString(c1.getColumnIndex("fld_submodule"))));
	      		    request.addProperty("fld_primary",db.decode(c1.getString(c1.getColumnIndex("fld_primary"))));
	      		    
	      			String str = Html.fromHtml(db.decode(c1.getString(c1.getColumnIndex("fld_secondary")))).toString();
	         		if(str.endsWith(","))
	         		{
	         			str = str.substring(0,str.length()-1);
	         		}
	         		request.addProperty("fld_secondary",str);
	      		    
	      		    request.addProperty("fld_comments",db.decode(c1.getString(c1.getColumnIndex("fld_comments"))));
	      		    request.addProperty("fld_condition",db.decode(c1.getString(c1.getColumnIndex("fld_condition"))));
	      		    request.addProperty("fld_other",db.decode(c1.getString(c1.getColumnIndex("fld_other"))));
	      		    request.addProperty("fld_checkbox",db.decode(c1.getString(c1.getColumnIndex("fld_checkbox"))));
	      		    request.addProperty("fld_inspectionpoint",db.decode(c1.getString(c1.getColumnIndex("fld_inspectionpoint"))));
	      		    
	      		  request.addProperty("fld_apprrepaircost",db.decode(c1.getString(c1.getColumnIndex("fld_apprrepaircost"))));
	      		  request.addProperty("fld_contractortype",db.decode(c1.getString(c1.getColumnIndex("fld_contractortype"))));
	      		  request.addProperty("fld_photooption",db.decode(c1.getString(c1.getColumnIndex("fld_printphoto"))));
	      		    
	      		  
	      		  Cursor 	c2 = db.hi_db.rawQuery("select * from " + db.SaveEditVC + " WHERE fld_inspectorid='"+db.UserId+"' " +
	      				" and fld_module='"+modulename+"' and fld_srid='"+getselectedho+"' " +
	      						"and fld_VCName='"+c1.getString(c1.getColumnIndex("fld_vcname"))+"'", null);
	      		  if(c2.getCount()>0)
	      		  {
	      			  c2.moveToFirst();
	      			  request.addProperty("fld_ed_id",c2.getString(c2.getColumnIndex("fld_sendtoserver")));
	      		  }
	      		  else
	      		  {
	      			request.addProperty("fld_ed_id",c1.getString(c1.getColumnIndex("fld_ed_id")));  
	      		  }
	      		    
	      		    envelope.setOutputSoapObject(request);
	         		System.out.println("Save_Setvc request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	                try
	                {
	                      androidHttpTransport1.call(wb.NAMESPACE+"Save_Setvc", envelope);
	                      response = (SoapObject)envelope.getResponse();
	                      System.out.println("Save_Setvc "+response);
	                      
	                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	 try
	          				 {
	                    		// System.out.println("UPDATE "+db.SaveEditVC+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+db.encode(modulename)+"' and fld_submodule='"+db.encode(cf.arrsub[j])+"' and fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"'");
	          					db.hi_db.execSQL("UPDATE "+db.SaveSetVC+ " SET fld_sendtoserver='"+response.getProperty("ssv_Id").toString()+"' " +
	          							"WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+db.encode(modulename)+"' and " +
	          									"fld_submodule='"+db.encode(cf.arrsub[j])+"' and " +
	          											"fld_VCName='"+db.encode(response.getProperty("fld_VCName").toString())+"'");
	          				 }
	          				 catch (Exception e) {
	          					// TODO: handle exception
	          				 }
	                    	handler_msg = 0;
	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                catch(Exception e)
	                {
	                     e.printStackTrace();
	                }
	        	 }
	        }
    	}
 		catch (Exception e) {
			// TODO: handle exception
		}
		}
   }
	private void Submit_Photos(String modulename) throws IOException, XmlPullParserException {			
		db.userid();		
		 cf.GetCurrentModuleName(modulename);
		   Bitmap bitmap=null;
	 		
	 		for(int j=0;j<cf.arrsub.length;j++)
			{
		  try
 		  {
 			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveVCImage + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_module='"+modulename+"' and fld_submodule='"+cf.arrsub[j]+"'", null);
	      // System.out.println("select * from " + db.SaveVCImage + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_module='"+modulename+"' and fld_submodule='"+cf.arrsub[j]+"'");
 			/*System.out.println("coun imahe="+cur.getCount());*/
	        if(cur.getCount()>0)
	        {
	        	 cur.moveToFirst();	        	
	        	 for(int i=0;i<cur.getCount();i++,cur.moveToNext())
	        	 {
	        	 
	        	       		
	         		
	         		
	         		String imagepath = db.decode(cur.getString(cur.getColumnIndex("fld_image")));
	         		
				
						File f = new File(imagepath);
		        		if (f.exists()) {
		        			try {
		        				
		        				SoapObject request = new SoapObject(wb.NAMESPACE,"Save_VCImage");
		    	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 
		    	      		    envelope.dotNet = true;
		    	      		    
		    	        		request.addProperty("si_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
		    	         		request.addProperty("fld_inspectorid",db.UserId);
		    	         		request.addProperty("fld_srid",db.decode(cur.getString(cur.getColumnIndex("fld_srid"))));
		    	         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
		    	         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
		    	         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
		    	         		request.addProperty("fld_caption",db.decode(cur.getString(cur.getColumnIndex("fld_caption"))));
		    	         		request.addProperty("fld_vc",db.decode(cur.getString(cur.getColumnIndex("fld_vc"))));	      
		        				
		        				
		        				if(bitmap!=null)
		    					{
		    						bitmap.recycle();
		    					}
		    					bitmap = cf.ShrinkBitmap(imagepath, 400, 400);
		    					if(bitmap!=null)							
		    					{
		    						ByteArrayOutputStream out = new ByteArrayOutputStream();
		    						bitmap.compress(CompressFormat.PNG, 100, out);
		    						byte[] raw = out.toByteArray();
		    						request.addProperty("fld_image", raw);
		    					}
		    					else
		    					{
		    						request.addProperty("fld_image","");
		    					}
		    					
		    					
		    					envelope.setOutputSoapObject(request);
		    	         		//System.out.println("Save_VCImage="+request);
		    	         		MarshalBase64 marshal = new MarshalBase64();
		    	        		marshal.register(envelope);
		    	         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
		    	         		SoapObject response = null;
		    	                try
		    	                {
		    	                      androidHttpTransport.call(wb.NAMESPACE+"Save_VCImage", envelope);
		    	                      response = (SoapObject)envelope.getResponse();
		    	                      //System.out.println("Save_VCImage response"+response);
		    	                      
		    	                      if(response.getProperty("StatusCode").toString().equals("0"))
		    	              		  {
		    	                    	  insp_sta[i] = "true";
		    	                    	 try
		    	          				 {
		    	                    		db.hi_db.execSQL("UPDATE "+db.SaveVCImage+ " SET fld_sendtoserver='"+response.getProperty("si_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+db.encode(modulename)+"' and fld_submodule='"+db.encode(cf.arrsub[j])+"' and fld_srid='"+getselectedho+"'");
		    	          				 }
		    	          				 catch (Exception e) {
		    	          					// TODO: handle exception
		    	          				 }
		    	                    	handler_msg = 0;		                      	
		    	              		  }
		    	              		  else
		    	              		  {
		    	              			 insp_sta[i] = "false";
		    	              			 error_msg = response.getProperty("StatusMessage").toString();
		    	              			 Error_tracker += error_msg + " @";
		    	              			 handler_msg=1;
		    	              			

		    	              		  }
		    	                }
		    	                catch(Exception e)
		    	                {
		    	                     e.printStackTrace();
		    	                }
		    					
							} catch (Exception e) {
								// TODO: handle exception
								
							}
						}
						else
						{
							insp_sta[i] = "false";
							//System.out.println("camr hrerr"+i+insp_sta[i]);
							Error_tracker += "Please check the following Image is available in "+ " Path="
									+ imagepath + " @";
							show_handler=2;
							
						}
					
	         		
	         		
	        	 }
	        }
    	}
 		catch (Exception e) {
			// TODO: handle exception
		}
			}
   }
	private void Submit_Datapoints(String modulename,int k) throws IOException, XmlPullParserException {
		
		db.userid();System.out.println("Submit_Datapoints name"+modulename);
		 cf.GetCurrentModuleName(modulename);
	   Exportbartext = "Exporting "+modulename +" information";
 		for(int i=0;i<cf.arrsub.length;i++)
		{
 			
		  try
 		  {
 			Cursor cur = db.hi_db.rawQuery("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and  fld_module='"+db.encode(modulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'", null);
	        //System.out.println("select * from " + db.SaveSubModule + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"' and fld_module='"+db.encode(modulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"'"+"CUR="+cur.getCount());
 			if(cur.getCount()>0)
	        {
	        	cur.moveToFirst();	        	
	        	
	        	/*for(int i=0;i<cur.getCount();i++,cur.moveToNext())
	        	 * 
	        	{*/
	        	    SoapObject request = new SoapObject(wb.NAMESPACE,"Save_SubModule");
	                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope.dotNet = true;
	      		    
	        		request.addProperty("ssm_Id",cur.getInt(cur.getColumnIndex("fld_sendtoserver")));System.out.println("SENDTOSERVER="+cur.getInt(cur.getColumnIndex("fld_sendtoserver")));
	         		request.addProperty("fld_inspectorid",db.UserId);
	         		request.addProperty("fld_srid",db.decode(cur.getString(cur.getColumnIndex("fld_srid"))));
	         		request.addProperty("fld_module",db.decode(cur.getString(cur.getColumnIndex("fld_module"))));
	         		request.addProperty("fld_submodule",db.decode(cur.getString(cur.getColumnIndex("fld_submodule"))));
	         		request.addProperty("fld_templatename",db.decode(cur.getString(cur.getColumnIndex("fld_templatename"))));
	         		request.addProperty("fld_NA",cur.getInt(cur.getColumnIndex("fld_NA")));
	         		String str = Html.fromHtml(db.decode(cur.getString(cur.getColumnIndex("fld_description")))).toString();
	         		if(str.endsWith(","))
	         		{
	         			str = str.substring(0,str.length()-1);
	         		}
	         		request.addProperty("fld_description",str);
	         		
	         		envelope.setOutputSoapObject(request);
	         		//System.out.println("request="+request);
	         		AndroidHttpTransport androidHttpTransport = new AndroidHttpTransport(wb.URL);
	         		SoapObject response = null;
	                try
	                {
	                      androidHttpTransport.call(wb.NAMESPACE+"Save_SubModule", envelope);
	                      response = (SoapObject)envelope.getResponse();
	                    System.out.println("Save_SubModule "+response);
	                      
	                      //statuscode ==0 -> sucess && statuscode !=0 -> failure
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	 insp_sta[k]="true";
	                    		  try
	 	          				 {
	                    			  System.out.println("UPDATE "+db.SaveSubModule+ " SET fld_sendtoserver='"+response.getProperty("ssm_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+db.encode(modulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getselectedho+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.SaveSubModule+ " SET fld_sendtoserver='"+response.getProperty("ssm_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and  fld_module='"+db.encode(modulename)+"' and fld_submodule='"+db.encode(cf.arrsub[i])+"' and fld_srid='"+getselectedho+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	                     	  
	                    	 
	                      	
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                catch(Exception e)
	                {
	                     e.printStackTrace();
	                }
	        	}
	        else
	        {
	        	 insp_sta[k]="true";
	        	handler_msg = 0;
	        }
	  	}
 		catch (Exception e) {
			// TODO: handle exception
		}
		 // sub_total = 100;
		}      
       
   }
	private void Save_Invoice() throws IOException, XmlPullParserException {
    	
		// TODO Auto-generated method stub
		Exportbartext = "Exporting Invoice";System.out.println("subinv="+sub_total);
		double cc = 100 - sub_total;System.out.println("saveinov="+cc);
		db.CreateTable(41);
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.Invoice + " WHERE fld_inspectorid='"+db.UserId+"' " +
				"and fld_srid='"+getselectedho+"'", null);
		
			  if(c1.getCount()>0)
   	          {
   	        	c1.moveToFirst();   	        
				 do
				 {				
					SoapObject  request = new SoapObject(wb.NAMESPACE,"Invoice");
					SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
	      		    envelope1.dotNet = true;
	      		    
	      		    request.addProperty("invoice_id",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
	      		    request.addProperty("fld_inspectorid",db.UserId);
	      		    request.addProperty("fld_srid",c1.getString(c1.getColumnIndex("fld_srid")));
	      		    
	      		    request.addProperty("fld_invoiceno",db.decode(c1.getString(c1.getColumnIndex("fld_invoiceno"))));
	      		    request.addProperty("fld_invoicedate",db.decode(c1.getString(c1.getColumnIndex("fld_invoicedate"))));
	      		    
	      		    request.addProperty("fld_occupied",db.decode(c1.getString(c1.getColumnIndex("fld_occupied"))));
	      		    request.addProperty("fld_utilitieson",db.decode(c1.getString(c1.getColumnIndex("fld_utilitieson"))));	      		    
	      		    request.addProperty("fld_additionalfees",db.decode(c1.getString(c1.getColumnIndex("fld_additionalfees"))));
	      		    request.addProperty("fld_annualinspection",db.decode(c1.getString(c1.getColumnIndex("fld_annualinspection"))));
	      		    request.addProperty("fld_shippingandhandling",db.decode(c1.getString(c1.getColumnIndex("fld_shippingandhandling"))));
	      		    
	      		    
	      		    request.addProperty("fld_eocoverage",db.decode(c1.getString(c1.getColumnIndex("fld_eocoverage"))));
	      		    request.addProperty("fld_homeguide",db.decode(c1.getString(c1.getColumnIndex("fld_homeguide"))));
	      		    
	      		    if(db.decode(c1.getString(c1.getColumnIndex("fld_payment"))).contains("&#94;"))
	      		    {
	      		    	request.addProperty("fld_payment",db.decode(c1.getString(c1.getColumnIndex("fld_payment"))).replace("&#94;", "^"));
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_payment",db.decode(c1.getString(c1.getColumnIndex("fld_payment"))));
	      		    }
	      		    
	      		    
	      		  if(db.decode(c1.getString(c1.getColumnIndex("fld_cardtype"))).contains("&#94;"))
	      		    {
	      		    	request.addProperty("fld_cardtype",db.decode(c1.getString(c1.getColumnIndex("fld_cardtype"))).replace("&#94;", "^"));
	      		    }
	      		    else
	      		    {
	      		    	request.addProperty("fld_cardtype",db.decode(c1.getString(c1.getColumnIndex("fld_cardtype"))));
	      		    }
	      		  
	      		    request.addProperty("fld_specialnotes",db.decode(c1.getString(c1.getColumnIndex("fld_specialnotes"))));
	      		    
	      		    request.addProperty("fld_standardhomeinsp",db.decode(c1.getString(c1.getColumnIndex("fld_standardhomeinsp"))));
	      		    request.addProperty("fld_crawlfees",db.decode(c1.getString(c1.getColumnIndex("fld_crawlfees"))));	      		    
	      		    request.addProperty("fld_subtotal",db.decode(c1.getString(c1.getColumnIndex("fld_subtotal"))));
	      		    request.addProperty("fld_salestax",db.decode(c1.getString(c1.getColumnIndex("fld_salestax"))));
	      		    request.addProperty("fld_totalfees",db.decode(c1.getString(c1.getColumnIndex("fld_totalfees"))));
	      		    request.addProperty("fld_amountreceived",db.decode(c1.getString(c1.getColumnIndex("fld_amountreceived"))));
	      		    request.addProperty("fld_balancedue",db.decode(c1.getString(c1.getColumnIndex("fld_balancedue"))));
	      		    
	      		    envelope1.setOutputSoapObject(request);
	         		System.out.println("Invoice request="+request);
	         		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
	         		SoapObject response =null;
	         		try
	                {
	         			 androidHttpTransport1.call(wb.NAMESPACE+"Invoice", envelope1);
						
	                      response = (SoapObject)envelope1.getResponse();
	                      System.out.println("Invoice "+response);
	                      handler_msg = 0;	  
	                      if(response.getProperty("StatusCode").toString().equals("0"))
	              		  {
	                    	     try
	 	          				 {	     
	                    	    	 System.out.println("UPDATE "+db.Invoice+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
	 	          					db.hi_db.execSQL("UPDATE "+db.Invoice+ " SET fld_sendtoserver='"+response.getProperty("sed_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
	 	          				 }
	 	          				 catch (Exception e) {
	 	          					// TODO: handle exception
	 	          				 }
	 	                    	handler_msg = 0;	  
	              		  }
	              		  else
	              		  {
	              			 error_msg = response.getProperty("StatusMessage").toString();
	              			 handler_msg=1;
	              			
	              		  }
	                }
	                
	                catch(Exception e)
	                {
	                	System.out.println("sadsf"+e.getMessage());
	                     e.printStackTrace();
	                }
	        	} while (c1.moveToNext());
   	          }
     		  else
     		  {
     			  handler_msg=0;
     		  }
			  System.out.println("handlerinv="+handler_msg);
			  double sub  = Math.round(cc/5);System.out.println("CCinv="+sub);
			  sub_total=sub+sub_total;System.out.println("ccsubcse="+sub_total);
	}
	private boolean send_feedbackinfo() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		Cursor 	c1 = db.hi_db.rawQuery("select * from " + db.FeedBackInfo + " WHERE FD_InspectorId='"+db.UserId+"' " +
				"and FD_SRID='"+getselectedho+"'", null);
	
		System.out.println("feedback "+c1.getCount());
		
		if(c1.getCount()>0)
	          {
	        	c1.moveToFirst();   	        
					
				SoapObject  request = new SoapObject(wb.NAMESPACE,"ExportFeedbackInformation");
				
				SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
    		    envelope1.dotNet = true;
    		    
    		    
    		    request.addProperty("WSID",c1.getInt(c1.getColumnIndex("fld_sendtoserver")));
    		    request.addProperty("SRID",getselectedho);
    		    	
    		    request.addProperty("IsCusServiceCompltd",c1.getString(c1.getColumnIndex("FD_CSC")));
    		    request.addProperty("IsInspectionPaperAvbl",db.decode(c1.getString(c1.getColumnIndex("FD_insupaperwork"))));
    		    request.addProperty("IsManufacturerInfo",c1.getString(c1.getColumnIndex("FD_manuinfo")));
    		    request.addProperty("Attendees",db.decode(c1.getString(c1.getColumnIndex("FD_Attendees"))));    		    
    		    request.addProperty("PresentatInspection",c1.getString(c1.getColumnIndex("FD_whowas")));
    		    request.addProperty("OtherPresent",db.decode(c1.getString(c1.getColumnIndex("FD_whowas_other"))));
    		    request.addProperty("FeedbackComments",c1.getString(c1.getColumnIndex("FD_comments")));
    		    request.addProperty("CreatedOn",db.decode(c1.getString(c1.getColumnIndex("FD_CreatedOn"))));
    		  
    		    
    		    envelope1.setOutputSoapObject(request);
       		System.out.println("ExportFeedbackInformation request="+request);
       		AndroidHttpTransport androidHttpTransport1 = new AndroidHttpTransport(wb.URL);
       		SoapObject response =null;
       		try
              {
       			 androidHttpTransport1.call(wb.NAMESPACE+"ExportFeedbackInformation", envelope1);
					
       		  response = (SoapObject)envelope1.getResponse();
              System.out.println("Save_ImageforHomeInspection "+response);
              handler_msg = 0;	  
              if(response.getProperty("StatusCode").toString().equals("0"))
      		  {
            	     try
       				 {	     
            	    	 System.out.println("UPDATE "+db.FeedBackInfo+ " SET fld_sendtoserver='"+response.getProperty("El_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
       					db.hi_db.execSQL("UPDATE "+db.FeedBackInfo+ " SET fld_sendtoserver='"+response.getProperty("El_Id").toString()+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
       				 }
       				 catch (Exception e) {
       					// TODO: handle exception
       				 }
                 	handler_msg = 0;	
                 	return true;
      		  }
      		  else
      		  {
      		
      			 error_msg = response.getProperty("StatusMessage").toString();
      			 handler_msg=1;
      			  return false;
      		  }
            
            		
              }	                
              catch(Exception e)
              {
              	System.out.println("sadsf"+e.getMessage());
                   e.printStackTrace();
              }
	          }
       		return true;		
	  }
	private void Send_FeedbackDocument() throws IOException, XmlPullParserException, NetworkErrorException 
	{ 
		// TODO Auto-generated method stub	
		  Exportbartext = "Exporting Feedback Information";
		db.CreateTable(32);
		if(send_feedbackinfo())
		{
		Cursor c =  db.hi_db.rawQuery("Select * from " + db.FeedBackDocument+ " Where FD_D_SRID='" +getselectedho+ "' and  FD_D_InspectorId ='" +db.UserId + "'",null);
		
		System.out.println("count="+c.getCount());
		c.moveToFirst();
		String result_val = "";
		Bitmap bitmap;
		String insp_online1="";
		
         SoapObject response=null;
		if (c.getCount() >= 1) {
			System.out.println("gretaer than 1");
			MarshalBase64 marshal = new MarshalBase64();
			ByteArrayOutputStream out = null;				
			c.moveToFirst();
			
			int countchk = c.getCount();
			for (int j = 0; j < c.getCount(); j++) 
			{
				Exportbartext="Uploading Feedback documents   "+(j+1)+" / "+countchk;
				String substring = db.decode(c.getString(c.getColumnIndex("FD_D_path")));
				 SoapObject request = new SoapObject(wb.NAMESPACE,"ExportFeedbackDocument");
		         SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); 		
		         envelope.dotNet = true;
				
				request.addProperty("DocumentId",  c.getString(c.getColumnIndex("fld_sendtoserver")));
				request.addProperty("SRID", getselectedho);
				request.addProperty("DocumentTitle", db.decode(c.getString(c.getColumnIndex("FD_D_doctit"))));
				
				Boolean b = (c.getString(c.getColumnIndex("FD_D_type"))
						.equals("1")) ? true : false;
				
				request.addProperty("IsOfficeUse", b);
				request.addProperty("CreatedDate", c.getString(c.getColumnIndex("FD_D_CreatedOn")));
				request.addProperty("ImageOrder", 0);
				//request.addProperty("ImageNameWithExtension", ".jpg");
			//System.out.println("reqq="+request);				

				if (substring.endsWith(".pdf")) 
				{
					File dir = Environment.getExternalStorageDirectory();
					if (substring.startsWith("file:///")) 
					{
						substring = substring.substring(11);
					}
					else
					{
						substring = substring;
					}
					File assist = new File(substring);
					if (assist.exists()) 
					{
						System.out.println("file exists "+assist);
						String mypath = substring;
						String temppath[] = mypath.split("/");
						int ss = temppath[temppath.length - 1].lastIndexOf(".");// substring(".");
						String tests = temppath[temppath.length - 1].substring(ss);
						String namedocument;
						if (tests.equals(".pdf")) 
						{
							namedocument = "pdf_document" + j + ".pdf";
							request.addProperty("ImageNameWithExtension",namedocument);
							try
							{
								InputStream fis = new FileInputStream(assist);
								long length = assist.length();
								byte[] bytes = new byte[(int) length];
								int offset = 0;
								int numRead = 0;
								while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
								{
									offset += numRead;
								}
								strBase64 = Base64.encode(bytes);										
								request.addProperty("imgByte", strBase64);										
								try
								{
								envelope.setOutputSoapObject(request);
								HttpTransportSE httpTransport = new HttpTransportSE(wb.URL);
								httpTransport.call(wb.NAMESPACE+"ExportFeedbackDocument",envelope);
								response = (SoapObject)envelope.getResponse();
								if(response.getProperty("StatusCode").toString().equals("0"))
					      		{
									//statuschange[4]=true;
								}							
								}
								catch (IOException e) {
									// TODO: handle exception											
								}
								
							}
							catch (Exception e) 
							{ 
								 
								error_msg = response.getProperty("StatusMessage").toString();
		              			 handler_msg=1;
							}
						}
						else
						{
							System.out.println("issues in extention missing");
							
						}

					}
					else
					{						
						
						//Error_traker("You have problem in uploading document in the feedback document tab Document Title"+cf.getsinglequotes(c.getString(2)) );
					}
				} 
				else
				{
					String mypath = substring;
					String temppath[] = mypath.split("/");
					int ss = temppath[temppath.length - 1].lastIndexOf(".");// substring(".");
					String tests = temppath[temppath.length - 1].substring(ss);
					System.out.println("tests feed "+tests);
					String elevationttype;
					File f = new File(substring);
					if (f.exists()) 
					{
					
						if (tests.equals("") || tests.equals("null")|| tests == null) 
						{
							
						}
						else
						{
							elevationttype = "feedbackimage" + j + tests;
							request.addProperty("ImageNameWithExtension",
									elevationttype);
							bitmap = cf.ShrinkBitmap(substring, 800, 800);
							
							out = new ByteArrayOutputStream();
							bitmap.compress(CompressFormat.PNG, 100, out);
							byte[] raw = out.toByteArray();
							//System.out.println("request"+request.toString());
							request.addProperty("imgByte", raw);
							marshal.register(envelope);
						    System.out.println("request"+request.toString());
							try
							{
							envelope.setOutputSoapObject(request);
							HttpTransportSE httpTransport = new HttpTransportSE(wb.URL);
							httpTransport.call(wb.NAMESPACE+"ExportFeedbackDocument", envelope);

							response = (SoapObject)envelope.getResponse();
							if(response.getProperty("StatusCode").toString().equals("0"))
							  {
							
				      		  }
							 else
							 {
								 error_msg = response.getProperty("StatusMessage").toString();
								 System.out.println("SFFF"+error_msg);

				      			 handler_msg=1;
							 }

							}
							catch (IOException e) {
								// TODO: handle exception
								
							}
							
						}

						
					}
					else
					{
						System.out.println("error file not exist");

					}
					
				}						
				
				c.moveToNext();
			}
			
	}
		else
		{
			handler_msg=0;
		}
		}
	}
	public boolean check_Status(String result) {
		// TODO Auto-generated method stub
		try
		{
			if(result!=null)
			{
			if(result.trim().equals("true"))
			{
				return true;
			}
			else
			{
				return false;
			}
			}
			else
			{
				return false;
			}
			
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			// pd.dismiss();
			System.out.println("show_handler="+show_handler+insp_sta[0]+" main="+main_sta[4]);
			
			if (show_handler == 1) 
			{
				if (insp_sta[0] == "true" && insp_sta[1] == "true" && insp_sta[2] == "true" && insp_sta[3] == "true" && insp_sta[4] == "true" 
						&& insp_sta[5] == "true" && insp_sta[6] == "true" && insp_sta[7] == "true" && insp_sta[8] == "true" 
						&& insp_sta[9] == "true" && insp_sta[10] == "true" && insp_sta[11] == "true" && insp_sta[12] == "true"
						&& insp_sta[13] == "true" && insp_sta[14] == "true" && insp_sta[15] == "true" && main_sta[0]=="true" && main_sta[1]=="true" 
						&& main_sta[2]=="true" && main_sta[3]=="true" && main_sta[4]=="true")
				{
					AlertDialog alertDialog = new AlertDialog.Builder(LoadModules.this).create();
					alertDialog.setMessage("Inspection Exported Successfully.");
					alertDialog.setButton("OK",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int which) {
							Intent in = new Intent(getApplicationContext(), LoadModules.class);
							in.putExtra("type", "export");
							startActivity(in);
													
						}
					});
					alertDialog.show();
				}
			
			} 
			else 
			{

				System.out.println("Error_trackerdsfdf"+Error_tracker.length());
				if (Error_tracker.length() > 2) 
				{
					Error_tracker = Error_tracker.substring(0,Error_tracker.length() - 1);
				}
				/*Intent n = new Intent(LoadModules.this, ExportAlert.class);
				n.putExtra("Srid", getselectedho);
				n.putExtra("insp", insp_boo);
				n.putExtra("errorinsp", insp_sta);
				n.putExtra("result", "true");
				n.putExtra("error", Error_tracker);
				startActivityForResult(n, 825);*/
				
			}
		}
	};
	public void failure_alert() {
		// TODO Auto-generated method stub
      
		AlertDialog al = new AlertDialog.Builder(LoadModules.this).setMessage(error_msg).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				if (Error_tracker.length() > 2) {
					Error_tracker = Error_tracker.substring(0,
							Error_tracker.length() - 1);
				}
				
				Intent n = new Intent(LoadModules.this, ExportAlert.class);
				n.putExtra("Srid", getselectedho);
				n.putExtra("insp", insp_boo);
				n.putExtra("errorinsp", insp_sta);
				n.putExtra("main", main_boo);
				n.putExtra("result", "true");
				n.putExtra("error", Error_tracker);
				startActivityForResult(n, 825);
				
			}
		}).setTitle(title+" Export Failure").create();
		al.setCancelable(false);
		al.show();
	}
	public void success_alert() {

		// TODO Auto-generated method stub
       
		AlertDialog al = new AlertDialog.Builder(LoadModules.this).setMessage("You've successfully submitted your Inspection.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				handler_msg = 2 ;
				 try
				 {
				  System.out.println("UPDATE "+db.OrderInspection+ " SET fld_status='2' WHERE fld_orderedid='"+getselectedho+"'");
				  
					db.hi_db.execSQL("UPDATE "+db.OrderInspection+ " SET fld_status='2' WHERE fld_orderedid='"+getselectedho+"'");
				 }
				 catch (Exception e) {
					// TODO: handle exception
				 }
				    Intent in = new Intent(LoadModules.this,CompletedInspection.class);
					Bundle b5 = new Bundle();
					b5.putString("inspectorid", inspectorid);			
					in.putExtras(b5);
					startActivity(in);
					finish();
					
				/*Intent in = new Intent(LoadModules.this,LoadModules.class);
		    	in.putExtra("selectedid", getselectedho);
		    	in.putExtra("inspectorid", inspectorid);
		    	startActivity(in);*/
				/*startActivity(new Intent(LoadModules.this,HomeScreen.class));
				finish();*/
				
				
				
			}
		}).setTitle(title+" Export Success").create();
		al.setCancelable(false);
		al.show();
	}
	protected void show_alert() {
		// TODO Auto-generated method stub
		k=1;
		final Dialog add_dialog = new Dialog(LoadModules.this);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.warrantyinfo);
		
		rdg = (RadioGroup)add_dialog.findViewById(R.id.homerdg);
		Cursor cur = db.hi_db.rawQuery("select * from " + db.HomeWarranty + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
		if(cur.getCount()>0)
        {
			cur.moveToFirst();	
			((RadioButton)rdg.findViewWithTag(cur.getString(cur.getColumnIndex("fld_homewarrantoption")))).setChecked(true);
        }
		else
		{
			((RadioButton)rdg.findViewWithTag("Yes")).setChecked(true);
		}
		
		TextView txt = (TextView) add_dialog.findViewById(R.id.txt);
		String str = "<b>This home is eligible for a one year Home Warranty Offer!. Items included in the one year home warranty if purchased are:\n</b>" +
				"\t Furnaces, Boilers, Heat Pumps, Central Air Conditioners, Electrical Systems, Thermostats, Water Heater System, Plumbing, Polybutylene Lines, " +
				"Sump Pumps, Whirlpools, Dishwasher, Food Waste Disposer, Cooking Range/Oven, Microwave, Kitchen Refrigerator, Trash Compactor, " +
				"Plumbing Fixtures and Faucets, Domestic Water Softner, Well Water System, Fireplace Gas Burner, Attic and Exhausts Fans, Humidifiers, Dehumidifiers, " +
				"Lighting Fixtures, Septic Lines, Ejector Pump";
		txt.setText(Html.fromHtml(str));
		
		Button btnclose = (Button)add_dialog.findViewById(R.id.close);
		btnclose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				add_dialog.dismiss();
			}
		});
		
		Button btncancel = (Button) add_dialog.findViewById(R.id.cancel);
		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				add_dialog.dismiss();
			}
		});
		
		Button btnsave= (Button) add_dialog.findViewById(R.id.submit);
		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					
					int warrantyrdiogrpid= rdg.getCheckedRadioButtonId();
					getwarrantyoption = (warrantyrdiogrpid==-1)? "":((RadioButton) add_dialog.findViewById(warrantyrdiogrpid)).getText().toString();
					
					Cursor cur = db.hi_db.rawQuery("select * from " + db.HomeWarranty + " WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'", null);
					if(cur.getCount()>0)
		            {
		            	db.hi_db.execSQL("UPDATE "
								+ db.HomeWarranty
								+ " SET fld_homewarrantoption='"+getwarrantyoption
								+"'  WHERE fld_inspectorid='"+db.UserId+"' and fld_srid='"+getselectedho+"'");
		            }
		            else
		            {
		            	db.hi_db.execSQL(" INSERT INTO "
								+ db.HomeWarranty
								+ " (fld_inspectorid,fld_srid,fld_homewarrantoption) VALUES"
								+ "('"+db.UserId+"','"+getselectedho+"','" + getwarrantyoption+ "')");
		            }
					cf.DisplayToast("Home Warranty information saved successfully");
					add_dialog.dismiss();
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("hoe warranty="+e.getMessage());
				}
			}
		});
		
		add_dialog.show();
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
		
		   public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
	    	gettemplatename = parent.getItemAtPosition(pos).toString();
	    		
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	public static class EfficientAdapter extends BaseAdapter {

		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;
		
		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
			
		}

		public int getCount() {
			int arrlen = 0;
			arrlen = arrstrmodule.length;
			

			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.startlist, null);
				holder.text = (TextView) convertView.findViewById(R.id.TextView01);
				holder.text3 = (TextView) convertView.findViewById(R.id.progressbtn);
				holder.text3.setVisibility(convertView.VISIBLE);
				holder.text2 = (ImageView) convertView.findViewById(R.id.TextView02);
				holder.text2.setVisibility(convertView.VISIBLE);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			    if (rows[position].contains("null")) {
			    	rows[position] = rows[position].replace("null", "");
					
				}
			    
			   /* if (arrna[position].contains("null")) {
			    	arrna[position] = arrna[position].replace("null", "");
					
				}*/
			
				if(rows[position].equals("0"))
				{
					holder.text3.setText("Not Started");
				}
				else if(!rows[position].equals("0") && sendtoserver[position]==0 )
				{
					holder.text3.setText("In Progress");
				}
			    /*else if(arrna[position].equals("1") && sendtoserver[position]==0 && !rows[position].equals("0"))
				{
					holder.text3.setText("Not Applicable");
				}*/
			    else if(sendtoserver[position]!=0)
				{
					holder.text3.setText("Completed/Submitted");
				}
			
				if (arrstrmodule[position].contains("null")) {
					arrstrmodule[position] = arrstrmodule[position].replace("null", "");
					holder.text.setText(Html.fromHtml(arrstrmodule[position]));
				}
				else
				{
					holder.text.setText(Html.fromHtml(arrstrmodule[position]));
				}
			
			return convertView;
		}

		static class ViewHolder {
			TextView text,text3;
			ImageView text2;
		
		}

	}
	
	private void getselectedinsp(Intent data) {
		// TODO Auto-generated method stub
		Error_tracker = "";
		insp_boo = data.getStringArrayExtra("insp");
		main_boo = data.getStringArrayExtra("main");
	
		int j = 0;
		for (int i = 0; i < insp_boo.length; i++) {

			if (insp_boo[i].equals("true")) {
				j++;
			}

		}
		
		for (int i = 0; i < main_boo.length; i++) {
			System.out.println("main_boo="+main_boo[i]);
			if (main_boo[i].equals("true")) {
				j++;
			}

		}
		
		
		if (j != 0) {
			PerIns_inc = 100 / j;
		}

		/*** Get teh number of insepction selected ends ***/
		for (int i = 0; i < insp_sta.length; i++) {
			insp_sta[i] = "";

		}
		if(cf.isInternetOn())
		  {
			   new Start_xporting().execute("");
		  }
		  else
		  {
			  cf.DisplayToast("Please enable your internet connection.");
			  
		  } 
	
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			System.out.println("inside if");
			getselectedinsp(data);
			
		} else {
			System.out.println("inside if eles");
		}

	}	
	@Override
    protected void onSaveInstanceState(Bundle outState) 
	{	 
		 outState.putInt("showexportelert",showexportelert);
		 if (p_sv != null) {
			  p_sv.progress_live=false;
		 }
		 
		 outState.putInt("exportalert1",exportalert);
		 if(exportalert==1)
		 {
			 outState.putDouble("exporttotal",sub_total);System.out.println("Mess="+tv_head.getText().toString());
			 outState.putString("exportmessage",tv_head.getText().toString());
		 }
		
        super.onSaveInstanceState(outState);
    }
	
	
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
 
    {   	
    	exportalert = savedInstanceState.getInt("exportalert1");    	
    	if(exportalert==1)
    	{
    		new Start_xporting().execute("");
    		sub_total = savedInstanceState.getDouble("exporttotal");System.out.println("ONESTsub_total="+sub_total+"Mess="+savedInstanceState.getString("exportmessage"));
    		p_sv.progress_Msg = savedInstanceState.getString("exportmessage");
    		mProgressBar.setProgress((int) sub_total);
			tv_head_per.setText((int) sub_total + "%");
			tv_head.setText(Html.fromHtml(p_sv.progress_Msg));	
    	}
    	
        super.onRestoreInstanceState(savedInstanceState);
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(LoadModules.this, StartInspection.class);
			i.putExtra("inspectorid",inspectorid);
			startActivity(i);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
