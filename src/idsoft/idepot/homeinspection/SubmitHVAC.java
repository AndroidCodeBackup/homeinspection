package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.DatabaseFunction;
import idsoft.idepot.homeinspection.support.Progress_dialogbox;
import idsoft.idepot.homeinspection.support.Progress_staticvalues;
import idsoft.idepot.homeinspection.support.Static_variables;
import idsoft.idepot.homeinspection.support.Webservice_config;

import java.io.IOException;
import java.util.Random;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class SubmitHVAC extends Activity 
{
	String word;
	EditText wrdedit;
	DatabaseFunction db;
	Webservice_config wb;
	CommonFunction cf;
	Progress_staticvalues p_sv;
	int handler_msg=2;
	private static final Random rgenerator = new Random();
	 public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        
	        
	        setContentView(R.layout.input_submit_inspection);
	    	db = new DatabaseFunction(this);
			cf = new CommonFunction(this);
			wb = new Webservice_config(this);
	        final String myString[] = getResources().getStringArray(R.array.myArray);
			
			word = myString[rgenerator.nextInt(myString.length)];
			 wrdedit = ((EditText) findViewById(R.id.wrdedt));
			 
			 wrdedit.setText(word);
			 if (word.contains(" ")) { word = word.replace(" ", ""); }
				db.userid();
			 
			
			  ((TextView) findViewById(R.id.tv1)).setText(Html
						.fromHtml("<font color=black>I, "
								+ "</font>"
								+ "<b><font color=#bddb00>"			
								 + db.Userfirstname +" "+db.Userlastname
								+ "</font></b><font color=black> have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
								+ "</font>"));
			 
				((TextView) findViewById(R.id.tv2)).setText(Html
						.fromHtml("<font color=black>I, "
								+ "</font>"
								+ "<b><font color=#bddb00>"
								+ db.Userfirstname +" "+db.Userlastname
								+ "</font></b><font color=black> also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
								+ "</font>"));
				
				
				((Button) findViewById(R.id.submit)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (((RadioButton) findViewById(R.id.accept)).isChecked() == true) {
							
							if (((EditText) findViewById(R.id.wrdedt1)).getText().toString().equals("")) 
							{
								cf.DisplayToast("Please enter Word Verification.");
								((EditText) findViewById(R.id.wrdedt1)).requestFocus();
							} 
							else
							{
								if (((EditText) findViewById(R.id.wrdedt1)).getText().toString().equals(word.trim().toString())) 
								{
									System.out.println("111");
								
									if(cf.isInternetOn())
									  {
										   new Start_xporting().execute("");
									  }
									  else
									  {
										  cf.DisplayToast("Please enable your internet connection.");
										  
									  } 
								} 
								else
								{
									cf.DisplayToast("Please enter valid Word Verification(case sensitive).");
									((EditText) findViewById(R.id.wrdedt1)).setText("");
									((EditText) findViewById(R.id.wrdedt1)).requestFocus();
								}
							}
						} 
						else 
						{
							cf.DisplayToast("Please select Accept Radio Button.");
						}

					}
				});			
				
				((Button) findViewById(R.id.cancel)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
					}
				});			
				
				((Button) findViewById(R.id.S_refresh)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						word = myString[rgenerator.nextInt(myString.length)];			
						 ((EditText) findViewById(R.id.wrdedt)).setText(word);
						if (word.contains(" ")) {
							word = word.replace(" ", "");
						}	
						
					}
				});	
	 }
	 class Start_xporting extends AsyncTask <String, String, String> {
			
		   @Override
		    public void onPreExecute() {
		        super.onPreExecute();
		       
		        p_sv.progress_live=true;
		     
		        	p_sv.progress_Msg="Please wait..Your template has been submitting.";
		        
			    
		       
				startActivityForResult(new Intent(SubmitHVAC.this,Progress_dialogbox.class), Static_variables.progress_code);
		       
		    }
		
		@Override
		    protected String doInBackground(String... aurl){		
			 	System.out.println("insde");
		        return null;
		    }
		
	 }
}
