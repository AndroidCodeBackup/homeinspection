package idsoft.idepot.homeinspection;

import idsoft.idepot.homeinspection.support.CommonFunction;
import idsoft.idepot.homeinspection.support.CustomTextWatcher;
import idsoft.idepot.homeinspection.support.DatabaseFunction;



import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class SetVC extends Activity {
	String gettempname="",getcurrentview="",getmodulename="",strrd_cond="",getinspectionpoint="",str_ip="",
			getprimaryvc="",getsecondaryvc="",getsubmodulename="",strother="",strcomments="",getph="",
			getvc="",gettempoption="";
	TextView tv_seltemplate,tv_modulename,tvcomment,sp_inspectionpoint;
	Spinner sp_primaryvc,sp_secondaryvc;
	Button btnback,btnhome,btn_save,btn_clear;
	DatabaseFunction db;
	EditText etcomments,otheret;
	CommonFunction cf;
	ArrayAdapter primaryadap,secondaryadap;
	String[] primaryarr,secondaryyarr;
	RadioGroup rgcnd;
	boolean rgcond=false;
	View v;
	int scnt;
	CheckBox[] ch;
	public int[] chkbxid = {R.id.chk1,R.id.chk2,R.id.chk3,R.id.chk4};
	public CheckBox[] cb = new CheckBox[chkbxid.length];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
		Bundle b = this.getIntent().getExtras();
		gettempname = b.getString("templatename");
		Bundle b1 = this.getIntent().getExtras();
		getmodulename = b1.getString("modulename");
		Bundle b2 = this.getIntent().getExtras();
		getsubmodulename = b2.getString("submodulename");
		Bundle b3 = this.getIntent().getExtras();
		getcurrentview = b3.getString("currentview");
		Bundle b4 = this.getIntent().getExtras();
		getph = b4.getString("selectedid");
		Bundle b6 = this.getIntent().getExtras();
		getvc = b6.getString("selectedvc");
		Bundle b7 = this.getIntent().getExtras();
		gettempoption = b7.getString("tempoption");
		
		setContentView(R.layout.activity_createvc);
		System.out.println("cammmmmme");
		
		db = new DatabaseFunction(this);
		cf = new CommonFunction(this);
		db.CreateTable(1);
		db.CreateTable(5);
		db.CreateTable(6);
		db.userid();
		
		tv_seltemplate = (TextView)findViewById(R.id.seltemplate);
		tv_seltemplate.setText(Html.fromHtml("<b>Selected Template : </b>"+gettempname));
		tv_modulename = (TextView)findViewById(R.id.modulename);
		tv_modulename.setText("Module Name : "+getmodulename);
		
		btnback = (Button)findViewById(R.id.btn_back);
		btnback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				back();
			}
		});
		
		btnhome = (Button)findViewById(R.id.btn_home);
		btnhome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(SetVC.this,HomeScreen.class));
				finish();
			}
		});
		
		 btn_save = (Button)findViewById(R.id.ok);
         btn_save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 strother = otheret.getText().toString();
				 strcomments = etcomments.getText().toString();
				 
				  if(getprimaryvc.equals("--Select--"))
				  {
						 cf.DisplayToast("Please select Primary Condition");
				  }
					 else 
					 {
						/* if(getsecondaryvc.equals("--Select--"))
						 {
							 cf.DisplayToast("Please select Secondary Condition");
						 }
						 else
						 {*/
							 if(strcomments.equals(""))
							 {
								 cf.DisplayToast("Please enter Comments");
							 }
							 else
							 {
								 if(strrd_cond.equals(""))
								 {
									 cf.DisplayToast("Please select Condition");
								 }
								 else
								 {
									 if(strrd_cond.equals("Other"))
									 {
										 if(strother.equals(""))
										 {
											 cf.DisplayToast("Please enter the Other text");
										 }
										 else
										 {
											 checkboxvalidation();
										 }
									 }
									 else
									 {
										 checkboxvalidation();
									 }
								 }
							 }
						// }
					 }
				 }
		
		});
         
         btn_clear = (Button)findViewById(R.id.clear);
         btn_clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clear();
				sp_primaryvc.setSelection(0);
				 
			}
		});
		
		sp_primaryvc = (Spinner)findViewById(R.id.spin_primaryvc);
		sp_secondaryvc = (Spinner)findViewById(R.id.spin_secondaryvc);
		
		cf.GetCurrentModuleName(getmodulename);
		primaryarr = cf.submodvcid;
		primaryadap = new ArrayAdapter<String>(SetVC.this,android.R.layout.simple_spinner_item,primaryarr);
		primaryadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	sp_primaryvc.setAdapter(primaryadap);
	 	sp_primaryvc.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(1));
	 	
	 	secondaryyarr = cf.submodvcid;
	 	secondaryadap = new ArrayAdapter<String>(SetVC.this,android.R.layout.simple_spinner_item,secondaryyarr);
	 	secondaryadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	sp_secondaryvc.setAdapter(secondaryadap);
	 	sp_secondaryvc.setOnItemSelectedListener(new MyOnItemSelectedListenerdata(2));
	 	
	 	etcomments = (EditText)findViewById(R.id.comments);
        tvcomment = (TextView)findViewById(R.id.tvcomment);
        etcomments.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
    			if(s.toString().startsWith(" "))
    			{
    				etcomments.setText("");
    			}
    			cf.ShowingLimit(s.toString(),tvcomment,1000); 
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				

			}
		});
        
        rgcnd = (RadioGroup)findViewById(R.id.rgcond);
		otheret = (EditText)findViewById(R.id.othertxt);
		otheret.addTextChangedListener(new CustomTextWatcher(otheret));
		rgcnd.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				    RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
			        boolean isChecked = checkedRadioButton.isChecked();
			        if (isChecked)
			        {
			         	strrd_cond= checkedRadioButton.getText().toString().trim();
			         	if(strrd_cond.equals("Other"))
			         	{
			         		otheret.setVisibility(v.VISIBLE);
			         		otheret.requestFocus();
			         	}
			         	else
			         	{
			         		otheret.setVisibility(v.GONE);
			         	}
			         	rgcond=false;
					
				   }
			}
		});
		
		for(int i=0;i<chkbxid.length;i++)
		{
			cb[i] = (CheckBox)findViewById(chkbxid[i]);
		}
		
		sp_inspectionpoint = (TextView)findViewById(R.id.spin_inspectionpoint);
		sp_inspectionpoint.setText("--Select--");
		sp_inspectionpoint.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showalert();
			}
		});
		
		Display();
	}
	private void Display() {
		// TODO Auto-generated method stub
		 try
		 {
			    Cursor cur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
	            scnt = cur.getCount();
	            if(scnt>0)
	            {
	            	cur.moveToFirst();
	            	
				 	String get_pcon = cur.getString(cur.getColumnIndex("fld_primary"));
				 	int pc = primaryadap.getPosition(get_pcon);
	            	sp_primaryvc.setSelection(pc);
	            	
	            	String get_scon = cur.getString(cur.getColumnIndex("fld_secondary"));
	            	System.out.println("get_scon"+get_scon);
	            	int sc = secondaryadap.getPosition(get_scon);
	            	sp_secondaryvc.setSelection(sc);
	            	System.out.println("successssec");
	            	String getcomments = db.decode(cur.getString(cur.getColumnIndex("fld_comments")));
	            	etcomments.setText(getcomments);
	            	
	            	String getcond =cur.getString(cur.getColumnIndex("fld_condition"));
	            	((RadioButton) rgcnd.findViewWithTag(getcond)).setChecked(true);
	            	String getother =db.decode(cur.getString(cur.getColumnIndex("fld_other")));
	            	if(getcond.equals("Other"))
	            	{
	            		otheret.setVisibility(v.VISIBLE);
	            	}
	            	else
	            	{
	            		otheret.setVisibility(v.GONE);
	            	}
	            	otheret.setText(getother);

	            	String getoption =cur.getString(cur.getColumnIndex("fld_checkbox"));
	            	cf.setValuetoCheckbox(cb, getoption);

	            	str_ip = cur.getString(cur.getColumnIndex("fld_inspectionpoint"));
	            	System.out.println("str_ip"+str_ip);
	            }
		 }
		 catch (Exception e) {
			// TODO: handle exception
		}
	}
	protected void checkboxvalidation() {
		// TODO Auto-generated method stub
		 String strchkval = cf.getvaluefromchk(cb);
		 
		 if(strchkval.equals(""))
		 {
			 cf.DisplayToast("Please select at least any one checkbox option");
		 }
		 else
		 {
			 if(str_ip.equals(""))
			 {
				 cf.DisplayToast("Please select Inspection Point");
			 }
			 else
			 {
				 try
					{
					    Cursor cur = db.hi_db.rawQuery("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"'", null);
					    System.out.println("select * from " + db.SetVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"'");
			            int cnt = cur.getCount();
			            if(cnt==0)
			            {
			            	db.hi_db.execSQL(" INSERT INTO "
										+ db.SetVC
										+ " (fld_inspectorid,fld_module,fld_submodule,fld_templatename,fld_primary,fld_secondary,fld_comments,fld_condition,fld_other,fld_checkbox,fld_inspectionpoint) VALUES"
										+ "('"+db.UserId+"','"+getmodulename+"','"+getsubmodulename+"','"+db.encode(gettempname)+"','"+getprimaryvc+"','"
										+getsecondaryvc+"','"+db.encode(strcomments)+"','"+strrd_cond+"','"+db.encode(strother)+"','"+strchkval+"','"+str_ip+"')");
			            }
			            else
			            {
			            	db.hi_db.execSQL("UPDATE "+db.SetVC+ " SET fld_primary='"+getprimaryvc+"',fld_secondary='"+getsecondaryvc+"',"
			            			+"fld_comments='"+db.encode(strcomments)+"',fld_condition='"+strrd_cond+"',fld_other='"+db.encode(strother)+"',fld_checkbox='"+strchkval+"',"
			            			+"fld_inspectionpoint='"+str_ip+"' WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_submodule='"+getsubmodulename+"' and fld_templatename='"+db.encode(gettempname)+"'");
						
			            }
					    cf.DisplayToast("Visible Condition saved successfully");
			            clear();
			            sp_primaryvc.setSelection(0);
					}
					catch (Exception e) {
						// TODO: handle exception
					}
			 }
		 }
	}
	protected void showalert() {
		// TODO Auto-generated method stub
		String str = "";
		final Dialog dialog = new Dialog(SetVC.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.getWindow().setContentView(R.layout.customizedialog);
	
		LinearLayout lin = (LinearLayout) dialog.findViewById(R.id.chk);
		lin.setOrientation(LinearLayout.VERTICAL);
		
		TextView hdr = (TextView)dialog.findViewById(R.id.header);
		hdr.setText("Inspection Point");
		
		if(getinspectionpoint.equals(""))
		{
			str=str;
		}
		else
		{
			str=getinspectionpoint;
		}
		ch = new CheckBox[cf.insp_arr.length];
		for (int i = 0; i < cf.insp_arr.length; i++) {

			ch[i] = new CheckBox(SetVC.this);
			ch[i].setText(cf.insp_arr[i]);
			ch[i].setTextColor(Color.BLACK);
			lin.addView(ch[i]);
		}

		if (str != "") {
			cf.setValuetoCheckbox(ch, str);
		}
		
		Button btnok = (Button) dialog.findViewById(R.id.ok);
		btnok.setText(" OK ");
		btnok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String seltdval = "";
				for (int i = 0; i < cf.insp_arr.length; i++) {
						if (ch[i].isChecked()) {
							seltdval += ch[i].getText().toString() + "&#44;";
						}
					}
			
				str_ip = seltdval;
				dialog.dismiss();
			}
		});
				
		Button btncancel = (Button) dialog.findViewById(R.id.cancel);
		btncancel.setVisibility(v.GONE);
		Button btnaddnew = (Button) dialog.findViewById(R.id.addnew);
		btnaddnew.setVisibility(v.GONE);
		
		dialog.show();
	}
	private class MyOnItemSelectedListenerdata implements OnItemSelectedListener {
	     
		int j;
		   public MyOnItemSelectedListenerdata(int i) {
			// TODO Auto-generated constructor stub
			   j=i;
		}

		public void onItemSelected(AdapterView<?> parent,
			        View view, int pos, long id) {	
		    if(j==1)
			{
				getprimaryvc = parent.getItemAtPosition(pos).toString();
				
				
				if(!getprimaryvc.equals("--Select--")&& scnt==0)				
				{
					try
					{
					    Cursor cur = db.hi_db.rawQuery("select * from " + db.AddVC + " WHERE fld_inspectorid='"+db.UserId+"' and fld_module='"+getmodulename+"' and fld_primary='"+getprimaryvc+"'", null);
			            int cnt = cur.getCount();
			           System.out.println("cnt="+cnt);
			            if(cnt>0)
			            {
			            	cur.moveToFirst();
			            	
						 	String get_pcon = cur.getString(cur.getColumnIndex("fld_primary"));
						 	int pc = primaryadap.getPosition(get_pcon);
			            	sp_primaryvc.setSelection(pc);
			            	
			            	String get_scon = cur.getString(cur.getColumnIndex("fld_secondary"));
			            	int sc = secondaryadap.getPosition(get_scon);
			            	sp_secondaryvc.setSelection(sc);
			            	
			            	String getcomments = db.decode(cur.getString(cur.getColumnIndex("fld_comments")));
			            	etcomments.setText(getcomments);
			            	
			            	String getcond =cur.getString(cur.getColumnIndex("fld_condition"));
			            	((RadioButton) rgcnd.findViewWithTag(getcond)).setChecked(true);
			            	String getother =db.decode(cur.getString(cur.getColumnIndex("fld_other")));
			            	if(getcond.equals("Other"))
			            	{
			            		otheret.setVisibility(v.VISIBLE);
			            	}
			            	else
			            	{
			            		otheret.setVisibility(v.GONE);
			            	}
			            	otheret.setText(getother);
	
			            	String getoption =cur.getString(cur.getColumnIndex("fld_checkbox"));
			            	cf.setValuetoCheckbox(cb, getoption);
	
			            	str_ip = cur.getString(cur.getColumnIndex("fld_inspectionpoint"));
			            }
			            else
			            {
			            	clear();
			            	
			            }
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
			else if(j==2)
			{
				getsecondaryvc = parent.getItemAtPosition(pos).toString();
			}
			
		 		
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	private void clear() {
		// TODO Auto-generated method stub
		
		 sp_secondaryvc.setSelection(0);
		 etcomments.setText("");
		 otheret.setVisibility(v.GONE);
		 otheret.setText("");
		 rgcond=true;
		 try
		 {
			 if(rgcond)
			 {
				 rgcnd.clearCheck();
			 }
		 }
		 catch (Exception e) {
			// TODO: handle exception
		}
		 for(int i=0;i<chkbxid.length;i++)
		 {
			 cb[i].setChecked(false);
		 }
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		back();
	}
	private void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(SetVC.this, NewSetVC.class);
		Bundle b2 = new Bundle();
		b2.putString("templatename", gettempname);			
		intent.putExtras(b2);
		Bundle b3 = new Bundle();
		b3.putString("currentview", getcurrentview);			
		intent.putExtras(b3);
		Bundle b4 = new Bundle();
		b4.putString("modulename", getmodulename);			
		intent.putExtras(b4);
		Bundle b5 = new Bundle();
		b5.putString("tempoption", gettempoption);			
		intent.putExtras(b5);
		Bundle b6 = new Bundle();
		b6.putString("submodulename", getsubmodulename);			
		intent.putExtras(b6);
		startActivity(intent);
		finish();
		
		
	}
}
